/***************************************************************************
                          ut_xml_par.h  -  description
                             -------------------
    begin                : Sat Jun 12 2004
    copyright            : (C) 2004 by Uukrul Telecom, Proshin Alexey
    email                : info@u-telecom.ru, microprosh@freemail.ru
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef UT_XML_PAR_H
#define UT_XML_PAR_H


/**It`s "ut" xml standart parser
  *@author Proshin Alexey
  */

#include "ut_xml_tag.hpp"

class ut_xml_par
{
    protected:

       ut_xml_par() {};

    public:

       virtual ~ut_xml_par() {};

       virtual void mem_char_format(const std::string &format) = 0;
       virtual int load(char const *f_name)=0;
       virtual void parse(const std::string &text)=0;

       virtual void to_text(std::string &text,const std::string &lend) const =0;

       virtual ut_xml_tag *get_root_tag()=0;

       virtual void clear()=0;

       virtual void release()=0;
};

ut_xml_par *create_ut_xml_parser();

#endif
