/***************************************************************************
                          ut_xml_tag.cpp  -  description
                             -------------------
    begin                : Sat Jun 12 2004
    copyright            : (C) 2004 by Uukrul Telecom, Proshin Alexey
    email                : info@u-telecom.ru, microprosh@freemail.ru
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "../ut_xml_tag.hpp"

// ----------------------------------------------------------
class ut_xml_tag_imp:public ut_xml_tag
{

  private:

     std::string name;
     std::string value;
     _tags tags;
     ut_xml_tag *parent;

  public:

      ut_xml_tag_imp():parent(NULL) {};
      virtual ~ut_xml_tag_imp() { for(unsigned int i=0;i<tags.size();i++) tags[i]->release(); };

      virtual ut_xml_tag *add(std::string _name,std::string _value);

      virtual _const_ptr_tags get_tags() const { return &tags; };
      virtual _ptr_tags get_tags() { return &tags; };

      virtual const ut_xml_tag *find(const std::string &path,const std::string *val=NULL) const;
      //virtual const ut_xml_tag *find_eq_by_name(const std::string &val,const std::string &path) const;

      virtual const std::string &get_value() const { return value; };
      virtual const std::string &get_name() const { return name; };

      virtual void set_value(const std::string &val) { value=val; };
      virtual void set_name(const std::string &n) { name=n; };

      virtual const ut_xml_tag *get_parent_tag() const { return parent; };
      virtual const ut_xml_tag *get_root_tag() const
      {
          const ut_xml_tag *u=this;
          while (u->get_parent_tag()) { u=u->get_parent_tag(); };
          return u;
      };

      virtual void to_text(std::string &text,const std::string &lend,const std::string &prefix) const;

      virtual void release();// { delete this; };

};

// ----------------------------------------------------------
ut_xml_tag *create_ut_xml_tag() { return new ut_xml_tag_imp; }

// ----------------------------------------------------------
/** Add new tag with given name an value to list of current tag... */
ut_xml_tag *ut_xml_tag_imp::add(std::string _name,std::string _value)
{
  ut_xml_tag_imp *tmp = new ut_xml_tag_imp;
  tmp->name = _name;
  tmp->value = _value;
  tmp->parent=this;
  tags.push_back(tmp);

  return tags.back();
}

// -----------------------------------------------------------
void ut_xml_tag_imp::release()
{
    delete this;
}

// -----------------------------------------------------------
const ut_xml_tag *ut_xml_tag_imp::find(const std::string &path,const std::string *val) const
{

    bool its_last=false;
    std::string name;
    std::string::size_type pos=path.find('.');
    if (pos==std::string::npos) { pos=path.length(); its_last=true; };

    name.assign(path,0,pos);

    _tags::const_iterator it = get_tags()->begin();
    while(it!=get_tags()->end())
    {
        if ((*it)->get_name()==name)
        {
           if (its_last)
           {
               if (!val) return (*it);
               if ((*it)->get_value()==*val) return (*it);
           }
           else
           {
               const ut_xml_tag *t=(*it)->find(std::string(path,pos+1,path.length()-pos-1),val);
               if (!val || t) return t;
           }
        }
        it++;
    }

    return NULL;
}

// -----------------------------------------------------------
void ut_xml_tag_imp::to_text(std::string &text,const std::string &lend,const std::string &prefix) const
{
    text+=lend;
    text+=prefix;
    text+='<';
    text+=name;
    text+='>';

    if (value.length())
    {
        text+=value;
        text+="</";
        text+=name;
        text+='>';
    }
    else
    {
        _tags::const_iterator tit = tags.begin();
        while(tit!=tags.end())
        {
            (*tit)->to_text(text,lend,prefix+' ');
            tit++;
        }

        text+=lend;
        text+=prefix;
        text+="</";
        text+=name;
        text+='>';
        //text+=lend;
    }

}


