#include "../ut_xml_par.hpp"
#include <iostream>

static int level;

void print_sp()
{
    for(int i=0;i<level;i++) std::cout << ' ';
}

void print_tag(ut_xml_tag *tag)
{
    print_sp();
    std::cout << "!" << tag->get_name().c_str() << "!" << std::endl;;
    level++;

    ut_xml_tag::_tags::iterator it=tag->get_tags()->begin();
    while(it!=tag->get_tags()->end())
    {
        print_tag((*it));
        it++;
    }
    
    level--;    
    print_sp();
    std::cout << "!?" << tag->get_name().c_str() << "?!" << std::endl;    
}

void main()
{
    ut_xml_par *par=create_ut_xml_parser();

    par->load("c:\\a.dat");

    level=0;
    print_tag(par->get_root_tag());

    par->release();
}
