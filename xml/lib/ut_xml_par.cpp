/***************************************************************************
                          ut_xml_par.cpp  -  description
                             -------------------
    begin                : Sat Jun 12 2004
    copyright            : (C) 2004 by Uukrul Telecom, Proshin Alexey
    email                : info@u-telecom.ru, microprosh@freemail.ru
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "../ut_xml_par.hpp"
#include <iostream>
#include <fstream>
#include <codepage.hpp>

// -----------------------------------------------
class ut_xml_par_imp:public ut_xml_par
{
    private:

      std::string cm_char_format;

      ut_xml_tag *root_tag;

      void parse_tag(ut_xml_tag &tag,std::string &src_text);

      /** Parse option string text to iner format & bind it to given tag... */
      void parse_options(ut_xml_tag *tag,const std::string &opt_string);

      /** Parse comment tag <?comment or settings?>. Tag text give by tag_head. Full text may be change in this function */
      void parse_comment_tag(ut_xml_tag &tag,const std::string &tag_head,std::string &full_text);

      /** Parse simple tag <name options/>. Tag text give by tag_head.*/
      void parse_simple_tag(ut_xml_tag &tag,const std::string &tag_head);

      /** Parse normal tag <name options>value</name>. Tag head "name options" give by tag_head. begin_pos - its position of tag in full_text */
      /* ITS return length of all tag */
      int parse_normal_tag(ut_xml_tag &tag,const std::string &tag_head,int begin_pos,std::string &full_text);

      /** Decode given text from given format to ASCII*/
      void decode(std::string &text,const std::string &format);
      /** Decode given text from UTF-8 format to ASCII*/
      void decode_from_utf8(std::string &text);

    public:

      ut_xml_par_imp():cm_char_format("win1251"),root_tag(create_ut_xml_tag()) {};
      virtual ~ut_xml_par_imp() { root_tag->release(); };

      /** load xml data from file. Return 0 if done, not null if error was. */
      virtual int load(char const *f_name);

      /** Parse given text to iner format... */
      virtual void parse(const std::string &text) { std::string tmp=text; parse_tag(*root_tag,tmp); };

      virtual void to_text(std::string &text,const std::string &lend) const;

      /** Get root tag of xml. */
      virtual ut_xml_tag *get_root_tag() { return root_tag; };

      virtual void clear() { root_tag->release(); root_tag=create_ut_xml_tag(); };

      virtual void release() { delete this; };

      virtual void mem_char_format(const std::string &format) { cm_char_format=format; };

};

ut_xml_par *create_ut_xml_parser() { return new ut_xml_par_imp; }

// -----------------------------------------------
/** load xml data from file. Return 0 if done, not null if error was. */
int ut_xml_par_imp::load(char const *f_name)
{

  std::fstream f;
  f.open(f_name,std::ios::in | std::ios::binary);
  if (!f) return -1;

  char *tmp;

  f.seekg(0,std::ios::end);
  int size = f.tellg();
  f.seekg(0,std::ios::beg);

  tmp = new char [size];
  f.read(tmp,size);

  f.close();

  //To_Koi8R((unsigned char *)tmp,size);


  std::string text(tmp, size);
  delete [] tmp;

  parse(text);

  return 0;
}
// ---------------------------------------------------------------------
/** Parse one tag with text. Bind it to tag... */
void ut_xml_par_imp::parse_tag(ut_xml_tag &tag,std::string &text)
{

  std::string::size_type tbb = 0;
  std::string::size_type tbe = 0;


  while(1)
  {
      // 1. find first < & >
      tbb = text.find('<',tbb);
      tbe = text.find('>',tbb+1);

      if (tbb==std::string::npos || tbe==std::string::npos) return;  // no tag here, its value string or end

      tbb++;
      std::string tag_head(text,tbb,tbe-tbb);
      if (tag_head.length()==0) { tbb+=2; continue; }  // for <> sitiation

      std::string tag_name;
      std::string tag_options;
      if (tag_head[0]=='?')                         // its coment tag <?...?>
      {
          parse_comment_tag(tag,std::string(tag_head,1,tag_head.length()-2),text);
          tbb+=tag_head.length()+1;
      }
      else if (tag_head[tag_head.length()-1]=='/')  // its simple tag <..../>
      {
          parse_simple_tag(tag,std::string(tag_head,0,tag_head.length()-1));
          tbb+=tag_head.length()+1;
      }
      else                                          // its normal tag <...>...</...>
      {
          tbb+=parse_normal_tag(tag,std::string(tag_head,0,tag_head.length()),tbb,text);
      }
  }

}

// ---------------------------------------------------------------------
/** Parse option string text to iner format & bind it to given tag... */
void ut_xml_par_imp::parse_options(ut_xml_tag *tag,const std::string &opt_string)
{
    unsigned int n_pos=0;

    while (1)
    {
        for(;n_pos<opt_string.length();n_pos++) if (opt_string[n_pos]!=' ') break;
        if (n_pos==opt_string.length()) return;    // error - only space chars in string

        unsigned int eq_pos=n_pos;
        unsigned int e_pos = eq_pos;
        for(;eq_pos<opt_string.length();eq_pos++)
        {
            e_pos = eq_pos;
            if (opt_string[eq_pos]=='=') break;
            if (opt_string[eq_pos]==' ')
            {
                while (opt_string[eq_pos]==' ')
                    eq_pos++;
                if (opt_string[eq_pos] != '=')
                    return;
                break;
            }
        }
        if (eq_pos==opt_string.length()) return;   // error - no = after name

        std::string name=std::string(opt_string,n_pos,e_pos-n_pos);

        eq_pos++;
        while (opt_string[eq_pos] == ' ') eq_pos++;
        unsigned int v_e_pos=eq_pos;
        if (opt_string[eq_pos]=='"')
        {
            v_e_pos++;
            eq_pos++;
            for(;v_e_pos<opt_string.length();v_e_pos++)
               if (opt_string[v_e_pos]=='"')
               {
                  if (v_e_pos+1==opt_string.length()) { v_e_pos++; break; }
                  if (opt_string[v_e_pos+1]=='"') { v_e_pos++; continue; };
                  v_e_pos++;
                  break;
               }
            n_pos=v_e_pos+1;
        } else
        {
            for(;v_e_pos<opt_string.length();v_e_pos++) if (opt_string[v_e_pos]==' ') break;
            v_e_pos++;
            n_pos=v_e_pos;
        }

        std::string value=std::string(opt_string,eq_pos,v_e_pos-eq_pos-1);

        // TODO : rebuild value (if need) - clear ""->" and so on...

        //cout << "name=" << name << " value=" << value << endl;

        tag->add(name,value);

        if (n_pos>=opt_string.length()) break;
    }

}

// ---------------------------------------------------------------------
/** Parse comment tag <?comment or settings?>. Tag text give by tag_head. Full text may be change in this function */
void ut_xml_par_imp::parse_comment_tag(ut_xml_tag &tag,const std::string &tag_head,std::string &full_text)
{
    unsigned int i;
    std::string tag_name;
    for(i=0;i<tag_head.length();i++)
        if (tag_head[i]==' ')
        {
            tag_name=std::string(tag_head,1,i-1);
            break;
        }

     if (tag_name=="") return ;

     i++;
     if (tag_head.length()==i)  return; // don`t have options after name

     ut_xml_tag *tmp_tag=create_ut_xml_tag();
     parse_options(tmp_tag,std::string(tag_head,i,tag_head.length()-i));

     ut_xml_tag::_tags::iterator it=tmp_tag->get_tags()->begin();
     while(it!=tmp_tag->get_tags()->end())
     {
         if ((*it)->get_name()=="encoding") decode(full_text,(*it)->get_value());
         it++;
     }

     tmp_tag->release();
}

// ---------------------------------------------------------------------
/** Parse simple tag <name options/>. Tag text give by tag_head.*/
void ut_xml_par_imp::parse_simple_tag(ut_xml_tag &tag,const std::string &tag_head)
{
    unsigned int i;
    std::string tag_name;
    for(i=0;i<tag_head.length();i++)
        if (tag_head[i]==' ')
        {
            tag_name.assign(tag_head,0,i);
            break;
        }

     if (tag_name=="")  /* empty tag <tag_name/> */
     {
         tag.add(tag_head,"");
         return ;
     }

     ut_xml_tag *u=tag.add(tag_name,"");

     i++;
     if (tag_head.length()==i)  return;      // don`t have options after name
     parse_options(u,std::string(tag_head,i,tag_head.length()-i));

}

// ---------------------------------------------------------------------
/** Parse normal tag <name options>value</name>. Tag head "name options" give by tag_head. begin_pos - its position of tag in full_text */
/* ITS return length of all tag */
int ut_xml_par_imp::parse_normal_tag(ut_xml_tag &tag,const std::string &tag_head,int begin_pos,std::string &full_text)
{
    if (tag_head.length()==0) return 2;

    unsigned int i;
    std::string tag_name;
    for(i=0;i<tag_head.length();i++)
        if (tag_head[i]==' ')
        {
            tag_name=std::string(tag_head,0,i);
            break;
        }

     if (tag_name=="")    // tag <tag_name>... no options
     {
         i--;
         tag_name=tag_head;
     }

     ut_xml_tag *u=tag.add(tag_name,"");

     i++;
     if (tag_head.length()!=i) parse_options(u,std::string(tag_head,i,tag_head.length()-i));
     // else don`t have options after name

     std::string open_tag="<";
     open_tag+=tag_name;

     std::string close_tag="</";
     close_tag+=tag_name;

     int level=1;
     int last=begin_pos+1;

     while(level>0)
     {
         std::string::size_type pos_open=full_text.find(open_tag,last);
         if (pos_open==std::string::npos) pos_open=full_text.length();

         std::string::size_type pos_close=full_text.find(close_tag,last);
         if (pos_close==std::string::npos) return 1;

         if (pos_open<pos_close) { last=pos_open+1; level++; } else { level--; last=pos_close+1; };
     }
     last--;

     int num_tags = u->get_tags()->size();
     std::string tmp_ss=std::string(full_text,begin_pos+tag_head.length()+1,last-begin_pos-tag_head.length()-1);
     parse_tag(*u,tmp_ss);

     if (u->get_tags()->size() == num_tags) u->set_value(std::string(full_text,begin_pos+tag_head.length()+1,last-begin_pos-tag_head.length()-1));

     //cout << "----" << endl;
     //cout << std::string(full_text,begin_pos+tag_head.length()+2,last-begin_pos-tag_head.length()-2) << endl;

     return last+close_tag.length()+1-begin_pos;
}


// ---------------------------------------------------------------------
/** Decode given text from given format to ASCII*/
void ut_xml_par_imp::decode(std::string &text,const std::string &format)
{
    if (format=="UTF-8")
    {
        if (cm_char_format=="win1251") _codepage::utf8_to_win1251(text);
    }
}

// ---------------------------------------------------------------------
void ut_xml_par_imp::to_text(std::string &text,const std::string &lend) const
{
    text.clear();

    ut_xml_tag::_tags::const_iterator it = root_tag->get_tags()->begin();
    while(it!=root_tag->get_tags()->end())
    {
        (*it)->to_text(text,lend,"");
        it++;
    }
	text+=lend;
}



