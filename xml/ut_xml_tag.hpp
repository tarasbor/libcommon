/***************************************************************************
                          ut_xml_tag.h  -  description
                             -------------------
    begin                : Sat Jun 12 2004
    copyright            : (C) 2004 by Uukrul Telecom, Proshin Alexey
    email                : info@u-telecom.ru, microprosh@freemail.ru
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef UT_XML_TAG_H
#define UT_XML_TAG_H

#include <string>
#include <vector>

/**One ut xml tag.
  *@author Proshin Alexey
  */


class ut_xml_tag
{
    protected:

      ut_xml_tag() {};

    public:

      virtual ~ut_xml_tag() {};

      typedef std::vector<ut_xml_tag *> _tags;
      typedef std::vector<ut_xml_tag *>* _ptr_tags;
      typedef const std::vector<ut_xml_tag *>* _const_ptr_tags;

      virtual ut_xml_tag *add(std::string _name,std::string _value)=0;

      virtual _const_ptr_tags get_tags() const=0;
      virtual _ptr_tags get_tags()=0;

      virtual const ut_xml_tag *find(const std::string &path,const std::string *val=NULL) const =0;
      //virtual const ut_xml_tag *find_eq_by_name(const std::string &val,const std::string &path) const =0;

      virtual const std::string &get_value() const =0;
      virtual const std::string &get_name() const =0;

      virtual void set_value(const std::string &val) = 0;
      virtual void set_name(const std::string &n) = 0;

      virtual const ut_xml_tag *get_parent_tag() const = 0;
      virtual const ut_xml_tag *get_root_tag() const = 0;

      virtual void to_text(std::string &text,const std::string &lend,const std::string &prefix) const = 0;

      virtual void release()=0;

};

ut_xml_tag *create_ut_xml_tag();

#endif
