#include "misc.hpp"
#include <stdlib.h>     /* srand, rand */

unsigned int gen_random_number(const unsigned int from, const unsigned int to) {
    return rand() % to + from;
}
