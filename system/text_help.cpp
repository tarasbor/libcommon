#include "text_help.hpp"
#include <string.h>

#include <algorithm> // for std::search
#include <iomanip>      //setfill,setw

#include <iostream>


using namespace std;

// ------------------------------------------------------------------------------
std::string _text_help::replaceAll(std::string& strInOut, const std::string& find, const std::string& replaceTo)
{
    size_t start_pos = 0;
    while((start_pos = strInOut.find(find, start_pos)) != std::string::npos) {
        strInOut.replace(start_pos, find.length(), replaceTo);
        start_pos += replaceTo.length(); // Handles case where 'replaceTo' is a substring of 'find'
    }
    return strInOut;
}

// ------------------------------------------------------------------------------
std::string _text_help::ToHex(const std::string& s, bool upper_case)
{
    std::ostringstream ret;
    for (std::string::size_type i = 0; i < s.length(); ++i) {
        ret  << std::setfill('0') << std::setw(2) << std::hex << (upper_case ? std::uppercase : std::nouppercase) << (int)(unsigned char)s[i];
    }
    return ret.str();
}

// ------------------------------------------------------------------------------
std::string _text_help::ToHexForBrowser(const std::string& s, bool upper_case)
{
    std::ostringstream ret;
    for (std::string::size_type i = 0; i < s.length(); ++i) {
        ret  << '%' << std::setfill('0') << std::setw(2) << std::hex << (upper_case ? std::uppercase : std::nouppercase) << (int)(unsigned char)s[i];
    }
    return ret.str();
}


//
int char2int(char input)
{
    if(input >= '0' && input <= '9')
        return input - '0';
    if(input >= 'A' && input <= 'F')
        return input - 'A' + 10;
    if(input >= 'a' && input <= 'f')
        return input - 'a' + 10;
    throw std::invalid_argument("Invalid input string");
}

// This function assumes src to be a zero terminated sanitized string with
// an even number of [0-9a-f] characters, and target to be sufficiently large
void hex2bin(const char* src, char* target)
{
    while(*src && src[1])
    {
        *(target++) = char2int(*src)*16 + char2int(src[1]);
        src += 2;
    }
}

// ------------------------------------------------------------------------------
std::string _text_help::replaceHexBrowserToStr(const std::string& in)
{
    string result(in);
    size_t  pos=0;
    while( (pos=result.find('%',pos++)) != std::string::npos ) {
        // find first byte
        if( pos + 3 >= result.size() ) continue;    // ...%D0
        // find second byte
        size_t pos2 = result.find('%',pos+3);       // %D0%
        if(pos2!=pos+3)        continue;            // second % not foounded
        if(pos2+2>=result.size())   continue;       // ...%D0%
        std::string hexstr = result.substr(pos+1,2);    // cut D0 ...%D0%99...
        hexstr+= result.substr(pos+4,2);     // cut 99 ...%D0%99...
        //std::cout << "debugonly hexstring:" << hexstr << std::endl;
        char bytes[2]={0,0};
        hex2bin(hexstr.c_str(),bytes);  // convert 4 symbols D099 to 2 bytes
        std::string str;
        for(int i=0; i<2; i++) {
            str.push_back(bytes[i]);
        }
        //cout << "debugonly to hex:"<< ToHex(str) << endl;

        // replace result
        result.replace(pos,6,str);
        pos++;  // %D0%99 replaced to two byte Й
    }

    return result;
}

// --------------------------------------------------------------------------------
std::string _text_help::get_field_from_st(const std::string &in,char delim,int f_n)
{
    int c_f=0;
    std::string::size_type pos_b=0;
    std::string::size_type pos_e=0;
    while(1)
    {
//        if (in[pos_e]==' ' || pos_e==in.length() || in[pos_e]=='\r' || in[pos_e]==';')
        if (in[pos_e]==delim || pos_e==in.length())
        {
            if (f_n==c_f)
            {
                return std::string(in,pos_b,pos_e-pos_b);
            }
            pos_b=pos_e+1;
            if (pos_e==in.length()) return "";
            c_f++;
        }
        pos_e++;
    }
}

// --------------------------------------------------------------------------------
std::string _text_help::get_field_from_st(const std::string &in,const char *delim,int f_n)
{
    int c_f=0;
    std::string::size_type pos_b=0;
    std::string::size_type pos_e=0;
    while(1)
    {
        if (strchr(delim,in[pos_e]) || pos_e==in.length())
        {
            if (f_n==c_f)
            {
                return std::string(in,pos_b,pos_e-pos_b);
            }
            pos_b=pos_e+1;
            if (pos_e==in.length()) return "";
            c_f++;
        }
        pos_e++;
    }
}

// --------------------------------------------------------------------------------
void _text_help::split_text_by(const std::string &text,const char *delim,list<std::string> &l)
{
    l.clear();

    int delim_len=strlen(delim);

    std::string::size_type old_pos=0;
    while(1)
    {
        std::string::size_type pos=text.find(delim,old_pos);
        if (pos==std::string::npos) break;

        l.push_back(std::string(text,old_pos,pos-old_pos));

        old_pos=pos+delim_len;
    }

    if (old_pos!=text.length()) l.push_back(std::string(text,old_pos,text.length()-old_pos));
}

// --------------------------------------------------------------------------------
void _text_help::split_text_by_endl(const std::string &text,list<std::string> &l)
{
    l.clear();

    std::string::size_type old_pos=0;
    while(1)
    {
		// We MUST have \n in both cases - in case \n and in \r\n case
		// so, search \n
        std::string::size_type pos=text.find('\n',old_pos);
		if (pos==std::string::npos) break;

		int len=1;

		// check \r befor \n
		if (pos!=0)
		{
			if (text[pos-1]=='\r')
			{
				pos--;
				len=2;
			}
		}

        l.push_back(std::string(text,old_pos,pos-old_pos));

        old_pos=pos+len;
    }

    if (old_pos<text.length()) l.push_back(std::string(text,old_pos,text.length()-old_pos));
}

// --------------------------------------------------------------------------------
void _text_help::string_to_map(const std::string &in,std::map<string,string> &m,char delim)
{
    m.clear();

    std::string::size_type pos=0;
    std::string::size_type len=in.length();

    while(pos<len)
    {
        if (in[pos]==delim)
        {
            std::string::size_type lpos=pos;
            while(1)
            {
                if (in[lpos]==' ' || in[lpos]=='\r' || in[lpos]=='\n' || (lpos!=pos && in[lpos]=='=')) { lpos++; break; }
                if (lpos==0) break;
                lpos--;
            }

            std::string param=std::string(in,lpos,pos-lpos);
            if (param.empty()) { pos++; continue; }

            pos++;
            // may be "p1=" or "p1= " and so on
            if (pos==len || in[pos]==' ' || in[pos]=='\r' || in[pos]=='\n')
            {
                m[param]="";
                continue;
            }

            std::string::size_type rpos=pos;
            while(rpos<len)
            {
                if (in[rpos]==' ' || in[rpos]=='\r' || in[rpos]=='\n') break;
                rpos++;
            }
            m[param]=std::string(in,pos,rpos-pos);
        }

        pos++;
    }
}

// --------------------------------------------------------------------------------
bool _text_help::is_dig_value(const std::string &s)
{
    std::string::const_iterator i=s.begin();
    while(i!=s.end())
    {
        if ((*i)<'0' || (*i)>'9') return false;
        i++;
    }
    return true;
}

// --------------------------------------------------------------------------------
std::string _text_help::get_param_value_from_st(const std::string &in,const std::string &param)
{
    std::string::size_type pos=in.find(param+"=");
    if (pos==std::string::npos) return "";

    pos+=param.length()+1;

    //bool use_quot=false;
    if (in[pos]=='"')
    {
        pos++;
        return get_field_from_st(string(in,pos,in.length()-pos),'"',0);
    }

    return get_field_from_st(string(in,pos,in.length()-pos)," ,;\r",0);
}


// -------------------------------------------------------------------------------
// templated version of two_str_equal so it could work with both char and wchar_t
template<typename charT>
struct two_str_equal {
    two_str_equal( const std::locale& loc ) : loc_(loc) {}
    bool operator()(charT ch1, charT ch2) {
        return std::toupper(ch1, loc_) == std::toupper(ch2, loc_);
    }
private:
    const std::locale& loc_;
};

// find substring (case insensitive). it could work with both char and wchar_t
template<typename T>
int ci_find_substr( const T& str1, const T& str2, const std::locale& loc)
{
    typename T::const_iterator it = std::search( str1.begin(), str1.end(),
        str2.begin(), str2.end(), two_str_equal<typename T::value_type>(loc) );
    if ( it != str1.end() ) return it - str1.begin();
    else return -1; // not found
}

// wrapper for char string
std::string::size_type _text_help::ci_find_substr_char(const string &subject, const string &search, const locale &loc)
{
    int rez=ci_find_substr(subject,search,loc);
    if(rez==-1)
        return std::string::npos;
    else
        return rez;
}

