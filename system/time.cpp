#include "time.hpp"
#include <stdio.h>

#include <iostream>
using namespace std;

#ifdef WINDOWS
        #include <windows.h>
#else
        //#define WU_DIFF 116444736000000000
        #include <time.h>
        #include <sys/time.h>
#endif



// --------------------------------------------------------------------------
_time::_time():time_val(0)
{
}

_time::_time(const _time::time_val_i &st):time_val(st)
{
}


// --------------------------------------------------------------------------
_time::_time(int y,int m,int d,int h,int n,int s,int ms)
{
#ifdef WINDOWS
    SYSTEMTIME tm;
    SYSTEMTIME st;

    tm.wYear=y;
    tm.wMonth=m;
    tm.wDay=d;
    tm.wHour=h;
    tm.wMinute=n        ;
    tm.wSecond=s;
    tm.wMilliseconds=ms;

    TzSpecificLocalTimeToSystemTime(NULL,&tm,&st);

    FILETIME ft;
    SystemTimeToFileTime(&st,&ft);
    memcpy(&time_val,&ft,sizeof(time_val_i));
#else
    tm t;
    t.tm_year=y-1900;
    t.tm_mon=m-1;
    t.tm_mday=d;
    t.tm_hour=h;
    t.tm_min=n;
    t.tm_sec=s;
    t.tm_isdst=-1;   // use timezone information & system database

    time_val_i utval = mktime(&t);
    time_val = utval*10000000+ms*10000+WU_DIFF;
#endif
}

// --------------------------------------------------------------------------
bool _time::operator<(const _time &t) const
{
    return time_val<t.time_val;
}

// --------------------------------------------------------------------------
bool _time::operator>(const _time &t) const
{
    return time_val>t.time_val;
}

// --------------------------------------------------------------------------
bool _time::operator==(const _time &t) const
{
    return time_val==t.time_val;
}


// --------------------------------------------------------------------------
bool _time::operator>=(const _time &t) const
{
    return time_val>=t.time_val;
}

// --------------------------------------------------------------------------
bool _time::operator<=(const _time &t) const
{
    return time_val<=t.time_val;
}

// --------------------------------------------------------------------------
bool _time::operator!=(const _time &t) const
{
    return time_val!=t.time_val;
}

/*
// --------------------------------------------------------------------------
int _time::operator-(const _time &t) const
{
    time_val_i res = time_val - t.time_val;
    res/=10000;
    return int(res);
}
*/

// --------------------------------------------------------------------------
void _time::set_now()
{
#ifdef WINDOWS
    SYSTEMTIME tm;
    SYSTEMTIME st;

    GetLocalTime(&tm);

    TzSpecificLocalTimeToSystemTime(NULL,&tm,&st);

    FILETIME ft;
    SystemTimeToFileTime(&st,&ft);
    memcpy(&time_val,&ft,sizeof(time_val_i));
#else
    timespec ts;
    clock_gettime(0,&ts);

    time_val = (time_val_i)(ts.tv_sec)*10000000+ts.tv_nsec/100+WU_DIFF;
    //time_val = (time_val_i)(ts.tv_sec+tz_set.zone_s())*10000000+ts.tv_nsec/100+WU_DIFF;
#endif
}

// --------------------------------------------------------------------------
// LOCAL
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
int _time::year() const
{
#ifdef WINDOWS
    SYSTEMTIME st;
    SYSTEMTIME tm;

    FileTimeToSystemTime((FILETIME *)&time_val,&st);
    SystemTimeToTzSpecificLocalTime(NULL,&st,&tm);
    return tm.wYear;
#else
    return split().y;
#endif
}

// --------------------------------------------------------------------------
int _time::month() const
{
#ifdef WINDOWS
    SYSTEMTIME st;
    SYSTEMTIME tm;

    FileTimeToSystemTime((FILETIME *)&time_val,&st);
    SystemTimeToTzSpecificLocalTime(NULL,&st,&tm);
    return tm.wMonth;
#else
    return split().m;
#endif
}

// --------------------------------------------------------------------------
int _time::day() const
{
#ifdef WINDOWS
    SYSTEMTIME st;
    SYSTEMTIME tm;

    FileTimeToSystemTime((FILETIME *)&time_val,&st);
    SystemTimeToTzSpecificLocalTime(NULL,&st,&tm);
    return tm.wDay;
#else
    return split().d;
#endif
}

// --------------------------------------------------------------------------
int _time::day_of_week() const
{
#ifdef WINDOWS
    SYSTEMTIME st;
    SYSTEMTIME tm;

    FileTimeToSystemTime((FILETIME *)&time_val,&st);
    SystemTimeToTzSpecificLocalTime(NULL,&st,&tm);
    return tm.wDayOfWeek;
#else

    time_val_i ut=time_val-WU_DIFF;
    ut/=10000000;

    time_t utt=ut;

    tm uts;
    localtime_r(&utt,&uts);

    return uts.tm_wday;
#endif
}

// --------------------------------------------------------------------------
int _time::hour() const
{
#ifdef WINDOWS
    SYSTEMTIME st;
    SYSTEMTIME tm;

    FileTimeToSystemTime((FILETIME *)&time_val,&st);
    SystemTimeToTzSpecificLocalTime(NULL,&st,&tm);
    return tm.wHour;
#else
    return split().h;
#endif
}

// --------------------------------------------------------------------------
int _time::minute() const
{
#ifdef WINDOWS
    SYSTEMTIME st;
    SYSTEMTIME tm;

    FileTimeToSystemTime((FILETIME *)&time_val,&st);
    SystemTimeToTzSpecificLocalTime(NULL,&st,&tm);
    return tm.wMinute;
#else
    return split().n;
#endif
}

// --------------------------------------------------------------------------
int _time::second() const
{
#ifdef WINDOWS
    SYSTEMTIME st;
    SYSTEMTIME tm;

    FileTimeToSystemTime((FILETIME *)&time_val,&st);
    SystemTimeToTzSpecificLocalTime(NULL,&st,&tm);
    return tm.wSecond;
#else
    return split().s;
#endif
}

// --------------------------------------------------------------------------
int _time::msecond() const
{
#ifdef WINDOWS
    SYSTEMTIME st;
    SYSTEMTIME tm;

    FileTimeToSystemTime((FILETIME *)&time_val,&st);
    SystemTimeToTzSpecificLocalTime(NULL,&st,&tm);
    return tm.wMilliseconds;
#else
    return split().ms;
#endif
}

// --------------------------------------------------------------------------
void _time::split(_time::_ys &ys) const
{
#ifdef WINDOWS
    SYSTEMTIME st;
    SYSTEMTIME tm;

    FileTimeToSystemTime((FILETIME *)&time_val,&st);
    SystemTimeToTzSpecificLocalTime(NULL,&st,&tm);

    ys.y=tm.wYear;
    ys.m=tm.wMonth;
    ys.d=tm.wDay;
    ys.h=tm.wHour;
    ys.n=tm.wMinute;
    ys.s=tm.wSecond;
    ys.ms=tm.wMilliseconds;
#else
    time_val_i ut=time_val;
    ut-=WU_DIFF;
    //ut-=(time_val_i)tz_set.zone_s()*10000000;

    ut/=10000000;
    ys.ms=(time_val-WU_DIFF-ut*10000000)/10000;
    //ys.ms=(time_val-WU_DIFF-tz_set.zone_s()*10000000-ut*10000000)/10000;

    time_t utt=ut;

    tm uts;
    localtime_r(&utt,&uts);

    ys.y=uts.tm_year+1900;
    ys.m=uts.tm_mon+1;
    ys.d=uts.tm_mday;
    ys.h=uts.tm_hour;
    ys.n=uts.tm_min;
    ys.s=uts.tm_sec;
#endif
}

// --------------------------------------------------------------------------
void _time::year(int s)
{
#ifdef WINDOWS
    SYSTEMTIME tm;
    SYSTEMTIME st;
    FileTimeToSystemTime((FILETIME *)&time_val,&st);
    SystemTimeToTzSpecificLocalTime(NULL,&st,&tm);

    tm.wYear=s;

    TzSpecificLocalTimeToSystemTime(NULL,&tm,&st);
    SystemTimeToFileTime(&st,(FILETIME *)&time_val);
#else
    _ys ys;

    time_val_i ut=time_val-WU_DIFF;
    ut/=10000000;
    ys.ms=(time_val-WU_DIFF-ut*10000000)/10000;

    time_t utt=ut;

    tm uts;
    localtime_r(&utt,&uts);

    uts.tm_year=s-1900;

    time_val_i utval = mktime(&uts);
    time_val = utval*10000000+ys.ms*10000+WU_DIFF;
#endif
}

// --------------------------------------------------------------------------
void _time::month(int s)
{
#ifdef WINDOWS
    SYSTEMTIME tm;
    SYSTEMTIME st;
    FileTimeToSystemTime((FILETIME *)&time_val,&st);
    SystemTimeToTzSpecificLocalTime(NULL,&st,&tm);

    tm.wMonth=s;

    TzSpecificLocalTimeToSystemTime(NULL,&tm,&st);
    SystemTimeToFileTime(&st,(FILETIME *)&time_val);
#else
    _ys ys;

    time_val_i ut=time_val-WU_DIFF;
    ut/=10000000;
    ys.ms=(time_val-WU_DIFF-ut*10000000)/10000;

    time_t utt=ut;

    tm uts;
    localtime_r(&utt,&uts);

    uts.tm_mon=s-1;

    time_val_i utval = mktime(&uts);
    time_val = utval*10000000+ys.ms*10000+WU_DIFF;
#endif
}

// --------------------------------------------------------------------------
void _time::day(int s)
{
#ifdef WINDOWS
    SYSTEMTIME tm;
    SYSTEMTIME st;
    FileTimeToSystemTime((FILETIME *)&time_val,&st);
    SystemTimeToTzSpecificLocalTime(NULL,&st,&tm);

    tm.wDay=s;

    TzSpecificLocalTimeToSystemTime(NULL,&tm,&st);
    SystemTimeToFileTime(&st,(FILETIME *)&time_val);
#else
    _ys ys;

    time_val_i ut=time_val-WU_DIFF;
    ut/=10000000;
    ys.ms=(time_val-WU_DIFF-ut*10000000)/10000;

    time_t utt=ut;

    tm uts;
    localtime_r(&utt,&uts);

    uts.tm_mday=s;

    time_val_i utval = mktime(&uts);
    time_val = utval*10000000+ys.ms*10000+WU_DIFF;
#endif
}

// --------------------------------------------------------------------------
void _time::hour(int s)
{
#ifdef WINDOWS
    SYSTEMTIME tm;
    SYSTEMTIME st;
    FileTimeToSystemTime((FILETIME *)&time_val,&st);
    SystemTimeToTzSpecificLocalTime(NULL,&st,&tm);

    tm.wHour=s;

    TzSpecificLocalTimeToSystemTime(NULL,&tm,&st);
    SystemTimeToFileTime(&st,(FILETIME *)&time_val);
#else
    _ys ys;

    time_val_i ut=time_val-WU_DIFF;
    ut/=10000000;
    ys.ms=(time_val-WU_DIFF-ut*10000000)/10000;

    time_t utt=ut;

    tm uts;
    localtime_r(&utt,&uts);

    uts.tm_hour=s;

    time_val_i utval = mktime(&uts);
    time_val = utval*10000000+ys.ms*10000+WU_DIFF;
#endif
}

// --------------------------------------------------------------------------
void _time::minute(int s)
{
#ifdef WINDOWS
    SYSTEMTIME tm;
    SYSTEMTIME st;
    FileTimeToSystemTime((FILETIME *)&time_val,&st);
    SystemTimeToTzSpecificLocalTime(NULL,&st,&tm);

    tm.wMinute=s;

    TzSpecificLocalTimeToSystemTime(NULL,&tm,&st);
    SystemTimeToFileTime(&st,(FILETIME *)&time_val);
#else
    _ys ys;

    time_val_i ut=time_val-WU_DIFF;
    ut/=10000000;
    ys.ms=(time_val-WU_DIFF-ut*10000000)/10000;

    time_t utt=ut;

    tm uts;
    localtime_r(&utt,&uts);

    uts.tm_min=s;

    time_val_i utval = mktime(&uts);
    time_val = utval*10000000+ys.ms*10000+WU_DIFF;
#endif
}

// --------------------------------------------------------------------------
void _time::second(int s)
{
#ifdef WINDOWS
    SYSTEMTIME tm;
    SYSTEMTIME st;
    FileTimeToSystemTime((FILETIME *)&time_val,&st);
    SystemTimeToTzSpecificLocalTime(NULL,&st,&tm);

    tm.wSecond=s;

    TzSpecificLocalTimeToSystemTime(NULL,&tm,&st);
    SystemTimeToFileTime(&st,(FILETIME *)&time_val);
#else
    _ys ys;

    time_val_i ut=time_val-WU_DIFF;
    ut/=10000000;
    ys.ms=(time_val-WU_DIFF-ut*10000000)/10000;

    time_t utt=ut;

    tm uts;
    localtime_r(&utt,&uts);

    uts.tm_sec=s;

    time_val_i utval = mktime(&uts);
    time_val = utval*10000000+ys.ms*10000+WU_DIFF;
#endif
}

// --------------------------------------------------------------------------
void _time::msecond(int s)
{
#ifdef WINDOWS
    SYSTEMTIME tm;
    SYSTEMTIME st;
    FileTimeToSystemTime((FILETIME *)&time_val,&st);
    SystemTimeToTzSpecificLocalTime(NULL,&st,&tm);

    tm.wMilliseconds=s;

    TzSpecificLocalTimeToSystemTime(NULL,&tm,&st);
    SystemTimeToFileTime(&st,(FILETIME *)&time_val);

    /*SYSTEMTIME tm;
    FileTimeToSystemTime((FILETIME *)&time_val,&tm);

    tm.wMilliseconds=s;

    SystemTimeToFileTime(&tm,(FILETIME *)&time_val);
    */
#else
    time_val_i ut=time_val-WU_DIFF;
    ut/=10000000;
    time_val = ut*10000000+s*10000+WU_DIFF;
#endif
}

// --------------------------------------------------------------------------
// UTC
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
int _time::utc_year() const
{
#ifdef WINDOWS
    //TODO
    /*SYSTEMTIME tm;

    FileTimeToSystemTime((FILETIME *)&time_val,&tm);
    return tm.wYear;
    */
    return 0;
#else
    return utc_split().y;
#endif
}

// --------------------------------------------------------------------------
int _time::utc_month() const
{
#ifdef WINDOWS
    //TODO
    /*SYSTEMTIME tm;

    FileTimeToSystemTime((FILETIME *)&time_val,&tm);
    return tm.wMonth;
    */
    return 0;
#else
    return utc_split().m;
#endif
}

// --------------------------------------------------------------------------
int _time::utc_day() const
{
#ifdef WINDOWS
    //TODO
    /*SYSTEMTIME tm;

    FileTimeToSystemTime((FILETIME *)&time_val,&tm);
    return tm.wDay;
    */
    return 0;
#else
    return utc_split().d;
#endif
}

// --------------------------------------------------------------------------
int _time::utc_day_of_week() const
{
#ifdef WINDOWS
    //TODO
    /*SYSTEMTIME tm;

    FileTimeToSystemTime((FILETIME *)&time_val,&tm);
    return tm.wDayOfWeek;
    */
	// TODO
	return 0;
#else

    time_val_i ut=time_val-WU_DIFF;
    ut/=10000000;

    time_t utt=ut;

    tm uts;
    gmtime_r(&utt,&uts);

    return uts.tm_wday;
#endif
}

// --------------------------------------------------------------------------
int _time::utc_hour() const
{
#ifdef WINDOWS
    //TODO
    /*SYSTEMTIME tm;

    FileTimeToSystemTime((FILETIME *)&time_val,&tm);
    return tm.wHour;
    */
    return 0;
#else
    return utc_split().h;
#endif
}

// --------------------------------------------------------------------------
int _time::utc_minute() const
{
#ifdef WINDOWS
    //TODO
    /*SYSTEMTIME tm;

    FileTimeToSystemTime((FILETIME *)&time_val,&tm);
    return tm.wMinute;
    */
    return 0;
#else
    return utc_split().n;
#endif
}

// --------------------------------------------------------------------------
int _time::utc_second() const
{
#ifdef WINDOWS
    //TODO
    /*SYSTEMTIME tm;

    FileTimeToSystemTime((FILETIME *)&time_val,&tm);
    return tm.wSecond;
    */
    return 0;
#else
    return utc_split().s;
#endif
}

// --------------------------------------------------------------------------
int _time::utc_msecond() const
{
#ifdef WINDOWS
    //TODO
    /*SYSTEMTIME tm;

    FileTimeToSystemTime((FILETIME *)&time_val,&tm);
    return tm.wMilliseconds;
    */
    return 0;
#else
    return utc_split().ms;
#endif
}

// --------------------------------------------------------------------------
void _time::utc_split(_time::_ys &ys) const
{
#ifdef WINDOWS
    // TODO
    /*SYSTEMTIME tm;

    FileTimeToSystemTime((FILETIME *)&time_val,&tm);

    ys.y=tm.wYear;
    ys.m=tm.wMonth;
    ys.d=tm.wDay;
    ys.h=tm.wHour;
    ys.n=tm.wMinute;
    ys.s=tm.wSecond;
    ys.ms=tm.wMilliseconds;
    */
#else
    time_val_i ut=time_val;
    ut-=WU_DIFF;
    //ut-=(time_val_i)tz_set.zone_s()*10000000;

    ut/=10000000;
    ys.ms=(time_val-WU_DIFF-ut*10000000)/10000;
    //ys.ms=(time_val-WU_DIFF-tz_set.zone_s()*10000000-ut*10000000)/10000;

    time_t utt=ut;

    tm uts;
    gmtime_r(&utt,&uts);

    ys.y=uts.tm_year+1900;
    ys.m=uts.tm_mon+1;
    ys.d=uts.tm_mday;
    ys.h=uts.tm_hour;
    ys.n=uts.tm_min;
    ys.s=uts.tm_sec;
#endif
}


// --------------------------------------------------------------------------
// STAMPS
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
char *_time::stamp_to_str(char *s) const
{
#ifdef WATCOM
    sprintf(s,"%I64i",time_val);
#else
    sprintf(s,"%lli",time_val);
#endif
    return s;
}

// --------------------------------------------------------------------------
std::string &_time::stamp_to_str(std::string &s) const
{
    s=to_string(time_val);
    return s;
}

// --------------------------------------------------------------------------
std::string _time::stamp_to_str() const
{
    return to_string(time_val);
}

// --------------------------------------------------------------------------
void _time::str_to_stamp(const char *s)
{
#ifdef WATCOM
    sscanf(s,"%I64i",&time_val);
#else
    sscanf(s,"%lli",&time_val);
#endif
}

// --------------------------------------------------------------------------
void _time::str_to_stamp(const std::string &s)
{
    time_val=stoll(s);
}


// --------------------------------------------------------------------------
std::string _time::text() const
{
    string st;
    st=to_string(year());
    st+=".";
    st+=to_string(month());
    st+=".";
    st+=to_string(day());
    st+=" ";
    st+=to_string(hour());
    st+=":";
    st+=to_string(minute());
    st+=":";
    st+=to_string(second());
    st+=".";
    st+=to_string(msecond());
    return st;
}

// --------------------------------------------------------------------------
void _time::set_undefined()
{
    stamp(NOTDEFINED_TIMESTAMP);
}

// --------------------------------------------------------------------------
bool _time::undefined()
{
    return (stamp()==NOTDEFINED_TIMESTAMP)?true:false;
}


// --------------------------------------------------------------------------
bool _time::try_parse(const std::string &str)
{
    unsigned int timeval(0);
    string buff,ins(str);

    //parse year
    if(ins.find(".")==std::string::npos) {
        cout << "Wrong input time. Example: '2015.12.31 23:59:59.999', income: '" << str << "'" << endl;
        return false;
    }
    buff=ins.substr(0,ins.find("."));
    if(ins.find(".")+1>=ins.size()) {
        cout << "Wrong input time. Example: '2015.12.31 23:59:59.999', income: '" << str << "'" << endl;
        return false;
    }
    ins=ins.substr(ins.find(".")+1);
    if(buff.empty()) {
        cout << "Wrong input time. Example: '2015.12.31 23:59:59.999', income: '" << str << "'" << endl;
        return false;
    }
    timeval=stoi(buff);
    year(timeval);

    //parse month
    if(ins.find(".")==std::string::npos) {
        cout << "Wrong input time. Example: '2015.12.31 23:59:59.999', income: '" << str << "'" << endl;
        return false;
    }
    buff=ins.substr(0,ins.find("."));
    if(ins.find(".")+1>=ins.size()) {
        cout << "Wrong input time. Example: '2015.12.31 23:59:59.999', income: '" << str << "'" << endl;
        return false;
    }
    ins=ins.substr(ins.find(".")+1);
    if(buff.empty()) {
        cout << "Wrong input time. Example: '2015.12.31 23:59:59.999', income: '" << str << "'" << endl;
        return false;
    }
    timeval=stoi(buff);
    month(timeval);

    //parse day
    if(ins.find(" ")==std::string::npos) {
        cout << "Wrong input time. Example: '2015.12.31 23:59:59.999', income: '" << str << "'" << endl;
        return false;
    }
    buff=ins.substr(0,ins.find(" "));
    if(ins.find(" ")+1>=ins.size()) {
        cout << "Wrong input time. Example: '2015.12.31 23:59:59.999', income: '" << str << "'" << endl;
        return false;
    }
    ins=ins.substr(ins.find(" ")+1);
    if(buff.empty()) {
        cout << "Wrong input time. Example: '2015.12.31 23:59:59.999', income: '" << str << "'" << endl;
        return false;
    }
    timeval=stoi(buff);
    day(timeval);

    //parse hours
    if(ins.find(":")==std::string::npos) {
        cout << "Wrong input time. Example: '2015.12.31 23:59:59.999', income: '" << str << "'" << endl;
        return false;
    }
    buff=ins.substr(0,ins.find(":"));
    if(ins.find(":")+1>=ins.size()) {
        cout << "Wrong input time. Example: '2015.12.31 23:59:59.999', income: '" << str << "'" << endl;
        return false;
    }
    ins=ins.substr(ins.find(":")+1);
    if(buff.empty()) {
        cout << "Wrong input time. Example: '2015.12.31 23:59:59.999', income: '" << str << "'" << endl;
        return false;
    }
    timeval=stoi(buff);
    hour(timeval);

    //parse minutes
    if(ins.find(":")==std::string::npos) {
        cout << "Wrong input time. Example: '2015.12.31 23:59:59.999', income: '" << str << "'" << endl;
        return false;
    }
    buff=ins.substr(0,ins.find(":"));
    if(ins.find(":")+1>=ins.size()) {
        cout << "Wrong input time. Example: '2015.12.31 23:59:59.999', income: '" << str << "'" << endl;
        return false;
    }
    ins=ins.substr(ins.find(":")+1);
    if(buff.empty()) {
        cout << "Wrong input time. Example: '2015.12.31 23:59:59.999', income: '" << str << "'" << endl;
        return false;
    }
    timeval=stoi(buff);
    minute(timeval);

    //parse seconds
    if(ins.find(".")==std::string::npos) {
        cout << "Wrong input time. Example: '2015.12.31 23:59:59.999', income: '" << str << "'" << endl;
        return false;
    }
    buff=ins.substr(0,ins.find("."));
    if(ins.find(".")+1>=ins.size()) {
        cout << "Wrong input time. Example: '2015.12.31 23:59:59.999', income: '" << str << "'" << endl;
        return false;
    }
    ins=ins.substr(ins.find(".")+1);
    if(buff.empty()) {
        cout << "Wrong input time. Example: '2015.12.31 23:59:59.999', income: '" << str << "'" << endl;
        return false;
    }
    timeval=stoi(buff);
    second(timeval);

    //parse milliseconds
    buff=ins;
    if(buff.empty()) {
        cout << "Wrong input time. Example: '2015.12.31 23:59:59.999', income: '" << str << "'" << endl;
        return false;
    }
    timeval=stoi(buff);
    msecond(timeval);


    return true;
}
