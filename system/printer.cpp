#include <printer.hpp>

// --------------------------------------------------------
_printers_enum::_printers_enum():info_array(NULL)
{
    DWORD bytes_need = sizeof(PRINTER_INFO_2);
    DWORD flags = PRINTER_ENUM_LOCAL | PRINTER_ENUM_NAME;

    BOOL res = EnumPrinters(flags,
                            NULL,2,
                            NULL,0,
                            &bytes_need,
                            &printers_count);

    PRINTER_INFO_2 *i = (PRINTER_INFO_2 *)malloc(bytes_need);

         res = EnumPrinters(flags,
                            NULL,2,
                            (unsigned char *)i,bytes_need,
                            &bytes_need,
                            &printers_count);
    if (!res)
    {
        printers_count=0;
    }

    info_array = new _printer_info [printers_count];
    int j;

    for(j=0;j<printers_count;j++)
    {
        info_array[j].printer_name=i[j].pPrinterName;
        info_array[j].driver_name =i[j].pDriverName;
    }

    free(i);
}

// --------------------------------------------------------
_printer::_printer(int index)
{
    _printers_enum e;
    _printers_enum::iterator it = e.begin();
    while (it!=e.end() && index!=0)
    {
        it++;
        index--;
    }

    if (!index)
    {
        memcpy(&info,it,sizeof(info));
        found = true;
    }
}

// --------------------------------------------------------
_printer::_printer(const char *printer_name)
{
    _printers_enum *e  =new _printers_enum;
    _printers_enum::iterator it = e->begin();
    while (it!=e->end())
    {
        if (it->printer_name==printer_name)
        {
            info.printer_name=(*it).printer_name;
            info.driver_name =(*it).driver_name;
            found = true;

            delete e;

            return;
        }
        it++;
    }
}

// --------------------------------------------------------
void _printer::print(_imglib::_image *img)
{
    HDC pr_dc = CreateDC(info.driver_name.c_str(), info.printer_name.c_str(), NULL, NULL); //

    DOCINFO di;
    di.cbSize = sizeof(di);
    di.lpszDocName="GL DOC";
    di.lpszOutput=NULL;

    StartDoc(pr_dc,&di);
    StartPage(pr_dc);

    int ww = GetDeviceCaps(pr_dc, HORZRES);
    int wh = GetDeviceCaps(pr_dc, VERTRES);

    BITMAPINFO binfo;
    binfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
    binfo.bmiHeader.biWidth = img->w;
    binfo.bmiHeader.biHeight = -img->h;
    binfo.bmiHeader.biPlanes = 1;
    binfo.bmiHeader.biBitCount = 24;
    binfo.bmiHeader.biCompression = BI_RGB;
    binfo.bmiHeader.biSizeImage = 0;

    int x;
    int y;

    // In RGB data array given for StretchDIBits one row size (IN BYTES!!!) have align
    // we need calc bytes in one row & skeep step

    int bytes_in_row = (((img->w * 24) + 31) / 32) * 4;
    int skeep_step = bytes_in_row - (img->w * 3);

    unsigned char *RGB = new unsigned char [bytes_in_row * img->h];

    int d=0;

    for(y=0;y<img->h;y++)
    {
        for(x=0;x<img->w;x++)
        {
            // BGR <- RGB
            RGB[(x+y*img->w)*3+d+0] = img->rgba[(x+y*img->w)*4+2];
            RGB[(x+y*img->w)*3+d+1] = img->rgba[(x+y*img->w)*4+1];
            RGB[(x+y*img->w)*3+d+2] = img->rgba[(x+y*img->w)*4+0];
        }
        d+=skeep_step;

        if (!(y%100)) Sleep(10);
    }


    StretchDIBits(pr_dc,0,0,ww,wh,0,0,img->w,img->h,RGB,&binfo,DIB_RGB_COLORS,SRCCOPY);

    EndPage(pr_dc);
    EndDoc(pr_dc);
    DeleteDC(pr_dc);

    delete RGB;
}


// --------------------------------------------------------
void _printer::print(const unsigned char *img,int w,int h)
{
    HDC pr_dc = CreateDC(info.driver_name.c_str(), info.printer_name.c_str(), NULL, NULL); //

    DOCINFO di;
    di.cbSize = sizeof(di);
    di.lpszDocName="GL DOC";
    di.lpszOutput=NULL;

    StartDoc(pr_dc,&di);
    StartPage(pr_dc);

    int ww = GetDeviceCaps(pr_dc, HORZRES);
    int wh = GetDeviceCaps(pr_dc, VERTRES);

    BITMAPINFO binfo;
    binfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
    binfo.bmiHeader.biWidth = w;
    binfo.bmiHeader.biHeight = h;
    binfo.bmiHeader.biPlanes = 1;
    binfo.bmiHeader.biBitCount = 24;
    binfo.bmiHeader.biCompression = BI_RGB;
    binfo.bmiHeader.biSizeImage = 0;

    int x;
    int y;

    // In RGB data array given for StretchDIBits one row size (IN BYTES!!!) have align
    // we need calc bytes in one row & skeep step

    int bytes_in_row = (((w * 24) + 31) / 32) * 4;
    int skeep_step = bytes_in_row - (w * 3);

    unsigned char *RGB = new unsigned char [bytes_in_row * h];

    int d=0;

    for(y=0;y<h;y++)
    {
        for(x=0;x<w;x++)
        {
            // BGR <- RGB
            RGB[(x+y*w)*3+d+0] = img[(x+y*w)*4+2];
            RGB[(x+y*w)*3+d+1] = img[(x+y*w)*4+1];
            RGB[(x+y*w)*3+d+2] = img[(x+y*w)*4+0];
        }
        d+=skeep_step;

        //if (!(y%100)) Sleep(10);
    }


    StretchDIBits(pr_dc,0,0,ww,wh,0,0,w,h,RGB,&binfo,DIB_RGB_COLORS,SRCCOPY);

    EndPage(pr_dc);
    EndDoc(pr_dc);
    DeleteDC(pr_dc);

    delete RGB;
}



