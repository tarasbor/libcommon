#ifndef __TEXT_HELP_FUNCTIONS__
#define __TEXT_HELP_FUNCTIONS__

#include <string>
#include <list>
#include <map>

#include <locale> // ci_find_substr_char()
#include <sstream>  // for to_std_string()

namespace _text_help
{
    template <class T>
    inline std::string to_std_string (const T& t)
    {
        std::stringstream ss;
        ss << t;
        return ss.str();
    }

    std::string replaceAll(std::string& strInOut, const std::string& find, const std::string& replaceTo);

    std::string ToHex(const std::string& s, bool upper_case = true);
    std::string ToHexForBrowser(const std::string& s, bool upper_case = true);

    std::string replaceHexBrowserToStr(const std::string& in);

    std::string get_field_from_st(const std::string &in,char delim,int f_n);
    std::string get_field_from_st(const std::string &in,const char *delim,int f_n);

    void split_text_by(const std::string &text,const char *delim,std::list<std::string> &l);
	void split_text_by_endl(const std::string &text,std::list<std::string> &l);

	// make map - key is left side from delim word, value is right side word
	// Example 1 "param1=value1 param2=value2", delim='=' -> m[param1]=value1, m[param2]=value2
	// Example 2 "param1=value1\r\nparam2=value2", delim='=' -> m[param1]=value1, m[param2]=value2
	void string_to_map(const std::string &in,std::map<std::string,std::string> &m,char delim = '=');

	bool is_dig_value(const std::string &n);

	std::string get_param_value_from_st(const std::string &in,const std::string &param);



    /// Find first 'search' in 'subject' (substring), case insensitive.
    std::string::size_type ci_find_substr_char( const std::string &subject, const std::string &search, const std::locale& loc = std::locale() );


};

#endif
