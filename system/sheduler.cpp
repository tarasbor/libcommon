#include "sheduler.hpp"
#include <iostream>
#include <list>

using namespace std;

class _sheduler_imp: public _sheduler
{
    protected:

        struct _task_rec
        {
            _task *task;
            int last;

            bool operator==(const _task_rec &r) const { return task->name()==r.task->name(); };
            bool operator<(const _task_rec &r) const { return task->interval()<r.task->interval(); };
        };

        typedef list<_task_rec> _tasks;
        _tasks tasks;

        _locker cm_lock;

    public:

      _sheduler_imp();
      virtual ~_sheduler_imp();

      virtual int Poll();

      virtual void add_task(_task *t);
      virtual _task *remove_task(const std::string &name);
};

// --------------------------------------------------
_sheduler_imp::_sheduler_imp()
{
    SetSleepTime(1000);
}

// --------------------------------------------------
_sheduler_imp::~_sheduler_imp()
{
}

// --------------------------------------------------
int _sheduler_imp::Poll()
{
    // very basic version ....
    cm_lock.lock();

    _tasks::iterator it = tasks.begin();
    while(it!=tasks.end())
    {
        (*it).last--;
        if ((*it).last==0)
        {
            (*it).task->make_actions();
            (*it).last=(*it).task->interval();
        }
        it++;
    }

    cm_lock.unlock();

    return 0;
}


// --------------------------------------------------
void _sheduler_imp::add_task(_task *t)
{
    _task_rec rec;
    rec.task=t;
    rec.last=t->interval();

    cm_lock.lock();
    tasks.push_back(rec);
    cm_lock.unlock();
}


// --------------------------------------------------
_sheduler::_task *_sheduler_imp::remove_task(const std::string &name)
{
    cm_lock.lock();

    _tasks::iterator it = tasks.begin();
    while(it!=tasks.end())
    {
        if ((*it).task->name()==name)
        {
            _sheduler::_task *t=it->task;
            tasks.erase(it);

            cm_lock.unlock();

            return t;
        }
        it++;
    }

    cm_lock.unlock();

    return NULL;
}


// --------------------------------------------------
// --------------------------------------------------
_sheduler *create_sheduler()
{
    return new _sheduler_imp;
}
