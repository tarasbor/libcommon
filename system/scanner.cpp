#include "scanner.hpp"
#include <eztwain.h>

#include <iostream>
#include <fstream>

using namespace std;

// ----------------------------------------------------------------
_scanner::_scanner(HINSTANCE app_inst,HWND main_wnd)
    :_app_inst(app_inst),_main_wnd(main_wnd)
{
    
}

// ----------------------------------------------------------------
_scanner::~_scanner()
{
}

// ----------------------------------------------------------------
int _scanner::scan(_imglib::_image *img)
{

    //ofstream f;
    //f.open("c:\\123\\scan.log",ios::out | ios::binary);

    TWAIN_SetHideUI(TRUE);

    if (!TWAIN_OpenDefaultSource()) return -1;

    //TWAIN_SetCurrentUnits(TWAIN_INCHES);
    //cout << "SET RGB = " << TWAIN_SetCurrentPixelType(TWAIN_RGB) << endl;
    //cout << "SET BitDepth(8) = " << TWAIN_SetBitDepth(4) << endl;
    //TWAIN_SetCurrentResolution(300.0);
    
    HANDLE hdib = TWAIN_AcquireNative(_main_wnd, TWAIN_RGB);

    //TWAIN_WriteNativeToFilename(hdib, "c:\\123\\test_wn.bmp");

    if (!hdib) return -2;

    BITMAPINFOHEADER *info = (BITMAPINFOHEADER *)GlobalLock(hdib);
    unsigned char *p = ((unsigned char *)info) + sizeof(BITMAPINFOHEADER);

   
    //f << "w " << info->biWidth << endl;
    //f << "h " << info->biHeight << endl;
    //f << "Planes " << info->biPlanes << endl;
    //f << "BitCount " << info->biBitCount << endl;
    //f << "Compression " << info->biCompression << endl;
    //f << "SizeImage " << info->biSizeImage << endl;
    //f << "XPels " << info->biXPelsPerMeter << endl;
    //f << "YPels " << info->biYPelsPerMeter << endl;
    //f << "ClrUsed " << info->biClrUsed << endl;
    //f << "ClrImportant " << info->biClrImportant << endl;
    

    img->w = info->biWidth;
    /*
    img->w/=8;
    img->w*=8;
    if (img->w!=info->biWidth) img->w+=8;
    */
    img->h = info->biHeight;

    //cout << img->w << "x" << img->h << "x" << info->biBitCount << endl;
    
    if (img->rgba)
    {
        delete img->rgba;
        img->rgba = NULL;
    }
    img->rgba_size = img->w*img->h*4;
    img->rgba = new unsigned char [img->rgba_size];

    //f << "rgba size " << img->rgba_size << endl;;

    int x;
    int y;

    int bytes_in_row = (((info->biWidth * 24) + 31) / 32) * 4;
    
    /*int skeep_step = (info->biWidth*3/8);
    skeep_step *=8;
    if (skeep_step == info->biWidth*3)
    {
    }
    else
    {
        skeep_step +=8;
        skeep_step -=info->biWidth*3;
    }
    */
    int skeep_step = bytes_in_row - (info->biWidth * 3);

    //f << "skeep step " << skeep_step << endl;

    int d=skeep_step*(img->h-1);

    //f << "delta " << d << endl;
    
    for(y=0;y<img->h;y++)
    {
        //f << "line " << y;
        
        for(x=0;x<info->biWidth;x++)
        {
            img->rgba[(x+y*img->w)*4]=p[(x+(img->h-y-1)*info->biWidth)*3+2+d];
            img->rgba[(x+y*img->w)*4+1]=p[(x+(img->h-y-1)*info->biWidth)*3+1+d];
            img->rgba[(x+y*img->w)*4+2]=p[(x+(img->h-y-1)*info->biWidth)*3+d];
            img->rgba[(x+y*img->w)*4+3]=0xFF;
        }
        d-=skeep_step;

        //f << "ok...d =  " << d << endl;
        //f.flush();
    }

    GlobalUnlock(hdib);

    TWAIN_FreeNative(hdib);

    //f << "Free  " << endl;
    //f.flush();
    //f.close();
    
    return 0;
}
