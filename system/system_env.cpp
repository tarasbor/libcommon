#include "system_env.hpp"
#include <stdlib.h>
#include <stdio.h>
#include <sys/sysinfo.h>

using namespace std;

std::string se_get_exe_path(HINSTANCE app_inst)
{
#ifdef WINDOWS
    char tmp[MAX_PATH];
    char Path[_MAX_PATH];
    char Drive[_MAX_DRIVE];
    char Name[_MAX_FNAME];
    char Ext[_MAX_EXT];

    int cnt=GetModuleFileName(app_inst,tmp,MAX_PATH);
    if (!cnt)
    {
        DWORD err=GetLastError();
        err++;
    }
    tmp[cnt]='\0';
    _splitpath(tmp,Drive,Path,Name,Ext);
    sprintf(tmp,"%s%s",Drive,Path);

    return tmp;
#else
    char *tmp;

    tmp = getcwd(NULL,0);

    std::string rez = tmp;
    rez+='/';

    free(tmp);

    return rez;
#endif
}


long system_get_uptime()
{
    struct sysinfo s_info;
    int error = sysinfo(&s_info);
    if(error != 0)
    {
        return 0;   // error occured
    }
    return s_info.uptime;
}
