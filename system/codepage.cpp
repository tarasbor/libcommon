#include <codepage.hpp>

// ---------------------------------------------------------------------
unsigned int _codepage::win1251_to_unicode(unsigned int c)
{
    if (c<0x80) return c;

    unsigned int map[]={ 0x0402,0x0403,0x201A,0x0453,0x201E,0x2026,0x2020,0x2021,0x20AC,0x2030,0x0409,0x2039,0x040A,0x040C,0x040B,0x040F,
                         0x0452,0x2018,0x2019,0x201C,0x201D,0x2022,0x2013,0x2014,0x0428,0x2122,0x0459,0x203A,0x045A,0x045C,0x045B,0x045F,
                         0x00A0,0x040E,0x045E,0x0408,0x00A4,0x0490,0x00A6,0x00A7,0x0401,0x00A9,0x0404,0x00AB,0x00AC,0x00AD,0x00AE,0x0407,
                         0x00B0,0x00B1,0x0406,0x0456,0x0491,0x00B5,0x00B6,0x00B7,0x0451,0x2116,0x0454,0x00BB,0x0458,0x0405,0x0455,0x0457,
                         0x0410,0x0411,0x0412,0x0413,0x0414,0x0415,0x0416,0x0417,0x0418,0x0419,0x041A,0x041B,0x041C,0x041D,0x041E,0x041F,
                         0x0420,0x0421,0x0422,0x0423,0x0424,0x0425,0x0426,0x0427,0x0428,0x0429,0x042A,0x042B,0x042C,0x042D,0x042E,0x042F,
                         0x0430,0x0431,0x0432,0x0433,0x0434,0x0435,0x0436,0x0437,0x0438,0x0439,0x043A,0x043B,0x043C,0x043D,0x043E,0x043F,
                         0x0440,0x0441,0x0442,0x0443,0x0444,0x0445,0x0446,0x0447,0x0448,0x0449,0x044A,0x044B,0x044C,0x044D,0x044E,0x044F};

    return map[c-0x80];
}

// ---------------------------------------------------------------------
unsigned int _codepage::unicode_to_win1251(unsigned int c)
{
    if (c<0x80) return c;

    switch(c)
    {
        case 0x0402: return 0x80;
        case 0x0403: return 0x81;
        case 0x201A: return 0x82;
        case 0x0453: return 0x83;
        case 0x201E: return 0x84;
        case 0x2026: return 0x85;
        case 0x2020: return 0x86;
        case 0x2021: return 0x87;
        case 0x20AC: return 0x88;
        case 0x2030: return 0x89;
        case 0x0409: return 0x8A;
        case 0x2039: return 0x8B;
        case 0x040A: return 0x8C;
        case 0x040C: return 0x8D;
        case 0x040B: return 0x8E;
        case 0x040F: return 0x8F;

        case 0x0452: return 0x90;
        case 0x2018: return 0x91;
        case 0x2019: return 0x92;
        case 0x201C: return 0x93;
        case 0x201D: return 0x94;
        case 0x2022: return 0x95;
        case 0x2013: return 0x96;
        case 0x2014: return 0x97;
        //case 0x0: return 0x98;
        case 0x2122: return 0x99;
        case 0x0459: return 0x9A;
        case 0x203A: return 0x9B;
        case 0x045A: return 0x9C;
        case 0x045C: return 0x9D;
        case 0x045B: return 0x9E;
        case 0x045F: return 0x9F;

        case 0x00A0: return 0xA0;
        case 0x040E: return 0xA1;
        case 0x045E: return 0xA2;
        case 0x0408: return 0xA3;
        case 0x00A4: return 0xA4;
        case 0x0490: return 0xA5;
        case 0x00A6: return 0xA6;
        case 0x00A7: return 0xA7;
        case 0x0401: return 0xA8;
        case 0x00A9: return 0xA9;
        case 0x0404: return 0xAA;
        case 0x00AB: return 0xAB;
        case 0x00AC: return 0xAC;
        case 0x00AD: return 0xAD;
        case 0x00AE: return 0xAE;
        case 0x0407: return 0xAF;

        case 0x00B0: return 0xB0;
        case 0x00B1: return 0xB1;
        case 0x0406: return 0xB2;
        case 0x0456: return 0xB3;
        case 0x0491: return 0xB4;
        case 0x00B5: return 0xB5;
        case 0x00B6: return 0xB6;
        case 0x00B7: return 0xB7;
        case 0x0451: return 0xB8;
        case 0x2116: return 0xB9;
        case 0x0454: return 0xBA;
        case 0x00BB: return 0xBB;
        case 0x0458: return 0xBC;
        case 0x0405: return 0xBD;
        case 0x0455: return 0xBE;
        case 0x0457: return 0xBF;

        case 0x0410: return 0xC0;
        case 0x0411: return 0xC1;
        case 0x0412: return 0xC2;
        case 0x0413: return 0xC3;
        case 0x0414: return 0xC4;
        case 0x0415: return 0xC5;
        case 0x0416: return 0xC6;
        case 0x0417: return 0xC7;
        case 0x0418: return 0xC8;
        case 0x0419: return 0xC9;
        case 0x041A: return 0xCA;
        case 0x041B: return 0xCB;
        case 0x041C: return 0xCC;
        case 0x041D: return 0xCD;
        case 0x041E: return 0xCE;
        case 0x041F: return 0xCF;

        case 0x0420: return 0xD0;
        case 0x0421: return 0xD1;
        case 0x0422: return 0xD2;
        case 0x0423: return 0xD3;
        case 0x0424: return 0xD4;
        case 0x0425: return 0xD5;
        case 0x0426: return 0xD6;
        case 0x0427: return 0xD7;
        case 0x0428: return 0xD8;
        case 0x0429: return 0xD9;
        case 0x042A: return 0xDA;
        case 0x042B: return 0xDB;
        case 0x042C: return 0xDC;
        case 0x042D: return 0xDD;
        case 0x042E: return 0xDE;
        case 0x042F: return 0xDF;

        case 0x0430: return 0xE0;
        case 0x0431: return 0xE1;
        case 0x0432: return 0xE2;
        case 0x0433: return 0xE3;
        case 0x0434: return 0xE4;
        case 0x0435: return 0xE5;
        case 0x0436: return 0xE6;
        case 0x0437: return 0xE7;
        case 0x0438: return 0xE8;
        case 0x0439: return 0xE9;
        case 0x043A: return 0xEA;
        case 0x043B: return 0xEB;
        case 0x043C: return 0xEC;
        case 0x043D: return 0xED;
        case 0x043E: return 0xEE;
        case 0x043F: return 0xEF;

        case 0x0440: return 0xF0;
        case 0x0441: return 0xF1;
        case 0x0442: return 0xF2;
        case 0x0443: return 0xF3;
        case 0x0444: return 0xF4;
        case 0x0445: return 0xF5;
        case 0x0446: return 0xF6;
        case 0x0447: return 0xF7;
        case 0x0448: return 0xF8;
        case 0x0449: return 0xF9;
        case 0x044A: return 0xFA;
        case 0x044B: return 0xFB;
        case 0x044C: return 0xFC;
        case 0x044D: return 0xFD;
        case 0x044E: return 0xFE;
        case 0x044F: return 0xFF;

        default: return 0;
    }
}

// ---------------------------------------------------------------
void _codepage::utf8_to_win1251(std::string &text)
{
    std::string tmp=text;
    unsigned int i;
    unsigned int j;

    for(i=0,j=0;i<text.length();i++,j++)
    {
        unsigned char c=text[i];
        if (c <= 0x7F) tmp[j]=text[i];
        else if (c <= 0xDF)
        {
            int v=((text[i] & 0x1F) << 6) | (text[i+1] & 0x3F);
            i++;
            tmp[j]=unicode_to_win1251(v);
        }
        else if ( c <= 0xEF)
        {
            int v=(((text[i] & 0x0F) << 5) | ((text[i+1] & 0x3F) << 2) << 2) | (text[i+2] & 0x3F);
            i+=2;
            tmp[j]=unicode_to_win1251(v);
        }
        else if ( c <= 0xF7)
        {
            int v=((((text[i] & 0x0F) << 4) | ((text[i+1] & 0x3F) << 2) << 2) | (text[i+2] & 0x3F) << 2) | (text[i+3] & 0x3F);
            i+=3;
            tmp[j]=unicode_to_win1251(v);
        }
    }

    text.assign(tmp,0,j);
}

// ---------------------------------------------------------------
int _codepage::utf8_chars_len(const std::string &text)
{
    unsigned int i;
    unsigned int j;

    for(i=0,j=0;i<text.length();i++,j++)
    {
        unsigned char c=text[i];

        if (c <= 0x7F)          ;
        else if (c <= 0xDF)  i++;
        else if ( c <= 0xEF) i+=2;
        else if ( c <= 0xF7) i+=3;
    }

    return j;
}

// ---------------------------------------------------------------
void _codepage::utf8_chars_resize(std::string &text,int chars_len)
{
    unsigned int i;
    unsigned int j;

    for(i=0,j=0;i<text.length(),j<chars_len;i++,j++)
    {
        unsigned char c=text[i];

        if (c <= 0x7F)          ;
        else if (c <= 0xDF)  i++;
        else if ( c <= 0xEF) i+=2;
        else if ( c <= 0xF7) i+=3;
    }

    if (i!=text.length()) text.resize(i);
}

// ---------------------------------------------------------------
void _codepage::win1251_to_utf8(std::string &text)
{
    std::string rez;

    unsigned int i;
    for(i=0;i<text.length();i++)
    {
        unsigned char c=text[i];
        if (c<0x80) rez+=text[i];
        else
        {
            unsigned int u = win1251_to_unicode(c);
            if (u>=0x80 && u<0x7FF)
            {
                c=(0xC0 | (u >> 6));
                rez+=c;
                c=(0x80 | (u & 0x3F));
                rez+=c;
            }
            else if (u>=0x800 && u<0xFFFF)
            {
                c=(0xE0 | (u >> 12));
                rez+=c;
                c=(0x80 | ((u >> 6) & 0x3F));
                rez+=c;
                c=(0x80 | (u & 0x3F));
                rez+=c;
            }
        }
    }

    text=rez;
}

// ----------------------------------------------------
unsigned char _codepage::cc_code(unsigned char c)
{
    switch(c)
    {
        case '0': return 0;
        case '1': return 1;
        case '2': return 2;
        case '3': return 3;
        case '4': return 4;
        case '5': return 5;
        case '6': return 6;
        case '7': return 7;
        case '8': return 8;
        case '9': return 9;
        case 'A': return 10;
        case 'B': return 11;
        case 'C': return 12;
        case 'D': return 13;
        case 'E': return 14;
        case 'F': return 15;
    }

    return 0;
}

// ---------------------------------------------------------------
void _codepage::from_url_params_format(std::string &s)
{
    std::string r;
    int l=s.length();
    int i=0;
    for(;i<l;i++)
    {
		if ((unsigned char)(s[i])=='%')
        {
            if (i+1<l)
            {
                if ((unsigned char)(s[i+1])=='%')
                {
                    r+='%';
                    i++;
                }
                else
                {
                    if (i+2<l)
                    {
                        r+=(unsigned char)((cc_code(s[i+1])<<4)+cc_code(s[i+2]));
                        i+=2;
                    }
                }
            }
        }
        else if ((unsigned char)(s[i])=='+')
        {
            r+=' ';
        }
        else
        {
            r+=(unsigned char)(s[i]);
        }
    }

    s=r;
}

const char *to_url_params_format_map[256]=
{
  "%00", //
  "%01", // 
  "%02", // 
  "%03", // 
  "%04", // 
  "%05", // 
  "%06", // 
  "%07", // 
  "%08", // 
  "%09", //
  "%0A", //
  "%0B", // 
  "%0C", // 
  "%0D", //
  "%0E", // 
  "%0F", // 
  "%10", // 
  "%11", // 
  "%12", // 
  "%13", // 
  "%14", // 
  "%15", // 
  "%16", // 
  "%17", // 
  "%18", // 
  "%19", // 
  "%1A", // 
  "%1B", // 
  "%1C", // 
  "%1D", // 
  "%1E", // 
  "%1F", // 
  "%20", // SPACE
  "%21", // !
  "%22", // "
  "%23", // #
  "%24", // $
  "%25", // %
  "%26", // &
  "%27", // '
  "%28", // (
  "%29", // )
  "%2A", // *
  "%2B", // +
  "%2C", // ,
  "-", // -
  ".", // .
  "%2F", // /
  "0", // 0
  "1", // 1
  "2", // 2
  "3", // 3
  "4", // 4
  "5", // 5
  "6", // 6
  "7", // 7
  "8", // 8
  "9", // 9  "%3A", // :  "%3B", // ;  "%3C", // <"%3D", // ="%3E", // >"%3F", // ?"%40", // @"A", // A"B", // B"C", // C"D", // D"E", // E"F", // F"G", // G"H", // H"I", // I"J", // J"K", // K"L", // L"M", // M"N", // N"O", // O"P", // P"Q", // Q"R", // R"S", // S"T", // T"U", // U"V", // V"W", // W"X", // X"Y", // Y"Z", // Z"%5B", // ["%5C", // \""%5D", // ]"%5E", // ^"_", // _"%60", // `"a", // a"b", // b"c", // c"d", // d"e", // e"f", // f"g", // g"h", // h"i", // i"j", // j"k", // k"l", // l"m", // m"n", // n"o", // o"p", // p"q", // q"r", // r"s", // s"t", // t"u", // u"v", // v"w", // w"x", // x"y", // y"z", // z"%7B", // {"%7C", // |"%7D", // }"~", // ~"%7F", // "%80", // Ђ"%81", // Ѓ"%82", // ‚"%83", // ѓ"%84", // „"%85", // …"%86", // †"%87", // ‡"%88", // €"%89", // ‰"%8A", // Љ"%8B", // ‹"%8C", // Њ"%8D", // Ќ"%8E", // Ћ"%8F", // Џ"%90", // ђ"%91", // ‘"%92", // ’"%93", // “"%94", // ”"%95", // •"%96", // –"%97", // —"%98", // "%99", // ™"%9A", // љ"%9B", // ›"%9C", // њ"%9D", // ќ"%9E", // ћ"%9F", // џ"%A0", //  "%A1", // Ў"%A2", // ў"%A3", // Ј"%A4", // ¤"%A5", // Ґ"%A6", // ¦"%A7", // §"%A8", // Ё"%A9", // ©"%AA", // Є"%AB", // «"%AC", // ¬"%AD", // ­"%AE", // ®"%AF", // Ї"%B0", // °"%B1", // ±"%B2", // І"%B3", // і"%B4", // ґ"%B5", // µ"%B6", // ¶"%B7", // ·"%B8", // ё"%B9", // №"%BA", // є"%BB", // »"%BC", // ј"%BD", // Ѕ"%BE", // ѕ"%BF", // ї"%C0", // А"%C1", // Б"%C2", // В"%C3", // Г"%C4", // Д"%C5", // Е"%C6", // Ж"%C7", // З"%C8", // И"%C9", // Й"%CA", // К"%CB", // Л"%CC", // М"%CD", // Н"%CE", // О"%CF", // П"%D0", // Р"%D1", // С"%D2", // Т"%D3", // У"%D4", // Ф"%D5", // Х"%D6", // Ц"%D7", // Ч"%D8", // Ш"%D9", // Щ"%DA", // Ъ"%DB", // Ы"%DC", // Ь"%DD", // Э"%DE", // Ю"%DF", // Я"%E0", // а"%E1", // б"%E2", // в"%E3", // г"%E4", // д"%E5", // е"%E6", // ж"%E7", // з"%E8", // и"%E9", // й"%EA", // к"%EB", // л"%EC", // м"%ED", // н"%EE", // о"%EF", // п"%F0", // р"%F1", // с"%F2", // т"%F3", // у"%F4", // ф"%F5", // х"%F6", // ц"%F7", // ч"%F8", // ш"%F9", // щ"%FA", // ъ"%FB", // ы"%FC", // ь"%FD", // э"%FE", // ю"%FF" //
};

#include <iostream>

// ---------------------------------------------------------------
void _codepage::to_url_params_format(std::string &s)
{
    std::string r;
    int l=s.length();
    int i=0;
    for(;i<l;i++)
    {
        //unsigned char c=s[i];
        r+=to_url_params_format_map[(unsigned char)s[i]];
    }

    s=r;
}

// ---------------------------------------------------------------
bool _codepage::is_utf8(const std::string &s)
{
    int l=s.length();
    int i=0;

    for(;i<l;i++)
    {
		int c=s[i];
        if ((c | 0x7F) == 0x7F) continue;

        if ((c >> 5) == 0x06)
        {
            if (i+1>=l) return false;
			int c2=s[i+1];
            if ((c2 >> 6) != 0x02) return false;
            i++;
        }

        if ((c >> 4) == 0x0E)
        {
            if (i+2>=l) return false;
			int c2=s[i+1];
			int c3=s[i+2];
            if (((c2 >> 6) != 0x02) || ((c3 >> 6) != 0x02)) return false;
            i+=2;
        }

        if ((c >> 3) == 0x1E)
        {
            if (i+3>=l) return false;
			int c2=s[i+1];
			int c3=s[i+2];
			int c4=s[i+3];
            if (((c2 >> 6) != 0x02) || ((c3 >> 6) != 0x02) || ((c4 >> 6) != 0x02)) return false;
            i+=3;
        }
    }

    return true;
}

// ---------------------------------------------------------------
std::string _codepage::string_to_hex_string(const std::string &s)
{
    std::string rez;

    int l=s.length();
    int i=0;

    while(i<l)
    {
        char tmp[4];
        unsigned int v=(unsigned char)s[i];
        sprintf(tmp,"%02x",v);
        rez.append(tmp,2);
        i++;
    }

    return rez;
}
