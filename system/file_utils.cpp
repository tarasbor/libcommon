#include "file_utils.hpp"
//#include <windows.h>
//#include <winnetwk.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#ifdef WINDOWS
    #define SLASH_CHAR  '\\'
#else
    #define SLASH_CHAR  '/'
#endif // WIN32

#include <iostream>

/*
int fu_remove_files(const std::string &mask)
{
    WIN32_FIND_DATA fd;
    HANDLE h=FindFirstFile(mask.c_str(),&fd);
    if (h==INVALID_HANDLE_VALUE) return 0;

    int cnt=1;

    std::string::size_type pos=mask.rfind('\\');
    if (pos==std::string::npos) pos=mask.size()-1;

    std::string dir(mask,0,pos+1);

    while(1)
    {
        if ((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != FILE_ATTRIBUTE_DIRECTORY)
        {
            std::string full_name=dir+fd.cFileName;
            DeleteFile(full_name.c_str());
        }
        if (!FindNextFile(h,&fd)) break;
    }

    FindClose(h);

    return cnt;
}
*/

// --------------------------------------------------------
bool fu_is_file_exist(const std::string &file_name)
{
    struct stat s;
    if (stat(file_name.c_str(),&s)) return false;
    return true;
}

// --------------------------------------------------------
int fu_create_path(const std::string &path)
{
    std::string s = path;
    if (s[s.length()-1]!=SLASH_CHAR) s+=SLASH_CHAR;

    std::string::size_type pos=1;

    #ifdef WINDOWS
    if (path[1]==':') pos=3;
    #endif

    while((pos=s.find(SLASH_CHAR,pos))!=std::string::npos)
    {
        #ifdef WINDOWS
        int err=mkdir(std::string(s,0,pos).c_str());
        #else
        int err=mkdir(std::string(s,0,pos).c_str(),S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
        #endif

        if (err)
        {
            if (errno!=EEXIST) return errno;
        }

        pos++;
    }

    return 0;
}

// --------------------------------------------------------
// \return
//      0 - if directory not exist
//      1 - if exist
int dirExists(const char *path)
{
    struct stat info;

    if(stat( path, &info ) != 0)
        return 0;
    else if(info.st_mode & S_IFDIR)
        return 1;
    else
        return 0;
}
