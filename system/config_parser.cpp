#include <config_parser.hpp>
#include <text_help.hpp>
#include <list>
#include <fstream>
#include <error.hpp>
#include <iostream>

using namespace std;

// -----------------------------------------------------------------
void _config_parser::from_text(const string &text)
{
	cm_params_map.clear();

	list<string> lines;
	_text_help::split_text_by_endl(text,lines);

	list<string>::iterator lit=lines.begin();

	while(lit!=lines.end())
    {
        if ((*lit).empty())
        {
            lit++;
			continue;
        }

		if ((*lit)[0]=='#')
		{
			lit++;
			continue;
		}

				
        string::size_type pos=lit->find('=');
        if (pos==string::npos)
		{
			lit++;
			continue;
		}

		cm_params_map[string((*lit),0,pos)]=string((*lit),pos+1,lit->length()-1);

		lit++;
    }

	parse_done();
}

// -----------------------------------------------------------------
void _config_parser_from_file::load(const string file_name)
{
	ifstream ifile;
	ifile.open(file_name.c_str(),ios::in | ios::binary);
    if (!ifile) {
        ifile.close();
        throw _error(0,string("_config_parser_from_file::load - can not open config file ")+file_name);
    }

	ifile.seekg(0,ios::end);
	int size=ifile.tellg();
	ifile.seekg(0,ios::beg);

	char *buffer = new char [size];
	ifile.read(buffer,size);

    string text=string(buffer,size);
    delete [] buffer;

	ifile.close();

	from_text(text);
}

