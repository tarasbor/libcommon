#ifndef __RDG_PRINTER_H__
#define __RDG_PRINTER_H__

#include <windows.h>
#include <string>
#include <imglib.hpp>

using namespace std;

// ---------------------------------------------------------
// ---------------------------------------------------------
struct _printer_info
{
    string printer_name;
    string driver_name;
};

// ---------------------------------------------------------
// ---------------------------------------------------------
class _printers_enum
{
    private:

        _printer_info *info_array;
        DWORD printers_count;

    public:

        _printers_enum();
        ~_printers_enum()
        {
            if (info_array) delete [] info_array;
            info_array = NULL;
        };

        class iterator
        {
            friend class _printers_enum;

            private:

                _printers_enum *en;
                int index;

                iterator(_printers_enum *e,int i):index(i),en(e) {};

            public:

                iterator(const iterator &i):index(i.index),en(i.en) {};
                bool operator==(const iterator &i) const { return (index==i.index && en==i.en); };

                void operator++(int) { if (index<en->printers_count) index++; };

                _printer_info *operator->() { return &(en->info_array[index]); };
                inline operator _printer_info*() { return &(en->info_array[index]); };
        };

        iterator begin() { return iterator(this,0); };
        iterator end() { return iterator(this,printers_count); };

};

// --------------------------------------------------------
// --------------------------------------------------------
class _printer
{
    private:

        bool found;
        _printer_info info;

    public:

        //_printer();
        _printer(int index);
        _printer(const char *);

        bool founded() const { return found; };

        void print(_imglib::_image *img);
        void print(const unsigned char *rgba,int w,int h);
};

#endif
