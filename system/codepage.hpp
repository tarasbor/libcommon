#ifndef __CODEPAGE_FUNCTIONS__
#define __CODEPAGE_FUNCTIONS__

#include <string>

namespace _codepage
{
  // Transform one unicode simbol to win1251 codepage
  unsigned int unicode_to_win1251(unsigned int c);

  // Transform one simbol from win1251 codepage to unicode
  unsigned int win1251_to_unicode(unsigned int c);

  // Decode given text from UTF-8 format to ASCII (win-1251 codepage)
  void utf8_to_win1251(std::string &text);

  // Return len in chars (not in bytes) of string in UTF-8 format
  int utf8_chars_len(const std::string &text);

  // Resize string to CHARS_LEN chars (not bytes)
  void utf8_chars_resize(std::string &text,int chars_len);


  // Decode given text from ASCII (win-1251 codepage) to UTF-8 format
  void win1251_to_utf8(std::string &text);

  // return value of one hex char digit ( example 'A' -> 10 ) or 0
  unsigned char cc_code(unsigned char c);

  // raplace all hex simbols market as %XY in string to char (example %33%34%35%% -> 012%) and so on... see URL string format
  void from_url_params_format(std::string &s);

  // raplace all non standart & reserved chars to hex simbols market as %XY  see URL string format (https://en.wikipedia.org/wiki/Percent-encoding)
  void to_url_params_format(std::string &s);


  // check - is string is valid UTF8 string
  bool is_utf8(const std::string &s);

  // return string maded from s string. Each byte in s strign replace to 2-digit hex code
  // example s="12XYZ" -> "313258595A" (31 is hex code for 1? 31 - for 2 & so on)
  std::string string_to_hex_string(const std::string &s);

}


#endif
