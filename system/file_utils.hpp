#ifndef __FILE_UTILS_H__
#define __FILE_UTILS_H__

#include <string>

//int fu_remove_files(const std::string &mask);

bool fu_is_file_exist(const std::string &file_name);

int fu_create_path(const std::string &path);

// \return
//      0 - if directory not exist
//      1 - if exist
int dirExists(const char *path);


#endif
