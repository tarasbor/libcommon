#ifndef __SYSTEM_ENVINTORMENT__UTILS__
#define __SYSTEM_ENVINTORMENT__UTILS__

#include <string>

#ifdef WINDOWS
	#include <windows.h>
#else
	#define HINSTANCE int
    #include <unistd.h>
#endif

std::string se_get_exe_path(HINSTANCE app_inst);

long system_get_uptime();

#endif

