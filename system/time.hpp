#ifndef __CYBERVIEW_TIME_H__
#define __CYBERVIEW_TIME_H__

#include <ctime>

/*! \defgroup sys_time Время и дата
    \ingroup sys
    \brief Работа с датой, временем и штампами времени.
    @{
*/

/*! \defgroup sys_time_build_opts Опции компиляции
    \ingroup sys_time
    @{

    TIME_NO_STL - Если определен, то при компиляции не используется STL класс std::string (и нет части функций, использующих std::string в качестве параметра или результата)
*/

#ifndef TIME_NO_STL
#include <string>
#endif
/*!
 @}
*/

// some usefull constants
#ifdef WATCOM
    // how much 100ns in X (us,ms,s,m,h & one day)
    const __int64  time_1us_100ns =           10;
    const __int64  time_1ms_100ns =        10000;
    const __int64   time_1s_100ns =     10000000;
    const __int64   time_1m_100ns =    600000000;
    const __int64   time_1h_100ns =  36000000000;
    const __int64   time_1d_100ns = 864000000000;

    // difference between WINDOWS epoch (UTC 1980.01.01 00:00:00) & UNIX epoch (UTC 1970.01.01 00:00:00)
    const __int64   WU_DIFF = 116444736000000000;
#else
    // how much 100ns in X (us,ms,s,m,h & one day)
    const long long  time_1us_100ns =           10;
    const long long  time_1ms_100ns =        10000;
    const long long   time_1s_100ns =     10000000;
    const long long   time_1m_100ns =    600000000;
    const long long   time_1h_100ns =  36000000000;
    const long long   time_1d_100ns = 864000000000;

    // difference between WINDOWS epoch (UTC 1601.01.01 00:00:00) & UNIX epoch (UTC 1970.01.01 00:00:00)
    const long long   WU_DIFF = 116444736000000000;
#endif

/// Hardcode not defined time stamp (1900.01.01 00:00:00.000)
#define  NOTDEFINED_TIMESTAMP   116444628000000000


/*!
 \class _time
 \brief Класс для работы с датой, временем и временными штампами.

 Данный класс фактически является реализайией кроссплатформенного штампа времени.
 \nДетализация времени может осуществляться вплоть до:
 \n- Linux - сотен наносекунд
 \n- Windows - милисекунд
 \n
 \nШтампы времени полученные в Windows и Linux на одно и то же время равны между собой - это делает возможным:
 \n- использование класса во взаимодействии программ, работающих под Windoiws и Linux без дополнительных преобразований;
 \n- писать единый код для кроссплатформенных приложений.
 \n
 \nДля корректной работы класса на компьютере должны быть правильно настроены временные зоны.
 Внутренее значение штампа времени храниться относительно начального момента отсчета. Моментом отсчета считается 01 января 1601 года, 0:00:00 UTC+0.
 \note Время храниться относительно UTC.

 Функции без префикса utc_ "работают" с ЛОКАЛЬНЫМ временем, с учетом базы временных зон. Функции с префиксом utc_ "работают" с UTC временем.
*/
class _time
{
    public:

      #ifdef WATCOM
        typedef __int64 time_val_i;
      #else
        typedef long long time_val_i;   ///< Тип для внутренего представление штампа времени.
      #endif

    private:
        // raw datetime stamp
        time_val_i time_val;    ///< Внетреннее представление времени (штамп в сотнях наносекунд начиная с 01.01.1601 0:00:00.000)
                // In Linux Fedora23 minimal value is `100000000000000000` according to `1917.11.21 21:17:59.0`

    public:
        // splited view of datetime stamp
        /*!
        \struct _ys
        \brief Структура с полями-реквизитами того или иного момента времени
        */
        struct _ys
        {
            int y;      ///< год
            int m;      ///< номер месяца (1-12)
            int d;      ///< номер дня в месяце
            int h;      ///< час (0-23)
            int n;      ///< минуты (0-59)
            int s;      ///< секунды (0-59)
            int ms;     ///< милисекунды (0-999)
        };

        /*!
        \brief Конструктор по умолчанию.
        Штамп времени инициализируется нулем (дата и время будут равны 01.01.1601 0:00:00.000)
        */
        _time();

        /*!
        \brief Конструктор с устоановкой даты и времени.
        \param [in] y Год
        \param [in] m Месяц (1 - Январь, 12 - Декабрь)
        \param [in] d День (номер дня в месяце)
        \param [in] h Час (0-23)
        \param [in] n Минута (0-59)
        \param [in] s Секунда (0-59)
        \param [in] ms Милисекунда (0-999)
        */
        _time(int y,int m,int d,int h,int n,int s,int ms);

        /// Конструктор копии
        _time(const time_val_i &st);

        /// Деструктор
        ~_time() {}

        // tick (+ towards, -backwards) datetime stamp
        // param is in 100ns, microseconds (us), miliseconds (ms), seconds (s), minute (m), hour (h) or day (d))
        /*!
        \brief Увеличить или уменьшить штамп времени на заданное количество сотен НАНОсекунд
        \param [in] hns перевести время назад (hns<0) или вперед (hns>0). hns задается в сотнях наносекунд.
        */
        inline void tick_100ns(time_val_i hns) { time_val+=hns; }

        /*!
        \brief Увеличить или уменьшить штамп времени на заданное количество МИКРОсекунд
        \param [in] usec перевести время назад (usec<0) или вперед (usec>0). usec задается в микросекундах.
        */
        inline void tick_us(time_val_i usec)   { time_val+=usec*time_1us_100ns; }

        /*!
        \brief Увеличить или уменьшить штамп времени на заданное количество МИЛИсекунд
        \param [in] msec перевести время назад (msec<0) или вперед (msec>0). msec задается в милисекундах.
        */
        inline void tick_ms(time_val_i msec)   { time_val+=msec*time_1ms_100ns; }

        /*!
        \brief Увеличить или уменьшить штамп времени на заданное количество секунд
        \param [in] sec перевести время назад (sec<0) или вперед (sec>0). sec задается в секундах.
        */
        inline void tick_s(time_val_i s)       { time_val+=s   *time_1s_100ns; }

        /*!
        \brief Увеличить или уменьшить штамп времени на заданное количество минут
        \param [in] m перевести время назад (m<0) или вперед (m>0). m задается в минутах.
        */
        inline void tick_m(time_val_i m)       { time_val+=m   *time_1m_100ns; }

        /*!
        \brief Увеличить или уменьшить штамп времени на заданное количество часов
        \param [in] h перевести время назад (h<0) или вперед (h>0). h задается в часах.
        */
        inline void tick_h(time_val_i h)       { time_val+=h   *time_1h_100ns; }

        /*!
        \brief Увеличить или уменьшить штамп времени на заданное количество дней
        \param [in] d перевести время назад (d<0) или вперед (d>0). d задается в днях.
        */
        inline void tick_d(time_val_i d)       { time_val+=d   *time_1d_100ns; }

        // obsolete (use tick_ms direct call) - only for old code
        /*!
        \brief Увеличить или уменьшить штамп времени
        \param [in] msec перевести время назад (msec<0) или вперед (msec>0). msec задается в милисекундах
        \deprecated
        */
        inline void tick(time_val_i msec) { tick_ms(msec); }


        /*!
        \brief Получить год из штампа
        \return Год, соответсвующий штампу. Расчитывается с переводом на ЛОКАЛЬНОЕ время с учетом настроек временной зоны на момент штампа.
        */
        int year() const;

        /*!
        \brief Получить месяц из штампа
        \return Месяц, соответсвующий штампу. Расчитывается с переводом на ЛОКАЛЬНОЕ время с учетом настроек временной зоны на момент штампа.
        \note Январь - 1 месяц, Декабрь - 12.
        */
        int month() const;

        /*!
        \brief Получить день месяца из штампа
        \return День месяца, соответсвующий штампу (1-31). Расчитывается с переводом на ЛОКАЛЬНОЕ время с учетом настроек временной зоны на момент штампа.
        */
        int day() const;

        /*!
        \brief Получить день недели
        \return День недели, соответсвующий штампу. Расчитывается с переводом на ЛОКАЛЬНОЕ время с учетом настроек временной зоны на момент штампа.
        \note Воскресенье - 0, Понедельник - 1,...,Суббота - 6.
        */
        int day_of_week() const;

        /*!
        \brief Получить час из штампа
        \return Час, соответсвующий штампу (0-23). Расчитывается с переводом на ЛОКАЛЬНОЕ время с учетом настроек временной зоны на момент штампа.
        */
        int hour() const;

        /*!
        \brief Получить минуты из штампа
        \return Минуты, соответсвующие штампу (0-59). Расчитывается с переводом на ЛОКАЛЬНОЕ время с учетом настроек временной зоны на момент штампа.
        */
        int minute() const;

        /*!
        \brief Получить секунды из штампа
        \return Секунды, соответсвующий штампу (0-59). Расчитывается с переводом на ЛОКАЛЬНОЕ время с учетом настроек временной зоны на момент штампа.
        */
        int second() const;

        /*!
        \brief Получить милисекунды из штампа
        \return Милисекунды, соответсвующий штампу (0-999). Расчитывается с переводом на ЛОКАЛЬНОЕ время с учетом настроек временной зоны на момент штампа.
        */
        int msecond() const;


        /*!
        \brief Перевести (разбить) штамп в год, месяц, день, час, минуты, секунды и милисекунды за раз.
        \return Заполенная стуктура _ys. Перевод расчитывается на ЛОКАЛЬНОЕ время с учетом настроек временной зоны на момент штампа.
        \note Рекомендуется использовать при получении всех или нескольких реквизитов штампа вместо полсдевательного вызова нескольких
        функций получения тех или иных реквизитов (типа year(), hour(), и т.д.).
        */
        inline _ys split() const { _ys ys; split(ys); return ys; }

        /*!
        \brief Перевести (разбить) штамп в год, месяц, день, час, минуты, секунды и милисекунды за раз.
        \param [in,out] ys Стуктура _ys, в которую будет помещен результат.
        \nПеревод расчитывается на ЛОКАЛЬНОЕ время с учетом настроек временной зоны на момент штампа.
        \note Рекомендуется использовать при получении всех или нескольких реквизитов штампа вместо полсдевательного вызова нескольких
        функций получения тех или иных реквизитов (типа year(), hour(), и т.д.).
        */
        void split(_ys &ys) const;

        // set (local)
        void year(int s);
        void month(int s);
        void day(int s);
        void hour(int s);
        void minute(int s);
        void second(int s);
        void msecond(int s);

        // get (UTC)
        int utc_year() const;
        int utc_month() const;
        int utc_day() const;
        int utc_day_of_week() const;
        int utc_hour() const;
        int utc_minute() const;
        int utc_second() const;
        int utc_msecond() const;

        // split stamp to more humane relatiable view -))
        inline _ys utc_split() const { _ys ys; utc_split(ys); return ys; }
        // split stamp to more humane relatiable view (optimazed - no temporary return copy)
        void utc_split(_ys &ys) const;


        // convert stamp to string value
        char        *stamp_to_str(char *s) const;           // its return given s
#ifndef TIME_NO_STL
        std::string &stamp_to_str(std::string &s) const;    // its return given s
        std::string  stamp_to_str() const;                  // its return temporary copy of string
#endif

        // set stamp from string value
        void str_to_stamp(const char *s);
#ifndef TIME_NO_STL
        void str_to_stamp(const std::string &s);
#endif

        // get & set raw stamp value
        const time_val_i stamp() const { return time_val; }
        void stamp(time_val_i s) { time_val=s; }

        bool operator<(const _time &t) const;
        bool operator>(const _time &t) const;
        bool operator<=(const _time &t) const;
        bool operator>=(const _time &t) const;
        bool operator==(const _time &t) const;
        bool operator!=(const _time &t) const;

        // difference in milliseconds
        // remove this - it is not good semantic (difference return in what (s,ms, may be ns)?)
        // int operator-(const _time &t) const;

        // set datetime stamp to current (now) local computer clock
        void set_now();

        // Human readable  e.g. 2015.12.31 23:59:59.789
        std::string text() const;

        // Set to NOTDEFINED_TIMESTAMP, which equivalent to 1900.01.01 00:00:00.000
        void set_undefined();
        // Check whether equal to NOTDEFINED_TIMESTAMP (1900.01.01 00:00:00.000)
        bool undefined();
        // parse from format 2015.12.31 23:59:59.789
        bool try_parse(const std::string &str);


        // some usefull staff

        // difference between me and some stamp
        inline time_val_i diff_100ns(time_val_i d) const { return (time_val-d); }
        inline time_val_i diff_us   (time_val_i d) const { return (time_val-d)/time_1us_100ns; }
        inline time_val_i diff_ms   (time_val_i d) const { return (time_val-d)/time_1ms_100ns; }
        inline time_val_i diff_s    (time_val_i d) const { return (time_val-d)/time_1s_100ns; }
        inline time_val_i diff_m    (time_val_i d) const { return (time_val-d)/time_1m_100ns; }
        inline time_val_i diff_h    (time_val_i d) const { return (time_val-d)/time_1h_100ns; }
        inline time_val_i diff_d    (time_val_i d) const { return (time_val-d)/time_1d_100ns; }

        // difference between me and some _time
        inline time_val_i diff_100ns(const _time &t) const { return (time_val-t.time_val); }
        inline time_val_i diff_us   (const _time &t) const { return (time_val-t.time_val)/time_1us_100ns; }
        inline time_val_i diff_ms   (const _time &t) const { return (time_val-t.time_val)/time_1ms_100ns; }
        inline time_val_i diff_s    (const _time &t) const { return (time_val-t.time_val)/time_1s_100ns; }
        inline time_val_i diff_m    (const _time &t) const { return (time_val-t.time_val)/time_1m_100ns; }
        inline time_val_i diff_h    (const _time &t) const { return (time_val-t.time_val)/time_1h_100ns; }
        inline time_val_i diff_d    (const _time &t) const { return (time_val-t.time_val)/time_1d_100ns; }

        // return _time seted to begin date & time of *this year (month,day)
        inline _time year_begin() const { return _time(year(),1,1,0,0,0,0); }
        inline _time month_begin() const { _ys ys; split(ys); return _time(ys.y,ys.m,1,0,0,0,0); }
        inline _time day_begin() const { _ys ys; split(ys); return _time(ys.y,ys.m,ys.d,0,0,0,0); }

        // set *this to begin date & time of *this year (month,day)
        inline void to_year_begin()  { *this=year_begin(); }
        inline void to_month_begin() { *this=month_begin(); }
        inline void to_day_begin()   { *this=day_begin(); }

        // return _time seted to end date & time of *this year (month,day)
        inline _time year_end() const  { _time t(year()+1,1,1,0,0,0,0); t.tick_100ns(-1); return t; }
        inline _time month_end() const { _time t=month_begin(); t.tick_d(31+1); t.to_month_begin(); t.tick_100ns(-1); return t; }
        inline _time day_end() const   { _time t=day_begin(); t.tick_h(24+1); t.to_day_begin(); t.tick_100ns(-1); return t; }

        // set *this to end date & time of *this year (month,day)
        inline void to_year_end()  { *this=year_end(); }
        inline void to_month_end() { *this=month_end(); }
        inline void to_day_end()   { *this=day_end(); }

        // return _time in wich date = param date & time = *this time
        inline _time same_date(const _time &s) const { _time t=s.day_begin(); t.tick_100ns(diff_100ns(day_begin())); return t;} //{ _time t=s; t.tick_100ns(diff_100ns(day_begin())); return t; };
        // set *this date = param date
        inline void to_same_date(const _time &s) { *this=same_date(s); }

        // return _time in wich time = param time & date = *this date
        inline _time same_time(const _time &s) const { _time t=day_begin(); t.tick_100ns(s.diff_100ns(s.day_begin())); return t; }
        // set *this time = param time
        inline void to_same_time(const _time &s) { *this=same_time(s); }
};

/*!
 @}
*/

#endif
