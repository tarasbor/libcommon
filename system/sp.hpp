#ifndef __SMART_PTR_H__
#define __SMART_PTR_H__

// --------------------------------------------------
// (Simple) Smart Pointer for objects with release method
// --------------------------------------------------
template<typename T> class _sp_rel
{
    protected:
        T *cm_ptr;

        _sp_rel(const _sp_rel &o);
        _sp_rel& operator=(const _sp_rel &o);
    public:
        _sp_rel(T *p):cm_ptr(p) {};
        ~_sp_rel() { if (cm_ptr) cm_ptr->release(); };

        inline T *operator->() { return cm_ptr; };
        inline const T *operator->() const { return cm_ptr; };

        inline operator T *() { return cm_ptr; };

};

// --------------------------------------------------
// (Simple) Smart Pointer for objects with public destructor
// --------------------------------------------------
template<typename T> class _sp_del
{
    protected:
        T *cm_ptr;

        _sp_del(const _sp_del &o);
        _sp_del& operator=(const _sp_del &o);
    public:
        _sp_del(T *p):cm_ptr(p) {};
        ~_sp_del() { if (cm_ptr) delete cm_ptr; };

        inline T *operator->() { return cm_ptr; };
        inline const T *operator->() const { return cm_ptr; };

        inline operator T *() { return cm_ptr; };

};


#endif
