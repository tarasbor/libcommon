#ifndef __U_DEFS_H__
#define __U_DEFS_H__

#define DEL(X) { if(X) delete X; X=NULL; }
#define RELEASE(X) { if(X) (X)->release(); X=NULL; }

#ifdef WINDOWS
    #define PATH_SLASH  '\\'
#else
    #define PATH_SLASH  '/'
#endif

#endif
