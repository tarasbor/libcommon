#ifndef __SCANNER_CLASS_H__
#define __SCANNER_CLASS_H__

#include <windows.h>
#include <imglib.hpp>

class _scanner
{
    private:
    
        HINSTANCE _app_inst;
        HWND _main_wnd;
        
    public:

        _scanner(HINSTANCE app_inst,HWND main_wnd);
        ~_scanner();

        int scan(_imglib::_image *img);
};

#endif
