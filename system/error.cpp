#include <error.hpp>
#include <stdio.h>

const string _rdg_exception::description_header(const string& file, unsigned int line, const string& function)
{
    char cbuff[32];
    sprintf(cbuff, "%i", line);
    return file + ":" + cbuff + "\n" + function;
}

_rdg_exception::_rdg_exception(const char* file, unsigned int line, const char* function, const string& msg, _rdg_exception* prev_ex/* = NULL*/) :
    file(file), line(line), function(function),
    msg(msg)
{
    if(prev_ex)
    {
        prev = new _rdg_exception(*prev_ex);
        prev_ex->prev = NULL; //to protect against delete in destructor of prev_ex
    }
    else
        prev = NULL;
}

_rdg_exception::~_rdg_exception()
{
    if(prev)
        for(_rdg_exception* e = prev; e != NULL; delete e)
            e = e->prev;
}

const string _rdg_exception::description(void)
{
    return description_header(file, line, function) + "\n" + msg;
}

const string _rdg_exception::full_description(const char* file, unsigned int line, const char* function)
{
    string sbuff(description_header(file, line, function));
    for(_rdg_exception* e = this; e != NULL; e = e->prev)
    {
            sbuff += "\n------->\n";
            sbuff += e->description();
    }
    sbuff += "\n------->\n";
    return sbuff;
}
