#ifndef __INTERVAL_TEMPLATE_H__
#define __INTERVAL_TEMPLATE_H__

template<class T> class _interval
{
    private:

      T _begin;
      T _end;
       
    public:

      _interval():_begin(),_end() {};
      _interval(const T &b,const T &e):_begin(b),_end(e) {};
      _interval(const _interval<T> &i):_begin(i._begin),_end(i._end) {};

      _interval<T> &operator=(const _interval<T> &i)
      {
          _begin=i._begin;
          _end=i._end;
          return *this;
      };

      bool operator<(const _interval<T> &i) const { return _begin<i._begin; };
      bool operator==(const _interval<T> &i) const { return (_begin==i._begin && _end==i._end); };

      void begin(const T &v) { _begin=v; };
      void end(const T &v) { _end=v; };

      const T &begin() const { return _begin; };
      const T &end() const { return _end; };
	  T &begin() { return _begin; };
	  T &end() { return _end; };

      bool in(const T &v) const
      {
          if (v<_begin || v>_end) return false;
          return true;
      };
};

#endif
