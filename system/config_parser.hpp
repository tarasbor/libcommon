#ifndef __CONFIG_PARSER__
#define __CONFIG_PARSER__

#include <string>
#include <map>

using namespace std;

// ------------------------------------------------------------
class _config_parser
{
protected:
	typedef map<string,string> _params_map;
	_params_map cm_params_map;
	
	_config_parser() {};
	virtual ~_config_parser() {};
	
public:		
	virtual void from_text(const string &text);
	virtual void parse_done() const = 0;
	
};

// ------------------------------------------------------------
class _config_parser_from_file:public _config_parser
{
protected:		
	_config_parser_from_file() {};
	virtual ~_config_parser_from_file() {};
	
public:	
	virtual void load(const string file_name);
};


#endif