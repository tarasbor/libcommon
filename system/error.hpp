#ifndef __ERROR_EXCEPTION_H__
#define __ERROR_EXCEPTION_H__

#include <string>
#include <stdlib.h>

using namespace std;

// -----------------------------------------------------------------
class _error
{
    private:

      std::string s;
      int n;

    public:

      _error(int num,const std::string &str):s(str),n(num) {}
      const std::string &str() const { return s;}
      int num() const { return n; }

};

// -----------------------------------------------------------------

class _rdg_exception
{
protected:

    string file;
    unsigned int line;
    string function;

    int errnum;
    string msg;
    _rdg_exception* prev;

    const string& env(void);

    // starting string of error description
    static const string description_header(const string& file, unsigned int line, const string& function);

public:

    _rdg_exception(const char* file, unsigned int line, const char* function, const string& msg, _rdg_exception* prev_ex = NULL);
    virtual ~_rdg_exception();

    // returns one only description of the own error
    virtual const string description(void);

    // returns unfolded exception stack descriptions
    // to be used with GET_FULL_EXCEPTION_DESCRIPTION
    const string full_description(const char* file, unsigned int line, const char* function);
};

#define THROW_EXCEPTION(msg) throw _rdg_exception(__FILE__, __LINE__, __PRETTY_FUNCTION__, msg)
#define THROW_NEXT_EXCEPTION(msg, prev) throw _rdg_exception(__FILE__, __LINE__, __PRETTY_FUNCTION__, msg, prev)
#define GET_FULL_EXCEPTION_DESCRIPTION(e) e.full_description(__FILE__, __LINE__, __PRETTY_FUNCTION__)

#endif
