/***************************************************************************
 *   What is it: This file is part of CRC (Cyclic redundancy code) calculation lib.
 *   Content: lib functions header
 *   Version: 0.1b
 *   Last-Mod: 15.11.2009
 *
 *   Copyright (C) 2006 by Proshin Alexey
 *   paa713@mail.ru
 *
 *   The contents of this file are subject to free software;
     you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   The contents of this file is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***************************************************************************/
#ifndef __CRC_LIB__
#define __CRC_LIB__

#ifdef __cplusplus
  #include <string>
  using namespace std;
#endif

// ----------------------------------------------------------------------------
/*
  Name  : CRC-8
  Poly  : 0x31    x^8 + x^5 + x^4 + 1
  Init  : 0xFF
  Revert: false
  XorOut: 0x00
  Check : 0xF7 ("123456789")
  MaxLen: 15 ����(127 ���) - �����������
    ���������, �������, ������� � ���� �������� ������
*/
unsigned char crc_8(const unsigned char *d,unsigned char len);

// ----------------------------------------------------------------------------
/*
  Name  : CRC-16 (ANSI or IBM)
  Poly  : 0x8005    x^16 + x^15 + x^2 + 1
  Init  : 0xFFFF
  Revert: true
  XorOut: 0x0000
  Check : 0x4B37 ("123456789")
  MaxLen: 4095 ���� (32767 ���) - �����������
    ���������, �������, ������� � ���� �������� ������
*/
unsigned short crc_16_ansi(const unsigned char *d, unsigned short len);

// ----------------------------------------------------------------------------
/*
  Name  : CRC-16 CCITT
  Poly  : 0x1021    x^16 + x^12 + x^5 + 1
  Init  : 0xFFFF
  Revert: false
  XorOut: 0x0000
  Check : 0x29B1 ("123456789")
  MaxLen: 4095 ���� (32767 ���) - �����������
    ���������, �������, ������� � ���� �������� ������
*/
unsigned short crc_16_ccitt(const unsigned char *d, unsigned short len);


// ----------------------------------------------------------------------------
/*
  Name  : CRC-32 (IEEE)
  Poly  : 0x04C11DB7    x^32 + x^26 + x^23 + x^22 + x^16 + x^12 + x^11
                       + x^10 + x^8 + x^7 + x^5 + x^4 + x^2 + x + 1
  Init  : 0xFFFFFFFF
  Revert: true
  XorOut: 0xFFFFFFFF
  Check : 0xCBF43926 ("123456789")
  MaxLen: 268 435 455 ���� (2 147 483 647 ���) - �����������
   ���������, �������, �������� � ���� �������� ������
*/
unsigned int crc_32(const unsigned char *d,unsigned int len);

#ifdef __cplusplus
  unsigned char crc_8(const string &d);
  unsigned short crc_16_ansi(const string &d);
  unsigned short crc_16_ccitt(const string &d);
  unsigned int crc_32(const string &d);
#endif


#endif
