/***************************************************************************
                          htmlrep.cpp  -  description
                             -------------------
    begin                : Thu Dec 2 2004
    copyright            : (C) 2004 by Uukrul Telecom, Proshin Alexey
    email                : info@u-telecom.ru, microprosh@freemail.ru
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "../htmlrep.hpp"
#include <fstream>
#include <string.h>
#include <list>
#include <map>

struct _replace_dict_rec
{
     std::string from;
     std::string to;

     _replace_dict_rec():from(""),to("") {}

     _replace_dict_rec(const char *f,const char *t):from(f),to(t) {}
     _replace_dict_rec(const _replace_dict_rec &r):from(r.from),to(r.to) {}

     _replace_dict_rec &operator=(const _replace_dict_rec &r) { from=r.from; to=r.to; return *this; }
          
     bool operator==(const _replace_dict_rec &r) const { return from==r.from; }
     bool operator<(const _replace_dict_rec &r) const { return from<r.from; }
};

typedef std::list<_replace_dict_rec> _replace_dictonary;

_replace_dictonary create_html_rep_dict();
_replace_dictonary create_null_rep_dict();

typedef std::map<std::string, std::string> str_dict;

class _report_imp:public _report
{
    friend _report *create_html_report();
    friend _report *create_text_report();
    
    private:

      str_dict sections;
      str_dict vars;
      _replace_dictonary r_dict;

      void clearVars();
      void clear();

      int getline(std::ifstream &file,std::string &in);

      _report_imp(const _replace_dictonary &d):r_dict(d) { };
      virtual ~_report_imp() { clear(); };

    public:

      virtual int setTemplate(const char *fname);
      virtual int setTemplateText(const char *text);
      
      virtual int outSection(const char *sname,std::string &to);

      virtual void setVar(const char *name,const char *value);      
      virtual void setVar(const char *name,int value) { char t[32]; std::sprintf(t,"%i",value); setVarNoRep(name,t); };
      virtual void setVar(const char *name,mbint value)
      {
          char t[32];
          #ifdef WINDOWS
            std::sprintf(t,"%I64i",value);
          #else
            std::sprintf(t,"%Li",value);
          #endif
          setVarNoRep(name,t);
      };
      virtual void setVar(const char *name,float value,char del='.',int zdig=2)
      {
          char format[16];
          std::sprintf(format,"%%.%if",zdig);
          
          char t[32];
          std::sprintf(t,format,value);
          if (del!='.')
            for(char *s=t;*s!='\0';s++)
              if (*s=='.')
              {
                  *s=del;
                  break;
              }
          setVarNoRep(name,t);
      };
      virtual void setVar(const char *name,const std::string &value) { setVar(name,value.c_str()); };

      virtual void setVarNoRep(const char *name,const char *value);
      virtual void setVarNoRep(const char *name,int value)  { setVar(name,value); };
      virtual void setVarNoRep(const char *name,mbint value)  { setVar(name,value); };
      virtual void setVarNoRep(const char *name,float value,char del='.',int zdig=2) { setVar(name,value); };
      virtual void setVarNoRep(const char *name,const std::string &value) { setVarNoRep(name,value.c_str()); };
      

      virtual void release() { delete this; };
};

// --------------------------------------------------------------
_report *create_html_report() { return new _report_imp(create_html_rep_dict()); };
_report *create_text_report() { return new _report_imp(create_null_rep_dict()); };

// --------------------------------------------------------------
int _report_imp::getline(std::ifstream &file,std::string &in)
{
    char buff[1024];
    file.clear();
    file.getline(buff,1024);

    if (file.eof()) return 0;
    if (file.fail()) return 0;

    in=buff;
    
    return 1;
}

// --------------------------------------------------------------
int _report_imp::setTemplate(const char *fname)
{

  clear();

  std::ifstream file;

  file.clear();
  file.open(fname,std::ios::in | std::ios::binary);
  if (!file) return 1;

  file.seekg(0,std::ios::end);
  int size=file.tellg();
  file.seekg(0,std::ios::beg);
  char *tmp=new char [size+1];
  memset(tmp,0,size+1);
  file.read(tmp,size);
  file.close();

  int res=setTemplateText(tmp);

  delete tmp;

  return res;
}

// --------------------------------------------------------------
int _report_imp::setTemplateText(const char *text)
{

  clear();

  std::string sname="MAIN";
  std::string in="";
  std::string txt=text;

  size_t pos=0;
  size_t section_begin=0;
  while(1)
  {
      pos=txt.find("<!-- SECTION ",pos);
      if (pos==std::string::npos) break;

      if (section_begin!=pos)
      {
          in.assign(txt,section_begin,pos-section_begin);
          sections[sname]=in;
      }
      
      size_t pos_end=txt.find(" -->",pos);
      if (pos_end==std::string::npos) return 2;
      sname.assign(txt,pos+13,pos_end-pos-13);
          
      section_begin=pos_end+4;
          
      pos=section_begin+1;
  }

  in.assign(txt,section_begin,txt.length()-1-section_begin);
  sections[sname]=in;
  
  return 0;
}

// ---------------------------------------------------------
void _report_imp::clear()
{
  sections.clear();
  clearVars();
}

// -----------------------------------------------------------
int _report_imp::outSection(const char *sname,std::string &to)
{

  to="";

  std::string replace_end;

  str_dict::iterator it=sections.find(sname);
  if (it==sections.end()) return 1;
  
      to=(*it).second;

      size_t pos=0;
      while(1)
      {
          pos=to.find("<!-- VAR ",pos);
          if (pos==std::string::npos) break;

          replace_end="";

          size_t pos2=to.find(" -->",pos);
          if (pos2==std::string::npos)
          {
              pos2=to.find(" -#>",pos);              
              if (pos2==std::string::npos) break;
              replace_end=">";
          } else
          {
              size_t pos3=to.find(" -#>",pos);
              if (pos3!=std::string::npos && pos3<pos2)
              {
                  pos2=pos3;
                  replace_end=">";
              }
          }

          std::string vname(to.substr(pos+9,pos2-pos-9));

          str_dict::iterator vit=vars.find(vname);
          if (vit==vars.end())
              to.replace(pos,pos2+4-pos,replace_end);
          else
              to.replace(pos,pos2+4-pos,(*vit).second);

          //std::cout << "!" << to << endl;

      }

      return 0;
  
}

// ------------------------------------------------------
void _report_imp::clearVars()
{
  vars.clear();
}

// ------------------------------------------------------
void _report_imp::setVar(const char *name,const char *value)
{

  std::string val=value;
  
  _replace_dictonary::iterator dit=r_dict.begin();
  while(dit!=r_dict.end())
  {
      size_t pos=0;
      while(1)
      {
          pos=val.find((*dit).from,pos);
          if (pos==std::string::npos) break;
          val.replace(pos,(*dit).from.length(),(*dit).to);
          pos++;
      }
      dit++;
  }

  vars[name]=val;

}

// ------------------------------------------------------
void _report_imp::setVarNoRep(const char *name,const char *value)
{    

  vars[name]=value;
}

// ------------------------------------------------------
_replace_dictonary create_html_rep_dict()
{
    _replace_dictonary d;

    d.push_back(_replace_dict_rec("&","&amp;"));
    d.push_back(_replace_dict_rec("<","&lt;"));
    d.push_back(_replace_dict_rec(">","&gt;"));
    d.push_back(_replace_dict_rec("\r\n","<br>"));
    d.push_back(_replace_dict_rec("\n","<br>"));
    d.push_back(_replace_dict_rec("\"","&quot;"));
    //d.push_back(html_report::_replace_dict_rec(" ","&nbsp;"));
    
    return d;
}

// ------------------------------------------------------
_replace_dictonary create_null_rep_dict()
{
    _replace_dictonary d;
        
    return d;
}


