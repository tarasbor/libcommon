#include <iostream>
#include <stdio.h>
#include <clthread.hpp>
#include <map>
#include <list>

#include "base_db2.hpp"

#define OTL_DB2_CLI  // Compile OTL 4.0/DB2_CLI
#define OTL_STL 1

#ifdef WINDOWS
        #define OTL_UNCAUGHT_EXCEPTION_ON
        #define OTL_DESTRUCTORS_DO_NOT_THROW
        #define OTL_EXCEPTION_STM_TEXT_SIZE 255
        #define OTL_NO_TMPL_MEMBER_FUNC_SUPPORT
        #define OTL_BIGINT __int64
        #define MBINT      __int64
#else
        #define OTL_BIGINT long long
        #define MBINT      long long
#endif

#include <otlv4.h> // include the OTL 4.0 header file

class _tr_db2;
class _stat_db2;
class _db_db2;

// --------------------------------------
template<class T> void push_and_check(otl_stream *stream,T &val)
{
        try
        {
                (*stream) << val;
        }
        catch (const otl_exception& p)
        {
                throw _db_error(string((char *) p.msg) + " on " + p.stm_text);
        }
}

template<class T> void pop_and_check(otl_stream *stream,T &val)
{
        try
        {
                (*stream) >> val;
        }
        catch (const otl_exception& p)
        {
                throw _db_error(string((char *) p.msg) + " on " + p.stm_text);
        }

}

/*
// --------------------------------------
push_and_check_ll(otl_stream *stream,)
{
        try
        {
                (*stream) << val;
        }
        catch (const otl_exception& p)
        {
                throw _db_error(string((char *) p.msg) + " on " + p.stm_text);
        }
*/

// --------------------------------------
// --------------------------------------
// Statement
// --------------------------------------
// --------------------------------------
class _stat_db2: public _statement
{
private:

        otl_stream *stream;
        otl_connect *con;

        bool first_fetch;

public:

        _stat_db2(otl_connect *c);
        virtual ~_stat_db2();

        virtual void prepare(const string &sql);
        virtual void execute();
        virtual bool fetch();
        virtual void close(); // call SIGABRT if exception thrown(in stream->close()) WHEN called from DESTRUCTOR!!.
                              // To avoid it, call close() BEFORE ~_stat_db2() destroying!!!!!!!!

        virtual void start_push() {};
        virtual void push(int v) { push_and_check(stream, v); };
        virtual void push(MBINT v) { push_and_check(stream, v); };
        virtual void push(const _db_id &v) { push_and_check(stream, v.value()); };
        virtual void push(float v) { push_and_check(stream, v); };
        virtual void push(const string &v) { push_and_check(stream, v); } ;
        virtual void push(const char *v) { push_and_check(stream, v); };
        //virtual void push(const _blob_st &v) = 0;// { _transaction t(st->Transaction()); _blob b(t); b=v; st->Set(cur_set_f,b.handle()); cur_set_f++; };
        virtual void push(const _time &v,bool use_db_type=false) { _time::time_val_i t=v.stamp(); push_and_check(stream,t); };
        virtual void push(const _blob_st &v) { throw _db_error("DB2::statement::push - not implement yet."); };

        virtual void start_pop() { stream->skip_to_end_of_row(); };
        virtual bool pop(int *v) { pop_and_check(stream,(*v)); return true; };
        virtual bool pop(MBINT *v) { pop_and_check(stream,(*v)); return true; };
        virtual bool pop(_db_id &v) { pop_and_check(stream,v.value()); return true; };
        virtual bool pop(float *v) { pop_and_check(stream,(*v)); return true; };
        virtual bool pop(string &v) { pop_and_check(stream,v); return true; };
        virtual bool pop(char *v) { pop_and_check(stream,(*v)); return true; };
        virtual bool pop(_time &v,bool use_db_type=false) { _time::time_val_i t; pop_and_check(stream,t); v.stamp(t); return true; };
        virtual bool pop(_blob_st &v) { throw _db_error("DB2::statement::pop_blob - not implement yet."); };
        virtual bool pop_is_null() { throw _db_error("DB2::statement::pop_is_null - not implement yet."); };
        virtual void pop_skip() { throw _db_error("DB2::statement::pop_skip - not implement yet."); };
        // stub;
};

// --------------------------------------
// --------------------------------------
// Transaction class (& real connection)
// --------------------------------------
// --------------------------------------
class _tr_db2: public _transaction
{
private:

        otl_connect *con;

public:

        _tr_db2(char const *con_str);
        _tr_db2(HENV henv, HDBC hdbc);

        virtual ~_tr_db2();

        virtual void start() {};
        virtual void commit();
        virtual void rollback();

        virtual _statement *statement();

        virtual _db_id gen_id(const char *gen_name, int diff);

        virtual OTL_HENV &get_henv() { return con->get_connect_struct().get_henv(); };
        virtual OTL_HDBC &get_hdbc() { return con->get_connect_struct().get_hdbc(); };
};

// --------------------------------------
// --------------------------------------
// Thread transaction (its handle - its just use _tr_db2)
// --------------------------------------
// --------------------------------------
class _thread_tr_db2: public _transaction
{
private:
        _transaction *real_tr;
        bool del_tr_on_del;
public:

        _thread_tr_db2(_transaction *t,bool df):real_tr(t),del_tr_on_del(df) {};
        virtual ~_thread_tr_db2() { if (del_tr_on_del) delete real_tr; };

        virtual void start()    { real_tr->start(); };
        virtual void commit()   { real_tr->commit(); };
        virtual void rollback() { real_tr->rollback(); };

        virtual _statement *statement() { return real_tr->statement(); };

        virtual _db_id gen_id(const char *gen_name, int diff) { return real_tr->gen_id(gen_name,diff); };
};

// -------------------------------------------------------
// -------------------------------------------------------
// DB Class
// -------------------------------------------------------
// -------------------------------------------------------
class _db_db2: public _db
{
private:

        struct _state
        {
                string dsn;
                string uid;
                string pass;
                string con_str;

                _tr_db2 *null_tr;
        };

        typedef list<_transaction *> _tr_list;
        typedef map<unsigned long,_tr_list> _tr_map;

        static _tr_map cm_tr_map;

        static _state state;
        static int reference;
        static _locker cm_lock;

public:

        _db_db2();
        virtual ~_db_db2();

        virtual bool is_connected() { return true; }; // stub

        virtual void connect(const string &dsn, const string &user, const string &pass);
        virtual void disconnect();

        virtual void lock() { cm_lock.lock(); };
        virtual void unlock() { cm_lock.unlock(); };

        virtual _transaction *transaction(_db::tr_rw_mode rw_mode,_db::tr_w_mode w_mode);
        virtual _transaction *thread_transaction(bool create);
        virtual void          thread_transaction_remove(_transaction *tr);
};

// -------------------------------------------------------
_db_db2::_state _db_db2::state;
int _db_db2::reference = 0;
_locker _db_db2::cm_lock;
_db_db2::_tr_map _db_db2::cm_tr_map;


// -----------------------------------------------------
// STATEMENT IMP
// -----------------------------------------------------
// --------------------------------------
_stat_db2::_stat_db2(otl_connect *c)
:stream(NULL), con(c)
{
        stream = new otl_stream();
}

// --------------------------------------
_stat_db2::~_stat_db2()
{
        close();
        if (stream)
        {
                delete stream;
                stream = NULL;
        }
}

// --------------------------------------
void _stat_db2::prepare(const string &sql)
{
        close();
        try
        {
                stream->open(50, sql.c_str(), *con);
                first_fetch = true;
        }
        catch (const otl_exception& p)
        {
                throw _db_error(string((char *) p.msg) + " on " + p.stm_text);
        }
}

// --------------------------------------
void _stat_db2::execute()
{
        // stub
        // real execute do when all in params is set or when first out param is get
}

// --------------------------------------
bool _stat_db2::fetch()
{
        if (!first_fetch)
        {
                try
                {
                        stream->check_end_of_row();
                }
                catch (otl_exception &e)
                {
                        stream->skip_to_end_of_row();
                }
        }
        first_fetch = false;

        if (stream->eof())
                return false;
        else
                return true;
}

// --------------------------------------
//   Call SIGABRT if exception thrown(in stream->close()) WHEN called from DESTRUCTOR!!.
// To avoid it, call close() BEFORE ~_stat_db2() destroying!!!!!!!!
void _stat_db2::close()
{
        try
        {
                stream->close();
        }
        catch (otl_exception& p)
        {
                throw _db_error(string((char *) p.msg) + " on " + p.stm_text);
        }
}


// -----------------------------------------------------
// TRANSACTION IMP
// -----------------------------------------------------
// -------------------------------------------------------
_tr_db2::_tr_db2(char const *con_str)
:con(NULL)
{
        try
        {
                con = new otl_connect(con_str, 0);
        }
        catch (otl_exception& p)
        {
                throw _db_error(string((char *) p.msg) + " on " + p.stm_text);
        }
}

// -------------------------------------------------------
_tr_db2::_tr_db2(HENV henv, HDBC hdbc)
:con(NULL) //char const *con_str):con(NULL)
{
        try
        {
                con = new otl_connect((long) henv, (long) hdbc, 0);
        }
        catch (otl_exception& p)
        {
                throw _db_error(string((char *) p.msg) + " on " + p.stm_text);
        }
}

// -------------------------------------------------------
_tr_db2::~_tr_db2()
{
        try
        {
                if (con)
                {
                        con->logoff();
                        delete con;
                }
        }
        catch (otl_exception& p)
        {
                throw _db_error(string((char *) p.msg) + " on " + p.stm_text);
        }

        _db_db2().thread_transaction_remove(this);
}

// -------------------------------------------------------
_statement *_tr_db2::statement()
{
        return new _stat_db2(con);
}

// -------------------------------------------------------
void _tr_db2::commit()
{
        try
        {
                if (con)
                        con->commit();
                else
                        throw _db_error("commit on no created transaction");
        }
        catch (otl_exception& p)
        {
                throw _db_error(string((char *) p.msg) + " on " + p.stm_text);
        }
}

// -------------------------------------------------------
void _tr_db2::rollback()
{
        try
        {
                if (con)
                        con->rollback();
                else
                        throw _db_error("rollback on no created transaction");
        }
        catch (otl_exception& p)
        {
                throw _db_error(string((char *) p.msg) + " on " + p.stm_text);
        }
}

// -------------------------------------------------------
_db_id _tr_db2::gen_id(const char *gen_name, int diff)
{
        _db_id res;

        try
        {
                char tmp[256];
				sprintf(tmp,"select next value for %s from sysibm.sysdummy1", gen_name);
                otl_stream s(1, tmp, *con);

                s >> res.value();
        }
        catch (otl_exception& p)
        {
                throw _db_error(string((char *) p.msg) + " on " + p.stm_text);
        }

        return res;
}

// -----------------------------------------------------
// DATABASE IMP
// -----------------------------------------------------
// -------------------------------------------------------
_db_db2::_db_db2()
{
        if (!reference)
        {
                otl_connect::otl_initialize(1); // initialize DB2-CLI environment
        }
        reference++;
}

// -------------------------------------------------------
_db_db2::~_db_db2()
{
        reference--;
        if (!reference)
        {
                disconnect();
        }
}

// -------------------------------------------------------
void _db_db2::connect(const string &dsn, const string &user,const string &pass)
{
        state.dsn = dsn;
        state.uid = user;
        state.pass = pass;

        state.con_str = "DSN=";
        state.con_str += dsn;
        state.con_str += ";UID=";
        state.con_str += user;
        state.con_str += ";PWD=";
        state.con_str += pass;

        state.null_tr = NULL;
        state.null_tr = new _tr_db2(state.con_str.c_str());
        state.null_tr->start();
        state.null_tr->commit();
}

// -------------------------------------------------------
void _db_db2::disconnect()
{
        if (state.null_tr)
        {
                delete state.null_tr;
                state.null_tr = NULL;
        }
}

// -------------------------------------------------------
_transaction *_db_db2::transaction(_db::tr_rw_mode rw_mode,_db::tr_w_mode w_mode)
{
    cm_lock.lock();
    _transaction *tr=new _tr_db2(state.con_str.c_str());

    // put transaction to threads/transactions map
    _tr_map::iterator it=cm_tr_map.find(get_current_thread_id());
    if (it==cm_tr_map.end())
    {
        pair<_tr_map::iterator,bool> res=cm_tr_map.insert(pair<unsigned long,_tr_list>(get_current_thread_id(),_tr_list()));
        if (!res.second)
        {
            delete tr;
            throw _db_error("_db_db2::transaction - can not insert transaction to transactions map for thread"+to_string(get_current_thread_id()));
        }
        it=res.first;
    }
    it->second.push_front(tr);

    cm_lock.unlock();
    return tr;
}

// -------------------------------------------------------
_transaction *_db_db2::thread_transaction(bool create)
{
    //cout << get_current_thread_id() << "_db_db2::thread_transaction" << endl;
    cm_lock.lock();

    // try find transaction in map
    _tr_map::iterator it=cm_tr_map.find(get_current_thread_id());
    if (it==cm_tr_map.end())
    {
        cm_lock.unlock();

        if (create)
        {
            _transaction *tr=transaction(trm_read,trwm_wait);
            return new _thread_tr_db2(tr,true);
        }
        else return NULL;
    }

    // transactions list may be empty
    if (it->second.empty())
    {
        cm_lock.unlock();

        if (create)
        {
            _transaction *tr=transaction(trm_read,trwm_wait);
            return new _thread_tr_db2(tr,true);
        }
        else return NULL;
    }

    // uch - finded
    //cout << get_current_thread_id() << "_db_db2::thread_transaction - finded " << endl;
    _transaction *tr=new _thread_tr_db2(it->second.front(),false);

    cm_lock.unlock();

    return tr;
}

// -------------------------------------------------------
void _db_db2::thread_transaction_remove(_transaction *tr)
{
    //cout << get_current_thread_id() << "_db_db2::thread_transaction_remove" << endl;
    cm_lock.lock();

    // try find transaction in map
    _tr_map::iterator it=cm_tr_map.find(get_current_thread_id());
    if (it==cm_tr_map.end())
    {
        cm_lock.unlock();
        return;
    }

    _tr_list::iterator lit=it->second.begin();
    while(lit!=it->second.end())
    {
        if (*lit==tr)
        {
            it->second.erase(lit);
            if (it->second.empty()) cm_tr_map.erase(it);

            cm_lock.unlock();
            return;
        }
        lit++;
    }

    cm_lock.unlock();
}


// -------------------------------------------------------
_db *create_db_db2()
{
        return new _db_db2;
}
