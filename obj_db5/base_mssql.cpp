#include <iostream>
#include <stdio.h>
#include <clthread.hpp>

#include "base_mssql.hpp"

#define OTL_ODBC_MSSQL_2008  // Compile OTL 4.0/ MSSQL 2008 ODBC
#define OTL_STL 1

#ifdef WINDOWS
        //#define OTL_UNCAUGHT_EXCEPTION_ON
        //#define OTL_DESTRUCTORS_DO_NOT_THROW
        //#define OTL_EXCEPTION_STM_TEXT_SIZE 255
        //#define OTL_NO_TMPL_MEMBER_FUNC_SUPPORT
        #define OTL_BIGINT __int64
        #define MBINT      __int64
#else
        #define OTL_BIGINT long long
        #define MBINT      long long
#endif

#include <otlv4.h> // include the OTL 4.0 header file

class _tr_mssql;
class _stat_mssql;
class _db_mssql;

// --------------------------------------
template<class T> void push_and_check(otl_stream *stream,T &val)
{
        try
        {
                (*stream) << val;
        }
        catch (const otl_exception& p)
        {
                throw _db_error(string((char *) p.msg) + " on " + p.stm_text);
        }
}

template<class T> void pop_and_check(otl_stream *stream,T &val)
{
        try
        {
                (*stream) >> val;
        }
        catch (const otl_exception& p)
        {
                throw _db_error(string((char *) p.msg) + " on " + p.stm_text);
        }

}

/*
// --------------------------------------
push_and_check_ll(otl_stream *stream,)
{
        try
        {
                (*stream) << val;
        }
        catch (const otl_exception& p)
        {
                throw _db_error(string((char *) p.msg) + " on " + p.stm_text);
        }
*/

// --------------------------------------
// --------------------------------------
// Statement
// --------------------------------------
// --------------------------------------
class _stat_mssql: public _statement
{
private:

        otl_stream *stream;
        otl_connect *con;

        bool first_fetch;
        
        struct _lob_rec
        {
            otl_lob_stream *ls;
            const _blob_st *data;
        };
        
        typedef list<_lob_rec> _lobs;
        _lobs lobs;

public:

        _stat_mssql(otl_connect *c);
        virtual ~_stat_mssql();

        virtual void prepare(const string &sql);
        virtual void execute();
        virtual bool fetch();
        virtual void close();

        virtual void start_push() {};
        virtual void push(int v) { push_and_check(stream, v); };
        virtual void push(MBINT v) { push_and_check(stream, v); };
        virtual void push(const _db_id &v) { push_and_check(stream, v.value()); };
        virtual void push(float v) { push_and_check(stream, v); };
        virtual void push(const string &v) { push_and_check(stream, v); } ;
        virtual void push(const char *v) { push_and_check(stream, v); };
        //virtual void push(const _blob_st &v) = 0;// { _transaction t(st->Transaction()); _blob b(t); b=v; st->Set(cur_set_f,b.handle()); cur_set_f++; };
        #if defined(DB_USE_COMMON_TIME)
        virtual void push(const _time &v,bool use_db_type=false) { _time::time_val_i t=v.stamp(); push_and_check(stream,t); };
        #endif
		virtual void push(const _blob_st &v);// { throw _db_error("DB2::statement::push - not implement yet."); };

        virtual void start_pop() { stream->skip_to_end_of_row(); };
        virtual bool pop(int *v) { pop_and_check(stream,(*v)); return true; };
        virtual bool pop(MBINT *v) { pop_and_check(stream,(*v)); return true; };
        virtual bool pop(_db_id &v) { pop_and_check(stream,v.value()); return true; };
        virtual bool pop(float *v) { pop_and_check(stream,(*v)); return true; };
        virtual bool pop(string &v) { pop_and_check(stream,v); return true; };
        virtual bool pop(char *v) { pop_and_check(stream,(*v)); return true; };
        #if defined(DB_USE_COMMON_TIME)
        virtual bool pop(_time &v,bool use_db_type=false) { _time::time_val_i t; pop_and_check(stream,t); v.stamp(t); return true; };
        #endif
        virtual bool pop(_blob_st &v);// { throw _db_error("MSSQL::statement::pop_blob - not implement yet."); };
        virtual bool pop_is_null() { throw _db_error("MSSQL::statement::pop_is_null - not implement yet."); };
        virtual void pop_skip() { throw _db_error("MSSQL::statement::pop_skip - not implement yet."); };
        // stub;
};

// --------------------------------------
_stat_mssql::_stat_mssql(otl_connect *c)
:stream(NULL), con(c)
{
    try
    {
        stream = new otl_stream();
    }
    catch(const otl_exception& p)
    {
        throw _db_error(string((char *) p.msg) + " on " + p.stm_text);
    }
}

// --------------------------------------
_stat_mssql::~_stat_mssql()
{
    close();
    if (stream)
    {
        delete stream;
        stream = NULL;
    }
}

// --------------------------------------
void _stat_mssql::prepare(const string &sql)
{
        close();
        try
        {
            if (sql.find("<raw_long>")!=string::npos || sql.find("<varchar_long>")!=string::npos)
            {
                stream->set_lob_stream_mode(true);
                stream->open(1, sql.c_str(), *con);
            }
            else
            {
                stream->set_lob_stream_mode(false);
                stream->open(50, sql.c_str(), *con); // 50, sql.c_str(), *con);
            }
            stream->set_commit(0);
            first_fetch = true;
        }
        catch (const otl_exception& p)
        {
            throw _db_error(string((char *) p.msg) + " on " + p.stm_text);
        }
}

// --------------------------------------
void _stat_mssql::execute()
{
    // we need fill all lobs (if was any)
    try
    {
        _lobs::iterator lit=lobs.begin();
        while(lit!=lobs.end())
        {
            lit->ls->set_len(lit->data->length());
            (*lit->ls) << (*lit->data);
            lit->ls->close();
            
            lit++;
        }
        
        lit=lobs.begin();
        while(lit!=lobs.end())
        {
            delete lit->ls;
            lit++;
        }
        lobs.clear(); 
    }
    catch (otl_exception& p)
    {
        throw _db_error(string((char *) p.msg) + " on " + p.stm_text);
    }
}

// --------------------------------------
bool _stat_mssql::fetch()
{
    if (!first_fetch)
    {
        try
        {
            stream->check_end_of_row();
        }
        catch (otl_exception &e)
        {
            stream->skip_to_end_of_row();
        }
    }
    first_fetch = false;

    if (stream->eof())
        return false;
    else
        return true;
}

// --------------------------------------
void _stat_mssql::close()
{
    try
    {
        _lobs::iterator lit=lobs.begin();
        while(lit!=lobs.end())
        {
            lit->ls->close();
            delete lit->ls;
        }
        lobs.clear();
        
        stream->close();
    }
    catch (otl_exception& p)
    {
        throw _db_error(string((char *) p.msg) + " on " + p.stm_text);
    }
}

// --------------------------------------
void _stat_mssql::push(const _blob_st &v)
{
    try
    {
        _lob_rec r;
        r.ls=new otl_lob_stream;
        r.data=&v;
        (*stream) << (*r.ls);
        
        lobs.push_back(r);
    }
    catch(otl_exception& p)
    {
        throw _db_error(string((char *) p.msg) + " on " + p.stm_text);
    }
}

// --------------------------------------
bool _stat_mssql::pop(_blob_st &v)
{
    try
    {
        otl_lob_stream ls;
        (*stream) >> ls;
        ls >> v;
        ls.close();
    }
    catch(otl_exception& p)
    {
        throw _db_error(string((char *) p.msg) + " on " + p.stm_text);
    }
    return true;
}

// --------------------------------------
// --------------------------------------
// Transaction class (& real connection)
// --------------------------------------
// --------------------------------------
class _tr_mssql: public _transaction
{
private:

        otl_connect *con;
       
public:

        _tr_mssql(char const *con_str);
        _tr_mssql(HENV henv, HDBC hdbc);

        virtual ~_tr_mssql();

        virtual void start() {};
        virtual void commit();
        virtual void rollback();

        virtual _statement *statement();

        virtual _db_id gen_id(const char *gen_name, int diff);

        virtual OTL_HENV &get_henv() { return con->get_connect_struct().get_henv(); };
        virtual OTL_HDBC &get_hdbc() { return con->get_connect_struct().get_hdbc(); };
};

// -------------------------------------------------------
_tr_mssql::_tr_mssql(char const *con_str)
:con(NULL)
{
        try
        {
                con = new otl_connect(con_str, 0);
        }
        catch (otl_exception& p)
        {
                throw _db_error(string((char *) p.msg) + " on " + p.stm_text);
        }
}

// -------------------------------------------------------
_tr_mssql::_tr_mssql(HENV henv, HDBC hdbc)
:con(NULL) //char const *con_str):con(NULL)
{
        try
        {
                con = new otl_connect( henv, hdbc, 0);
        }
        catch (otl_exception& p)
        {
                throw _db_error(string((char *) p.msg) + " on " + p.stm_text);
        }
}

// -------------------------------------------------------
_tr_mssql::~_tr_mssql()
{
        try
        {
                if (con)
                {
                        con->logoff();
                        delete con;
                }
        }
        catch (otl_exception& p)
        {
                throw _db_error(string((char *) p.msg) + " on " + p.stm_text);
        }
}

// -------------------------------------------------------
_statement *_tr_mssql::statement()
{
        return new _stat_mssql(con);
}

// -------------------------------------------------------
void _tr_mssql::commit()
{
        try
        {
                if (con)
                        con->commit();
                else
                        throw _db_error("commit on no created transaction");
        }
        catch (otl_exception& p)
        {
                throw _db_error(string((char *) p.msg) + " on " + p.stm_text);
        }
}

// -------------------------------------------------------
void _tr_mssql::rollback()
{
        try
        {
                if (con)
                        con->rollback();
                else
                        throw _db_error("rollback on no created transaction");
        }
        catch (otl_exception& p)
        {
                throw _db_error(string((char *) p.msg) + " on " + p.stm_text);
        }
}

// -------------------------------------------------------
_db_id _tr_mssql::gen_id(const char *gen_name, int diff)
{
// MS SQL Server 2005, 2008 do not have SEQUENCES (FUCK!!!!)
// emulate it with table sequences & stored procedure gen_seq_val
//
// Text of stored proc    
/*
USE [AC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[gen_seq_val] 
	@seq_name varchar(255), 
	@ginc bigint,
	@val bigint out
AS
BEGIN
    SET NOCOUNT ON
    declare @z bigint
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	BEGIN TRAN
		UPDATE sequences WITH(HOLDLOCK) SET @z = currval = currval+@ginc WHERE seq_name = @seq_name
		select @val=@z
	COMMIT TRAN
END
*/
        __db_id_type res;
        
        try
        {
            otl_stream o(1, // buffer size should be equal to 1 in case of stored procedure call
              "{call gen_seq_val("
              " :seq_name<char[255],in>, "
              " :gdiff<bigint,in>, "
              " :val<bigint,out> "
              ")}",
              *con
             );

            o.set_commit(0); // set stream auto-commit off since
                             // the stream does not generate transaction

            __db_id_type bdiff=diff;
            
            o<<gen_name<<bdiff;
            o>>res;
            
        }
        catch (otl_exception& p)
        {
            throw _db_error(string((char *) p.msg) + " on " + p.stm_text);
        }
        
        return res;
}

// -------------------------------------------------------
// -------------------------------------------------------
// DB Class
// -------------------------------------------------------
// -------------------------------------------------------
class _db_mssql: public _db
{
private:

        struct _state
        {
                string dsn;
                string uid;
                string pass;
                string con_str;

                _tr_mssql *null_tr;
        };

        static _state state;
        static int reference;
        static _locker cm_lock;

public:

        _db_mssql();
        virtual ~_db_mssql();

        virtual bool is_connected() { return true; }; // stub

        virtual void connect(const string &dsn, const string &user, const string &pass);
        virtual void disconnect();

        virtual void lock() { cm_lock.lock(); };
        virtual void unlock() { cm_lock.unlock(); };

        virtual _transaction *transaction(_db::tr_rw_mode rw_mode,_db::tr_w_mode w_mode);
};

// -------------------------------------------------------
_db_mssql::_state _db_mssql::state;
int _db_mssql::reference = 0;
_locker _db_mssql::cm_lock;

// -------------------------------------------------------
_db_mssql::_db_mssql()
{
        if (!reference)
        {
                otl_connect::otl_initialize(1); // initialize DB2-CLI environment
        }
        reference++;
}

// -------------------------------------------------------
_db_mssql::~_db_mssql()
{
        reference--;
        if (!reference)
        {
                disconnect();
        }
}

// -------------------------------------------------------
void _db_mssql::connect(const string &dsn, const string &user,const string &pass)
{
        state.dsn = dsn;
        state.uid = user;
        state.pass = pass;

        state.con_str = user;
        state.con_str += "/";
        state.con_str += pass;
        state.con_str += "@";
        state.con_str += dsn;

        state.null_tr = NULL;
        state.null_tr = new _tr_mssql(state.con_str.c_str());
        state.null_tr->start();
        state.null_tr->commit();
}

// -------------------------------------------------------
void _db_mssql::disconnect()
{
        if (state.null_tr)
        {
                delete state.null_tr;
                state.null_tr = NULL;
        }
}

// -------------------------------------------------------
_transaction *_db_mssql::transaction(_db::tr_rw_mode rw_mode,_db::tr_w_mode w_mode)
{
    cm_lock.lock();
    /*
    _transaction *tr=new _tr_mssql((void *) state.null_tr->get_henv(),
                        (void *) state.null_tr->get_hdbc());
     */
    _transaction *tr=new _tr_mssql(state.con_str.c_str());
    cm_lock.unlock();
    return tr;
}

// -------------------------------------------------------
_db *create_db_mssql()
{
        return new _db_mssql;
}

/*
 // --------------------------------------
 _db_id _transaction::gen_id(const char *gen_name,int diff)
 {
 _db_id res;

 try
 {
 _statement st(*this);

 char tmp[64];
 sprintf(tmp,"select gen_id(%s,%i) from rdb$database;",gen_name,diff);
 st.prepare(tmp);

 st.execute();
 st.fetch();
 st.get(1,res);

 }
 catch(IBPP::Exception &e)
 {
 std::string s="_transaction::gen_id(";
 s+=gen_name;
 s+=",);";
 s+=e.ErrorMessage();
 throw(_db_error(s));
 }

 return res;
 }

 // --------------------------------------
 // --------------------------------------

 // --------------------------------------
 void _statement::prepare(const std::string &sql)
 {
 try
 {
 st->Prepare(sql);
 }
 catch(IBPP::Exception &e)
 {
 std::string s="_statement::prepare - ";
 s+=e.ErrorMessage();
 cout << s.c_str() << endl << flush;
 throw(_db_error(s));
 }
 start_push();
 }

 // --------------------------------------
 void _statement::execute()
 {
 try
 {
 st->Execute();
 }
 catch(IBPP::Exception &e)
 {
 std::string s="_statement::execute - ";
 s+=e.ErrorMessage();
 cout << s.c_str() << endl << flush;
 throw(_db_error(s));
 }
 }

 // --------------------------------------
 bool _statement::fetch()
 {
 start_pop();
 bool k;

 try
 {
 k=st->Fetch();
 }
 catch(IBPP::Exception &e)
 {
 std::string s="_statement::fetch - ";
 s+=e.ErrorMessage();
 cout << s.c_str() << endl << flush;
 throw(_db_error(s));
 }

 return k;
 }

 // --------------------------------------
 void _statement::close()
 {
 try
 {
 st->Close();
 }
 catch(IBPP::Exception &e)
 {
 std::string s="_statement::close - ";
 s+=e.ErrorMessage();
 cout << s.c_str() << endl << flush;
 throw(_db_error(s));
 }
 }

 // --------------------------------------
 // --------------------------------------


 // --------------------------------------
 const std::string &_selector::sql_ref()
 {
 sql="select ";
 list<std::string>::iterator it;

 if (vars.empty()) sql+="* ";
 else
 {
 it=vars.begin();
 while(it!=vars.end())
 {
 sql+=(*it);
 sql+=", ";
 it++;
 }

 sql.resize(sql.length()-2);
 sql+=' ';
 }

 sql+="from ";

 it=froms.begin();
 while(it!=froms.end())
 {
 sql+=(*it);
 sql+=", ";
 it++;
 }
 sql.resize(sql.length()-2);
 sql+=' ';

 if (!conds.empty())
 {
 sql+="where ";
 it=conds.begin();
 while(it!=conds.end())
 {
 sql+=(*it);
 sql+=" and ";
 it++;
 }

 sql.resize(sql.length()-4);
 sql+=' ';
 }

 if (order!="")
 {
 sql+="order by ";
 sql+=order;
 }

 return sql;
 }

 // --------------------------------------
 // --------------------------------------


 _blob_st::_blob_st():std::string() {};
 _blob_st::_blob_st(const char *s):std::string(s) {};
 _blob_st::_blob_st(const std::string &s):std::string(s) {};

 // --------------------------------------
 _blob &_blob::operator=(const _blob_st &s)
 {
 data->Create();
 int pos=0;
 int size=0;
 while(pos<s.length())
 {
 size=1024;
 if (size+pos>s.length()) size=s.length()-pos;
 data->Write(&s.c_str()[pos],size);
 pos+=size;
 }
 data->Close();

 return *this;
 }

 // --------------------------------------
 _blob::operator _blob_st() const
 {
 data->Open();
 int all_size;
 int buff_size;
 int s_count;
 data->Info(&all_size,&buff_size,&s_count);

 if (all_size==0) { return std::string(); }

 char *buffer = new char [buff_size];
 _blob_st res="";
 int readed;
 while((readed=data->Read(buffer,buff_size)))
 {
 res+=std::string(buffer,readed);
 }
 delete buffer;

 data->Close();

 return res;
 }
 */

