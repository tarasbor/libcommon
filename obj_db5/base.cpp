#include "base.hpp"

// --------------------------------------
const std::string &_selector::sql_ref()
{
    sql="select ";
    list<std::string>::iterator it;

    if (vars.empty()) sql+="* ";
    else
    {
        it=vars.begin();
        while(it!=vars.end())
        {
            sql+=(*it);
            sql+=", ";
            it++;
        }

        sql.resize(sql.length()-2);
        sql+=' ';
    }

    sql+="from ";

    it=froms.begin();
    while(it!=froms.end())
    {
        sql+=(*it);
        sql+=", ";
        it++;
    }
    sql.resize(sql.length()-2);
    sql+=' ';

    if (!join.empty())
    {
        it=join.begin();
        while(it!=join.end())
        {
            sql+=(*it);
            sql+=" ";
            it++;
        }
    }

    if (!conds.empty())
    {
        sql+="where ";
        it=conds.begin();
        while(it!=conds.end())
        {
            sql+=(*it);
            sql+=" and ";
            it++;
        }

        sql.resize(sql.length()-4);
        sql+=' ';
    }

    if (order!="")
    {
        sql+="order by ";
        sql+=order;
    }

	if (group!="")
	{
		sql+=" group by ";
		sql+=group;
	}

    return sql;
}
