#ifndef __OBJ_DATABASE_H__
#define __OBJ_DATABASE_H__

#include <list>
#include <string>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

#ifdef WINDOWS
    // on Windows we use MS RUNTIME - it use "%I64i" to print long long
    #define ID_PRINT	"%I64i"
    typedef __int64 __db_id_type;
#else
    // on Linux runtime use "%Li" to print long long
    #define ID_PRINT	"%Li"
    typedef long long __db_id_type;
#endif

#include <time.hpp>

// ----------------------------------------------------
class _db_error
{
    private:

      string _error_st;

    public:

      _db_error(const string &s):_error_st(s) {};
      inline const string &error_st() const { return _error_st; };
};


// ----------------------------------------------------
class _db_id
{
    private:

      __db_id_type id_val;

    public:

      _db_id():id_val(0) {};
      _db_id(const string &s) { id_val=atoll(s.c_str()); };
      _db_id(const _db_id &s) { id_val=s.id_val; };
      _db_id(const __db_id_type &s) { id_val=s; };

      bool operator==(const _db_id &i) const { return id_val==i.id_val; };
      bool operator!=(const _db_id &i) const { return id_val!=i.id_val; };
      bool operator<(const _db_id &i) const { return id_val<i.id_val; };
      bool operator>(const _db_id &i) const { return id_val>i.id_val; };

      bool is_set() const { return id_val!=0; };
      void undef() { id_val=0; };

      __db_id_type &value() { return id_val; };
      const __db_id_type &value() const { return id_val; };

	  const char *value_str(char *to) const { sprintf(to,ID_PRINT,id_val); return to; }
	  void from_str(const char *from) { sscanf(from,ID_PRINT,&id_val); };
	  inline void from_str(const string &from) { from_str(from.c_str()); };

      //operator __int64() { return id_val; };
};

// -----------------------------------------------------------------
class _blob_st:public std::string
{
    public:

      _blob_st():std::string() {};
      _blob_st(const char *s):std::string(s) {};
      _blob_st(const string &s):std::string(s) {};
};

// -----------------------------------------------------------------
class _db_event
{
	public:
		_db_event() {};
		virtual ~_db_event() {};

		virtual void on_event(const string &name,int n) = 0;
};

// -----------------------------------------------------------------
class _statement
{
    private:

      _statement &operator=(const _statement &_s);

    public:

      _statement() {};
      virtual ~_statement() {};

      _statement(const _statement &_s);

      virtual void prepare(const string &sql) = 0;
      virtual void execute() = 0;
      virtual bool fetch() = 0;
      virtual void close() = 0;

      /*virtual void set(int n,int v) = 0;// { st->Set(n,v); };
      virtual void set(int n,__int64 v) = 0;// { st->Set(n,v); };
      virtual void set(int n,const _db_id &v) = 0;// { st->Set(n,v.value()); };
      virtual void set(int n,float v) = 0;// { st->Set(n,v); };
      virtual void set(int n,const string &v) = 0;// { st->Set(n,v); };
      virtual void set(int n,const char *v) = 0;// { st->Set(n,v); };
      //virtual void set(int n,const _blob_st &v) = 0;// { _transaction t(st->Transaction()); _blob b(t); b=v; st->Set(n,b.handle()); };
      //void set(int n,const _blob_string &v) { }

      virtual bool get(int n,int *v) = 0;// { return st->Get(n,v); };
      virtual bool get(int n,__int64 *v) = 0;// { return st->Get(n,v); };
      virtual bool get(int n,_db_id &v) = 0;// { return st->Get(n,&(v.value())); };
      virtual bool get(int n,float *v) = 0;//) { return st->Get(n,v); };
      virtual bool get(int n,string &v) = 0;// { return st->Get(n,v); };
      virtual bool get(int n,char *v) = 0;// { return st->Get(n,v); };
      //virtual bool get(int n,_blob_st &v) = 0;// { _transaction t(st->Transaction()); _blob b(t); bool res=st->Get(n,b.handle()); v=b; return res; };
      virtual bool is_null(int n) = 0;// { return st->IsNull(n); };

      virtual int column_num(const string& s) = 0;// { return st->ColumnNum(s); };
      */

      virtual void start_push() = 0;// { cur_set_f=1; };
      virtual void push(int v) = 0;// { st->Set(cur_set_f,v); cur_set_f++; };
      virtual void push(__db_id_type v) = 0;// { st->Set(cur_set_f,v); cur_set_f++; };
      virtual void push(const _db_id &v) = 0;// { st->Set(cur_set_f,v.value()); cur_set_f++; };
      virtual void push(float v) = 0;// { st->Set(cur_set_f,v); cur_set_f++; };
      virtual void push(const string &v) = 0;// { st->Set(cur_set_f,v); cur_set_f++; };
      virtual void push(const char *v) = 0;// { st->Set(cur_set_f,v); cur_set_f++; };
      virtual void push(const _time &v,bool use_db_type=false) = 0;
      virtual void push(const _blob_st &v) = 0;// { _transaction t(st->Transaction()); _blob b(t); b=v; st->Set(cur_set_f,b.handle()); cur_set_f++; };

      virtual void start_pop() = 0;// { cur_get_f=1; };
      virtual bool pop(int *v) = 0;// { cur_get_f++; return st->Get(cur_get_f-1,v);  };
      virtual bool pop(__db_id_type *v) = 0;// { cur_get_f++; return st->Get(cur_get_f-1,v); };
      virtual bool pop(_db_id &v) = 0;// { cur_get_f++; return st->Get(cur_get_f-1,&(v.value())); };
      virtual bool pop(float *v) = 0;// { cur_get_f++; return st->Get(cur_get_f-1,v); };
      virtual bool pop(std::string &v) = 0;// { cur_get_f++; return st->Get(cur_get_f-1,v); };
      virtual bool pop(char *v) = 0;// { cur_get_f++; return st->Get(cur_get_f-1,v); };
      virtual bool pop(_time &v,bool use_db_type=false) = 0;
      virtual bool pop(_blob_st &v) = 0;// { _transaction t(st->Transaction()); _blob b(t); bool res=st->Get(cur_get_f,b.handle()); v=b; cur_get_f++; return res; };
      virtual bool pop_is_null() = 0;// { return st->IsNull(cur_get_f); };
	  virtual void pop_skip() = 0;
};

// -----------------------------------------------------------------
class _transaction
{
    private:

      _transaction &operator=(const _transaction &_t);// { _db().lock(); tr=_t.tr; return *this; };
      _transaction(const _transaction &_t);//:need_unlock(false) { tr=_t.tr; };

    protected:
    public:

      _transaction() {};
      virtual ~_transaction() {}; // { if (need_unlock) _db().unlock(); };

      virtual _statement *statement() = 0;

      virtual void start() = 0;
      virtual void commit() = 0;
      virtual void rollback() = 0;

      virtual _db_id gen_id(const char *gen_name,int diff) = 0;
};

// ----------------------------------------------------
// ----------------------------------------------------
class _blob
{
    private:
       _blob &operator=(const _blob &b);

    public:

       _blob() {};
       virtual ~_blob() {};

       _blob(const _blob &b);

       virtual _blob &operator=(const _blob_st &s) = 0;
       virtual operator _blob_st() const = 0;
};

// ----------------------------------------------------
class _db
{
    private:

      _db &operator=(const _db &d);
      _db(const _db &d);

    public:

	  enum tr_rw_mode { trm_read, trm_write };
	  enum tr_w_mode { trwm_wait };

      _db() {};
      virtual ~_db() {};

      virtual bool is_connected() = 0;

      virtual void connect(const string &dsn,const string &user,const string &pass) = 0;
      virtual void disconnect() = 0;

      virtual void lock() = 0;
      virtual void unlock() = 0;

      virtual _transaction *transaction(tr_rw_mode rw_mode = trm_read ,tr_w_mode = trwm_wait) = 0;
      virtual _transaction *thread_transaction(bool create = false) = 0;
};

// ----------------------------------------------------
// ----------------------------------------------------
template<class Type> class _obj_db_iterator
{
    private:

      Type *ptr;

      _transaction *tr;
      _statement *st;

      bool is_eof;

      _obj_db_iterator& operator=(const _obj_db_iterator &it);
      _obj_db_iterator(const _obj_db_iterator &it); // :is_eof(it.eof()),tr(it.tr),st(it.st),ptr(NULL) { cout << "Copy constructor..." << endl; };

    public:

      _obj_db_iterator(_transaction *_tr,_statement *_st):ptr(NULL),tr(_tr),st(_st),is_eof(false) { operator++(0); }
      ~_obj_db_iterator()
      {
          if (ptr) delete ptr;
          if (st) delete st;
      }

      void operator++(int)
      {
          if (is_eof) return;

          if (!st->fetch())
          {
             delete ptr;
             ptr=NULL;
             is_eof=true;
             return;
          }

          _db_id id;
          st->pop(id);

          if (!ptr) ptr=new Type(id,tr,st); else ptr->next(id,st);
      }

      inline Type *operator->() { return ptr; };
      inline operator Type*() { return ptr; }

      inline const bool eof() const { return is_eof; };
};

// ----------------------------------------------------
// ----------------------------------------------------
class _condition
{
    protected:

      string sql;

    public:

      _condition():sql("") {};
      _condition(const _condition &c):sql(c.sql) {};
      virtual ~_condition() {};

      _condition &operator=(const _condition &c) { sql=c.sql; return *this; };

      const string &sql_ref() const { return sql; };

      _condition operator&(const _condition &c)
      {
          _condition rez;
          rez.sql=sql;
          rez.sql+=" and ";
          rez.sql+=c.sql;
          return rez;
      };
};

// --------------------------------------------------------------------------
class _selector
{
    private:

      string sql;

      list<string> vars;
      list<string> froms;
      list<string> conds;
      list<string> join;

      string order;
	  string group;

    public:

      inline void add_var(const string &_var) { vars.push_back(_var); };
      inline void add_from(const string &_from) { froms.push_back(_from); };
      inline void add_join(const string &_join) { join.push_back(_join); };
      inline void add_cond(const string &_cond) { conds.push_back(_cond); };
      inline void set_order(const string &_order) { order=_order; };
	  inline void set_group(const string &_group) { group=_group; };

      const string &sql_ref();

	  _selector() {};
};

// --------------------------------------------------------------------------
template<class T> class _db_obj
{
    protected:

      _transaction *ptr_tr;

      _db_id obj_id;
      _db_id obj_list_id;

      T obj;

      bool need_save;
      bool is_new;

      _db_obj<T>(const _db_obj<T> &u);
      _db_obj<T> &operator=(const _db_obj<T> &u);

      virtual void select_prepare(_statement *st)
      {
          st->prepare(obj.select_string);
          st->push(obj_id);
          st->execute();
      }

      virtual void select(_statement *st)
      {
          need_save=false;
          is_new=false;
          //st->pop(obj_id);
          st->pop(obj_list_id);
          obj.on_select(st);
      }

      virtual void update()
      {
          try
          {
              _statement *st = ptr_tr->statement();
              //_statement st(ptr_tr);
              if (!is_new) st->prepare(obj.update_string); else st->prepare(obj.insert_string);

              st->push(obj_list_id);
              obj.on_update(st);
              st->push(obj_id);

              st->execute();
              need_save=false;
              is_new=false;

              st->close(); // avoid app crash when exception in destructor
              delete st;
          }
          catch(_db_error &e)
          {
              std::string e_st=obj.obj_name;
              e_st+=" update - ";
              throw(_db_error(e_st+e.error_st()));
          }
      }

    public:

      _db_obj<T>(_transaction *tr):ptr_tr(tr),obj_id(tr->gen_id(obj.gen_name,1)),obj_list_id(),need_save(true),is_new(true) {}
      _db_obj<T>(const _db_id &id,_transaction *tr):ptr_tr(tr),obj_id(id),need_save(false),is_new(false)
      {
          _statement *st = ptr_tr->statement();
          select_prepare(st);
		  obj_id.undef();
          if (st->fetch())
		  {
			st->pop(obj_id);
			select(st);
		  }
		  else
		  {
			/* TODO - throw exception - may be*/
		  }
          delete st;
      }

      _db_obj<T>(const _db_id &id,_transaction *tr,_statement *st):ptr_tr(tr),obj_id(id),need_save(false),is_new(false)
      {
          select(st);
      }

      virtual ~_db_obj<T>()
      {
          try { if (need_save) update(); }
          catch(_db_error &e) { throw(e); }
      }

      inline void exc_update_to_db()
      {
          try { if (need_save) update(); }
          catch(_db_error &e) { throw(e); }
      }

      inline void next(const _db_id &id,_statement *st) { if (need_save) update(); obj_id=id; select(st); };

      inline const _db_id &id() const { return obj_id; };
      inline const _db_id &list_id() const { return obj_list_id; };

      inline void donot_save() { need_save=false; };

};

/*
// -----------------------------------------------------------------------------
template<class T> class _db_smart_obj
{
    protected:

      _transaction *ptr_tr;

      _db_id obj_id;
      _db_id obj_list_id;
      _time tbegin;
      _time tend;

      T obj;

      bool need_save;
      bool is_new;

      _db_smart_obj<T>(const _db_smart_obj<T> &u);
      _db_smart_obj<T> &operator=(const _db_smart_obj<T> &u);

      virtual void select_prepare(_statement *st)
      {
          st->prepare(obj.select_string);
          st->push(obj_id);
          st->execute();
      }

      virtual void select(_statement *st)
      {
          need_save=false;
          is_new=false;
          //st->pop(obj_id);
          st->pop(obj_list_id);
          obj.on_select(st);
      }

      virtual void update()
      {
          try
          {
              _statement *st = ptr_tr->statement();
              //_statement st(ptr_tr);
              if (!is_new) st->prepare(obj.update_string); else st->prepare(obj.insert_string);

              st->push(obj_list_id);
              obj.on_update(st);
              st->push(obj_id);

              st->execute();
              need_save=false;
              is_new=false;

              delete st;
          }
          catch(_db_error &e)
          {
              std::string e_st=obj.obj_name;
              e_st+=" update - ";
              throw(_db_error(e_st+e.error_st()));
          }
      }

    public:

      _db_obj<T>(_transaction *tr):ptr_tr(tr),obj_id(tr->gen_id("GL_GEN",1)),obj_list_id(),need_save(true),is_new(true) {};
      _db_obj<T>(const _db_id &id,_transaction *tr):ptr_tr(tr),obj_id(id),need_save(false),is_new(false)
      {
          _statement *st = ptr_tr->statement();
          select_prepare(st);
		  obj_id.undef();
          if (st->fetch())
		  {
			st->pop(obj_id);
			select(st);
		  }
		  else
		  {
			// TODO - throw exception - may be
		  }
          delete st;
      }

      _db_obj<T>(const _db_id &id,_transaction *tr,_statement *st):ptr_tr(tr),obj_id(id),need_save(false),is_new(false)
      {
          select(st);
      }

      virtual ~_db_obj<T>()
      {
          try { if (need_save) update(); }
          catch(_db_error &e) { throw(e); }
      }

      inline void next(const _db_id &id,_statement *st) { if (need_save) update(); obj_id=id; select(st); };

      inline const _db_id &id() const { return obj_id; };
      inline const _db_id &list_id() const { return obj_list_id; };

      inline void donot_save() { need_save=false; };

};
*/

#endif
