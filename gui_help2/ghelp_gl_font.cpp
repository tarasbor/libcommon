#include "ghelp_gl_font.hpp"
#include <error.hpp>
#include <GL/gl.h>

//#include <ftglyph.h>
#include <freetype/ftglyph.h>
#include <iostream>

using namespace _gh;

// --------------------------------------------------------------------
// This function gets the first power of 2 >= the int that we pass it.
// --------------------------------------------------------------------
inline int next_p2 ( int a )
{
	int rval=1;
	while(rval<a) rval<<=1;
	return rval;
}

// --------------------------------------------------------------------
// One char
// --------------------------------------------------------------------
_rchar::_rchar(FT_Face &face,unsigned int code)
{
    cm_char.id=code;

    if(FT_Load_Glyph( face, FT_Get_Char_Index( face, code ), FT_LOAD_DEFAULT ))
		throw _error(0,"_rchar::_rchar - FT_Load_Glyph failed");

	//Move the face's glyph into a Glyph object.
    FT_Glyph glyph;
    if(FT_Get_Glyph( face->glyph, &glyph ))
		throw _error(0,"_rchar::_rchar - FT_Get_Glyph failed");

	//Convert the glyph to a bitmap.
	FT_Glyph_To_Bitmap( &glyph, ft_render_mode_normal, 0, 1 );
    FT_BitmapGlyph bitmap_glyph = (FT_BitmapGlyph)glyph;

	//This reference will make accessing the bitmap easier
	FT_Bitmap& bitmap=bitmap_glyph->bitmap;

    cm_char.w=bitmap.width;
    cm_char.h=bitmap.rows;

	//Use our helper function to get the widths of
	//the bitmap data that we will need in order to create
	//our texture.
	cm_text.w = next_p2( bitmap.width );
	cm_text.h = next_p2( bitmap.rows );

	//Allocate memory for the texture data.
	GLubyte* expanded_data = new GLubyte[ 2 * cm_text.w * cm_text.h];

	//Here we fill in the data for the expanded bitmap.
	//Notice that we are using two channel bitmap (one for
	//luminocity and one for alpha), but we assign
	//both luminocity and alpha to the value that we
	//find in the FreeType bitmap.
	//We use the ?: operator so that value which we use
	//will be 0 if we are in the padding zone, and whatever
	//is the the Freetype bitmap otherwise.
	for(unsigned int j=0; j <cm_text.h;j++)
    {
		for(unsigned int i=0; i < cm_text.w; i++)
        {
			expanded_data[2*(i+j*cm_text.w)] = expanded_data[2*(i+j*cm_text.w)+1] =
				(i>=bitmap.width || j>=bitmap.rows) ? 0 : bitmap.buffer[i + bitmap.width*j];
		}
	}

    // Create texture
    glGenTextures(1,&cm_text.id);

	//Now we just setup some texture paramaters.
    glBindTexture( GL_TEXTURE_2D, cm_text.id);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);

	//Here we actually create the texture itself, notice
	//that we are using GL_LUMINANCE_ALPHA to indicate that
	//we are using 2 channel data.
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, cm_text.w, cm_text.h,
		  0, GL_LUMINANCE_ALPHA, GL_UNSIGNED_BYTE, expanded_data );

	//With the texture created, we don't need to expanded data anymore
    delete [] expanded_data;

	//So now we can create the display list
    cm_list_id=glGenLists(1);
	glNewList(cm_list_id,GL_COMPILE);

	glBindTexture(GL_TEXTURE_2D,cm_text.id);

	//Now we need to account for the fact that many of
	//our textures are filled with empty padding space.
	//We figure what portion of the texture is used by
	//the actual character and store that information in
	//the x and y variables, then when we draw the
	//quad, we will only reference the parts of the texture
	//that we contain the character itself.
	float	x=(float)(bitmap.width) / (float)(cm_text.w),
			y=(float)bitmap.rows / (float)cm_text.h;

	glTranslatef((face->glyph->metrics.horiBearingX)>>6,-(face->glyph->metrics.horiBearingY)>>6,0);

	glBegin(GL_QUADS);

    glTexCoord2d(0,y); glVertex2f(0,bitmap.rows);
	glTexCoord2d(0,0); glVertex2f(0,0);
	glTexCoord2d(x,0); glVertex2f(bitmap.width,0);
	glTexCoord2d(x,y); glVertex2f(bitmap.width,bitmap.rows);

	glEnd();


    // Move to next char & restore y position
	//glTranslatef(face->glyph->advance.x >> 6 ,(face->glyph->metrics.horiBearingY)>>6,0);
    glTranslatef((face->glyph->metrics.horiAdvance-face->glyph->metrics.horiBearingX)>>6,(face->glyph->metrics.horiBearingY)>>6,0);

	//Finnish the display list
	glEndList();

    cm_char.advice_w=face->glyph->metrics.horiAdvance>>6;
    cm_char.advice_h=face->glyph->advance.y;

    FT_Done_Glyph(glyph);
}

// --------------------------------------------------------------------
_rchar::~_rchar()
{
    glDeleteLists(cm_list_id,1);
    glDeleteTextures(1,&cm_text.id);
}

// --------------------------------------------------------------------
void _rchar::out() const
{
    glCallList(cm_list_id);
}


// --------------------------------------------------------------------
// Set of chars for one font (IMPLEMENTATION)
// --------------------------------------------------------------------
_rfont::_rfont(FT_Library &lib,const string &file_name, unsigned int h):cm_char_h(h)
{
    //cout << file_name << endl;
	if (FT_New_Face(lib, file_name.c_str(), 0, &ft_face ))
		throw _error(0,"_rfont::_rfont - FT_New_Face failed (there is probably a problem with your font file)");
}

// --------------------------------------------------------------------
_rfont::~_rfont()
{
    _rchars_map::iterator it=cm_cmap.begin();
    while(it!=cm_cmap.end())
    {
        delete it->second;
        it++;
    }

    FT_Done_Face(ft_face);
}

// --------------------------------------------------------------------
const _rchar *_rfont::operator[](unsigned int i)
{
    _rchars_map::iterator it=cm_cmap.find(i);
    if (it!=cm_cmap.end()) return it->second;

    FT_Set_Char_Size( ft_face, cm_char_h << 6, cm_char_h << 6, 96, 96);

    _rchar *c=new _rchar(ft_face,i);
    cm_cmap[i]=c;
    return c;
}

// --------------------------------------------------------------------
// Set of fonts (IMPLEMENTATION)
// --------------------------------------------------------------------
unsigned int _rfonts::reference=0;
_rfonts::_rfonts_map _rfonts::cm_fmap;
FT_Library _rfonts::library;

// --------------------------------------------------------------------
_rfonts::_rfonts()
{
    if (!reference)
    {
        if (FT_Init_FreeType( &library )) throw _error(0,"FT_Init_FreeType failed");
    }

    reference++;
}

// --------------------------------------------------------------------
_rfonts::~_rfonts()
{
    reference--;

    if (!reference)
    {
        _rfonts_map::iterator it=cm_fmap.begin();
        while(it!=cm_fmap.end())
        {
            delete it->second;
            it++;
        }

        cm_fmap.clear();

        FT_Done_FreeType(library);
    }
}

// --------------------------------------------------------------------
const _rfont *_rfonts::make_font_from_file(const string &file_name,const string &name,unsigned int char_bh)
{
    _rfonts_map::iterator it=cm_fmap.find(name);
    if (it!=cm_fmap.end()) throw _error(0,string("_rfonts::make_font_from_file - you already have font with name ")+name);

    _rfont *f=new _rfont(library,file_name,char_bh);
    cm_fmap[name]=f;

    return f;
}

// --------------------------------------------------------------------
_rfont *_rfonts::operator[](const string &name) const
{
    _rfonts_map::const_iterator it=cm_fmap.find(name);
    if (it!=cm_fmap.end()) return it->second;
    return NULL;
}

// --------------------------------------------------------------------
void _rfonts::free_font(const string &name)
{
    _rfonts_map::iterator it=cm_fmap.find(name);
    if (it!=cm_fmap.end())
    {
        delete it->second;
        cm_fmap.erase(it);
    }
}
