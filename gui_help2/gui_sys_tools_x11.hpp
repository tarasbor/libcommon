#ifndef __GUI_HELP_SYS_TOOLS_X11_LIB_H__
#define __GUI_HELP_SYS_TOOLS_X11_LIB_H__

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

namespace _gui_sys_tools
{
    Window find_window(const char *disp_name,const char *wname);
    bool make_window_borderless(const char *disp_name,const char *wname);
    bool make_window_topmost(const char *disp_name,const char *wname);

    bool resize_window(const char *disp_name,const char *wname,int nw,int nh);
};

#endif
