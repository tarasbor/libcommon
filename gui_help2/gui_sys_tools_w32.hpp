#ifndef __GUI_HELP_SYS_TOOLS_W32_LIB_H__
#define __GUI_HELP_SYS_TOOLS_W32_LIB_H__

#include <windows.h>

namespace _gui_sys_tools
{
    HWND find_window(const char *display_name,const char *wname);
    bool make_window_borderless(const char *display_name,const char *wname);
    bool make_window_topmost(const char *display_name,const char *wname);
};

#endif
