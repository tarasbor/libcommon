#include "ghelp_ui_btn_wnd.hpp"
//#include <clthread.hpp>
//#include <GL/gl.h>
//#include <ghelp_gl_font.hpp>
//#include <ghelp_gl_fun.hpp>

#include <error.hpp>

class _btn_wnd_imp:public _gh_ui::_btn_wnd
{
    private:

        _gh_ui::_btn_config cm_conf;

        bool is_pushed;

        virtual void render();

        virtual void on_create();
        virtual void on_delete();

    public:

        _btn_wnd_imp(_gh::_wnd *p,const _gh::_rect &r,const _gh_ui::_btn_config &conf);
        virtual ~_btn_wnd_imp();

        virtual bool on_mouse_event(int btn,int state,float x,float y);
};

// --------------------------------------------------------------
_btn_wnd_imp::_btn_wnd_imp(_gh::_wnd *p,const _gh::_rect &r,const _gh_ui::_btn_config &conf)
:_gh_ui::_btn_wnd(p,r),cm_conf(conf),is_pushed(false)
{
}

// --------------------------------------------------------------
void _btn_wnd_imp::on_create()
{
    try
    {
        if (!cm_conf.normal_bg.empty())
            _gh::_gl_props::set(string("text.")+cm_conf.normal_bg,new _gh::_gl_prop_texture(cm_conf.normal_bg.c_str()));

        if (!cm_conf.disabled_bg.empty())
            _gh::_gl_props::set(string("text.")+cm_conf.disabled_bg,new _gh::_gl_prop_texture(cm_conf.disabled_bg.c_str()));

        if (!cm_conf.pushed_bg.empty())
            _gh::_gl_props::set(string("text.")+cm_conf.pushed_bg,new _gh::_gl_prop_texture(cm_conf.pushed_bg.c_str()));

        _gh::_gl_props::set("color.white_for_ui_btn",new _gh::_gl_prop_color(1.0));
    }
    catch(_error &e)
    {
    }

    cm_conf.disabled_bg.insert(0,"text.");
    cm_conf.normal_bg.insert(0,"text.");
    cm_conf.pushed_bg.insert(0,"text.");
}

// --------------------------------------------------------------
_btn_wnd_imp::~_btn_wnd_imp()
{
}

// --------------------------------------------------------------
void _btn_wnd_imp::on_delete()
{
}

// ------------------------------------------------------------
void _btn_wnd_imp::render()
{
    _gh::_gl_props::select("color.white_for_ui_btn");
    glEnable(GL_TEXTURE_2D);

    _gh::_gl_props::select(cm_conf.normal_bg);
    if (is_enable())
    {
        if (is_pushed) _gh::_gl_props::select(cm_conf.pushed_bg);
    }
    else _gh::_gl_props::select(cm_conf.disabled_bg);

    glBegin(GL_QUADS);
        glTexCoord2f(0,0); glVertex2f(0,0);
        glTexCoord2f(1,0); glVertex2f(cm_rect.w,0);
        glTexCoord2f(1,1); glVertex2f(cm_rect.w,cm_rect.h);
        glTexCoord2f(0,1); glVertex2f(0,cm_rect.h);
    glEnd();
}

// ------------------------------------------------------------
bool _btn_wnd_imp::on_mouse_event(int btn,int state,float x,float y)
{
    if (btn==GLUT_LEFT_BUTTON)
    {
        if (state==GLUT_DOWN)
        {
            is_pushed=true;
            if (cm_action) cm_action->action();
            refresh();
        }
        else if (state==GLUT_UP)
        {
            is_pushed=false;
            refresh();
        }
    }

    return true;
}

// --------------------------------------------------------------
_gh_ui::_btn_wnd *_gh_ui::create_btn_window(_gh::_wnd *p,const _gh::_rect &r,const _gh_ui::_btn_config &conf)
{
    return new _btn_wnd_imp(p,r,conf);
}


