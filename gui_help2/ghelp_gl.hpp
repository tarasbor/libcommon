/*
 * File:   ghelp_gl.hpp
 * Author: paa
 *
 * Created on 12 Октябрь 2013 г., 18:23
 */

#ifndef __GHELP_GL_HPP__
#define	__GHELP_GL_HPP__

#include "ghelp.hpp"
#include <map>
#include <string>
//#include <glfw3.h>
#include <freeglut.h>
#include <clthread.hpp>
#include <iostream>

namespace _gh
{

// ----------------------------------------------------------------
// ----------------------------------------------------------------
// Base class for some window rendered via OpenGL
// ----------------------------------------------------------------
class _gl_wnd:public _wnd
{
    protected:

		_locker lock;
		unsigned long gl_th_id;

        virtual void render()=0; // MUST BE 0
        //virtual void on_mouse_event(int btn,int state,float x,float y); // MUST BE 0
        //virtual void on_mouse_move(float x,float y);  // MUST BE 0

        virtual void pre_render();

    public:
        _gl_wnd(_wnd *p,const _rect &r):_wnd(p,r) { };
        virtual ~_gl_wnd() {};
};

// ----------------------------------------------------------------
// ----------------------------------------------------------------
// Main window of app (via GLUT)
// ----------------------------------------------------------------
class _glut_screen:public _gl_wnd
{
    protected:
        //GLFWwindow *wnd_ptr;
		int wnd_id;

        int refresh_cnt;
        int render_at_refresh_cnt;
        //bool need_refresh;
        unsigned long create_th_id;

        pthread_mutex_t wait_mutex;
        pthread_cond_t wait_cond;

        /*
		static void stub_draw(GLFWwindow *wnd);
        static void stub_resize(GLFWwindow *wnd,int x,int y);
        static void stub_mouse_event(GLFWwindow *wnd,int btn,int state,int x,int y);
        static void stub_mouse_move(GLFWwindow *wnd,int x,int y);
        static void stub_mouse_entry(GLFWwindow *wnd,int state);
        static void stub_close(GLFWwindow *wnd);
        //static void repos_stub(int x,int y);
		*/
		static void stub_draw();
        static void stub_resize(int x,int y);
        static void stub_mouse_event(int btn,int state,int x,int y);
        static void stub_mouse_move(int x,int y);
        static void stub_active_mouse_move(int x,int y);
        static void stub_mouse_entry(int state);
        static void stub_keyboard(unsigned char key,int x,int y);
        static void stub_keyboard_special(int key,int x,int y);

        static void stub_close();
		static void stub_idle();

        virtual void render() = 0;
        virtual void pre_render();

        void draw_now();

    public:
        _glut_screen(const char *caption,const _rect &r,bool full_screen);
        virtual ~_glut_screen();

        virtual const _rect &resize(float new_w,float new_h) { glutReshapeWindow(new_w,new_h); cm_rect.w=new_w; cm_rect.h=new_h; if (cm_layout) cm_layout->resize(new_w,new_h); return cm_rect; };
        virtual const _rect &repos(float new_x,float new_y) { glutPositionWindow(new_x,new_y); cm_rect.x=new_x; cm_rect.y=new_y; return cm_rect; };
        virtual const _rect &resize_repos(const _rect &r) { glutPositionWindow(r.x,r.y); glutReshapeWindow(r.w,r.h); cm_rect=r; cm_rect.x=0; cm_rect.y=0; if (cm_layout) cm_layout->resize(r.w,r.h); return cm_rect; };

        virtual void on_close() = 0;

        virtual void refresh();
};

// ----------------------------------------------------------------
// ----------------------------------------------------------------
// Base class for OpenGL draw properties
// ----------------------------------------------------------------
class _gl_prop
{
    public:
        virtual ~_gl_prop() {}
        virtual void select() = 0;
};

// ----------------------------------------------------------------
// ----------------------------------------------------------------
// Property for color
// ----------------------------------------------------------------
class _gl_prop_color:public _gl_prop
{
    protected:
        float cm_r;
        float cm_g;
        float cm_b;
        float cm_alpha;
    public:
        _gl_prop_color(float l):cm_r(l),cm_g(l),cm_b(l),cm_alpha(1.0) {};
        _gl_prop_color(float r,float g,float b):cm_r(r),cm_g(g),cm_b(b),cm_alpha(1.0) {};
        _gl_prop_color(float r,float g,float b,float alpha):cm_r(r),cm_g(g),cm_b(b),cm_alpha(alpha) {};
        virtual ~_gl_prop_color() {};
        virtual void select();

        virtual void r(float s) {cm_r=s; };
        virtual void g(float s) {cm_g=s; };
        virtual void b(float s) {cm_b=s; };
        virtual void alpha(float s) {cm_alpha=s; };
};

class _gl_prop_color_byte:public _gl_prop_color
{
    public:
        _gl_prop_color_byte(unsigned char l):_gl_prop_color((float)l/255.0) {};
        _gl_prop_color_byte(unsigned char r,unsigned char g,unsigned char b):_gl_prop_color((float)r/255.0,(float)g/255.0,(float)b/255.0) {};
        _gl_prop_color_byte(unsigned char r,unsigned char g,unsigned char b,unsigned char alpha):_gl_prop_color((float)r/255.0,(float)g/255.0,(float)b/255.0,(float)alpha/255.0) {};
        virtual ~_gl_prop_color_byte() {};
};

// ----------------------------------------------------------------
// ----------------------------------------------------------------
// Property for line style
// ----------------------------------------------------------------
class _gl_prop_line:public _gl_prop
{
    protected:
        float cm_width;
        int cm_factor;
        unsigned short cm_pattern;
    public:
        _gl_prop_line():cm_width(1.0),cm_factor(1),cm_pattern(0xFFFF) {};
        _gl_prop_line(float width):cm_width(width),cm_factor(1),cm_pattern(0xFFFF) {};
        _gl_prop_line(float width,int factor,unsigned short pattern):cm_width(width),cm_factor(factor),cm_pattern(pattern) {};
        virtual ~_gl_prop_line() {};
        virtual void select();
};

// ----------------------------------------------------------------
// ----------------------------------------------------------------
// Property for image
// ----------------------------------------------------------------
class _gl_prop_image:public _gl_prop
{
	protected:
		static int devil_ref;

		unsigned int id;

		void init();

	public:
		_gl_prop_image(const char *fname);
		_gl_prop_image(const char *data,const char *type,unsigned int size);
		virtual ~_gl_prop_image();
		virtual void select();
};

// ----------------------------------------------------------------
// ----------------------------------------------------------------
// Property for texture
// ----------------------------------------------------------------
class _gl_prop_texture:public _gl_prop
{
	protected:
		unsigned int id;
		unsigned int cm_w;
		unsigned int cm_h;

	public:
        _gl_prop_texture(const char *fname);
        _gl_prop_texture(const char *data,const char *type,unsigned int size);
		virtual ~_gl_prop_texture();
		virtual void select();
		virtual unsigned int w() { return cm_w; };
		virtual unsigned int h() { return cm_h; };
};

// ----------------------------------------------------------------
// ----------------------------------------------------------------
// Global MAP of all props
// ----------------------------------------------------------------
class _gl_props
{
    protected:
        typedef map<string,_gl_prop *> _props_map;

        static _props_map cm_props_map;
        static int ref_count;

    public:
        _gl_props();
        ~_gl_props();

        static void delete_all();
        static bool set(const string &n,_gl_prop *p);
        static bool remove(const string &n);
        static _gl_prop *select(const string &n);
};

};

#endif	/* GHELP_GL_HPP */

