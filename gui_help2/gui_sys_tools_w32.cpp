#include "gui_sys_tools_w32.hpp"

// CLIENT FUNC
// -----------------------------------------------------
HWND _gui_sys_tools::find_window(const char *display_name,const char *wname)
{
    // TODO - find in childs (now search only for main windows)
    HWND w=FindWindow(NULL,wname);
    return w;
}

// -----------------------------------------------------
bool _gui_sys_tools::make_window_borderless(const char *display_name,const char *wname)
{
    HWND w=_gui_sys_tools::find_window(display_name,wname);
    if (w==NULL) return false;

    LONG l=GetWindowLong(w,GWL_STYLE);
    l^=WS_CAPTION;
    l^=WS_THICKFRAME;
    SetWindowLong(w,GWL_STYLE,l);

    return true;
}

// -----------------------------------------------------
bool _gui_sys_tools::make_window_topmost(const char *display_name,const char *wname)
{
    HWND w=_gui_sys_tools::find_window(display_name,wname);
    if (w==NULL) return false;

    ShowWindow(w,SW_SHOW); // may be SetWindowPos with TOP_MOST ???
    return true;
}

