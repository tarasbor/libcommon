/*
 * File:   ghelp_gl_fun.hpp
 * Author: paa
 *
 * Created on 12 2013 Рі., 18:23
 */

#ifndef __GHELP_GL_FUN_HPP__
#define	__GHELP_GL_FUN_HPP__

#include "ghelp_gl.hpp"
#include "ghelp_gl_font.hpp"

namespace _gh
{
    void draw_text_utf8(const string &text,float x,float y,float h,const string &font_name);
    unsigned int draw_text_utf8_max_width(const string &text,float x,float y,float h,const string &font_name,float max_w,float dy);
    float calc_width_text_utf8(const string &text,float h,const string &font_name);

    void draw_simple_text_rect(const string &text_name,const _gh::_rect &r);
    void draw_multy_text_rect(const string &text_name,const _gh::_rect &r,const _gh::_rect &text_div,float text_w,float text_h);
};

#endif

