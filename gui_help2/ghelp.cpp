#include "ghelp.hpp"
#include <clthread.hpp>
#include <text_help.hpp>

#include <log.hpp>
#define MLOG(L) _log() << _log::_set_module_level("prog",(L))

using namespace _gh;

unsigned long _gl_context::gl_thread_id = 0;

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// Layout Horizontal box
// -------------------------------------------------------------------------
void _layout_h_box::add(_layout* i)
{
    cm_items.push_back(i);
    cm_w+=i->w();
    if (cm_h < i->h()) cm_h=i->h();
    if (i->v_style()==_layout::grow) cm_v_style=_layout::grow;
    if (i->h_style()==_layout::grow) cm_h_style=_layout::grow; else cm_fix_w+=i->w();
}

// ---------------------------------------------------------------------------
void _layout_h_box::clear()
{
    _layout_items::iterator it=cm_items.begin();
    while(it!=cm_items.end())
    {
        delete (*it);
        it++;
    }
    cm_items.clear();

    cm_w=cm_fix_w=cm_h=0;
}

// ---------------------------------------------------------------------------
void _layout_h_box::resize(float new_w,float new_h)
{
    if (cm_v_style==_layout::grow) cm_h=new_h;

    float scale=(new_w-cm_fix_w)/(cm_w-cm_fix_w);

    float x_pos=cm_x;

    _layout_items::iterator it=cm_items.begin();
    while(it!=cm_items.end())
    {
        (*it)->repos(x_pos,(*it)->y());
        (*it)->resize((*it)->w()*scale,cm_h);

        x_pos+=(*it)->w();
        it++;
    }

    cm_w=new_w;
}

// ---------------------------------------------------------------------------
void _layout_h_box::repos(float new_x,float new_y)
{
    float dx=new_x-cm_x;
    float dy=new_y-cm_y;

    _layout_items::iterator it=cm_items.begin();
    while(it!=cm_items.end())
    {
        (*it)->repos((*it)->x()+dx,(*it)->y()+dy);
        it++;
    }

    cm_x=new_x;
    cm_y=new_y;
}

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// Layout Vertical box
// -------------------------------------------------------------------------
void _layout_v_box::add(_layout* i)
{
    cm_items.push_back(i);
    cm_h+=i->h();
    if (cm_w < i->w()) cm_w=i->w();
    if (i->h_style()==_layout::grow) cm_h_style=_layout::grow;
    if (i->v_style()==_layout::grow) cm_v_style=_layout::grow; else cm_fix_h+=i->h();
}

// ---------------------------------------------------------------------------
void _layout_v_box::clear()
{
    _layout_items::iterator it=cm_items.begin();
    while(it!=cm_items.end())
    {
        delete (*it);
        it++;
    }
    cm_items.clear();

    cm_w=cm_fix_h=cm_h=0;
}

// ---------------------------------------------------------------------------
void _layout_v_box::resize(float new_w,float new_h)
{
    if (cm_h_style==_layout::grow) cm_w=new_w;

    float scale=(new_h-cm_fix_h)/(cm_h-cm_fix_h);

    float y_pos=cm_y;

    _layout_items::iterator it=cm_items.begin();
    while(it!=cm_items.end())
    {
        (*it)->repos((*it)->x(),y_pos);
        (*it)->resize(cm_w,(*it)->h()*scale);

        y_pos+=(*it)->h();
        it++;
    }

    cm_h=new_h;
}

// ---------------------------------------------------------------------------
void _layout_v_box::repos(float new_x,float new_y)
{
    float dx=new_x-cm_x;
    float dy=new_y-cm_y;

    _layout_items::iterator it=cm_items.begin();
    while(it!=cm_items.end())
    {
        (*it)->repos((*it)->x()+dx,(*it)->y()+dy);
        it++;
    }

    cm_x=new_x;
    cm_y=new_y;
}


// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// Some window
// -------------------------------------------------------------------------
_wnd::_wnd(_wnd* p, const _rect& r)
:cm_rect(r),cm_layout(NULL),cm_parent(p),cm_state(p?p->cm_state:_state())
{
	cm_state.need_create=true;
	cm_state.was_created=false;
	cm_state.need_delete=false;
	cm_state.was_deleted=false;
	cm_state.have_focus=false;

    if (p)
    {
        p->cm_childs.push_front(this);
    }
}

// ----------------------------------------------------------------
_wnd::~_wnd()
{
}

// ----------------------------------------------------------------
void _wnd::release()
{
    /*
    _time tn1;
    tn1.set_now();
    cout << tn1.stamp() << " wnd::release 1 thread=" << get_current_thread_id() << " y=" << cm_rect.y << endl;
    */

    // at first - we need release childs
	while(!cm_childs.empty())
    {
        (*cm_childs.begin())->release();
    }

    // second - we need mark itself as need deleted (to prevent render & childs render)
    cm_state.need_delete=true;

    /*_time tn2;
    tn2.set_now();
    cout << tn2.stamp() << " wnd::release 2 call refresh thread=" << get_current_thread_id() << " y=" << cm_rect.y << " " << tn2.stamp()-tn1.stamp() << endl;
    */

    if (get_current_thread_id()==_gl_context::gl_thread_id)
    {
        pre_render();
    }
    else
    {
        refresh();
    }
    //pre_render();

    /*
    _time tn3;
    tn3.set_now();
    cout << tn3.stamp() << " wnd::release 3 call refresh done thread=" << get_current_thread_id() << " y=" << cm_rect.y << " " << cm_state.was_deleted << " " << tn3.stamp()-tn2.stamp() << endl;
    */

    int cnt=0;
	while(1)
    {
        if (cm_state.was_deleted) break;
        refresh();
        Sleep(1);
        cnt++;
        if (cnt==100000)
        {
            MLOG(0) << "_wnd::release - cnt > 100000 but .was_deleted=false" << endl;
            this->on_delete();
            cm_state.need_delete=false;
            cm_state.was_deleted=true;
        }
    }

    /*
    _time tn4;
    tn4.set_now();
    cout << tn4.stamp() << " wnd::release 4 wait done thread=" << get_current_thread_id() << " y=" << cm_rect.y << " cnt=" << cnt << " " << tn4.stamp()-tn3.stamp() << " all=" << tn4.stamp()-tn1.stamp() << endl;
    */

    /*
    if (cm_parent)
    {
        _childs::iterator it=cm_parent->cm_childs.begin();
        while(it!=cm_childs.end())
        {
             if ((*it)==this)
             {
                 cm_parent->cm_childs.erase(it);
                 break;
             }
             it++;
        }
    }
    */

	delete this;
}

/*
// ----------------------------------------------------------------
_wnd *_wnd::root()
{
    _wnd *r=this;
    while(r->cm_parent) r=cm_parent;
    return r;
}

// ----------------------------------------------------------------
void _wnd::refresh()
{
    _wnd *r=this;
    while(r->cm_parent) r=cm_parent;
    if (r!=this) r->refresh();
}
*/

#include <iostream>

// ----------------------------------------------------------------
void _wnd::pre_render()
{
    //cout << "Pre render thread=" << get_current_thread_id() << " y=" << cm_rect.y << endl;

	if (cm_state.need_create)
	{
		this->on_create();
		cm_state.need_create=false;
		cm_state.was_created=true;
	}

	if (cm_state.need_delete)
	{
        /*
        _time tn1;
        tn1.set_now();
        cout << tn1.stamp() << " Pre render 1 need_delete=true" << endl;
        */

		this->on_delete();
		cm_state.need_delete=false;
		cm_state.was_deleted=true;

        /*
		_time tn2;
        tn2.set_now();
        cout << tn2.stamp() << " Pre render 2 was_deleted=true " << tn2.stamp()-tn1.stamp() << endl;
        */

        // ATTENTION!!!!
        // we need pre_render childs (to call on_delete() for them
        _childs::reverse_iterator it=cm_childs.rbegin();
        while(it!=cm_childs.rend())
        {
            (*it)->pre_render();
            it++;
        }

        /*
        _time tn3;
        tn3.set_now();
        cout << tn3.stamp() << " Pre render 3 childs pre_render done " << tn3.stamp()-tn2.stamp() << endl;
        */

        if (cm_parent)
        {
            _childs::iterator pit=cm_parent->cm_childs.begin();
            while(pit!=cm_childs.end())
            {
                if ((*pit)==this)
                {
                    cm_parent->cm_childs.erase(pit);
                    break;
                }
                pit++;
            }
        }

        /*
        _time tn4;
        tn4.set_now();
        cout << tn4.stamp() << " Pre render 4 parent erase done done" << tn4.stamp()-tn3.stamp() << " all=" << tn4.stamp()-tn1.stamp() << endl;
        */

		return;
	}

    if (!cm_state.is_visible) return;

    render();

    _childs::reverse_iterator it=cm_childs.rbegin();
    while(it!=cm_childs.rend())
    {
        (*it)->pre_render();
        it++;
    }
}

// ----------------------------------------------------------------
void _wnd::show(bool make_top)
{
    cm_state.is_visible=true;
    if (make_top && cm_parent)
    {
        _childs::iterator it=cm_parent->cm_childs.begin();
        while(it!=cm_parent->cm_childs.end())
        {
            if ((*it)==this)
            {
                cm_parent->cm_childs.erase(it);
                cm_parent->cm_childs.push_front(this);
                break;
            }
            it++;
        }
    }
    //refresh();
    //root()->update();
}

// ----------------------------------------------------------------
void _wnd::hide()
{
    cm_state.is_visible=false;
    //refresh();
    //root()->update();
}

// ----------------------------------------------------------------
void _wnd::enable()
{
    cm_state.is_enable=true;

    _childs::iterator it=cm_childs.begin();
    while(it!=cm_childs.end())
    {
        (*it)->enable();
        it++;
    }
    //refresh();
    //root()->update();
}

// ----------------------------------------------------------------
void _wnd::disable()
{
    cm_state.is_enable=false;

    _childs::iterator it=cm_childs.begin();
    while(it!=cm_childs.end())
    {
        (*it)->disable();
        it++;
    }
    //refresh();
    //root()->update();
}

// ----------------------------------------------------------------
bool _wnd::proc_mouse_event(int btn, int state, float x, float y)
{
    //if (!cm_state.is_visible) return;
    //if (!cm_state.is_enable) return;

    _childs::iterator it=cm_childs.begin();
    while(it!=cm_childs.end())
    {
        if ((*it)->cm_state.is_visible && (*it)->cm_state.is_enable)
        {
            float wx=x-(*it)->rect().x;
            float wy=y-(*it)->rect().y;
            if (wx>=0 && wy>=0 && wx<=(*it)->rect().w && wy<=(*it)->rect().h)
            {
                _wnd *w=(*it);
                cm_childs.erase(it);
                cm_childs.push_front(w);

                if (w->proc_mouse_event(btn,state,wx,wy)) return true;

                return false;
            }
        }
        it++;
    }

    on_mouse_event(btn,state,x,y);
    return true;
}

// ----------------------------------------------------------------
bool _wnd::proc_mouse_move(float x, float y)
{
    //if (!cm_state.is_visible) return;
    //if (!cm_state.is_enable) return;

    _childs::iterator it=cm_childs.begin();
    while(it!=cm_childs.end())
    {
        if ((*it)->cm_state.is_visible && (*it)->cm_state.is_enable)
        {
            float wx=x-(*it)->rect().x;
            float wy=y-(*it)->rect().y;
            if (wx>=0 && wy>=0 && wx<=(*it)->rect().w && wy<=(*it)->rect().h)
            {
                if ((*it)->proc_mouse_move(wx,wy)) return true;
                return false;
            }
        }
        it++;
    }

    return on_mouse_move(x,y);
}

// ----------------------------------------------------------------
void _wnd::proc_active_mouse_move(float x, float y)
{
    //if (!cm_state.is_visible) return;
    //if (!cm_state.is_enable) return;

    _childs::iterator it=cm_childs.begin();
    while(it!=cm_childs.end())
    {
        if ((*it)->cm_state.is_visible && (*it)->cm_state.is_enable)
        {
            float wx=x-(*it)->rect().x;
            float wy=y-(*it)->rect().y;
            if (wx>=0 && wy>=0 && wx<=(*it)->rect().w && wy<=(*it)->rect().h)
            {
                (*it)->proc_active_mouse_move(wx,wy);
                return;
            }
        }
        it++;
    }

    on_active_mouse_move(x,y);
}

// ----------------------------------------------------------------
bool _wnd::proc_keyboard(unsigned char key,float x,float y)
{
    if (cm_state.have_focus)
    {
        on_keyboard(key,x,y);
        return true;
    }

    _childs::iterator it=cm_childs.begin();
    while(it!=cm_childs.end())
    {
        if ((*it)->proc_keyboard(key,x,y)) return true;
        it++;
    }

    return false;
}

// ----------------------------------------------------------------
bool _wnd::proc_keyboard_special(int key,float x,float y)
{
    if (cm_state.have_focus)
    {
        return on_keyboard_special(key,x,y);
    }

    _childs::iterator it=cm_childs.begin();
    while(it!=cm_childs.end())
    {
        if ((*it)->proc_keyboard_special(key,x,y)) return true;
        it++;
    }

    return false;
}


// ----------------------------------------------------------------
void _wnd::set_focus()
{
    if (cm_state.have_focus) return;

    root()->clear_focus();

    on_set_focus();
    cm_state.have_focus=true;
}

// ----------------------------------------------------------------
void _wnd::clear_focus()
{
    if (cm_state.have_focus)
    {
        on_clear_focus();
        cm_state.have_focus=false;
        return;
    }

    _childs::iterator it=cm_childs.begin();
    while(it!=cm_childs.end())
    {
        (*it)->clear_focus();
        it++;
    }
}


// ----------------------------------------------------------------
// ----------------------------------------------------------------
string _gh::_wnd_signal::operator[](const string &param) const
{
    return _text_help::get_param_value_from_st(cm_data,param);
}

