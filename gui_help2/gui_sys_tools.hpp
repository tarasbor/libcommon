#ifndef __GUI_HELP_SYS_TOOLS_LIB_H__
#define __GUI_HELP_SYS_TOOLS_LIB_H__

#ifdef WINDOWS
#include "gui_sys_tools_w32.hpp"
#else
#include "gui_sys_tools_x11.hpp"
#endif

#endif
