/*
 * File:   rfont.hpp
 * Author: paa
 *
 * Created on 28 09 2013, 23:54
 */

#ifndef __GHELP_GL_FONT_HPP__
#define	__GHELP_GL_FONT_HPP__

#include <map>
#include <string>

#include <ft2build.h>
#include FT_FREETYPE_H

using namespace std;

namespace _gh
{

// --------------------------------------------------------------------
// One char
// --------------------------------------------------------------------
class _rchar
{
private:

    struct _char
    {
        unsigned int id;
        unsigned int w;
        unsigned int h;
        unsigned int advice_w;
        unsigned int advice_h;
    } cm_char;

    struct _text
    {
        unsigned int id;
        unsigned int w;
        unsigned int h;
    } cm_text;

    unsigned int cm_list_id;

public:

    _rchar(FT_Face &face,unsigned int code);
    ~_rchar();

    void out() const;
    unsigned int advice_w() const { return cm_char.advice_w; };
    unsigned int advice_h() const { return cm_char.advice_h; };
};

// --------------------------------------------------------------------
// Set of chars for one font
// --------------------------------------------------------------------
class _rfont
{
friend class _rfonts;

private:

    FT_Face ft_face;

    typedef map<unsigned int,_rchar *> _rchars_map;
    _rchars_map cm_cmap;

    unsigned int cm_char_h;

protected:

    _rfont(FT_Library &lib,const string &file_name,unsigned int h);
    ~_rfont();

public:

    const _rchar *operator[](unsigned int i);
    unsigned int char_base_h() { return cm_char_h; };
};

// --------------------------------------------------------------------
// Set of fonts
// --------------------------------------------------------------------
class _rfonts
{
private:

    static FT_Library library;

    static unsigned int reference;

    typedef map<string,_rfont *> _rfonts_map;
    static _rfonts_map cm_fmap;

public:

    _rfonts();
    ~_rfonts();

    const _rfont *make_font_from_file(const string &file_name,const string &name,unsigned int char_bh);
    _rfont *operator[](const string &name) const;
    void free_font(const string &name);

};

};

#endif

