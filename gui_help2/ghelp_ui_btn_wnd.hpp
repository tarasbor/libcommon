#ifndef __GHELP_GL_UI_BTN_HPP__
#define __GHELP_GL_UI_BTN_HPP__

#include <string>
#include <ghelp_gl.hpp>

namespace _gh_ui
{
    class _btn_wnd;

// --------------------------------------------------------
    class _btn_action
    {
        protected:
            _btn_wnd *cm_btn_wnd;

        public:
            _btn_action(_btn_wnd *w):cm_btn_wnd(w) {};
            virtual ~_btn_action() {};

            virtual void action() = 0;
    };

// --------------------------------------------------------
    struct _btn_config
    {
        string normal_bg;
        string pushed_bg;
        string disabled_bg;
    };

// --------------------------------------------------------
    class _btn_wnd:public _gh::_gl_wnd
    {
        protected:
            _btn_action *cm_action;

        public:
            _btn_wnd(_gh::_wnd *p,const _gh::_rect &r):_gh::_gl_wnd(p,r),cm_action(NULL) {};
            virtual ~_btn_wnd() {};

            virtual void set_action(_btn_action *a) { cm_action=a; };
            virtual _btn_action *get_action() const { return cm_action; };
    };

// --------------------------------------------------------
    _btn_wnd *create_btn_window(_gh::_wnd *p,const _gh::_rect &r,const _btn_config &conf);
}

#endif

