#include "ghelp_gl_fun.hpp"

#include <codepage.hpp>
#include <iostream>

using namespace _gh;

#ifdef UNICODE
#define CONVERT_UTF8_SYS_CHARSET(A)     utf8_to_ucode((A),(B),(C))
#else
#define CONVERT_UTF8_SYS_CHARSET(A,B,C)     utf8_to_ucode((A),(B),(C))
//utf8_to_win1251((A),(B),(C))
#endif

// ---------------------------------------------------------------------------------------
unsigned int utf8_to_win1251(const string &text,unsigned int i,unsigned int *code)
{
    unsigned char c=text[i];
    if (c <= 0x7F)
    {
        *code=text[i];
        return 1;
    }
    else if (c <= 0xDF)
    {
        *code=_codepage::unicode_to_win1251(((text[i] & 0x1F) << 6) | (text[i+1] & 0x3F));
        return 2;
    }
    else if ( c <= 0xEF)
    {
        *code=_codepage::unicode_to_win1251((((text[i] & 0x0F) << 5) | ((text[i+1] & 0x3F) << 2) << 2) | (text[i+2] & 0x3F));
        return 3;
    }
    else if ( c <= 0xF7)
    {
        *code=_codepage::unicode_to_win1251(((((text[i] & 0x0F) << 4) | ((text[i+1] & 0x3F) << 2) << 2) | (text[i+2] & 0x3F) << 2) | (text[i+3] & 0x3F));
        return 4;
    }

    return 0;
}

// ---------------------------------------------------------------------------------------
unsigned int utf8_to_ucode(const string &text,unsigned int i,unsigned int *code)
{
    unsigned char c=text[i];
    if (c <= 0x7F)
    {
        *code=text[i];
        return 1;
    }
    else if (c <= 0xDF)
    {
        *code=((text[i] & 0x1F) << 6) | (text[i+1] & 0x3F);
        return 2;
    }
    else if ( c <= 0xEF)
    {
        *code=(((text[i] & 0x0F) << 5) | ((text[i+1] & 0x3F) << 2) << 2) | (text[i+2] & 0x3F);
        return 3;
    }
    else if ( c <= 0xF7)
    {
        *code=((((text[i] & 0x0F) << 4) | ((text[i+1] & 0x3F) << 2) << 2) | (text[i+2] & 0x3F) << 2) | (text[i+3] & 0x3F);
        return 4;
    }

    return 0;
}

// ---------------------------------------------------------------------------------------
void _gh::draw_text_utf8(const string &text,float x,float y,float h,const string &font_name)
{
    //cout << text << " len=" << text.length() << endl;
    _rfont *fnt=_rfonts()[font_name];
    if (!fnt) return;

    float scale=(float)h/fnt->char_base_h();

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);

    glPushMatrix();

    glTranslatef(x,y,0);
    glScalef(scale,scale,1.0);

    // we must convert UTF8 char(s) to unicode code & out simbol for this code
    unsigned int i=0;
    while(i<text.length())
    {
        unsigned int code;
        i+=CONVERT_UTF8_SYS_CHARSET(text,i,&code);
        fnt->operator [](code)->out();
    }

    glPopMatrix();
}

// ---------------------------------------------------------------------------------------
unsigned int _gh::draw_text_utf8_max_width(const string &text,float x,float y,float h,const string &font_name,float max_w,float dy)
{
    unsigned int l_cnt=1;

    //cout << text << " len=" << text.length() << endl;
    _rfont *fnt=_rfonts()[font_name];
    if (!fnt) return 0;

    float scale=(float)h/fnt->char_base_h();

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);

    glPushMatrix();

    glTranslatef(x,y,0);
    glScalef(scale,scale,1.0);

    unsigned int max_advice_line_w=max_w*fnt->char_base_h()/h;
    unsigned int line_w=0;

    // we must convert UTF8 char(s) to unicode code & out simbol for this code
    unsigned int i=0;
    while(i<text.length())
    {
        unsigned int code;
        i+=CONVERT_UTF8_SYS_CHARSET(text,i,&code);

        if (code==' ')
        {
            int j=i;
            unsigned int line_w_j=0;
            while(j<text.length())
            {
                unsigned int code_j;
                j+=CONVERT_UTF8_SYS_CHARSET(text,j,&code_j);

                if (code_j==' ') break;

                line_w_j+=fnt->operator [](code_j)->advice_w();
            }

            if (line_w+line_w_j>max_advice_line_w)
            {
                line_w=0;

                glPopMatrix();
                glPushMatrix();

                y+=dy;
                l_cnt++;

                glTranslatef(x,y,0);
                glScalef(scale,scale,1.0);
                continue;
            }
        }

        line_w+=fnt->operator [](code)->advice_w();
        fnt->operator [](code)->out();
    }

    glPopMatrix();

    return l_cnt;
}


// ---------------------------------------------------------------------------------------
float _gh::calc_width_text_utf8(const string &text,float h,const string &font_name)
{
    _rfont *fnt=_rfonts()[font_name];
    if (!fnt) return 0.0;

    float res=0.0;

    unsigned int i=0;
    while(i<text.length())
    {
        unsigned int code;
        i+=CONVERT_UTF8_SYS_CHARSET(text,i,&code);
        res+=fnt->operator [](code)->advice_w();
    }

    return (float)res*h/fnt->char_base_h();
}

// ---------------------------------------------------------------------------------------
void _gh::draw_simple_text_rect(const string &text_name,const _gh::_rect &r)
{
    _gh::_gl_props::select(text_name);

    glBegin(GL_QUADS);
        glTexCoord2f(0.0, 0.0); glVertex2f(r.x,r.y);
        glTexCoord2f(1.0, 0.0); glVertex2f(r.x+r.w,r.y);
        glTexCoord2f(1.0, 1.0); glVertex2f(r.x+r.w,r.y+r.h);
        glTexCoord2f(0.0, 1.0); glVertex2f(r.x,r.y+r.h);
    glEnd();
}

// ---------------------------------------------------------------------------------------
void _gh::draw_multy_text_rect(const string &text_name,const _gh::_rect &r,const _gh::_rect &text_div,float text_w,float text_h)
{
    if (text_div.x==0 && text_div.y==0 && text_div.w==text_w && text_div.h==text_h)
    {
        draw_simple_text_rect(text_name,r);
        return;
    }

    _gh::_gl_props::select(text_name);

    glBegin(GL_QUADS);
    //glBegin(GL_LINE_LOOP);

        // X 0 0
        // 0 0 0
        // 0 0 0
        _gh::_rect pix(r.x, r.y, r.x+text_div.x    , r.y+text_div.y);
        _gh::_rect tex(0.0, 0.0, text_div.x/text_w , text_div.y/text_h);

        glTexCoord2f(tex.x,tex.y); glVertex2f(pix.x, pix.y);
        glTexCoord2f(tex.w,tex.y); glVertex2f(pix.w, pix.y);
        glTexCoord2f(tex.w,tex.h); glVertex2f(pix.w, pix.h);
        glTexCoord2f(tex.x,tex.h); glVertex2f(pix.x, pix.h);

        // 0 X 0
        // 0 0 0
        // 0 0 0
        pix=_gh::_rect(r.x+text_div.x    ,r.y, r.x+r.w-(text_w-text_div.w), r.y+text_div.y);
        tex=_gh::_rect(text_div.x/text_w ,0.0, text_div.w/text_w          , text_div.y/text_h);

        glTexCoord2f(tex.x,tex.y); glVertex2f(pix.x, pix.y);
        glTexCoord2f(tex.w,tex.y); glVertex2f(pix.w, pix.y);
        glTexCoord2f(tex.w,tex.h); glVertex2f(pix.w, pix.h);
        glTexCoord2f(tex.x,tex.h); glVertex2f(pix.x, pix.h);

        // 0 0 X
        // 0 0 0
        // 0 0 0
        pix=_gh::_rect(r.x+r.w-(text_w-text_div.w), r.y, r.x+r.w, r.y+text_div.y);
        tex=_gh::_rect(text_div.w/text_w          , 0.0, 1.0    , text_div.y/text_h);

        glTexCoord2f(tex.x,tex.y); glVertex2f(pix.x, pix.y);
        glTexCoord2f(tex.w,tex.y); glVertex2f(pix.w, pix.y);
        glTexCoord2f(tex.w,tex.h); glVertex2f(pix.w, pix.h);
        glTexCoord2f(tex.x,tex.h); glVertex2f(pix.x, pix.h);

        // ------------------------------------------------------------

        // 0 0 0
        // X 0 0
        // 0 0 0
        pix=_gh::_rect(r.x, r.y+text_div.y   , r.x+text_div.x    , r.y+r.h-(text_h-text_div.h));
        tex=_gh::_rect(0.0, text_div.y/text_h, text_div.x/text_w , text_div.w/text_h);

        glTexCoord2f(tex.x,tex.y); glVertex2f(pix.x, pix.y);
        glTexCoord2f(tex.w,tex.y); glVertex2f(pix.w, pix.y);
        glTexCoord2f(tex.w,tex.h); glVertex2f(pix.w, pix.h);
        glTexCoord2f(tex.x,tex.h); glVertex2f(pix.x, pix.h);

        // 0 0 0
        // 0 X 0
        // 0 0 0
        pix=_gh::_rect(r.x+text_div.x    ,r.y+text_div.y   , r.x+r.w-(text_w-text_div.w), r.y+r.h-(text_h-text_div.h));
        tex=_gh::_rect(text_div.x/text_w ,text_div.y/text_h, text_div.w/text_w          , text_div.w/text_h);

        glTexCoord2f(tex.x,tex.y); glVertex2f(pix.x, pix.y);
        glTexCoord2f(tex.w,tex.y); glVertex2f(pix.w, pix.y);
        glTexCoord2f(tex.w,tex.h); glVertex2f(pix.w, pix.h);
        glTexCoord2f(tex.x,tex.h); glVertex2f(pix.x, pix.h);

        // 0 0 0
        // 0 0 X
        // 0 0 0
        pix=_gh::_rect(r.x+r.w-(text_w-text_div.w), r.y+text_div.y   , r.x+r.w, r.y+r.h-(text_h-text_div.h));
        tex=_gh::_rect(text_div.w/text_w          , text_div.y/text_h, 1.0    , text_div.w/text_h);

        glTexCoord2f(tex.x,tex.y); glVertex2f(pix.x, pix.y);
        glTexCoord2f(tex.w,tex.y); glVertex2f(pix.w, pix.y);
        glTexCoord2f(tex.w,tex.h); glVertex2f(pix.w, pix.h);
        glTexCoord2f(tex.x,tex.h); glVertex2f(pix.x, pix.h);

        // ------------------------------------------------------------

        // 0 0 0
        // 0 0 0
        // X 0 0
        pix=_gh::_rect(r.x, r.y+r.h-(text_h-text_div.h), r.x+text_div.x    , r.y+r.h);
        tex=_gh::_rect(0.0, text_div.w/text_h          , text_div.x/text_w , 1.0);

        glTexCoord2f(tex.x,tex.y); glVertex2f(pix.x, pix.y);
        glTexCoord2f(tex.w,tex.y); glVertex2f(pix.w, pix.y);
        glTexCoord2f(tex.w,tex.h); glVertex2f(pix.w, pix.h);
        glTexCoord2f(tex.x,tex.h); glVertex2f(pix.x, pix.h);

        // 0 0 0
        // 0 0 0
        // 0 X 0
        pix=_gh::_rect(r.x+text_div.x    ,r.y+r.h-(text_h-text_div.h), r.x+r.w-(text_w-text_div.w), r.y+r.h);
        tex=_gh::_rect(text_div.x/text_w ,text_div.w/text_h          ,text_div.w/text_w           , 1.0);

        glTexCoord2f(tex.x,tex.y); glVertex2f(pix.x, pix.y);
        glTexCoord2f(tex.w,tex.y); glVertex2f(pix.w, pix.y);
        glTexCoord2f(tex.w,tex.h); glVertex2f(pix.w, pix.h);
        glTexCoord2f(tex.x,tex.h); glVertex2f(pix.x, pix.h);

        // 0 0 0
        // 0 0 0
        // 0 0 X
        pix=_gh::_rect(r.x+r.w-(text_w-text_div.w), r.y+r.h-(text_h-text_div.h), r.x+r.w, r.y+r.h);
        tex=_gh::_rect(text_div.w/text_w          , text_div.w/text_h          , 1.0    ,1.0);

        glTexCoord2f(tex.x,tex.y); glVertex2f(pix.x, pix.y);
        glTexCoord2f(tex.w,tex.y); glVertex2f(pix.w, pix.y);
        glTexCoord2f(tex.w,tex.h); glVertex2f(pix.w, pix.h);
        glTexCoord2f(tex.x,tex.h); glVertex2f(pix.x, pix.h);

    glEnd();
}

