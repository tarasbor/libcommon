#include "gui_sys_tools_x11.hpp"


struct MwmHints {
    unsigned long flags;
    unsigned long functions;
    unsigned long decorations;
    long input_mode;
    unsigned long status;
};

enum {
    MWM_HINTS_FUNCTIONS = (1L << 0),
    MWM_HINTS_DECORATIONS =  (1L << 1),

    MWM_FUNC_ALL = (1L << 0),
    MWM_FUNC_RESIZE = (1L << 1),
    MWM_FUNC_MOVE = (1L << 2),
    MWM_FUNC_MINIMIZE = (1L << 3),
    MWM_FUNC_MAXIMIZE = (1L << 4),
    MWM_FUNC_CLOSE = (1L << 5)
};

#define _NET_WM_STATE_REMOVE        0    /* remove/unset property */
#define _NET_WM_STATE_ADD           1    /* add/set property */
#define _NET_WM_STATE_TOGGLE        2    /* toggle property  */

// INTERNAL FUNC
// -----------------------------------------------------
Window rec_find_windows_by_name(Display* display, Window window, const char *cmp_name)
{
    int i;

    XTextProperty text;
    XGetWMName(display, window, &text);
    char* name;
    XFetchName(display, window, &name);

    if (name && strcmp(name,cmp_name)==0) return window;
    if (text.value && strcmp((char *)text.value,cmp_name)==0) return window;

    Window res=0;

    Window root, parent;
    Window* children;
    unsigned int n;
    XQueryTree(display, window, &root, &parent, &children, &n);
    if (children != NULL)
    {
        for (i=0; i<n; i++)
        {
            res=rec_find_windows_by_name(display, children[i], cmp_name);
            if (res) break;
        }
        XFree(children);
    }

    return res;
}

// -----------------------------------------------------------------
int client_msg(Display *disp, Window win, char *msg,
        unsigned long data0, unsigned long data1,
        unsigned long data2, unsigned long data3,
        unsigned long data4)
{
    XEvent event;
    long mask = SubstructureRedirectMask | SubstructureNotifyMask;

    event.xclient.type = ClientMessage;
    event.xclient.serial = 0;
    event.xclient.send_event = True;
    event.xclient.message_type = XInternAtom(disp, msg, False);
    event.xclient.window = win;
    event.xclient.format = 32;
    event.xclient.data.l[0] = data0;
    event.xclient.data.l[1] = data1;
    event.xclient.data.l[2] = data2;
    event.xclient.data.l[3] = data3;
    event.xclient.data.l[4] = data4;

    if (XSendEvent(disp, DefaultRootWindow(disp), False, mask, &event)) return 0;

    return -1;
}

// -----------------------------------------------------------------
int window_state(Display *disp, Window win,unsigned long action,const char *state)
{
    Atom prop = XInternAtom(disp, state, False);;
    //unsigned long action=_NET_WM_STATE_ADD; //_NET_WM_STATE_TOGGLE;
    //Atom prop1 = XInternAtom(disp, "_NET_WM_STATE_ABOVE", False);;

    //Atom prop2 = 0;
    //char *p1, *p2;
    //const char *argerr = "The -b option expects a list of comma separated parameters: \"(remove|add|toggle),<PROP1>[,<PROP2>]\"\n";

    /*
    if ((p1 = strchr(arg, ','))) {
        gchar *tmp_prop1, *tmp1;

        *p1 = '\0';

        if (strcmp(arg, "remove") == 0) {
            action = _NET_WM_STATE_REMOVE;
        }
        else if (strcmp(arg, "add") == 0) {
            action = _NET_WM_STATE_ADD;
        }
        else if (strcmp(arg, "toggle") == 0) {
            action = _NET_WM_STATE_TOGGLE;
        }
        else {
            fputs("Invalid action. Use either remove, add or toggle.\n", stderr);
            return EXIT_FAILURE;
        }
        p1++;

        // the second property
        if ((p2 = strchr(p1, ','))) {
            gchar *tmp_prop2, *tmp2;
            *p2 = '\0';
            p2++;
            if (strlen(p2) == 0) {
                fputs("Invalid zero length property.\n", stderr);
                return EXIT_FAILURE;
            }
            tmp_prop2 = g_strdup_printf("_NET_WM_STATE_%s", tmp2 = g_ascii_strup(p2, -1));
            p_verbose("State 2: %s\n", tmp_prop2);
            prop2 = XInternAtom(disp, tmp_prop2, False);
            g_free(tmp2);
            g_free(tmp_prop2);
        }

        /* the first property
        if (strlen(p1) == 0) {
            fputs("Invalid zero length property.\n", stderr);
            return EXIT_FAILURE;
        }
        tmp_prop1 = g_strdup_printf("_NET_WM_STATE_%s", tmp1 = g_ascii_strup(p1, -1));
        p_verbose("State 1: %s\n", tmp_prop1);
        prop1 = XInternAtom(disp, tmp_prop1, False);
        g_free(tmp1);
        g_free(tmp_prop1);
        */

        return client_msg(disp, win, "_NET_WM_STATE", action, (unsigned long)prop, 0, 0, 0);
}

// CLIENT FUNC
// -----------------------------------------------------
Window _gui_sys_tools::find_window(const char *disp_name,const char *wname)
{
    Display *display;

    display=XOpenDisplay(disp_name);
    if (display==NULL) return 0;

    Window rootWin = DefaultRootWindow(display);

    Window res=rec_find_windows_by_name(display, rootWin, wname);

    XCloseDisplay(display);

    return res;
}

// -----------------------------------------------------
bool _gui_sys_tools::make_window_borderless(const char *disp_name,const char *wname)
{
    Display *display;

    display=XOpenDisplay(disp_name);
    if (display==NULL) return false;

    Window rootWin = DefaultRootWindow(display);

    Window w=rec_find_windows_by_name(display, rootWin, wname);

    bool res=true;

    if (w)
    {
        Atom mwmHintsProperty = XInternAtom(display, "_MOTIF_WM_HINTS", 0);
        struct MwmHints hints;
        hints.flags = MWM_HINTS_DECORATIONS;
        hints.decorations = 0;
        XChangeProperty(display, w, mwmHintsProperty, mwmHintsProperty, 32,
        PropModeReplace, (unsigned char *)&hints, 5);
    }
    else res=false;

    XCloseDisplay(display);

    return res;
}

// -----------------------------------------------------
bool _gui_sys_tools::make_window_topmost(const char *disp_name,const char *wname)
{
    Display *display;

    display=XOpenDisplay(disp_name);
    if (display==NULL) return false;

    Window rootWin = DefaultRootWindow(display);

    Window w=rec_find_windows_by_name(display, rootWin, wname);

    bool res=true;

    if (w)
    {
        if (window_state(display,w,_NET_WM_STATE_ADD,"_NET_WM_STATE_ABOVE")) res=false;
    }
    else res=false;

    XCloseDisplay(display);

    return res;
}

// -----------------------------------------------------
bool _gui_sys_tools::resize_window(const char *disp_name,const char *wname,int nw,int nh)
{
    Display *display;

    display=XOpenDisplay(disp_name);
    if (display==NULL) return false;

    Window rootWin = DefaultRootWindow(display);

    Window w=rec_find_windows_by_name(display, rootWin, wname);

    bool res=true;

    if (w)
    {
        int err=XMoveWindow(display,w,100,100);
        XFlush(display);
        if(err==11111)
        {
            err++;
        }
    }
    else res=false;

    XCloseDisplay(display);

    return res;
}

