#include "ghelp_gl.hpp"
#include "clthread.hpp"
#include <freeglut.h>
#include <error.hpp>
#include <map>
#include <string.h>
//#include <iostream>
//#include <fstream>


#define ILUT_USE_OPENGL
#include <IL/il.h>
#include <IL/ilu.h>
#include <IL/ilut.h>

using namespace _gh;

#include <iostream>
#include <time.hpp>

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// Main window of app (via GLUT)
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
_glut_screen::_glut_screen(const char* caption,const _rect &r, bool full_screen)
:_gl_wnd(NULL,r),wnd_id(0),create_th_id(get_current_thread_id())
{

    _gl_context::gl_thread_id=create_th_id;

    pthread_cond_init(&wait_cond,NULL);
    pthread_mutex_init(&wait_mutex,NULL);

    if (full_screen)
    {
        char tmp_s[128];
        sprintf(tmp_s,"%ix%i:32",(int)r.w,(int)r.h);
        //cout << tmp_s << endl;

        glutGameModeString(tmp_s);
        wnd_id=glutEnterGameMode();
        glutSetWindow(wnd_id);
        glutSetWindowData(this);
    }
    else
    {
        glutInitWindowSize(r.w, r.h);
        glutInitWindowPosition(r.x, r.y);

        wnd_id = glutCreateWindow(caption);
        glutSetWindow(wnd_id);
        glutSetWindowData(this);
    }

    glutSetWindow(wnd_id);

    glutDisplayFunc(stub_draw);
    glutReshapeFunc(stub_resize);
    glutMouseFunc(stub_mouse_event);
    glutPassiveMotionFunc(stub_mouse_move);
    glutMotionFunc(stub_active_mouse_move);
    glutEntryFunc(stub_mouse_entry);
    glutCloseFunc(stub_close);
    glutIdleFunc(stub_idle);
    glutSpecialFunc(stub_keyboard_special);
    glutKeyboardFunc(stub_keyboard);

    //glutPositionFunc(repos_stub);

    // some setup
    glClearColor(0.0, 0.0, 0.0, 1.0);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);


    int ww,wh;
    ww=glutGet(GLUT_WINDOW_WIDTH);
    wh=glutGet(GLUT_WINDOW_HEIGHT);

    resize(ww,wh);
}

// -------------------------------------------------------------------------
_glut_screen::~_glut_screen()
{
    glutDestroyWindow(wnd_id);

    pthread_cond_destroy(&wait_cond);
    pthread_mutex_destroy(&wait_mutex);
}

// -------------------------------------------------------------------------
void _glut_screen::pre_render()
{
    _wnd::pre_render();
}

// -------------------------------------------------------------------------
void _glut_screen::refresh()
{
    if (get_current_thread_id()==create_th_id)
    {
        //cout << "DRAW NOW" << endl;
        draw_now();
    }
    else
    {
        #ifdef WINDOWS
            refresh_cnt++;
            //cout << refresh_cnt << " " << render_at_refresh_cnt << endl;
        #else
            pthread_mutex_lock(&wait_mutex);
            refresh_cnt++;
            pthread_cond_signal(&wait_cond);
            pthread_mutex_unlock(&wait_mutex);
        #endif // WINDOWS

    }
}

// -------------------------------------------------------------------------
void _glut_screen::stub_draw()
{
    _glut_screen *s = (_glut_screen *) glutGetWindowData();
    if (s)
    {
        if (!s->wnd_id) return;

        //cout << "stub draw - wait flag, thread id=" << get_current_thread_id() << " " << s->refresh_cnt << endl;

        #ifdef WINDOWS
            if (s->refresh_cnt==s->render_at_refresh_cnt)
            {
                Sleep(1);
                return;
            }
            s->render_at_refresh_cnt=s->refresh_cnt;
        #else
            pthread_mutex_lock(&s->wait_mutex);
            while (s->refresh_cnt==s->render_at_refresh_cnt) pthread_cond_wait(&s->wait_cond, &s->wait_mutex);
            s->render_at_refresh_cnt=s->refresh_cnt;
            pthread_mutex_unlock(&s->wait_mutex);
        #endif // WINDOWS

        //cout << "stub draw, thread id=" << get_current_thread_id() << " s->render_at_refresh_cnt=" << s->render_at_refresh_cnt << " s->refresh_cnt=" << s->refresh_cnt << endl;

        s->draw_now();

        //cout << "stub draw, thread id=" << get_current_thread_id() << " render time = " << (t2.diff_us(t1)) << " us"<< endl;
        //s->need_refresh=false;
    }
};

// -------------------------------------------------------------------------
void _glut_screen::draw_now()
{
    //cout << "draw_now - real draw, thread id=" << get_current_thread_id() << endl;

    glClear(GL_COLOR_BUFFER_BIT);

    glViewport(0,0,cm_rect.w,cm_rect.h);
    glLoadIdentity();

    glTranslatef(-1.0, 1.0, 0.0);
    glScalef(2.0/cm_rect.w, -2.0/(cm_rect.h), 1.0);
    glTranslatef(0.0, 1.0, 0.0); // ?????? why y coord (TODO - search in Inet)

    pre_render();

    glutSwapBuffers();

    //cout << "draw_now - real draw done, thread id=" << get_current_thread_id() << endl;
};


// -------------------------------------------------------------------------
void _glut_screen::stub_resize(int x, int y)
{
    _glut_screen *s = (_glut_screen *) glutGetWindowData();
    if (s) s->resize(x,y);
}

// -------------------------------------------------------------------------
void _glut_screen::stub_mouse_event(int btn, int state, int x, int y)
{
    _glut_screen *s = (_glut_screen *) glutGetWindowData();
    if (s) s->proc_mouse_event(btn,state,x,y);
}

// -------------------------------------------------------------------------
void _glut_screen::stub_mouse_move(int x,int y)
{
    _glut_screen *s = (_glut_screen *) glutGetWindowData();
    if (s) s->proc_mouse_move(x,y);
}

void _glut_screen::stub_active_mouse_move(int x,int y)
{
    _glut_screen *s = (_glut_screen *) glutGetWindowData();
    if (s) s->proc_active_mouse_move(x,y);
}

// -------------------------------------------------------------------------
void _glut_screen::stub_mouse_entry(int state)
{
    //_glut_screen *s = (_glut_screen *) glutGetWindowData();
    //if (s) s->proc_mouse_move(x,y);
}

// -------------------------------------------------------------------------
void _glut_screen::stub_close()
{
    _glut_screen *s = (_glut_screen *) glutGetWindowData();
    if (s) s->on_close();
}

// -------------------------------------------------------------------------
void _glut_screen::stub_idle()
{
    glutPostRedisplay();
};

void _glut_screen::stub_keyboard(unsigned char key,int x,int y)
{
    _glut_screen *s = (_glut_screen *) glutGetWindowData();
    if (s) s->proc_keyboard(key,x,y);
}

void _glut_screen::stub_keyboard_special(int key,int x,int y)
{
    _glut_screen *s = (_glut_screen *) glutGetWindowData();
    if (s) s->proc_keyboard_special(key,x,y);
}


// ----------------------------------------------------------------
// ----------------------------------------------------------------
// Base class for some window rendered via OpenGL
// ----------------------------------------------------------------

// ----------------------------------------------------------------
void _gl_wnd::pre_render()
{
    //lock.lock();
    glPushMatrix();

    //lock.lock();
    glTranslatef(cm_rect.x, cm_rect.y, 0.0);
    _wnd::pre_render();
    //lock.unlock();

    glPopMatrix();
    //lock.unlock();
}

// ----------------------------------------------------------------
// ----------------------------------------------------------------
// Props
// ----------------------------------------------------------------
void _gl_prop_color::select()
{
    glDisable(GL_TEXTURE_2D);
    glColor4f(cm_r,cm_g,cm_b,cm_alpha);
}

// ----------------------------------------------------------------
void _gl_prop_line::select()
{
    glLineWidth(cm_width);
    glLineStipple(cm_factor,cm_pattern);
}

// ----------------------------------------------------------------
// IMAGE props
// ----------------------------------------------------------------
int _gl_prop_image::devil_ref = 0;

// ----------------------------------------------------------------
void _gl_prop_image::init()
{
    if (!devil_ref)
    {
        devil_ref++;

        ilInit();
        iluInit();
        ilutInit();

        ilEnable(IL_CONV_PAL);
        ilEnable(ILUT_OPENGL_CONV);

        ilutRenderer(ILUT_OPENGL);
    }
}

// ----------------------------------------------------------------
_gl_prop_image::_gl_prop_image(const char *fname):id(0)
{
    init();

    ilGenImages(1,&id);
    ilBindImage(id);

    if (ilLoadImage(fname)==IL_FALSE)
    {
        throw _error(0,string("_gl_prop_image::_gl_prop_image(fname) - error load image from file ")+fname);
    }
}

// ----------------------------------------------------------------
_gl_prop_image::_gl_prop_image(const char *data,const char *type,unsigned int size)
{
    init();

    ilGenImages(1,&id);
    ilBindImage(id);

    int btype=IL_JPG;
    if (!strcmp(type,"png")) btype=IL_PNG;
    else if (!strcmp(type,"bmp")) btype=IL_BMP;
    else if (!strcmp(type,"gif")) btype=IL_GIF;

    // TODO - set right type
    if (ilLoadL(btype,data,size)==IL_FALSE)
    {
        //free(Lump);
        throw _error(0,"_gl_prop_image::_gl_prop_image(data,type,size) - error load image from mem ");
    }

    //free(Lump);
}

// ----------------------------------------------------------------
_gl_prop_image::~_gl_prop_image()
{
    devil_ref--;
    if (!devil_ref)
    {

    }

    if (id) ilDeleteImages(1,&id);
}

// ----------------------------------------------------------------
void _gl_prop_image::select()
{
    ilBindImage(id);
}

// ----------------------------------------------------------------
// TEXTURE props
// ----------------------------------------------------------------
_gl_prop_texture::_gl_prop_texture(const char *fname):id(0)
{
    _gl_prop_image img(fname);

    glGenTextures(1,&id);
    glBindTexture(GL_TEXTURE_2D,id);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

    int w=ilGetInteger(IL_IMAGE_WIDTH);
    int h=ilGetInteger(IL_IMAGE_HEIGHT);

    /*int nw=1;
    int nh=1;

    while(nw<w) nw<<=1;
    while(nh<h) nh<<=1;

    if (nw!=w || nh!=h)
    {
        iluImageParameter(ILU_FILTER,ILU_BILINEAR);
        iluScale(nw,nh,1);
    }
    */
    cm_w=w;
    cm_h=h;

    unsigned char *data=new unsigned char [cm_w*cm_h*4];
    ilCopyPixels(0,0,0,cm_w,cm_h,1,IL_RGBA,IL_UNSIGNED_BYTE,data);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, cm_w, cm_h,
              0, GL_RGBA, GL_UNSIGNED_BYTE, data);

    delete [] data;
}

// ----------------------------------------------------------------
_gl_prop_texture::_gl_prop_texture(const char *img_data,const char *type,unsigned int size)
{
    _gl_prop_image img(img_data,type,size);

    glGenTextures(1,&id);
    glBindTexture(GL_TEXTURE_2D,id);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

    unsigned int w=ilGetInteger(IL_IMAGE_WIDTH);
    unsigned int h=ilGetInteger(IL_IMAGE_HEIGHT);

    /*int nw=1;
    int nh=1;

    while(nw<w) nw<<=1;
    while(nh<h) nh<<=1;

    if (nw!=w || nh!=h)
    {
        iluImageParameter(ILU_FILTER,ILU_BILINEAR);
        iluScale(nw,nh,1);
    }
    */
    cm_w=w;
    cm_h=h;

    unsigned char *data=new unsigned char [cm_w*cm_h*4];
    ilCopyPixels(0,0,0,cm_w,cm_h,1,IL_RGBA,IL_UNSIGNED_BYTE,data);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, cm_w, cm_h,
              0, GL_RGBA, GL_UNSIGNED_BYTE, data);

    delete [] data;
}

// ----------------------------------------------------------------
_gl_prop_texture::~_gl_prop_texture()
{
    glDeleteTextures(1,&id);
}

// ----------------------------------------------------------------
void _gl_prop_texture::select()
{
    glColor4f(1.0,1.0,1.0,1.0);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D,id);
}


// ----------------------------------------------------------------
// ----------------------------------------------------------------
// Global MAP of all props
// ----------------------------------------------------------------
int _gl_props::ref_count = 0;
_gl_props::_props_map _gl_props::cm_props_map;

// ----------------------------------------------------------------
_gl_props::_gl_props()
{
    ref_count++;
}

// ----------------------------------------------------------------
_gl_props::~_gl_props()
{
    ref_count--;
    //if (ref_count==0) delete_all();
}

// ----------------------------------------------------------------
void _gl_props::delete_all()
{
    _props_map::iterator it=cm_props_map.begin();
    while(it!=cm_props_map.end())
    {
        delete it->second;
        it++;
    }
    cm_props_map.clear();
}

// ----------------------------------------------------------------
bool _gl_props::set(const string &n,_gl_prop *p)
{
    _props_map::iterator it=cm_props_map.find(n);
    if (it!=cm_props_map.end())
    {
        std::swap(it->second, p);
        delete p;
        return true;
    }

    cm_props_map.insert(pair<string,_gl_prop *>(n,p));
    return false;
}

// ----------------------------------------------------------------
bool _gl_props::remove(const string &n)
{
    _props_map::iterator it=cm_props_map.find(n);
    if (it!=cm_props_map.end())
    {
        delete it->second;
        cm_props_map.erase(it);
            return true;
    }
        return false;
}

// ----------------------------------------------------------------
_gl_prop *_gl_props::select(const string &n)
{
    _props_map::iterator it=cm_props_map.find(n);
    if (it!=cm_props_map.end())
    {
        it->second->select();
        return it->second;
    }
    return NULL;
}
