/*
 * File:   ghelp.hpp
 * Author: paa
 *
 * Created on 12 Октябрь 2013 г., 18:02
 */

#ifndef __GHELP_HPP__
#define	__GHELP_HPP__

#include <set>
#include <list>
#include <string>

using namespace std;

namespace _gh
{

struct _gl_context
{
    static unsigned long gl_thread_id;
};

// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
struct _rect
{
    float x;
    float y;
    float w;
    float h;

    _rect():x(0.0),y(0.0),w(0.0),h(0.0) {};
    _rect(float sx,float sy,float sw,float sh):x(sx),y(sy),w(sw),h(sh) {};
};

// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
class _layout
{
    public:
        enum grow_style { fix, grow };
        typedef list<_layout *> _layout_items;

    protected:
        grow_style cm_v_style;
        grow_style cm_h_style;

    public:
        _layout(grow_style h_style,grow_style v_style):cm_v_style(v_style),cm_h_style(h_style) {};
        virtual ~_layout() {};

        grow_style v_style() { return cm_v_style; };
        grow_style h_style() { return cm_h_style; };

        virtual float x() = 0;
        virtual float y() = 0;
        virtual float w() = 0;
        virtual float h() = 0;

        virtual void clear() {};

        virtual void resize(float new_w,float new_h) = 0;
        virtual void repos(float new_x,float new_y) = 0;
};

// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
class _layout_h_box:public _layout
{
    protected:
        float cm_x;
        float cm_y;
        float cm_h;
        float cm_w;
        float cm_fix_w;

        _layout::_layout_items cm_items;

    public:
        _layout_h_box():_layout(_layout::fix,_layout::fix),cm_x(0),cm_y(0),cm_h(0),cm_w(0),cm_fix_w(0) {};
        virtual ~_layout_h_box() { clear(); };

        virtual float x() { return cm_x; };
        virtual float y() { return cm_y; };
        virtual float w() { return cm_w; };
        virtual float h() { return cm_h; };


        virtual void add(_layout *i);
        virtual void clear();

        virtual void resize(float new_w,float new_h);
        virtual void repos(float new_x,float new_y);
};

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
class _layout_v_box:public _layout
{
    protected:
        float cm_x;
        float cm_y;
        float cm_h;
        float cm_w;
        float cm_fix_h;

        _layout::_layout_items cm_items;

    public:
        _layout_v_box():_layout(_layout::fix,_layout::fix),cm_x(0),cm_y(0),cm_h(0),cm_w(0),cm_fix_h(0) {};
        virtual ~_layout_v_box() { clear(); };

        virtual float x() { return cm_x; };
        virtual float y() { return cm_y; };
        virtual float w() { return cm_w; };
        virtual float h() { return cm_h; };

        virtual void add(_layout *i);
        virtual void clear();

        virtual void resize(float new_w,float new_h);
        virtual void repos(float new_x,float new_y);
};

class _layout_space:public _layout
{
    protected:
        _rect cm_rect;

    public:
        _layout_space(const _rect &r,_layout::grow_style hs,_layout::grow_style vs):_layout(hs,vs),cm_rect(r) {};
        virtual ~_layout_space() {};

        virtual float x() { return cm_rect.x; };
        virtual float y() { return cm_rect.y; };
        virtual float w() { return cm_rect.w; };
        virtual float h() { return cm_rect.h; };

        virtual void resize(float new_w,float new_h)
        {
            if (cm_h_style!=_layout::fix) cm_rect.w=new_w;
            if (cm_v_style!=_layout::fix) cm_rect.h=new_h;
        };
        virtual void repos(float new_x,float new_y) { cm_rect.x=new_x; cm_rect.y=new_y; };
};

class _wnd_signal;

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
class _wnd
{
    public:
        typedef list<_wnd *> _childs;

        struct _state
        {
            bool is_visible;
            bool is_enable;
            bool have_focus;

			bool need_create;
			bool was_created;
			bool need_delete;
			bool was_deleted;

            _state():is_visible(true),is_enable(true),have_focus(false),
					 need_create(false),was_created(false),
			         need_delete(false),was_deleted(false) {};
        };

    protected:
        _rect cm_rect;

        _layout *cm_layout;

        _wnd *cm_parent;
        _childs cm_childs;

        _state cm_state;

        virtual void render() = 0;

		virtual void on_create() = 0;
		virtual void on_delete() = 0;

		virtual ~_wnd();

    public:

        _wnd(_wnd *p,const _rect &r);

        const _rect &rect() const { return cm_rect; };

        virtual const _rect &resize(float new_w,float new_h) { cm_rect.w=new_w; cm_rect.h=new_h; if (cm_layout) cm_layout->resize(new_w,new_h); return cm_rect; };
        virtual const _rect &repos(float new_x,float new_y) { cm_rect.x=new_x; cm_rect.y=new_y; return cm_rect; };
        virtual const _rect &resize_repos(const _rect &r) { cm_rect=r; if (cm_layout) cm_layout->resize(r.w,r.h); return cm_rect; };

        virtual bool proc_mouse_event(int btn,int state,float x,float y);
        virtual void on_mouse_event(int btn,int state,float x,float y) {  }

        virtual bool proc_mouse_move(float x,float y);
        virtual bool on_mouse_move(float x,float y) { return true; };

        virtual void proc_active_mouse_move(float x,float y);
        virtual void on_active_mouse_move(float x,float y) { };

        // we have not proc_keyboard & proc_keyboard_special
        // we call on_keyboard & on_keyboard_special
        virtual bool proc_keyboard(unsigned char key,float x,float y);
        virtual void on_keyboard(unsigned char key,float x,float y) { }

        virtual bool proc_keyboard_special(int key,float x,float y);
        virtual bool on_keyboard_special(int key,float x,float y) { return true; };

        virtual void set_focus();
        virtual void on_set_focus() {};

        virtual void clear_focus();
        virtual void on_clear_focus() {};

        _wnd *parent() { return cm_parent; };
        _wnd *root() { if (!cm_parent) return this; return cm_parent->root(); };
        const _childs &childs() const { return cm_childs; };

        const _state &state() const { return cm_state; };
        bool is_enable() const { return cm_state.is_enable; };
        bool is_visible() const { return cm_state.is_visible; };

        void show(bool make_top);
        void hide();

        void enable();
        void disable();

        // common window can be without parent (& parent refresh & render)
        // we must check it - do not infinity recursive loop
        // & we must try pre_render it
        virtual void refresh() { _wnd *r=root(); if (r!=this) r->refresh(); else pre_render(); };
        virtual void pre_render();

        virtual bool on_signal(_wnd_signal &sig) { return true; };

        //virtual void on_child_signal(_wnd_signal *sig) { if (cm_parent) cm_parent->on_child_signal(sig); };

		virtual void release();
};


// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
class _wnd_signal
{
    protected:
        _wnd *cm_sender;
        string cm_data;

        static string null_str;
    public:
        _wnd_signal(_wnd *s,const string &d):cm_sender(s),cm_data(d) {};

        inline _wnd *sender() { return cm_sender; };
        inline const _wnd *sender() const { return cm_sender; };

        inline const string &data() const { return cm_data; };
                     string operator[](const string &param) const;
};

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
class _layout_wnd:public _layout
{
    protected:
        _wnd *cm_wnd;

    public:
        _layout_wnd(_wnd *w,grow_style h_style,grow_style v_style):_layout(h_style,v_style),cm_wnd(w) {};
        virtual ~_layout_wnd() {};

        virtual float x() { return cm_wnd->rect().x; };
        virtual float y() { return cm_wnd->rect().y; };
        virtual float w() { return cm_wnd->rect().w; };
        virtual float h() { return cm_wnd->rect().h; };

        virtual void clear() {};

        virtual void resize(float new_w,float new_h)
        {
            if (cm_h_style==_layout::fix) new_w=w();
            if (cm_v_style==_layout::fix) new_h=h();
            cm_wnd->resize(new_w,new_h);
        };

        virtual void repos(float new_x,float new_y) { cm_wnd->repos(new_x,new_y); };
};

};


#endif	/* GHELP_HPP */

