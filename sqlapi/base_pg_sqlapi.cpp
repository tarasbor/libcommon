#include "base_pg_sqlapi.hpp"
//#include "base.hpp"
///#include <system/misc.hpp>
#include <udefs.hpp>
#include <system/text_help.hpp>

#include <boost/algorithm/string.hpp>

#include <list>
#include <SQLAPI.h>

// Uncomment to enable log printing to baq_server.log, for loglevel 7,8. ()
//#define LOG_BASE_PG_SQLAPI

#ifdef LOG_BASE_PG_SQLAPI
// Uncomment LOG_BASE_PG_SQLAPI and bottom to show operations with _time (push/pull)
//#define LOG_TIMESTAMP_OPERATIONS
#endif

#include <log2/log.hpp> // for debug
#define LOGL8_TRAC 15   //instead of new

//#define MLOG(A) _g_logger() << _g_logger::_log_module_level("base_pg_sqlapi",(A))
#define MLOG(A)     _log() << _log::_set_module_level("base_pg_sqlapi",(A))

// Converts SAString -> (const char*)
#define SASTD(X)  X.GetUTF8Chars()
// #define STDSA(X)  X.c_str()

// Call SAException ErrText/ErrMessage
#define SAERRTEXTSTD(X) SASTD(X.ErrMessage())

#define IFNULLTHROW(pointer,errorText)   if(pointer==NULL) throw _pg_db_error(errorText)
#define IFNOTNULLTHROW(pointer,errorText)   if(pointer!=NULL) throw _pg_db_error(errorText)


// converts STD -> SAString
const SAString& std_to_sastr(SAString& out, const std::string &in)
{
    out.Empty();
    out.SetUTF8Chars(in.c_str());
    return out;
}

// converts STD -> SAString
const SAString  std_to_sastr(const std::string &in)
{
    SAString    out;
    out.SetUTF8Chars(in.c_str());
    return out;
}


//-------------------------------------------------------------------
class _s_statement: public _pg_stat
{
private:
    SACommand*      mpCommand;
    SAConnection*   mpConnection;

    // result
    /*bool mFirstFetch;
    uint mCurRow;   */
    // in sqlapi++ mFirstFetch and mCurRow unneccessary, because have its own fetch(),
    // only current field counter needed/.
    int mCurField;

    // exec params
    list<std::string> mListPushParams;

public:
    _s_statement(SAConnection*  saconn):mpConnection(saconn),mpCommand(NULL)/*,mFirstFetch(true),mCurRow(0)*/,mCurField(0) {}    // ASSUME saconn!=NULL
    virtual ~_s_statement() { DEL(mpCommand); }     // no throw


    virtual void push(const char *in);
    virtual void push(const std::string &in);   // quote income string
    virtual void push(const _pg_id &in);
    virtual void push(const _time &in);
    virtual void push(const int in);       // no quotation
    virtual void push(const json& in);
    virtual void push(const float in);   // no quotation, postgres real(4 bytes)
    virtual void push(const bool in);    // no quotation


    virtual void exec(const std::string &query);    // throw

    //Методы упрощающие вызов insert и update запросов
    //(INSERT) в id возвращает id созданной строки
    //(UPDATE) возвращает количество затронутых строк
    virtual void exec_insert(const string& table_name, const json& jObj, _pg_id *createdId=NULL);
    virtual int exec_update(const string& table_name, const json& jArgs, const string& where_args);
    virtual int exec_delete(const string& table_name, const string& where_args = "");

    virtual int count_columns();          //Количество полей в ответе
    virtual long count_rows_affected();    //Поличество строк затронутых запросом

    //virtual long rows_affected();    // throw
    virtual bool fetch();           // throw

    // Throw.
    virtual void pop(std::string &out);
    virtual void pop(_pg_id &out);
    virtual void pop(_time &out);
    virtual void pop(int &out);
    virtual void pop(json& out);
    virtual void pop(float& out);
    virtual void pop(bool& out);

    // Throw. Set &out to &def if value == NULL
    virtual bool pop_nnull(std::string& out, const std::string& def="");
    virtual bool pop_nnull(_pg_id& out, const _pg_id& def = 0);
    virtual bool pop_nnull(_time& out, const _time& def=_time(NOTDEFINED_TIMESTAMP));
    virtual bool pop_nnull(int& out, const int def = 0);
    virtual bool pop_nnull(json& out, const json& def = json({}));
    virtual bool pop_nnull(float& out, const float def = 0.0);
    virtual bool pop_nnull(bool& out, const bool def = false);

    virtual std::string pop_nnull_retStr(const std::string &def = "");
    virtual _pg_id  pop_nnull_dbId(const _pg_id &def = 0);
    virtual int     pop_nnull_retInt(const int def = 0);
    virtual bool    pop_nnull_retBool(const bool def = false);
};

// ---------
void _s_statement::push(const char *in) {
    std::string buff(in);
    push(buff);
}


// --------------------------------------------------------------------
/// Quoting. Don`t quote params, when use push!
void _s_statement::push(const std::string &in)
{
    std::string quoting;
    quoting = in;
    // Double single quotes
    boost::replace_all(quoting, "'", "''");
    quoting.insert(quoting.begin(),'\'');
    quoting += '\'';
    mListPushParams.push_back(quoting);
}

// ---------
void _s_statement::push(const _pg_id &in)
{
    mListPushParams.push_back(_text_help::to_std_string(in.value()));    // no quotation
}

// ---------
void _s_statement::push(const _time &in)
{
#ifdef  LOG_TIMESTAMP_OPERATIONS
    MLOG(LOGL8_TRAC) << "push(_time).text:" << in.text() << endl;
#endif
    push(in.text());    // push with single quotted
}

// ---------
void _s_statement::push(const int in)
{
    mListPushParams.push_back(_text_help::to_std_string(in));    // no quotation
}

// ---------
void _s_statement::push(const json& in)
{
    push(in.dump());    // push with single quotted
}

// ---------
void _s_statement::push(const float in)
{
    mListPushParams.push_back(_text_help::to_std_string(in));    // no quotation
}

// ---------
void _s_statement::push(const bool in)
{
    if(in)
        mListPushParams.push_back("true");    // no quotation
    else
        mListPushParams.push_back("false");   // no quotation
}

int _s_statement::count_columns()
{
    IFNULLTHROW(mpCommand,"_s_statement SACommand==NULL");  // in case fetch() before exec()
    return mpCommand->FieldCount();
}

long _s_statement::count_rows_affected()
{
    IFNULLTHROW(mpCommand,"_s_statement SACommand==NULL");  // in case fetch() before exec()
    return mpCommand->RowsAffected();
}


// ---------
bool _s_statement::fetch()
{
    IFNULLTHROW(mpCommand,"_s_statement SACommand==NULL");  // in case fetch() before exec()
    try {
        /*if(mpCommand->RowsAffected()==0)
            return false;

        if(mFirstFetch)     // if we here result mResult.size > 0
            mFirstFetch = false;
        else {
            if( mCurRow+1 >= mpCommand->RowsAffected() )
                return false;
            mCurRow++;
            mCurField = 0;
        }
        return true;*/
        mCurField = 0;
        return mpCommand->FetchNext();
    } catch (const SAException& E) {throw _pg_db_error(SAERRTEXTSTD(E));}
}

// ---------
void _s_statement::exec_insert(const string& table_name, const json& jObj, _pg_id *id)
{
    try
    {
        std::string text_query = "INSERT INTO "+table_name; bool first = true;
        std::string columns,values;

        for (json::const_iterator it = jObj.begin(); it != jObj.end(); ++it)
        {
            if(first == false) {columns+=", ";  values+=", ";}
            columns+=it.key();
            if(it.value().is_string())              { values += ":<>"; push(it.value().get<string>());} else
            if(it.value().is_number_float())        { values += ":<>"; push(it.value().get<float>()); } else
            if(it.value().is_number_integer())      { values += ":<>"; push(it.value().get<int>()); } else
            if(it.value().is_boolean())             { values += ":<>"; push(it.value().get<bool>()); } else
            if(it.value().is_structured())          { values += ":<>"; push(it.value()); } else
            /* Если NULL или просто не удалось определить тип */ { values += "null"; }
            first = false;
        }

        text_query += "("+columns+") VALUES("+values+")";
        if(id!=NULL) text_query += " RETURNING id";
        text_query += ";";
        exec(text_query);

        if(id!=NULL && fetch()!=false)  {pop_nnull(*id);}
    }
    catch (const _pg_db_error &e)       { throw e; }
    catch (json::exception &e)       { throw _pg_db_error("json error (exec_insert)"); }
}

// ---------
int _s_statement::exec_update(const string& table_name, const json& jArgs, const string& where_args)
{
    try
    {
        std::string text_query = "UPDATE "+table_name+" SET "; bool first = true;

        for (json::const_iterator it = jArgs.begin(); it != jArgs.end(); ++it)
        {
            if(first == false) text_query+=", ";
            text_query += it.key() + " = ";
            if(it.value().is_string())              { text_query += ":<>"; push(it.value().get<string>());} else
            if(it.value().is_number_float())        { text_query += ":<>"; push(it.value().get<float>()); } else
            if(it.value().is_number_integer())      { text_query += ":<>"; push(it.value().get<int>()); } else
            if(it.value().is_boolean())             { text_query += ":<>"; push(it.value().get<bool>()); /*bool b = it.value().get<bool>(); if(b) text_query += "true"; else text_query += "false";*/ } else
            if(it.value().is_structured())          { text_query += ":<>"; push(it.value()); } else
            /* Если NULL или просто не удалось определить тип */ { text_query += "null"; }
            first = false;
        }
        if(where_args!="") {text_query += " WHERE "+where_args;}
        text_query+=";";
        exec(text_query);
    }
    catch (const _pg_db_error &e)       { throw e; }
    catch (json::exception &e)       { throw _pg_db_error("json error (exec_update)"); }

    return count_rows_affected();
}

// ---------
int _s_statement::exec_delete(const string& table_name, const string& where_args)
{
    try {
        std::string text_query = "DELETE FROM "+table_name;
        text_query += " WHERE "+where_args+";";
        exec(text_query);
    }
    catch (const _pg_db_error &e)       { throw e; }

    return count_rows_affected();
}

// ---------
void _s_statement::exec(const std::string &query)
{
    DEL(mpCommand);     // Clear command. ~SACommand() don`t throw
    IFNULLTHROW(mpConnection,"_s_statement SAConnection==NULL");    // in case when constructed _s_statement(SAConnection==NULL)

    std::string queryExec = query;
    try{
        // Fill query and clear mListPopParams
        std::size_t c = 0, i = 0;
        std::string buff;
        while(!mListPushParams.empty())
        {
            i = queryExec.find(":<",i);
            if(i==queryExec.npos) {
                mListPushParams.clear();
                throw _pg_db_error("ERROR: In db query syntax, no open tag ':<' founded.");
            }
            c = queryExec.find(">",i+2);
            if(c==queryExec.npos) {
                mListPushParams.clear();
                throw _pg_db_error("ERROR: In db query syntax, no closing tag '>' founded.");
            }

            buff = mListPushParams.front();
            // push already doubled inner single quotes if exist
            mListPushParams.pop_front();            
            queryExec.replace(i,c-i+1,buff);
            i+=buff.size();
        }

#ifdef  LOG_BASE_PG_SQLAPI
        MLOG(LOGL7_DBG) << "exec '" << queryExec << "'" << endl;
#endif
        mpCommand = new SACommand(mpConnection);
        // Enable multiple SACommand in one SAConnection
        // The limitation - the single, forward-only result set only.
        // Result stored at the client side, see http://www.sqlapi.com/Support/FAQ.html#QandA5 (Question 12)
        mpCommand->setOption("UseStatement") = "TRUE";
        mpCommand->setCommandText( std_to_sastr(queryExec) );
        mpCommand->Execute();
        /*mFirstFetch = true;
        mCurRow = 0;*/
        mCurField = 0;
    } catch (const SAException& E) {
        std::string buff=SAERRTEXTSTD(E);
        buff+="\"";
        buff+=queryExec;
        buff+="\"";
        throw _pg_db_error(buff);
    }
}

// ---------
void _s_statement::pop(std::string &out)
{
    IFNULLTHROW(mpCommand,"_s_statement.pop(string) SACommand==NULL");
    try {
       if(mCurField >= mpCommand->FieldCount())
         throw _pg_db_error("ERROR: Current field index is out of FieldCount.");
       if(mpCommand->Field(mCurField+1).isNull())   // in SQAPI++ column number start from 1
         throw _pg_db_error("ERROR: pop(), string = NULL.");
       out = SASTD(mpCommand->Field(mCurField+1).asString()); // in SQAPI++ column number start from 1
       mCurField++;
    } catch (const SAException& E) {throw _pg_db_error(SAERRTEXTSTD(E));}
}

// ---------
void _s_statement::pop(_pg_id &out)
{
    IFNULLTHROW(mpCommand,"_s_statement.pop(_pg_id) SACommand==NULL");
    try{
        // TODO check? long 8 byte > int (4 bytes)
        if(mCurField >= mpCommand->FieldCount())
            throw _pg_db_error("ERROR: Current field index is out of FieldCount.");
        if(mpCommand->Field(mCurField+1).isNull())      // in SQAPI++ column number start from 1
            throw _pg_db_error("ERROR: pop(), _pg_id = NULL.");
        //out = mpCommand->Field(mCurField+1).asLong();
        out.from_str( SASTD(mpCommand->Field(mCurField+1).asString()) );
        mCurField++;
    } catch (const SAException& E) { throw _pg_db_error(SAERRTEXTSTD(E)); }

}

// ---------
void _s_statement::pop(_time &out)
{
    IFNULLTHROW(mpCommand,"_s_statement.pop(_time) SACommand==NULL");
    try{
       if(mCurField >= mpCommand->FieldCount())
         throw _pg_db_error("ERROR: Current field index is out of FieldCount.");
       if(mpCommand->Field(mCurField+1).isNull())
         throw _pg_db_error("ERROR: pop(), _time = NULL.");
       std::string v = SASTD(mpCommand->Field(mCurField+1).asString());
#ifdef  LOG_TIMESTAMP_OPERATIONS
       MLOG(LOGL8_TRAC) << "pop(_time)" << v << endl;   // debug only
#endif
       mCurField++;
       if(!out.try_parse(v))
         throw _pg_db_error("ERROR: Trying parse _time from std::string.");
    } catch (const SAException& E) { throw _pg_db_error(SAERRTEXTSTD(E)); }
}


// ---------
void _s_statement::pop(int &out)
{
    IFNULLTHROW(mpCommand,"_s_statement.pop(int) SACommand==NULL");
    try{  // TODO check? long 8 byte > int (4 bytes)
        if(mCurField >= mpCommand->FieldCount())
            throw _pg_db_error("ERROR: Current field index is out of FieldCount.");
        if(mpCommand->Field(mCurField+1).isNull())
            throw _pg_db_error("ERROR: pop(), int = NULL.");
        out = mpCommand->Field(mCurField+1).asLong();  // in SQAPI++ column number start from 1
        mCurField++;
    } catch (const SAException& E) {throw _pg_db_error(SAERRTEXTSTD(E));}
}


// ---------
void _s_statement::pop(json &out)
{
    IFNULLTHROW(mpCommand,"_s_statement.pop(json) SACommand==NULL");
    std::string buff;
    pop(buff);  // throw exception if NULL, but not empty ("")

    try{    // parse json
        if(buff.empty())
            throw _pg_db_error("ERROR: Trying parse json from std::string (empty string).");
        out = json::parse(buff.c_str()); // jsn.parse() not work!!!
    } catch(const json::exception &e) {
        throw _pg_db_error(std::string("ERROR: trying pop(json): ") + e.what());
    }
}

// ---------
void _s_statement::pop(float &out)
{
    IFNULLTHROW(mpCommand,"_s_statement.pop(float) SACommand==NULL");
    try{  // TODO long 8 byte > int (4 bytes)
        if(mCurField >= mpCommand->FieldCount())
            throw _pg_db_error("ERROR: Current field index is out of FieldCount.");
        if(mpCommand->Field(mCurField+1).isNull())
            throw _pg_db_error("ERROR: pop(), float = NULL.");
        out = mpCommand->Field(mCurField+1).asDouble();  // in SQAPI++ column number start from 1
        mCurField++;
    } catch (const SAException& E) {throw _pg_db_error(SAERRTEXTSTD(E));}
}

// ---------
void _s_statement::pop(bool &out)
{
    IFNULLTHROW(mpCommand,"_s_statement.pop(bool) SACommand==NULL");
    try{  // TODO long 8 byte > int (4 bytes)
        if(mCurField >= mpCommand->FieldCount())
            throw _pg_db_error("ERROR: Current field index is out of FieldCount.");
        if(mpCommand->Field(mCurField+1).isNull())
            throw _pg_db_error("ERROR: pop(), bool = NULL.");
        out = mpCommand->Field(mCurField+1).asBool();  // in SQAPI++ column number start from 1
        mCurField++;
    } catch (const SAException& E) {throw _pg_db_error(SAERRTEXTSTD(E));}
}

// ---------
bool _s_statement::pop_nnull(string &out, const string &def)
{
    IFNULLTHROW(mpCommand,"_s_statement.pop_nnull(string) SACommand==NULL");
    try{
        if(mCurField >= mpCommand->FieldCount())
            throw _pg_db_error("ERROR: Current field index is out of FieldCount.");
        mCurField++;    // in SQAPI++ column number start from 1

        /*if(mpCommand->Field(mCurField).FieldType()!=SA_dtString) {  // fix segfault because sqlapi conversion types assert
            throw _pg_db_error("ERROR: Wrong pop_nnull(string) field type.");
        }*/

        if(mpCommand->Field(mCurField).isNull()) {
            out = def;
            return false;
        }
        out = SASTD(mpCommand->Field(mCurField).asString()); // Try to get result. In SQAPI++ column number start from 1.
    } catch (const SAException& E) {throw _pg_db_error(SAERRTEXTSTD(E));}

    return true;
}

// ---------
bool _s_statement::pop_nnull(_pg_id &out, const _pg_id &def)
{
    IFNULLTHROW(mpCommand,"_s_statement.pop_nnull(_pg_id) SACommand==NULL");
    try{
        if(mCurField >= mpCommand->FieldCount())
            throw _pg_db_error("ERROR: Current field index is out of FieldCount.");
        mCurField++;    // in SQAPI++ column number start from 1

        /*if(mpCommand->Field(mCurField).FieldType()!=SA_dtString)   // fix segfault because sqlapi conversion types assert
            throw _pg_db_error("ERROR: Wrong pop_nnull(_pg_id) field type.");*/

        if(mpCommand->Field(mCurField).isNull())
        {
            out = def;
            return false;
        }
        out.from_str( SASTD(mpCommand->Field(mCurField).asString()) );
    } catch (const SAException& E) {throw _pg_db_error(SAERRTEXTSTD(E));}

    return true;
}

// ---------
bool _s_statement::pop_nnull(_time &out, const _time &def)
{
    IFNULLTHROW(mpCommand,"_s_statement.pop_nnull(_time) SACommand==NULL");
    try{
        if(mCurField >= mpCommand->FieldCount())
            throw _pg_db_error("ERROR: Current field index is out of FieldCount.");
        mCurField++;    // in SQAPI++ column number start from 1

        /*if(mpCommand->Field(mCurField).FieldType()!=SA_dtString)   // fix segfault because sqlapi conversion types assert
            throw _pg_db_error("ERROR: Wrong pop_nnull(_time) field type.");*/

        if(mpCommand->Field(mCurField).isNull())  {
            out = def;
            return false;
        }
        std::string v = SASTD(mpCommand->Field(mCurField).asString());
#ifdef  LOG_TIMESTAMP_OPERATIONS
        MLOG(LOGL8_TRAC) << "pop_nnull(time) " << v << endl; // debugonly
#endif
        if(!out.try_parse(v))
            throw _pg_db_error("ERROR: Trying parse _time from std::string.");
    } catch (const SAException& E) {throw _pg_db_error(SAERRTEXTSTD(E));}

    return true;
}

// ---------
bool _s_statement::pop_nnull(int &out, const int def)
{
    IFNULLTHROW(mpCommand,"_s_statement.pop_nnull(int) SACommand==NULL");
    try{
        if(mCurField >= mpCommand->FieldCount())
            throw _pg_db_error("ERROR: Current field index is out of FieldCount.");
        mCurField++;    // in SQAPI++ column number start from 1

        /*if(mpCommand->Field(mCurField).FieldType()!=SA_dtLong)   // fix segfault because sqlapi conversion types assert
            throw _pg_db_error("ERROR: Wrong pop_nnull(int) field type.");*/

        if(mpCommand->Field(mCurField).isNull())
        {
            out = def;
            return false;
        }
        out = mpCommand->Field(mCurField).asLong();  //! implicit conversion from long(8 bytes?) to int(4 bytes?). In SQAPI++ column number start from 1
    } catch (const SAException& E) {throw _pg_db_error(SAERRTEXTSTD(E));}

    return true;
}


// ---------
bool _s_statement::pop_nnull(json& out, const json& def)
{
    IFNULLTHROW(mpCommand,"_s_statement.pop_nnull(json) SACommand==NULL");
    std::string buff;
    if(!pop_nnull(buff)) // json column = NULL
    {
        out = def;
        return false;
    }

    try{    // parse json
        if(buff.empty())
            throw _pg_db_error("ERROR: Trying parse json from std::string (empty string).");
        out = json::parse(buff.c_str()); // jsn.parse() not work!!!
    } catch(const json::exception &e) {
        throw _pg_db_error(std::string("ERROR: trying pop(json): ") + e.what());
    }

    return true;
}

// ---------
bool _s_statement::pop_nnull(float& out, const float def)
{
    IFNULLTHROW(mpCommand,"_s_statement.pop_nnull(float) SACommand==NULL");
    try{
        if(mCurField >= mpCommand->FieldCount())
            throw _pg_db_error("ERROR: Current field index is out of FieldCount.");
        mCurField++;    // in SQAPI++ column number start from 1

        /*if(mpCommand->Field(mCurField).FieldType()!=SA_dtDouble)   // fix segfault because sqlapi conversion types assert
            throw _pg_db_error("ERROR: Wrong pop_nnull(float) field type.");*/

        if(mpCommand->Field(mCurField).isNull())
        {
            out = def;
            return false;
        }
        out = mpCommand->Field(mCurField).asDouble();  //! implicit conversion from double(8 bytes?) to float(4 bytes). In SQAPI++ column number start from 1
    } catch (const SAException& E) {throw _pg_db_error(SAERRTEXTSTD(E));}

    return true;
}

// ---------
bool _s_statement::pop_nnull(bool& out, const bool def)
{
    IFNULLTHROW(mpCommand,"_s_statement.pop_nnull(bool) SACommand==NULL");
    try{
        if(mCurField >= mpCommand->FieldCount())
            throw _pg_db_error("ERROR: Current field index is out of FieldCount.");
        mCurField++;    // in SQAPI++ column number start from 1

        /*if(mpCommand->Field(mCurField).FieldType()!=SA_dtBool)   // fix segfault because sqlapi conversion types assert
            throw _pg_db_error("ERROR: Wrong pop_nnull(bool) field type.");*/

        if(mpCommand->Field(mCurField).isNull())
        {
            out = def;
            return false;
        }
        out = mpCommand->Field(mCurField).asBool();  // In SQAPI++ column number start from 1
    } catch (const SAException& E) {throw _pg_db_error(SAERRTEXTSTD(E));}

    return true;
}

string _s_statement::pop_nnull_retStr(const string &def)
{
    std::string val(def); pop_nnull(val,def); return val;
}

_pg_id  _s_statement::pop_nnull_dbId(const _pg_id &def)
{
    _pg_id  val(def); pop_nnull(val,def); return val;
}

int _s_statement::pop_nnull_retInt(const int def)
{
    int val(def); pop_nnull(val,def); return val;
}

bool _s_statement::pop_nnull_retBool(const bool def)
{
    bool val(def); pop_nnull(val,def); return val;
}

//---------------------------------------------------------------------
class _s_transact: public _pg_tr
{
private:
    //SAConnection   mConnection;
    SAConnection*   mpConnection;
    std::string     msConnInfoDB;
    std::string     msConnInfoDBLogin;
    std::string     msConnInfoDBPasswd;

public:
    _s_transact(const std::string &db,
                const std::string &dblogin,
                const std::string &dbpasswd)
        :mpConnection(NULL),
        msConnInfoDB(db),
        msConnInfoDBLogin(dblogin),
        msConnInfoDBPasswd(dbpasswd)
    {}
    virtual ~_s_transact() ;


    virtual _pg_stat* statement();    // throw _pg_db_error

    virtual void connect();     // throw _pg_db_error
    virtual void disconnect();

    virtual void commit();      // throw _pg_db_error
    virtual void rollback();    // throw _pg_db_error

};

//----------
_s_transact::~_s_transact()
{
    disconnect();
}

//----------
void _s_transact::connect()
{
    IFNOTNULLTHROW(mpConnection,"SAConnection!=NULL, already initialized");


    MLOG(LOGL8_TRAC) << "new{" << endl;

    mpConnection = new SAConnection();
    try{
        mpConnection->Connect(std_to_sastr(msConnInfoDB),
                              std_to_sastr(msConnInfoDBLogin),
                              std_to_sastr(msConnInfoDBPasswd),
                              SA_PostgreSQL_Client);
        mpConnection->setAutoCommit(SA_AutoCommitOff);    // manual commit mode

        //
        // Use utc timestamptz when read/write from/to postgres, owing to, postgres server will not convert timestamp to his local time.
        // For current session (SAConnection) only.
        /*std::unique_ptr<_statement> t(statement());
        t->exec("set time zone 'UTC';");*/
        // To reduce time lost, because of long delete t _statement( which call commit() ) execute this code in
        // _s_transact::statement()
    } catch (const SAException& E) {

        MLOG(LOGL8_TRAC) << "}" << endl;
        throw _pg_db_error(SAERRTEXTSTD(E));
    }

    MLOG(LOGL8_TRAC) << "}" << endl;
}

//----------
void _s_transact::disconnect()
{
    MLOG(LOGL8_TRAC) << "close{" << endl;

    try {
        // Disconnect is optional
        // autodisconnect will ocur in destructor if needed
        if (mpConnection) mpConnection->Disconnect();
        //mConnection.Disconnect();
    } catch (const SAException &E) {
        //try{ mConnection.Rollback(); } catch(...) {}
        // SAConnection::Rollback() can also throw an exception (if a network error for example) we will be ready.
        if(mpConnection) try{ mpConnection->Rollback(); } catch(...) {}
    }
    DEL(mpConnection);

    MLOG(LOGL8_TRAC) << "     }" << endl;
}

// ----------
void _s_transact::rollback()
{
    IFNULLTHROW(mpConnection,"SAConnection not initialized, no connect()");
    try{ mpConnection->Rollback(); }
    catch (const SAException& E) {throw _pg_db_error(SAERRTEXTSTD(E));}
}

// ----------
void _s_transact::commit()
{
    IFNULLTHROW(mpConnection,"SAConnection not initialized, no connect()");
    try{ mpConnection->Commit(); }
    catch (const SAException& E) {throw _pg_db_error(SAERRTEXTSTD(E));}
}

// ----------
_pg_stat *_s_transact::statement()
{
    IFNULLTHROW(mpConnection,"SAConnection not initialized, no connect()");
    //_statement* pnewst = new _s_statement(mpConnection);
    /// Use utc timestamptz when read/write from/to postgres
    //try{ pnewst->exec("set time zone 'UTC';"); } catch (const SAException& E) { /*Do nothing*/ }
    //return pnewst;
    return new _s_statement(mpConnection);
}


// ----------
_pg_tr* create_sqlapi_transact(const std::string& db, const std::string& user, const std::string& passwd)
{
    //"127.0.0.1@abs_db","abs_admin","ubuntu"
    return new _s_transact(db,user,passwd);
}
