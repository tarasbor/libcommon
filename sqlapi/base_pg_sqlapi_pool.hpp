#ifndef BASE_CONNECT_POOL_HPP
#define BASE_CONNECT_POOL_HPP

#include <list>
#include <algorithm>
#include <assert.h>
//#include <thread/eqthread.hpp>

//#include <system/misc.hpp>
#include <sqlapi/base.hpp>
#include <sqlapi/base_pg_sqlapi.hpp>
#include <udefs.hpp> // DEL




// --------------------------------------------------------------------------
// Global static thread safe class managing connections
class _g_db_connect_pool
{
public:
    _g_db_connect_pool() {}
    ~_g_db_connect_pool() {}

    /// Currently not throw. Check database connect manual.
    /// If create connect pool, then could throw.
   static  void init(const std::string &db, const std::string &user, const std::string &passwd){    // throw
        msDb = db;
        msUser = user;
        msPasswd = passwd;
        //Poco::ScopedLock<Poco::Mutex> lockguard(mMutex);
        /*for(int i=0; i<2; i++)     {
            _db_connect* pdbc = create_new_pg_connection();
            pdbc->connect(connection_string);
            mpFreeConnects.push_back(pdbc);
        }*/
    }

    /// Not throw. SAConnection construct, but nothing to do before _transact::connect() invoke.
    static _pg_tr* get_connect(void) {
        //Poco::ScopedLock<Poco::Mutex> lockguard(mMutex);
/*        mMutex.lock();
        while(mpFreeConnects.empty())
        {
            mMutex.unlock();
            Sleep(100); // !! wait timeout before another thread free connection
            mMutex.lock();
        }
        _db_connect* pdbc = mpFreeConnects.back();
        mpFreeConnects.pop_back();
        mpBusyConnects.push_back(pdbc);
        mMutex.unlock();*/
        _pg_tr* pdbc = create_sqlapi_transact(msDb,msUser,msPasswd);

        return pdbc;
    }

    /// Not throw.
    static void free_connect(_pg_tr* freeConnect) {
        DEL(freeConnect);
        /*Poco::ScopedLock<Poco::Mutex> lockguard(mMutex);

        std::list<_db_connect*>::iterator findIter = std::find(mpBusyConnects.begin(),mpBusyConnects.end(),freeConnect);
        if(findIter != mpBusyConnects.end())     {
            mpFreeConnects.push_back(freeConnect);
            mpBusyConnects.erase(findIter);
        }*/
    }


private:
    static std::string msDb;
    static std::string msUser;
    static std::string msPasswd;
/*    static  Poco::Mutex mMutex;
    static  std::list<_db_connect*> mpFreeConnects;
    static  std::list<_db_connect*> mpBusyConnects;*/
};


// -------------------------------------------------------------------------------------
// Class for useful storing _transact pointer.
// When destruct it automatically release  _transact* (or return to pool in future TO DO).
// Should be used in one scope. Create in stack or std::unique<>.
// Copy and assign forbidden.
class _db_transact_box
{
private:
    _db_transact_box &operator=(const _db_transact_box &d);
    _db_transact_box(const _db_transact_box &d);

public:
    _db_transact_box():mpTransaction(NULL) { mpTransaction = _g_db_connect_pool().get_connect();}
    ~_db_transact_box() { _g_db_connect_pool().free_connect(mpTransaction); }

    _pg_tr* tr() { assert(mpTransaction!=NULL); return mpTransaction; } // don`t do anything with this pointer(copy, assign etc.)

private:
    _pg_tr* mpTransaction;
};


#endif // BASE_CONNECT_POOL_HPP
