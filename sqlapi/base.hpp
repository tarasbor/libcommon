#ifndef BASE_HPP
#define BASE_HPP

#include <string>
#include <system/time.hpp>
#include <json/json_misc.hpp>

class _pg_tr;
class _pg_stat;


using namespace std;

//#ifdef WINDOWS
//    // on Windows we use MS RUNTIME - it use "%I64i" to print long long
//    #define ID_PRINT	"%I64i"
//    typedef __int64 __db_id_type;
//#else
    // on Linux runtime use "%Li" to print long long
    // In postgreSQL we use SERIAL which is 4 bytes ( 1 to 2 147 483 647) like sign int.
    //   Postgres don`t have unsigned values. c++ int is 4 bytes (from -2 147 483 648 to 2 147 483 647 = INT_LEAST32_MAX)
    //   so we will restrict negative part in constructor
    // change _db_id.from_str if change type.
    #define ID_PRINT	"%i"
    typedef int __pg_id_type;
    #define MAX_DBID_VALUE INT_LEAST32_MAX
    #define MIN_DBID_VALUE 0
//#endif

class _pg_id; // instead of db2 _db_id


// ----------------------------------------------------
class _pg_db_error:public _db_error
{    
    public:

      _pg_db_error(const string &s):_db_error(s) {}
      inline const string &err_st() const { return error_st(); }
};

// -----------------------------------------------------------------
class _pg_id
{
    private:

      __pg_id_type id_val;

    public:

      _pg_id():id_val(0) {}
      _pg_id(const string &s) { from_str(s.c_str()); }
      _pg_id(const _pg_id &s) { id_val=s.id_val; }
      _pg_id(const __pg_id_type &s) {
          id_val=0;
          if(s<MIN_DBID_VALUE)
              throw _pg_db_error("ERROR: _pg_id is out of postgres serial range."); /*throw exception not enought, because compiler can implicit cast e.g. long int 6147483647 to int=1852516351 before enter here.*/
          id_val=s;
      }

      _pg_id& operator=(const std::string &rhs) { this->from_str(rhs.c_str()); return *this; }

      bool operator==(const _pg_id &i) const { return id_val==i.id_val; }
      bool operator!=(const _pg_id &i) const { return id_val!=i.id_val; }
      bool operator<(const _pg_id &i) const { return id_val<i.id_val; }
      bool operator>(const _pg_id &i) const { return id_val>i.id_val; }


      bool is_set() const { return id_val>0?true:false; }
      void undef() { id_val=0; }

      __pg_id_type &value() { return id_val; }
      const __pg_id_type &value() const { return id_val; }

      std::string   to_string() const { char ch[32]; value_str(ch); return std::string(ch); }
      const char*   value_str(char *to) const { sprintf(to,ID_PRINT,id_val); return to; }
      void from_str(const char *from) {
          long long i = strtoq(from,0,10); // string to 64bit long long int
          if(i<MIN_DBID_VALUE||i>MAX_DBID_VALUE)
            throw _pg_db_error("_pg_id-ERROR: _pg_id is out of typedef __pg_id_type range (postgres SERIAL = 4 bytes).");
          id_val = i;
      }
      inline void from_str(const string &from) { from_str(from.c_str()); }

      //operator __int64() { return id_val; };
};

// -----------------------------------------------------------------

// -----------------------------------------------------------------
// Contain pointer to command.
// Contain query results and input methods for generating result
class _pg_stat
{
private:
    _pg_stat &operator=(const _pg_stat &); // forbid assign operator
    _pg_stat(const _pg_stat &); // forbid copy operator
public:

    _pg_stat() {}
    virtual ~_pg_stat() {}


    // Подставляет в запрос значения вместо :<>
    virtual void push(const char *in) = 0;
    virtual void push(const std::string &in) = 0;
    virtual void push(const _pg_id &in) = 0;
    virtual void push(const _time &in) = 0;
    virtual void push(const int in) = 0;
    virtual void push(const json& in) = 0;
    virtual void push(const float in) = 0;
    virtual void push(const bool in) = 0;
    
    virtual void exec(const std::string &) = 0;   

    //Методы упрощающие вызов insert и update запросов
    //(INSERT) в id возвращает id созданной строки
    //(UPDATE) возвращает количество затронутых строк
    virtual void exec_insert(const string& table_name, const json& jObj, _pg_id *createdId=NULL) = 0;
    virtual int exec_update(const string& table_name, const json& jArgs, const string& where_args = "") = 0;
    virtual int exec_delete(const string& table_name, const string& where_args) = 0;

    virtual int count_columns() = 0;          //Количество полей в ответе
    virtual long count_rows_affected() = 0;    //Поличество строк затронутых запросом

    // получение первой, переход к следующей строке
    virtual bool fetch() = 0;

    // Возвращает значение из поля таблицы и переходит к следующему
    virtual void pop(std::string &out) = 0;
    virtual void pop(_pg_id &out) = 0;
    virtual void pop(_time &out) = 0;
    virtual void pop(int &out) = 0;
    virtual void pop(json& out) = 0;
    virtual void pop(float& out) = 0;
    virtual void pop(bool& out) = 0;

    virtual std::string pop_nnull_retStr(const std::string &def = "") = 0;
    virtual int     pop_nnull_retInt(const int def = 0) = 0;
    virtual _pg_id  pop_nnull_dbId(const _pg_id &def = 0) = 0;
    virtual bool    pop_nnull_retBool(const bool def = false) = 0;

    // Возвращает значение из поля таблицы (с установкой значения по умолчанию в случае null)
    virtual bool pop_nnull(std::string &out, const std::string &def="") = 0;
    virtual bool pop_nnull(_pg_id &out, const _pg_id& def = 0) = 0;
    virtual bool pop_nnull(_time &out, const _time &def=_time()) = 0;
    virtual bool pop_nnull(int& out, const int def = 0) = 0;
    virtual bool pop_nnull(json& out, const json& def = json({})) = 0;
    virtual bool pop_nnull(float& out, const float def = 0.0) = 0;
    virtual bool pop_nnull(bool& out, const bool def = false) = 0;

};

// -----------------------------------------------------------------
// Contain pointer to database connection.
// Thread unsafe. Use only one connection in one thread.
// Use commits sequentially.
class _pg_tr   // thread unsafe!
{
    private:
      _pg_tr &operator=(const _pg_tr &_t);// pqxx forbid assign
      _pg_tr(const _pg_tr &_t);// pqxx forbid copy
    protected:
    public:

    _pg_tr() {}   // NOte:pqxx forbid constructor without connection.
    virtual ~_pg_tr() {}


//      virtual void start() = 0;   // postgres begin transaction when pqxx::work constructed    
/*    virtual std::string quote(const string &) const = 0;
    virtual std::string quote(const _db_id &) const = 0;
    virtual std::string quote(const int &) const = 0;
*/
//
//    virtual void exec_update(std::string table_name, json &jArgs, string where_arg) = 0;    //Выполняет запрос на update с параметрами из jArgs
//    virtual void exec_insert(std::string table_name, json &jArgs) = 0;                      //Выполняет запрос на insert с параметрами из jArgs
    virtual _pg_stat* statement() = 0;
    virtual void connect() = 0;     // throw
    virtual void disconnect() = 0;  // no exception

    virtual void commit() = 0;      // throw
    virtual void rollback() = 0;    // throw
};


// -----------------------------------------------------------------
/*class _db_connect //This class thread unsafe. Use every object, and related to this objects, only by single thread.
{
    private:
      _db_connect &operator=(const _db_connect &d);
      _db_connect(const _db_connect &d);

    public:
//      enum tr_rw_mode { trm_read=0, trm_write, trm_readwrite };
//      enum tr_w_mode { trwm_wait };

      _db_connect() {}
      virtual ~_db_connect() {}


      virtual void connect(const string &) = 0;
//      virtual void connect(const string &dsn,const string &user,const string &pass) = 0;
      virtual void disconnect() = 0;

      virtual _pg_tr *new_pg_trion() = 0; // create new transaction and return pointer. Use with std::unique_ptr
      virtual bool was_connected() = 0;
//      virtual void lock() = 0;
//      virtual void unlock() = 0;
//      virtual _pg_tr *transaction() = 0;
//      virtual _pg_tr *thread_pg_trion(bool create = false) = 0;
};
*/

/*
class _db_pg_tr_pool
{
private:
    _db_pg_tr_pool &operator=(const _db_pg_tr_pool &);
    _db_pg_tr_pool(const _db_pg_tr_pool &);

public:
    _db_pg_tr_pool() {}
    virtual ~_db_pg_tr_pool() {}

    _pg_tr* transaction() {}
//    virtual
};
*/


#endif // BASE_HPP
