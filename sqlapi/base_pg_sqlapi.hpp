#ifndef BASE_PG_SQLAPI_HPP
#define BASE_PG_SQLAPI_HPP
#include "base.hpp"

//_db_connect*   create_new_pg_connection();

// not throw
_pg_tr*  create_sqlapi_transact(const std::string& db, const std::string& user, const std::string& passwd);   // no throw, only create

#endif // BASE_PG_SQLAPI_HPP
