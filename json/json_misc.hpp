#ifndef JSON_MISC_H
#define JSON_MISC_H
#include <json/json.hpp>
#include <string>
#include <obj_db5/base.hpp>

using json = nlohmann::json;
using namespace std;

bool json_value_to_str(const json &j, std::string &out);


//Ищет элемент в json и возвращает его в требуемом типе
int             json_find_value_int(const json &j, const std::string& key, const int defV = 0);
std::string     json_find_value_str(const json &j, const std::string& key, const string& defV = "");
unsigned long long json_find_value_ull(const json &j, const std::string& key, const unsigned long long defV = 0);
_db_id          json_find_value_db_id(const json &j, const std::string& key, const _db_id& defV = 0);

#endif // JSON_MISC_H
