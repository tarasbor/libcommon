#include "json_misc.hpp"
#include <log2/log.hpp>

#define MLOG(A) _log() << _log::module_level("json_misc",(A))

bool json_value_to_str(const json &j, std::string &out)
{
    out="";
    try {
        if(j.is_null())
        {
            MLOG(1) << "Warning: json_value_to_str() failed, json value empty(null)" << endl;
            return false;
        } else if(j.is_string()) {
            out = j.get<std::string>();
            return true;
        } else if(j.is_number_unsigned()) // convert
        {
            unsigned long long i = j.get<unsigned long long>();
            out = std::to_string(i);
            return true;
        }
        else if(j.is_number_integer())
        {
            long long i = j.get<long long>();
            out = std::to_string(i);
            return true;
        }
        else if(j.is_boolean())
        {
            bool b = j.get<bool>();
            if(b) out = "1"; else out = "0";
            return true;
        }
        else if(j.is_number_float())
        {
            float f = j.get<float>();
            out = std::to_string(f);
            return true;
        }
        else {
            MLOG(1) << "Warning: json_value_to_str() failed" << endl;
            return false;
        }
    } catch (json::exception &e) {
        MLOG(1) << "Warning: Exception threw, while json_value_to_str() : " << e.what() << endl;
        return false;
    }
}

int json_find_value_int(const json &j, const string &key, const int defV)
{
    try
    {
        //Проверки
        if(j.is_null()) return defV;
        json::const_iterator itJ = j.find(key);
        if(itJ==j.end()) return defV;
        const json& jVal = itJ.value();
        if(jVal.is_null()) return defV;
        //преобразование
        int otv = defV;
        if(jVal.is_string())                 { otv = std::stoi(jVal.get<std::string>()); }
        else if(jVal.is_number_integer())    { otv = jVal.get<int>(); }
        else if(jVal.is_number_float())      { otv = jVal.get<float>(); }
        else if(jVal.is_boolean())           { bool b = jVal.get<bool>(); if(b) otv = 1; else otv = 0; }
        return otv;
    }
    catch (json::exception &e) { return defV; }
    return defV;
}

string json_find_value_str(const json &j, const string &key, const string &defV)
{
    try
    {
        //проверки
        if(j.is_null()) return defV;
        json::const_iterator itJ = j.find(key);
        if(itJ==j.end()) return defV;
        const json& jVal = itJ.value();
        if(jVal.is_null()) return defV;
        //преобразование
        std::string otv = defV;
        if(jVal.is_string())                 { otv = jVal.get<std::string>(); }
        else if(jVal.is_number_unsigned())   { otv = std::to_string(jVal.get<unsigned long long>()); }
        else if(jVal.is_number_integer())    { otv = std::to_string(jVal.get<long long>()); }
        else if(jVal.is_number_float())      { otv = std::to_string(jVal.get<float>()); }
        else if(jVal.is_boolean())           { bool b = jVal.get<bool>(); if(b) otv = "1"; else otv = "0"; }
        else if(jVal.is_structured())        { otv = jVal.dump(); }
        return otv;
    }
    catch (json::exception &e) { return defV; }
    return defV;
}

unsigned long long json_find_value_ull(const json &j, const string& key, const unsigned long long defV)
{
    try
    {
        //Проверки
        if(j.is_null()) return defV;
        json::const_iterator itJ = j.find(key);
        if(itJ==j.end()) return defV;
        const json& jVal = itJ.value();
        if(jVal.is_null()) return defV;
        //преобразование
        unsigned long long otv = defV;
        if(jVal.is_string())                 { otv = std::stoull(jVal.get<std::string>()); }
        else if(jVal.is_number_unsigned())   { otv = jVal.get<unsigned long long>(); }
        else if(jVal.is_number_integer())    { otv = jVal.get<long long>(); }
        else if(jVal.is_number_float())      { otv = jVal.get<float>(); }
        else if(jVal.is_boolean())           { bool b = jVal.get<bool>(); if(b) otv = 1; else otv = 0; }
        return otv;
    }
    catch (json::exception &e) { return defV; }
    return defV;
}


_db_id  json_find_value_db_id(const json &j, const string& key, const _db_id& defV)
{
    std::string buff = json_find_value_str(j,key);
     _db_id  buffid;
    try{ buffid.from_str(buff); } catch ( const _db_error& e) { return defV; }
    return (buffid.is_set()==true) ? buffid : defV;
}
