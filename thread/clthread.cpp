// clthread.cpp: implementation of the pthread class.
//
//////////////////////////////////////////////////////////////////////

#include "clthread.hpp"

#ifdef WINDOWS
    #include <process.h>
#else
    #include <unistd.h>
#endif

#ifdef WINDOWS
DWORD WINAPI ThreadLoop(LPVOID param);
#else
int ThreadLoop(void *param);
#endif

#ifdef THREAD_USE_LOG
#include <log.hpp>
#include <string.h>
#endif

//#include <iostream>
//#include <string.h>

#define MLOG(A) _log() << _log::_set_module_level("thread",(A))

using namespace std;

// ------------------------------------------------------------------
_thread::_thread()
{
	lock.lock();
    status=TH_STATUS_NOT_CREATED;
	command=TH_COMMAND_NOP;

    SetSleepTime();
#ifdef WINDOWS
    handle=(void *)_beginthreadex(NULL,8192,
                   (unsigned __stdcall (*)(void *))ThreadLoop,
                   //(void *)this,CREATE_SUSPENDED,(unsigned *)&(threadid));
                   (void *)this,0,(unsigned *)&(threadid));
#else
    pthread_attr_t attr;
    int err=pthread_attr_init(&attr);
	if (err)
	{
		#ifdef THREAD_USE_LOG
		MLOG(0) << "pthread_attr_init error #" << err <<  strerror(err) <<endl;
		#endif
	}
    err=pthread_create(&threadid,&attr,(void *(*)(void *))ThreadLoop,(void *)this);
	if (err)
	{
		#ifdef THREAD_USE_LOG
		MLOG(0) << "pthread_create error #" << err <<  strerror(err) <<endl;
		#endif
	}
#endif

    status=TH_STATUS_PAUSED;
	lock.unlock();
}

// ------------------------------------------------------------------
_thread::~_thread()
{
    Kill();
    #ifndef WINDOWS
	pthread_join(threadid,NULL);
    #endif
}

// ------------------------------------------------------------------
int _thread::Start()
{
    if (status<0) return -1;

    lock.lock();
    status=TH_STATUS_LOOP;
    command=TH_COMMAND_LOOP;
    lock.unlock();

    return 0;
}

// ------------------------------------------------------------------
int _thread::Stop()
{
    if (status<0) return -1;

    lock.lock();
    status=TH_STATUS_PAUSED;
    command=TH_COMMAND_NOP;
    lock.unlock();

    return 0;
}

// ------------------------------------------------------------------
void _thread::Kill()
{
    lock.lock();
    command=TH_COMMAND_KILL;
    Sleep(sleep_time*2);
    lock.unlock();
}


// ------------------------------------------------------------------
#ifdef WINDOWS
DWORD WINAPI ThreadLoop(LPVOID param)
#else
int ThreadLoop(void *param)
#endif
{
    _thread *obj=(_thread *)param;

    while(1)
    {
        obj->lock.lock();
        if (obj->command==TH_COMMAND_KILL)
        {
            obj->status=TH_STATUS_KILLED;
            obj->lock.unlock();

            #ifdef WINDOWS
            _endthreadex(0);
            #else
            //cout << "end thread" << endl;
            pthread_exit(0);
            #endif

            return 0;
        }
        else if (obj->command==TH_COMMAND_LOOP)
        {
            if (obj->Poll()<0)
            {
                obj->status=TH_STATUS_CRUSH;
                obj->lock.unlock();

                #ifdef WINDOWS
                _endthreadex(-1);
                #else
                //cout << "end thread" << endl;
                pthread_exit(0);
                #endif

                return -1;
            }
        }
        obj->lock.unlock();
        Sleep(obj->sleep_time);
    }
}

// ------------------------------------------------------------------
/// newSleepTime - in Milliseconds.
void _thread::SetSleepTime(int newSleepTime)
{
    lock.lock();
    sleep_time=newSleepTime;
	lock.unlock();
}

// ------------------------------------------------------------------
int _thread::GetSleepTime()
{
    return sleep_time;
}

// ------------------------------------------------------------------
int _thread::GetStatus()
{
    return status;
}

// ------------------------------------------------------------------
// WINDOWS API Sleep emulation
#if !defined(WINDOWS)
void Sleep(int msec)
{
        // usleep arg in MICRO (not in MILI) seconds
        usleep(msec*1000);
}
#endif

// ---------------------------------------------------------------------------
// LOCK class implementation
// ---------------------------------------------------------------------------
_locker::_locker()
{
#ifdef WINDOWS
        InitializeCriticalSection(&cs);
#else
        pthread_mutexattr_init(&attr);
        pthread_mutexattr_settype(&attr,PTHREAD_MUTEX_RECURSIVE);
        pthread_mutex_init(&mutex,&attr);
#endif
}

// ------------------------------------------------------------------
_locker::~_locker()
{
#ifdef WINDOWS
        DeleteCriticalSection(&cs);
#else
        pthread_mutex_destroy(&mutex);
        pthread_mutexattr_destroy(&attr);
#endif
}

// ------------------------------------------------------------------
void _locker::lock()
{
#ifdef WINDOWS
        EnterCriticalSection(&cs);
#else
        pthread_mutex_lock(&mutex);
#endif
}

// ------------------------------------------------------------------
bool _locker::try_lock()
{
#ifdef WINDOWS
        if (TryEnterCriticalSection(&cs)) return true;
#else
        if (pthread_mutex_trylock(&mutex)==0) return true;
#endif
        return false;
}

// ------------------------------------------------------------------
void _locker::unlock()
{
#ifdef WINDOWS
        LeaveCriticalSection(&cs);
#else
        pthread_mutex_unlock(&mutex);
#endif
}

// ------------------------------------------------------------------
unsigned long get_current_thread_id()
{
#ifdef WINDOWS
   return GetCurrentThreadId();
#else
   return pthread_self();
#endif
}
