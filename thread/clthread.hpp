// clthread.hpp: interface for the thread (multi-platform) class
//
//////////////////////////////////////////////////////////////////////

//#define WINDOWS

#ifndef __cl_thread__
#define __cl_thread__

/*! \defgroup cl_thread Thread Lib
    \brief Библиотека кросплатформенной реализации потоков выполнения.
    @{
*/

/*!
  \defgroup cl_thread_about О библиотеке.
  @{
  \section one Введение
  Данная библиотека предназначена для более простого и удобного написания мультипоточных кросплатформенных (Win и Linux) приложений.
  \nФактически библиотека предоставляет базовый абстрактный класс потока _thread и класс _locker для синхронизации потоков.
  \n
  \note Win - используется API Windows, Linux - используется POSIX threads (pthreads)

  \nПользовательский код потока задается в классе наследнике от _thread в функции _thread::Poll().

  \section example Краткий пример.
  Допустим нам нужен поток, в котором мы будем принимать данные из сети в некий буфер.
  \n

  \code{.cpp}
  class _net_thread:public _thread        // Это класс - наследник
  {                                       // В нем надо определить и реализовать абстрактную функцию Poll
    virtual int Poll();
  }

  int _net_thread::Poll()
  {
    get_from_net_and_place_to_buffer();
    return 0;
  }
  ....

  _thread th1=new _net_thread();          // где то в коде создаем поток
  th1->Start();                           // и стартуем его

  ....                                    // где то в коде мы можем проверить состояние потока
  if (th1->GetStatus()==TH_STATUS_CRUSH)
  {
    // ох-ох, что то случилось в потоке (подробнее см. ниже)
  }

  .....
  th1->Kill();                            // где то в коде мы можем "убить" поток
  while(th1->GetStatus()==TH_STATUS_LOOP) // ждать пока он не умрет
    Sleep(100);
  delete th1;                             // освободить память
  \endcode
  @}

  \defgroup cl_thrtead_thread_states Состояния потока.
  @{
  Класс потока может быть в нескольких состояния:
  \n- TH_STATUS_NOT_CREATED - реально поток не создан, система отказала в его создании (например, исчерпаны ресурсы системы);
  \n- TH_STATUS_PAUSED - поток создан, но остановлен;
  \n- TH_STATUS_LOOP - поток создан и запущен (выполняется);
  \n- TH_STATUS_KILLED - поток "убит" по запросу клиенткского кода;
  \n- TH_STATUS_CRUSH - поток "умер" сам, т.к. в процессе выполнения (в _thread::Poll()) что-то случилось такое, что дальнейшее выполнение не возможно или не имеет смысла.
  @}

  \defgroup cl_thread_poll_details Подробнее о Poll и об управлении потоком.
  @{
  \n Практически в каждом низкоуровневом API поток имеет функцию - которая опделяет "начало" потока. И обычно код этой функции построен на цикле (часто бесконечном).
  В цикле периодически выполняется какая-то полезная работа, например обработка поступившей порции данных, после чего поток ждет новой порции и т.п.
  \n Класс _thread как раз организовывает вызов Poll в цикле. Интервал между вызовами задается SetSleepTime (в милисекундах).
  \n
  \n Теоретически пользовательский код заключенный в Poll может выполняться сколь угодно долго, однако на практике этого лучше избегать.
  При длительном выполнении кода в Poll или длительной блокировки потока внутри Poll управляющий поток (часто родительский поток) не сможет в случае необходимости
  остановить или убить "подвисший" поток и освободить ресурсы. Особенно это актуально, если код Poll содержит блокирующий ввод-вывод.
  \section poll_bad_example "Плохой" пример.
  Например, у нас есть браузер, который создает поток для получения большого файла по сети.
  Функцию Poll можно было бы организовать так (псевдокод):
  \code{.cpp}
  int _example_thread::Poll()
  {
     connect_to_server();
     while(!end_of_file())
     {
       get_data_portion_from_net();
     }
  }
  \endcode
  Если файл большой и/или сеть медленная, то данная реализация Poll может выполняться долго, и если пользователь, например, решил прекратить загрузку,
  то управляющий поток должен "убить" (фактически остановить и освободить ресурсы) поток загрузки.
  \code{.cpp}
  _example_thread *download_thread=new _example_thread;
  download_thread->Start();
  ....
  download_thread->Kill();
  delete download_thread;
  \endcode
  Однако - delete освободит память, которую скорее всего использует код в Poll, что приведет к краху всего приложения.
  Мы должны дождаться подтверждения от потока, что он остановлен (убит) и вызова Poll не происходит и не произойдет.
  \code{.cpp}
  _example_thread *download_thread=new _example_thread;
  download_thread->Start();
  ....
  download_thread->Kill();
  while(download_thread->GetStatus()==TH_STATUS_LOOP) Sleep(1);
  delete download_thread;
  \endcode
  Из-за нашей "плохой" реализации Poll цикл ожидания закончиться не скоро (пока не загрузиться весь файл), что часто неприемлемо
  для логики работы большинства приложений. Представте, пользователь нажал крестик - а ему говорят, ок - я завершу работу, но сначала
  все же загружу файл -)).
  \note При ожидании сравнивать статус потока лучше всего с TH_STATUS_LOOP - во всех других статусах вызов Poll не происходит.

  \section poll_good_example "Хороший" пример.
  Переделаем реализацию Poll:
  \code{.cpp}
  int _example_thread::Poll()
  {
     if (my_state==do_connect)
     {
        connect_to_server();
        my_state=do_download;
        return 0;
     }

     if (my_state==do_download)
     {
        get_data_portion_from_net();
        if (end_of_file()) my_state=all_done;
        return 0;
     }

     if (my_state==all_done)
     {
        return 0;
     }

     return 0;
  }
  \endcode
  Теперь Poll за раз будет делать только кусок работы (подключение, получение порции данных или ничего - в зависимости от внутреннего состояния,
  в переменной my_state). И максимальное время ожидания при убийстве потока сильно сократиться.

  \section poll_perfect_example "Превосходный" пример.
  Реализация Poll, приведенная выше, все же обладает потенциальными проблемами. Например, сетевые функции (тот же recv в сокетах) блокирует выполнение потока
  пока не получит данные по сети. Если сервер вдруг прекратил подачу нам данных, то recv подвиснет на довольно долгое время (пока операционная система не
  не поймет, что TCP соединение разорвано).
  Превосходная реализация Poll должна стараться избегать блокировки на операциях ввода-вывода - для этого, например, функция get_data_portion_from_net() должна
  возвразщать управление в Poll, если данных по сети нет, а не ждать их появления.

  \section poll_return_value Значение, возвращаемое Poll.
  Если есть необходимость остановить поток из кода Poll, например, случилась ошибка или ситуация после которых выполнение потока
  не имеет смысла - реализация Poll должна вернуть ОТРИЦАТЕЛЬНОЕ значение.
  Во всех остальных случаях - вернуть 0 иди ПОЛОЖИТЕЛЬНОЕ.
  \note При возврате Poll отрицательного результата убивается системный поток, а класс потока переходит в состояние TH_STATUS_CRUSH.
  @}
*/

#ifdef WINDOWS
  #include <windows.h>
#else
  #include <pthread.h>
#endif

#define TH_COMMAND_KILL         -1
#define TH_COMMAND_NOP          0
#define TH_COMMAND_LOOP         1


#define TH_STATUS_NOT_CREATED   -1
#define TH_STATUS_KILLED        -2
#define TH_STATUS_CRUSH         -3
#define TH_STATUS_PAUSED        0
#define TH_STATUS_LOOP          1

#if !defined(WINDOWS)
  //#define Sleep(x) usleep((x)*1000)
  void Sleep(int msec);
#endif

/*! \class _locker
	\brief Блокиратор - служит для ограничения досупа к ресурсам для одного и только для одного потока.

	Работает аналогично CriticalSection в Win API или  pthread_mutex_t в POSIX Threads.
	Фактически блокирует выполение потока при запросе того или иного ресурса, если ресурс заблокирован другим потоком.
	\n\n
	Пример - вывод на консоль (использование ресурсов консоли) одномоментно получит только один поток (А или В),
	второй поток при вызове lock() будут приостановлен (до разблокировки console_locker первым потоком):
	\code{.cpp}
	_locket console_locker;

	....

	// поток А
	console_locker.lock();
	out_to_console("Thread A\n");
	console_locker.unlock();

	....

	// поток B
	console_locker.lock();
	out_to_console("Thread B\n");
	console_locker.unlock();
	\endcode

	\nВызовы lock и unlock() могут быть "вложены" в одном потоке, они должны быть парными (сколько lock, столько и unlock).
	\code{.cpp}
	_locket console_locker;

	....

	// поток А
	console_locker.lock();
	out_to_console("Thread A 1 lock\n");
	...
	console_locker.lock();
	out_to_console("Thread A 2 lock\n");  // т.к. поток уже заблокирован потоком А, то блокировки выполнения не произойдет
	...
	console_locker.unlock();
	...
	console_locker.unlock();

	....

	\endcode

*/
// -------------------------------------------------------------------
class _locker
{
    private:

        #ifdef WINDOWS
        CRITICAL_SECTION cs;
        #else
        pthread_mutex_t mutex;
        pthread_mutexattr_t attr;
        #endif

        _locker(const _locker &l);
        _locker &operator=(const _locker &l);

    public:
        /// Конструктор
        _locker();

        /// Деструктор
        ~_locker();

        /*!
        * \brief Блокировка ресурса.
        */
        void lock();

        /*!
        * \brief Разблокировка ресурса.
        */
        void unlock();

        /*!
        * \brief Попытаться заблокировать ресурс.
        * \return true - вышла блокировка "нашим" потоком, false - блокирка не выполнена, т.к. ресурс уже заблокирован другим потоком
        */
        bool try_lock();
};

// -------------------------------------------------------------------
class _thread
{
    #ifdef WINDOWS
        friend DWORD WINAPI ThreadLoop(LPVOID);
    #else
        friend int ThreadLoop(void *);
    #endif

    private:

        int status;
        int command;
        int sleep_time;
        _locker lock;

        #ifdef WINDOWS
            HANDLE handle;
            DWORD threadid;
        #else
            pthread_t threadid;
        #endif
        int Number;

    public:

        /// Конструктор
        _thread();
        /// Деструктор
        virtual ~_thread();

        /*!
        * \brief Запускает поток на выполнение.
          \return -1 если запуск невозможен, в остальных случаях >=0
        */
        int Start();

        /*!
        * \brief Останавливает выполнение потока (но не удаляет системные ресурсы).
          \return -1 если остановить невозможено, в остальных случаях >=0
        */
        int Stop();

        /*!
        * \brief Останавливает выполнение потока и удаляет системные ресурсы.
        */
        void Kill();

        /*!
        * \brief Устанавливает время простоя между возвращением из Poll и следующим вызовом Poll.
          \param [in] newSleepTime интервал простоя в милисекундах
        */
        void SetSleepTime(int newSleepTime = 100);

        /*!
        * \brief Получить время простоя.
          \return Установленное время простоя в милисекундах.
        */
        int GetSleepTime();

        /*!
        * \brief Получить статус потока.
          \return Текущий статус потока (одно из значений, определенных в макросах TH_STATUS_*)
        */
        int GetStatus();

        /*!
        * \brief Выполнить код потока
          \return >=0 - поток будет выполняться дальше, <0 - в ходе выполнения потока случилась ошибка или создалась ситуация, что дальнейшая работа потока не имеет смысла.
          Данную функцию пользователь библиотеки определяет в своем классе, наслудуемом от _thread. Подробнее о Poll написано в разделе ....
        */
        virtual int Poll() = 0;
};

/*!
* \brief Получить идентификатор текущего потока (потока, откуда была вызвана даная функция)
  \return Идентификатор потока из которого вызвали данную функцию.
*/
// -------------------------------------------------------------------
unsigned long get_current_thread_id();

/*!
    @}
*/

#endif
