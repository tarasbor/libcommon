#include "imglib.hpp"
#include <iostream>
#include <fstream>
#include <windows.h>

#include <stdio.h>

#define HAVE_BOOLEAN
#include <jpeglib.h>

using namespace std;
using namespace _imglib;

// ------------------------------------------------------------------
int _imglib::load_bmp(const char *fname,_image *image)
{
    if (image->rgba)
    {
        delete image->rgba;
        image->rgba=NULL;
    }
    
    ifstream f;
    f.open(fname,ios::in | ios::binary);
    if (!f) return -1;

    BITMAPFILEHEADER f_header;
    f.read((char *)&f_header,sizeof(BITMAPFILEHEADER));

    BITMAPINFOHEADER i_header;
    f.read((char *)&i_header,sizeof(BITMAPINFOHEADER));
    /*
    cout << "Width " << i_header.biWidth << endl;
    cout << "Height " << i_header.biHeight << endl;
    cout << "BPP " << i_header.biBitCount << endl;
    cout << "Comp " << i_header.biCompression << endl;
    */

    image->w=i_header.biWidth;
    image->h=i_header.biHeight;

    int seek=f_header.bfOffBits;//-sizeof(BITMAPINFOHEADER);
    f.seekg(seek,ios::beg);

    image->rgba_size=i_header.biWidth*i_header.biHeight*4;

    
    image->rgba=new unsigned char [image->rgba_size];
    
    f.read((char *)image->rgba,image->rgba_size);
    
    f.close();
    return 0;
}

// ------------------------------------------------------------------
int _imglib::write_bmp(const char *fname,_image *image)
{
    if (!image->rgba) return -2;
    
    ofstream f;
    f.open(fname,ios::out | ios::binary);
    if (!f) return -1;

    BITMAPFILEHEADER f_header;
    f_header.bfType = 0x4d42;
    f_header.bfSize = (DWORD)(sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + image->w*image->h*3);
    f_header.bfReserved1 = 0;
    f_header.bfReserved2 = 0;
    f_header.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
    f.write((char *)&f_header,sizeof(BITMAPFILEHEADER));

    cout << "fsize = " << f_header.bfSize << endl;
    cout << "offset = " << f_header.bfOffBits << endl;

    BITMAPINFOHEADER i_header;
    i_header.biSize = sizeof(BITMAPINFOHEADER);
    i_header.biWidth = image->w;
    i_header.biHeight = image->h;
    i_header.biPlanes = 1;  
    i_header.biBitCount = 24;
    i_header.biCompression = BI_RGB;
    i_header.biSizeImage = 0;
    i_header.biXPelsPerMeter = 1;
    i_header.biYPelsPerMeter = 1;
    i_header.biClrUsed = 0;
    i_header.biClrImportant = 0;
    
    f.write((char *)&i_header,sizeof(BITMAPINFOHEADER));
    /*
    cout << "Width " << i_header.biWidth << endl;
    cout << "Height " << i_header.biHeight << endl;
    cout << "BPP " << i_header.biBitCount << endl;
    cout << "Comp " << i_header.biCompression << endl;
    */

    //image->w=i_header.biWidth;
    //image->h=i_header.biHeight;

    //int seek=f_header.bfOffBits;//-sizeof(BITMAPINFOHEADER);
    //f.seekg(seek,ios::beg);

    //image->rgba_size=i_header.biWidth*i_header.biHeight*4;

    
    //image->rgba=new unsigned char [image->rgba_size];

    unsigned char *buffer = new unsigned char [image->w*image->h*3];
    int x;
    int y;

    for(y=0;y<image->h;y++)
    {
        for(x=0;x<image->w;x++)
        {
            buffer[(x+y*image->w)*3]=image->rgba[(x+y*image->w)*4];
            buffer[(x+y*image->w)*3+1]=image->rgba[(x+y*image->w)*4+1];
            buffer[(x+y*image->w)*3+2]=image->rgba[(x+y*image->w)*4+2];
        }
    }
    /*
    int i=0;
    int b=0;
    for(;b<image->w*image->h*3;b++,i++)
    {
        if (b%3==0 && b) i++;
        buffer[b]=image->rgba[i];
    }
    */
    
    f.write((const char *)image->rgba,image->w*image->h*3);

    delete buffer;
    
    f.close();
    return 0;
}

// ------------------------------------------------------------------
int _imglib::load_jpeg(const char *fname,_image *image)
{
    if (image->rgba)
    {
        delete image->rgba;
        image->rgba=NULL;
    }


    struct jpeg_decompress_struct cinfo;
    jpeg_error_mgr jerr;

    cinfo.err = jpeg_std_error(&jerr);
    jpeg_create_decompress(&cinfo);

    FILE * infile;
    if ((infile = fopen(fname, "rb")) == NULL) return -1;
 
    jpeg_stdio_src(&cinfo, infile);

    jpeg_read_header(&cinfo, TRUE);

    jpeg_start_decompress(&cinfo);

    int row_size = cinfo.output_width * cinfo.output_components;
    JSAMPARRAY buffer;

    buffer = (*cinfo.mem->alloc_sarray)
                ((j_common_ptr) &cinfo, JPOOL_IMAGE, row_size, 1);

    image->w=cinfo.image_width;
    image->h=cinfo.image_height;
    image->rgba_size=image->w * image->h * 4;
    image->rgba=new unsigned char [image->rgba_size];

    int i_i=0;

    while(cinfo.output_scanline < cinfo.output_height)
    {
        jpeg_read_scanlines(&cinfo,buffer,1);
        JSAMPROW row=buffer[0];

        int i=0;
        while(i<image->w*3)
        {
            image->rgba[i_i]=row[i];
            i_i++;
            i++;
            image->rgba[i_i]=row[i];
            i_i++;
            i++;
            image->rgba[i_i]=row[i];
            i_i++;
            i++;
            
            image->rgba[i_i]=0xFF;
            i_i++;
        }
    }
        

    jpeg_finish_decompress(&cinfo);

    jpeg_destroy_decompress(&cinfo);

    fclose(infile);
    
    return 0;
}

// ------------------------------------------------------------------
int _imglib::mem_to_jpeg(const char *data,int data_len,_image *image)
{
    if (image->rgba)
    {
        delete image->rgba;
        image->rgba=NULL;
    }


    struct jpeg_decompress_struct cinfo;
    jpeg_error_mgr jerr;

    cinfo.err = jpeg_std_error(&jerr);
    jpeg_create_decompress(&cinfo);
 
    jpeg_mem_src(&cinfo, (unsigned char *)data, data_len);

    jpeg_read_header(&cinfo, TRUE);

    jpeg_start_decompress(&cinfo);

    int row_size = cinfo.output_width * cinfo.output_components;
    JSAMPARRAY buffer;

    buffer = (*cinfo.mem->alloc_sarray)
                ((j_common_ptr) &cinfo, JPOOL_IMAGE, row_size, 1);

    image->w=cinfo.image_width;
    image->h=cinfo.image_height;
    image->rgba_size=image->w * image->h * 4;
    image->rgba=new unsigned char [image->rgba_size];

    int i_i=0;

    while(cinfo.output_scanline < cinfo.output_height)
    {
        jpeg_read_scanlines(&cinfo,buffer,1);
        JSAMPROW row=buffer[0];

        int i=0;
        while(i<image->w*3)
        {
            image->rgba[i_i]=row[i];
            i_i++;
            i++;
            image->rgba[i_i]=row[i];
            i_i++;
            i++;
            image->rgba[i_i]=row[i];
            i_i++;
            i++;
            
            image->rgba[i_i]=0xFF;
            i_i++;
        }
    }
        

    jpeg_finish_decompress(&cinfo);

    jpeg_destroy_decompress(&cinfo);

    
    return 0;
}

// ------------------------------------------------------------------
int _imglib::jpeg_to_mem(_image *image, unsigned char *d, unsigned long *size)
{
    if (!image->rgba) return -1;

    struct jpeg_compress_struct cinfo;
    struct jpeg_error_mgr jerr;

    cinfo.err = jpeg_std_error(&jerr);
    jpeg_create_compress(&cinfo);

    jpeg_mem_dest(&cinfo, &d, size);

    cinfo.image_width = image->w;      /* image width and height, in pixels */
    cinfo.image_height = image->h;
    cinfo.input_components = 3;     /* # of color components per pixel */
    cinfo.in_color_space = JCS_RGB; /* colorspace of input image */

    jpeg_set_defaults(&cinfo);

    jpeg_set_quality(&cinfo, 75, TRUE /* limit to baseline-JPEG values */);

    jpeg_start_compress(&cinfo, TRUE);

    JSAMPROW row_pointer[1];        /* pointer to a single row */
    int row_stride;                 /* physical row width in buffer */

    row_stride = image->w * 3;   /* JSAMPLEs per row in image_buffer */

    unsigned char *buffer = new unsigned char [image->w*image->h*3];
    int i=0;
    int b=0;
    for(;b<image->w*image->h*3;b++,i++)
    {
        if (b%3==0 && b) i++;
        buffer[b]=image->rgba[i];
    }

    while (cinfo.next_scanline < cinfo.image_height)
    {
        row_pointer[0] = & buffer[cinfo.next_scanline * row_stride];
        jpeg_write_scanlines(&cinfo, row_pointer, 1);
    }

    delete buffer;

    jpeg_finish_compress(&cinfo);

    jpeg_destroy_compress(&cinfo);


    return 0;
}

// ------------------------------------------------------------------
int _imglib::write_jpeg(const char *fname,_image *image)
{
    if (!image->rgba) return -1;

    struct jpeg_compress_struct cinfo;
    struct jpeg_error_mgr jerr;

    cinfo.err = jpeg_std_error(&jerr);
    jpeg_create_compress(&cinfo);

    unsigned char *pp = new unsigned char [2*1024*1024];
    unsigned long psize=2*1024*1024;

    FILE * infile;
    if ((infile = fopen(fname, "wb")) == NULL) return -1;

    jpeg_stdio_dest(&cinfo, infile);

    cinfo.image_width = image->w;      /* image width and height, in pixels */
    cinfo.image_height = image->h;
    cinfo.input_components = 3;     /* # of color components per pixel */
    cinfo.in_color_space = JCS_RGB; /* colorspace of input image */

    jpeg_set_defaults(&cinfo);

    jpeg_set_quality(&cinfo, 75, TRUE /* limit to baseline-JPEG values */);

    jpeg_start_compress(&cinfo, TRUE);

    JSAMPROW row_pointer[1];        /* pointer to a single row */
    int row_stride;                 /* physical row width in buffer */

    row_stride = image->w * 3;   /* JSAMPLEs per row in image_buffer */

    unsigned char *buffer = new unsigned char [image->w*image->h*3];
    int i=0;
    int b=0;
    for(;b<image->w*image->h*3;b++,i++)
    {
        if (b%3==0 && b) i++;
        buffer[b]=image->rgba[i];
    }

    while (cinfo.next_scanline < cinfo.image_height)
    {
        row_pointer[0] = & buffer[cinfo.next_scanline * row_stride];
        jpeg_write_scanlines(&cinfo, row_pointer, 1);
    }

    jpeg_finish_compress(&cinfo);

    jpeg_destroy_compress(&cinfo);

    delete buffer;

    fclose(infile);

    return 0;
}
