#ifndef __IMAGE_LIB_H__
#define __IMAGE_LIB_H__

#if defined(WINDOWS)
#include <windows.h>
#else
#endif

#include <cstring>

namespace _imglib
{

    struct _image
    {
        public:
        
            int w;
            int h;
            
            int rgba_size;
            unsigned char *rgba;
    
            _image():w(0),h(0),rgba_size(0),rgba(NULL) {};
            _image(const _image &i):w(i.w),h(i.h),rgba_size(i.rgba_size),rgba(new unsigned char [i.rgba_size]) { memcpy(rgba,i.rgba,rgba_size); };
            
            const _image &operator=(const _image &i)
            {
                if (this==&i) return *this;
                if (rgba) delete rgba;

                w=i.w;
                h=i.h;
                rgba_size=i.rgba_size;
                rgba = new unsigned char [rgba_size];
                memcpy(rgba,i.rgba,rgba_size);

                return *this;
            };
            
            ~_image() { if (rgba) delete rgba; };
    
        private:

             
    };
    
    int load_bmp(const char *fname,_image *image);
    int write_bmp(const char *fname,_image *image);
    
    int load_jpeg(const char *fname,_image *image);
    int mem_to_jpeg(const char *data,int data_len,_image *image);

    int jpeg_to_mem(_image *imgage, unsigned char *d, unsigned long *size);
    int write_jpeg(const char *fname,_image *image);
    
};

#endif
