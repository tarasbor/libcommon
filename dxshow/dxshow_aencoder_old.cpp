#include "dxshow.hpp"

#ifdef WINDOWS

const GUID      CLSID_LAMEFilter = {0xB8D27088, 0xFF5F, 0x4B7C, {0x98, 0xDC, 0x0E, 0x91, 0xA1, 0x69, 0x62, 0x86}};

// ------------------------------------------------------------------
// ------------------------------------------------------------------
class _aenc:public _audio_encoder
{
    private:

        IBaseFilter *pEncoderFilter;

    public:

        _aenc(int index);
        ~_aenc();

        IBaseFilter *filter() { return pEncoderFilter; };

        virtual void release() { delete this; };
};

// ------------------------------------------------------------------
_aenc::_aenc(int index)
    :pEncoderFilter(NULL)
{

    
    HRESULT hr;

    hr = CoCreateInstance(CLSID_LAMEFilter, NULL, CLSCTX_INPROC, IID_IBaseFilter, (void **) &pEncoderFilter);
    if (FAILED(hr)) throw(_video_capture_error(hr,"_aenc::_aenc - can not create Base Filter Interface"));
    
    
    /*IFilterMapper2 *pMapper = NULL;
    IEnumMoniker *pEnum = NULL;

    HRESULT hr;

    hr = CoCreateInstance(CLSID_FilterMapper2, NULL, CLSCTX_INPROC, IID_IFilterMapper2, (void **) &pMapper);
    if (FAILED(hr)) throw(_video_capture_error(hr,"_aenc::_aenc - can not create Filter Mapper"));


    GUID arrayInTypes[1];
    arrayInTypes[0] = MEDIATYPE_Audio;
    
    hr = pMapper->EnumMatchingFilters(
            &pEnum,
            0,                  // Reserved.
            TRUE,               // Use exact match?
            MERIT_PREFERRED+1, // Minimum merit.
            FALSE,               // At least one input pin?
            0, //1,                  // Number of major type/subtype pairs for input.
            NULL,//arrayInTypes,       // Array of major type/subtype pairs for input.
            NULL,               // Input medium.
            NULL,               // Input pin category.
            FALSE,              // Must be a renderer?
            FALSE,               // At least one output pin?
            0,                  // Number of major type/subtype pairs for output.
            NULL,               // Array of major type/subtype pairs for output.
            NULL,               // Output medium.
            NULL);              // Output pin category.

    // Enumerate the monikers.
    IMoniker *pMoniker;
    ULONG cFetched;
    while (pEnum->Next(1, &pMoniker, &cFetched) == S_OK)
    {
        IPropertyBag *pPropBag = NULL;
        hr = pMoniker->BindToStorage(0, 0, IID_IPropertyBag, (void **)&pPropBag);
    
        if (SUCCEEDED(hr))
        {
            // To retrieve the friendly name of the filter, do the following:
            VARIANT varName;
            VariantInit(&varName);
            
            hr = pPropBag->Read(L"FriendlyName", &varName, 0);
            if (SUCCEEDED(hr))
            {
                // Check name.
                char tmp[128];
                sprintf(tmp,"%S",varName.bstrVal);
                
                if (!strcmp(tmp,"LAME Audio Encoder"))
                {
                    hr = pMoniker->BindToObject(NULL, NULL, IID_IBaseFilter, (void**)&pEncoderFilter);

                    pPropBag->Release();
                    pMoniker->Release();
                    pMapper->Release();
                    pEnum->Release();
    
                    return;
                }
            }
            VariantClear(&varName);
    
            // Clean up.
            pPropBag->Release();
        }
        pMoniker->Release();
    }

    pMapper->Release();
    pEnum->Release();

    /*
    IEnumMoniker *pEnum;
    bool not_empty = _video_help::create_enumerator(CLSID_VideoCompressorCategory, &pEnum);

    if (!not_empty)
    {
        pEnum->Release();
        throw(_video_capture_error(0,"_video_encoder::_video_encoder - you have NOT encoders"));
    }

    IMoniker *pMoniker = NULL;
    while (pEnum->Next(1, &pMoniker, NULL) == S_OK)
    {
        if (index==0)
        {
            HRESULT hr = pMoniker->BindToObject(0, 0, IID_IBaseFilter, (void**)&pEncoderFilter);
            if (FAILED(hr)) throw(_video_capture_error(hr,"_video_encoder::_video_encoder - can not bind Encoder Filter"));

            pMoniker->Release();
            pEnum->Release();
            return;
        }

        index--;
    }

    pMoniker->Release();
    pEnum->Release();

    throw(_video_capture_error(index,"_video_encoder::_video_encoder - can not create encoder with given index - no such encoder"));
    */
}

// ------------------------------------------------------------------
_aenc::~_aenc()
{
    if (pEncoderFilter) pEncoderFilter->Release();
}


// ------------------------------------------------------------------
_audio_encoder *create_audio_encoder(int index)
{
    return new _aenc(index);
}

#endif