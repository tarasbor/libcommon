#include "dxshow.hpp"
#include "dxshow_av_bs_filter.hpp"

//#ifdef DEBUG
#include <log.hpp>
#define MLOG(L) _log() << _log::_set_module_level("vcall",(L))
//#endif

#define __STDC_CONSTANT_MACROS
#define __STDC_LIMIT_MACROS
#define __STDC_FORMAT_MACROS

extern "C" {
#include <libavutil/opt.h>
#include <libavcodec/avcodec.h>
#include <libavutil/imgutils.h>
}

#include <iostream>

using namespace std;


// ======================================================================================
//                                           ENCODER
// ======================================================================================
class _av_bsf;

// ----------------------------------- AVENCODER IN PIN ---------------------------------
class _av_bsf_in_pin:public _pin
{
	protected:
		_av_bsf *cm_bsf;

		virtual ~_av_bsf_in_pin();

	public:
		_av_bsf_in_pin(_av_bsf *bsf);

		virtual void start();
		virtual void stop();
		virtual void pause();

		virtual void push_media_sample(const _media_sample *ms);
		virtual void push_stream_header(const _stream_header *sh);
};

// ----------------------------------- AVENCODER OUT PIN ---------------------------------
class _av_bsf_out_pin:public _pin
{
	protected:
		_av_bsf *cm_bsf;

		virtual ~_av_bsf_out_pin();
	public:

		_av_bsf_out_pin(_av_bsf *enc);

		virtual void connect(_pin *to);
};

// ----------------------------------- AVENCODER ---------------------------------
class _av_bsf:public _av_bs_filter
{
    friend class _av_bsf_out_pin;

	protected:

	    AVBitStreamFilterContext* bsf_ctx;

		AVCodecContext *cm_codec_ctx;

		_media_sample cm_out_ms;

		int frame_number;

		_av_bsf_in_pin *cm_in_pin;
		_pins_ptr_list cm_in_pins;

		_av_bsf_out_pin *cm_out_pin;
		_pins_ptr_list cm_out_pins;
	public:

		_av_bsf(const char *filter_name);
        virtual ~_av_bsf();

        virtual void release() { delete this; };

		virtual void proccess_input_media_sample(const _media_sample *ms);
		virtual void proccess_input_stream_header(const _stream_header *sh);

		virtual const _pins_ptr_list &out_pins() const { return cm_out_pins; };
		virtual const _pins_ptr_list &in_pins() const { return cm_in_pins; };
};

// -----------------------------------------------------------------------------
// PINS
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// IN
// -----------------------------------------------------------------------------
_av_bsf_in_pin::_av_bsf_in_pin(_av_bsf *bsf)
 :cm_bsf(bsf)
{
	cm_is_input=true;
}

// -----------------------------------------------------------------------------
_av_bsf_in_pin::~_av_bsf_in_pin()
{
}

// -----------------------------------------------------------------------------
void _av_bsf_in_pin::start()
{
	(*cm_bsf->out_pins().begin())->start();
	cm_state=started;
}

// -----------------------------------------------------------------------------
void _av_bsf_in_pin::stop()
{
	(*cm_bsf->out_pins().begin())->stop();
	cm_state=stoped;
}

// -----------------------------------------------------------------------------
void _av_bsf_in_pin::pause()
{
	(*cm_bsf->out_pins().begin())->pause();
	cm_state=paused;
}

// -----------------------------------------------------------------------------
void _av_bsf_in_pin::push_media_sample(const _media_sample* ms)
{
	cm_bsf->proccess_input_media_sample(ms);
}

// -----------------------------------------------------------------------------
void _av_bsf_in_pin::push_stream_header(const _stream_header* sh)
{
    cm_bsf->proccess_input_stream_header(sh);
}

// -----------------------------------------------------------------------------
// OUT
// -----------------------------------------------------------------------------
_av_bsf_out_pin::_av_bsf_out_pin(_av_bsf *bsf)
 :cm_bsf(bsf)
{
	cm_is_input=false;
}

// -----------------------------------------------------------------------------
_av_bsf_out_pin::~_av_bsf_out_pin()
{
}

// -----------------------------------------------------------------------------
void _av_bsf_out_pin::connect(_pin *to)
{
    _pin::connect(to);

    _av_codec_context_header *acch=new _av_codec_context_header;
    acch->context=cm_bsf->cm_codec_ctx;

    _stream_header sh;
    sh.set_format_header(acch);

    to->push_stream_header(&sh);
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
_av_bsf::_av_bsf(const char *filter_name):bsf_ctx(NULL),cm_codec_ctx(NULL),
						frame_number(0),cm_in_pin(NULL),cm_out_pin(NULL)
{
    cm_out_ms.cm_btype=MEDIA_SAMPLE_BT_AV_PACKET;
    cm_out_ms.cm_mtype=MEDIA_SAMPLE_MT_AUDIO;

    //avcodec_register_all();

    bsf_ctx = av_bitstream_filter_init(filter_name);
    if (!bsf_ctx) throw _pm_error(0,string("_av_bsf::_av_bsf - Can not create ")+filter_name+" filter context");

    cm_in_pin=new _av_bsf_in_pin(this);
	cm_in_pins.push_back(cm_in_pin);

	cm_out_pin=new _av_bsf_out_pin(this);
	cm_out_pins.push_back(cm_out_pin);
}

// -----------------------------------------------------------------------------
_av_bsf::~_av_bsf()
{
    if (cm_in_pin)
	{
		cm_in_pin->release();
	}
	cm_in_pins.clear();

	if (cm_out_pin)
	{
		// TODO - normal end_of_stream implementation
		// Comment now - infinity recurse loop
		// cm_out_pin->end_of_stream();
		cm_out_pin->release();
	}
	cm_out_pins.clear();

	if (bsf_ctx) av_bitstream_filter_close(bsf_ctx);
}

// -----------------------------------------------------------------------------
void _av_bsf::proccess_input_stream_header(const _stream_header *sh)
{
    if (sh->format_header->format_type!=MEDIA_FORMAT_AV_CODEC_CONTEXT)
        throw _pm_error(0,"_av_bsf::proccess_input_stream_header - stream header type != MEDIA_FORMAT_AV_CODEC_CONTEXT");

    cm_codec_ctx=(AVCodecContext *)((_av_codec_context_header *)(sh->format_header))->context;
}

// -----------------------------------------------------------------------------
void _av_bsf::proccess_input_media_sample(const _media_sample* ms)
{
    if (ms->cm_mtype!=MEDIA_SAMPLE_MT_AUDIO) return ;

    AVPacket *in_packet;

    if (ms->cm_btype==MEDIA_SAMPLE_BT_AV_PACKET)
    {
        in_packet=(AVPacket *)ms->buffer;
    }
    else
    {
        throw _pm_error(0,string("_av_bsf::proccess_input_media_sample - can not process media sample with type=")+to_string(ms->cm_btype));
    }

    AVPacket out_packet;
    av_init_packet(&out_packet);
    out_packet.data = NULL;
    out_packet.size = 0;
    av_copy_packet(&out_packet,in_packet);

	int ret = av_bitstream_filter_filter(bsf_ctx, cm_codec_ctx,
                                         NULL,
                                         &out_packet.data,&out_packet.size,
                                         in_packet->data,in_packet->size,
                                         in_packet->flags & AV_PKT_FLAG_KEY);

    if (ret<0) throw _pm_error(0,"_av_bsf::proccess_input_media_sample - failed to filter bitstream.");

    cm_out_ms.buffer=(unsigned char *)&out_packet;
    cm_out_ms.stream_stamp=ms->stream_stamp;
	cm_out_ms.can_break=(out_packet.flags & AV_PKT_FLAG_KEY);

	cm_out_pin->push_media_sample(&cm_out_ms);

    if (ret>0) av_free_packet(&out_packet); // if ret==0 out_packet data point to in_packet->data

    frame_number++;
}

// -----------------------------------------------------------------------------
_av_bs_filter *create_av_bs_filter(const char *filter_name)
{
    return new _av_bsf(filter_name);
}
