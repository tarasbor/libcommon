#ifndef __DXSHOW_AV_AUDIO_FILTER__
#define __DXSHOW_AV_AUDIO_FILTER__

#include "dxshow.hpp"

extern "C" {
#include <libavcodec/avcodec.h>
}


class _av_a_filter:public _media_filter
{
    protected:
        virtual ~_av_a_filter() {};

    public:
        _av_a_filter() {};

        virtual void release() = 0;
};

_av_a_filter *create_av_a_filter(const char *filter_str,const char *input_format,
                                 AVSampleFormat out_sample_format,int64_t out_ch_layout,int out_sample_rate);//const char *filter_name);

#endif
