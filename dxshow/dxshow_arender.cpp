#include "dxshow.hpp"

extern "C" {
#include <libavcodec/avcodec.h>
}

#ifdef WINDOWS
#include "dx_proxy_proc.hpp"
#include <iostream>

class _aren;

class _aren_in_pin:public _pin
{
    protected:

        _aren *cm_render;

        virtual ~_aren_in_pin();

    public:

        _aren_in_pin(_aren *r);

        virtual void release() { delete this; };

        virtual void start();
        virtual void stop();
        virtual void pause();

        virtual void push_media_sample(const _media_sample* ms);
        virtual void push_stream_header(const _stream_header *sh);
};

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
class _aren:public _audio_render
{
    friend class _aren_in_pin;
    protected:

        IGraphBuilder *pFilterGraphBuilder;
        ICaptureGraphBuilder2 *pCaptureGraphBuilder;

        IMediaControl *pMediaControl;

        IBaseFilter *pRenderFilter;

        _pins_ptr_list cm_in_pins;
        _pin *cm_in_pin;

        _dx_proxy *cm_dx_proxy;
        _dx_proxy_to_tar_processor *cm_dx_proxy_proc;

        virtual void start_device_render();
        virtual void pause_device_render();
		virtual void stop_device_render();

    public:

        _aren(int index);
        virtual ~_aren();

        virtual void release() { delete this; };

        virtual const _pins_ptr_list &in_pins() const { return cm_in_pins; };

};

// -------------------------------------------------------------------------
_aren::_aren(int index)
    :pRenderFilter(NULL),cm_in_pin(NULL),pFilterGraphBuilder(NULL),pCaptureGraphBuilder(NULL),pMediaControl(NULL),cm_dx_proxy(NULL),cm_dx_proxy_proc(NULL)
{
    // init COM
    HRESULT hr=CoInitialize(NULL);
    if (FAILED(hr)) throw(_pm_error(hr,"Can not initialize COM..."));

    // create GRAPH Builder
    hr = CoCreateInstance(CLSID_CaptureGraphBuilder2, NULL, CLSCTX_INPROC_SERVER, IID_ICaptureGraphBuilder2, (void**)&pCaptureGraphBuilder );
    if (FAILED(hr)) throw(_pm_error(hr,"Can not get Capture Graph Builder interface"));

    // Create the Filter Graph Manager.
    hr = CoCreateInstance(CLSID_FilterGraph, 0, CLSCTX_INPROC_SERVER, IID_IGraphBuilder, (void**)&pFilterGraphBuilder);
    if (FAILED(hr)) throw(_pm_error(hr,"Can not get Filter Graph Builder interface"));

    pCaptureGraphBuilder->SetFiltergraph(pFilterGraphBuilder);


    IEnumMoniker *pEnum;
    bool not_empty = _video_help::create_enumerator(CLSID_AudioRendererCategory, &pEnum);

    if (!not_empty)
    {
        pEnum->Release();
        throw(_pm_error(0,"_aren::_aren - you have NOT AUDIO render devices"));
    }

    IMoniker *pMoniker = NULL;
    while (pEnum->Next(1, &pMoniker, NULL) == S_OK)
    {
        if (index==0)
        {
            HRESULT hr = pMoniker->BindToObject(0, 0, IID_IBaseFilter, (void**)&pRenderFilter);
            if (FAILED(hr)) throw(_pm_error(hr,"_aren::_aren - can not bind Render Filter"));

            pMoniker->Release();
            pEnum->Release();

            cm_in_pin=new _aren_in_pin(this);
            cm_in_pins.push_back(cm_in_pin);

            return;
        }

        index--;
    }

    pMoniker->Release();
    pEnum->Release();

    throw(_pm_error(index,"_aren::_aren - can not create device with given index - no such device"));
}

// -------------------------------------------------------------------------
_aren::~_aren()
{
    if (cm_in_pin)
    {
        cm_in_pin->release();
        cm_in_pin=NULL;
    }
    cm_in_pins.clear();

    if (pMediaControl)
    {
        pMediaControl->Stop();
        pMediaControl->Release();
    }

    if (pRenderFilter) pRenderFilter->Release();
    if (cm_dx_proxy) cm_dx_proxy->release();
    if (cm_dx_proxy_proc) cm_dx_proxy_proc->release();

    if (pCaptureGraphBuilder) pCaptureGraphBuilder->Release();

    // TODO - we crush ig call release of FGB (may be it released in CGB release ?)
    //if (pFilterGraphBuilder) pFilterGraphBuilder->Release();

    CoUninitialize();

}

// -------------------------------------------------------------------------
void _aren::start_device_render()
{
    if (pMediaControl)
    {
        pMediaControl->Run();
        return;
    }

    HRESULT hr;

    // create memory proxy filter & proxy processor
    cm_dx_proxy_proc=create_dx_proxy_to_tar_processor(cm_in_pin);
    cm_dx_proxy=create_dx_proxy_to_target(cm_dx_proxy_proc);

    hr=pFilterGraphBuilder->AddFilter(pRenderFilter,L"Render");
    if (FAILED(hr)) throw(_pm_error(hr,"_audio_render::start_device_render - can not add Render Filter to Filter Graph"));

    hr=pFilterGraphBuilder->AddFilter(cm_dx_proxy->filter_iface(),L"Source");
    if (FAILED(hr)) throw(_pm_error(hr,"_audio_render::start_device_render - can not add Capture (Mem Proxy) Filter to Filter Graph"));

    IFilterGraph2 *pFG2 = NULL;

    hr = pFilterGraphBuilder->QueryInterface( IID_IFilterGraph2, (void **)&pFG2);
    if (FAILED(hr)) throw(_pm_error(hr,"_audio_render::start - can not create Filter Graph"));

    IPin *pPin;
    IEnumPins *pEnumPins;

    cm_dx_proxy->filter_iface()->EnumPins(&pEnumPins);
    if (FAILED(hr)) throw(_pm_error(hr,"_audio_render::start - can not enum DX pins"));
    pEnumPins->Next(1, &pPin, 0);
    pEnumPins->Release();

    hr = pFG2->RenderEx(pPin,AM_RENDEREX_RENDERTOEXISTINGRENDERERS,NULL);
    if (FAILED(hr)) throw(_pm_error(hr,"_audio_render::start - can not render"));

    pPin->Release();

    pFG2->Release();

    //hr = pCaptureGraphBuilder->RenderStream(&PIN_CATEGORY_CAPTURE, &MEDIATYPE_Audio, cm_dx_proxy->filter_iface(), NULL, pRenderFilter);
    //if (FAILED(hr)) throw(_pm_error(hr,"_audio_pipeline::render - can not start render"));

    hr = pFilterGraphBuilder->QueryInterface( IID_IMediaControl, (void **)&pMediaControl );
    if (FAILED(hr)) throw(_pm_error(hr,"_audio_render::start_device_capture - can get Media Control Interface"));

    pMediaControl->Run();
}

// -------------------------------------------------------------------------
void _aren::pause_device_render()
{
    if (pMediaControl) pMediaControl->Pause();
}

// -------------------------------------------------------------------------
void _aren::stop_device_render()
{
    if (pMediaControl) pMediaControl->Stop();
}

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
_aren_in_pin::_aren_in_pin(_aren *r)
:cm_render(r)
{
    cm_is_input=true;
}

// -------------------------------------------------------------------------
_aren_in_pin::~_aren_in_pin()
{
}

// -------------------------------------------------------------------------
void _aren_in_pin::start()
{
    cm_render->start_device_render();
}

// -------------------------------------------------------------------------
void _aren_in_pin::stop()
{
    cm_render->stop_device_render();
}

// -------------------------------------------------------------------------
void _aren_in_pin::pause()
{
    cm_render->pause_device_render();
}

// -------------------------------------------------------------------------
void _aren_in_pin::push_media_sample(const _media_sample* ms)
{
    _pin::push_media_sample(ms); // proxy input pin connected to this pin (look at _proxy_to_target)- so just call base push
}

// -------------------------------------------------------------------------
void _aren_in_pin::push_stream_header(const _stream_header *sh)
{
    _pin::push_stream_header(sh); // proxy input pin connected to this pin (look at _proxy_to_target)- so just call base push
}



// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
_audio_render *create_audio_render(int index)
{
    return new _aren(index);
}
// ---------------------------------------- END WIN IMP ------------------------
#else
// ------------------------------------------ ALSA IMP -------------------------

#include <errno.h>
#include <string.h>
#include <stdio.h>

#include <iostream>
#include <alsa/asoundlib.h>
#include <clthread.hpp>

class _aren;
class _aren_thread;

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
class _aren_in_pin:public _pin
{
	protected:

		_aren *cm_ren;
		_aren_thread *cm_thread;
		virtual ~_aren_in_pin();

		struct _sample_buffer
		{
			_locker lock;
			string data;
			int frames_cnt;
		};

		_sample_buffer cm_sample_buffer;

		_time t_prev;

		bool cm_firsts_push;
		snd_async_handler_t *pcm_callback;

		static void play_callback(snd_async_handler_t *pcm_ch);

	public:
		_aren_in_pin(_aren *r);

		virtual void start();
		virtual void stop();

		virtual void push_media_sample(const _media_sample *ms);
		virtual int render();

		virtual void on_connect(_pin *from);
};

// --------------------------------------------------------------
class _aren_thread:public _thread
{
	protected:
		_aren_in_pin *pin;

	public:
	  _aren_thread(_aren_in_pin *p);
	  virtual ~_aren_thread();

	  virtual int Poll();
};

// --------------------------------------------------------------
// --------------------------------------------------------------
class _aren:public _audio_render
{
	friend class _aren_in_pin;
	protected:
		_pins_ptr_list cm_in_pins;
		_pin *cm_in_pin;

		string cm_dev_name;

		snd_pcm_t *dev_h;

		//virtual void start_play_device();
		//virtual void stop_play_device();

	public:

		_aren(const string &dname);
		virtual ~_aren();

		virtual void release() { delete this; };

		virtual const _pins_ptr_list &in_pins() const { return cm_in_pins; };
};

// --------------------------------------------------------------
// --------------------------------------------------------------

// --------------------------------------------------------------
_aren::_aren(const string &dname):cm_in_pin(NULL),cm_dev_name(dname),dev_h(NULL)
{
	cm_in_pin=new _aren_in_pin(this);
	cm_in_pins.push_back(cm_in_pin);

	int err;
	err = snd_pcm_open(&dev_h, cm_dev_name.c_str(), SND_PCM_STREAM_PLAYBACK, 0);
	if (err < 0)
		throw _pm_error(err,string("_aren::_aren - cannot open audio device - ")+snd_strerror(err));
}

// --------------------------------------------------------------
_aren::~_aren()
{
	if (dev_h)
	{
		snd_pcm_drop(dev_h);
		snd_pcm_close(dev_h);
		dev_h=NULL;
	}

	cm_in_pins.clear();
	if (cm_in_pin) cm_in_pin->release();
}

// --------------------------------------------------------------
// THREAD
// --------------------------------------------------------------
_aren_thread::_aren_thread(_aren_in_pin *p)
:pin(p)
{
}

// --------------------------------------------------------------
_aren_thread::~_aren_thread()
{
}

// --------------------------------------------------------------
int _aren_thread::Poll()
{
	return pin->render();
}

// --------------------------------------------------------------
// PIN
// --------------------------------------------------------------

// --------------------------------------------------------------
_aren_in_pin::_aren_in_pin(_aren* r):cm_ren(r),cm_thread(NULL),cm_firsts_push(true),pcm_callback(NULL)
{
	cm_is_input=true;

	cm_sample_buffer.frames_cnt=0;
}

// --------------------------------------------------------------
_aren_in_pin::~_aren_in_pin()
{
	stop();
	if (pcm_callback)
	{
        snd_async_del_handler(pcm_callback);
	}
}

// --------------------------------------------------------------
void _aren_in_pin::start()
{
	cout << "_aren_in_pin::start" << endl;
	int err=snd_pcm_prepare (cm_ren->dev_h);
	if (err<0)
		throw _pm_error(err,string("_aren_in_pin::start - cannot prepare audio interface for use - ")+snd_strerror(err));

	cm_thread=new _aren_thread(this);
	cm_thread->SetSleepTime(5);

	cm_state=started;
}

// --------------------------------------------------------------
void _aren_in_pin::stop()
{

	if (cm_thread)
	{
		cm_thread->Kill();
		while(cm_thread->GetStatus()!=TH_STATUS_KILLED) Sleep(10);
		delete cm_thread;
		cm_thread=NULL;
	}

	if (cm_ren->dev_h)
	{
		snd_pcm_drop(cm_ren->dev_h);
		//int err=snd_pcm_drop(cm_ren->dev_h);
		//if (err<0)
		//	throw _pm_error(err,string("_aren_in_pin::stop - cannot drop sound- ")+snd_strerror(err));
	}

	cm_state=stoped;
}

// --------------------------------------------------------------
void _aren_in_pin::push_media_sample(const _media_sample *ms)
{
    if (ms->cm_mtype!=MEDIA_SAMPLE_BT_AV_FRAME) throw _pm_error(ms->cm_mtype,"_aren_in_pin::push_media_sample - can not work with media sample with type!=MEDIA_SAMPLE_BT_AV_FRAME");

    AVFrame *in_frame=(AVFrame *)ms->buffer;

    int frame_size=2;
	int frames_cnt=in_frame->nb_samples;//ms->buffer_size/frame_size;

    cm_sample_buffer.lock.lock();
	//cm_sample_buffer.data.append((const char *)ms->buffer,ms->buffer_size);
	cm_sample_buffer.data.append((const char *)in_frame->data[0],frames_cnt*frame_size);
	cm_sample_buffer.frames_cnt+=frames_cnt;
	cm_sample_buffer.lock.unlock();

	_time t;
	t.set_now();

	//cout << t.stamp() <<" _aren_in_pin::push " << ms->data_size/2 << " frames" << endl;

    // at first
    if (cm_firsts_push)
    {
        if (cm_sample_buffer.frames_cnt>=240*4*4)
        {

            //snd_async_add_pcm_handler(&pcm_callback, cm_ren->dev_h, play_callback, this);

            //render();
            cm_thread->Start();

            cm_firsts_push=false;
        }
    }

    /*
    int frame_size=2;
	int frames_cnt=ms->buffer_size/frame_size;

    cout << "+" << flush;

	// add media sample data to buffer
	cm_sample_buffer.lock.lock();
	cm_sample_buffer.data.append((const char *)ms->buffer,ms->buffer_size);
	cm_sample_buffer.frames_cnt+=frames_cnt;
	//cout << "append " << ms->buffer_size << " bytes to buffer - now it have " << cm_sample_buffer.frames_cnt << " frames" << endl;
	cm_sample_buffer.lock.unlock();

	_time t;
	t.set_now();

	//cout << t.stamp() <<" _aren_in_pin::push " << ms->data_size/2 << " frames" << endl;

    // at first
    if (cm_firsts_push)
    {
        if (cm_sample_buffer.frames_cnt>=240*4*4)
        {

            //snd_async_add_pcm_handler(&pcm_callback, cm_ren->dev_h, play_callback, this);

            //render();
            cm_thread->Start();

            cm_firsts_push=false;
        }
    }
    */
}

// --------------------------------------------------------------
int _aren_in_pin::render()
{
    unsigned char rbuffer[1000];

    int err;

    _time t;
    t.set_now();

    //if (cm_sample_buffer.frames_cnt==0)
    //{
        //snd_pcm_pause(cm_ren->dev_h,1);
        //cout << t.stamp() << " !" << endl;
        //return 0;
    //}



    int avail = snd_pcm_avail_update(cm_ren->dev_h);
    while (avail >= 240)
    {

        cm_sample_buffer.lock.lock();
        memcpy(rbuffer,cm_sample_buffer.data.data(),240*2);
        cm_sample_buffer.data.erase(0,240*2);
        cm_sample_buffer.frames_cnt-=240;
        cm_sample_buffer.lock.unlock();

        snd_pcm_writei(cm_ren->dev_h, rbuffer, 240);
        avail = snd_pcm_avail_update(cm_ren->dev_h);

        //cout << t.stamp() << "_aren_in_pin::render 240 frames. we have " << cm_sample_buffer.frames_cnt << "frames." << endl;
    }

    //cout << "stop render loop ...." << endl;

/*
    err=snd_pcm_writei(cm_ren->dev_h, rbuffer, 240);
    if (err!=240)
    {
        cout << "SHORT WRITE!!!! write " << err << endl;
    }
    if (err<0)
    {
        cout << t.stamp() << " snd_pcm_write error " << snd_strerror(err) << endl;
        if (err==-EPIPE)
        {
            err = snd_pcm_prepare(cm_ren->dev_h);
            if (err < 0)
            {
                        cout << "cannot recovery prepare failed " <<  snd_strerror(err) << endl;
            }
        }
        return 0;
            //throw _pm_error(err,string("_aren_in_pin::push_media_sample - cannot write data - ")+snd_strerror(err));
    }

    /*int avail = snd_pcm_avail_update(cm_ren->dev_h);
    while (avail >= 480)
    {
        cout << "." << endl;

        avail = snd_pcm_avail_update(cm_ren->dev_h);
    }
    */

    /*
    err=snd_pcm_writei(cm_ren->dev_h, rbuffer, 240);  // TODO must be writei for interleave
	if (err<0)
	{
		cout << "snd_pcm_write error " << snd_strerror(err) << endl;
		return 0;
		//throw _pm_error(err,string("_aren_in_pin::push_media_sample - cannot write data - ")+snd_strerror(err));
	}

    /*
	unsigned char rbuffer[40960];

	int render_frames=0;

	cm_sample_buffer.lock.lock();
	if (!cm_sample_buffer.frames_cnt)
	{
		cm_sample_buffer.lock.unlock();
		Sleep(1);
		return 0;
	}

	if (cm_sample_buffer.frames_cnt<2400)
	{
        cm_sample_buffer.lock.unlock();
        cout << "skip" << endl;
		Sleep(1);
		return 0;
	}

	if (cm_sample_buffer.frames_cnt>2400) render_frames=2400; else render_frames=cm_sample_buffer.frames_cnt;
	cm_sample_buffer.frames_cnt-=render_frames;

	memcpy(rbuffer,cm_sample_buffer.data.data(),render_frames*2);
	cm_sample_buffer.data.erase(0,render_frames*2);

	cm_sample_buffer.lock.unlock();

	int err;

	cout << "_aren_in_pin::render - " << render_frames << endl;

	//return 0;

	_time t1;
	t1.set_now();

	err=snd_pcm_writei(cm_ren->dev_h, rbuffer, render_frames);  // TODO must be writei for interleave
	if (err<0)
	{
		cout << "snd_pcm_write error " << snd_strerror(err) << endl;
		return 0;
		//throw _pm_error(err,string("_aren_in_pin::push_media_sample - cannot write data - ")+snd_strerror(err));
	}

	_time t2;
	t2.set_now();
	cout << "write was " << (t2.stamp()-t1.stamp())/100000 << " ms. Prev write was << " << (t2.stamp()-t_prev.stamp())/100000 << endl;
	t_prev=t2;

	if (err!=render_frames)
	{
		cout << "snd_pcm_write write less frames " << err << " need " << render_frames << endl;
	}
	*/

	return 0;
}

// --------------------------------------------------------------
void _aren_in_pin::play_callback(snd_async_handler_t *pcm_ch)
{
    _aren_in_pin *pin=(_aren_in_pin *)snd_async_handler_get_callback_private(pcm_ch);
    pin->render();
}

// --------------------------------------------------------------
void _aren_in_pin::on_connect(_pin* from)
{
	cout << "_aren_in_pin::on_connect" << endl;

	//_audio_header *ah=(_audio_header *)from->stream_header()->format_header;

	int err;

    /*
	snd_pcm_hw_params_t *hw_params;

	err=snd_pcm_hw_params_malloc(&hw_params);
	if (err< 0)
		throw _pm_error(err,string("_aren_in_pin::on_connect - cannot allocate hardware parameter structure - ")+snd_strerror(err));

	err=snd_pcm_hw_params_any (cm_ren->dev_h, hw_params);
	if (err<0)
		throw _pm_error(err,string("_aren_in_pin::on_connect - cannot initialize hardware parameter structure - ")+snd_strerror(err));

	err=snd_pcm_hw_params_set_access (cm_ren->dev_h, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED);
	if (err< 0)
		throw _pm_error(err,string("_aren_in_pin::on_connect - cannot set access type - ")+snd_strerror(err));

	err=snd_pcm_hw_params_set_format (cm_ren->dev_h, hw_params, SND_PCM_FORMAT_S16_LE);
	if (err< 0)
		throw _pm_error(err,string("_aren_in_pin::on_connect - cannot set sample format - ")+snd_strerror(err));

	unsigned int rsample_rate=8000;
	err = snd_pcm_hw_params_set_rate_near (cm_ren->dev_h, hw_params, &rsample_rate, 0);
	if (err< 0)
		throw _pm_error(err,string("_aren_in_pin::on_connect - cannot set sample rate - ")+snd_strerror(err));
	if (rsample_rate!=8000)
	{
	  cout << "Supported rate= " << rsample_rate << endl;
	  throw _pm_error(err,string("_aren_in_pin::on_connect - cannot set sample rate - r!=s"));
	}

	err=snd_pcm_hw_params_set_channels (cm_ren->dev_h, hw_params,1);
	if (err<0)
		throw _pm_error(err,string("_aren_in_pin::on_connect - cannot set channel count - ")+snd_strerror(err));

	err=snd_pcm_hw_params (cm_ren->dev_h, hw_params);
	if (err<0)
		throw _pm_error(err,string("_aren_in_pin::on_connect - cannot set parameters - ")+snd_strerror(err));

	snd_pcm_hw_params_free(hw_params);


	/*snd_pcm_set_params(cm_ren->dev_h,
	                   SND_PCM_FORMAT_S16_LE,
	                   SND_PCM_ACCESS_RW_INTERLEAVED,
					   ah->channels_count,
					   ah->sample_rate,
					   1,
					   500000);*/

    err=snd_pcm_set_params(cm_ren->dev_h,
	                   SND_PCM_FORMAT_S16_LE,
	                   SND_PCM_ACCESS_RW_INTERLEAVED,
					   1,
					   8000,
					   1,
					   120000);


    if (err<0)
		throw _pm_error(err,string("_aren_in_pin::on_connect - cannot set hw parameters - ")+snd_strerror(err));

	// set software params
    snd_pcm_sw_params_t *sw_params;

    snd_pcm_sw_params_malloc (&sw_params);
    snd_pcm_sw_params_current (cm_ren->dev_h, sw_params);

    //int buffer_size=300*2*8000/1000;
    //int period_size=240*2; // 240 - samples

    int start_at_frames=240*4; // 240 ms
    //int stop_at_frames=240; // 30 ms
    int min_avial=240;//start_at_frames-240;


    err=snd_pcm_sw_params_set_start_threshold(cm_ren->dev_h, sw_params, start_at_frames);
    if (err<0)
		throw _pm_error(err,string("_aren_in_pin::on_connect - cannot set - start_threshold ")+snd_strerror(err));

	//err=snd_pcm_sw_params_set_stop_threshold(cm_ren->dev_h, sw_params, stop_at_frames);
    //if (err<0)
	//	throw _pm_error(err,string("_aren_in_pin::on_connect - cannot set - start_threshold ")+snd_strerror(err));

    err=snd_pcm_sw_params_set_avail_min(cm_ren->dev_h, sw_params, min_avial);
    if (err<0)
        throw _pm_error(err,string("_aren_in_pin::on_connect - cannot set - set_acail ")+snd_strerror(err));

    err=snd_pcm_sw_params(cm_ren->dev_h, sw_params);
    if (err<0)
        throw _pm_error(err,string("_aren_in_pin::on_connect - cannot set sw params ")+snd_strerror(err));

}


// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
_audio_render *create_audio_render_snd_card(const string &dev_name)
{
	return new _aren(dev_name);
}

_audio_render *create_audio_render(int index)
{
	return new _aren("default");
}


#endif

