#ifndef __DXSHOW_AV_LOCK__
#define __DXSHOW_AV_LOCK__

#include "dxshow.hpp"

class _av_lock_manager
{
    public:
        _av_lock_manager() {};
        virtual ~_av_lock_manager() {};
};

_av_lock_manager *create_av_lock_manager();

#endif // __DXSHOW_AV_LOCK__
