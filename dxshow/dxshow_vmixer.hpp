#ifndef __DXSHOW_V_MIXER__
#define __DXSHOW_V_MIXER__

#include "dxshow.hpp"

class _vmixer:public _media_filter
{
    protected:
        virtual ~_vmixer() {};

    public:
        _vmixer() {};

        virtual void release() = 0;

        virtual _pin *create_input_pin(const _rect &dst_rec,bool sink) = 0;
        virtual _pin *create_output_pin(const _rect &img_rec) = 0;
};

_vmixer *create_vmixer();

#endif // __DXSHOW_AV_CAPTURE__
