#ifndef __DXSHOW_NET_RENDER__SOURCE__
#define __DXSHOW_NET_RENDER__SOURCE__

#include "dxshow.hpp"
#include <ghelp_gl.hpp>

// ---------------------------------------------------------
// ---------------------------------------------------------
class _buffer_render:public _media_target
{
    public:

        virtual const _stream_header *stream_header() const = 0;
        virtual _media_sample *pop_media_sample() = 0;
};

_buffer_render *create_buffer_render();

// ---------------------------------------------------------
// ---------------------------------------------------------
class _buffer_source:public _media_source
{
    public:
        virtual void push_stream_header(const _stream_header *sh) = 0;
        virtual void push_media_sample(const _media_sample *ms) = 0;

        virtual void start() = 0;
};

_buffer_source *create_buffer_source();

#endif // __DXSHOW_VIDEO_GL_RENDER__
