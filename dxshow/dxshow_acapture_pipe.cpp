#include "dxshow.hpp"

#ifdef WINDOWS

// ------------------------------------------------------------------
// ------------------------------------------------------------------
class _acap_pipe:public _audio_capture_pipe
{
    private:

        IGraphBuilder *pFilterGraphBuilder;
        ICaptureGraphBuilder2 *pCaptureGraphBuilder;

        IMediaControl *control;

        _audio_capture *capture;
        _audio_encoder *encoder;
        _audio_render *render;

        bool _need_preview;

        void init_graph_builders();
        
    public:

        _acap_pipe();
        ~_acap_pipe();

        virtual void set_capture_device(_audio_capture *vc);
        virtual _audio_capture *capture_device() { return capture; };

        virtual void set_audio_encoder(_audio_encoder *e);
        virtual _audio_encoder *audio_encoder() { return encoder; };

        virtual void set_audio_render(_audio_render *r);
        virtual _audio_render *audio_render() { return render; };

        virtual IGraphBuilder *graph() { return pFilterGraphBuilder; };

        virtual void start();
        virtual void stop();
        virtual void pause();

        virtual void set_need_preview(bool n) { _need_preview=n; };

        virtual void release() { delete this; };
};

// ------------------------------------------------------------------
// ------------------------------------------------------------------
_acap_pipe::_acap_pipe()
    :pFilterGraphBuilder(NULL),pCaptureGraphBuilder(NULL),control(NULL),
    encoder(NULL),capture(NULL),render(NULL),_need_preview(false)
{
    // Initialize the COM library.
    HRESULT hr = CoInitialize(NULL);
    if (FAILED(hr))
    {
        throw(_video_capture_error(hr,"Can not initialize COM library."));
    }

    init_graph_builders();
}

// ------------------------------------------------------------------
_acap_pipe::~_acap_pipe()
{
    if (control)
    {
        control->Stop();
        control->Release();
    }
    
    if (encoder) encoder->release();
    if (capture) capture->release();
    
    if (pFilterGraphBuilder) pFilterGraphBuilder->Release();
    if (pCaptureGraphBuilder) pCaptureGraphBuilder->Release();

    //if (render) render->release();
    render = NULL;

    CoUninitialize();
}

// ------------------------------------------------------------------
void _acap_pipe::init_graph_builders()
{
    HRESULT hr = CoCreateInstance(CLSID_CaptureGraphBuilder2, NULL, CLSCTX_INPROC_SERVER, IID_ICaptureGraphBuilder2, (void**)&pCaptureGraphBuilder );
    if (FAILED(hr)) throw(_video_capture_error(hr,"Can not get Capture Graph Builder interface"));
    
    // Create the Filter Graph Manager.
    hr = CoCreateInstance(CLSID_FilterGraph, 0, CLSCTX_INPROC_SERVER, IID_IGraphBuilder, (void**)&pFilterGraphBuilder);
    if (FAILED(hr)) throw(_video_capture_error(hr,"Can not get Filter Graph Builder interface"));
    
    pCaptureGraphBuilder->SetFiltergraph(pFilterGraphBuilder);
}

// ------------------------------------------------------------------
void _acap_pipe::set_capture_device(_audio_capture *vc)
{
    HRESULT hr;
    
    if (capture)
    {
        hr = pFilterGraphBuilder->RemoveFilter(capture->filter());
        if (FAILED(hr)) throw(_video_capture_error(hr,"_audio_pipeline::capture_device(set) - can not remove prev Capture Filter from Filter Graph"));
        
        capture->release();
    }

    capture = vc;

    if (capture)
    {
        hr = pFilterGraphBuilder->AddFilter(capture->filter(), L"Audio Capture Filter");
        if (FAILED(hr)) throw(_video_capture_error(hr,"_audio_pipeline::capture_device(set) - can not add Capture Filter to Filter Graph"));
    }
}

// ------------------------------------------------------------------
void _acap_pipe::set_audio_encoder(_audio_encoder *e)
{
    HRESULT hr;
    
    if (encoder)
    {
        hr = pFilterGraphBuilder->RemoveFilter(encoder->filter());
        if (FAILED(hr)) throw(_video_capture_error(hr,"_audio_pipeline::audio_encoder(set) - can not remove prev Encoder Filter from Filter Graph"));
        
        encoder->release();
    }

    encoder = e;

    if (encoder)
    {
        hr = pFilterGraphBuilder->AddFilter(encoder->filter(), L"Encoder");
        if (FAILED(hr)) throw(_video_capture_error(hr,"_audio_pipeline::audio_encoder(set) - can not add Encoder Filter to Filter Graph"));
    }
}

// ------------------------------------------------------------------
void _acap_pipe::set_audio_render(_audio_render *r)
{
    HRESULT hr;
    
    if (render)
    {
        hr = pFilterGraphBuilder->RemoveFilter(render->filter());
        if (FAILED(hr)) throw(_video_capture_error(hr,"_audio_pipeline::audio_render(set) - can not remove prev Render Filter from Filter Graph"));
        
        render->release();
    }

    render = r;

    if (render)
    {
        hr = pFilterGraphBuilder->AddFilter(render->filter(), L"Render");
        if (FAILED(hr)) throw(_video_capture_error(hr,"_audio_pipeline::audio_render(set) - can not add Render Filter to Filter Graph"));
    }
}


// ------------------------------------------------------------------
void _acap_pipe::start()
{
    if (control)
    {
        control->Run();
        return;
    }
        
    HRESULT hr;

    hr = pCaptureGraphBuilder->RenderStream(&PIN_CATEGORY_CAPTURE, &MEDIATYPE_Audio, capture->filter(), encoder->filter(), render->filter());
    if (FAILED(hr)) throw(_video_capture_error(hr,"_audio_pipeline::render - can not start capture render"));

    if (_need_preview)
    {
        hr = pCaptureGraphBuilder->RenderStream(&PIN_CATEGORY_PREVIEW, &MEDIATYPE_Audio, capture->filter(), NULL, NULL);
        if (FAILED(hr)) throw(_video_capture_error(hr,"_audio_pipeline::render - can not start preview render"));
    }

    if (FAILED(hr)) throw(_video_capture_error(hr,"_audio_pipeline::start - can render capture stream"));

    hr = pFilterGraphBuilder->QueryInterface( IID_IMediaControl, (void **)&control );
    if (FAILED(hr)) throw(_video_capture_error(hr,"_audio_pipeline::start - can get Media Control Interface"));
    
    control->Run();
}

// ------------------------------------------------------------------
void _acap_pipe::stop()
{
    if (control) control->Stop();
}

// ------------------------------------------------------------------
void _acap_pipe::pause()
{
    if (control) control->Pause();
}

// ------------------------------------------------------------------
_audio_capture_pipe *create_audio_capture_pipe()
{
    return new _acap_pipe;
}

#endif