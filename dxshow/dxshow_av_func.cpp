#include "dxshow_av_func.hpp"
#include <string.h>


// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
struct _pcm_ss_drec
{
    AUDIO_PCM_FORMAT pcm;
    int byte_sample_size;
    int bit_sample_size;
};

_pcm_ss_drec pcm_ss_dict[]=
{
  { AUDIO_PCM_FORMAT_S8, 1 , 8},
  { AUDIO_PCM_FORMAT_U8, 1 , 8 },
  { AUDIO_PCM_FORMAT_S16_LE, 2 , 16 },
  { AUDIO_PCM_FORMAT_S16_BE, 2 , 16 },
  { AUDIO_PCM_FORMAT_U16_LE, 2 , 16 },
  { AUDIO_PCM_FORMAT_U16_BE, 2 , 16 },
  { AUDIO_PCM_FORMAT_S24_LE, 3 , 24 },
  { AUDIO_PCM_FORMAT_S24_BE, 3 , 24 },
  { AUDIO_PCM_FORMAT_U24_LE, 3 , 24 },
  { AUDIO_PCM_FORMAT_U24_BE, 3 , 24 },
  { AUDIO_PCM_FORMAT_S32_LE, 4 , 32 },
  { AUDIO_PCM_FORMAT_S32_BE, 4 , 32 },
  { AUDIO_PCM_FORMAT_U32_LE, 4 , 32 },
  { AUDIO_PCM_FORMAT_U32_BE, 4 , 32 },
  { AUDIO_PCM_FORMAT_FLOAT_LE, 4 , 32 }, // may be sizeof ????
  { AUDIO_PCM_FORMAT_FLOAT_BE, 4 , 32 },
  { AUDIO_PCM_FORMAT_FLOAT64_LE, 8 , 64 },
  { AUDIO_PCM_FORMAT_FLOAT64_BE, 8 , 64 },
  { AUDIO_PCM_FORMAT_S24_3LE, 3 , 24 },
  { AUDIO_PCM_FORMAT_S24_3BE, 3 , 24 },
  { AUDIO_PCM_FORMAT_U24_3LE, 3 , 24 },
  { AUDIO_PCM_FORMAT_U24_3BE, 3 , 24 },
  { AUDIO_PCM_FORMAT_S20_3LE, 3 , 20 },
  { AUDIO_PCM_FORMAT_S20_3BE, 3 , 20 },
  { AUDIO_PCM_FORMAT_U20_3LE, 3 , 20 },
  { AUDIO_PCM_FORMAT_U20_3BE, 3 , 20 },
  { AUDIO_PCM_FORMAT_S18_3LE, 3 , 18 },
  { AUDIO_PCM_FORMAT_S18_3BE, 3 , 18 },
  { AUDIO_PCM_FORMAT_U18_3LE, 3 , 18 },
  { AUDIO_PCM_FORMAT_U18_3BE, 3 , 18 },
  /*
  { AUDIO_PCM_FORMAT_G723_24, SND_PCM_FORMAT_G723_24 },
  { AUDIO_PCM_FORMAT_G723_24_1B, SND_PCM_FORMAT_G723_24_1B },
  { AUDIO_PCM_FORMAT_G723_40, SND_PCM_FORMAT_G723_40 },
  { AUDIO_PCM_FORMAT_G723_40_1B, SND_PCM_FORMAT_G723_40_1B },
  { AUDIO_PCM_FORMAT_DSD_U8, SND_PCM_FORMAT_DSD_U8 },
  { AUDIO_PCM_FORMAT_DSD_U16_LE, SND_PCM_FORMAT_DSD_U16_LE },
  { AUDIO_PCM_FORMAT_DSD_U32_LE, SND_PCM_FORMAT_DSD_U32_LE },
  { AUDIO_PCM_FORMAT_DSD_U16_BE, SND_PCM_FORMAT_DSD_U16_BE },
  { AUDIO_PCM_FORMAT_DSD_U32_BE, SND_PCM_FORMAT_DSD_U32_BE },
  */
  { AUDIO_PCM_FORMAT_UNKNOWN, 0, 0 }
};

// ----------------------------------------------------------------------------------
int get_sample_size_by_pcm_format(AUDIO_PCM_FORMAT pcm_format)
{
    for(int i=0;;i++)
    {
        if (pcm_ss_dict[i].pcm==pcm_format || pcm_ss_dict[i].pcm==AUDIO_PCM_FORMAT_UNKNOWN) return pcm_ss_dict[i].byte_sample_size;
    }
    return 0;
}

// ----------------------------------------------------------------------------------
int get_bits_sample_size_by_pcm_format(AUDIO_PCM_FORMAT pcm_format)
{
    for(int i=0;;i++)
    {
        if (pcm_ss_dict[i].pcm==pcm_format || pcm_ss_dict[i].pcm==AUDIO_PCM_FORMAT_UNKNOWN) return pcm_ss_dict[i].bit_sample_size;
    }
    return 0;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
struct _pcm_string_drec
{
    AUDIO_PCM_FORMAT pcm;
    const char *str;
};

_pcm_string_drec pcm_string_dict[]=
{
  { AUDIO_PCM_FORMAT_S8, "AUDIO_PCM_FORMAT_S8" },
  { AUDIO_PCM_FORMAT_U8, "AUDIO_PCM_FORMAT_U8" },
  { AUDIO_PCM_FORMAT_S16_LE, "AUDIO_PCM_FORMAT_S16_LE" },
  { AUDIO_PCM_FORMAT_S16_BE, "AUDIO_PCM_FORMAT_S16_BE" },
  { AUDIO_PCM_FORMAT_U16_LE, "AUDIO_PCM_FORMAT_U16_LE" },
  { AUDIO_PCM_FORMAT_U16_BE, "AUDIO_PCM_FORMAT_U16_BE" },
  { AUDIO_PCM_FORMAT_S24_LE, "AUDIO_PCM_FORMAT_S24_LE" },
  { AUDIO_PCM_FORMAT_S24_BE, "AUDIO_PCM_FORMAT_S24_BE" },
  { AUDIO_PCM_FORMAT_U24_LE, "AUDIO_PCM_FORMAT_U24_LE" },
  { AUDIO_PCM_FORMAT_U24_BE, "AUDIO_PCM_FORMAT_U24_BE" },
  { AUDIO_PCM_FORMAT_S32_LE, "AUDIO_PCM_FORMAT_S32_LE" },
  { AUDIO_PCM_FORMAT_S32_BE, "AUDIO_PCM_FORMAT_S32_BE" },
  { AUDIO_PCM_FORMAT_U32_LE, "AUDIO_PCM_FORMAT_U32_LE" },
  { AUDIO_PCM_FORMAT_U32_BE, "AUDIO_PCM_FORMAT_U32_BE" },
  { AUDIO_PCM_FORMAT_FLOAT_LE, "AUDIO_PCM_FORMAT_FLOAT_LE" },
  { AUDIO_PCM_FORMAT_FLOAT_BE, "AUDIO_PCM_FORMAT_FLOAT_BE" },
  { AUDIO_PCM_FORMAT_FLOAT64_LE, "AUDIO_PCM_FORMAT_FLOAT64_LE" },
  { AUDIO_PCM_FORMAT_FLOAT64_BE, "AUDIO_PCM_FORMAT_FLOAT64_BE" },
  { AUDIO_PCM_FORMAT_IEC958_SUBFRAME_LE, "AUDIO_PCM_FORMAT_IEC958_SUBFRAME_LE" },
  { AUDIO_PCM_FORMAT_IEC958_SUBFRAME_BE, "AUDIO_PCM_FORMAT_IEC958_SUBFRAME_BE" },
  { AUDIO_PCM_FORMAT_MU_LAW, "AUDIO_PCM_FORMAT_MU_LAW" },
  { AUDIO_PCM_FORMAT_A_LAW, "AUDIO_PCM_FORMAT_A_LAW" },
  { AUDIO_PCM_FORMAT_IMA_ADPCM, "AUDIO_PCM_FORMAT_IMA_ADPCM" },
  { AUDIO_PCM_FORMAT_MPEG, "AUDIO_PCM_FORMAT_MPEG" },
  { AUDIO_PCM_FORMAT_GSM, "AUDIO_PCM_FORMAT_GSM" },
  { AUDIO_PCM_FORMAT_SPECIAL, "AUDIO_PCM_FORMAT_SPECIAL" },
  { AUDIO_PCM_FORMAT_S24_3LE, "AUDIO_PCM_FORMAT_S24_3LE" },
  { AUDIO_PCM_FORMAT_S24_3BE, "AUDIO_PCM_FORMAT_S24_3BE" },
  { AUDIO_PCM_FORMAT_U24_3LE, "AUDIO_PCM_FORMAT_U24_3LE" },
  { AUDIO_PCM_FORMAT_U24_3BE, "AUDIO_PCM_FORMAT_U24_3BE" },
  { AUDIO_PCM_FORMAT_S20_3LE, "AUDIO_PCM_FORMAT_S20_3LE" },
  { AUDIO_PCM_FORMAT_S20_3BE, "AUDIO_PCM_FORMAT_S20_3BE" },
  { AUDIO_PCM_FORMAT_U20_3LE, "AUDIO_PCM_FORMAT_U20_3LE" },
  { AUDIO_PCM_FORMAT_U20_3BE, "AUDIO_PCM_FORMAT_U20_3BE" },
  { AUDIO_PCM_FORMAT_S18_3LE, "AUDIO_PCM_FORMAT_S18_3LE" },
  { AUDIO_PCM_FORMAT_S18_3BE, "AUDIO_PCM_FORMAT_S18_3BE" },
  { AUDIO_PCM_FORMAT_U18_3LE, "AUDIO_PCM_FORMAT_U18_3LE" },
  { AUDIO_PCM_FORMAT_U18_3BE, "AUDIO_PCM_FORMAT_U18_3BE" },
  /*
  { AUDIO_PCM_FORMAT_G723_24, SND_PCM_FORMAT_G723_24 },
  { AUDIO_PCM_FORMAT_G723_24_1B, SND_PCM_FORMAT_G723_24_1B },
  { AUDIO_PCM_FORMAT_G723_40, SND_PCM_FORMAT_G723_40 },
  { AUDIO_PCM_FORMAT_G723_40_1B, SND_PCM_FORMAT_G723_40_1B },
  { AUDIO_PCM_FORMAT_DSD_U8, SND_PCM_FORMAT_DSD_U8 },
  { AUDIO_PCM_FORMAT_DSD_U16_LE, SND_PCM_FORMAT_DSD_U16_LE },
  { AUDIO_PCM_FORMAT_DSD_U32_LE, SND_PCM_FORMAT_DSD_U32_LE },
  { AUDIO_PCM_FORMAT_DSD_U16_BE, SND_PCM_FORMAT_DSD_U16_BE },
  { AUDIO_PCM_FORMAT_DSD_U32_BE, SND_PCM_FORMAT_DSD_U32_BE },
  */
  { AUDIO_PCM_FORMAT_UNKNOWN, "AUDIO_PCM_FORMAT_UNKNOWN" }
};

// ----------------------------------------------------------------------------------
const char         *get_str_name_by_pcm_format(AUDIO_PCM_FORMAT pcm_format)
{
    for(int i=0;;i++)
    {
        if (pcm_string_dict[i].pcm==pcm_format || pcm_string_dict[i].pcm==AUDIO_PCM_FORMAT_UNKNOWN) return pcm_string_dict[i].str;
    }
}

// ----------------------------------------------------------------------------------
AUDIO_PCM_FORMAT    get_pcm_format_by_str_name(const char *name)
{
    for(int i=0;;i++)
    {
        if (strcmp(pcm_string_dict[i].str,name)==0 || pcm_string_dict[i].pcm==AUDIO_PCM_FORMAT_UNKNOWN) return pcm_string_dict[i].pcm;
    }
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
struct _pcm_av_drec
{
    AUDIO_PCM_FORMAT pcm;
    AVSampleFormat av;
};

_pcm_av_drec pcm_av_dict[]=
{
  { AUDIO_PCM_FORMAT_U8,            AV_SAMPLE_FMT_U8 },
  { AUDIO_PCM_FORMAT_S16_LE,        AV_SAMPLE_FMT_S16 },
  { AUDIO_PCM_FORMAT_S32_LE,        AV_SAMPLE_FMT_S32 },
  { AUDIO_PCM_FORMAT_FLOAT_LE,      AV_SAMPLE_FMT_FLT },
  { AUDIO_PCM_FORMAT_FLOAT64_LE,    AV_SAMPLE_FMT_DBL },
  { AUDIO_PCM_FORMAT_UNKNOWN,       AV_SAMPLE_FMT_NONE }
};


// -----------------------------------------------------------------------------
AVSampleFormat    get_av_format_by_pcm_format(AUDIO_PCM_FORMAT pcm_format)
{
    for(int i=0;;i++)
    {
        if (pcm_av_dict[i].pcm==pcm_format || pcm_av_dict[i].pcm==AUDIO_PCM_FORMAT_UNKNOWN) return pcm_av_dict[i].av;
    }
    return AV_SAMPLE_FMT_NONE;
}

// -----------------------------------------------------------------------------
AUDIO_PCM_FORMAT    get_pcm_format_by_av_format(AVSampleFormat av_format)
{
    for(int i=0;;i++)
    {
        if (pcm_av_dict[i].av==av_format || pcm_av_dict[i].av==AV_SAMPLE_FMT_NONE) return pcm_av_dict[i].pcm;
    }
    return AUDIO_PCM_FORMAT_UNKNOWN;
}
