#include "dxshow.hpp"

#ifdef WINDOWS

// ------------------------------------------------------------------
// ------------------------------------------------------------------
class _aren_pipe:public _audio_render_pipe
{
    private:

        IGraphBuilder *pFilterGraphBuilder;
        _media_source *source;
        _audio_render *render;
        IMediaControl *control;
        
    public:

        _aren_pipe();
        ~_aren_pipe();

        virtual void set_audio_source(_media_source *vs);
        virtual _media_source *audio_source() { return source; };

        /*
        virtual void set_video_decoder(_video_encoder *e) = 0;
        virtual _video_decoder *video_encoder() = 0;
        */

        virtual void set_audio_render(_audio_render *r);
        virtual _audio_render *audio_render() { return render; };
        

        virtual IGraphBuilder *graph() { return pFilterGraphBuilder; };

        virtual void start();
        virtual void stop();
        virtual void pause();

        //virtual void set_video_window(RECT *r,HWND w);

        //virtual void set_need_preview(bool n) = 0;

        virtual void release() { delete this; };
};

// ------------------------------------------------------------------
_aren_pipe::_aren_pipe()
    :pFilterGraphBuilder(NULL),source(NULL),control(NULL),render(NULL)
{
    
    HRESULT hr = CoInitialize(NULL);
    if (FAILED(hr)) if (FAILED(hr)) throw(_video_capture_error(0,"_aren_pipe::_aren_pipe - can not init COM"));

    // Create the Filter Graph Manager.
    hr = CoCreateInstance(CLSID_FilterGraph, 0, CLSCTX_INPROC_SERVER, IID_IGraphBuilder, (void**)&pFilterGraphBuilder);
    if (FAILED(hr)) throw(_video_capture_error(0,"_aren_pipe::_aren_pipe - can not create Graph Builder"));
}

// ------------------------------------------------------------------
_aren_pipe::~_aren_pipe()
{
    if (pFilterGraphBuilder) pFilterGraphBuilder->Release();

    if (control)
    {
        control->Stop();
        control->Release();
    }
    
    //if (source) source->release();
    source = NULL;
    render = NULL;

    CoUninitialize();
}

// ------------------------------------------------------------------
void _aren_pipe::set_audio_source(_media_source *vs)
{
    HRESULT hr;
    
    if (source)
    {
        hr = pFilterGraphBuilder->RemoveFilter(source->filter());
        if (FAILED(hr)) throw(_video_capture_error(hr,"_audio_render_pipe::video_source(set) - can not remove prev Source Filter from Filter Graph"));
        
        source->release();
    }

    source = vs;

    if (source)
    {
        hr = pFilterGraphBuilder->AddFilter(source->filter(), L"Source Filter");
        if (FAILED(hr)) throw(_video_capture_error(hr,"_audio_render_pipe::video_source(set) - can not add Source Filter to Filter Graph"));
    }
}

// ------------------------------------------------------------------
void _aren_pipe::set_audio_render(_audio_render *vs)
{
    HRESULT hr;
    
    if (render)
    {
        hr = pFilterGraphBuilder->RemoveFilter(render->filter());
        if (FAILED(hr)) throw(_video_capture_error(hr,"_audio_render_pipe::audio_render(set) - can not remove prev Render Filter from Filter Graph"));
        
        render->release();
    }

    render = vs;

    if (render)
    {
        hr = pFilterGraphBuilder->AddFilter(render->filter(), L"Render Filter");
        if (FAILED(hr)) throw(_video_capture_error(hr,"_audio_render_pipe::audio_render(set) - can not add Render Filter to Filter Graph"));
    }
}

// ------------------------------------------------------------------
void _aren_pipe::start()
{
    if (control)
    {
        control->Run();
        return;
    }

    IFilterGraph2 *pFG2 = NULL;
    
    HRESULT hr = pFilterGraphBuilder->QueryInterface( IID_IFilterGraph2, (void **)&pFG2);
    if (FAILED(hr)) throw(_video_capture_error(hr,"_aren_pipe::start - can not create Filter Graph"));

    hr = pFG2->RenderEx(source->get_pin(0),AM_RENDEREX_RENDERTOEXISTINGRENDERERS,NULL);
    if (FAILED(hr)) throw(_video_capture_error(hr,"_aren_pipe::start - can not render"));

    pFG2->Release();
    
    /*
    HRESULT hr = pFilterGraphBuilder->Render(source->get_pin(0));
    if (FAILED(hr)) throw(_video_capture_error(hr,"_aren_pipe::start - can not render"));
    */

    hr = pFilterGraphBuilder->QueryInterface( IID_IMediaControl, (void **)&control );
    if (FAILED(hr)) throw(_video_capture_error(hr,"_aren_pipe::start - can get Control Interface"));
    
    hr = control->Run();
    //if (FAILED(hr)) throw(_video_capture_error(hr,"_aren_pipe::start - can Run"));
}


// ------------------------------------------------------------------
void _aren_pipe::stop()
{
    if (control) control->Stop();
    if (source) source->clear_media_samples();
    
}

// ------------------------------------------------------------------
void _aren_pipe::pause()
{
    if (control) control->Pause();
}

// ------------------------------------------------------------------
_audio_render_pipe *create_audio_render_pipe()
{
    return new _aren_pipe;
}

#endif