#include "dxshow.hpp"

#ifdef WINDOWS

// ------------------------------------------------------------------
// ------------------------------------------------------------------
class _vren_pipe:public _video_render_pipe
{
    private:

        IGraphBuilder *pFilterGraphBuilder;        
        _media_source *source;
        _video_render *render;
        
        IMediaControl *control;
        
        //IBaseFilter *VMR;
        //IVMRWindowlessControl *Wl_control;
        
        //IVideoWindow *window;

        RECT video_window_pos;
        HWND parent_window;
        
    public:

        _vren_pipe();
        ~_vren_pipe();

        virtual void set_video_source(_media_source *vs);
        virtual _media_source *video_source() { return source; };

        /*
        virtual void set_video_decoder(_video_encoder *e) = 0;
        virtual _video_decoder *video_encoder() = 0;
        */
        
        virtual void set_video_render(_video_render *r);
        virtual _video_render *video_render() { return render; };

        virtual IGraphBuilder *graph() { return pFilterGraphBuilder; };

        virtual void start();
        virtual void stop();
        virtual void pause();

        virtual void set_video_window(RECT *r,HWND w);

        //virtual void set_need_preview(bool n) = 0;

        virtual void release() { delete this; };

        virtual void redraw_video();
};

// ------------------------------------------------------------------
_vren_pipe::_vren_pipe()
    :pFilterGraphBuilder(NULL),source(NULL),control(NULL),parent_window(NULL),render(NULL)
    //,window(NULL),VMR(NULL),Wl_control(NULL)
{
    
    HRESULT hr = CoInitialize(NULL);
    if (FAILED(hr)) if (FAILED(hr)) throw(_video_capture_error(0,"_vren_pipe::_vren_pipe - can not init COM"));

    // Create the Filter Graph Manager.
    hr = CoCreateInstance(CLSID_FilterGraph, 0, CLSCTX_INPROC_SERVER, IID_IGraphBuilder, (void**)&pFilterGraphBuilder);
    if (FAILED(hr)) throw(_video_capture_error(0,"_vren_pipe::_vren_pipe - can not create Graph Builder"));

    video_window_pos.left=0;
    video_window_pos.top=0;
    video_window_pos.right=0;
    video_window_pos.left=0;
}

// ------------------------------------------------------------------
_vren_pipe::~_vren_pipe()
{
    if (control)
    {
        control->Stop();
    }

    /*
    if (Wl_control) { Wl_control->Release(); Wl_control = NULL; }
    if (VMR) { VMR->Release(); VMR = NULL; }
    */
    
    //if (window) window->put_Owner(NULL);
    if (pFilterGraphBuilder) { pFilterGraphBuilder->Release(); pFilterGraphBuilder = NULL; }

    if (control) { control->Release(); control = NULL; }

    //if (window) window->Release();
    
    //if (source) source->release();
    source = NULL;

    CoUninitialize();
}

// ------------------------------------------------------------------
void _vren_pipe::set_video_source(_media_source *vs)
{
    HRESULT hr;
    
    if (source)
    {
        hr = pFilterGraphBuilder->RemoveFilter(source->filter());
        if (FAILED(hr)) throw(_video_capture_error(hr,"_video_render_pipe::video_source(set) - can not remove prev Source Filter from Filter Graph"));
        
        source->release();
    }

    source = vs;

    if (source)
    {
        hr = pFilterGraphBuilder->AddFilter(source->filter(), L"Source Filter");
        if (FAILED(hr)) throw(_video_capture_error(hr,"_video_render_pipe::video_source(set) - can not add Source Filter to Filter Graph"));
    }
}

// ------------------------------------------------------------------
void _vren_pipe::set_video_render(_video_render *vr)
{
    HRESULT hr;
    
    if (render)
    {
        hr = pFilterGraphBuilder->RemoveFilter(render->filter());
        if (FAILED(hr)) throw(_video_capture_error(hr,"_video_render_pipe::video_render(set) - can not remove prev Render Filter from Filter Graph"));
        
        render->release();
    }

    render = vr;

    if (render)
    {
        hr = pFilterGraphBuilder->AddFilter(render->filter(), L"Render Filter");
        if (FAILED(hr)) throw(_video_capture_error(hr,"_video_render_pipe::video_render(set) - can not add Render Filter to Filter Graph"));
    }
}

// ------------------------------------------------------------------
void _vren_pipe::start()
{
    if (control)
    {
        control->Run();
        return;
    }

    HRESULT hr;

    /*HRESULT hr = CoCreateInstance(CLSID_VideoMixingRenderer, NULL, CLSCTX_INPROC, IID_IBaseFilter, (void**)&VMR);
    if (FAILED(hr)) throw(_video_capture_error(hr,"_vren_pipe::start - can not create VMR"));

    hr = pFilterGraphBuilder->AddFilter(VMR,L"Video Mixing Renderer");
    if (FAILED(hr)) throw(_video_capture_error(hr,"_vren_pipe::start - can not add VMR Filter"));

    IVMRFilterConfig* pConfig;
    
    hr = VMR->QueryInterface(IID_IVMRFilterConfig, (void**)&pConfig); 
    if (FAILED(hr)) throw(_video_capture_error(hr,"_vren_pipe::start - can not get Config Interface"));

    hr = pConfig->SetRenderingMode(VMRMode_Windowless); 
    pConfig->Release(); 

    hr = VMR->QueryInterface(IID_IVMRWindowlessControl, (void**)&Wl_control);
    if (FAILED(hr)) throw(_video_capture_error(hr,"_vren_pipe::start - can not get WL Control"));

    hr = Wl_control->SetVideoClippingWindow(parent_window);

    RECT rcSrc, rcDest; 
    // Set the source rectangle.
    SetRect(&rcSrc, 0, 0, 352, 288); 
    
    // Get the window client area.
    GetClientRect(parent_window, &rcDest); 
    // Set the destination rectangle.
    SetRect(&rcDest, video_window_pos.left, video_window_pos.top, video_window_pos.right, video_window_pos.bottom); 
    
    // Set the video position.
    hr = Wl_control->SetVideoPosition(NULL, &rcDest); 
*/

    IFilterGraph2 *pFG2 = NULL;
    
    hr = pFilterGraphBuilder->QueryInterface( IID_IFilterGraph2, (void **)&pFG2);
    if (FAILED(hr)) throw(_video_capture_error(hr,"_vren_pipe::start - can not create Filter Graph"));

    hr = pFG2->RenderEx(source->get_pin(0),AM_RENDEREX_RENDERTOEXISTINGRENDERERS,NULL);
    if (FAILED(hr)) throw(_video_capture_error(hr,"_vren_pipe::start - can not render"));

    pFG2->Release();
    
    /*
    hr = pFilterGraphBuilder->Render(source->get_pin(0));
    if (FAILED(hr)) throw(_video_capture_error(hr,"_vren_pipe::start - can not render"));
    */


    hr = pFilterGraphBuilder->QueryInterface( IID_IMediaControl, (void **)&control );
    if (FAILED(hr)) throw(_video_capture_error(hr,"_vren_pipe::start - can get Control Interface"));
    
    control->Run();

    /*hr = pFilterGraphBuilder->QueryInterface( IID_IVideoWindow, (void **)&window );
    if (FAILED(hr)) throw(_video_capture_error(hr,"_vren_pipe::start - can get Window Interface"));
    */

    /*
    if (video_window_pos.left!=video_window_pos.right && video_window_pos.top!=video_window_pos.bottom)
    {
        if (parent_window) window->put_Owner((long)parent_window);
        window->put_WindowStyle(WS_VISIBLE | WS_CHILD |  WS_CLIPSIBLINGS);
        window->SetWindowPosition(video_window_pos.left,video_window_pos.top,video_window_pos.right-video_window_pos.left,video_window_pos.bottom-video_window_pos.top);        
        window->SetWindowForeground(TRUE);
        window->put_AutoShow(TRUE);
    }
    */

    // if 
}

// ------------------------------------------------------------------
void _vren_pipe::set_video_window(RECT *r,HWND w)
{
    memcpy(&video_window_pos,r,sizeof(RECT));
    parent_window=w;

    /*if (video_window_pos.left!=video_window_pos.right && video_window_pos.top!=video_window_pos.bottom)
    {
        if (window)
        {
            if (parent_window) window->put_Owner((long)parent_window);
            window->put_WindowStyle(WS_VISIBLE | WS_CHILD |  WS_CLIPSIBLINGS);
            window->SetWindowPosition(video_window_pos.left,video_window_pos.top,video_window_pos.right-video_window_pos.left,video_window_pos.bottom-video_window_pos.top);            
        }
    }
    */
}

// ------------------------------------------------------------------
void _vren_pipe::stop()
{
    if (control) control->Stop();
}

// ------------------------------------------------------------------
void _vren_pipe::pause()
{
    if (control) control->Pause();
}

// ------------------------------------------------------------------
void _vren_pipe::redraw_video()
{
    /*
    if (window)
    {
        window->NotifyOwnerMessage((OAHWND)parent_window,WM_PAINT,NULL,NULL);
    }
    */

    /*
    if (Wl_control)
    {
        PAINTSTRUCT ps; 
        HDC         hdc;
    
        hdc = BeginPaint(parent_window, &ps); 

        Wl_control->RepaintVideo(parent_window, hdc);

        EndPaint(parent_window,&ps);
    }
    */
}

// ------------------------------------------------------------------
_video_render_pipe *create_video_render_pipe()
{
    return new _vren_pipe;
}

#endif