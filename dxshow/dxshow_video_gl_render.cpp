#include "dxshow_video_gl_render.hpp"
#include <ghelp.hpp>
#include <ghelp_gl.hpp>

#include <iostream>
#include <string.h>

extern "C" {
#include <libavutil/opt.h>
#include <libavcodec/avcodec.h>
#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>
//#include <libavutil/motion_vector.h>
}

#include <log.hpp>
#define MLOG(L) _log() << _log::_set_module_level("vcall",(L))


class _v_gl_render;
class _v_gl_render_wnd;

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
class _v_gl_render_in_pin:public _pin
{
	protected:
		_v_gl_render_wnd *cm_wnd;

		virtual ~_v_gl_render_in_pin() { disconnect_from_all(); };

	public:
		_v_gl_render_in_pin(_v_gl_render_wnd *wnd):cm_wnd(wnd) { cm_is_input=true; };

		virtual void start();
		virtual void stop();
		virtual void pause();

		virtual void push_media_sample(const _media_sample *ms);
		virtual void push_stream_header(const _stream_header *sh);

        // sometimes render window deleted by its parent window before pin delete & we need notify pin do not use window
		virtual void render_wnd_deleted() { cm_wnd=NULL; };
		//virtual void on_connect(_pin *from);
};

// --------------------------------------------------------------
// --------------------------------------------------------------
class _v_gl_render_wnd:public _gh::_gl_wnd
{
    protected:

        /*
        struct _motion_vector
        {
            int dir;
            float sx;
            float sy;
            float dx;
            float dy;
            float w;
            float h;
            int color;
        };

        struct _cell
        {
            float vx;
            float vy;

            float avg_x;
            float avg_y;

            int cnt;

            int sx;
            int sy;
            int dx;
            int dy;

            int rsx;
            int rsy;
            int rdx;
            int rdy;

            int color;

            bool is_move;
        };

        _cell *cm_grid;
        int cm_grid_x;
        int cm_grid_y;

        _motion_vector *cm_motion_vectors;
        int cm_mvs_cnt;
        */

        virtual void render();

        virtual void on_create();
        virtual void on_delete();

        unsigned int texture_id;
        int frame_cnt;

        SwsContext *cm_sws_ctx;
        AVPicture cm_out_pic;

        int img_w;
        int img_h;

        _v_gl_render *cm_render;

        _locker lock;

    public:
      _v_gl_render_wnd(_gh::_wnd *p,const _gh::_rect &r,_v_gl_render *ren);
      virtual ~_v_gl_render_wnd();

	  virtual void render_media_sample(const _media_sample *ms);
};

// --------------------------------------------------------------
// --------------------------------------------------------------
class _v_gl_render:public _video_render
{
	protected:
		_pins_ptr_list cm_in_pins;
		_v_gl_render_in_pin *cm_in_pin;

		_gh::_gl_wnd *cm_mwnd;
		_v_gl_render_wnd *cm_wnd;

	public:

		_v_gl_render(_gh::_gl_wnd *mwnd,const _gh::_rect &r);
		virtual ~_v_gl_render();

		virtual void release() { delete this; };

		virtual const _pins_ptr_list &in_pins() const { return cm_in_pins; };

        // sometimes render window deleted by its parent window before render delete & we need notify render do not use & delete window
		virtual void render_wnd_deleted() { if (cm_in_pin) cm_in_pin->render_wnd_deleted(); cm_wnd=NULL; };

		virtual void resize_repos(const _rect &r) { _gh::_rect wr; wr.x=r.x; wr.y=r.y; wr.w=r.w; wr.h=r.h; if (cm_wnd) cm_wnd->resize_repos(wr); };
};

// --------------------------------------------------------------
// --------------------------------------------------------------
_v_gl_render_wnd::_v_gl_render_wnd(_gh::_wnd *p,const _gh::_rect &r,_v_gl_render *ren)
:_gh::_gl_wnd(p,r),texture_id(0),frame_cnt(0),cm_sws_ctx(NULL),cm_render(ren)
//,cm_motion_vectors(NULL),cm_mvs_cnt(0),cm_grid(NULL)
{
}

// --------------------------------------------------------------
void _v_gl_render_wnd::on_create()
{
    lock.lock();
	glGenTextures(1,&texture_id);
	lock.unlock();
}

// --------------------------------------------------------------
_v_gl_render_wnd::~_v_gl_render_wnd()
{
    lock.lock();

    if (cm_sws_ctx)
    {
        sws_freeContext(cm_sws_ctx);
        avpicture_free(&cm_out_pic);
    }

    lock.unlock();

    /*
    cm_mvs_cnt=0;
    if (cm_motion_vectors)
    {
        delete cm_motion_vectors;
        cm_motion_vectors=NULL;
    }

    if (cm_grid)
    {
        delete cm_grid;
        cm_grid=NULL;
    }
    */
}

// --------------------------------------------------------------
void _v_gl_render_wnd::on_delete()
{
    lock.lock();

    cm_render->render_wnd_deleted();

	frame_cnt=0;
	glDeleteTextures(1,&texture_id);

	lock.unlock();
}

// ------------------------------------------------------------
void _v_gl_render_wnd::render()
{
    if (!texture_id) return;

    //if (!lock.try_lock()) return;

    //lock.lock();
	if (frame_cnt)
	{
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,texture_id);

		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, img_w, img_h, 0, GL_RGBA, GL_UNSIGNED_BYTE, cm_out_pic.data[0]);
		//glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, img_w, img_h, GL_RGBA, GL_UNSIGNED_BYTE, cm_out_pic.data[0]);

		glColor3ub(0xFF,0xFF,0xFF);

		glBegin(GL_QUADS);
			glTexCoord2d(0,1); glVertex2f(0,cm_rect.h);
			glTexCoord2d(0,0); glVertex2f(0,0);
			glTexCoord2d(1,0); glVertex2f(cm_rect.w,0);
			glTexCoord2d(1,1); glVertex2f(cm_rect.w,cm_rect.h);
		glEnd();


        /*if (1)
        {
            glDisable(GL_TEXTURE_2D);

            _cell *c=cm_grid;
            int i=0;
            for(;i<cm_grid_x*cm_grid_y;i++,c++)
            {
                //if (c->color==250)
                //{
                //    if (c->vx*c->vx+c->vy*c->vy < 200)
                //    {
                //        c->color==250;
                //        continue;
                //    }
                //}

                if (c->cnt<1 && c->color==0) continue;

                if (c->is_move)
                {
                    glColor3ub(0,c->color,0);
                    glBegin(GL_LINE_LOOP);
                        glVertex2f(c->rsx, c->rsy);
                        glVertex2f(c->rdx, c->rsy);
                        glVertex2f(c->rdx, c->rdy);
                        glVertex2f(c->rsx, c->rdy);
                    glEnd();
                }
                else
                {
                    //glColor3ub(c->color,c->color,c->color);
                }


                glColor3ub(0xFF,0x00,0x00);
                glBegin(GL_LINES);
                    glVertex2f(c->rsx, c->rsy);
                    glVertex2f(c->rsx + c->vx, c->rsy + c->vy);
                glEnd();

                //c->cnt=0;
                c->color-=25;
                //c->vx=0;
                //c->vy=0;
                if (c->color==0)
                {

                }
            }
            /*
            int i;
            for(i=0;i<cm_mvs_cnt;i++)
            {
                float dx=cm_motion_vectors[i].sx-cm_motion_vectors[i].dx;
                float dy=cm_motion_vectors[i].sy-cm_motion_vectors[i].dy;
                if (cm_motion_vectors[i].color)
                {
                    glColor3ub(0xFF,0xFF,0xFF);
                    glBegin(GL_LINE_LOOP);
                    glVertex2f(cm_motion_vectors[i].sx, cm_motion_vectors[i].sy);
                    glVertex2f(cm_motion_vectors[i].sx + cm_motion_vectors[i].w, cm_motion_vectors[i].sy);
                    glVertex2f(cm_motion_vectors[i].sx + cm_motion_vectors[i].w, cm_motion_vectors[i].sy + cm_motion_vectors[i].h);
                    glVertex2f(cm_motion_vectors[i].sx, cm_motion_vectors[i].sy + cm_motion_vectors[i].h);
                    glEnd();

                    glColor3ub(0xFF,0x00,0x00);
                    glBegin(GL_LINES);
                    glVertex2f(cm_motion_vectors[i].sx, cm_motion_vectors[i].sy);
                    glVertex2f(cm_motion_vectors[i].dx ,cm_motion_vectors[i].dy);
                    glEnd();
                }
            }
            */
        /*
        }
        */

	}
	//lock.unlock();
}

// --------------------------------------------------------------
void _v_gl_render_wnd::render_media_sample(const _media_sample *ms)
{
    if (!is_visible()) return; // we can save CPU usage -))

    lock.lock();

    if (ms->cm_btype!=MEDIA_SAMPLE_BT_AV_FRAME)
    {
        lock.unlock();
        throw _pm_error(0,"_v_gl_render_wnd::render_media_sample - media sample type!=MEDIA_SAMPLE_TYPE_AV_FRAME, this type not support yet.");
    }

    AVFrame *in_frame=(AVFrame *)ms->buffer;

    // motion test
    /*
    if (frame_cnt==0) // first frame
    {
        float scale_x=cm_rect.w/in_frame->width;
        float scale_y=cm_rect.h/in_frame->height;

        // make grid
        cm_grid_x=in_frame->width >> 3;
        cm_grid_y=in_frame->height >> 3;
        cm_grid=new _cell [cm_grid_x * cm_grid_y];
        for(int y=0;y<cm_grid_y;y++)
        {
            for(int x=0;x<cm_grid_x;x++)
            {
                _cell *c=cm_grid+(x+y*cm_grid_x);
                c->sx=x<<3;
                c->sy=y<<3;
                c->dx=((x+1)<<3)-1;
                c->dy=((y+1)<<3)-1;

                c->rsx=c->sx*scale_x;
                c->rsy=c->sy*scale_y;
                c->rdx=c->dx*scale_x;
                c->rdy=c->dy*scale_y;
            }
        }
    }

    /*AVFrameSideData *side_data = av_frame_get_side_data(in_frame, AV_FRAME_DATA_MOTION_VECTORS);
    if (side_data)
    {
        const AVMotionVector *mvs;
        int mv_cnt = side_data->size / sizeof(*mvs);

        int i;
        mvs = (const AVMotionVector *)side_data->data;
        for (i = 0; i < side_data->size / sizeof(*mvs); i++)
        {
            const AVMotionVector *mv = &mvs[i];

            float vx=mv->dst_x - mv->src_x;
            float vy=mv->dst_y - mv->src_y;

            //float nx=(vx*352)/in_frame->width;
            //float ny=(vy*288)/in_frame->height;
            //if (nx*nx+ny*ny < 2*2+2*2) continue;

            int v=mv->src_x; if (v<0) v=0; else if (v>=in_frame->width) v=in_frame->width-1;
            int sx = v >> 3;

            v=mv->src_y; if (v<0) v=0; else if (v>=in_frame->height) v=in_frame->height-1;
            int sy = v >> 3;

            v=mv->src_x + mv->w; if (v<0) v=0; else if (v>=in_frame->width) v=in_frame->width-1;
            int dx = v >> 3;

            v=mv->src_y + mv->h; if (v<0) v=0; else if (v>=in_frame->height) v=in_frame->height-1;
            int dy = (v) >> 3;

            for(int y=sy;y<=dy;y++)
            {
                for(int x=sx;x<=dx;x++)
                {
                    _cell *c=cm_grid+x+y*cm_grid_x;
                    c->cnt++;
                    //if (c->color>200) c->is_move=true; else c->is_move=false;
                    c->color=250;

                    if (frame_cnt%11==0)
                    {
                        float avg_x=c->vx;//12;
                        float diff_x=avg_x-c->avg_x;

                        float avg_y=c->vy;//12;
                        float diff_y=avg_y-c->avg_y;

                        if (diff_x*diff_x+diff_y*diff_y > (8*8)*11 )
                        {
                            c->is_move=true;
                        }
                        else
                        {
                            c->is_move=false;
                        }

                        c->vx=0;
                        c->vy=0;
                        c->avg_x=avg_x;
                        c->avg_y=avg_y;
                    }
                    else
                    {
                        c->vx+=vx;
                        c->vy+=vy;
                    }
                }
            }

            /*
            cm_motion_vectors[i].dir = mv->source > 0;
            cm_motion_vectors[i].sx = mv->src_x / scale_x;
            cm_motion_vectors[i].sy = mv->src_y / scale_y;
            cm_motion_vectors[i].dx = mv->dst_x / scale_x;
            cm_motion_vectors[i].dy = mv->dst_y / scale_y;
            cm_motion_vectors[i].w = mv->w / scale_x;
            cm_motion_vectors[i].h = mv->h / scale_y;
            cm_motion_vectors[i].color = 0;
            */
            /*
            // do something with motion vector
        }
        */

        /*
        if (cm_motion_vectors)
        {
            cm_mvs_cnt=0;
            delete cm_motion_vectors;
        }
        cm_motion_vectors = new _motion_vector [mv_cnt];
        cm_mvs_cnt=mv_cnt;

        //MLOG(0) << "cm_mvs_cnt = " << mv_cnt << " side_data->size=" << side_data->size << " AVMotionVector=" << sizeof(*mvs) << endl;

        float scale_x=in_frame->width/cm_rect.w;
        float scale_y=in_frame->height/cm_rect.h;

        int i;
        mvs = (const AVMotionVector *)side_data->data;
        for (i = 0; i < side_data->size / sizeof(*mvs); i++)
        {
            const AVMotionVector *mv = &mvs[i];
            cm_motion_vectors[i].dir = mv->source > 0;
            cm_motion_vectors[i].sx = mv->src_x / scale_x;
            cm_motion_vectors[i].sy = mv->src_y / scale_y;
            cm_motion_vectors[i].dx = mv->dst_x / scale_x;
            cm_motion_vectors[i].dy = mv->dst_y / scale_y;
            cm_motion_vectors[i].w = mv->w / scale_x;
            cm_motion_vectors[i].h = mv->h / scale_y;
            cm_motion_vectors[i].color = 0;
            // do something with motion vector
        }

        int j=0;
        for (i=0;i<cm_mvs_cnt;i++)
        {
            float dx=cm_motion_vectors[i].sx-cm_motion_vectors[i].dx;
            float dy=cm_motion_vectors[i].sy-cm_motion_vectors[i].dy;
            if (dx*dx+dy*dy<10) continue;
            if (cm_motion_vectors[i].color) continue;

            for(j=0;j<cm_mvs_cnt;j++)
            {
                float dx=cm_motion_vectors[j].sx-cm_motion_vectors[j].dx;
                float dy=cm_motion_vectors[j].sy-cm_motion_vectors[j].dy;
                if (dx*dx+dy*dy<10) continue;

                if (i==j) continue;
                if (cm_motion_vectors[j].sx<=cm_motion_vectors[i].sx && cm_motion_vectors[i].sx<=cm_motion_vectors[j].sx+cm_motion_vectors[j].w &&
                    cm_motion_vectors[j].sy<=cm_motion_vectors[i].sy && cm_motion_vectors[i].sy<=cm_motion_vectors[j].sy+cm_motion_vectors[j].h)
                    {
                        cm_motion_vectors[i].color = 1;
                        cm_motion_vectors[j].color = 1;
                        continue;
                    }
                if (cm_motion_vectors[j].sx<=cm_motion_vectors[i].sx+cm_motion_vectors[i].w && cm_motion_vectors[i].sx+cm_motion_vectors[i].w<=cm_motion_vectors[j].sx+cm_motion_vectors[j].w &&
                    cm_motion_vectors[j].sy<=cm_motion_vectors[i].sy && cm_motion_vectors[i].sy<=cm_motion_vectors[j].sy+cm_motion_vectors[j].h)
                    {
                        cm_motion_vectors[i].color = 1;
                        cm_motion_vectors[j].color = 1;
                        continue;
                    }
                if (cm_motion_vectors[j].sx<=cm_motion_vectors[i].sx+cm_motion_vectors[i].w && cm_motion_vectors[i].sx+cm_motion_vectors[i].w<=cm_motion_vectors[j].sx+cm_motion_vectors[j].w &&
                    cm_motion_vectors[j].sy<=cm_motion_vectors[i].sy+cm_motion_vectors[i].h && cm_motion_vectors[i].sy+cm_motion_vectors[i].h<=cm_motion_vectors[j].sy+cm_motion_vectors[j].h)
                    {
                        cm_motion_vectors[i].color = 1;
                        cm_motion_vectors[j].color = 1;
                        continue;
                    }
                if (cm_motion_vectors[j].sx<=cm_motion_vectors[i].sx && cm_motion_vectors[i].sx<=cm_motion_vectors[j].sx+cm_motion_vectors[j].w &&
                    cm_motion_vectors[j].sy<=cm_motion_vectors[i].sy+cm_motion_vectors[i].h && cm_motion_vectors[i].sy+cm_motion_vectors[i].h<=cm_motion_vectors[j].sy+cm_motion_vectors[j].h)
                    {
                        cm_motion_vectors[i].color = 1;
                        cm_motion_vectors[j].color = 1;
                        continue;
                    }

            }
        }
        */
    /*
    }
    */

    if (frame_cnt==0) // first frame
    {
        MLOG(0) << "_v_gl_render_wnd::render_media_sample frame_cnt=0" << endl;
        cm_sws_ctx=sws_getContext(in_frame->width,in_frame->height,(AVPixelFormat)in_frame->format,
                                  in_frame->width,in_frame->height,AV_PIX_FMT_RGBA,
                                  SWS_FAST_BILINEAR,
                                  NULL,NULL,0);
        if (!cm_sws_ctx)
        {
            lock.unlock();
            throw _pm_error(0,"_v_gl_render_wnd::render_media_sample - can not create sws Context");
        }

        int err=avpicture_alloc(&cm_out_pic,AV_PIX_FMT_RGBA,in_frame->width,in_frame->height);
        if (err<0)
        {
            lock.unlock();
            throw _pm_error(0,"_v_gl_render_wnd::render_media_sample - can not alloc out picture");
        }

        img_w=in_frame->width;
        img_h=in_frame->height;

        MLOG(0) << "_v_gl_render_wnd::render_media_sample frame_cnt=0 done" << endl;
    }

    sws_scale(cm_sws_ctx,
              in_frame->data,in_frame->linesize,0,in_frame->height,
              cm_out_pic.data,cm_out_pic.linesize);

    frame_cnt++;

    lock.unlock();
}

// --------------------------------------------------------------
// PIN
// --------------------------------------------------------------
void _v_gl_render_in_pin::start()
{
}

// --------------------------------------------------------------
void _v_gl_render_in_pin::stop()
{
}

// --------------------------------------------------------------
void _v_gl_render_in_pin::pause()
{
}


// --------------------------------------------------------------
void _v_gl_render_in_pin::push_media_sample(const _media_sample *ms)
{
    if (cm_wnd) cm_wnd->render_media_sample(ms);
}

// --------------------------------------------------------------
void _v_gl_render_in_pin::push_stream_header(const _stream_header *sh)
{
}

// --------------------------------------------------------------
// --------------------------------------------------------------

// --------------------------------------------------------------
_v_gl_render::_v_gl_render(_gh::_gl_wnd *mw,const _gh::_rect &r):cm_in_pin(NULL),cm_mwnd(mw)
{
	cm_wnd=new _v_gl_render_wnd(mw,r,this);

	cm_in_pin=new _v_gl_render_in_pin(cm_wnd);
	cm_in_pins.push_back(cm_in_pin);
}

// --------------------------------------------------------------
_v_gl_render::~_v_gl_render()
{
    //cout << "_v_gl_render::~_v_gl_render" << endl;
    cm_in_pins.clear();
	if (cm_in_pin)
    {
        cm_in_pin->release();
        cm_in_pin=NULL;
    }

    if (cm_wnd)
    {
        cm_wnd->release();
        //cm_mwnd->refresh();
        //while(!cm_wnd->state().was_deleted) Sleep(1);
    }
    //cout << "_v_gl_render::~_v_gl_render done" << endl;
}

// --------------------------------------------------------------
// --------------------------------------------------------------
_video_render *create_video_gl_render(_gh::_gl_wnd *mw,const _gh::_rect &r)
{
    return new _v_gl_render(mw,r);
}
