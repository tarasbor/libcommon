#define __STDC_CONSTANT_MACROS
#define __STDC_LIMIT_MACROS
#define __STDC_FORMAT_MACROS

#include "dxshow.hpp"
#include "dxshow_av_func.hpp"

#ifdef DEBUG
#include <log.hpp>
#define MLOG(L) _log() << _log::_set_module_level("vcall",(L))
#endif

#include <fstream>


extern "C" {
#include <libavutil/opt.h>
#include <libavcodec/avcodec.h>
#include <libavutil/imgutils.h>
}

#include <iostream>
#include <ut_xml_par.hpp>

using namespace std;


// ======================================================================================
//                                           ENCODER
// ======================================================================================
class _aenc;

// ----------------------------------- AVENCODER IN PIN ---------------------------------
class _aenc_in_pin:public _pin
{
	protected:
		_aenc *cm_enc;

		virtual ~_aenc_in_pin();

	public:
		_aenc_in_pin(_aenc *enc);

		virtual void start();
		virtual void stop();
		virtual void pause();

		virtual void push_media_sample(const _media_sample *ms);
		virtual void push_stream_header(const _stream_header *sh);
};

// ----------------------------------- AVENCODER OUT PIN ---------------------------------
class _aenc_out_pin:public _pin
{
	protected:
		_aenc *cm_enc;

		virtual ~_aenc_out_pin();
	public:

		_aenc_out_pin(_aenc *enc);

		virtual void connect(_pin *to);
};

// ----------------------------------- AVENCODER ---------------------------------
class _aenc:public _audio_encoder
{
    friend class _aenc_out_pin;
    friend class _aenc_in_pin;

	protected:

        struct _config
        {
            int ch_count;
            int sample_rate;
            int sample_size;
            int bitrate;
            AVSampleFormat av_pcm_format;
            AUDIO_PCM_FORMAT pcm_format;
        } cm_config;

		AVCodec *codec;
		AVCodecContext *ccontext;

        _media_sample cm_out_ms;
		AVPacket packet;

		AVFrame *tmp_frame;
		int frame_number;
		int64_t prev_frame_pts;

		//uint16_t *samples;
		uint8_t *samples;
		int samples_nb_size;

		int tmp_frame_samples_have;

		int64_t codec_portion_pts;

		bool is_open;

		_aenc_in_pin *cm_in_pin;
		_pins_ptr_list cm_in_pins;

		_aenc_out_pin *cm_out_pin;
		_pins_ptr_list cm_out_pins;

		virtual void open_encoder();
		virtual void close_encoder();

	public:

        //ofstream file;

		_aenc(int index);
        ~_aenc();

        //IBaseFilter *filter() { return NULL; }; // stub
        virtual void release() { delete this; };

		virtual void config(int bitrate);
		virtual void config(int bitrate,int ch_count,int sample_rate,AUDIO_PCM_FORMAT pcm_format);

		virtual void proccess_input_media_sample(const _media_sample *ms);
		virtual void proccess_input_stream_header(const _stream_header *sh);

		virtual const _pins_ptr_list &out_pins() const { return cm_out_pins; };
		virtual const _pins_ptr_list &in_pins() const { return cm_in_pins; };
};

// -----------------------------------------------------------------------------
// PINS
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// IN
// -----------------------------------------------------------------------------
_aenc_in_pin::_aenc_in_pin(_aenc *enc)
 :cm_enc(enc)
{
	cm_is_input=true;
}

// -----------------------------------------------------------------------------
_aenc_in_pin::~_aenc_in_pin()
{
}

// -----------------------------------------------------------------------------
void _aenc_in_pin::start()
{
	(*cm_enc->out_pins().begin())->start();
	cm_state=started;
}

// -----------------------------------------------------------------------------
void _aenc_in_pin::stop()
{
    //cm_enc->close_encoder();
	(*cm_enc->out_pins().begin())->stop();
	cm_state=stoped;
}

// -----------------------------------------------------------------------------
void _aenc_in_pin::pause()
{
	(*cm_enc->out_pins().begin())->pause();
	cm_state=paused;
}

// -----------------------------------------------------------------------------
void _aenc_in_pin::push_media_sample(const _media_sample* ms)
{
	cm_enc->proccess_input_media_sample(ms);
}

// -----------------------------------------------------------------------------
void _aenc_in_pin::push_stream_header(const _stream_header* sh)
{
    cm_enc->proccess_input_stream_header(sh);
}

// -----------------------------------------------------------------------------
// OUT
// -----------------------------------------------------------------------------
_aenc_out_pin::_aenc_out_pin(_aenc *enc)
 :cm_enc(enc)
{
	cm_is_input=false;
}

// -----------------------------------------------------------------------------
_aenc_out_pin::~_aenc_out_pin()
{
}

// -----------------------------------------------------------------------------
void _aenc_out_pin::connect(_pin *to)
{
    _pin::connect(to);

    _av_codec_context_header *acch=new _av_codec_context_header;
    acch->context=cm_enc->ccontext;

    _stream_header sh;
    sh.set_format_header(acch);

    to->push_stream_header(&sh);
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
_aenc::_aenc(int index):codec(NULL),ccontext(NULL),tmp_frame(NULL),
						frame_number(0),is_open(false),
						cm_in_pin(NULL),cm_out_pin(NULL),prev_frame_pts(0),samples(NULL),
						tmp_frame_samples_have(0)
{
    cm_out_ms.cm_btype=MEDIA_SAMPLE_BT_AV_PACKET;
    cm_out_ms.cm_mtype=MEDIA_SAMPLE_MT_AUDIO;

    avcodec_register_all();

	// find encoder
	codec = avcodec_find_encoder((AVCodecID)index);//AV_CODEC_ID_H264);//codec_id);
	if (!codec) throw _pm_error(index,"_aenc::_aenc - Can not find codec.");

	// create context
	ccontext = avcodec_alloc_context3(codec);
	if (!ccontext) throw _pm_error(index,"_aenc::_aenc - Can not create codec context.");

	//file.open("g:\\prj\\cpp\\common\\ffmpeg\\so\\bin\\test.bin",ios::out | ios::binary);
}

// -----------------------------------------------------------------------------
_aenc::~_aenc()
{
    //file.close();

    cout << "_aenc::~_aenc()" << endl;
    close_encoder();
    cout << "_aenc::~_aenc() - close done" << endl;

    if (cm_in_pin)
	{
	    cout << "_aenc::~_aenc() - free in pin" << endl;
		cm_in_pin->release();
	}
	cm_in_pins.clear();

	if (cm_out_pin)
	{
	    cout << "_aenc::~_aenc() - free out pin" << endl;
		// TODO - normal end_of_stream implementation
		// Comment now - infinity recurse loop
		// cm_out_pin->end_of_stream();
		cm_out_pin->release();
	}
	cm_out_pins.clear();

    cout << "_aenc::~_aenc() - free context" << endl;
	av_free(ccontext);
	cout << "_aenc::~_aenc() done" << endl;

}

/* check that a given sample format is supported by the encoder */
/*
// -----------------------------------------------------------------------------
static int check_sample_fmt(AVCodec *codec, enum AVSampleFormat sample_fmt)
{
    const enum AVSampleFormat *p = codec->sample_fmts;

    while (*p != AV_SAMPLE_FMT_NONE) {
        if (*p == sample_fmt)
            return 1;
        p++;
    }
    return 0;
}

/* just pick the highest supported samplerate */
/*
// -----------------------------------------------------------------------------
static int select_sample_rate(AVCodec *codec)
{
    const int *p;
    int best_samplerate = 0;

    if (!codec->supported_samplerates)
        return 44100;

    p = codec->supported_samplerates;
    while (*p) {
        best_samplerate = FFMAX(*p, best_samplerate);
        p++;
    }
    return best_samplerate;
}

/* select layout with the highest channel count */
/*
// -----------------------------------------------------------------------------
static int select_channel_layout(AVCodec *codec)
{
    const uint64_t *p;
    uint64_t best_ch_layout = 0;
    int best_nb_channels   = 0;

    if (!codec->channel_layouts)
        return AV_CH_LAYOUT_STEREO;

    p = codec->channel_layouts;
    while (*p) {
        int nb_channels = av_get_channel_layout_nb_channels(*p);

        if (nb_channels > best_nb_channels) {
            best_ch_layout    = *p;
            best_nb_channels = nb_channels;
        }
        p++;
    }
    return best_ch_layout;
}
*/

// -----------------------------------------------------------------------------
void _aenc::config(int bitrate)
{
    //cout << "_aenc::config" << endl;

    cm_config.bitrate=bitrate;

	/*
	if (tmp_frame)
	{
        av_freep(&tmp_frame->data[0]);
		avcodec_free_frame(&tmp_frame);
		tmp_frame=NULL;
	}

	if (cm_in_pin)
	{
		cm_in_pins.clear();
		cm_in_pin->release();
	}

	if (cm_out_pin)
	{
		// TODO cm_out_pin->end_of_stream();
		cm_out_pins.clear();
		cm_in_pin->release();
	}

    close_encoder();
    */
	// create pins
	cm_in_pin=new _aenc_in_pin(this);
	cm_in_pins.push_back(cm_in_pin);

	cm_out_pin=new _aenc_out_pin(this);
	cm_out_pins.push_back(cm_out_pin);

    /*
    open_encoder();
    */
}

// -----------------------------------------------------------------------------
void _aenc::config(int bitrate,int ch_count,int sample_rate,AUDIO_PCM_FORMAT pcm_format)
{
    cm_config.bitrate=bitrate;
    cm_config.ch_count=ch_count;
    cm_config.sample_rate=sample_rate;
    cm_config.pcm_format=pcm_format;

    cm_config.sample_size=get_sample_size_by_pcm_format(cm_config.pcm_format);
    cm_config.av_pcm_format=get_av_format_by_pcm_format(cm_config.pcm_format);

	// create pins
	cm_in_pin=new _aenc_in_pin(this);
	cm_in_pins.push_back(cm_in_pin);

	cm_out_pin=new _aenc_out_pin(this);
	cm_out_pins.push_back(cm_out_pin);
}


// -----------------------------------------------------------------------------
void _aenc::open_encoder()
{
	if (is_open) throw _pm_error(0,"_aenc::open - Codec is already open.");

	frame_number=0;

	ccontext->bit_rate=cm_config.bitrate; //128*1024;
    ccontext->sample_rate=cm_config.sample_rate; //44100;
    ccontext->channels=cm_config.ch_count;// 2;
    ccontext->sample_fmt=cm_config.av_pcm_format;

    //if (!check_sample_fmt(codec, ccontext->sample_fmt)) throw(_pm_error(0,"Encoder does not support sample format "));

    //ccontext->block_align=0;
    //ccontext->cutoff=0;

    /* select other audio parameters supported by the encoder */
    //ccontext->sample_rate    = 8000;// select_sample_rate(codec);
    ccontext->channel_layout = av_get_default_channel_layout (cm_config.ch_count);//select_channel_layout(codec);
    //ccontext->channels       = 2;//av_get_channel_layout_nb_channels(ccontext->channel_layout);


	// frames per second
	//ccontext->time_base= (AVRational){1,fps};//{1,fps};

	ccontext->strict_std_compliance = FF_COMPLIANCE_EXPERIMENTAL; // TODO - set only if need

    if (avcodec_open2(ccontext, codec, NULL) < 0) throw _pm_error(0,"_aenc::open - Can not open codec.");

    // prepare packet
    av_init_packet(&packet);
    packet.data = NULL; // packet data will be allocated by the encoder
    packet.size = 0;

	is_open=true;
}

// -----------------------------------------------------------------------------
void _aenc::close_encoder()
{
	if (!is_open) return;

    // sometimes we need flush codec
    cout << "flush codec..." << endl;
	int encode_done=1;
	while(encode_done)
    {
        int ret = avcodec_encode_audio2(ccontext, &packet, NULL, &encode_done);
        if (ret < 0) break;

        if (encode_done)
        {
            // TODO - this code will crush app - check later
            /*
            cout << "flush codec...encode done" << endl;
            cm_out_ms.buffer=(unsigned char *)&packet;
            cm_out_ms.stream_stamp+=codec_portion_pts;
            cm_out_ms.can_break=(packet.flags & AV_PKT_FLAG_KEY);

            cm_out_pin->push_media_sample(&cm_out_ms);

            av_free_packet(&packet);

            frame_number++;
            cout << "flush codec...encode done & pushed" << endl;
            */
        }
    }

	if (samples)
	{
	    cout << "free samples" << endl;
		av_freep(&samples);
		samples=NULL;
	}
	if (tmp_frame)
    {
        cout << "free tmp frame" << endl;
        avcodec_free_frame(&tmp_frame);
        tmp_frame=NULL;
    }

    cout << "close codec...." << endl;
	avcodec_close(ccontext);
	cout << "close codec....done" << endl;
	is_open=false;
}


// -----------------------------------------------------------------------------
void _aenc::proccess_input_stream_header(const _stream_header *sh)
{
    close_encoder();

    if (sh->format_header->format_type!=MEDIA_FORMAT_PORTABLE) throw _pm_error(sh->format_header->format_type,"_aenc::proccess_input_stream_header - can not proccess header with givent format type");

    _portable_header *ph=(_portable_header *)sh->format_header;

    ut_xml_par *parser=create_ut_xml_parser();
    parser->parse(ph->text);

    cout << ph->text << endl;

    const ut_xml_tag *root=parser->get_root_tag();
    string value="0";
    const ut_xml_tag *stream_tag=root->find("header.streams.stream.index",&value);
    if (!stream_tag)
    {
        parser->release();
        throw _pm_error(0,"_aenc::proccess_input_stream_header - can not find stream with index 0");
    }

    stream_tag=stream_tag->get_parent_tag();

    cm_config.ch_count=0;
    const ut_xml_tag *val=stream_tag->find("ch_count");
    if (val) cm_config.ch_count=atoi(val->get_value().c_str());

    cm_config.sample_rate=0;
    val=stream_tag->find("sample_rate");
    if (val) cm_config.sample_rate=atoi(val->get_value().c_str());

    val=stream_tag->find("sample_format");
    if (val) cm_config.pcm_format=get_pcm_format_by_str_name(val->get_value().c_str());

    cm_config.sample_size=get_sample_size_by_pcm_format(cm_config.pcm_format);
    cm_config.av_pcm_format=get_av_format_by_pcm_format(cm_config.pcm_format);


    parser->release();

    open_encoder();
}

// -----------------------------------------------------------------------------
void _aenc::proccess_input_media_sample(const _media_sample* ms)
{
    if (ms->stream_stamp<=0) return ;
    if (ms->cm_mtype!=MEDIA_SAMPLE_MT_AUDIO) return;

    AVFrame *in_frame;

    if (ms->cm_btype==MEDIA_SAMPLE_BT_AV_FRAME)
    {
        in_frame=(AVFrame *)ms->buffer;
    }
    else
    {
        throw _pm_error(0,string("_aenc::proccess_input_media_sample - can not proccess media sample with type=")+to_string(ms->cm_btype));
    }

    // alloc tmp_frame at first media sample
    if (!samples)
    {
        // TODO - make init frame alloc in proccess_input_stream_header
        tmp_frame = av_frame_alloc();
        if (!tmp_frame) throw _pm_error(0,"_aenc::proccess_input_media_sample - Can not alloc tmp frame.");

        tmp_frame->nb_samples     = ccontext->frame_size;
        tmp_frame->format         = in_frame->format;
        tmp_frame->channel_layout = in_frame->channel_layout;
        tmp_frame->channels       = in_frame->channels;
        tmp_frame->sample_rate    = in_frame->sample_rate;

        // the codec gives us the frame size, in samples,
        // we calculate the size of the samples buffer in bytes
        samples_nb_size = av_samples_get_buffer_size(NULL, in_frame->channels, ccontext->frame_size,
                                                     (AVSampleFormat)in_frame->format, 0);
        samples = (uint8_t *)av_malloc(samples_nb_size);
        if (!samples) throw (_pm_error(0,string("Could not allocate ")+to_string(samples_nb_size)+" bytes for samples buffer"));

        //* setup the data pointers in the AVFrame
        int ret = avcodec_fill_audio_frame(tmp_frame, in_frame->channels, (AVSampleFormat)in_frame->format,
                                            samples, samples_nb_size, 0);
        if (ret < 0) throw(_pm_error(ret,"Could not setup audio frame"));

    }

    // Input frame can have some count of samples
    // Encoder can encode at one call just some of them (AVCodecContext.frame_size)
    // We must get portion of samples from input frame, put it to tmp frame & encode while
    // input frame have samples more or eq AVCodecContext.frame_size (if less just copy to tmp_frame to encode in next push_sample)

    int in_samples_pos=0;

    //cout << "enter ... " << endl;
    //cout << "in_samples_pos=0 & tmp_frame_samples_have=" << tmp_frame_samples_have << endl;

    while(1)
    {
        //cout << "tick..............." << endl;
        int cp_samples=tmp_frame->nb_samples-tmp_frame_samples_have;
        if (cp_samples>in_frame->nb_samples-in_samples_pos) cp_samples=in_frame->nb_samples-in_samples_pos;
        //cout << "cp_samples=" << cp_samples << " in_samples_pos=" << in_samples_pos << endl;

        if (!tmp_frame_samples_have)
        {
            tmp_frame->pts = ms->stream_stamp;
            tmp_frame->pts+= (_time::time_val_i) 10000000*in_samples_pos/cm_config.sample_rate;// 44100;
            tmp_frame->pts*=ccontext->time_base.den;
            tmp_frame->pts/=10000000;

            codec_portion_pts=(_time::time_val_i) 10000000*1024/cm_config.sample_rate;//44100;
            codec_portion_pts*=ccontext->time_base.den;
            codec_portion_pts/=10000000;

            //tmp_frame->pts = ((ms->stream_stamp + 10000000*in_samples_pos/44100 ) * ccontext->time_base.den) / 10000000;
            //cout << "update pts=" << tmp_frame->pts << endl;
        }

        av_samples_copy(tmp_frame->extended_data, in_frame->extended_data,
                        tmp_frame_samples_have,in_samples_pos,
                        cp_samples,
                        tmp_frame->channels,
                        (AVSampleFormat)tmp_frame->format);

        tmp_frame_samples_have+=cp_samples;

        if (tmp_frame_samples_have!=tmp_frame->nb_samples) break;

        //file.write((const char *)tmp_frame->extended_data[0],samples_nb_size);

        in_samples_pos+=cp_samples;

        // encode the image
        int encode_done;
        int ret = avcodec_encode_audio2(ccontext, &packet, tmp_frame, &encode_done);
        if (ret < 0) throw _pm_error(ret,"_aenc::proccess_frame - Can not encode frame.");

        if (encode_done)
        {
            cm_out_ms.buffer=(unsigned char *)&packet;
            cm_out_ms.stream_stamp=(tmp_frame->pts)*10000000/(ccontext->time_base.den);
            cm_out_ms.can_break=(packet.flags & AV_PKT_FLAG_KEY);

            cm_out_pin->push_media_sample(&cm_out_ms);

            av_free_packet(&packet);

            frame_number++;

            tmp_frame_samples_have=0;
        }

        if (in_samples_pos==in_frame->nb_samples) break;
    }
}

// -----------------------------------------------------------------------------
_audio_encoder *create_audio_encoder(int index)
{
    return new _aenc(index);
}

// ======================================================================================
//                                           DECODER
// ======================================================================================
class _adec;

// ----------------------------------- AVENCODER IN PIN ---------------------------------
class _adec_in_pin:public _pin
{
	protected:
		_adec *cm_dec;

		virtual ~_adec_in_pin();

	public:
		_adec_in_pin(_adec *dec);

		virtual void start();
		virtual void stop();
		virtual void pause();

		virtual void push_media_sample(const _media_sample *ms);
		virtual void push_stream_header(const _stream_header *sh);

};

// ----------------------------------- AVENCODER OUT PIN ---------------------------------
class _adec_out_pin:public _pin
{
	protected:
		_adec *cm_dec;

		virtual ~_adec_out_pin();
	public:

		_adec_out_pin(_adec *dec);
};

// ----------------------------------- AVENCODER ---------------------------------
class _adec:public _audio_decoder
{
	protected:

		AVCodec *codec;
		AVCodecContext *ccontext;

		AVFrame *frame;
		int frame_number;

		_media_sample cm_out_ms;

		bool is_open;

		_adec_in_pin *cm_in_pin;
		_pins_ptr_list cm_in_pins;

		_adec_out_pin *cm_out_pin;
		_pins_ptr_list cm_out_pins;

		virtual void open_decoder();
		virtual void close_decoder();

	public:

        //ofstream file;

		_adec(int index);
        ~_adec();

        virtual void release() { delete this; };

        virtual void config(int ch_num,int sample_rate);

		virtual void proccess_media_sample(const _media_sample *ms);
		virtual void proccess_stream_header(const _stream_header *sh);

		virtual const _pins_ptr_list &out_pins() const { return cm_out_pins; };
		virtual const _pins_ptr_list &in_pins() const { return cm_in_pins; };
};

// -----------------------------------------------------------------------------
// PINS
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// IN
// -----------------------------------------------------------------------------
_adec_in_pin::_adec_in_pin(_adec *dec)
 :cm_dec(dec)
{
	cm_is_input=true;
}

// -----------------------------------------------------------------------------
_adec_in_pin::~_adec_in_pin()
{
}

// -----------------------------------------------------------------------------
void _adec_in_pin::start()
{
	(*cm_dec->out_pins().begin())->start();
	cm_state=started;
}

// -----------------------------------------------------------------------------
void _adec_in_pin::stop()
{
	(*cm_dec->out_pins().begin())->stop();
	cm_state=stoped;
}

// -----------------------------------------------------------------------------
void _adec_in_pin::pause()
{
	(*cm_dec->out_pins().begin())->pause();
	cm_state=paused;
}

// -----------------------------------------------------------------------------
void _adec_in_pin::push_media_sample(const _media_sample* ms)
{
	cm_dec->proccess_media_sample(ms);
}

// -----------------------------------------------------------------------------
void _adec_in_pin::push_stream_header(const _stream_header* sh)
{
    cm_dec->proccess_stream_header(sh);
}

// -----------------------------------------------------------------------------
// OUT
// -----------------------------------------------------------------------------
_adec_out_pin::_adec_out_pin(_adec *dec)
 :cm_dec(dec)
{
	cm_is_input=false;
}

_adec_out_pin::~_adec_out_pin()
{
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
_adec::_adec(int index):codec(NULL),ccontext(NULL),frame(NULL),
						frame_number(0),is_open(false),cm_in_pin(NULL),cm_out_pin(NULL)
{
	cm_out_ms.cm_btype=MEDIA_SAMPLE_BT_AV_FRAME;
	cm_out_ms.cm_mtype=MEDIA_SAMPLE_MT_AUDIO;

	avcodec_register_all();

	// find decoder
	codec = avcodec_find_decoder((AVCodecID)index);//AV_CODEC_ID_G723_1);//AV_CODEC_ID_H264);//codec_id);
	if (!codec) throw _pm_error(index,"_adec::_adec - Can not find codec.");

	// create context
	ccontext = avcodec_alloc_context3(codec);
	if (!ccontext) throw _pm_error(index,"_adec::_adec - Can not create codec context.");

	//file.open("g:\\prj\\cpp\\common\\ffmpeg\\so\\bin\\test_to_dec.bin",ios::out | ios::binary);
}

// -----------------------------------------------------------------------------
_adec::~_adec()
{
    //file.close();

	if (cm_in_pin)
    {
		cm_in_pin->release();
	}
	cm_in_pins.clear();

	if (cm_out_pin)
	{
		// see comments in _venc::~_venc - cm_out_pin->end_of_stream();
		cm_out_pin->release();
	}
	cm_out_pins.clear();

	if (frame)
	{
		// crush on it av_freep(&frame->data[0]);
		//cout << "dec - frame release - frame" << endl;
		avcodec_free_frame(&frame);
	}

	close_decoder();

	av_free(ccontext);
}

// -----------------------------------------------------------------------------
void _adec::config(int ch_num,int sample_rate)
{
	if (frame)
	{
		av_freep(&frame->data[0]);
		avcodec_free_frame(&frame);
		frame=NULL;
	}

	if (cm_in_pin)
	{
		cm_in_pins.clear();
		cm_in_pin->release();
	}

	if (cm_out_pin)
	{
		cm_out_pin->end_of_stream();
		cm_out_pins.clear();
		cm_in_pin->release();
	}

    close_decoder();

	frame = av_frame_alloc();
	if (!frame) throw _pm_error(0,"_adec::_adec - Can not alloc frame.");

	ccontext->channels=ch_num;
	ccontext->sample_rate=sample_rate;

	open_decoder();

	frame->channels=ch_num;

	// create pins
	cm_in_pin=new _adec_in_pin(this);
	cm_in_pins.push_back(cm_in_pin);

	cm_out_pin=new _adec_out_pin(this);
	cm_out_pins.push_back(cm_out_pin);
}

// -----------------------------------------------------------------------------
void _adec::open_decoder()
{
	if (is_open) throw _pm_error(0,"_adec::open - codec is already open.");

	frame_number=0;

	// open it
	if (avcodec_open2(ccontext, codec, NULL) < 0) throw _pm_error(0,"_adec::_adec - Can not open codec.");

	//cout << "_adec::open_decoder ccontext->frame_size=" << ccontext->frame_size << endl;

	is_open=true;
}

// -----------------------------------------------------------------------------
void _adec::close_decoder()
{
	if (!is_open) return;

	avcodec_close(ccontext);
	is_open=false;
}

// -----------------------------------------------------------------------------
void _adec::proccess_stream_header(const _stream_header *sh)
{
    cm_out_pin->push_stream_header(sh);
}

// -----------------------------------------------------------------------------
void _adec::proccess_media_sample(const _media_sample* pms)
{
    #ifdef DEBUG
    MLOG(10) << "_vdec::proccess_media_sample" << endl;
    #endif // DEBUG

    if (pms->cm_mtype!=MEDIA_SAMPLE_MT_AUDIO) return;

    AVPacket *in_packet;

    if (pms->cm_btype==MEDIA_SAMPLE_BT_AV_PACKET)
    {
        in_packet=(AVPacket *)pms->buffer;
    }
    else throw _pm_error(0,string("_adec::proccess_media_sample - can not process media sample with type=")+to_string(pms->cm_btype));

	// decode
	int len, got_frame;
	len = avcodec_decode_audio4(ccontext, frame, &got_frame, in_packet);
	if (len < 0) throw _pm_error(len,"_adec::proccess_media_sample - error while decoding frame");

    //cout << "_adec::proccess_media_sample - decode done len=" << len << endl;

	//file.write((const char *)frame->extended_data[0],in_packet->size);

    if (got_frame)
	{
        //file.write((char *)frame->data[0],frame->nb_samples*2);
        //file.flush();

        //cout << av_get_sample_fmt_name((AVSampleFormat)frame->format) << endl;

		frame->pts = (pms->stream_stamp*ccontext->time_base.den)/10000000;

        cm_out_ms.buffer=(unsigned char *)frame;
        cm_out_ms.stream_stamp=pms->stream_stamp;
		cm_out_ms.can_break=frame->key_frame;

        //cout << ":" << flush;
		cm_out_pin->push_media_sample(&cm_out_ms);

		frame_number++;

		av_frame_unref(frame);
	}

    /*
    if (got_frame)
	{
		_media_sample *oms=new _media_sample;

        oms->cm_mtype=MEDIA_SAMPLE_MT_AUDIO;
        oms->cm_btype=MEDIA_SAMPLE_BT_RAW;

        oms->copy_to_buffer(frame->data[0],frame->nb_samples*2);  // * sample_size in bytes
        //f.write((char *)frame->data[0],frame->nb_samples*2);

		oms->stream_stamp=pms->stream_stamp;
		oms->can_break=frame->key_frame;

        //cout << " decoder push sample" << endl;
        cm_out_pin->push_media_sample(oms);

		delete oms;

		frame_number++;
	}
    */
}

// -----------------------------------------------------------------------------
_audio_decoder *create_audio_decoder(int index)
{
    return new _adec(index);
}

