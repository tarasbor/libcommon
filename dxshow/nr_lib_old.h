#ifndef __NET_RENDER_LIB_H__
#define __NET_RENDER_LIB_H__

#include <dshow.h>
#include <time.hpp>

#ifndef DLL_MODE_COMPILE
        #define EXPORT(A)       extern "C" __declspec(dllimport) A __stdcall 
#else
        #define EXPORT(A)       extern "C" __declspec(dllexport) A __stdcall 
#endif

// -------------------------------------------------------
class _media_sample
{
public:

        virtual void __stdcall set_buffer(const unsigned char *d, unsigned int s) = 0;
        virtual void __stdcall set_can_break(unsigned char t) = 0;

        virtual unsigned int __stdcall buffer_size() const = 0;
        virtual const unsigned char * __stdcall buffer() const = 0;
        virtual unsigned char __stdcall can_break() const = 0;

        virtual void __stdcall release() = 0;

        virtual int __stdcall pack_size() const = 0;
        virtual void __stdcall pack(unsigned char *d) const = 0;
        virtual int __stdcall depack(const unsigned char *d, int d_size) = 0;

        virtual _time::time_val_i __stdcall fill_stamp() const = 0;
        virtual void __stdcall set_fill_stamp(_time::time_val_i val) = 0;
};

EXPORT(_media_sample *) create_media_sample();

// -------------------------------------------------------
class _some_format_header
{
public:

        virtual void __stdcall release() = 0;

        virtual int __stdcall pack_size() const = 0;
        virtual void __stdcall pack(unsigned char *d) const = 0;
        virtual int __stdcall depack(const unsigned char *d, int d_size) = 0;

};

// -------------------------------------------------------
class _wave_header:public _some_format_header
{
public:

        virtual unsigned int __stdcall format_tag() const = 0;
        virtual unsigned int __stdcall channels_count() const = 0;
        virtual unsigned int __stdcall samples_per_sec() const = 0;
        virtual unsigned int __stdcall avg_bytes_per_sec() const = 0;
        virtual unsigned int __stdcall block_align() const = 0;
        virtual unsigned int __stdcall bits_per_sample() const = 0;

        virtual void __stdcall set_format_tag(unsigned int t) = 0;
        virtual void __stdcall set_channels_count(unsigned int t) = 0;
        virtual void __stdcall set_samples_per_sec(unsigned int t) = 0;
        virtual void __stdcall set_avg_bytes_per_sec(unsigned int t) = 0;
        virtual void __stdcall set_block_align(unsigned int t) = 0;
        virtual void __stdcall set_bits_per_sample(unsigned int t) = 0;

        virtual void __stdcall release() = 0;

        virtual int __stdcall pack_size() const = 0;
        virtual void __stdcall pack(unsigned char *d) const = 0;
        virtual int __stdcall depack(const unsigned char *d, int d_size) = 0;
};

EXPORT(_some_format_header *) create_wave_format_header();

// -------------------------------------------------------
class _video_header:public _some_format_header
{
public:

        virtual const RECT * __stdcall source_rect() const = 0;
        virtual const RECT * __stdcall target_rect() const = 0;
        virtual unsigned int __stdcall bit_rate() const = 0;
        virtual unsigned int __stdcall error_bit_rate() const = 0;
        virtual REFERENCE_TIME __stdcall avg_time_per_frame() const = 0;
        virtual const BITMAPINFOHEADER * __stdcall image_header() const = 0;

        virtual void __stdcall set_source_rect(const RECT *t) = 0;
        virtual void __stdcall set_target_rect(const RECT *t) = 0;
        virtual void __stdcall set_bit_rate(unsigned int t) = 0;
        virtual void __stdcall set_error_bit_rate(unsigned int t) = 0;
        virtual void __stdcall set_avg_time_per_frame(REFERENCE_TIME t) = 0;
        virtual void __stdcall set_image_header(const BITMAPINFOHEADER *t) = 0;

        virtual void __stdcall release() = 0;

        virtual int __stdcall pack_size() const = 0;
        virtual void __stdcall pack(unsigned char *d) const = 0;
        virtual int __stdcall depack(const unsigned char *d, int d_size) = 0;
};

EXPORT(_some_format_header *) create_video_format_header();

// -------------------------------------------------------
class _media_header
{
public:

        virtual void __stdcall set_major_type(const GUID *t) = 0;
        virtual void __stdcall set_sub_type(const GUID *t) = 0;
        virtual void __stdcall set_samples_are_fixed_size(unsigned char t) = 0;
        virtual void __stdcall set_compression_use_interfames(unsigned char t) = 0;
        virtual void __stdcall set_format(const GUID *t,const _some_format_header *fh) = 0;

        virtual const GUID * __stdcall major_type() const = 0;
        virtual const GUID * __stdcall sub_type() const = 0;
        virtual unsigned char __stdcall samples_are_fixed_size() const = 0;
        virtual unsigned char __stdcall compression_use_interfames() const = 0;
        virtual const GUID * __stdcall format_type() const = 0;
        virtual const _some_format_header * __stdcall format() const = 0;
        
        virtual void __stdcall release() = 0;

        virtual int __stdcall pack_size() const = 0;
        virtual void __stdcall pack(unsigned char *d) const = 0;
        virtual int __stdcall depack(const unsigned char *d, int d_size) = 0;
};

EXPORT(_media_header *) create_media_header();

// -------------------------------------------------------
class _net_render_filter
{
public:

        virtual void __stdcall connect_to(IPin *from) = 0;
        virtual void __stdcall set_graph(IGraphBuilder *g) = 0;
        virtual IBaseFilter * __stdcall filter_iface() = 0;

        virtual _media_header * __stdcall media_header() = 0;
        virtual _media_sample * __stdcall media_sample() = 0;

        virtual void __stdcall release() = 0;
};

EXPORT(_net_render_filter *) create_net_render_filter();

// -------------------------------------------------------
class _gl_render_filter
{
public:

        //virtual void __stdcall connect_to(IPin *from) = 0;
        //virtual void __stdcall set_graph(IGraphBuilder *g) = 0;
        virtual IBaseFilter * __stdcall filter_iface() = 0;

        virtual void __stdcall set_wnd(HWND wnd,RECT *r,int rot_angle) = 0;

        //virtual _media_header * __stdcall media_header() = 0;
        //virtual _media_sample * __stdcall media_sample() = 0;

        virtual void __stdcall release() = 0;
};

EXPORT(_gl_render_filter *) create_gl_render_filter();

// -------------------------------------------------------
class _net_source_filter
{
public:

        /*
        virtual void __stdcall connect_to(IPin *from) = 0;
        virtual void __stdcall set_graph(IGraphBuilder *g) = 0;
        */
        virtual IBaseFilter * __stdcall filter_iface() = 0;

        virtual void __stdcall set_media_header(_media_header *mh) = 0;
        virtual void __stdcall set_media_sample(_media_sample *ms) = 0;

        virtual void __stdcall release() = 0;

        virtual IPin * __stdcall get_pin(int n) = 0;

        virtual void __stdcall set_sample_time(int st) = 0;

        virtual void __stdcall clear_media_samples() = 0;
        virtual int __stdcall media_samles_buffer_count() = 0;
};

EXPORT(_net_source_filter *) create_net_source_filter();


#endif
