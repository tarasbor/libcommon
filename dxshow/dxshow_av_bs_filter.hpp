#ifndef __DXSHOW_AV_BS_FILTER__
#define __DXSHOW_AV_BS_FILTER__

#include "dxshow.hpp"

class _av_bs_filter:public _media_filter
{
    protected:
        virtual ~_av_bs_filter() {};

    public:
        _av_bs_filter() {};

        virtual void release() = 0;
};

_av_bs_filter *create_av_bs_filter(const char *filter_name);

#endif // __DXSHOW_AV_OUT__
