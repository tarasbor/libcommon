#include "dxshow_net.hpp"
#include "dxshow_av_func.hpp"
#include <ghelp.hpp>
#include <ghelp_gl.hpp>

extern "C" {
#include <libavcodec/avcodec.h>
}

#include <log.hpp>

#define MLOG(L) _log() << _log::_set_module_level("vcall",(L))

class _buffer_render_imp;

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
class _buffer_in_pin:public _pin
{
	protected:
		_buffer_render_imp *cm_render;

		virtual ~_buffer_in_pin() {};

	public:
		_buffer_in_pin(_buffer_render_imp *r):cm_render(r) { cm_is_input=true; };

		virtual void start() {};
		virtual void stop();
		virtual void pause() {};

		virtual void push_media_sample(const _media_sample *ms);
		virtual void push_stream_header(const _stream_header *sh);
};

// --------------------------------------------------------------
// --------------------------------------------------------------
class _buffer_render_imp:public _buffer_render
{
    friend class _buffer_in_pin;

	protected:
		_pins_ptr_list cm_in_pins;
		_buffer_in_pin *cm_in_pin;

		_stream_header *cm_sh;

		typedef list<_media_sample *> _media_samples_ptr;
		_media_samples_ptr cm_media_samples_ptr;

		_locker sink;

	public:

		_buffer_render_imp();
		virtual ~_buffer_render_imp();

		virtual void release() { delete this; };

		virtual const _pins_ptr_list &in_pins() const { return cm_in_pins; };

		virtual const _stream_header *stream_header() const { return cm_sh; };
		virtual _media_sample *pop_media_sample();
};

// --------------------------------------------------------------
// --------------------------------------------------------------

// --------------------------------------------------------------
_buffer_render_imp::_buffer_render_imp():cm_in_pin(NULL),cm_sh(NULL)
{
	cm_in_pin=new _buffer_in_pin(this);
	cm_in_pins.push_back(cm_in_pin);
}

// --------------------------------------------------------------
_buffer_render_imp::~_buffer_render_imp()
{
    //MLOG(0) << "_buffer_render_imp::~_buffer_render_imp" << endl;
    sink.lock();
	cm_in_pins.clear();
	if (cm_in_pin) cm_in_pin->release();

	//MLOG(0) << "_buffer_render_imp::~_buffer_render_imp - pins deleted" << endl;

	if (cm_sh) delete cm_sh;

	//MLOG(0) << "_buffer_render_imp::~_buffer_render_imp - header deleted" << endl;

    _media_samples_ptr::iterator it=cm_media_samples_ptr.begin();
	while(it!=cm_media_samples_ptr.end())
    {
        delete (*it);
        (*it)=NULL;
        it++;
    }
    cm_media_samples_ptr.clear();

    //MLOG(0) << "_buffer_render_imp::~_buffer_render_imp - samples deleted" << endl;

    sink.unlock();
}

// --------------------------------------------------------------
_media_sample *_buffer_render_imp::pop_media_sample()
{
    if (cm_media_samples_ptr.empty()) return NULL;

    sink.lock();
    _media_sample *ms=(*cm_media_samples_ptr.begin());
    cm_media_samples_ptr.pop_front();
    sink.unlock();

    return ms;
}

// --------------------------------------------------------------
// PIN
// --------------------------------------------------------------

// --------------------------------------------------------------
void _buffer_in_pin::stop()
{
    cm_render->sink.lock();
    _buffer_render_imp::_media_samples_ptr::iterator it=cm_render->cm_media_samples_ptr.begin();
	while(it!=cm_render->cm_media_samples_ptr.end())
    {
        delete (*it);
        (*it)=NULL;
        it++;
    }
    cm_render->cm_media_samples_ptr.clear();
    cm_render->sink.unlock();
}

// --------------------------------------------------------------
void _buffer_in_pin::push_media_sample(const _media_sample *ms)
{
    _media_sample *m=new _media_sample(*ms);

	cm_render->sink.lock();
	cm_render->cm_media_samples_ptr.push_back(m);
	cm_render->sink.unlock();
}

// --------------------------------------------------------------
void _buffer_in_pin::push_stream_header(const _stream_header *sh)
{
    cm_render->sink.lock();
    if (cm_render->cm_sh)
    {
        delete cm_render->cm_sh;
        cm_render->cm_sh=NULL;
    }

	cm_render->cm_sh = new _stream_header;
	_portable_header *fh=new _portable_header();

	if      (sh->format_header->format_type==MEDIA_FORMAT_VIDEO)
	{
        cm_render->sink.unlock();
        throw _pm_error(0,"_buffer_in_pin::push_stream_header - can not work with MEDIA_FORMAT_VIDEO - not release yet");
	}
	else if (sh->format_header->format_type==MEDIA_FORMAT_AUDIO)
	{
        cm_render->sink.unlock();
        throw _pm_error(0,"_buffer_in_pin::push_stream_header - can not work with MEDIA_FORMAT_AUDIO - not release yet");
	}
	else if (sh->format_header->format_type==MEDIA_FORMAT_AV_FORMAT_CONTEXT)
	{
        cm_render->sink.unlock();
        throw _pm_error(0,"_buffer_in_pin::push_stream_header - can not work with MEDIA_AV_FORMAT_CONTEXT - not release yet");
	}
	else if (sh->format_header->format_type==MEDIA_FORMAT_AV_CODEC_CONTEXT)
	{
        _av_codec_context_header *ch=(_av_codec_context_header *)sh->format_header;
        AVCodecContext *cc=(AVCodecContext *)ch->context;

        fh->text="<header><streams>";

        fh->text+="<stream index=\"0\" source_proto=\"encoder\" ";

        if      (cc->codec_type==AVMEDIA_TYPE_VIDEO)
        {
            fh->text+="media_type=\"video\" ";
            fh->text+="w=\"";
            fh->text+=to_string(cc->width);
            fh->text+="\" ";

            fh->text+="h=\"";
            fh->text+=to_string(cc->height);
            fh->text+="\" ";

            fh->text+="gop_size=\"";
            fh->text+=to_string(cc->gop_size);
            fh->text+="\" ";

            fh->text+="pix_fmt=\"";
            fh->text+=to_string(cc->pix_fmt);
            fh->text+="\" ";
        }
        else if (cc->codec_type==AVMEDIA_TYPE_AUDIO)
        {
            fh->text+="media_type=\"audio\" ";

            fh->text+="ch_count=\"";
            fh->text+=to_string(cc->channels);
            fh->text+="\" ";

            fh->text+="sample_rate=\"";
            fh->text+=to_string(cc->sample_rate);
            fh->text+="\" ";

            fh->text+="sample_format=\"";
            fh->text+=get_str_name_by_pcm_format(get_pcm_format_by_av_format(cc->sample_fmt));
            fh->text+="\" ";

            fh->text+="frame_size=\"";
            fh->text+=to_string(cc->frame_size);
            fh->text+="\" ";
        }
        else
        {
            cm_render->sink.unlock();
            throw _pm_error(cc->codec_type,"_buffer_in_pin::push_stream_header - MEDIA_FORMAT_AV_CODEC_CONTEXT - can not work with unknown codec_type");
        }

        fh->text+="codec_id=\"";
        fh->text+=to_string(cc->codec_id);
        fh->text+="\" ";

        fh->text+="bitrate=\"";
        fh->text+=to_string(cc->bit_rate);
        fh->text+="\" ";

        fh->text+="time_base_num=\"";
        fh->text+=to_string(cc->time_base.num);
        fh->text+="\" ";

        fh->text+="time_base_den=\"";
        fh->text+=to_string(cc->time_base.den);
        fh->text+="\" ";

        fh->text+="/>";

        fh->text+="</streams></header>";
	}
	else if (sh->format_header->format_type==MEDIA_FORMAT_PORTABLE)
	{
        fh->text=((_portable_header *)sh->format_header)->text;
	}

    cm_render->cm_sh->set_format_header(fh);

	cm_render->sink.unlock();

	cout << "_buffer_in_pin::push_stream_header - " << fh->text << endl;
}

// --------------------------------------------------------------
// --------------------------------------------------------------
_buffer_render *create_buffer_render()
{
    return new _buffer_render_imp();
}

// ---------------------------------------------------------
// ---------------------------------------------------------

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
class _buffer_out_pin:public _pin
{
	protected:
		virtual ~_buffer_out_pin() {};

	public:
		_buffer_out_pin() {};

		virtual void start() { _pin::start(); };
		virtual void stop() { _pin::stop(); };
		virtual void pause() { _pin::pause(); };

		virtual void push_media_sample(const _media_sample *ms) { _pin::push_media_sample(ms); };
		virtual void push_stream_header(const _stream_header *sh) { _pin::push_stream_header(sh); };
};


// ---------------------------------------------------------
// ---------------------------------------------------------
class _buffer_source_imp:public _buffer_source
{
    protected:

        _pins_ptr_list cm_out_pins;
        _pin *cm_out_pin;

    public:

        _buffer_source_imp();
        virtual ~_buffer_source_imp();

        virtual void release() { delete this; };

        virtual const _pins_ptr_list &out_pins() const { return cm_out_pins; };

        virtual void push_stream_header(const _stream_header *sh);
        virtual void push_media_sample(const _media_sample *ms);

        virtual void start() { cm_out_pin->start(); };
};

// ---------------------------------------------------------
_buffer_source_imp::_buffer_source_imp()
{
    cm_out_pin=new _buffer_out_pin();
	cm_out_pins.push_back(cm_out_pin);
}

// ---------------------------------------------------------
_buffer_source_imp::~_buffer_source_imp()
{
    cm_out_pins.clear();
    cm_out_pin->release();
}


// ---------------------------------------------------------
void _buffer_source_imp::push_stream_header(const _stream_header *sh)
{
    cm_out_pin->push_stream_header(sh);
}

// ---------------------------------------------------------
void _buffer_source_imp::push_media_sample(const _media_sample *ms)
{
    cm_out_pin->push_media_sample(ms);
}

// ---------------------------------------------------------
// ---------------------------------------------------------
_buffer_source *create_buffer_source()
{
    return new _buffer_source_imp;
}
