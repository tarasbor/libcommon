#ifndef __DXSHOW_AV_FUNC__
#define __DXSHOW_AV_FUNC__

#include "dxshow.hpp"

extern "C" {
#include <libavcodec/avcodec.h>
}

// get FFMPEG sample format eq to given AUDIO_PCM_FORMAT
AVSampleFormat      get_av_format_by_pcm_format(AUDIO_PCM_FORMAT pcm_format);

// get AUDIO_PCM_FORMAT sample format eq to given FFMPEG sample format
AUDIO_PCM_FORMAT    get_pcm_format_by_av_format(AVSampleFormat av_format);

// get sample size (in bytes) for given AUDIO_PCM_FORMAT
int                 get_sample_size_by_pcm_format(AUDIO_PCM_FORMAT pcm_format);

// get sample size (in bits) for given AUDIO_PCM_FORMAT
int                 get_bits_sample_size_by_pcm_format(AUDIO_PCM_FORMAT pcm_format);

// get string name for given AUDIO_PCM_FORMAT
const char         *get_str_name_by_pcm_format(AUDIO_PCM_FORMAT pcm_format);

// get AUDIO_PCM_FORMAT for given string name
AUDIO_PCM_FORMAT    get_pcm_format_by_str_name(const char *name);

#endif
