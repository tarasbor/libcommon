#include "dxshow.hpp"

#include <errno.h>
#include <string.h>
#include <stdio.h>

#include <iostream>

#include <clthread.hpp>
#include <text_help.hpp>
#include <dx_headers.hpp>
#include <dxshow_av_func.hpp>
#include <net_http_client.h>

#include <log.hpp>

#define MLOG(A) _log() << _log::_set_module_level("av",(A))
#define DEBUG   10
#define DEBUG2  15

extern "C" {
#include <libavformat/avformat.h>
#include <libavdevice/avdevice.h>
}

class _av_cap;
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
class _avc_read_thread:public _thread
{
	protected:
		_av_cap *vc;

		_time cm_first_error_time;

	public:
		_avc_read_thread(_av_cap *v);
		virtual ~_avc_read_thread();

		virtual int Poll();
};

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
class _avc_push_thread:public _thread
{
	protected:
		_av_cap *vc;

	public:
		_avc_push_thread(_av_cap *v);
		virtual ~_avc_push_thread();

		virtual int Poll();
};

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
class _av_cap_out_pin:public _pin
{
	protected:
		_av_cap *cm_cap;

		virtual ~_av_cap_out_pin();

	public:
		_av_cap_out_pin(_av_cap *vc);

		virtual void start();
		virtual void pause();
		virtual void stop();

		virtual void push_media_sample(const _media_sample *ms);
		virtual void push_stream_header(const _stream_header *sh);

		virtual _state state() const;
};

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
class _av_cap:public _media_source
{
	friend class _avc_read_thread;
	friend class _avc_push_thread;
	friend class _av_cap_out_pin;

	private:

        string cm_format_str;
		string cm_uri;
		string cm_options;

		AVFormatContext *cm_fmt_ctx;         // format context
        AVDictionary *cm_av_opts;            // av options

		_avc_read_thread *avc_read_thread;
		_avc_push_thread *avc_push_thread;

		virtual void start_capture();
		virtual void pause_capture();
		virtual void stop_capture();

		virtual void sink_http_hls();

		virtual int on_poll_read();
		virtual int on_poll_push();
		virtual void push_packet(AVPacket *av_pkt);

		_pins_ptr_list cm_out_pins;
		_pin *cm_out_pin;

		_media_sample cm_media_sample;

		int64_t cm_first_packet_pts;

		static _time::time_val_i gl_first_packet_stamp;
		_time::time_val_i cm_me_and_gl_first_diff;

		bool cm_first_packet;

		int frame_cnt;

		typedef list<AVPacket *> _buffer_list;
		_buffer_list buffer_list;
		int max_buffer_list_size;
		int max_buffer_list_time;

		_time::time_val_i cm_first_push_pts;
		_time cm_first_push_time;

		bool cm_drop_audio;
		bool cm_fix_fps;

	public:
		_av_cap(const string &format,const string &uri,const string &opttions);
		virtual ~_av_cap();

		virtual void release() { delete this; };

		virtual const _pins_ptr_list &out_pins() const { return cm_out_pins; };

		virtual bool is_thread_ok() const;
};

_time::time_val_i _av_cap::gl_first_packet_stamp = 0;

// -----------------------------------------------------------------------------
_av_cap::_av_cap(const string &format,const string &uri,const string &options)
:avc_read_thread(NULL),avc_push_thread(NULL),
cm_out_pin(NULL),frame_cnt(0),cm_uri(uri),cm_options(options),cm_format_str(format),
cm_fmt_ctx(NULL),cm_av_opts(NULL),cm_first_packet_pts(0),max_buffer_list_size(1),max_buffer_list_time(0),
cm_first_push_pts(0),cm_drop_audio(false),cm_fix_fps(false)
{
    cm_media_sample.cm_btype=MEDIA_SAMPLE_BT_AV_PACKET;

    av_register_all();

    // TODO - check format & init needed subsys
    if (cm_format_str=="dshow" || cm_format_str=="v4l2")
    {
        avdevice_register_all();
    }

    // TODO - analize protocols
    avformat_network_init();

	int err;

	cm_out_pin=new _av_cap_out_pin(this);
	cm_out_pins.push_back(cm_out_pin);
}

// -----------------------------------------------------------------------------
_av_cap::~_av_cap()
{
    stop_capture();

    if (cm_out_pin) cm_out_pin->end_of_stream();

    cm_out_pins.clear();
	if (cm_out_pin) cm_out_pin->release();

    avformat_network_deinit();
}

// -----------------------------------------------------------------------------
void _av_cap::start_capture()
{
	if (avc_read_thread) throw _pm_error(0,"_av_cap::start_capture - already started");

    // detect & cut our (not codec) options
    int i=0;
    string codec_options;

    bool use_buffer=false;

    while(1)
    {
        string pv=_text_help::get_field_from_st(cm_options,',',i);
        if (pv.empty()) break;

        string p=_text_help::get_field_from_st(pv,'=',0);
        string v=_text_help::get_field_from_st(pv,'=',1);

        if (p.empty() || v.empty()) break;

        if (p=="avc_packet_buffer")
        {
            max_buffer_list_size=atoi(v.c_str());
            MLOG(0) << "For source " << cm_uri << " use avc_packet_buffer=" << max_buffer_list_size << endl;
            use_buffer=true;
        }
        else if (p=="avc_time_buffer")
        {
            max_buffer_list_time=atoi(v.c_str());
            MLOG(0) << "For source " << cm_uri << " use avc_time_buffer=" << max_buffer_list_time << endl;
            use_buffer=true;
        }
        else if (p=="drop_audio")
        {
            cm_drop_audio=true;
        }
        else if (p=="fix_fps")
        {
            cm_fix_fps=true;
            use_buffer=true;
        }
        else
        {
            if (!codec_options.empty()) codec_options+=",";
            codec_options+=pv;
        }

        i++;
    }

	int err;

    cm_fmt_ctx = avformat_alloc_context();

    // TODO options parse
    av_dict_parse_string(&cm_av_opts,codec_options.c_str(),"=",",",0);//"rtsp_transport","tcp",0);

    AVInputFormat * a= NULL;
    if (!cm_format_str.empty())
    {
        a = av_find_input_format(cm_format_str.c_str());
        if (!a) throw _pm_error(err,string("_av_cap::start_capture - av_find_input_format - can not find given format ")+cm_format_str);
    }

    if (cm_uri.find("http://")!=std::string::npos)
    {
        sink_http_hls();
    }

    cout << "cm_uri=" << cm_uri << endl;
    /* open input file, and allocate format context */
    err=avformat_open_input(&cm_fmt_ctx, cm_uri.c_str(), a, &cm_av_opts);
    if (err < 0) throw _pm_error(err,"_av_cap::start_capture - avformat_open_input error");

    /* retrieve stream information */
    err=avformat_find_stream_info(cm_fmt_ctx, NULL);
    if (err < 0) throw _pm_error(err,"_av_cap::start_capture - avformat_find_stream_info error");

    av_dump_format(cm_fmt_ctx, 0, cm_uri.c_str(), 0);

    //cout << "CODEC ID = " << cm_fmt_ctx->streams[0]->codec->codec_id << endl;

    _stream_header *sh=new _stream_header;
    _portable_header *ph=new _portable_header;

    ph->text="<header><streams>";

    int video_index=-1;
    for(i=0;i<cm_fmt_ctx->nb_streams;i++)
    {
        if (cm_fmt_ctx->streams[i]->codec->codec_type==AVMEDIA_TYPE_VIDEO)
        {
            ph->text+="<stream index=\"";
            ph->text+=to_string(i);
            // TODO - detect right type
            ph->text+="\" media_type=\"video\" source_proto=\"rtsp\"";
            ph->text+=" codec=\"";
            ph->text+=to_string(cm_fmt_ctx->streams[i]->codec->codec_id);
            ph->text+="\"";

            ph->text+=" w=\"";
            ph->text+=to_string(cm_fmt_ctx->streams[i]->codec->width);
            ph->text+="\"";

            ph->text+=" h=\"";
            ph->text+=to_string(cm_fmt_ctx->streams[i]->codec->height);
            ph->text+="\"";

            ph->text+="/>";
            video_index=i;
        }
        else if (cm_fmt_ctx->streams[i]->codec->codec_type==AVMEDIA_TYPE_AUDIO && cm_drop_audio==false)
        {
            ph->text+="<stream index=\"";
            ph->text+=to_string(i);
            // TODO - detect right type
            ph->text+="\" media_type=\"audio\" source_proto=\"rtsp\"";
            ph->text+=" codec=\"";
            ph->text+=to_string(cm_fmt_ctx->streams[i]->codec->codec_id);
            ph->text+="\"";

            ph->text+=" bit_rate=\"";
            ph->text+=to_string(cm_fmt_ctx->streams[i]->codec->bit_rate);
            ph->text+="\"";

            ph->text+=" sample_rate=\"";
            ph->text+=to_string(cm_fmt_ctx->streams[i]->codec->sample_rate);
            ph->text+="\"";

            ph->text+=" ch_count=\""; //channels
            ph->text+=to_string(cm_fmt_ctx->streams[i]->codec->channels);
            ph->text+="\"";

            ph->text+=" sample_format=\"";
            ph->text+=get_str_name_by_pcm_format(get_pcm_format_by_av_format(cm_fmt_ctx->streams[i]->codec->sample_fmt));
            ph->text+="\"";

            ph->text+=" block_align=\"";
            ph->text+=to_string(cm_fmt_ctx->streams[0]->codec->block_align);
            ph->text+="\"";

            ph->text+=" channel_layout=\"";
            ph->text+=to_string(cm_fmt_ctx->streams[0]->codec->channel_layout);
            ph->text+="\"";

            ph->text+="/>";
        }
    }

    ph->text+="</streams></header>";

    MLOG(0) << "header = " << ph->text << endl;

	sh->set_format_header(ph);
	// --------------------------------------

	// --------------------------------------
	/*
	if (video_index>=0)
    {
        _av_codec_context_header *acch=new _av_codec_context_header;
        acch->context=cm_fmt_ctx->streams[video_index]->codec;

        _stream_header sh1;
        sh1.set_format_header(acch);

        cm_out_pin->push_stream_header(&sh1);
    }
    else
    {
        cm_out_pin->push_stream_header(sh);
    }
    */

    cm_out_pin->push_stream_header(sh);

    delete sh;
    // --------------------------------------

	cm_first_packet=true;
	frame_cnt=0;

	avc_read_thread=new _avc_read_thread(this);
	avc_read_thread->SetSleepTime(0);
	avc_read_thread->Start();

    if (use_buffer)
    {
        avc_push_thread=new _avc_push_thread(this);
        avc_push_thread->SetSleepTime(5);
        avc_push_thread->Start();
    }
    else
    {
        avc_push_thread=NULL;
    }
}

// -----------------------------------------------------------------------------
void _av_cap::pause_capture()
{
    stop_capture();
}

// -----------------------------------------------------------------------------
void _av_cap::stop_capture()
{
	if (avc_read_thread)
	{
		avc_read_thread->Kill();
		while(avc_read_thread->GetStatus()==TH_STATUS_LOOP) Sleep(10);
		delete avc_read_thread;
		avc_read_thread=NULL;
	}

	if (avc_push_thread)
	{
		avc_push_thread->Kill();
		while(avc_push_thread->GetStatus()==TH_STATUS_LOOP) Sleep(10);
		delete avc_push_thread;
		avc_push_thread=NULL;
	}

	if (cm_fmt_ctx)
    {
        avformat_close_input(&cm_fmt_ctx);
        avformat_free_context(cm_fmt_ctx);
        cm_fmt_ctx=NULL;
    }
}

// -----------------------------------------------------------------------------
int _av_cap::on_poll_read()
{
    _time tnow;
	tnow.set_now();

	AVPacket *av_pkt = new AVPacket;

	av_init_packet(av_pkt);
	av_pkt->data=NULL;
	av_pkt->size=0;

	int err=av_read_frame(cm_fmt_ctx,av_pkt);
	frame_cnt++;

	if (err<0)
    {
        MLOG(0) << "_av_cap::on_poll - av_read_frame return error " << err << endl;
        throw _pm_error(err,"_av_cap::on_poll - av_read_frame error");
    }

	if (cm_first_packet)
    {
        _time t;
        t.set_now();

        if (!gl_first_packet_stamp)
        {
            gl_first_packet_stamp=t.stamp();
            cm_me_and_gl_first_diff=0;
        }
        else
        {
            cm_me_and_gl_first_diff=t.stamp()-gl_first_packet_stamp;
        }

        cm_first_packet_pts=av_pkt->pts;
        cm_first_packet=false;
    }

    if (cm_drop_audio && cm_fmt_ctx->streams[av_pkt->stream_index]->codec->codec_type==AVMEDIA_TYPE_AUDIO) return 0;

    if (avc_push_thread)   // we use buffer & separated push thread
    {
        buffer_list.push_back(av_pkt);
    }
    else
    {
        push_packet(av_pkt);

        av_free_packet(av_pkt);
        delete av_pkt;
    }

    return 0;

    /*if (cm_fmt_ctx->streams[av_pkt->stream_index]->codec->codec_type==AVMEDIA_TYPE_VIDEO &&
        cm_uri.find("192.168.0.10")!=std::string::npos)
    {
        if (cm_last_packet_time)
        {
            _time tnow;
            tnow.set_now();

            _time::time_val_i t1=buffer_list.front()->pts*100000/(cm_fmt_ctx->streams[buffer_list.front()->stream_index]->time_base.den);

            MLOG(0) << "tdiff=" << tnow.diff_us(cm_last_send_packet_time) << " pts diff=" << t1-cm_last_packet_time << endl;

            if (tnow.diff_us(cm_last_send_packet_time)<t1-cm_last_packet_time) return 0;

            cm_last_packet_time=t1;
        }
        else
        {
            cm_last_packet_time=(av_pkt->pts)*100000/(cm_fmt_ctx->streams[av_pkt->stream_index]->time_base.den);  // in ms
            cm_last_send_packet_time.set_now();
        }
    }
    else if (max_buffer_list_time)   // if this param set - use it
    {
        _time::time_val_i t1=buffer_list.front()->pts;
        _time::time_val_i t2=buffer_list.back()->pts;
        _time::time_val_i diff=(t2-t1)*1000/(cm_fmt_ctx->streams[av_pkt->stream_index]->time_base.den); // in ms

        if (diff<max_buffer_list_time)
        {
            return 0;
        }
    }
    else                        // else use size param
    {
        if (buffer_list.size()<max_buffer_list_size)
        {
            return 0;
        }
    }

    av_pkt=buffer_list.front();
    buffer_list.pop_front();

    if (cm_uri.find("192.168.0.10")!=std::string::npos) MLOG(0) << "push_sample " << av_pkt->pts << endl;

    if (cm_fmt_ctx->streams[av_pkt->stream_index]->codec->codec_type==AVMEDIA_TYPE_VIDEO)
    {
        cm_media_sample.cm_mtype=MEDIA_SAMPLE_MT_VIDEO;
    }
    else if (cm_fmt_ctx->streams[av_pkt->stream_index]->codec->codec_type==AVMEDIA_TYPE_AUDIO)
    {
        cm_media_sample.cm_mtype=MEDIA_SAMPLE_MT_AUDIO;
    }
    else
    {
        cm_media_sample.cm_mtype=MEDIA_SAMPLE_MT_UNDEF;
    }

    //cm_media_sample.stream_stamp=(av_pkt.pts-cm_first_packet_pts)*10000000/(cm_fmt_ctx->streams[0]->time_base.den) + cm_me_and_gl_first_diff;
    cm_media_sample.stream_stamp=(av_pkt->pts-cm_fmt_ctx->streams[av_pkt->stream_index]->start_time)*10000000/(cm_fmt_ctx->streams[av_pkt->stream_index]->time_base.den) + cm_me_and_gl_first_diff;

    //cout << "(" << av_pkt.pts << "-" << cm_first_packet_pts << ")/" << cm_fmt_ctx->streams[0]->time_base.den << " + " << cm_me_and_gl_first_diff << " = " << cm_media_sample.stream_stamp << " " << cm_fmt_ctx->streams[0]->time_base.num << endl;

    cm_media_sample.buffer=(unsigned char *)av_pkt;
    cm_media_sample.can_break=(av_pkt->flags & AV_PKT_FLAG_KEY);

    cm_out_pin->push_media_sample(&cm_media_sample);

	av_free_packet(av_pkt);
	delete av_pkt;

	cm_last_send_packet_time.set_now();

	return 0;

	/*_time tnow;
	tnow.set_now();

	AVPacket av_pkt;

	av_init_packet(&av_pkt);
	av_pkt.data=NULL;
	av_pkt.size=0;

	int err=av_read_frame(cm_fmt_ctx,&av_pkt);
	frame_cnt++;

	if (err<0)
    {
        MLOG(0) << "_av_cap::on_poll - av_read_frame return error " << err << endl;
        throw _pm_error(err,"_av_cap::on_poll - av_read_frame error");
    }

	if (cm_first_packet)
    {
        _time t;
        t.set_now();

        if (!gl_first_packet_stamp)
        {
            gl_first_packet_stamp=t.stamp();
            cm_me_and_gl_first_diff=0;
        }
        else
        {
            cm_me_and_gl_first_diff=t.stamp()-gl_first_packet_stamp;
        }

        cm_first_packet_pts=av_pkt.pts;
        cm_first_packet=false;
    }

    if (cm_fmt_ctx->streams[av_pkt.stream_index]->codec->codec_type==AVMEDIA_TYPE_VIDEO)
    {
        cm_media_sample.cm_mtype=MEDIA_SAMPLE_MT_VIDEO;
    }
    else if (cm_fmt_ctx->streams[av_pkt.stream_index]->codec->codec_type==AVMEDIA_TYPE_AUDIO)
    {
        cm_media_sample.cm_mtype=MEDIA_SAMPLE_MT_AUDIO;
    }
    else
    {
        cm_media_sample.cm_mtype=MEDIA_SAMPLE_MT_UNDEF;
    }

    //cm_media_sample.stream_stamp=(av_pkt.pts-cm_first_packet_pts)*10000000/(cm_fmt_ctx->streams[0]->time_base.den) + cm_me_and_gl_first_diff;
    cm_media_sample.stream_stamp=(av_pkt.pts-cm_fmt_ctx->streams[av_pkt.stream_index]->start_time)*10000000/(cm_fmt_ctx->streams[av_pkt.stream_index]->time_base.den) + cm_me_and_gl_first_diff;

    //cout << "(" << av_pkt.pts << "-" << cm_first_packet_pts << ")/" << cm_fmt_ctx->streams[0]->time_base.den << " + " << cm_me_and_gl_first_diff << " = " << cm_media_sample.stream_stamp << " " << cm_fmt_ctx->streams[0]->time_base.num << endl;

    cm_media_sample.buffer=(unsigned char *)&av_pkt;
    cm_media_sample.can_break=(av_pkt.flags & AV_PKT_FLAG_KEY);

    cm_out_pin->push_media_sample(&cm_media_sample);

	av_free_packet(&av_pkt);

	return 0;
	*/
}

// -----------------------------------------------------------------------------
int _av_cap::on_poll_push()
{
	if (buffer_list.empty()) return 0;

	if (cm_fix_fps)
    {
        if (!cm_first_push_pts)
        {
            cm_first_push_pts=buffer_list.front()->pts;
            cm_first_push_time.set_now();
        }
        else
        {
            _time tn;
            tn.set_now();

            _time::time_val_i pts_diff=buffer_list.front()->pts - cm_first_push_pts;
            _time::time_val_i play_diff_ms=(pts_diff*1000)/(cm_fmt_ctx->streams[buffer_list.front()->stream_index]->time_base.den);
            int real_diff_ms = tn.diff_ms(cm_first_push_time);
            if (play_diff_ms>real_diff_ms) Sleep(play_diff_ms-real_diff_ms);
        }
    }
    else if (max_buffer_list_time)   // if this param set - use it
    {
        _time::time_val_i t1=buffer_list.front()->pts;
        _time::time_val_i t2=buffer_list.back()->pts;
        _time::time_val_i diff=(t2-t1)*1000/(cm_fmt_ctx->streams[buffer_list.front()->stream_index]->time_base.den); // in ms

        if (diff<max_buffer_list_time)
        {
            return 0;
        }
    }
    else                        // else use size param
    {
        if (buffer_list.size()<max_buffer_list_size)
        {
            return 0;
        }
    }

    AVPacket *av_pkt=buffer_list.front();
    buffer_list.pop_front();

    push_packet(av_pkt);

	av_free_packet(av_pkt);
	delete av_pkt;

	return 0;
}

// -----------------------------------------------------------------------------
void _av_cap::push_packet(AVPacket *av_pkt)
{
    if (cm_fmt_ctx->streams[av_pkt->stream_index]->codec->codec_type==AVMEDIA_TYPE_VIDEO)
    {
        cm_media_sample.cm_mtype=MEDIA_SAMPLE_MT_VIDEO;
    }
    else if (cm_fmt_ctx->streams[av_pkt->stream_index]->codec->codec_type==AVMEDIA_TYPE_AUDIO)
    {
        cm_media_sample.cm_mtype=MEDIA_SAMPLE_MT_AUDIO;
    }
    else
    {
        cm_media_sample.cm_mtype=MEDIA_SAMPLE_MT_UNDEF;
    }

    cm_media_sample.stream_stamp=(av_pkt->pts-cm_fmt_ctx->streams[av_pkt->stream_index]->start_time)*10000000/(cm_fmt_ctx->streams[av_pkt->stream_index]->time_base.den) + cm_me_and_gl_first_diff;

    cm_media_sample.buffer=(unsigned char *)av_pkt;
    cm_media_sample.can_break=(av_pkt->flags & AV_PKT_FLAG_KEY);

    cm_out_pin->push_media_sample(&cm_media_sample);
}

// -----------------------------------------------------------------------------
bool _av_cap::is_thread_ok() const
{
    if (!avc_read_thread) return false;
    if (avc_read_thread->GetStatus()==TH_STATUS_LOOP) return true;
    return false;
}

// -----------------------------------------------------------------------------
void _av_cap::sink_http_hls()
{
    _time t1;
    t1.set_now();

    _http_client *hc=create_http_client();
    _http_options hc_opts;

    _http_answer ha;
    string prev_answer;

    while(1)
    {
        hc->get(cm_uri,hc_opts,ha);
        if (!prev_answer.empty())
        {
            if (prev_answer!=ha.data) break;
        }
        Sleep(100);
        prev_answer=ha.data;
    }

    hc->release();

    _time t2;
    t2.set_now();

    MLOG(0) << "HTTP HLS Sink done in " << t2.diff_ms(t1) << " msecs." << endl;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
_avc_read_thread::_avc_read_thread(_av_cap* v):vc(v),cm_first_error_time(0)
{
}

// -----------------------------------------------------------------------------
_avc_read_thread::~_avc_read_thread()
{
}

// -----------------------------------------------------------------------------
int _avc_read_thread::Poll()
{
	try
	{
	    vc->on_poll_read();
	    cm_first_error_time.stamp(0);
	}
	catch(_pm_error &e)
	{
	    MLOG(0) << "_avc_read_thread::Poll - some error " << e.str() << endl;

	    if (cm_first_error_time.stamp())
        {
            _time tn;
            tn.set_now();
            if (tn.diff_s(cm_first_error_time)>5) return -1;
        }
        else
        {
            cm_first_error_time.set_now();
        }
	    //MLOG(0) << "_avc_thread::Poll - stop & crush thread " << e.str() << endl;
		//return -1;
	}
	return 0;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
_avc_push_thread::_avc_push_thread(_av_cap* v):vc(v)
{
}

// -----------------------------------------------------------------------------
_avc_push_thread::~_avc_push_thread()
{

}

// -----------------------------------------------------------------------------
int _avc_push_thread::Poll()
{
	try
	{
	    vc->on_poll_push();
	}
	catch(_pm_error &e)
	{
	    MLOG(0) << "_avc_push_thread::Poll - some error " << e.str() << endl;
	    return 0;

	    //MLOG(0) << "_avc_thread::Poll - stop & crush thread " << e.str() << endl;
		//return -1;
	}
	return 0;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
_av_cap_out_pin::_av_cap_out_pin(_av_cap *vc)
 :cm_cap(vc)
{
    /*
	// set out pin header
	_media_type *mt=new _media_type;
	mt->major_type=0;
	mt->minor_type=0;
	mt->samples_are_fixed_size=0;
	mt->use_interframes=0;

	_video_header *vh=new _video_header;

	vh->source_rect.x=vh->source_rect.y=0;
	vh->source_rect.w=w;
	vh->source_rect.h=h;

	memcpy(&vh->target_rect,&vh->source_rect,sizeof(_rect));
	memcpy(&vh->image_dim,&vh->source_rect,sizeof(_rect));

	vh->avg_nsec_per_frame=(1000*1000*1000)/frame_rate;

	vh->bit_rate=0;
	vh->error_bit_rate=0;

	vh->image_bpp=0;
	vh->image_pixel_format=V4L2_PIX_FMT_YUYV;

	mt->format=vh;

	cm_mtype=mt;
	*/
}

// -----------------------------------------------------------------------------
_av_cap_out_pin::~_av_cap_out_pin()
{
}

// -----------------------------------------------------------------------------
void _av_cap_out_pin::start()
{
	cm_cap->start_capture();

	_pin::start();
}

// -----------------------------------------------------------------------------
void _av_cap_out_pin::stop()
{
	cm_cap->stop_capture();

	_pin::stop();
}

// -----------------------------------------------------------------------------
void _av_cap_out_pin::pause()
{
	cm_cap->pause_capture();

	_pin::pause();
}


// -----------------------------------------------------------------------------
void _av_cap_out_pin::push_media_sample(const _media_sample *ms)
{
	_pin::push_media_sample(ms);
}

// -----------------------------------------------------------------------------
void _av_cap_out_pin::push_stream_header(const _stream_header *sh)
{
	_pin::push_stream_header(sh);
}

// -----------------------------------------------------------------------------
_pin::_state _av_cap_out_pin::state() const
{
    //MLOG(0) << "_av_cap_out_pin::state" << endl;
    if (cm_state!=_pin::started) return cm_state;

    // We need check for error state
    if (cm_cap->is_thread_ok()) return _pin::started;

    return _pin::error;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
_media_source *create_av_capture(const string &format,const std::string &uri,const std::string &options)
{
    return new _av_cap(format,uri,options);
}
