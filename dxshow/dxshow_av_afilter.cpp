#include "dxshow.hpp"
#include "dxshow_av_afilter.hpp"

//#ifdef DEBUG
#include <log.hpp>
#define MLOG(L) _log() << _log::_set_module_level("dxshow_av_afilter",(L))
//#endif

#define __STDC_CONSTANT_MACROS
#define __STDC_LIMIT_MACROS
#define __STDC_FORMAT_MACROS

extern "C" {
#include <libavutil/opt.h>
#include <libavcodec/avcodec.h>
#include <libavutil/imgutils.h>
#include <libavfilter/avfiltergraph.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>
}

#include <iostream>

using namespace std;


// ======================================================================================
//                                           ENCODER
// ======================================================================================
class _av_af;

// ----------------------------------- AVENCODER IN PIN ---------------------------------
class _av_af_in_pin:public _pin
{
	protected:
		_av_af *cm_af;

		virtual ~_av_af_in_pin();

	public:
		_av_af_in_pin(_av_af *af);

		virtual void start();
		virtual void stop();
		virtual void pause();

		virtual void push_media_sample(const _media_sample *ms);
		virtual void push_stream_header(const _stream_header *sh);
};

// ----------------------------------- AVENCODER OUT PIN ---------------------------------
class _av_af_out_pin:public _pin
{
	protected:
		_av_af *cm_af;

		virtual ~_av_af_out_pin();
	public:

		_av_af_out_pin(_av_af *af);

		virtual void connect(_pin *to);
};

// ----------------------------------- AVENCODER ---------------------------------
class _av_af:public _av_a_filter
{
    friend class _av_af_out_pin;

	protected:
	    string cm_filter_str;

	    string cm_input_format;

	    AVSampleFormat cm_out_sample_format;
	    int64_t cm_out_ch_layout;
	    int cm_out_sample_rate;

		int cm_frame_number;

		_av_af_in_pin *cm_in_pin;
		_pins_ptr_list cm_in_pins;

		_av_af_out_pin *cm_out_pin;
		_pins_ptr_list cm_out_pins;

		AVFrame *cm_filt_frame;
		_media_sample cm_out_ms;

		AVFilterGraph *cm_filter_graph;

		AVFilterContext *cm_buffersink_ctx;
        AVFilterContext *cm_buffersrc_ctx;

	public:

		_av_af(const char *filter_str,const char *input_format,AVSampleFormat out_sample_format,int64_t out_ch_layout,int out_sample_rate);
        virtual ~_av_af();

        virtual void release() { delete this; };

		virtual void proccess_input_media_sample(const _media_sample *ms);
		virtual void proccess_input_stream_header(const _stream_header *sh);

		virtual const _pins_ptr_list &out_pins() const { return cm_out_pins; };
		virtual const _pins_ptr_list &in_pins() const { return cm_in_pins; };
};

// -----------------------------------------------------------------------------
// PINS
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// IN
// -----------------------------------------------------------------------------
_av_af_in_pin::_av_af_in_pin(_av_af *af)
 :cm_af(af)
{
	cm_is_input=true;
}

// -----------------------------------------------------------------------------
_av_af_in_pin::~_av_af_in_pin()
{
}

// -----------------------------------------------------------------------------
void _av_af_in_pin::start()
{
	(*cm_af->out_pins().begin())->start();
	cm_state=started;
}

// -----------------------------------------------------------------------------
void _av_af_in_pin::stop()
{
	(*cm_af->out_pins().begin())->stop();
	cm_state=stoped;
}

// -----------------------------------------------------------------------------
void _av_af_in_pin::pause()
{
	(*cm_af->out_pins().begin())->pause();
	cm_state=paused;
}

// -----------------------------------------------------------------------------
void _av_af_in_pin::push_media_sample(const _media_sample* ms)
{
	cm_af->proccess_input_media_sample(ms);
}

// -----------------------------------------------------------------------------
void _av_af_in_pin::push_stream_header(const _stream_header* sh)
{
    cm_af->proccess_input_stream_header(sh);
}

// -----------------------------------------------------------------------------
// OUT
// -----------------------------------------------------------------------------
_av_af_out_pin::_av_af_out_pin(_av_af *af)
 :cm_af(af)
{
	cm_is_input=false;
}

// -----------------------------------------------------------------------------
_av_af_out_pin::~_av_af_out_pin()
{
}

// -----------------------------------------------------------------------------
void _av_af_out_pin::connect(_pin *to)
{
    _pin::connect(to);

    /*
    _av_codec_context_header *acch=new _av_codec_context_header;
    acch->context=cm_bsf->cm_codec_ctx;

    _stream_header sh;
    sh.set_format_header(acch);

    to->push_stream_header(&sh);
    */
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
_av_af::_av_af(const char *filter_str,const char *input_format,AVSampleFormat out_sample_format,int64_t out_ch_layout,int out_sample_rate)
:cm_filter_str(filter_str),cm_input_format(input_format),
cm_out_sample_format(out_sample_format),cm_out_ch_layout(out_ch_layout),cm_out_sample_rate(out_sample_rate),
cm_frame_number(0),cm_in_pin(NULL),cm_out_pin(NULL),
cm_filt_frame(NULL),cm_filter_graph(NULL),cm_buffersink_ctx(NULL),cm_buffersrc_ctx(NULL)
{
    cm_filt_frame = av_frame_alloc();

    avfilter_register_all();

    cm_filter_graph = avfilter_graph_alloc();

    cm_out_ms.cm_btype=MEDIA_SAMPLE_BT_AV_FRAME;
    cm_out_ms.cm_mtype=MEDIA_SAMPLE_MT_AUDIO;
    cm_out_ms.buffer=(unsigned char *)cm_filt_frame;

    cm_in_pin=new _av_af_in_pin(this);
	cm_in_pins.push_back(cm_in_pin);

	cm_out_pin=new _av_af_out_pin(this);
	cm_out_pins.push_back(cm_out_pin);
}

// -----------------------------------------------------------------------------
_av_af::~_av_af()
{
    if (cm_in_pin)
	{
		cm_in_pin->release();
	}
	cm_in_pins.clear();

	if (cm_out_pin)
	{
		// TODO - normal end_of_stream implementation
		// Comment now - infinity recurse loop
		// cm_out_pin->end_of_stream();
		cm_out_pin->release();
	}
	cm_out_pins.clear();

	if (cm_filter_graph)
    {
        avfilter_graph_free(&cm_filter_graph);
        cm_filter_graph=NULL;
    }

	if (cm_filt_frame)
    {
        cm_filt_frame = NULL;
        av_frame_free(&cm_filt_frame);
    }

	//if (bsf_ctx) av_bitstream_filter_close(bsf_ctx);
}

// -----------------------------------------------------------------------------
void _av_af::proccess_input_stream_header(const _stream_header *sh)
{
    cm_out_pin->push_stream_header(sh);
    /*
    if (sh->format_header->format_type!=MEDIA_FORMAT_AV_CODEC_CONTEXT)
        throw _pm_error(0,"_av_bsf::proccess_input_stream_header - stream header type != MEDIA_FORMAT_AV_CODEC_CONTEXT");

    cm_codec_ctx=(AVCodecContext *)((_av_codec_context_header *)(sh->format_header))->context;
    */
}

// -----------------------------------------------------------------------------
void _av_af::proccess_input_media_sample(const _media_sample* ms)
{
    if (ms->cm_mtype!=MEDIA_SAMPLE_MT_AUDIO) return ;

    AVFrame *in_frame;

    if (ms->cm_btype==MEDIA_SAMPLE_BT_AV_FRAME)
    {
        in_frame=(AVFrame *)ms->buffer;
    }
    else
    {
        throw _pm_error(0,string("_av_afilter::proccess_input_media_sample - can not process media sample with type=")+to_string(ms->cm_btype));
    }

    int ret;

    if (!cm_frame_number)
    {
        char args[1024];

        // in filter

        ret = avfilter_graph_create_filter(&cm_buffersrc_ctx, avfilter_get_by_name("abuffer"), "in",
                                       cm_input_format.c_str(), NULL, cm_filter_graph);
        if (ret < 0) throw _pm_error(ret,"_av_afilter::proccess_input_media_sample - cannot create audio buffer source");

        // out filter
        /*snprintf(args, sizeof(args),
            "sample_rate=%d:sample_fmt=%s:channel_layout=0x%"PRIx64,
             in_frame->sample_rate,
             av_get_sample_fmt_name((AVSampleFormat)AV_SAMPLE_FMT_S16), in_frame->channel_layout);
             */

        ret = avfilter_graph_create_filter(&cm_buffersink_ctx, avfilter_get_by_name("abuffersink"), "out",
                                       NULL, NULL, cm_filter_graph);
        if (ret < 0) throw _pm_error(ret,"_av_afilter::proccess_input_media_sample - cannot create audio buffer sink");

        static const enum AVSampleFormat out_sample_fmts[] = { cm_out_sample_format, (AVSampleFormat)-1 };
        static const int64_t out_channel_layouts[] = { cm_out_ch_layout, -1 };//in_frame->channel_layout, -1 };
        static const int out_sample_rates[] = { cm_out_sample_rate, -1 };

        ret = av_opt_set_int_list(cm_buffersink_ctx, "sample_fmts", out_sample_fmts, -1,
                              AV_OPT_SEARCH_CHILDREN);
        if (ret < 0) {
            av_log(NULL, AV_LOG_ERROR, "Cannot set output sample format\n");
            return;
        }

        ret = av_opt_set_int_list(cm_buffersink_ctx, "channel_layouts", out_channel_layouts, -1,
                              AV_OPT_SEARCH_CHILDREN);
        if (ret < 0) {
            av_log(NULL, AV_LOG_ERROR, "Cannot set output channel layout\n");
            return;
        }

        ret = av_opt_set_int_list(cm_buffersink_ctx, "sample_rates", out_sample_rates, -1,
                                AV_OPT_SEARCH_CHILDREN);
        if (ret < 0) {
            av_log(NULL, AV_LOG_ERROR, "Cannot set output sample rate\n");
            return;
        }

        /* Endpoints for the filter graph. */
        AVFilterInOut *outputs = avfilter_inout_alloc();
        AVFilterInOut *inputs  = avfilter_inout_alloc();

        outputs->name       = av_strdup("in");
        outputs->filter_ctx = cm_buffersrc_ctx;
        outputs->pad_idx    = 0;
        outputs->next       = NULL;

        inputs->name       = av_strdup("out");
        inputs->filter_ctx = cm_buffersink_ctx;
        inputs->pad_idx    = 0;
        inputs->next       = NULL;

        ret = avfilter_graph_parse_ptr(cm_filter_graph, cm_filter_str.c_str(),&inputs, &outputs, NULL);
        if (ret < 0) throw _pm_error(ret,"_av_afilter::proccess_input_media_sample - cannot parse_ptr");

        ret = avfilter_graph_config(cm_filter_graph, NULL);
        if (ret < 0) throw _pm_error(ret,"_av_afilter::proccess_input_media_sample - cannot config graph");
    }

    /* push the audio data from decoded frame into the filtergraph */
    ret = av_buffersrc_add_frame_flags(cm_buffersrc_ctx, in_frame, 0);
    if (ret < 0) throw _pm_error(0,"_av_af::proccess_input_media_sample - Error while feeding the audio filtergraph");

    /* pull filtered audio from the filtergraph */
    while (1)
    {
        int ret = av_buffersink_get_frame(cm_buffersink_ctx, cm_filt_frame);
        if(ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) break;
        if(ret < 0) throw _pm_error(ret,"_av_af::proccess_input_media_sample - Error while feeding the audio filtergraph");

        cm_out_ms.stream_stamp=ms->stream_stamp;
        cm_out_ms.can_break=true; // TODO - detect & set

        cm_out_pin->push_media_sample(&cm_out_ms);

        av_frame_unref(cm_filt_frame);
    }

    cm_frame_number++;
}

// -----------------------------------------------------------------------------
_av_a_filter *create_av_a_filter(const char *filter_str,const char *input_format,AVSampleFormat out_sample_format,int64_t out_ch_layout,int out_sample_rate)//const char *filter_name)
{
    return new _av_af(filter_str,input_format,out_sample_format,out_ch_layout,out_sample_rate);//filter_name);
}
