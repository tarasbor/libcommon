#include "nix_dx.hpp"
#include "dx_error.hpp"
#include <log.hpp>

#define MLOG(L) _log() << _log::_set_module_level("vcall",(L))


// ------------------------------------------------------------------------
// PIN release
// ------------------------------------------------------------------------

// ------------------------------------------------------------------------
void _pin::connect(_pin *to)
{
	//if (!is_configured()) throw(_pm_error(0,"_pin::connect - can not connect pin - first pin is not configured yet"));

	if (to==this) throw(_pm_error(0,"_pin::connect - can not connect pin to itself"));

	//if (!to->try_accept_media_type(media_type()))
	//	throw(_pm_error(0,"_pin::connect - can not connect pins, second pin do not accept media type from first pin"));

	if (cm_is_input && to->is_input())
		throw(_pm_error(0,"_pin::connect - can not connect pins, both are input"));

	if (!cm_is_input && !to->is_input())
		throw(_pm_error(0,"_pin::connect - can not connect pins, both are output"));

	_pins_ptr_list::iterator it=cm_connected_to.begin();
	while(it!=cm_connected_to.end())
	{
		if ((*it)==to) throw(_pm_error(0,"_pin::connect - can not connect pins, already connected"));
		it++;
	}

	to->on_connect(this);

	cm_connected_to.push_back(to);
}

// ------------------------------------------------------------------------
void _pin::on_connect(_pin *from)
{
}

// ------------------------------------------------------------------------
void _pin::disconnect(_pin *to)
{
	_pins_ptr_list::iterator it=cm_connected_to.begin();
	while(it!=cm_connected_to.end())
	{
		if ((*it)==to)
		{
			to->on_disconnect(this);
			cm_connected_to.erase(it);
			return;
		}
		it++;
	}
}

// ------------------------------------------------------------------------
void _pin::on_disconnect(_pin *from)
{
	_pins_ptr_list::iterator it=cm_connected_to.begin();
	while(it!=cm_connected_to.end())
	{
		if ((*it)==from)
		{
			cm_connected_to.erase(it);
			return;
		}
		it++;
	}
}

// ------------------------------------------------------------------------
void _pin::disconnect_from_all()
{
	while(!cm_connected_to.empty()) cm_connected_to.erase(cm_connected_to.begin());
}


// ------------------------------------------------------------------------
void _pin::begin_flush()
{
	_pins_ptr_list::iterator it=cm_connected_to.begin();
	while(it!=cm_connected_to.end())
	{
		(*it)->begin_flush();
		it++;
	}
}

// ------------------------------------------------------------------------
void _pin::end_flush()
{
	_pins_ptr_list::iterator it=cm_connected_to.begin();
	while(it!=cm_connected_to.end())
	{
		(*it)->end_flush();
		it++;
	}
}

// ------------------------------------------------------------------------
void _pin::end_of_stream()
{
	_pins_ptr_list::iterator it=cm_connected_to.begin();
	while(it!=cm_connected_to.end())
	{
		(*it)->end_of_stream();
		it++;
	}
}

// ------------------------------------------------------------------------
void _pin::new_segment()
{
	_pins_ptr_list::iterator it=cm_connected_to.begin();
	while(it!=cm_connected_to.end())
	{
		(*it)->new_segment();
		it++;
	}
}

// ------------------------------------------------------------------------
void _pin::start()
{
	_pins_ptr_list::iterator it=cm_connected_to.begin();
	while(it!=cm_connected_to.end())
	{
		(*it)->start();
		it++;
	}

	cm_state=started;
}

// ------------------------------------------------------------------------
void _pin::stop()
{
	_pins_ptr_list::iterator it=cm_connected_to.begin();
	while(it!=cm_connected_to.end())
	{
		(*it)->stop();
		it++;
	}

	cm_state=stoped;
}

// ------------------------------------------------------------------------
void _pin::pause()
{
	_pins_ptr_list::iterator it=cm_connected_to.begin();
	while(it!=cm_connected_to.end())
	{
		(*it)->pause();
		it++;
	}

	cm_state=paused;
}

// ------------------------------------------------------------------------
void _pin::push_media_sample(const _media_sample* ms)
{
	_pins_ptr_list::iterator it=cm_connected_to.begin();
	while(it!=cm_connected_to.end())
	{
	    (*it)->push_media_sample(ms);
		it++;
	}
}

// ------------------------------------------------------------------------
void _pin::push_stream_header(const _stream_header* sh)
{
	_pins_ptr_list::iterator it=cm_connected_to.begin();
	while(it!=cm_connected_to.end())
	{
		(*it)->push_stream_header(sh);
		it++;
	}
}

// ------------------------------------------------------------------------
// ------------------------------------------------------------------------
class _pipe_imp:public _dxpipe
{
    protected:

        _media_source *cm_src;
        _media_filter *cm_filter;
        _media_target *cm_tar;
        _media_target *cm_preview;

        bool cm_need_rebuild;

        virtual ~_pipe_imp();

    public:

        _pipe_imp();

        virtual void capture(_media_source *cap) { if (cm_src) { cm_src->release(); cm_src=NULL; } cm_src=cap; cm_need_rebuild=true; };
        virtual void coder(_media_filter *fil) { if (cm_filter) { cm_filter->release(); cm_filter=NULL; } cm_filter=fil; cm_need_rebuild=true; };
        virtual void render(_media_target *tar) { if (cm_tar) { cm_tar->release(); cm_tar=NULL; } cm_tar=tar; cm_need_rebuild=true; };
        virtual void preview(_media_target *tar) { if (cm_preview) { cm_preview->release(); cm_preview=NULL; } cm_preview=tar; cm_need_rebuild=true; };

        virtual _media_source *capture() { return cm_src; };
        virtual _media_filter *coder() { return cm_filter; };
        virtual _media_target *render() { return cm_tar; };
        virtual _media_target *preview() { return cm_preview; };

        virtual void start();
        virtual void stop();
        virtual void pause();

        virtual void release() { delete this; };
};

// ------------------------------------------------------------------------
_pipe_imp::_pipe_imp():cm_src(NULL),cm_filter(NULL),cm_tar(NULL),cm_preview(NULL),cm_need_rebuild(false)
{

}

// ------------------------------------------------------------------------
_pipe_imp::~_pipe_imp()
{
    stop();
    MLOG(0) << "_pipe_imp::~_pipe_imp - stoped..." << endl;

    if (cm_preview) { cm_preview->release(); cm_preview=NULL; };
    MLOG(0) << "_pipe_imp::~_pipe_imp - preview released..." << endl;

    if (cm_tar) { cm_tar->release(); cm_tar=NULL; };
    MLOG(0) << "_pipe_imp::~_pipe_imp - target released..." << endl;

    if (cm_filter) { cm_filter->release(); cm_filter=NULL; };
    MLOG(0) << "_pipe_imp::~_pipe_imp - filter released..." << endl;

    if (cm_src) { cm_src->release(); cm_src=NULL; };
    MLOG(0) << "_pipe_imp::~_pipe_imp - source released..." << endl;
}

// ------------------------------------------------------------------------
void _pipe_imp::start()
{
    if (!cm_src) return;
    if (cm_src->out_pins().empty()) return;//throw _pm_error(0,"_pipe::start - source have not output pins...");

    if (cm_need_rebuild)
    {
        // connect source & coder
        if (cm_filter)
        {
            if (!cm_filter->in_pins().empty())
            {
                MLOG(10) << "Connect src & filter..." << endl;
                (*cm_src->out_pins().begin())->connect((*cm_filter->in_pins().begin()));
            }


                // connect coder & target
                if (cm_tar)
                    if (!cm_tar->in_pins().empty() )
                    {
                        MLOG(10) << "Connect filter & target..." << endl;
                        (*cm_filter->out_pins().begin())->connect((*cm_tar->in_pins().begin()));
                    }
        }


        // connect source & preview
        if (cm_preview)
            if (!cm_preview->in_pins().empty())
            {
                MLOG(10) << "Connect src & preview..." << endl;
                (*cm_src->out_pins().begin())->connect((*cm_preview->in_pins().begin()));
            }

        cm_need_rebuild=false;
    }

    MLOG(10) << "Start src pin..." << endl;
    (*cm_src->out_pins().begin())->start();
    MLOG(10) << "Pipe started..." << endl;
}

// ------------------------------------------------------------------------
void _pipe_imp::stop()
{
    if (!cm_src) return;
    if (cm_src->out_pins().empty()) ;//throw _pm_error(0,"_pipe::stop - source have not output pins...");
    (*cm_src->out_pins().begin())->stop();
}

// ------------------------------------------------------------------------
void _pipe_imp::pause()
{
    if (!cm_src) return;
    if (cm_src->out_pins().empty()) ;// throw _pm_error(0,"_pipe::pause - source have not output pins...");
    (*cm_src->out_pins().begin())->pause();
}

// ------------------------------------------------------------------------
// ------------------------------------------------------------------------
_dxpipe *create_pipe()
{
    return new _pipe_imp;
}
