#include "dxshow.hpp"

#ifdef WINDOWS

#include <ocidl.h>
#include <stdio.h>

#if !defined(CLSID_VideoInputDeviceCategory)
//const GUID      CLSID_VideoInputDeviceCategory = {0x860BB310,0x5D01,0x11d0, {0xBD,0x3B,0x00,0xA0,0xC9,0x11,0xCE,0x86}};
#endif

#if !defined(CLSID_AudioInputDeviceCategory)
//const GUID      CLSID_AudioInputDeviceCategory = {0x33d9a762, 0x90c8, 0x11d0, {0xbd, 0x43, 0x0, 0xa0, 0xc9, 0x11, 0xce, 0x86}};
#endif

#if !defined(CLSID_VideoCompressorCategory)
//const GUID      CLSID_VideoCompressorCategory = {0x33d9a760, 0x90c8, 0x11d0, {0xbd, 0x43, 0x0, 0xa0, 0xc9, 0x11, 0xce, 0x86}};
#endif

#if !defined(IID_IPropertyBag)
//const GUID      IID_IPropertyBag = {0x55272A00, 0x42CB, 0x11CE, {0x81, 0x35, 0x00, 0xAA, 0x00, 0x4B, 0xB8, 0x51}};
#endif

#ifndef WIN32
const GUID      IID_ICreateDevEnum = {0x29840822, 0x5b84, 0x11d0, {0xbd, 0x3b, 0x00, 0xa0, 0xc9, 0x11, 0xce, 0x86}};
#else
#endif

// ------------------------------------------------------------------
void enum_capture_devices(REFGUID category, _video_help::_capture_devices_info &devs_info);


// ------------------------------------------------------------------
// ------------------------------------------------------------------
bool _video_help::create_enumerator(REFGUID category, IEnumMoniker **ppEnum)
{
    ICreateDevEnum *pDevEnum;
    bool result = true;

    HRESULT hr = CoCreateInstance(CLSID_SystemDeviceEnum, NULL, CLSCTX_INPROC_SERVER, IID_ICreateDevEnum, (void **)&pDevEnum);
    if (FAILED(hr)) throw(_pm_error(hr,"Can not get Create Dev Enum interface"));


    // Create an enumerator for the category.
    hr = pDevEnum->CreateClassEnumerator(category, ppEnum, 0);

    // category is empty
    if (hr == S_FALSE) result=false;

    pDevEnum->Release();

    return result;
}

// ------------------------------------------------------------------
void enum_capture_devices(REFGUID category, _video_help::_capture_devices_info &devs_info)
{

    devs_info.clear();

    IEnumMoniker *pEnum;
    bool not_empty = _video_help::create_enumerator(category, &pEnum);

    if (not_empty)
    {
        IMoniker *pMoniker = NULL;

        while (pEnum->Next(1, &pMoniker, NULL) == S_OK)
        {
            _video_help::_capture_device_info d_info;

            IPropertyBag *pPropBag;
            HRESULT hr = pMoniker->BindToStorage(0, 0, IID_IPropertyBag, (void **)&pPropBag);
            if (FAILED(hr))
            {
                pMoniker->Release();
                continue;
            }

            VARIANT var;
            VariantInit(&var);

            char tmp[256];



            hr = pPropBag->Read(L"FriendlyName", &var, 0);
            if (SUCCEEDED(hr))
            {
                //StringCbPrintf(tmp,250,"%S",var.bstrVal);
                sprintf(tmp,"%S",var.bstrVal);

                d_info.friendly_name = tmp;
                VariantClear(&var);
            }

            hr = pPropBag->Read(L"Description", &var, 0);
            if (SUCCEEDED(hr))
            {
                //StringCbPrintf(tmp,250,"%S",var.bstrVal);
                sprintf(tmp,"%S",var.bstrVal);

                d_info.description = tmp;
                VariantClear(&var);
            }

            hr = pPropBag->Read(L"WaveInID", &var, 0);
            if (SUCCEEDED(hr))
            {
                //StringCbPrintf(tmp,250,"%L",var.bstrVal);
                sprintf(tmp,"%L",var.lVal);

                d_info.wave_in_id = tmp;
                VariantClear(&var);
            }

            hr = pPropBag->Read(L"DevicePath", &var, 0);
            if (SUCCEEDED(hr))
            {
                //StringCbPrintf(tmp,250,"%S",var.bstrVal);
                sprintf(tmp,"%S",var.bstrVal);

                d_info.dev_path = tmp;
                VariantClear(&var);
            }

            pPropBag->Release();
            pMoniker->Release();

            devs_info.push_back(d_info);
        }
    }

    pEnum->Release();
}

// ------------------------------------------------------------------
void _video_help::enum_video_capture_devices(_capture_devices_info &devs_info)
{
    enum_capture_devices(CLSID_VideoInputDeviceCategory, devs_info);
}

// ------------------------------------------------------------------
void _video_help::enum_audio_capture_devices(_capture_devices_info &devs_info)
{
    enum_capture_devices(CLSID_AudioInputDeviceCategory, devs_info);
}

// ------------------------------------------------------------------
void _video_help::enum_video_encoders(_video_encoders_info &es_info)
{
    es_info.clear();

    IEnumMoniker *pEnum;
    bool not_empty = create_enumerator(CLSID_VideoCompressorCategory, &pEnum);

    if (not_empty)
    {
        IMoniker *pMoniker = NULL;

        while (pEnum->Next(1, &pMoniker, NULL) == S_OK)
        {
            _video_encoder_info e_info;

            IPropertyBag *pPropBag;
            HRESULT hr = pMoniker->BindToStorage(0, 0, IID_IPropertyBag, (void **)&pPropBag);
            if (FAILED(hr))
            {
                pMoniker->Release();
                continue;
            }

            VARIANT var;
            VariantInit(&var);

            char tmp[256];
            hr = pPropBag->Read(L"FriendlyName", &var, 0);
            if (SUCCEEDED(hr))
            {
                //StringCbPrintf(tmp,250,"%S",var.bstrVal);
                sprintf(tmp,"%S",var.bstrVal);

                e_info.friendly_name = tmp;
            }
            VariantClear(&var);

            pPropBag->Release();
            pMoniker->Release();

            es_info.push_back(e_info);

        }
    }

    pEnum->Release();
}

#endif
