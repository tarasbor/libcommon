#ifndef __DX_SHOW_PIPELINE_H__
#define __DX_SHOW_PIPELINE_H__

#include "dx_error.hpp"

#ifdef WINDOWS
    #define NO_DSHOW_STRSAFE

    #include <windows.h>
    #include <dshow.h>
#else
#endif

#include "nix_dx.hpp"
#include "dx_headers.hpp"

#include <string>
#include <list>

using namespace std;

#ifndef WINDOWS

#endif

// ------------------------------------------------------------------
// ------------------------------------------------------------------
class _video_help
{
    public:

        struct _capture_device_info
        {
            string friendly_name;
            string description;
            string dev_path;
            string wave_in_id;
        };
        typedef list<_capture_device_info> _capture_devices_info;

        struct _video_encoder_info
        {
            string friendly_name;
        };
        typedef list<_video_encoder_info> _video_encoders_info;

		// move it from interface to implementation

		#ifdef WINDOWS
        static bool create_enumerator(REFGUID category, IEnumMoniker **ppEnum);
        #endif

        static void enum_video_capture_devices(_capture_devices_info &devs_info);
        static void enum_audio_capture_devices(_capture_devices_info &devs_info);
        static void enum_video_encoders(_video_encoders_info &es_info);
};

// ------------------------------------------------------------------
// ------------------------------------------------------------------
class _video_capture:public _media_source
{
    protected:
        virtual ~_video_capture() {};

    public:

        virtual void configure(int x,int y,int frame_rate) = 0;
        virtual void release() = 0;
};

_video_capture *create_video_capture(int index);

enum  	AUDIO_PCM_FORMAT
{
  AUDIO_PCM_FORMAT_UNKNOWN = -1,
  AUDIO_PCM_FORMAT_S8 = 0,
  AUDIO_PCM_FORMAT_U8 = 1,
  AUDIO_PCM_FORMAT_S16_LE = 2,
  AUDIO_PCM_FORMAT_S16_BE = 3,
  AUDIO_PCM_FORMAT_U16_LE = 4,
  AUDIO_PCM_FORMAT_U16_BE = 5,
  AUDIO_PCM_FORMAT_S24_LE = 6,
  AUDIO_PCM_FORMAT_S24_BE = 7,
  AUDIO_PCM_FORMAT_U24_LE = 8,
  AUDIO_PCM_FORMAT_U24_BE = 9,
  AUDIO_PCM_FORMAT_S32_LE = 10,
  AUDIO_PCM_FORMAT_S32_BE = 11,
  AUDIO_PCM_FORMAT_U32_LE = 12,
  AUDIO_PCM_FORMAT_U32_BE = 13,
  AUDIO_PCM_FORMAT_FLOAT_LE = 14,
  AUDIO_PCM_FORMAT_FLOAT_BE = 15,
  AUDIO_PCM_FORMAT_FLOAT64_LE = 16,
  AUDIO_PCM_FORMAT_FLOAT64_BE = 17,
  AUDIO_PCM_FORMAT_IEC958_SUBFRAME_LE = 18,
  AUDIO_PCM_FORMAT_IEC958_SUBFRAME_BE = 19,
  AUDIO_PCM_FORMAT_MU_LAW = 20,
  AUDIO_PCM_FORMAT_A_LAW = 21,
  AUDIO_PCM_FORMAT_IMA_ADPCM = 22,
  AUDIO_PCM_FORMAT_MPEG = 23,
  AUDIO_PCM_FORMAT_GSM = 24,
  AUDIO_PCM_FORMAT_SPECIAL = 31,
  AUDIO_PCM_FORMAT_S24_3LE = 32,
  AUDIO_PCM_FORMAT_S24_3BE = 33,
  AUDIO_PCM_FORMAT_U24_3LE = 35,
  AUDIO_PCM_FORMAT_U24_3BE = 36,
  AUDIO_PCM_FORMAT_S20_3LE = 37,
  AUDIO_PCM_FORMAT_S20_3BE = 38,
  AUDIO_PCM_FORMAT_U20_3LE = 39,
  AUDIO_PCM_FORMAT_U20_3BE = 40,
  AUDIO_PCM_FORMAT_S18_3LE = 41,
  AUDIO_PCM_FORMAT_S18_3BE = 42,
  AUDIO_PCM_FORMAT_U18_3LE = 43,
  AUDIO_PCM_FORMAT_U18_3BE = 44,
  /*
  AUDIO_PCM_FORMAT_G723_24 = 45,
  AUDIO_PCM_FORMAT_G723_24_1B = 46,
  AUDIO_PCM_FORMAT_G723_40 = 47,
  AUDIO_PCM_FORMAT_G723_40_1B = 48,
  AUDIO_PCM_FORMAT_DSD_U8 = 49,
  AUDIO_PCM_FORMAT_DSD_U16_LE = 50,
  AUDIO_PCM_FORMAT_DSD_U32_LE = 51,
  AUDIO_PCM_FORMAT_DSD_U16_BE = 52,
  AUDIO_PCM_FORMAT_DSD_U32_BE = 53,
  */
  AUDIO_PCM_FORMAT_LAST = AUDIO_PCM_FORMAT_U18_3BE, //AUDIO_PCM_FORMAT_DSD_U32_BE,
  AUDIO_PCM_FORMAT_S16 = AUDIO_PCM_FORMAT_S16_LE,
  AUDIO_PCM_FORMAT_U16 = AUDIO_PCM_FORMAT_U16_LE,
  AUDIO_PCM_FORMAT_S24 = AUDIO_PCM_FORMAT_S24_LE,
  AUDIO_PCM_FORMAT_U24 = AUDIO_PCM_FORMAT_U24_LE,
  AUDIO_PCM_FORMAT_S32 = AUDIO_PCM_FORMAT_S32_LE,
  AUDIO_PCM_FORMAT_U32 = AUDIO_PCM_FORMAT_U32_LE,
  AUDIO_PCM_FORMAT_FLOAT = AUDIO_PCM_FORMAT_FLOAT_LE,
  AUDIO_PCM_FORMAT_FLOAT64 = AUDIO_PCM_FORMAT_FLOAT64_LE,
  AUDIO_PCM_FORMAT_IEC958_SUBFRAME = AUDIO_PCM_FORMAT_IEC958_SUBFRAME_LE
};

// ------------------------------------------------------------------
// ------------------------------------------------------------------
class _audio_capture:public _media_source
{
    protected:
        virtual ~_audio_capture() {};

    public:

		virtual void configure(int ch_count,int sample_rate,AUDIO_PCM_FORMAT pcm_format,int buff_ms_len) = 0;
		// ch_count - channels count
		// sample_rate - how much samples per second
		// pcm_format - pcm format
		// buff_ms_len - len of capture buffer (in ms)

        virtual void release() = 0;
};

_audio_capture *create_audio_capture(int index);
_audio_capture *create_audio_capture(const string &s);


// ------------------------------------------------------------------
// ------------------------------------------------------------------
class _audio_render:public _media_target
{
    protected:
        virtual ~_audio_render() {};

    public:

        //virtual IBaseFilter *filter() = 0;
        //virtual _media_type *media_type() = 0;
        //virtual _media_sample *media_sample() = 0;
        virtual void release() = 0;
};

_audio_render *create_audio_render(int index);

// ------------------------------------------------------------------
// ------------------------------------------------------------------
class _video_encoder:public _media_filter
{
    protected:
        virtual ~_video_encoder() {};

    public:

        virtual void release() = 0;

		virtual void config(int bitrate,int w,int h,int fps,int base_frame) = 0;
};

_video_encoder *create_video_encoder(int index);

// ------------------------------------------------------------------
// ------------------------------------------------------------------
class _video_decoder:public _media_filter
{
    protected:
        virtual ~_video_decoder() {};

    public:
        virtual void release() = 0;

		virtual void config(int w,int h,int buffer_frame_cnt) = 0;
};

_video_decoder *create_video_decoder(int index);

// ------------------------------------------------------------------
class _video_render:public _media_target
{
    protected:
        virtual ~_video_render() {};

    public:
        virtual void release() = 0;
        virtual void resize_repos(const _rect &r) = 0;
};

// ------------------------------------------------------------------
// ------------------------------------------------------------------
class _audio_encoder:public _media_filter
{
    protected:
        virtual ~_audio_encoder() {};

    public:

        virtual void config(int bitrate) = 0;
        virtual void config(int bitrate,int ch_count,int sample_rate,AUDIO_PCM_FORMAT pcm_format) = 0;

        virtual void release() = 0;
};

_audio_encoder *create_audio_encoder(int index);

// ------------------------------------------------------------------
// ------------------------------------------------------------------
class _audio_decoder:public _media_filter
{
    protected:
        virtual ~_audio_decoder() {};

    public:

        virtual void config(int ch_num,int sample_rate) = 0;

        virtual void release() = 0;
};

_audio_decoder *create_audio_decoder(int index);


/*
// ------------------------------------------------------------------
// ------------------------------------------------------------------
class _video_render
{
    protected:
        virtual ~_video_render() {};

    public:

        //virtual IBaseFilter *filter() = 0;

        virtual _media_type *media_type() = 0;
        virtual _media_sample *media_sample() = 0;

        //virtual void set_draw_wnd(HWND wnd,const RECT *r,int rot_angle) = 0;
		virtual void set_message(const char *msg,bool clear) = 0;

        virtual void release() = 0;
};

// ------------------------------------------------------------------
_video_render *create_video_render_to_net();
_video_render *create_video_gl_render();


// ------------------------------------------------------------------
_audio_render *create_audio_render_to_net();
_audio_render *create_audio_render_snd_card(const string &dev_name);
*/

/*
// ------------------------------------------------------------------
// ------------------------------------------------------------------
class _media_source
{
    protected:
        virtual ~_media_source() {};

    public:

        virtual IBaseFilter *filter() = 0;

        virtual void set_media_type(const _media_type *mh) = 0;
        virtual void set_media_sample(const _media_sample *ms) = 0;

        //virtual IPin *get_pin(int n) = 0;

        virtual void release() = 0;

        virtual void set_sample_time(int st) = 0;

        virtual int media_samples_buffer_count() = 0;
        virtual void clear_media_samples() = 0;


};
*/

/*
// ------------------------------------------------------------------
// ------------------------------------------------------------------
class _video_source:public _media_source
{
    protected:
        virtual ~_video_source() {};

    public:

        virtual IBaseFilter *filter() = 0;

        virtual void set_media_header(_media_header *mh) = 0;
        virtual void set_media_sample(_media_sample *ms) = 0;

        virtual IPin *get_pin(int n) = 0;

        virtual void release() = 0;

        virtual void set_sample_time(int st) = 0;

        virtual int media_samples_buffer_count() = 0;


};
*/

// ------------------------------------------------------------------
_media_source *create_source_from_net();

/*
// ------------------------------------------------------------------
// ------------------------------------------------------------------
class _audio_source
{
    protected:
        ~_audio_source() {};

    public:

        virtual IBaseFilter *filter() = 0;

        virtual void set_media_header(_media_header *mh) = 0;
        virtual void set_media_sample(_media_sample *ms) = 0;

        virtual IPin *get_pin(int n) = 0;

        virtual void release() = 0;

        virtual void set_sample_time(int st) = 0;

        virtual void clear_media_samples() = 0;
};
*/

// ------------------------------------------------------------------
//_media_source *create_audio_source_from_net();

/*
// ------------------------------------------------------------------
// ------------------------------------------------------------------
class _video_capture_pipe
{
    protected:
        virtual ~_video_capture_pipe() {};

    public:

        virtual void set_capture_device(_video_capture *vc) = 0;
        virtual _video_capture *capture_device() = 0;

        virtual void set_video_encoder(_video_encoder *e) = 0;
        virtual _video_encoder *video_encoder() = 0;

        virtual void set_video_render(_video_render *r) = 0;
        virtual _video_render *video_render() = 0;

        virtual void set_video_preview(_video_render *r) = 0;
        virtual _video_render *video_preview() = 0;

        //virtual IGraphBuilder *graph() = 0;

        virtual void start() = 0;
        virtual void stop() = 0;
        virtual void pause() = 0;

        virtual void release() = 0;
};


// ------------------------------------------------------------------
_video_capture_pipe *create_video_capture_pipe();
*/


/*
// ------------------------------------------------------------------
// ------------------------------------------------------------------
class _audio_capture_pipe
{
    protected:
        virtual ~_audio_capture_pipe() {};

    public:

        virtual void set_capture_device(_audio_capture *vc) = 0;
        virtual _audio_capture *capture_device() = 0;

        virtual void set_audio_encoder(_audio_encoder *e) = 0;
        virtual _audio_encoder *audio_encoder() = 0;

        virtual void set_audio_render(_audio_render *r) = 0;
        virtual _audio_render *audio_render() = 0;

        //virtual IGraphBuilder *graph() = 0;

        virtual void start() = 0;
        virtual void stop() = 0;
        virtual void pause() = 0;

        virtual void set_need_preview(bool n) = 0;

        virtual void release() = 0;
};

// ------------------------------------------------------------------
_audio_capture_pipe *create_audio_capture_pipe();

// ------------------------------------------------------------------
// ------------------------------------------------------------------
class _video_render_pipe
{
    protected:
        virtual ~_video_render_pipe() {};

    public:

        virtual void set_video_source(_media_source *vs) = 0;
        virtual _media_source *video_source() = 0;


        virtual void set_video_decoder(_video_encoder *e) = 0;
        virtual _video_decoder *video_encoder() = 0;


        virtual void set_video_render(_video_render *r) = 0;
        virtual _video_render *video_render() = 0;

        //virtual IGraphBuilder *graph() = 0;

        virtual void start() = 0;
        virtual void stop() = 0;
        virtual void pause() = 0;

        //virtual void set_need_preview(bool n) = 0;

        //virtual void set_video_window(RECT *r,HWND w) = 0;

        virtual void release() = 0;

        virtual void redraw_video() = 0;
};

// ------------------------------------------------------------------
_video_render_pipe *create_video_render_pipe();

// ------------------------------------------------------------------
// ------------------------------------------------------------------
class _audio_render_pipe
{
    protected:
        virtual ~_audio_render_pipe() {};

    public:

        virtual void set_audio_source(_media_source *vs) = 0;
        virtual _media_source *audio_source() = 0;


        virtual void set_video_decoder(_video_encoder *e) = 0;
        virtual _video_decoder *video_encoder() = 0;


        virtual void set_audio_render(_audio_render *r) = 0;
        virtual _audio_render *audio_render() = 0;


        //virtual IGraphBuilder *graph() = 0;

        virtual void start() = 0;
        virtual void stop() = 0;
        virtual void pause() = 0;

        //virtual void set_need_preview(bool n) = 0;

        //virtual void set_video_window(RECT *r,HWND w) = 0;

        virtual void release() = 0;
};

// ------------------------------------------------------------------
_audio_render_pipe *create_audio_render_pipe();
*/

#ifdef WINDOWS
// ------------------------------------------------------------------
void WINAPI FreeMediaType(AM_MEDIA_TYPE& mt);
// ------------------------------------------------------------------
void WINAPI DeleteMediaType(AM_MEDIA_TYPE *pmt);
#endif

// We do not use now this functions - comment it
// -----------------------------------------------------
// HRESULT AddToRot(IUnknown *pUnkGraph, DWORD *pdwRegister);
// -----------------------------------------------------
// void RemoveFromRot(DWORD pdwRegister);

#endif
