#ifndef __DXSHOW_AV_CAPTURE__
#define __DXSHOW_AV_CAPTURE__

#include "dxshow.hpp"

_media_source *create_av_capture(const string &format,const std::string &uri,const std::string &options);

#endif // __DXSHOW_AV_CAPTURE__
