#ifndef __DX_SHOW_PROXY_PROC_H__
#define __DX_SHOW_PROXY_PROC_H__

#ifdef WINDOWS

#include "dxshow.hpp"

_dx_proxy_from_src_processor *create_dx_proxy_from_src_processor(_pin *to_pin);
_dx_proxy_to_tar_processor *create_dx_proxy_to_tar_processor(_pin *from_pin);


#endif

#endif // __DX_SHOW_PROXY_IMP_H__
