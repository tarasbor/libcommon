#include "dxshow_av_out.hpp"

#define __STDC_CONSTANT_MACROS
#define __STDC_LIMIT_MACROS
#define __STDC_FORMAT_MACROS

extern "C" {
#include <libavutil/opt.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/imgutils.h>
}

#include <clthread.hpp>

#include <iostream>

using namespace std;

// ======================================================================================
//                                           AV OUTPUT CLASS
// ======================================================================================
class _avo;

// ----------------------------------- MIXER IN PIN ---------------------------------
class _avo_in_pin:public _pin
{
	protected:
		_avo *cm_avo;

		bool cm_sink;

		AVCodecContext *cm_codec_ctx;
		AVStream *cm_stream;

		int cm_frame_cnt;

		virtual ~_avo_in_pin();

	public:
		_avo_in_pin(_avo *avo);

		virtual void start();
		virtual void stop();
		virtual void pause();

		virtual void push_media_sample(const _media_sample *ms);
		virtual void push_stream_header(const _stream_header *sh);
};

// ----------------------------------- MIXER ---------------------------------
class _avo:public _av_out
{
    friend class _avo_in_pin;

	protected:

		_pins_ptr_list cm_in_pins;

		AVFormatContext *cm_fmt_ctx;
		AVOutputFormat *cm_av_out_fmt;

		std::string cm_uri;

		bool cm_is_open;

		_locker cm_lock;

		virtual ~_avo();

	public:

		_avo(const std::string &guess_format);

        virtual void release() { delete this; };

        virtual _pin *create_input_pin();

		virtual const _pins_ptr_list &in_pins() const { return cm_in_pins; };

		virtual void open_out();
		virtual void close_out();

		virtual void uri(const char *uri) { cm_uri=uri; };
		virtual const char *uri() const { return cm_uri.c_str(); };
};

// -----------------------------------------------------------------------------
// PINS
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// IN
// -----------------------------------------------------------------------------
_avo_in_pin::_avo_in_pin(_avo *avo)
 :cm_avo(avo),cm_frame_cnt(0),cm_codec_ctx(NULL),cm_stream(NULL)
{
	cm_is_input=true;
}

// -----------------------------------------------------------------------------
_avo_in_pin::~_avo_in_pin()
{
    //if (cm_codec_ctx) av_free(cm_codec_ctx);
}

// -----------------------------------------------------------------------------
void _avo_in_pin::start()
{
    cout << "_avo_in_pin::start" << endl;
    cm_state=started;
    cm_avo->open_out();
}

// -----------------------------------------------------------------------------
void _avo_in_pin::stop()
{
    cout << "_avo_in_pin::stop" << endl;
    cm_state=stoped;
	cm_avo->close_out();
}

// -----------------------------------------------------------------------------
void _avo_in_pin::pause()
{
    cm_state=paused;
    cm_avo->close_out();
}

// -----------------------------------------------------------------------------
void _avo_in_pin::push_media_sample(const _media_sample* ms)
{
    cm_avo->cm_lock.lock();
    if (!cm_avo->cm_is_open)
    {
        cm_avo->cm_lock.unlock();
        return ;
    }

    AVPacket *p;
    if (ms->cm_btype==MEDIA_SAMPLE_BT_AV_PACKET)
    {
        p=(AVPacket *)ms->buffer;
    }
    else
        throw _pm_error(0,string("_avo_in_pin::push_media_sample - can not process media sample with type=")+to_string(ms->cm_btype));

    //cout << "_avo_in_pin::push_media_sample index=" << cm_stream->index << " is_open=" << cm_avo->cm_is_open << endl;

    int d=0;
    //if (cm_stream->codec->codec_type==AVMEDIA_TYPE_AUDIO) d=+400000*25;

    p->pts = ((ms->stream_stamp+d)*cm_stream->time_base.den)/10000000; //av_rescale_q(p->pts, AVRational{1,50}, cm_stream->time_base);
    p->dts = p->pts;
    p->stream_index = cm_stream->index;

    //cout << "stamp=" << ms->stream_stamp << " pts=" << p->pts << endl;

    //if (cm_stream->codec->codec_type==AVMEDIA_TYPE_VIDEO)
    //    cout << "V p->dts=" << p->dts << " ms->stream_stamp=" << ms->stream_stamp << endl;
    //else
    //    cout << "A p->dts=" << p->dts << " ms->stream_stamp=" << ms->stream_stamp << endl;

    int ret = av_interleaved_write_frame(cm_avo->cm_fmt_ctx, p);
    //int ret = av_write_frame(cm_avo->cm_fmt_ctx, p);
    cm_avo->cm_lock.unlock();

    if (ret < 0) throw _pm_error(0,string("_avo_in_pin::push_media_sample - error write sample ")+to_string(ret));

    cm_frame_cnt++;

    //cout << "INDEX=" << p->stream_index << " time_base=" << cm_stream->time_base.num << "/" << cm_stream->time_base.den << " PTS=" <<p->pts << " DTS=" << p->dts << endl;
}

// -----------------------------------------------------------------------------
void _avo_in_pin::push_stream_header(const _stream_header* sh)
{
    if (sh->format_header->format_type!=MEDIA_FORMAT_AV_CODEC_CONTEXT)
        throw _pm_error(sh->format_header->format_type,"_avo_in_pin::push_stream_header - recv unsupported stream header (must be MEDIA_FORMAT_AV_CONTEXT)");

    cout << "_avo_in_pin::push_stream_header" << endl;

    /*AVCodecContext *orig_ctx=(AVCodecContext *)((_av_codec_context_header *)(sh->format_header))->context;
    cm_codec_ctx=avcodec_alloc_context3(orig_ctx->codec);
    if (orig_ctx->codec_type==AVMEDIA_TYPE_AUDIO)
    {
        avcodec_copy_context(cm_codec_ctx,orig_ctx);
    }
    else
    {
        cm_codec_ctx->time_base.num=orig_ctx->time_base.num;
        cm_codec_ctx->time_base.den=orig_ctx->time_base.den;
        cm_codec_ctx->width=orig_ctx->width;
        cm_codec_ctx->height=orig_ctx->height;
        cm_codec_ctx->bit_rate=orig_ctx->bit_rate;
    }
    */

    cm_codec_ctx=(AVCodecContext *)((_av_codec_context_header *)(sh->format_header))->context;

    cm_stream = avformat_new_stream(cm_avo->cm_fmt_ctx,cm_codec_ctx->codec);
    if (!cm_stream) throw _pm_error(0,"_avo_in_pin::_avo_in_pin - Can not create stream.");
    cm_stream->codec = cm_codec_ctx;

    //avcodec_copy_context(cm_stream->codec,cm_codec_ctx);

    /*
    AVCodecContext *orig_ctx=(AVCodecContext *)((_av_codec_context_header *)(sh->format_header))->context;

    cm_stream = avformat_new_stream(cm_avo->cm_fmt_ctx,orig_ctx->codec);
    if (!cm_stream) throw _pm_error(0,"_avo_in_pin::_avo_in_pin - Can not create stream.");

    avcodec_copy_context(cm_stream->codec,orig_ctx);
    cm_stream->codec->codec_tag = 0;//cm_codec_ctx;
    */

    /*if (cm_stream->codec->codec_type==AVMEDIA_TYPE_AUDIO)
    {
        cm_stream->codec->bit_rate=128*1024;
        cm_stream->codec->sample_rate=44100;
        cm_stream->codec->channels=2;
        cm_stream->codec->channel_layout = av_get_default_channel_layout (2);
        cm_stream->codec->sample_fmt=AV_SAMPLE_FMT_FLT;
    }
    */

    cout << "_avo_in_pin:: index=" << cm_stream->index << " tb=" << cm_stream->time_base.num << "/" << cm_stream->time_base.den << endl;

    //cm_stream->codec->codec_tag = 0;
    if (cm_avo->cm_fmt_ctx->oformat->flags & AVFMT_GLOBALHEADER) cm_stream->codec->flags |= CODEC_FLAG_GLOBAL_HEADER;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
_avo::_avo(const std::string &guess_format)
:cm_fmt_ctx(NULL),cm_av_out_fmt(NULL),cm_is_open(false)
{
    av_register_all();

    cm_av_out_fmt=av_guess_format(NULL,guess_format.c_str(),NULL);
    if (!cm_av_out_fmt) throw _pm_error(0,string("_avo::_avo() - can not find output format - ")+guess_format);

    int err=avformat_alloc_output_context2(&cm_fmt_ctx,cm_av_out_fmt,NULL,NULL);
    if (err) throw _pm_error(0,"_avo::_avo() - can not create output context");
}

// -----------------------------------------------------------------------------
_avo::~_avo()
{
    cout << "_avo::~_avo" << endl;

    _pin_ptr_it it=cm_in_pins.begin();
    while(it!=cm_in_pins.end())
    {
        (*it)->release();
        it++;
    }
    cm_in_pins.clear();

    if (cm_is_open) close_out();

    if (cm_fmt_ctx)
    {
        //avformat_free_context(cm_fmt_ctx);
    }
}

// -----------------------------------------------------------------------------
_pin *_avo::create_input_pin()
{
    _avo_in_pin *ip=new _avo_in_pin(this);
    cm_in_pins.push_back(ip);
    return ip;
}

// -----------------------------------------------------------------------------
_av_out *create_av_out(const std::string &guess_format)
{
    return new _avo(guess_format);
}

// -----------------------------------------------------------------------------
void _avo::open_out()
{
    if (cm_is_open) return;

    // all pins MUST be started
    _pin_ptr_it it=cm_in_pins.begin();
    while(it!=cm_in_pins.end())
    {
        if ((*it)->state()!=_pin::started) return;
        it++;
    }

    av_dump_format(cm_fmt_ctx, 0, cm_uri.c_str(), 1);

    int ret;
    if (!(cm_av_out_fmt->flags & AVFMT_NOFILE))
    {
        ret = avio_open(&cm_fmt_ctx->pb, cm_uri.c_str(), AVIO_FLAG_WRITE);
        if (ret < 0) throw _pm_error(0,string("_avo::open_out - can not open output ")+cm_uri.c_str());
    }

    ret = avformat_write_header(cm_fmt_ctx, NULL);
    if (ret < 0) throw _pm_error(0,"_avo::open_out - can not write header");

    cm_is_open=true;
}

// -----------------------------------------------------------------------------
void _avo::close_out()
{
    if (!cm_is_open) return;

    // all pins MUST be stopped
    /*
    _pin_ptr_it it=cm_in_pins.begin();
    while(it!=cm_in_pins.end())
    {
        if ((*it)->state()==_pin::started) return;
        it++;
    }
    */

    cm_lock.lock();

    av_write_trailer(cm_fmt_ctx);

    if (!(cm_av_out_fmt->flags & AVFMT_NOFILE)) avio_closep(&cm_fmt_ctx->pb);

    cm_is_open=false;
    cm_lock.unlock();

}
