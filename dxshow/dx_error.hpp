#ifndef __DX_SHOW_PIPELINE_ERROR_H__
#define __DX_SHOW_PIPELINE_ERROR_H__

#include <error.hpp>

// ------------------------------------------------------------------
// ------------------------------------------------------------------
class _pm_error:public _error
{
    public:
        _pm_error(int err,const string &err_st):_error(err,err_st) {}; //_err_no(err),_err_str(err_st) {};
};

#endif
