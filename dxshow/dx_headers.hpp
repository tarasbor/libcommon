#ifndef __DX_HEADERS_H__
#define __DX_HEADERS_H__

#include <time.hpp>
#include <dx_error.hpp>

#define STDCALL(A)	A __stdcall

// ------------------------------------------------------------------------
struct _rect
{
	int x;
	int y;
	int w;
	int h;

	_rect():x(0),y(0),w(1),h(1) {};
	_rect(int sx,int sy,int sw,int sh):x(sx),y(sy),w(sw),h(sh) {};
};

#define MEDIA_SAMPLE_BT_CLASS     unsigned char
#define MEDIA_SAMPLE_BT_RAW       0
#define MEDIA_SAMPLE_BT_AV_PACKET 1
#define MEDIA_SAMPLE_BT_AV_FRAME  2

#define MEDIA_SAMPLE_MT_CLASS       unsigned char
#define MEDIA_SAMPLE_MT_UNDEF       0
#define MEDIA_SAMPLE_MT_VIDEO       1
#define MEDIA_SAMPLE_MT_AUDIO       2
#define MEDIA_SAMPLE_MT_DATA        3

// -------------------------------------------------------
// -------------------------------------------------------
struct _media_sample
{

    _time::time_val_i           stream_stamp;

    MEDIA_SAMPLE_BT_CLASS       cm_btype;
	MEDIA_SAMPLE_MT_CLASS       cm_mtype;

	unsigned char *buffer;
	unsigned int buffer_size;
	unsigned int data_size;

	unsigned char can_break;

	_media_sample();
	~_media_sample();

    void create_buffer(unsigned int size);
    void copy_to_buffer(unsigned char *d,unsigned int len);

    int pack_size() const;
    void pack(unsigned char *d) const;
    int depack(const unsigned char *d, int d_size);

    _media_sample(const _media_sample &s);
};

// -------------------------------------------------------
#define MEDIA_FORMAT_TYPE_CLASS		        unsigned int
#define MEDIA_FORMAT_VIDEO		            0
#define MEDIA_FORMAT_AUDIO		            1
#define MEDIA_FORMAT_AV_FORMAT_CONTEXT      2
#define MEDIA_FORMAT_AV_CODEC_CONTEXT       3
#define MEDIA_FORMAT_PORTABLE               100

// -------------------------------------------------------
struct _format_header
{
	MEDIA_FORMAT_TYPE_CLASS format_type;
	unsigned int avg_nsec_per_frame;

	_format_header():format_type(0),avg_nsec_per_frame(0) {};
	virtual ~_format_header() {};

	virtual int pack_size() const = 0;
	virtual void pack(unsigned char *d) const = 0;
	virtual int depack(const unsigned char *d, int d_size) = 0;
};

// -------------------------------------------------------
struct _audio_header:public _format_header
{
	unsigned int sample_rate;
	unsigned int channels_count;
	unsigned int avg_bytes_per_sec;
	unsigned int block_align;
	unsigned int bits_per_sample;
	unsigned char samples_order;

	virtual int pack_size() const;
	virtual void pack(unsigned char *d) const;
	virtual int depack(const unsigned char *d, int d_size);

	_audio_header();
	virtual ~_audio_header();
};

// -------------------------------------------------------
struct _video_header:public _format_header
{
	_video_header();
	virtual ~_video_header();

	_rect source_rect;
    _rect target_rect;

    unsigned int bit_rate;
    unsigned int error_bit_rate;

    _rect image_dim;
    unsigned int image_bpp;
    unsigned int image_pixel_format;

    virtual int pack_size() const;
	virtual void pack(unsigned char *d) const;
	virtual int depack(const unsigned char *d, int d_size);
};

// -------------------------------------------------------
struct _av_codec_context_header:public _format_header
{
    _av_codec_context_header() { format_type=MEDIA_FORMAT_AV_CODEC_CONTEXT; };
    ~_av_codec_context_header() {};

    void *context;

    virtual int pack_size() const { throw _pm_error(0,"_av_codec_context_header::pack_size - not release yet"); };
	virtual void pack(unsigned char *d) const { throw _pm_error(0,"_av_codec_context_header::pack - not release yet"); };
	virtual int depack(const unsigned char *d, int d_size) { throw _pm_error(0,"_av_codec_context_header::depack - not release yet"); };
};

// -------------------------------------------------------
struct _portable_header:public _format_header
{
    _portable_header() { format_type=MEDIA_FORMAT_PORTABLE; };
    ~_portable_header() {};

    string text;

    virtual int pack_size() const { return sizeof(format_type)+sizeof(avg_nsec_per_frame)+sizeof(unsigned int)+text.length(); };
	virtual void pack(unsigned char *d) const;
	virtual int depack(const unsigned char *d, int d_size);
};

// -------------------------------------------------------
#define STREAM_MAJOR_TYPE_CLASS		unsigned int
#define STREAM_MINOR_TYPE_CLASS		unsigned int

// -------------------------------------------------------
struct _stream_header
{
	_stream_header();
	~_stream_header();

	unsigned char samples_are_fixed_size;
    unsigned char use_interframes;

    STREAM_MAJOR_TYPE_CLASS major_type;
    STREAM_MINOR_TYPE_CLASS minor_type;

    _format_header *format_header;
    void set_format_header(_format_header *s);

    bool operator==(const _stream_header &mt) const;

    int pack_size() const;
	void pack(unsigned char *d) const;
	int depack(const unsigned char *d, int d_size);
};


#endif
