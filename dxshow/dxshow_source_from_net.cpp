#include "dxshow.hpp"

#ifdef WINDOWS

// ------------------------------------------------------------------
class _sour_from_net:public _media_source
{
    private:

        _net_source_filter *ns_filter;
        
    public:

        _sour_from_net();
        ~_sour_from_net();
        
        
        virtual IBaseFilter *filter() { if (ns_filter) return ns_filter->filter_iface(); return NULL; };
        
        virtual void set_media_header(const _media_header *mh) { if (ns_filter) ns_filter->set_media_header(mh); };
        virtual void set_media_sample(const _media_sample *ms) { if (ns_filter) ns_filter->set_media_sample(ms); };

        virtual IPin *get_pin(int n) { if (ns_filter) return ns_filter->get_pin(n); return NULL; };

        virtual void release() { delete this; };

        virtual void set_sample_time(int st) { if (ns_filter) ns_filter->set_sample_time(st); };

        virtual int media_samples_buffer_count() { if (ns_filter) return ns_filter->media_samles_buffer_count(); return 0; };
        virtual void clear_media_samples() { if (ns_filter) ns_filter->clear_media_samples(); };
        
};

// ------------------------------------------------------------------
_sour_from_net::_sour_from_net()
    :ns_filter(NULL)
{
    ns_filter = create_net_source_filter();
}

// ------------------------------------------------------------------
_sour_from_net::~_sour_from_net()
{
    if (ns_filter) ns_filter->release();
    ns_filter = NULL;
}

// ------------------------------------------------------------------
_media_source *create_source_from_net()
{
    return new _sour_from_net;
}

#endif