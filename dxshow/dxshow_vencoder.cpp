#include "dxshow.hpp"

//#ifdef DEBUG
#include <log.hpp>
#define MLOG(L) _log() << _log::_set_module_level("vcall",(L))
//#endif

#define __STDC_CONSTANT_MACROS
#define __STDC_LIMIT_MACROS
#define __STDC_FORMAT_MACROS

extern "C" {
#include <libavutil/opt.h>
#include <libavcodec/avcodec.h>
#include <libavutil/imgutils.h>
}

#include <iostream>

using namespace std;


// ======================================================================================
//                                           ENCODER
// ======================================================================================
class _venc;

// ----------------------------------- AVENCODER IN PIN ---------------------------------
class _venc_in_pin:public _pin
{
	protected:
		_venc *cm_enc;

		virtual ~_venc_in_pin();

	public:
		_venc_in_pin(_venc *enc,int bitrate,int w,int h,int frame_rate);

		virtual void start();
		virtual void stop();
		virtual void pause();

		virtual void push_media_sample(const _media_sample *ms);
		virtual void push_stream_header(const _stream_header *sh);
};

// ----------------------------------- AVENCODER OUT PIN ---------------------------------
class _venc_out_pin:public _pin
{
	protected:
		_venc *cm_enc;

		virtual ~_venc_out_pin();
	public:

		_venc_out_pin(_venc *enc);

		virtual void connect(_pin *to);
};

// ----------------------------------- AVENCODER ---------------------------------
class _venc:public _video_encoder
{
    friend class _venc_out_pin;

	protected:

		AVCodec *codec;
		AVCodecContext *ccontext;

		AVPacket packet;
		_media_sample cm_out_ms;
		_time::time_val_i cm_last_stamp;

		int frame_number;

		bool is_open;

		_venc_in_pin *cm_in_pin;
		_pins_ptr_list cm_in_pins;

		_venc_out_pin *cm_out_pin;
		_pins_ptr_list cm_out_pins;

		virtual void open_encoder();
		virtual void close_encoder();

	public:

		_venc(int index);
        virtual ~_venc();

        //IBaseFilter *filter() { return NULL; }; // stub
        virtual void release() { delete this; };

		virtual void config(int bitrate,int w,int h,int fps,int base_frame);


		virtual void proccess_input_media_sample(const _media_sample *ms);
		virtual void proccess_input_stream_header(const _stream_header *sh);

		virtual const _pins_ptr_list &out_pins() const { return cm_out_pins; };
		virtual const _pins_ptr_list &in_pins() const { return cm_in_pins; };
};

// -----------------------------------------------------------------------------
// PINS
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// IN
// -----------------------------------------------------------------------------
_venc_in_pin::_venc_in_pin(_venc *enc,int bitrate,int w,int h,int frame_rate)
 :cm_enc(enc)
{
	// set out pin header
	/*
	_media_type *mt=new _media_type;
	mt->major_type=0;
	mt->minor_type=0;
	mt->samples_are_fixed_size=0;
	mt->use_interframes=0;

	_video_header *vh=new _video_header;

	vh->source_rect.x=vh->source_rect.y=0;
	vh->source_rect.w=w;
	vh->source_rect.h=h;

	memcpy(&vh->target_rect,&vh->source_rect,sizeof(_rect));
	memcpy(&vh->image_dim,&vh->source_rect,sizeof(_rect));

	vh->avg_nsec_per_frame=(1000*1000*1000)/frame_rate;

	vh->bit_rate=bitrate;
	vh->error_bit_rate=0;

	vh->image_bpp=0;
	vh->image_pixel_format=AV_PIX_FMT_YUV420P;

	mt->format=vh;

	cm_mtype=mt;

    */
	cm_is_input=true;
}

// -----------------------------------------------------------------------------
_venc_in_pin::~_venc_in_pin()
{
}

// -----------------------------------------------------------------------------
void _venc_in_pin::start()
{
	(*cm_enc->out_pins().begin())->start();
	cm_state=started;
}

// -----------------------------------------------------------------------------
void _venc_in_pin::stop()
{
	(*cm_enc->out_pins().begin())->stop();
	cm_state=stoped;
}

// -----------------------------------------------------------------------------
void _venc_in_pin::pause()
{
	(*cm_enc->out_pins().begin())->pause();
	cm_state=paused;
}

// -----------------------------------------------------------------------------
void _venc_in_pin::push_media_sample(const _media_sample* ms)
{
	cm_enc->proccess_input_media_sample(ms);
}

// -----------------------------------------------------------------------------
void _venc_in_pin::push_stream_header(const _stream_header* sh)
{
    cm_enc->proccess_input_stream_header(sh);
}

// -----------------------------------------------------------------------------
// OUT
// -----------------------------------------------------------------------------
_venc_out_pin::_venc_out_pin(_venc *enc)
 :cm_enc(enc)
{
	cm_is_input=false;
}

// -----------------------------------------------------------------------------
_venc_out_pin::~_venc_out_pin()
{
}

// -----------------------------------------------------------------------------
void _venc_out_pin::connect(_pin *to)
{
    _pin::connect(to);

    _av_codec_context_header *acch=new _av_codec_context_header;
    acch->context=cm_enc->ccontext;

    _stream_header sh;
    sh.set_format_header(acch);

    to->push_stream_header(&sh);
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
_venc::_venc(int index):codec(NULL),ccontext(NULL),
						frame_number(0),is_open(false),cm_in_pin(NULL),cm_out_pin(NULL),
						cm_last_stamp(0)
{
    cm_out_ms.cm_btype=MEDIA_SAMPLE_BT_AV_PACKET;
    cm_out_ms.cm_mtype=MEDIA_SAMPLE_MT_VIDEO;

    avcodec_register_all();

	// find encoder
	codec = avcodec_find_encoder((AVCodecID)index);//);//AV_CODEC_ID_MPEG4););//codec_id);
	if (!codec) throw _pm_error(index,"_venc::_venc - Can not find codec.");

	// create context
	ccontext = avcodec_alloc_context3(codec);
	if (!ccontext) throw _pm_error(index,"_venc::_venc - Can not create codec context.");

    cout << "_venc::_venc - context created" << endl;
}

// -----------------------------------------------------------------------------
_venc::~_venc()
{
    //MLOG(0) << "_venc::~_venc " << endl;

    //MLOG(0) << "_venc::~_venc - delete in pin" << endl;
    if (cm_in_pin)
	{
		cm_in_pin->release();
	}
	cm_in_pins.clear();
	//MLOG(0) << "_venc::~_venc delete in pin done" << endl;


	//MLOG(0) << "_venc::~_venc - delete out pin" << endl;
    if (cm_out_pin)
	{
		// TODO - normal end_of_stream implementation
		// Comment now - infinity recurse loop
		// cm_out_pin->end_of_stream();
		cm_out_pin->release();
	}
	cm_out_pins.clear();
	//MLOG(0) << "_venc::~_venc - delete out pin done" << endl;

    //MLOG(0) << "_venc::~_venc - close encoder" << endl;
	close_encoder();
	//MLOG(0) << "_venc::~_venc - close encoder done" << endl;

	//MLOG(0) << "_venc::~_venc - free encoder" << endl;
	av_free(ccontext);
	//MLOG(0) << "_venc::~_venc - free encoder done" << endl;
}

// -----------------------------------------------------------------------------
void _venc::config(int bitrate,int w,int h,int fps,int base_frame)
{
    cout << "_venc::config" << endl;

	if (cm_in_pin)
	{
        cout << "_venc::config - delete in pin" << endl;
		cm_in_pins.clear();
		cm_in_pin->release();
	}

	if (cm_out_pin)
	{
        cout << "_venc::config - delete out pin" << endl;

		// TODO cm_out_pin->end_of_stream();
		cm_out_pins.clear();
		cm_out_pin->release();
	}

    cout << "_venc::config - close encoder" << endl;
	close_encoder();
	cout << "_venc::config - encoder closed" << endl;

	av_opt_set(ccontext->priv_data, "preset", "ultrafast", 0);//superfast", 0);
	//av_opt_set(ccontext->priv_data, "me", "dia", 0);

	//--bframes 0 --force-cfr --no-mbtree --sync-lookahead 0 --sliced-threads --rc-lookahead 0


	// put sample parameters
	ccontext->bit_rate = bitrate;

	if ((w%2) || (h%2)) throw _pm_error(0,"_venc::config - resolution must be a multiple of two");
	// resolution must be a multiple of two
	ccontext->width = w;
	ccontext->height = h;

	// frames per second
	ccontext->time_base= (AVRational){1,fps*2};//{1,fps};
	// emit one intra frame every X frames */
	ccontext->gop_size = base_frame;

	ccontext->max_b_frames=0;//1;
	ccontext->refs=4;
	ccontext->thread_type=FF_THREAD_SLICE;

	ccontext->pix_fmt = AV_PIX_FMT_YUV420P;
	// TODO - set it from settings
	//ccontext->flags|=CODEC_FLAG_GLOBAL_HEADER; - WE MUST set it if write to comlex out stream (flv or mp4 file/stream)

	open_encoder();

	// create pins
	cm_in_pin=new _venc_in_pin(this,bitrate,w,h,fps);
	cm_in_pins.push_back(cm_in_pin);

	cm_out_pin=new _venc_out_pin(this);
	cm_out_pins.push_back(cm_out_pin);

    cout << "_venc::config - pins created" << endl;
}

// -----------------------------------------------------------------------------
void _venc::open_encoder()
{
	if (is_open) throw _pm_error(0,"_venc::open - Codec is already open.");

	frame_number=0;

	// open it
	if (avcodec_open2(ccontext, codec, NULL) < 0) throw _pm_error(0,"_venc::open - Can not open codec.");

	is_open=true;
}

// -----------------------------------------------------------------------------
void _venc::close_encoder()
{
	if (!is_open) return;

    // TODO - we set this context as codec context in out - its free it
	avcodec_close(ccontext);
	is_open=false;
}


// -----------------------------------------------------------------------------
void _venc::proccess_input_stream_header(const _stream_header *sh)
{
}

// -----------------------------------------------------------------------------
void _venc::proccess_input_media_sample(const _media_sample* ms)
{
    if (ms->stream_stamp<=0) return ;
    if (ms->cm_mtype!=MEDIA_SAMPLE_MT_VIDEO) return;

    if (ms->stream_stamp<=cm_last_stamp) return;

    cm_last_stamp=ms->stream_stamp;

    AVFrame *in_frame;

    if (ms->cm_btype==MEDIA_SAMPLE_BT_AV_FRAME)
    {
        in_frame=(AVFrame *)ms->buffer;
    }
    else
    {
        throw _pm_error(0,string("_venc::proccess_input_media_sample - can not proccess media sample with type=")+to_string(ms->cm_btype));
    }

	// prepare packet
	av_init_packet(&packet);
	packet.data = NULL; // packet data will be allocated by the encoder
	packet.size = 0;

	in_frame->pts = (ms->stream_stamp*ccontext->time_base.den)/10000000;
	//cout << in_frame->pts << endl;

	// encode the image
	int encode_done;
	int ret = avcodec_encode_video2(ccontext, &packet, in_frame, &encode_done);
	if (ret < 0) throw _pm_error(ret,"_venc::proccess_frame - Can not encode frame.");

	if (encode_done)
	{
		cm_out_ms.buffer=(unsigned char *)&packet;
		cm_out_ms.stream_stamp=ms->stream_stamp;
		cm_out_ms.can_break=(packet.flags & AV_PKT_FLAG_KEY);

		cm_out_pin->push_media_sample(&cm_out_ms);

		av_free_packet(&packet);

		frame_number++;
	}
}

// -----------------------------------------------------------------------------
_video_encoder *create_video_encoder(int index)
{
    return new _venc(index);
}

// ======================================================================================
//                                           DECODER
// ======================================================================================
class _vdec;

// ----------------------------------- AVENCODER IN PIN ---------------------------------
class _vdec_in_pin:public _pin
{
	protected:
		_vdec *cm_dec;

		virtual ~_vdec_in_pin();

	public:
		_vdec_in_pin(_vdec *dec,int w,int h);

		virtual void start();
		virtual void stop();
		virtual void pause();

		virtual void push_media_sample(const _media_sample *ms);
		virtual void push_stream_header(const _stream_header *sh);

};

// ----------------------------------- AVENCODER OUT PIN ---------------------------------
class _vdec_out_pin:public _pin
{
	protected:
		_vdec *cm_dec;

		virtual ~_vdec_out_pin();
	public:

		_vdec_out_pin(_vdec *dec,int w,int h);
};

// ----------------------------------- AVENCODER ---------------------------------
class _vdec:public _video_decoder
{
	protected:

		AVCodec *codec;
		AVCodecContext *ccontext;

		AVFrame *frame;
		int frame_number;

		_media_sample cm_out_ms;

		bool is_open;

		_vdec_in_pin *cm_in_pin;
		_pins_ptr_list cm_in_pins;

		_vdec_out_pin *cm_out_pin;
		_pins_ptr_list cm_out_pins;

		virtual void open_decoder();
		virtual void close_decoder();

	public:

		_vdec(int index);
        virtual ~_vdec();

        virtual void release() { delete this; };

		virtual void config(int w,int h,int buffer_frame_cnt);


		virtual void proccess_media_sample(const _media_sample *ms);
		virtual void proccess_stream_header(const _stream_header *sh);

		virtual const _pins_ptr_list &out_pins() const { return cm_out_pins; };
		virtual const _pins_ptr_list &in_pins() const { return cm_in_pins; };
};

// -----------------------------------------------------------------------------
// PINS
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// IN
// -----------------------------------------------------------------------------
_vdec_in_pin::_vdec_in_pin(_vdec *dec,int w,int h)
 :cm_dec(dec)
{
	// set out pin header
	/*
	_media_type *mt=new _media_type;
	mt->major_type=0;
	mt->minor_type=0;
	mt->samples_are_fixed_size=0;
	mt->use_interframes=0;

	_video_header *vh=new _video_header;

	vh->source_rect.x=vh->source_rect.y=0;
	vh->source_rect.w=w;
	vh->source_rect.h=h;

	memcpy(&vh->target_rect,&vh->source_rect,sizeof(_rect));
	memcpy(&vh->image_dim,&vh->source_rect,sizeof(_rect));

	vh->avg_nsec_per_frame=(1000*1000*1000)/25;

	vh->bit_rate=0;
	vh->error_bit_rate=0;

	vh->image_bpp=0;
	vh->image_pixel_format=AV_PIX_FMT_YUV420P;

	mt->format=vh;

	cm_mtype=mt;
	*/

	cm_is_input=true;
}

// -----------------------------------------------------------------------------
_vdec_in_pin::~_vdec_in_pin()
{
}

// -----------------------------------------------------------------------------
void _vdec_in_pin::start()
{
	(*cm_dec->out_pins().begin())->start();
	cm_state=started;
}

// -----------------------------------------------------------------------------
void _vdec_in_pin::stop()
{
	(*cm_dec->out_pins().begin())->stop();
	cm_state=stoped;
}

// -----------------------------------------------------------------------------
void _vdec_in_pin::pause()
{
	(*cm_dec->out_pins().begin())->pause();
	cm_state=paused;
}

// -----------------------------------------------------------------------------
void _vdec_in_pin::push_media_sample(const _media_sample* ms)
{
	cm_dec->proccess_media_sample(ms);
}

// -----------------------------------------------------------------------------
void _vdec_in_pin::push_stream_header(const _stream_header* sh)
{
    cm_dec->proccess_stream_header(sh);
}

// -----------------------------------------------------------------------------
// OUT
// -----------------------------------------------------------------------------
_vdec_out_pin::_vdec_out_pin(_vdec *dec,int w,int h)
 :cm_dec(dec)
{
	// set out pin header
	/*
	_media_type *mt=new _media_type;
	mt->major_type=0;
	mt->minor_type=0;
	mt->samples_are_fixed_size=0;
	mt->use_interframes=0;

	_video_header *vh=new _video_header;

	vh->source_rect.x=vh->source_rect.y=0;
	vh->source_rect.w=w;
	vh->source_rect.h=h;

	memcpy(&vh->target_rect,&vh->source_rect,sizeof(_rect));
	memcpy(&vh->image_dim,&vh->source_rect,sizeof(_rect));

	vh->avg_nsec_per_frame=(1000*1000*1000)/25;

	vh->bit_rate=0;
	vh->error_bit_rate=0;

	vh->image_bpp=0;
	vh->image_pixel_format=AV_PIX_FMT_YUV420P;

	mt->format=vh;

	cm_mtype=mt;
    */
	cm_is_input=false;
}

_vdec_out_pin::~_vdec_out_pin()
{
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
_vdec::_vdec(int index):codec(NULL),ccontext(NULL),frame(NULL),
						frame_number(0),is_open(false),cm_in_pin(NULL),cm_out_pin(NULL)
{
    cm_out_ms.cm_btype=MEDIA_SAMPLE_BT_AV_FRAME;
    cm_out_ms.cm_mtype=MEDIA_SAMPLE_MT_VIDEO;

	avcodec_register_all();

	// find decoder
	codec = avcodec_find_decoder((AVCodecID)index);
	if (!codec) throw _pm_error(index,"_vdec::_vdec - Can not find codec.");

	// create context
	ccontext = avcodec_alloc_context3(codec);
	if (!ccontext) throw _pm_error(index,"_vdec::_vdec - Can not create codec context.");

	// stub - TODO set it as option while config
	if (index==AV_CODEC_ID_RAWVIDEO)
    {
         ccontext->pix_fmt=AV_PIX_FMT_YUYV422;
    }
}

// -----------------------------------------------------------------------------
_vdec::~_vdec()
{
	if (cm_in_pin)
	{
		cm_in_pin->release();
	}
	cm_in_pins.clear();

	if (cm_out_pin)
	{
		// see comments in _venc::~_venc - cm_out_pin->end_of_stream();
		cm_out_pin->release();
	}
	cm_out_pins.clear();

	if (frame)
	{
	    // crush on it av_freep(&frame->data[0]);
		//av_freep(&frame->data[0]);
		avcodec_free_frame(&frame);
	}

	close_decoder();

	av_free(ccontext);
}

// -----------------------------------------------------------------------------
void _vdec::config(int w,int h,int buffer_frame_cnt)
{
	if (frame)
	{
		av_freep(&frame->data[0]);
		avcodec_free_frame(&frame);
		frame=NULL;
	}

	if (cm_in_pin)
	{
		cm_in_pins.clear();
		cm_in_pin->release();
	}

	if (cm_out_pin)
	{
		cm_out_pin->end_of_stream();
		cm_out_pins.clear();
		cm_out_pin->release();
	}

	close_decoder();

	if ((w%2) || (h%2)) throw _pm_error(0,"_vdec::config - resolution must be a multiple of two");
	// resolution must be a multiple of two
	ccontext->width = w;
	ccontext->height = h;
	ccontext->coded_width = w;
	ccontext->coded_height = h;

	frame = av_frame_alloc();
	if (!frame) throw _pm_error(0,"_vdec::config - Can not alloc frame.");

	open_decoder();

	// create pins
	cm_in_pin=new _vdec_in_pin(this,w,h);
	cm_in_pins.push_back(cm_in_pin);

	cm_out_pin=new _vdec_out_pin(this,w,h);
	cm_out_pins.push_back(cm_out_pin);
}

// -----------------------------------------------------------------------------
void _vdec::open_decoder()
{
	if (is_open) throw _pm_error(0,"_vdec::open - Codec is already open.");

	frame_number=0;

	// open it

    /*
	// motion test
	AVDictionary *opts = NULL;
    av_dict_set(&opts, "flags2", "+export_mvs", 0);

	if (avcodec_open2(ccontext, codec, &opts) < 0) throw _pm_error(0,"_venc::open - Can not open codec.");
    */

    if (avcodec_open2(ccontext, codec, NULL) < 0) throw _pm_error(0,"_vdec::open - Can not open codec.");

	is_open=true;
}

// -----------------------------------------------------------------------------
void _vdec::close_decoder()
{
	if (!is_open) return;

	avcodec_close(ccontext);
	is_open=false;
}

// -----------------------------------------------------------------------------
void _vdec::proccess_stream_header(const _stream_header *sh)
{
    if (sh->format_header->format_type==MEDIA_FORMAT_AV_CODEC_CONTEXT)
    {
        AVCodecContext *orig_ctx=(AVCodecContext *)((_av_codec_context_header *)(sh->format_header))->context;
        //ccontext=avcodec_alloc_context3(NULL);//orig_ctx->codec);
        avcodec_copy_context(ccontext,orig_ctx);
    }
    else if (sh->format_header->format_type==MEDIA_FORMAT_PORTABLE)
    {
        cm_out_pin->push_stream_header(sh);
    }

    open_decoder();
}


// -----------------------------------------------------------------------------
void _vdec::proccess_media_sample(const _media_sample* pms)
{
    #ifdef DEBUG
    MLOG(10) << "_vdec::proccess_media_sample" << endl;
    #endif // DEBUG

    //MLOG(0) << "CM_MTYPE=" << (unsigned int)pms->cm_mtype << endl;

    if (pms->cm_mtype!=MEDIA_SAMPLE_MT_VIDEO) return ;

    AVPacket *in_packet;

    if (pms->cm_btype==MEDIA_SAMPLE_BT_AV_PACKET)
    {
        in_packet=(AVPacket *)pms->buffer;
    }
    else throw _pm_error(0,string("_vdec::proccess_media_sample - can not process media sample with type=")+to_string(pms->cm_btype));

    //MLOG(0) << "pts=" << in_packet->pts << " dts=" << in_packet->pts << " size=" << in_packet->size << " si=" << in_packet->stream_index << " flags=" << in_packet->flags << endl;

    /*
    ifstream pf;
    pf.open("p.txt",ios::out | ios::binary);
    pf.write(in_packet->data,in_packet->size);
    pf.close();
    */

	// decode
	int len, got_frame;
	len = avcodec_decode_video2(ccontext, frame, &got_frame, in_packet);
	if (len < 0) throw _pm_error(len,"_vdec::proccess_media_sample - error while decoding frame");

	if (got_frame)
	{
	    // motion test
	    /*
	    AVFrameSideData *side_data = av_frame_get_side_data(frame, AV_FRAME_DATA_MOTION_VECTORS);
	    if (side_data)
        {
            int i;
            const AVMotionVector *mvs = (const AVMotionVector *)side_data->data;
            for (i = 0; i < side_data->size / sizeof(*mvs); i++)
            {
                const AVMotionVector *mv = &mvs[i];
                const int direction = mv->source > 0;
             // do something with motion vector
            }
        }
        */

        /*
        pf.open("f.txt",ios::out | ios::binary);
        pf.write(in_packet->data,in_packet->size);
        pf.close();
        */

	    frame->pts = (pms->stream_stamp*ccontext->time_base.den)/10000000;

	    //cout << "frame->format=" << frame->format << endl;

        cm_out_ms.buffer=(unsigned char *)frame;
        cm_out_ms.stream_stamp=pms->stream_stamp;
		cm_out_ms.can_break=frame->key_frame;

		cm_out_pin->push_media_sample(&cm_out_ms);

		frame_number++;

		av_frame_unref(frame);
	}
	else
    {
        MLOG(10) << "HAVE NOT Got video frame" << endl;
    }
}

// -----------------------------------------------------------------------------
_video_decoder *create_video_decoder(int index)
{
    return new _vdec(index);
}
