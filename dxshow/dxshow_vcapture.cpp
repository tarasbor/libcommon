#include "dxshow.hpp"

#define __STDC_CONSTANT_MACROS
#define __STDC_LIMIT_MACROS
#define __STDC_FORMAT_MACROS

extern "C" {
#include <libavutil/opt.h>
#include <libavcodec/avcodec.h>
#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>
}


#ifdef WINDOWS

#include "dx_proxy_proc.hpp"

#include <iostream>


class _vcap;

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
class _vcap_out_pin:public _pin
{
	protected:
		_vcap *cm_cap;

		virtual ~_vcap_out_pin();

	public:
		_vcap_out_pin(_vcap *vc);

		virtual void start();
		virtual void pause();
		virtual void stop();

		virtual void push_media_sample(const _media_sample *ms);
		virtual void push_stream_header(const _stream_header *sh);
};

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
class _vcap:public _video_capture
{
    friend class _vcap_out_pin;

    private:

        _vcap_out_pin *cm_out_pin;
        _pins_ptr_list cm_out_pins;

        IGraphBuilder *pFilterGraphBuilder;
        ICaptureGraphBuilder2 *pCaptureGraphBuilder;

        IMediaControl *pMediaControl;

        IBaseFilter *pCaptureFilter;

        _dx_proxy *cm_dx_proxy;
        _dx_proxy_from_src_processor *cm_dx_proxy_proc;

        struct _frame_info
		{
            int w;
            int h;
		} cm_frame_info;

        AVFrame *cm_in_frame;
		AVFrame *cm_out_frame;
		SwsContext *cm_sws_ctx;

		_media_sample cm_media_sample;

        virtual void start_device_capture();
        virtual void pause_device_capture();
		virtual void stop_device_capture();

		virtual void alloc_buffs();
		virtual void free_buffs();

    public:

        _vcap(int index);
        virtual ~_vcap();

        virtual const _pins_ptr_list &out_pins() const { return cm_out_pins; };

        virtual void configure(int x,int y,int frame_rate);
        virtual void release() { delete this; };


};

// -------------------------------------------------------------------------
_vcap::_vcap(int index)
:cm_out_pin(NULL),pFilterGraphBuilder(NULL),pCaptureGraphBuilder(NULL),pMediaControl(NULL),pCaptureFilter(NULL),cm_dx_proxy(NULL),cm_dx_proxy_proc(NULL),
cm_in_frame(NULL),cm_out_frame(NULL),cm_sws_ctx(NULL)
{

    // init COM
    HRESULT hr=CoInitialize(NULL);
    if (FAILED(hr)) throw(_pm_error(hr,"_vcap::_vcap - can not initialize COM..."));

    // create GRAPH Builder
    hr = CoCreateInstance(CLSID_CaptureGraphBuilder2, NULL, CLSCTX_INPROC_SERVER, IID_ICaptureGraphBuilder2, (void**)&pCaptureGraphBuilder );
    if (FAILED(hr)) throw(_pm_error(hr,"_vcap::_vcap - can not get Capture Graph Builder interface"));

    // Create the Filter Graph Manager.
    hr = CoCreateInstance(CLSID_FilterGraph, 0, CLSCTX_INPROC_SERVER, IID_IGraphBuilder, (void**)&pFilterGraphBuilder);
    if (FAILED(hr)) throw(_pm_error(hr,"_vcap::_vcap - can not get Filter Graph Builder interface"));

    pCaptureGraphBuilder->SetFiltergraph(pFilterGraphBuilder);


    IEnumMoniker *pEnum;
    bool not_empty = _video_help::create_enumerator(CLSID_VideoInputDeviceCategory, &pEnum);

    if (!not_empty)
    {
        pEnum->Release();
        throw(_pm_error(0,"_video_capture::_video_capture - you have NOT capture devices"));
    }

    IMoniker *pMoniker = NULL;
    while (pEnum->Next(1, &pMoniker, NULL) == S_OK)
    {
        if (index==0)
        {
            HRESULT hr = pMoniker->BindToObject(0, 0, IID_IBaseFilter, (void**)&pCaptureFilter);
            if (FAILED(hr)) throw(_pm_error(hr,"_video_capture::_video_capture - can not bind Capture Filter"));

            pMoniker->Release();
            pEnum->Release();

            cm_out_pin=new _vcap_out_pin(this);
            cm_out_pins.push_back(cm_out_pin);

            return;
        }

        index--;
    }

    pMoniker->Release();
    pEnum->Release();

    throw(_pm_error(index,"_video_capture::_video_capture - can not create device with given index - no such device"));
}

// -------------------------------------------------------------------------
_vcap::~_vcap()
{
    if (pMediaControl)
    {
        pMediaControl->Stop();
        pMediaControl->Release();
    }

    if (pCaptureFilter) pCaptureFilter->Release();
    if (cm_dx_proxy) cm_dx_proxy->release();
    if (cm_dx_proxy_proc) cm_dx_proxy_proc->release();

    if (pCaptureGraphBuilder) pCaptureGraphBuilder->Release();
    //if (pFilterGraphBuilder) pFilterGraphBuilder->Release();

    cm_out_pins.clear();
    if (cm_out_pin) cm_out_pin->release();

    free_buffs();

    CoUninitialize();
}

// ------------------------------------------------------------------
void _vcap::configure(int x,int y,int frame_rate)
{
    cm_frame_info.w=x;
    cm_frame_info.h=y;

    HRESULT hr;

    IEnumPins *pPinEnum;

    hr = pCaptureFilter->EnumPins(&pPinEnum);

    IPin *pPin = NULL;
    while(pPinEnum->Next(1,&pPin,NULL)==S_OK)
    {
        PIN_INFO pi;
        pPin->QueryPinInfo(&pi);
        //printf("Pin name = %S\n",pi.achName);
        //printf("Pin direction = %i\n",pi.dir);

        IAMStreamConfig *MediaConfig;
        pPin->QueryInterface(IID_IAMStreamConfig, (void **)&MediaConfig);

        if (MediaConfig)
        {
            //printf("Have Media Config\n");

            int count = 0;
            int size = 0;
            hr = MediaConfig->GetNumberOfCapabilities(&count, &size);
            if (size==sizeof(VIDEO_STREAM_CONFIG_CAPS))
            {
                //printf("Its video stream\n");

                for (int format = 0; format < count; format++)
                {
                    VIDEO_STREAM_CONFIG_CAPS scc;
                    AM_MEDIA_TYPE *pmtConfig;
                    hr = MediaConfig->GetStreamCaps(format, &pmtConfig, (BYTE*)&scc);
                    if (SUCCEEDED(hr))
                    {
                        if (pmtConfig->subtype==MEDIASUBTYPE_YUY2)
                        {
                            VIDEOINFOHEADER *pVih = (VIDEOINFOHEADER*)pmtConfig->pbFormat;

                            cout << "CAM X=" << pVih->bmiHeader.biWidth << " W=" << pVih->bmiHeader.biHeight  << endl;

                            // TO DO - we must use scc to check ranges for x,y,and fps
                            // now use preseted mode
                            if (pVih->bmiHeader.biWidth==x && pVih->bmiHeader.biHeight==y)
                            {
                                pVih->AvgTimePerFrame=(10000000/frame_rate);
                                MediaConfig->SetFormat(pmtConfig);

                                cout << "VIDEO CAPTURE CONFIG DONE!!!" << endl;

                                DeleteMediaType(pmtConfig);

                                free_buffs();
                                alloc_buffs();

                                break;
                            }
                        }

                        DeleteMediaType(pmtConfig);
                    }
                }

            }

            MediaConfig->Release();
        }

        //printf("\n");
    }

    if (pPin) pPin->Release();
    pPinEnum->Release();
}

// -------------------------------------------------------------------------
void _vcap::start_device_capture()
{
    cout << "_vcap::start_device_capture()" << endl;
    if (pMediaControl)
    {
        pMediaControl->Run();
        return;
    }

    HRESULT hr;

    // create memory proxy filter & proxy processor
    cout << "create_dx_proxy_from_src_processor" << endl;
    cm_dx_proxy_proc=create_dx_proxy_from_src_processor(cm_out_pin);
    cout << "create_dx_proxy_from_src" << endl;
    cm_dx_proxy=create_dx_proxy_from_src(cm_dx_proxy_proc);

    cout << "pFilterGraphBuilder->AddFilter pCaptureFilter" << endl;
    hr = pFilterGraphBuilder->AddFilter(pCaptureFilter, L"Capture Filter");
    if (FAILED(hr)) throw(_pm_error(hr,"_video_capture::capture_device(set) - can not add Capture Filter to Filter Graph"));

    cout << "pFilterGraphBuilder->AddFilter cm_dx_proxy->filter_iface()" << endl;
    hr=pFilterGraphBuilder->AddFilter(cm_dx_proxy->filter_iface(),L"Render");
    if (FAILED(hr)) throw(_pm_error(hr,"_vcap::start_device_capture - can not add Render (Mem Proxy) Filter to Filter Graph"));

    cout << "pCaptureGraphBuilder->RenderStream" << endl;
    hr = pCaptureGraphBuilder->RenderStream(&PIN_CATEGORY_CAPTURE, &MEDIATYPE_Video, pCaptureFilter, NULL, cm_dx_proxy->filter_iface());
    if (FAILED(hr)) throw(_pm_error(hr,"_vcap::render - can not start render"));

    cout << "pFilterGraphBuilder->QueryInterface" << endl;
    hr = pFilterGraphBuilder->QueryInterface( IID_IMediaControl, (void **)&pMediaControl );
    if (FAILED(hr)) throw(_pm_error(hr,"_vcap::start_device_capture - can get Media Control Interface"));

    cout << "pMediaControl->Run()" << endl;
    pMediaControl->Run();
    cout << "pMediaControl->Run() done" << endl;
    cout << "_vcap::start_device_capture() done" << endl;
}

// -------------------------------------------------------------------------
void _vcap::pause_device_capture()
{
    if (pMediaControl) pMediaControl->Pause();
}

// -------------------------------------------------------------------------
void _vcap::stop_device_capture()
{
    if (pMediaControl) pMediaControl->Stop();
}

// -----------------------------------------------------------------------------
void _vcap::alloc_buffs()
{
    // TODO - detect in & out pixel format

    // in frame
	cm_in_frame=av_frame_alloc();

	av_image_alloc(cm_in_frame->data, cm_in_frame->linesize,
                                cm_frame_info.w, cm_frame_info.h,
                                (AVPixelFormat)AV_PIX_FMT_YUYV422, 32);
    cm_in_frame->width=cm_frame_info.w;
    cm_in_frame->height=cm_frame_info.h;
    cm_in_frame->format=AV_PIX_FMT_YUYV422;

    // out frame
	cm_out_frame=av_frame_alloc();

	av_image_alloc(cm_out_frame->data, cm_out_frame->linesize,
                                 cm_frame_info.w, cm_frame_info.h,
                                 (AVPixelFormat)AV_PIX_FMT_YUV420P, 32);
    cm_out_frame->width=cm_frame_info.w;
    cm_out_frame->height=cm_frame_info.h;
    cm_out_frame->format=AV_PIX_FMT_YUV420P;

    // scale context
    cm_sws_ctx=sws_getContext(cm_frame_info.w,cm_frame_info.h,(AVPixelFormat)AV_PIX_FMT_YUYV422,
                              cm_frame_info.w,cm_frame_info.w,(AVPixelFormat)AV_PIX_FMT_YUV420P,
                                      SWS_FAST_BILINEAR,
                                      NULL,NULL,0);

    cm_media_sample.cm_btype=MEDIA_SAMPLE_BT_AV_FRAME;
    cm_media_sample.cm_mtype=MEDIA_SAMPLE_MT_VIDEO;
    cm_media_sample.buffer=(unsigned char *)cm_out_frame;

}

// -----------------------------------------------------------------------------
void _vcap::free_buffs()
{
	if (cm_sws_ctx)
    {
        sws_freeContext(cm_sws_ctx);
    }

    if (cm_out_frame)
    {
        cm_media_sample.buffer=NULL;
        av_freep(&cm_out_frame->data[0]);
		avcodec_free_frame(&cm_out_frame);
		cm_out_frame=NULL;
    }

    if (cm_in_frame)
    {
        av_freep(&cm_in_frame->data[0]);
		avcodec_free_frame(&cm_in_frame);
		cm_in_frame=NULL;
    }
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
_vcap_out_pin::_vcap_out_pin(_vcap *vc)
 :cm_cap(vc)
{
}

// -----------------------------------------------------------------------------
_vcap_out_pin::~_vcap_out_pin()
{
}

// -----------------------------------------------------------------------------
void _vcap_out_pin::start()
{
	cm_cap->start_device_capture();

	_pin::start();
}

// -----------------------------------------------------------------------------
void _vcap_out_pin::pause()
{
	cm_cap->pause_device_capture();

	_pin::pause();
}

// -----------------------------------------------------------------------------
void _vcap_out_pin::stop()
{
	cm_cap->stop_device_capture();

	_pin::stop();
}

// -----------------------------------------------------------------------------
void _vcap_out_pin::push_media_sample(const _media_sample *ms)
{
    if (ms->cm_btype!=MEDIA_SAMPLE_BT_RAW) throw _pm_error(ms->cm_btype,"_vcap_out_pin::push_media_sample - input media sample is not MEDIA_SAMPLE_BT_RAW type.");

    unsigned char *data=(unsigned char *)ms->buffer;

	int y;
	int x;
	// Y
	for(y=0;y<cm_cap->cm_frame_info.h;y++)
	{
		//cout << "y=" << y << endl;

		for(x=0;x<cm_cap->cm_frame_info.w;x++)
		{
			//cout << "  x=" << x << " index=" << y*ccontext->width*2+x*2 << flush;

			cm_cap->cm_out_frame->data[0][y * cm_cap->cm_out_frame->linesize[0] + x]=data[y*(cm_cap->cm_frame_info.w<<1)+(x<<1)];

			//cout << "done" << endl;

		}

		//cout << "y=" << y << " done." << endl;

	}
	// Cb & Cr
	for(y=0;y<(cm_cap->cm_frame_info.h>>1);y++)
	{
		for(x=0;x<(cm_cap->cm_frame_info.w>>1);x++)
		{
            //cout << "x=" << x << " y=" << y << " in index=" << (y*2)*cm_frame_info.w*2+x*4+1 << endl;
			// Y Cb Y Cr
			cm_cap->cm_out_frame->data[1][y * cm_cap->cm_out_frame->linesize[1] + x] = data[y*(cm_cap->cm_frame_info.w<<2)+(x<<2)+1];
			cm_cap->cm_out_frame->data[2][y * cm_cap->cm_out_frame->linesize[2] + x] = data[y*(cm_cap->cm_frame_info.w<<2)+(x<<2)+3];
		}
		//cout << "---------------------------------" << endl;
	}



    //avpicture_fill((AVPicture *)cm_in_frame,(const uint8_t *)buffs[buf.index].start,AV_PIX_FMT_YUYV422,cm_frame_info.w,cm_frame_info.h);
    memcpy(cm_cap->cm_in_frame->data[0],(unsigned char *)data,ms->buffer_size);


    // WHY IT NOT WORK
    /*
    int ls[]={1280*2,640,640};
	int rc=sws_scale(cm_sws_ctx,&data,ls,0,cm_frame_info.h,
                          cm_out_frame->data,cm_out_frame->linesize);

    //cout << rc << endl;
    */

	cm_cap->cm_media_sample.stream_stamp=ms->stream_stamp;

	_pin::push_media_sample(&cm_cap->cm_media_sample);
}

// -----------------------------------------------------------------------------
void _vcap_out_pin::push_stream_header(const _stream_header *sh)
{
    // TODO - make portable header from given header
    _pin::push_stream_header(sh);
}


// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
_video_capture *create_video_capture(int index)
{
    return new _vcap(index);
}

// ---------------------------------------- END WIN IMP ------------------------
#else
// ------------------------------------------ V4L2 IMP -------------------------

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <fcntl.h>

#include <errno.h>
#include <string.h>
#include <stdio.h>

#include <iostream>

#include <linux/videodev2.h>

#include <clthread.hpp>
#include <dx_headers.hpp>

int xioctl(int fh, int request, void *arg)
{
        int r;

        do
		{
			r = ioctl(fh, request, arg);
        }
		while (-1 == r && EINTR == errno);

        return r;
}

class _vcap;
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
class _vc_thread:public _thread
{
	protected:
		_vcap *vc;

	public:
		_vc_thread(_vcap *v);
		virtual ~_vc_thread();

		virtual int Poll();
};

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
class _vcap_out_pin:public _pin
{
	protected:
		_vcap *cm_cap;

		virtual ~_vcap_out_pin();

	public:
		_vcap_out_pin(_vcap *vc,int w,int h,int frame_rate,const v4l2_format &fmt);

		virtual void start();
		virtual void pause();
		virtual void stop();

		virtual void push_media_sample(const _media_sample *ms);
		virtual void push_stream_header(const _stream_header *sh);
};

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
class _vcap:public _video_capture
{
	friend class _vc_thread;
	friend class _vcap_out_pin;

	private:

		int devd;

		struct _buffer
		{
			void   *start;
			size_t  length;
		};

		struct _frame_info
		{
            int w;
            int h;
		} cm_frame_info;

		_buffer *buffs;
		unsigned int buff_cnt;

		_vc_thread *vc_thread;

		AVFrame *cm_in_frame;
		AVFrame *cm_out_frame;
		SwsContext *cm_sws_ctx;

		virtual void alloc_buffs();
		virtual void free_buffs();

		virtual void start_device_capture();
		virtual void pause_device_capture();
		virtual void stop_device_capture();

		virtual int on_poll();

		_pins_ptr_list cm_out_pins;
		_pin *cm_out_pin;

		_media_sample cm_media_sample;

		int frame_cnt;

	public:
		_vcap(const string &dev_name);
		virtual ~_vcap();

		//virtual IBaseFilter *filter() { return vc_filter; };
        virtual void configure(int x,int y,int frame_rate);

		virtual void release() { delete this; };

		virtual const _pins_ptr_list &out_pins() const { return cm_out_pins; };
};

// -----------------------------------------------------------------------------
_vcap::_vcap(const string &dev_name)
:devd(-1),buff_cnt(0),buffs(NULL),vc_thread(NULL),cm_out_pin(NULL),frame_cnt(0),
cm_in_frame(NULL),cm_out_frame(NULL),cm_sws_ctx(NULL)//,vc_filter(NULL)
{
	int err;

	// 1. Open

	struct stat st;
	err=stat(dev_name.c_str(), &st);

    if (err == -1) throw _pm_error(errno,string("_vcap::_vcap - stat - Cannot identify ")+dev_name+" "+strerror(errno));

    if (!S_ISCHR(st.st_mode)) throw _pm_error(st.st_mode,string("_vcap::_vcap ")+dev_name+" is no device");

    devd = open(dev_name.c_str(), O_RDWR /* required */ | O_NONBLOCK, 0);

    if (devd == -1) throw _pm_error(errno,string("_vcap::_vcap - open - Can not open ")+dev_name+strerror(errno));

	// 2. Check

	struct v4l2_capability cap;

	err=xioctl(devd, VIDIOC_QUERYCAP, &cap);
    if (err == -1) throw _pm_error(0,"_vcap::_vcap - VIDIOC_QUERYCAP fail");

    if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE))
		throw _pm_error(0,string("_vcap::_vcap - ")+dev_name+" is no video capture device");

	// cap.capabilities & V4L2_CAP_READWRITE- TODO
	// cap.capabilities & V4L2_CAP_STREAMING - now implement ONLY this
    if (!(cap.capabilities & V4L2_CAP_STREAMING))
		throw _pm_error(0,string("_vcap::_vcap - ")+dev_name+" does not support streaming");

	// 3. Set CROP
	struct v4l2_cropcap cropcap;
    struct v4l2_crop crop;

	memset(&cropcap,0,sizeof(cropcap));
	cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	err=xioctl(devd, VIDIOC_CROPCAP, &cropcap);
	if (!err)
	{
		crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        crop.c = cropcap.defrect;					// reset to default
		xioctl(devd, VIDIOC_S_CROP, &crop);
	}
	else
	{
		// we have no crop capability - do nothing
	}
}

// -----------------------------------------------------------------------------
_vcap::~_vcap()
{
    //cout << "Stop device..." << endl;
    stop_device_capture();
    //cout << "Stop device...OK" << endl;

	//cout << "End of stream..." << endl;
	//if (cm_out_pin) cm_out_pin->end_of_stream();
	//cout << "End of stream...OK" << endl;

    //cout << "Free buffs..." << endl;
	free_buffs();
	//cout << "Free buffs...OK" << endl;

	if (devd!=-1)
	{
        //cout << "Close devd..." << endl;
		close(devd);
		devd = -1;
		//cout << "Close devd...OK" << endl;
	}

    //cout << "cm_out_pins..." << endl;
	cm_out_pins.clear();
	if (cm_out_pin) cm_out_pin->release();
	//cout << "cm_out_pins...OK" << endl;
}

// -----------------------------------------------------------------------------
void _vcap::configure(int x,int y,int frame_rate)
{
	// free mem
	free_buffs();

	if (cm_out_pin)
	{
		cm_out_pin->end_of_stream();
		cm_out_pins.clear();
		cm_out_pin->release();
	}

	// enum formats
	cout << "Support formats:" << endl;
	v4l2_fmtdesc fmt_q;
	int i=0;
	while(1)
	{
		memset(&fmt_q,0,sizeof(fmt_q));
		fmt_q.index=i;
		fmt_q.type=V4L2_BUF_TYPE_VIDEO_CAPTURE;
		int err=xioctl(devd, VIDIOC_ENUM_FMT, &fmt_q);
		if (err) if (errno==EINVAL) break;

		cout << "FMT " << i << endl;
		cout << "flags=" << fmt_q.flags << endl;
		cout << "desc=" << fmt_q.description << endl;
		cout << "pixelformat=" << fmt_q.pixelformat << endl;
		cout << endl << endl;

		i++;
	}

	cout << "Support frames (pixelformat=V4L2_PIX_FMT_YUYV):" << endl;
	v4l2_frmsizeenum frmsize_q;
	i=0;
	while(1)
	{
		memset(&frmsize_q,0,sizeof(frmsize_q));
		frmsize_q.index=i;
		frmsize_q.pixel_format=V4L2_PIX_FMT_YUYV;
		int err=xioctl(devd, VIDIOC_ENUM_FRAMESIZES, &frmsize_q);
		if (err)
		{
			cout << " Can not enum...errno=" << errno << " " << strerror(errno) << endl;
			break;
		}

		if (frmsize_q.type==V4L2_FRMSIZE_TYPE_DISCRETE)
		{
			cout << "Index " << i << endl;
			cout << "w=" << frmsize_q.discrete.width << endl;
			cout << "h=" << frmsize_q.discrete.height << endl;
			cout << endl << endl;
		}
		else if (frmsize_q.type==V4L2_FRMSIZE_TYPE_STEPWISE)
		{
			cout << "V4L2_FRMSIZE_TYPE_STEPWISE" << endl;
			cout << "width " << frmsize_q.stepwise.min_width << "-" << frmsize_q.stepwise.max_width << " step by " << frmsize_q.stepwise.step_width << endl;
			cout << "height " << frmsize_q.stepwise.min_height << "-" << frmsize_q.stepwise.max_height << " step by " << frmsize_q.stepwise.step_height << endl;
		}
		else if (frmsize_q.type==V4L2_FRMSIZE_TYPE_CONTINUOUS)
		{
			cout << "V4L2_FRMSIZE_TYPE_CONTINUOUS" << endl;
			cout << "width " << frmsize_q.stepwise.min_width << "-" << frmsize_q.stepwise.max_width << endl;
			cout << "height " << frmsize_q.stepwise.min_height << "-" << frmsize_q.stepwise.max_height << endl;
		}

		if (frmsize_q.type!=V4L2_FRMSIZE_TYPE_DISCRETE) break;
		i++;
	}


	// Select video input, video standard and tune here.
    struct v4l2_format fmt;
	int err;

	memset(&fmt,0,sizeof(fmt));

    fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	if (x>0 && y>0)
	{
		fmt.fmt.pix.width       = x;
        fmt.fmt.pix.height      = y;
        fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
        fmt.fmt.pix.field       = V4L2_FIELD_NONE;

		err=xioctl(devd, VIDIOC_S_FMT, &fmt);
        if (err == -1) throw _pm_error(errno,string("_vcap::configure - VIDIOC_S_FMT - ")+strerror(errno));

		cout << "REAL W=" << fmt.fmt.pix.width << " H=" << fmt.fmt.pix.height << " bytesperline=" << fmt.fmt.pix.bytesperline <<
		" pixformat=" << fmt.fmt.pix.pixelformat << " (V4L2_PIX_FMT_YUYV=" << V4L2_PIX_FMT_YUYV << ")" <<
		" field=" << fmt.fmt.pix.field << " (V4L2_FIELD_NONE=" << V4L2_FIELD_NONE << ")" << endl;

		cm_frame_info.w=fmt.fmt.pix.width;
		cm_frame_info.h=fmt.fmt.pix.height;
	}
	else
	{
		err=xioctl(devd, VIDIOC_G_FMT, &fmt);
		if (err == -1) throw _pm_error(errno,string("_vcap::configure - VIDIOC_G_FMT - ")+strerror(errno));
	}

	cm_out_pin=new _vcap_out_pin(this,x,y,frame_rate,fmt);
	cm_out_pins.push_back(cm_out_pin);

	alloc_buffs();
}

// -----------------------------------------------------------------------------
void _vcap::alloc_buffs()
{
	struct v4l2_requestbuffers req;

    memset(&req,0,sizeof(req));

    req.count = 4;
    req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory = V4L2_MEMORY_MMAP;

	int err=xioctl(devd, VIDIOC_REQBUFS, &req);
	if (err==-1) throw _pm_error(errno,string("_vcap::alloc_buffs - VIDIOC_REQBUFS - ")+strerror(errno));

    if (req.count < 2) throw _pm_error(req.count,"_vcap::alloc_buffs - Insufficient buffer memory.");

	buff_cnt=req.count;
	buffs=new _buffer [buff_cnt];

	int i;
    for (i=0; i<buff_cnt; i++)
	{
		struct v4l2_buffer bconf;

        memset(&bconf,0,sizeof(bconf));

        bconf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        bconf.memory      = V4L2_MEMORY_MMAP;
        bconf.index       = i;

		err=xioctl(devd, VIDIOC_QUERYBUF, &bconf);
        if (err==-1) throw _pm_error(errno,string("_vcap::alloc_buffs - VIDIOC_QUERYBUF - ")+strerror(errno));

        buffs[i].length = bconf.length;
        buffs[i].start =
                        mmap(NULL,                  /* start anywhere */
                              bconf.length,
                              PROT_READ | PROT_WRITE, /* required */
                              MAP_SHARED,             /* recommended */
                              devd, bconf.m.offset);

        if (buffs[i].start == MAP_FAILED) throw _pm_error(errno,string("_vcap::alloc_buffs - mmap - ")+strerror(errno));
	}

    // TODO - detect in & out pixel format

    // in frame
	cm_in_frame=av_frame_alloc();

	av_image_alloc(cm_in_frame->data, cm_in_frame->linesize,
                                cm_frame_info.w, cm_frame_info.h,
                                (AVPixelFormat)AV_PIX_FMT_YUYV422, 32);
    cm_in_frame->width=cm_frame_info.w;
    cm_in_frame->height=cm_frame_info.h;
    cm_in_frame->format=AV_PIX_FMT_YUYV422;

    // out frame
	cm_out_frame=av_frame_alloc();

	av_image_alloc(cm_out_frame->data, cm_out_frame->linesize,
                                 cm_frame_info.w, cm_frame_info.h,
                                 (AVPixelFormat)AV_PIX_FMT_YUV420P, 32);
    cm_out_frame->width=cm_frame_info.w;
    cm_out_frame->height=cm_frame_info.h;
    cm_out_frame->format=AV_PIX_FMT_YUV420P;

    // scale context
    cm_sws_ctx=sws_getContext(cm_frame_info.w,cm_frame_info.h,(AVPixelFormat)AV_PIX_FMT_YUYV422,
                              cm_frame_info.w,cm_frame_info.w,(AVPixelFormat)AV_PIX_FMT_YUV420P,
                                      SWS_FAST_BILINEAR,
                                      NULL,NULL,0);

    cm_media_sample.cm_btype=MEDIA_SAMPLE_BT_AV_FRAME;
    cm_media_sample.cm_mtype=MEDIA_SAMPLE_MT_VIDEO;
    cm_media_sample.buffer=(unsigned char *)cm_out_frame;

}

// -----------------------------------------------------------------------------
void _vcap::free_buffs()
{
	if (!buffs) return;

	int err;
	int i;

	for(i=0;i<buff_cnt;i++)
	{
		err=munmap(buffs[i].start, buffs[i].length);
		if (err==-1) throw _pm_error(errno,string("_vcap::free_buffs - munmap - ")+strerror(errno));
	}

	struct v4l2_requestbuffers req;
    memset(&req,0,sizeof(req));

	req.count = 0;
	req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	req.memory = V4L2_MEMORY_MMAP;

	err=xioctl(devd, VIDIOC_REQBUFS, &req);
	if (err==-1) throw _pm_error(errno,string("_vcap::free_buffs - VIDIOC_REQBUFS - ")+strerror(errno));

	buff_cnt=0;
	delete [] buffs;

	if (cm_sws_ctx)
    {
        sws_freeContext(cm_sws_ctx);
    }

    if (cm_out_frame)
    {
        cm_media_sample.buffer=NULL;
        av_freep(&cm_out_frame->data[0]);
		avcodec_free_frame(&cm_out_frame);
		cm_out_frame=NULL;
    }

    if (cm_in_frame)
    {
        av_freep(&cm_in_frame->data[0]);
		avcodec_free_frame(&cm_in_frame);
		cm_in_frame=NULL;
    }
}

// -----------------------------------------------------------------------------
void _vcap::start_device_capture()
{
	if (!buffs) throw _pm_error(0,"_vcap::start_device_capture - but not configured yet");
	if (vc_thread) throw _pm_error(0,"_vcap::start_device_capture - already started");

	int err;

	int i;
	for (i=0;i<buff_cnt;i++)
	{
		struct v4l2_buffer bufq;

		memset(&bufq,0,sizeof(bufq));

        bufq.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        bufq.memory = V4L2_MEMORY_MMAP;
        bufq.index = i;

		err=xioctl(devd, VIDIOC_QBUF, &bufq);
		if (err==-1) throw _pm_error(errno,string("_vcap::start_device_capture - VIDIOC_QBUF - ")+strerror(errno));
	}

	enum v4l2_buf_type type;
	type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	err=xioctl(devd, VIDIOC_STREAMON,&type);
    if (err==-1) throw _pm_error(errno,string("_vcap::start_device_capture - VIDIOC_STREAMON - ")+strerror(errno));

    _stream_header *sh=new _stream_header;
    _portable_header *ph=new _portable_header;

    ph->text="<header><streams>";

    ph->text+="<stream index=\"0\"";
    ph->text+="\" media_type=\"video\" source_proto=\"v4l2\"";
    ph->text+=" codec=\"RAW V4L2_PIX_FMT_YUYV\"";

    ph->text+=" w=\"";
    ph->text+=to_string(cm_frame_info.w);
    ph->text+="\"";

    ph->text+=" h=\"";
    ph->text+=to_string(cm_frame_info.h);
    ph->text+="\"";

    ph->text+="/>";

    ph->text+="</streams></header>";

	sh->set_format_header(ph);
	cm_out_pin->push_stream_header(sh);

	delete sh;

	vc_thread=new _vc_thread(this);
	vc_thread->SetSleepTime(0);
	vc_thread->Start();
}

// -----------------------------------------------------------------------------
void _vcap::pause_device_capture()
{
    stop_device_capture();
}

// -----------------------------------------------------------------------------
void _vcap::stop_device_capture()
{
    //cout << "Stop vc_thread" << endl;
	if (vc_thread)
	{
		vc_thread->Kill();
		while(vc_thread->GetStatus()!=TH_STATUS_KILLED) Sleep(10);
		delete vc_thread;
		vc_thread=NULL;
	}
	//cout << "Stop vc_thread OK" << endl;

	enum v4l2_buf_type type;

    type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	int err;

    //cout << "Stream xioctl off" << endl;
	err=xioctl(devd, VIDIOC_STREAMOFF, &type);
	//cout << "Stream xioctl off...OK" << endl;

	if (err==-1) throw _pm_error(errno,string("_vcap::stop_device_capture - VIDIOC_STREAMOFF - ")+strerror(errno));
}

// -----------------------------------------------------------------------------
int _vcap::on_poll()
{
	fd_set fds;
    struct timeval tv;
    int r;

	//cout << "frame_cnt=" << frame_cnt << endl;

	while(1)
	{
		FD_ZERO(&fds);
		FD_SET(devd, &fds);

		/* Timeout. */
		tv.tv_sec = 20;
		tv.tv_usec = 0;

		r = select(devd+1, &fds, NULL, NULL, &tv);

		if (r == -1)
		{
			if (EINTR == errno) continue;
			throw _pm_error(errno,string("_vcap::on_poll - select - ")+strerror(errno));
		}
		else if (r==0)
		{
			//cout << "frame_cnt=" << frame_cnt << endl;
			//cout << "_vcap::on_poll - select timeout" << endl;
			continue;
			//throw _pm_error(20,"_vcap::on_poll - select timeout");
		}

		break;
	}

	struct v4l2_buffer buf;
    unsigned int i;

    memset(&buf,0,sizeof(buf));

    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;

	// GET BUFFER for us
	int err;
	err=xioctl(devd, VIDIOC_DQBUF, &buf);
	if (err==-1)
	{
		if (errno==EAGAIN) return 0;
		/* Could ignore EIO, see spec. */
        /* fall through */
		if (errno==EIO) return 0;
		throw _pm_error(errno,string("_vcap::on_poll - VIDIOC_DQBUF - ")+strerror(errno));
	}

	if (buf.index>=buff_cnt) throw _pm_error(buf.index,"_vcap::on_poll - buffer index >= buffers count");

	//cout << "buf.index=" << buf.index << endl;
	frame_cnt++;

    unsigned char *data=(unsigned char *)buffs[buf.index].start;
	//memcpy(cm_in_frame->data[0],(unsigned char *)buffs[buf.index].start,buf.bytesused);

	int y;
	int x;
	// Y
	for(y=0;y<cm_frame_info.h;y++)
	{
		//cout << "y=" << y << endl;

		for(x=0;x<cm_frame_info.w;x++)
		{
			//cout << "  x=" << x << " index=" << y*ccontext->width*2+x*2 << flush;

			cm_out_frame->data[0][y * cm_out_frame->linesize[0] + x]=data[y*(cm_frame_info.w<<1)+(x<<1)];

			//cout << "done" << endl;

		}

		//cout << "y=" << y << " done." << endl;

	}
	// Cb & Cr
	for(y=0;y<(cm_frame_info.h>>1);y++)
	{
		for(x=0;x<(cm_frame_info.w>>1);x++)
		{
            //cout << "x=" << x << " y=" << y << " in index=" << (y*2)*cm_frame_info.w*2+x*4+1 << endl;
			// Y Cb Y Cr
			cm_out_frame->data[1][y * cm_out_frame->linesize[1] + x] = data[y*(cm_frame_info.w<<2)+(x<<2)+1];
			cm_out_frame->data[2][y * cm_out_frame->linesize[2] + x] = data[y*(cm_frame_info.w<<2)+(x<<2)+3];
		}
		//cout << "---------------------------------" << endl;
	}



    //avpicture_fill((AVPicture *)cm_in_frame,(const uint8_t *)buffs[buf.index].start,AV_PIX_FMT_YUYV422,cm_frame_info.w,cm_frame_info.h);
    //memcpy(cm_in_frame->data[0],(unsigned char *)data,buf.bytesused);


    // WHY IT NOT WORK
    /*
    int ls[]={1280*2,640,640};
	int rc=sws_scale(cm_sws_ctx,&data,ls,0,cm_frame_info.h,
                          cm_out_frame->data,cm_out_frame->linesize);

    //cout << rc << endl;
    */

	_time tnow;
	tnow.set_now();

	cm_media_sample.stream_stamp=tnow.stamp();

    /*
    cm_media_sample.cm_btype=MEDIA_SAMPLE_BT_RAW;
    cm_media_sample.cm_mtype=MEDIA_SAMPLE_MT_VIDEO;
	cm_media_sample.buffer=(unsigned char *)buffs[buf.index].start;
	cm_media_sample.buffer_size=buf.bytesused;
	cm_media_sample.can_break=true;
	cm_media_sample.stream_stamp=tnow.stamp();
	*/

	//cout << cm_media_sample.buffer_size << endl;

	cm_out_pin->push_media_sample(&cm_media_sample);

	// We MUST set buffer pointer to NULL to prevent free it in destructor
	//cm_media_sample.buffer=NULL;
	//}

	// FREE buffer (driver now can use it)
	err=xioctl(devd, VIDIOC_QBUF, &buf);
    if (err==-1) _pm_error(errno,string("_vcap::on_poll - VIDIOC_QBUF - ")+strerror(errno));

	return 0;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
_vc_thread::_vc_thread(_vcap* v):vc(v)
{
}

// -----------------------------------------------------------------------------
_vc_thread::~_vc_thread()
{

}

// -----------------------------------------------------------------------------
int _vc_thread::Poll()
{
	int err;
	try
	{
		err=vc->on_poll();
	}
	catch(_pm_error &e)
	{
		cout << endl << e.num() << e.str() << endl;
		return -1;
	}
	return err;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
_vcap_out_pin::_vcap_out_pin(_vcap *vc,int w,int h,int frame_rate,const v4l2_format &fmt)
 :cm_cap(vc)
{
    /*
	// set out pin header
	_media_type *mt=new _media_type;
	mt->major_type=0;
	mt->minor_type=0;
	mt->samples_are_fixed_size=0;
	mt->use_interframes=0;

	_video_header *vh=new _video_header;

	vh->source_rect.x=vh->source_rect.y=0;
	vh->source_rect.w=w;
	vh->source_rect.h=h;

	memcpy(&vh->target_rect,&vh->source_rect,sizeof(_rect));
	memcpy(&vh->image_dim,&vh->source_rect,sizeof(_rect));

	vh->avg_nsec_per_frame=(1000*1000*1000)/frame_rate;

	vh->bit_rate=0;
	vh->error_bit_rate=0;

	vh->image_bpp=0;
	vh->image_pixel_format=V4L2_PIX_FMT_YUYV;

	mt->format=vh;

	cm_mtype=mt;
	*/
}

// -----------------------------------------------------------------------------
_vcap_out_pin::~_vcap_out_pin()
{
}

// -----------------------------------------------------------------------------
void _vcap_out_pin::start()
{
	cm_cap->start_device_capture();

	_pin::start();
}

// -----------------------------------------------------------------------------
void _vcap_out_pin::stop()
{
	cm_cap->stop_device_capture();

	_pin::stop();
}

// -----------------------------------------------------------------------------
void _vcap_out_pin::pause()
{
	cm_cap->pause_device_capture();

	_pin::pause();
}


// -----------------------------------------------------------------------------
void _vcap_out_pin::push_media_sample(const _media_sample *ms)
{
	_pin::push_media_sample(ms);
}

// -----------------------------------------------------------------------------
void _vcap_out_pin::push_stream_header(const _stream_header *sh)
{
	_pin::push_stream_header(sh);
}


/*
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
_vc_filter::_vc_filter(_vcap* v)
:vc(v),cm_out(NULL)
{

}

// -----------------------------------------------------------------------------
_vc_filter::~_vc_filter()
{

}

// -----------------------------------------------------------------------------
void _vc_filter::set_output(IBaseFilter* out)
{
	cm_out=out;
}
*/

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
_video_capture *create_video_capture(int index)
{
	string dev_name="/dev/video";
	char tmp[16];
	sprintf(tmp,"%i",index);

	dev_name+=tmp;
    return new _vcap(dev_name);
}

#endif
