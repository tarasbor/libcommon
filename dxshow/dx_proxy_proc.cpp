#include "dx_proxy_proc.hpp"

#ifdef WINDOWS

#include <clthread.hpp>
//#include <Objbase.h>

extern "C" {
#include <libavutil/opt.h>
#include <libavcodec/avcodec.h>
#include <libavutil/imgutils.h>
}

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
class _proxy_proc_fs:public _dx_proxy_from_src_processor
{
    protected:
        virtual ~_proxy_proc_fs() {};

        _pin *cm_to_pin;

	public:
	    _proxy_proc_fs(_pin *to_pin):cm_to_pin(to_pin) {};

		virtual void __stdcall on_set_header(const AM_MEDIA_TYPE *mt);
		virtual void __stdcall on_render_sample(IMediaSample *pMediaSample);

		virtual void __stdcall release() { delete this; };
};

// -------------------------------------------------------------------------
void _proxy_proc_fs::on_set_header(const AM_MEDIA_TYPE *mt)
{

	/*LPOLESTR str;
	StringFromCLSID(mt->majortype,&str);

	StringFromCLSID(mt->subtype,&str);
	*/

	/*
	_stream_header *s_header = new _stream_header;


	s_header->major_type=0;//mt->Type();
	s_header->minor_type=0;//mt->Subtype();
	GUID mjt=mt->majortype;
	GUID mst=mt->subtype;

	if (mt->bFixedSizeSamples) s_header->samples_are_fixed_size=1;
	//if (mt->IsPartiallySpecified()) s_header->set_use_interframes(true); else s_header->set_use_interframes(false);

	if      (mt->formattype == FORMAT_VideoInfo)
	{
		VIDEOINFOHEADER *vi = (VIDEOINFOHEADER *)mt->pbFormat;

		_video_header *vh = new _video_header;
		//vh->avg_nsec_per_frame=;
		vh->format_type=MEDIA_FORMAT_VIDEO;

		vh->source_rect=_rect(vi->rcSource.left, vi->rcSource.top, vi->rcSource.right-vi->rcSource.left, vi->rcSource.bottom-vi->rcSource.top);
		vh->target_rect=_rect(vi->rcTarget.left, vi->rcTarget.top, vi->rcTarget.right-vi->rcTarget.left, vi->rcTarget.bottom-vi->rcTarget.top);

		vh->bit_rate=vi->dwBitRate;
		vh->error_bit_rate=vi->dwBitErrorRate;

		vh->image_dim=_rect(0,0,vi->bmiHeader.biWidth,vi->bmiHeader.biHeight);

		vh->image_bpp=vi->bmiHeader.biBitCount;
		vh->image_pixel_format=0; // TODO fix it

		s_header->set_format_header(vh);
	}
	else if (mt->formattype == FORMAT_WaveFormatEx)
	{
	    _audio_header *ah = new _audio_header;

	    WAVEFORMATEX *wi = (WAVEFORMATEX *)mt->pbFormat;

        ah->format_type=MEDIA_FORMAT_AUDIO;
		ah->channels_count=wi->nChannels;
		ah->avg_bytes_per_sec=wi->nAvgBytesPerSec;
		ah->block_align=wi->nBlockAlign;
		ah->bits_per_sample=wi->wBitsPerSample;

		s_header->set_format_header(ah);
	}

	cm_to_pin->push_stream_header(s_header);

	delete s_header;
	*/
}

// -------------------------------------------------------------------------
void _proxy_proc_fs::on_render_sample(IMediaSample *pMediaSample)
{
    if (pMediaSample->GetActualDataLength()>0)
	{
		_media_sample *s = new _media_sample;
		if (pMediaSample->IsDiscontinuity()) s->can_break=1;

		unsigned char *p;
		pMediaSample->GetPointer(&p);
		s->copy_to_buffer(p,pMediaSample->GetActualDataLength());

		_time t;
		t.set_now();
		s->stream_stamp=t.stamp();

		cm_to_pin->push_media_sample(s);

		delete s;
	}
}

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
_dx_proxy_from_src_processor *create_dx_proxy_from_src_processor(_pin *to_pin)
{
    return new _proxy_proc_fs(to_pin);
}

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------

class _proxy_proc_tt;

class _pp_tt_pin:public _pin
{
    protected:

        _proxy_proc_tt *cm_proc;

		virtual ~_pp_tt_pin() {};

	public:
		_pp_tt_pin(_proxy_proc_tt *proc):cm_proc(proc) { cm_is_input=false; };

		virtual void start() {};
		virtual void stop() { /* meybe clean media samples in proc ? */};
		virtual void pause() {};

		virtual void push_media_sample(const _media_sample *ms);
		virtual void push_stream_header(const _stream_header *sh);
};

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
class _proxy_proc_tt:public _dx_proxy_to_tar_processor
{
    friend class _pp_tt_pin;

    protected:
        virtual ~_proxy_proc_tt();

        _pin *cm_from_pin;
        _pp_tt_pin *cm_in_pin;

        _stream_header *cm_sh;

        typedef list<_media_sample *> _media_samples;
        _media_samples cm_media_samples;

        _locker cm_ms_lock;

        _time::time_val_i cm_stream_begin_at;

	public:
	    _proxy_proc_tt(_pin *from_pin);

		virtual void __stdcall on_get_header(AM_MEDIA_TYPE *mt);
		virtual void __stdcall on_get_sample(IMediaSample *pMediaSample);

		virtual void __stdcall release() { delete this; };
};

// -------------------------------------------------------------------------
_proxy_proc_tt::_proxy_proc_tt(_pin *from_pin)
:cm_from_pin(from_pin),cm_in_pin(NULL),cm_sh(NULL),cm_stream_begin_at(0)
{
    cm_in_pin = new _pp_tt_pin(this);
    cm_from_pin->connect(cm_in_pin);
}

// -------------------------------------------------------------------------
_proxy_proc_tt::~_proxy_proc_tt()
{
    cm_ms_lock.lock();

    if (cm_sh) delete cm_sh;

    _media_samples::iterator it=cm_media_samples.begin();
    while(it!=cm_media_samples.end())
    {
        delete (*it);
        it++;
    }
    cm_media_samples.clear();

    cm_ms_lock.unlock();
}

// -------------------------------------------------------------------------
void _proxy_proc_tt::on_get_header(AM_MEDIA_TYPE *mt)
{
    mt->majortype=MEDIATYPE_Audio;// {0x73647561,0x0000,0x0010,{0x80,0x00,0x00,0xaa,0x00,0x38,0x9b,0x71}};
    mt->subtype=MEDIASUBTYPE_PCM;//{0x00000001,0x0000,0x0010,{0x80,0x00,0x00,0xaa,0x00,0x38,0x9b,0x71}};
    mt->bFixedSizeSamples=TRUE;
    mt->bTemporalCompression=FALSE;
    mt->lSampleSize=2;
    mt->formattype=FORMAT_WaveFormatEx;
    mt->pUnk=NULL;
    mt->cbFormat=sizeof(WAVEFORMATEX);

    WAVEFORMATEX *wh=(WAVEFORMATEX *)CoTaskMemAlloc(sizeof(WAVEFORMATEX));
    wh->wFormatTag=WAVE_FORMAT_PCM;//1;
    wh->nChannels=1;
    wh->nSamplesPerSec=8000;
    wh->nAvgBytesPerSec=8000*1*2;
    wh->nBlockAlign=2;
    wh->wBitsPerSample=16;
    wh->cbSize=0;

    mt->pbFormat=(BYTE *)wh;
}

#include <iostream>

// -------------------------------------------------------------------------
void _proxy_proc_tt::on_get_sample(IMediaSample *pms)
{
    BYTE *pData;
    long lDataLen;

    pms->GetPointer(&pData);
    lDataLen = pms->GetSize();

    ZeroMemory(pData, lDataLen);

    cm_ms_lock.lock();

	if (cm_media_samples.empty())
	{
	    cm_ms_lock.unlock();
		pms->SetActualDataLength(0);

		// NOW we use this class for audio render only - skeep test
		//if ((*m_header->format_type()) == FORMAT_WaveFormatEx)
		//{
		//cout << "sleep..." << endl;
        Sleep(10);
		//}
		return; // NOERROR;
	}

	_media_samples::iterator mit = cm_media_samples.begin();

	int ds=(*mit)->data_size;
	//cout << "ms size =" << ds << endl;

	memcpy(pData,(*mit)->buffer,(*mit)->data_size);
	pms->SetActualDataLength((*mit)->data_size);

	_time::time_val_i sample_begin_at = (*mit)->stream_stamp;

	delete (*mit);
	cm_media_samples.pop_front();

	/*

    _time::time_val_i sample_time;

	mit = cm_media_samples.begin();
	if (mit!=cm_media_samples.end())
    {
        sample_time = ((*mit)->fill_stamp-sample_begin_at);
    }
    else
    {
        sample_time=120*1000*10;//10000000;
    }
    cout << "sample time=" << sample_time << endl;
    */


	cm_ms_lock.unlock();

    /*
	_time tplay;
	tplay.set_now();

	if (bstream)
	{
		_time::time_val_i ds=tb-bstream;
		_time::time_val_i dp=tplay.stamp()-bplay;

		if (ds<dp)
		{
			sample_time-=(dp-ds);
			if (sample_time<=0) sample_time=10000;
		}
		else if(ds>dp)
		{
			sample_time+=(ds-dp);
		}
	}
	else
	{
		bstream=tb;
		bplay=tplay.stamp();
	}

	sample_time/=10000;
	*/

	/*
	// TODO - realtime mode - set sample_time as diff from begin time of stream & current time
	//int msize=m_samples.size();
	//if (msize>5) sample_time/=(float)(msize/5);
	*/

	//REFERENCE_TIME rtStart = (LONG)sample_begin_at;//frame_time;

	//frame_time +=sample_time;

	//REFERENCE_TIME rtEnd = (LONG)sample_begin_at+sample_time;

	//pms->SetTime(&rtStart,&rtEnd);
}

// -------------------------------------------------------------------------
void _pp_tt_pin::push_media_sample(const _media_sample *ms)
{
    _media_sample *m;

    if (ms->cm_btype==MEDIA_SAMPLE_BT_RAW)
    {
        m=new _media_sample(*ms);
    }
    else if (ms->cm_btype==MEDIA_SAMPLE_BT_AV_FRAME)
    {
        m=new _media_sample;
        m->cm_mtype=ms->cm_mtype;
        m->cm_btype=MEDIA_SAMPLE_BT_RAW;

        AVFrame *frame=(AVFrame *)ms->buffer;

        m->copy_to_buffer(frame->data[0],frame->nb_samples*2);  // TODO replace 2 to normal format sample size detect
    }

    cm_proc->cm_ms_lock.lock();
    cm_proc->cm_media_samples.push_back(m);
    cm_proc->cm_ms_lock.unlock();
}

// -------------------------------------------------------------------------
void _pp_tt_pin::push_stream_header(const _stream_header *sh)
{
    cm_proc->cm_ms_lock.lock();
    if (cm_proc->cm_sh)
    {
        delete cm_proc->cm_sh;
        cm_proc->cm_sh=NULL;
    }

    cm_proc->cm_sh = new _stream_header;

    unsigned char *d=new unsigned char [sh->pack_size()];
	sh->pack(d);
	cm_proc->cm_sh->depack(d,sh->pack_size());

	delete d;

    cm_proc->cm_ms_lock.unlock();
}

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
_dx_proxy_to_tar_processor *create_dx_proxy_to_tar_processor(_pin *from_pin)
{
    return new _proxy_proc_tt(from_pin);
}


#endif
