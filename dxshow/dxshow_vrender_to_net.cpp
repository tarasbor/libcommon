#include "dxshow.hpp"

#ifdef WINDOWS

// ------------------------------------------------------------------
// ------------------------------------------------------------------
class _vren_to_net:public _video_render
{
    private:
    
        _net_render_filter *nr_filter;
        
    public:

        _vren_to_net();
        ~_vren_to_net();

        virtual IBaseFilter *filter() { if (nr_filter) return nr_filter->filter_iface(); return NULL; };
        
        virtual _media_header *media_header() { if (nr_filter) return nr_filter->media_header(); return NULL; };
        virtual _media_sample *media_sample() { if (nr_filter) return nr_filter->media_sample(); return NULL; };

        virtual void set_draw_wnd(HWND wnd,const RECT *r,int rot_angle) {};
		virtual void set_message(const char *msg,bool clear) {};

        virtual void release() { delete this; };
};

// ------------------------------------------------------------------
_vren_to_net::_vren_to_net()
    :nr_filter(NULL)
{
    nr_filter = create_net_render_filter();
}

// ------------------------------------------------------------------
_vren_to_net::~_vren_to_net()
{
    if (nr_filter) nr_filter->release();
    nr_filter = NULL;
}

// ------------------------------------------------------------------
_video_render *create_video_render_to_net()
{
    return new _vren_to_net;
}

#endif