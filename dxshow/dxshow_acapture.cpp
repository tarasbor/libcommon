#include "dxshow.hpp"
#include "dxshow_av_func.hpp"

extern "C" {
#include <libavutil/opt.h>
#include <libavcodec/avcodec.h>
}

#ifdef WINDOWS

#include "dx_proxy_proc.hpp"

#include <iostream>

using namespace std;

class _acap;

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
class _acap_out_pin:public _pin
{
	protected:
		_acap *cm_cap;

		virtual ~_acap_out_pin();

	public:
		_acap_out_pin(_acap *ac);

		virtual void start();
		virtual void stop();
		virtual void pause();

		virtual void connect(_pin *to);

		virtual void push_media_sample(const _media_sample *ms);
		virtual void push_stream_header(const _stream_header *sh);
};

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
class _acap:public _audio_capture
{
    friend class _acap_out_pin;

    protected:

        _pins_ptr_list cm_out_pins;
		_pin *cm_out_pin;

		IGraphBuilder *pFilterGraphBuilder;
        ICaptureGraphBuilder2 *pCaptureGraphBuilder;

        IMediaControl *pMediaControl;

        IBaseFilter *pCaptureFilter;

        _dx_proxy *cm_dx_proxy;
        _dx_proxy_from_src_processor *cm_dx_proxy_proc;

        struct _config
		{
			unsigned int sample_rate;
			int ch_count;
			AUDIO_PCM_FORMAT pcm_format;
			int sample_size;
			int packet_interval; // in ms
		} cm_config;

		AVFrame *cm_out_frame;
		_media_sample cm_media_sample;

		unsigned char *cm_buffer;
		int cm_buffer_len;

        virtual void start_device_capture();
        virtual void pause_device_capture();
		virtual void stop_device_capture();

    public:

        _acap(int index);
        ~_acap();

        //virtual IBaseFilter *filter() { return pCaptureFilter; };

        //virtual void configure(int ch_count,int sample_rate,int sample_size,int buff_ms_len);
        virtual void configure(int ch_count,int sample_rate,AUDIO_PCM_FORMAT pcm_format,int buff_ms_len);

        virtual void release() { delete this; };

        virtual const _pins_ptr_list &out_pins() const { return cm_out_pins; };
};

// -------------------------------------------------------------------------
_acap::_acap(int index)
    :cm_out_pin(NULL),pFilterGraphBuilder(NULL),pCaptureGraphBuilder(NULL),pMediaControl(NULL),pCaptureFilter(NULL),cm_dx_proxy(NULL),cm_dx_proxy_proc(NULL),
    cm_out_frame(NULL)
{
    // init COM
    HRESULT hr=CoInitialize(NULL);
    if (FAILED(hr)) throw(_pm_error(hr,"Can not initialize COM..."));

    // create GRAPH Builder
    hr = CoCreateInstance(CLSID_CaptureGraphBuilder2, NULL, CLSCTX_INPROC_SERVER, IID_ICaptureGraphBuilder2, (void**)&pCaptureGraphBuilder );
    if (FAILED(hr)) throw(_pm_error(hr,"Can not get Capture Graph Builder interface"));

    // Create the Filter Graph Manager.
    hr = CoCreateInstance(CLSID_FilterGraph, 0, CLSCTX_INPROC_SERVER, IID_IGraphBuilder, (void**)&pFilterGraphBuilder);
    if (FAILED(hr)) throw(_pm_error(hr,"Can not get Filter Graph Builder interface"));

    pCaptureGraphBuilder->SetFiltergraph(pFilterGraphBuilder);

    IEnumMoniker *pEnum;
    bool not_empty = _video_help::create_enumerator(CLSID_AudioInputDeviceCategory, &pEnum);

    if (!not_empty)
    {
        pEnum->Release();
        throw(_pm_error(0,"_acap::_acap - you have NOT AUDIO capture devices"));
    }

    IMoniker *pMoniker = NULL;
    while (pEnum->Next(1, &pMoniker, NULL) == S_OK)
    {
        if (index==0)
        {
            HRESULT hr = pMoniker->BindToObject(0, 0, IID_IBaseFilter, (void**)&pCaptureFilter);
            if (FAILED(hr)) throw(_pm_error(hr,"_acap::_acap - can not bind Capture Filter"));

            hr = pFilterGraphBuilder->AddFilter(pCaptureFilter, L"Audio Capture Filter");
            if (FAILED(hr)) throw(_pm_error(hr,"_audio_capture::_audio_capture - can not add Capture Filter to Filter Graph"));

            pMoniker->Release();
            pEnum->Release();

            cm_out_frame=av_frame_alloc();

            cm_media_sample.cm_btype=MEDIA_SAMPLE_BT_AV_FRAME;
            cm_media_sample.cm_mtype=MEDIA_SAMPLE_MT_AUDIO;
            cm_media_sample.can_break=true;
            cm_media_sample.buffer=(unsigned char *)cm_out_frame;
            cm_media_sample.data_size=0;

            return;
        }

        index--;
    }

    pMoniker->Release();
    pEnum->Release();

    throw(_pm_error(index,"_acap::_acap - can not create device with given index - no such device"));
}

// -------------------------------------------------------------------------
_acap::~_acap()
{
    if (pMediaControl)
    {
        pMediaControl->Stop();
        pMediaControl->Release();
    }

    if (pCaptureFilter) pCaptureFilter->Release();
    if (cm_dx_proxy) cm_dx_proxy->release();
    if (cm_dx_proxy_proc) cm_dx_proxy_proc->release();

    if (pCaptureGraphBuilder) pCaptureGraphBuilder->Release();
    //if (pFilterGraphBuilder) pFilterGraphBuilder->Release();

    cm_out_pins.clear();
    if (cm_out_pin) cm_out_pin->release();

    CoUninitialize();

    if (cm_out_frame)
    {
        cm_media_sample.buffer=NULL;
		avcodec_free_frame(&cm_out_frame);
		cm_out_frame=NULL;
    }
}

// ------------------------------------------------------------------
void _acap::configure(int ch_count,int sample_rate,AUDIO_PCM_FORMAT pcm_format,int buff_ms_len)
{
    cm_config.packet_interval=buff_ms_len;

    // get pin (Get First)
    IEnumPins *pEnumPins;
    pCaptureFilter->EnumPins(&pEnumPins);
    IPin *pPin;
    pEnumPins->Next(1, &pPin, 0);
    pEnumPins->Release();

    // config stream params
    IAMStreamConfig *pStrConf;
    HRESULT hr = pPin->QueryInterface(IID_IAMStreamConfig,(void **)&pStrConf);
    if (FAILED(hr)) throw(_pm_error(hr,"_audio_capture::_audio_capture - can not get stream config interface"));

    int sample_size = get_sample_size_by_pcm_format(pcm_format);

    AM_MEDIA_TYPE mt;
    mt.majortype=MEDIATYPE_Audio;
    mt.subtype=MEDIASUBTYPE_PCM;
    mt.bFixedSizeSamples=TRUE;
    mt.bTemporalCompression=FALSE;
    mt.lSampleSize=sample_size;
    mt.formattype=FORMAT_WaveFormatEx;
    mt.pUnk=NULL;
    mt.cbFormat=sizeof(WAVEFORMATEX);

    WAVEFORMATEX *wh=(WAVEFORMATEX *)CoTaskMemAlloc(sizeof(WAVEFORMATEX));
    wh->wFormatTag=WAVE_FORMAT_PCM;//1;
    wh->nChannels=ch_count;
    wh->nSamplesPerSec=sample_rate;
    wh->nBlockAlign=ch_count * sample_size;
    wh->nAvgBytesPerSec=sample_rate * wh->nBlockAlign;
    wh->wBitsPerSample=sample_size*8;
    wh->cbSize=0;

    mt.pbFormat=(BYTE *)wh;

    hr = pStrConf->SetFormat(&mt);
    if (FAILED(hr)) throw(_pm_error(hr,"_audio_capture::_audio_capture - can not set stream config format"));

    pStrConf->Release();

    // set buffer len
    IAMBufferNegotiation *pBufN;
    hr = pPin->QueryInterface(IID_IAMBufferNegotiation,(void **)&pBufN);
    if (FAILED(hr)) throw(_pm_error(hr,"_audio_capture::_audio_capture - can not set buffer size for pin"));

    ALLOCATOR_PROPERTIES ap;
    ap.cbAlign=-1;
    ap.cbPrefix=-1;
    ap.cBuffers=-1;
    ap.cbBuffer=(sample_rate * sample_size * ch_count * buff_ms_len)/1000;
    hr = pBufN->SuggestAllocatorProperties(&ap);
    if (FAILED(hr)) throw(_pm_error(hr,"_audio_capture::_audio_capture - can not set stream buffer size"));

    pBufN->Release();

    cm_out_pin=new _acap_out_pin(this);
    cm_out_pins.push_back(cm_out_pin);

    cm_config.ch_count=ch_count;
	cm_config.sample_rate=sample_rate;
	cm_config.pcm_format=pcm_format;
	cm_config.sample_size=get_sample_size_by_pcm_format(pcm_format);

	int frame_bit_len=(cm_config.sample_size*8)*cm_config.ch_count;
	int bit_len=cm_config.sample_rate*frame_bit_len;
	int byte_len=bit_len/8;
	int pack_len=byte_len*cm_config.packet_interval/1000;
	int pack_frames_count=cm_config.sample_rate*cm_config.packet_interval/1000;

	cm_out_frame->nb_samples=pack_frames_count/cm_config.ch_count; // by peer channel
	cm_out_frame->format=get_av_format_by_pcm_format(cm_config.pcm_format);

	cm_out_frame->sample_rate=cm_config.sample_rate;
	cm_out_frame->channel_layout=av_get_default_channel_layout(cm_config.ch_count);
	cm_out_frame->channels=cm_config.ch_count;

    int samples_nb_size = av_samples_get_buffer_size(NULL, cm_out_frame->channels, cm_out_frame->nb_samples,
                                                     (AVSampleFormat)cm_out_frame->format, 0);
    cm_buffer = (uint8_t *)av_malloc(samples_nb_size);
    cm_buffer_len=samples_nb_size;
    if (!cm_buffer) throw (_pm_error(0,string("_acap::configure - Could not allocate ")+to_string(samples_nb_size)+" bytes for samples buffer"));

    //* setup the data pointers in the AVFrame
    int ret = avcodec_fill_audio_frame(cm_out_frame, cm_out_frame->channels, (AVSampleFormat)cm_out_frame->format,
                                            cm_buffer, samples_nb_size, 0);
    if (ret < 0) throw(_pm_error(ret,"_acap::configure - Could not setup audio frame"));
}

// -----------------------------------------------------------------------------
void _acap::start_device_capture()
{
    if (pMediaControl)
    {
        pMediaControl->Run();
        return;
    }

    HRESULT hr;

    // create memory proxy filter & proxy processor
    cm_dx_proxy_proc=create_dx_proxy_from_src_processor(cm_out_pin);
    cm_dx_proxy=create_dx_proxy_from_src(cm_dx_proxy_proc);

    hr=pFilterGraphBuilder->AddFilter(cm_dx_proxy->filter_iface(),L"Render");
    if (FAILED(hr)) throw(_pm_error(hr,"_audio_capture::start_device_capture - can not add Render (Mem Proxy) Filter to Filter Graph"));

    hr = pCaptureGraphBuilder->RenderStream(&PIN_CATEGORY_CAPTURE, &MEDIATYPE_Audio, pCaptureFilter, NULL, cm_dx_proxy->filter_iface());
    if (FAILED(hr)) throw(_pm_error(hr,"_audio_pipeline::render - can not start preview render"));

    hr = pFilterGraphBuilder->QueryInterface( IID_IMediaControl, (void **)&pMediaControl );
    if (FAILED(hr)) throw(_pm_error(hr,"_audio_capture::start_device_capture - can get Media Control Interface"));

    //

    pMediaControl->Run();
}

// -----------------------------------------------------------------------------
void _acap::stop_device_capture()
{
    if (pMediaControl) pMediaControl->Stop();
}

// -----------------------------------------------------------------------------
void _acap::pause_device_capture()
{
    if (pMediaControl) pMediaControl->Pause();
}


// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
_acap_out_pin::_acap_out_pin(_acap *ac)
 :cm_cap(ac)
{
}

// -----------------------------------------------------------------------------
_acap_out_pin::~_acap_out_pin()
{
}

// -----------------------------------------------------------------------------
void _acap_out_pin::start()
{
	cm_cap->start_device_capture();

	_pin::start();
}

// -----------------------------------------------------------------------------
void _acap_out_pin::pause()
{
	cm_cap->pause_device_capture();

	_pin::pause();
}

// -----------------------------------------------------------------------------
void _acap_out_pin::stop()
{
	cm_cap->stop_device_capture();

	_pin::stop();
}

// -----------------------------------------------------------------------------
void _acap_out_pin::push_media_sample(const _media_sample *ms)
{
	//cout << "_acap_out_pin::push_media_sample" << endl;

	// we get media sample from proxy in RAW format
	// we need convert it to AV Frame format

	memcpy(cm_cap->cm_buffer,ms->buffer,ms->data_size);
    cm_cap->cm_media_sample.stream_stamp=ms->stream_stamp;
	cm_cap->cm_media_sample.buffer=(unsigned char *)cm_cap->cm_out_frame;

    _pin::push_media_sample(&cm_cap->cm_media_sample);

   	cm_cap->cm_media_sample.buffer=NULL;

}

// -----------------------------------------------------------------------------
void _acap_out_pin::push_stream_header(const _stream_header *sh)
{
    cout << "_acap_out_pin::push_stream_header" << endl;
    _pin::push_stream_header(sh);
}

// -----------------------------------------------------------------------------
void _acap_out_pin::connect(_pin *to)
{
    _pin::connect(to);

    _stream_header *sh=new _stream_header;
    _portable_header *ph=new _portable_header;

    ph->text="<header><streams>";

    ph->text+="<stream index=\"0\" media_type=\"audio\" source_proto=\"asound\" codec=\"RAW\"";

    ph->text+=" ch_count=\"";
    ph->text+=to_string(cm_cap->cm_config.ch_count);
    ph->text+="\"";

    ph->text+=" sample_rate=\"";
    ph->text+=to_string(cm_cap->cm_config.sample_rate);
    ph->text+="\"";

    ph->text+=" sample_format=\"";
    ph->text+=get_str_name_by_pcm_format(cm_cap->cm_config.pcm_format);
    ph->text+="\"";

    ph->text+=" interleave=\"1\"";

    // TODO - real interleave & fromat values

    ph->text+="/>";

    ph->text+="</streams></header>";

	sh->set_format_header(ph);

	to->push_stream_header(sh);

	delete sh;
}


// -------------------------------------------------------------------------
_audio_capture *create_audio_capture(int index)
{
    return new _acap(index);
}

// ---------------------------------------- END WIN IMP ------------------------
#else
// ------------------------------------------ ALSA IMP -------------------------

#include <errno.h>
#include <string.h>
#include <stdio.h>

#include <iostream>
#include <fstream>
#include <alsa/asoundlib.h>
#include <clthread.hpp>

class _acap;
class _acap_out_pin;

// -----------------------------------------------------------------------------
// help functions
// -----------------------------------------------------------------------------
snd_pcm_format_t    get_alsa_format_by_pcm_format(AUDIO_PCM_FORMAT pcm_format);

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
struct _pcm_alsa_drec
{
    AUDIO_PCM_FORMAT pcm;
    snd_pcm_format_t alsa;
};

_pcm_alsa_drec pcm_alsa_dict[]=
{
  { AUDIO_PCM_FORMAT_S8, SND_PCM_FORMAT_S8 },
  { AUDIO_PCM_FORMAT_U8, SND_PCM_FORMAT_U8 },
  { AUDIO_PCM_FORMAT_S16_LE, SND_PCM_FORMAT_S16_LE },
  { AUDIO_PCM_FORMAT_S16_BE, SND_PCM_FORMAT_S16_BE },
  { AUDIO_PCM_FORMAT_U16_LE, SND_PCM_FORMAT_U16_LE },
  { AUDIO_PCM_FORMAT_U16_BE, SND_PCM_FORMAT_U16_BE },
  { AUDIO_PCM_FORMAT_S24_LE, SND_PCM_FORMAT_S24_LE },
  { AUDIO_PCM_FORMAT_S24_BE, SND_PCM_FORMAT_S24_BE },
  { AUDIO_PCM_FORMAT_U24_LE, SND_PCM_FORMAT_U24_LE },
  { AUDIO_PCM_FORMAT_U24_BE, SND_PCM_FORMAT_U24_BE },
  { AUDIO_PCM_FORMAT_S32_LE, SND_PCM_FORMAT_S32_LE },
  { AUDIO_PCM_FORMAT_S32_BE, SND_PCM_FORMAT_S32_BE },
  { AUDIO_PCM_FORMAT_U32_LE, SND_PCM_FORMAT_U32_LE },
  { AUDIO_PCM_FORMAT_U32_BE, SND_PCM_FORMAT_U32_BE },
  { AUDIO_PCM_FORMAT_FLOAT_LE, SND_PCM_FORMAT_FLOAT_LE },
  { AUDIO_PCM_FORMAT_FLOAT_BE, SND_PCM_FORMAT_FLOAT_BE },
  { AUDIO_PCM_FORMAT_FLOAT64_LE, SND_PCM_FORMAT_FLOAT64_LE },
  { AUDIO_PCM_FORMAT_FLOAT64_BE, SND_PCM_FORMAT_FLOAT64_BE },
  { AUDIO_PCM_FORMAT_IEC958_SUBFRAME_LE, SND_PCM_FORMAT_IEC958_SUBFRAME_LE },
  { AUDIO_PCM_FORMAT_IEC958_SUBFRAME_BE, SND_PCM_FORMAT_IEC958_SUBFRAME_BE },
  { AUDIO_PCM_FORMAT_MU_LAW, SND_PCM_FORMAT_MU_LAW },
  { AUDIO_PCM_FORMAT_A_LAW, SND_PCM_FORMAT_A_LAW },
  { AUDIO_PCM_FORMAT_IMA_ADPCM, SND_PCM_FORMAT_IMA_ADPCM },
  { AUDIO_PCM_FORMAT_MPEG, SND_PCM_FORMAT_MPEG },
  { AUDIO_PCM_FORMAT_GSM, SND_PCM_FORMAT_GSM },
  { AUDIO_PCM_FORMAT_SPECIAL, SND_PCM_FORMAT_SPECIAL },
  { AUDIO_PCM_FORMAT_S24_3LE, SND_PCM_FORMAT_S24_3LE },
  { AUDIO_PCM_FORMAT_S24_3BE, SND_PCM_FORMAT_S24_3BE },
  { AUDIO_PCM_FORMAT_U24_3LE, SND_PCM_FORMAT_U24_3LE },
  { AUDIO_PCM_FORMAT_U24_3BE, SND_PCM_FORMAT_U24_3BE },
  { AUDIO_PCM_FORMAT_S20_3LE, SND_PCM_FORMAT_S20_3LE },
  { AUDIO_PCM_FORMAT_S20_3BE, SND_PCM_FORMAT_S20_3BE },
  { AUDIO_PCM_FORMAT_U20_3LE, SND_PCM_FORMAT_U20_3LE },
  { AUDIO_PCM_FORMAT_U20_3BE, SND_PCM_FORMAT_U20_3BE },
  { AUDIO_PCM_FORMAT_S18_3LE, SND_PCM_FORMAT_S18_3LE },
  { AUDIO_PCM_FORMAT_S18_3BE, SND_PCM_FORMAT_S18_3BE },
  { AUDIO_PCM_FORMAT_U18_3LE, SND_PCM_FORMAT_U18_3LE },
  { AUDIO_PCM_FORMAT_U18_3BE, SND_PCM_FORMAT_U18_3BE },
  /*
  { AUDIO_PCM_FORMAT_G723_24, SND_PCM_FORMAT_G723_24 },
  { AUDIO_PCM_FORMAT_G723_24_1B, SND_PCM_FORMAT_G723_24_1B },
  { AUDIO_PCM_FORMAT_G723_40, SND_PCM_FORMAT_G723_40 },
  { AUDIO_PCM_FORMAT_G723_40_1B, SND_PCM_FORMAT_G723_40_1B },
  { AUDIO_PCM_FORMAT_DSD_U8, SND_PCM_FORMAT_DSD_U8 },
  { AUDIO_PCM_FORMAT_DSD_U16_LE, SND_PCM_FORMAT_DSD_U16_LE },
  { AUDIO_PCM_FORMAT_DSD_U32_LE, SND_PCM_FORMAT_DSD_U32_LE },
  { AUDIO_PCM_FORMAT_DSD_U16_BE, SND_PCM_FORMAT_DSD_U16_BE },
  { AUDIO_PCM_FORMAT_DSD_U32_BE, SND_PCM_FORMAT_DSD_U32_BE },
  */
  { AUDIO_PCM_FORMAT_UNKNOWN, SND_PCM_FORMAT_UNKNOWN }
};

// -----------------------------------------------------------------------------
snd_pcm_format_t    get_alsa_format_by_pcm_format(AUDIO_PCM_FORMAT pcm_format)
{
    for(int i=0;;i++)
    {
        if (pcm_alsa_dict[i].pcm==pcm_format || pcm_alsa_dict[i].pcm==AUDIO_PCM_FORMAT_UNKNOWN) return pcm_alsa_dict[i].alsa;
    }
    return SND_PCM_FORMAT_UNKNOWN;
}


// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
class _ac_thread:public _thread
{
	protected:
		_acap *ac;

	public:
		_ac_thread(_acap *v);
		virtual ~_ac_thread();

		virtual int Poll();
};

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
class _acap:public _audio_capture
{
	friend class _ac_thread;
	friend class _acap_out_pin;

	private:

		string dev_name;
		snd_pcm_t *dev_h;

		_ac_thread *ac_thread;

		virtual void start_device_capture();
		virtual void stop_device_capture();

		virtual int on_poll();

		_pins_ptr_list cm_out_pins;
		_pin *cm_out_pin;

		unsigned char *cm_buffer;
		int cm_buffer_len;
		int cm_buffer_frames_count;   // how much frames in buffer

		AVFrame *cm_out_frame;

		struct _config
		{
			unsigned int sample_rate;
			int ch_count;
			AUDIO_PCM_FORMAT pcm_format;
			int sample_size;
			snd_pcm_access_t access_type;
			snd_pcm_format_t alsa_format;
			int packet_interval; // in ms
		} cm_config;

		_media_sample cm_media_sample;

		_time prev;
		int all;
		_time tstart;

		//int frame_cnt;

		//ofstream f;

	public:
		_acap(const string &dev_name);
		virtual ~_acap();

        virtual void configure(int ch_count,int sample_rate,AUDIO_PCM_FORMAT pcm_format,int buff_ms_len);
		//virtual void configure(int sample_rate,int ch_count,bool interleave,int packet_interval_ms);

		virtual void release() { delete this; };

		virtual const _pins_ptr_list &out_pins() const { return cm_out_pins; };
};

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
class _acap_out_pin:public _pin
{
	protected:
		_acap *cm_cap;

		virtual ~_acap_out_pin();

	public:
		_acap_out_pin(_acap *vc,const _acap::_config &conf);

		virtual void start();
		virtual void stop();

		//virtual void push_media_sample(const _media_sample *ms);
		//virtual void push_stream_header(const _stream_header *sh);

		virtual void connect(_pin *to);
};


// -----------------------------------------------------------------------------
_acap::_acap(const string &dname):dev_name(dname),dev_h(NULL),ac_thread(NULL),cm_out_pin(NULL),cm_buffer(NULL),cm_buffer_len(0),cm_out_frame(NULL)
{
	all=0;
	cm_config.sample_rate=0;

	int err;

	err = snd_pcm_open (&dev_h, dev_name.c_str(), SND_PCM_STREAM_CAPTURE, 0);
	if (err < 0)
		throw _pm_error(err,string("_acap::_acap - cannot open audio device - ")+snd_strerror(err));

    //f.open("acap.pcm",ios::out | ios::binary);

    cm_out_frame=av_frame_alloc();

    cm_media_sample.cm_btype=MEDIA_SAMPLE_BT_AV_FRAME;
    cm_media_sample.cm_mtype=MEDIA_SAMPLE_MT_AUDIO;
    cm_media_sample.can_break=true;
    cm_media_sample.buffer=(unsigned char *)cm_out_frame;
    cm_media_sample.data_size=0;
}

// -----------------------------------------------------------------------------
_acap::~_acap()
{
	stop_device_capture();

	int err;
	err=snd_pcm_close (dev_h);
	if (err < 0)
		throw _pm_error(err,string("_acap::~_acap - cannot close device handle - ")+snd_strerror(err));

	//if (cm_out_pin) cm_out_pin->end_of_stream();

	cm_out_pins.clear();
	if (cm_out_pin) cm_out_pin->release();

    if (cm_buffer)
    {
        av_freep(cm_buffer);
        cm_buffer=NULL;
    }

	if (cm_out_frame)
    {
        cm_media_sample.buffer=NULL;
        //av_freep(&cm_out_frame->data[0]);
		avcodec_free_frame(&cm_out_frame);
		cm_out_frame=NULL;
    }

	//f.close();
}

// -----------------------------------------------------------------------------
void _acap::configure(int ch_count,int sample_rate,AUDIO_PCM_FORMAT pcm_format,int buff_ms_len)
// ch_count - channels count
// sample_rate - how much samples per second
// sample_size - how much bytes per sample (per channel)
// buff_ms_len - len of capture buffer (in ms)
{
	if (cm_out_pin)
	{
		cm_out_pin->end_of_stream();
		cm_out_pins.clear();
		cm_out_pin->release();
	}

	if (cm_buffer)
	{
		delete cm_buffer;
		cm_buffer_len=0;
	}

	cm_config.ch_count=ch_count;
	cm_config.sample_rate=sample_rate;
	cm_config.pcm_format=pcm_format;
	cm_config.sample_size=get_sample_size_by_pcm_format(pcm_format);
	cm_config.alsa_format=get_alsa_format_by_pcm_format(pcm_format);

	cm_config.access_type=SND_PCM_ACCESS_RW_INTERLEAVED;
	/*if (ch_count==1)
        cm_config.access_type=SND_PCM_ACCESS_RW_INTERLEAVED;
	else
        cm_config.access_type=SND_PCM_ACCESS_RW_NONINTERLEAVED;
        */

	//if (!interleave) cm_config.access_type=SND_PCM_ACCESS_RW_NONINTERLEAVED;

	cm_config.packet_interval=buff_ms_len;

	int err;

	// configure
	snd_pcm_hw_params_t *hw_params;

	err = snd_pcm_hw_params_malloc (&hw_params);
	if (err < 0)
		throw _pm_error(err,string("_acap::configure - cannot allocate hardware parameter structure - ")+snd_strerror(err));

	err = snd_pcm_hw_params_any (dev_h, hw_params);
	if (err < 0)
		throw _pm_error(err,string("_acap::configure - cannot initialize hardware parameter structure - ")+snd_strerror(err));

	err = snd_pcm_hw_params_set_access (dev_h, hw_params, cm_config.access_type);
	if (err < 0)
		throw _pm_error(err,string("_acap::configure - cannot set access type - ")+snd_strerror(err));

	err = snd_pcm_hw_params_set_format (dev_h, hw_params, cm_config.alsa_format);
	if (err < 0)
		throw _pm_error(err,string("_acap::configure - cannot set sample format - ")+snd_strerror(err));

	err = snd_pcm_hw_params_set_rate_near (dev_h, hw_params, &cm_config.sample_rate, 0);
	if (err < 0)
		throw _pm_error(err,string("_acap::configure - cannot set sample rate - ")+snd_strerror(err));

	err = snd_pcm_hw_params_set_channels (dev_h, hw_params, cm_config.ch_count);
	if (err < 0)
		throw _pm_error(err,string("_acap::configure - cannot set channel count - ")+snd_strerror(err));

	err = snd_pcm_hw_params (dev_h, hw_params);
	if (err < 0)
		throw _pm_error(err,string("_acap::configure - cannot set parameters - ")+snd_strerror(err));

	snd_pcm_hw_params_free (hw_params);

	int frame_bit_len=(cm_config.sample_size*8)*cm_config.ch_count;
	int bit_len=cm_config.sample_rate*frame_bit_len;
	int byte_len=bit_len/8;
	int pack_len=byte_len*cm_config.packet_interval/1000;
	int pack_frames_count=cm_config.sample_rate*cm_config.packet_interval/1000;

	//cm_buffer=new unsigned char [pack_len];
	//cm_buffer_len=pack_len;
	cm_buffer_frames_count=pack_frames_count;


	 cout << "Sample Rate=" << cm_config.sample_rate << " bit_len=" << bit_len <<
			" byte_len=" << byte_len << " plen=" << pack_len <<
			" cm_buffer_frames_count=" << cm_buffer_frames_count << endl;


	cm_out_pin=new _acap_out_pin(this,cm_config);
	cm_out_pins.push_back(cm_out_pin);

	cm_out_frame->nb_samples=pack_frames_count/cm_config.ch_count; // by peer channel
	cm_out_frame->format=get_av_format_by_pcm_format(cm_config.pcm_format);
	/*
	if      (sample_size==1) cm_out_frame->format=AV_SAMPLE_FMT_U8;
	else if (sample_size==2) cm_out_frame->format=AV_SAMPLE_FMT_S16;
	else if (sample_size==3) throw(_pm_error(0,"_acap::configure - Could not setup audio frame - sample size = 3 (24bit) is not suppported"));
	else if (sample_size==4) cm_out_frame->format=AV_SAMPLE_FMT_S32;
	*/

	cm_out_frame->sample_rate=cm_config.sample_rate;
	cm_out_frame->channel_layout=av_get_default_channel_layout(cm_config.ch_count);
	cm_out_frame->channels=cm_config.ch_count;

    int samples_nb_size = av_samples_get_buffer_size(NULL, cm_out_frame->channels, cm_out_frame->nb_samples,
                                                     (AVSampleFormat)cm_out_frame->format, 0);
    cm_buffer = (uint8_t *)av_malloc(samples_nb_size);
    cm_buffer_len=samples_nb_size;
    if (!cm_buffer) throw (_pm_error(0,string("_acap::configure - Could not allocate ")+to_string(samples_nb_size)+" bytes for samples buffer"));

    //* setup the data pointers in the AVFrame
    int ret = avcodec_fill_audio_frame(cm_out_frame, cm_out_frame->channels, (AVSampleFormat)cm_out_frame->format,
                                            cm_buffer, samples_nb_size, 0);
    if (ret < 0) throw(_pm_error(ret,"_acap::configure - Could not setup audio frame"));
}

// -----------------------------------------------------------------------------
void _acap::start_device_capture()
{
	if (!cm_config.sample_rate) throw _pm_error(0,"_acap::start_device_capture - but not configured yet");
	if (ac_thread) throw _pm_error(0,"_acap::start_device_capture - already started");

	int err;

	// prepare
	err = snd_pcm_prepare (dev_h);
	if (err < 0)
		throw _pm_error(err,string("_acap::start_device_capture - cannot prepare audio interface for use - ")+snd_strerror(err));

	tstart.set_now();

	//of.open("zzz.dat",ios::out | ios::binary);

	ac_thread=new _ac_thread(this);
	ac_thread->SetSleepTime(100);
	ac_thread->Start();
}

// -----------------------------------------------------------------------------
void _acap::stop_device_capture()
{
	if (ac_thread)
	{
		ac_thread->Kill();
		while(ac_thread->GetStatus()!=TH_STATUS_KILLED) Sleep(10);
		delete ac_thread;
		ac_thread=NULL;
	}

	//of.close();
}

// -----------------------------------------------------------------------------
int _acap::on_poll()
{
	int frames_read=snd_pcm_readi(dev_h, cm_buffer, cm_buffer_frames_count); // must be snd_pcm_readi for interleave
	if (frames_read<0)
	{
      snd_pcm_prepare(dev_h);
      cout << "_acap::on_poll - <<<<<<<<<<<<<<< Buffer Overrun >>>>>>>>>>>>>>>" << endl;
      return 0;
    }

	if (frames_read<cm_buffer_frames_count)
	{
		cout << "_acap::on_poll - frames_read<cm_buffer_frames_count" << endl;
	}

	_time tnow;
	tnow.set_now();


	//
	//cm_media_sample.data_size=cm_buffer_len;
	cm_media_sample.stream_stamp=tnow.stamp();
	cm_media_sample.buffer=(unsigned char *)cm_out_frame;

	all+=cm_buffer_len;
	//cout << tnow.stamp() << " " << frames_read << " " << (tnow.stamp()-prev.stamp())/10000 << " " << (tnow.stamp()-tstart.stamp())/10000 << " all=" << all << endl;
	prev=tnow;

    //cout << tnow.stamp() << " capture - push sample" << endl;
	cm_out_pin->push_media_sample(&cm_media_sample);

	// We MUST set buffer pointer to NULL to prevent free it in destructor
	cm_media_sample.buffer=NULL;

	//f.write((char *)cm_buffer,cm_buffer_len);

	return 0;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
_ac_thread::_ac_thread(_acap* v):ac(v)
{
}

// -----------------------------------------------------------------------------
_ac_thread::~_ac_thread()
{

}

// -----------------------------------------------------------------------------
int _ac_thread::Poll()
{
	int err;
	try
	{
		err=ac->on_poll();
	}
	catch(_pm_error &e)
	{
		cout << endl << e.num() << e.str() << endl;
		return -1;
	}
	return err;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
_acap_out_pin::_acap_out_pin(_acap *ac,const _acap::_config &conf)
 :cm_cap(ac)
{
}

// -----------------------------------------------------------------------------
_acap_out_pin::~_acap_out_pin()
{
}

// -----------------------------------------------------------------------------
void _acap_out_pin::start()
{
	cm_cap->start_device_capture();

	_pin::start();
}

// -----------------------------------------------------------------------------
void _acap_out_pin::stop()
{
	cm_cap->stop_device_capture();

	_pin::stop();
}

/*
// -----------------------------------------------------------------------------
void _acap_out_pin::push_media_sample(const _media_sample *ms)
{
	_pin::push_media_sample(ms);
}

// -----------------------------------------------------------------------------
void _acap_out_pin::push_stream_header(const _stream_header *sh)
{
    _pin::push_stream_header(sh);
}
*/

void _acap_out_pin::connect(_pin *to)
{
    _pin::connect(to);

    _stream_header *sh=new _stream_header;
    _portable_header *ph=new _portable_header;

    ph->text="<header><streams>";

    ph->text+="<stream index=\"0\" media_type=\"audio\" source_proto=\"asound\" codec=\"RAW\"";

    ph->text+=" ch_count=\"";
    ph->text+=to_string(cm_cap->cm_config.ch_count);
    ph->text+="\"";

    ph->text+=" sample_rate=\"";
    ph->text+=to_string(cm_cap->cm_config.sample_rate);
    ph->text+="\"";

    ph->text+=" sample_format=\"";
    ph->text+=get_str_name_by_pcm_format(cm_cap->cm_config.pcm_format);
    ph->text+="\"";

    ph->text+=" interleave=\"1\"";

    // TODO - real interleave & fromat values

    ph->text+="/>";

    ph->text+="</streams></header>";

	sh->set_format_header(ph);

	to->push_stream_header(sh);

	delete sh;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
_audio_capture *create_audio_capture(const string &dev_name)
{
	return new _acap(dev_name);
}

_audio_capture *create_audio_capture(int index)
{
	return new _acap("default");
}


#endif

