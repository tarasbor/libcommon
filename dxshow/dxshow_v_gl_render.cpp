#include "dxshow.hpp"

#ifdef WINDOWS

// ------------------------------------------------------------------
// ------------------------------------------------------------------
class _v_gl_ren:public _video_render
{
    private:
    
        _gl_render_filter *gl_filter;
        
    public:

        _v_gl_ren();
        virtual ~_v_gl_ren();

        virtual IBaseFilter *filter() { if (gl_filter) return gl_filter->filter_iface(); return NULL; };
        
        virtual _media_header *media_header() { return NULL; };
        virtual _media_sample *media_sample() { return NULL; };

        virtual void set_draw_wnd(HWND wnd,const RECT *r,int rot_angle) { if (gl_filter) gl_filter->set_wnd(wnd,r,rot_angle); };
		virtual void set_message(const char *msg,bool clear) { if (gl_filter) gl_filter->set_message(msg,clear); };

        virtual void release() { delete this; };
};

// ------------------------------------------------------------------
_v_gl_ren::_v_gl_ren()
    :gl_filter(NULL)
{
    gl_filter = create_gl_render_filter();
}

// ------------------------------------------------------------------
_v_gl_ren::~_v_gl_ren()
{
    if (gl_filter) gl_filter->release();
    gl_filter = NULL;
}

// ------------------------------------------------------------------
_video_render *create_video_gl_render()
{
    return new _v_gl_ren;
}

#endif