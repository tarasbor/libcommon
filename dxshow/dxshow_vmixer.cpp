#include "dxshow_vmixer.hpp"

#define __STDC_CONSTANT_MACROS
#define __STDC_LIMIT_MACROS
#define __STDC_FORMAT_MACROS

extern "C" {
#include <libavutil/opt.h>
#include <libavcodec/avcodec.h>
#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>
}

#include <clthread.hpp>
#include <iostream>

using namespace std;

// ======================================================================================
//                                           VIDEO MIXER
// ======================================================================================
class _vmix;
class _vmix_out_pin;

// ----------------------------------- MIXER IN PIN ---------------------------------
class _vmix_in_pin:public _pin
{
	protected:
		_vmix *cm_mix;

		_rect cm_dst_rec;
		_rect cm_img_rec;

		bool cm_need_scale;
		bool cm_sink;

		AVFrame *cm_out_frame;
		_media_sample *cm_out_ms;

		int cm_frame_cnt;
        SwsContext *cm_sws_ctx;

		virtual ~_vmix_in_pin();

		_locker cm_lock;

	public:
		_vmix_in_pin(_vmix *mix,const _rect &dst_rec,bool sink);

		virtual void start();
		virtual void stop();
		virtual void pause();

		virtual void push_media_sample(const _media_sample *ms);
		virtual void push_stream_header(const _stream_header *sh);

		void to_mixer(_vmix_out_pin *out_pin);
};

// ----------------------------------- MIXER OUT PIN ---------------------------------
class _vmix_out_pin:public _pin
{
	protected:
		_vmix *cm_mix;

		_rect cm_img_rec;

		AVFrame *cm_out_frame;
		_media_sample *cm_out_ms;

		_time::time_val_i cm_fill_stamp;

		virtual ~_vmix_out_pin();
	public:

		_vmix_out_pin(_vmix *mix,const _rect &img_rec);

		void add_img(const _rect &rec,const _media_sample *ims,bool sink);
		void flush() { push_media_sample(cm_out_ms); };
};

// ----------------------------------- MIXER ---------------------------------
class _vmix:public _vmixer
{
	protected:

		_pins_ptr_list cm_in_pins;
        _pins_ptr_list cm_out_pins;

		virtual ~_vmix();

	public:

		_vmix();

        virtual void release() { delete this; };

        virtual _pin *create_input_pin(const _rect &dst_rec,bool sink);
        virtual _pin *create_output_pin(const _rect &img_rec);

		virtual const _pins_ptr_list &out_pins() const { return cm_out_pins; };
		virtual const _pins_ptr_list &in_pins() const { return cm_in_pins; };
};

// -----------------------------------------------------------------------------
// PINS
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// IN
// -----------------------------------------------------------------------------
_vmix_in_pin::_vmix_in_pin(_vmix *mix,const _rect &dst_rec,bool sink)
 :cm_mix(mix),cm_dst_rec(dst_rec),cm_sink(sink),cm_frame_cnt(0),cm_sws_ctx(NULL),cm_need_scale(false),cm_out_frame(NULL),cm_out_ms(NULL)
{
    if ((dst_rec.w%2) || (dst_rec.h%2)) throw _pm_error(0,"_vmix_in_pin::_vmix_in_pin - dst resolution must be a multiple of two");
	// set out pin header
	/*
	_media_type *mt=new _media_type;
	mt->major_type=0;
	mt->minor_type=0;
	mt->samples_are_fixed_size=0;
	mt->use_interframes=0;

	_video_header *vh=new _video_header;

	vh->source_rect.x=vh->source_rect.y=0;
	vh->source_rect.w=w;
	vh->source_rect.h=h;

	memcpy(&vh->target_rect,&vh->source_rect,sizeof(_rect));
	memcpy(&vh->image_dim,&vh->source_rect,sizeof(_rect));

	vh->avg_nsec_per_frame=(1000*1000*1000)/frame_rate;

	vh->bit_rate=bitrate;
	vh->error_bit_rate=0;

	vh->image_bpp=0;
	vh->image_pixel_format=AV_PIX_FMT_YUV420P;

	mt->format=vh;

	cm_mtype=mt;

    */
	cm_is_input=true;
}

// -----------------------------------------------------------------------------
_vmix_in_pin::~_vmix_in_pin()
{
    if (cm_sws_ctx)
    {
        sws_freeContext(cm_sws_ctx);
    }

    if (cm_out_frame)
    {
        av_freep(&cm_out_frame->data[0]);
		avcodec_free_frame(&cm_out_frame);
    }

    if (cm_out_ms)
    {
        cm_out_ms->buffer=NULL;
        delete cm_out_ms;
    }
}

// -----------------------------------------------------------------------------
void _vmix_in_pin::start()
{
    _pin_ptr_const_it oit=cm_mix->out_pins().begin();
    if (cm_sink)
    {
        while(oit!=cm_mix->out_pins().end())
        {
            (*oit)->start();
            oit++;
        }
    }
	cm_state=started;
}

// -----------------------------------------------------------------------------
void _vmix_in_pin::stop()
{
    cout << "_vmix_in_pin::stop" << endl;
	if (cm_sink)
    {
        _pin_ptr_const_it oit=cm_mix->out_pins().begin();
        while(oit!=cm_mix->out_pins().end())
        {
            (*oit)->stop();
            oit++;
        }
    }
    cm_state=stoped;
}

// -----------------------------------------------------------------------------
void _vmix_in_pin::pause()
{
	if (cm_sink)
    {
        _pin_ptr_const_it oit=cm_mix->out_pins().begin();
        while(oit!=cm_mix->out_pins().end())
        {
            (*oit)->pause();
            oit++;
        }
    }
    cm_state=paused;
}

// -----------------------------------------------------------------------------
void _vmix_in_pin::push_media_sample(const _media_sample* ms)
{
    if (ms->cm_btype!=MEDIA_SAMPLE_BT_AV_FRAME) throw _pm_error(0,string("_vmix_in_pin::push_media_sample - can not process media sample with type=")+to_string(ms->cm_btype));
    cm_lock.lock();

    AVFrame *in_frame=(AVFrame *)ms->buffer;

    if (cm_frame_cnt==0)
    {
        cm_out_ms = new _media_sample;

        cm_img_rec.x=0;
        cm_img_rec.y=0;
        cm_img_rec.w=in_frame->width;
        cm_img_rec.h=in_frame->height;

        if (cm_img_rec.w!=cm_dst_rec.w || cm_img_rec.h!=cm_dst_rec.h)
        {
            cm_need_scale=true;
            cm_sws_ctx=sws_getContext(cm_img_rec.w,cm_img_rec.h,(AVPixelFormat)in_frame->format,
                                      cm_dst_rec.w,cm_dst_rec.h,(AVPixelFormat)in_frame->format,
                                      SWS_BICUBIC,
                                      NULL,NULL,0);
            if (!cm_sws_ctx)
            {
                cm_lock.unlock();
                throw _pm_error(0,"_vmix_in_pin::push_media_sample - can not create sws Context");
            }
        }

        cm_out_frame = av_frame_alloc();
        if (!cm_out_frame)
        {
            cm_lock.unlock();
            throw _pm_error(0,"_vmix_in_pin::push_media_sample - Can not alloc frame.");
        }

        cm_out_ms->cm_btype=MEDIA_SAMPLE_BT_AV_FRAME;
        cm_out_ms->cm_mtype=MEDIA_SAMPLE_MT_VIDEO;
        cm_out_ms->buffer=(unsigned char *)cm_out_frame;

        cm_out_frame->format = in_frame->format;
        cm_out_frame->width = cm_dst_rec.w;
        cm_out_frame->height = cm_dst_rec.h;

        int ret = av_image_alloc(cm_out_frame->data, cm_out_frame->linesize,
                                 cm_out_frame->width, cm_out_frame->height,
                                 (AVPixelFormat)cm_out_frame->format, 32);
        if (ret < 0)
        {
            cm_lock.unlock();
            throw _pm_error(0,"_vmix_in_pin::push_media_sample - Can not alloc raw pic buffer.");
        }
    }

    if (cm_need_scale)
    {
        sws_scale(cm_sws_ctx,in_frame->data,in_frame->linesize,0,in_frame->height,
                          cm_out_frame->data,cm_out_frame->linesize);
    }
    else
    {
        // TODO - use
        // av_frame_copy(cm_out_frame,in_frame);
        // need new ffmpeg lib

        av_image_copy(cm_out_frame->data,cm_out_frame->linesize,
                      (const uint8_t **)in_frame->data,in_frame->linesize,
                      (AVPixelFormat)cm_out_frame->format,
                      cm_out_frame->width,cm_out_frame->height);
    }

    cm_out_ms->stream_stamp=ms->stream_stamp;
    cm_out_frame->pts = in_frame->pts;

	cm_frame_cnt++;

	if (cm_sink)
    {
        _pin_ptr_const_it oit=cm_mix->out_pins().begin();
        while(oit!=cm_mix->out_pins().end())
        {
            _pin_ptr_const_it iit=cm_mix->in_pins().begin();
            while(iit!=cm_mix->in_pins().end())
            {
                ((_vmix_in_pin *)(*iit))->to_mixer((_vmix_out_pin *)(*oit));
                iit++;
            }

            ((_vmix_out_pin *)(*oit))->flush();

            oit++;
        }
    }

    cm_lock.unlock();
}

// -----------------------------------------------------------------------------
void _vmix_in_pin::push_stream_header(const _stream_header* sh)
{
    /*
    cm_enc->proccess_stream_header(sh);
    //cm_enc->config(sh->)
    */
}

// -----------------------------------------------------------------------------
void _vmix_in_pin::to_mixer(_vmix_out_pin *out_pin)
{
    cm_lock.lock();
    out_pin->add_img(cm_dst_rec,cm_out_ms,cm_sink);
    cm_lock.unlock();
}

// -----------------------------------------------------------------------------
// OUT
// -----------------------------------------------------------------------------
_vmix_out_pin::_vmix_out_pin(_vmix *mix,const _rect &img_rec)
 :cm_mix(mix),cm_img_rec(img_rec),cm_out_frame(NULL),cm_out_ms(NULL)
{
	// set out pin header
	/*
	_media_type *mt=new _media_type;
	mt->major_type=0;
	mt->minor_type=0;
	mt->samples_are_fixed_size=0;
	mt->use_interframes=0;

	_video_header *vh=new _video_header;

	vh->source_rect.x=vh->source_rect.y=0;
	vh->source_rect.w=w;
	vh->source_rect.h=h;

	memcpy(&vh->target_rect,&vh->source_rect,sizeof(_rect));
	memcpy(&vh->image_dim,&vh->source_rect,sizeof(_rect));

	vh->avg_nsec_per_frame=(1000*1000*1000)/frame_rate;

	vh->bit_rate=bitrate;
	vh->error_bit_rate=0;

	vh->image_bpp=0;
	vh->image_pixel_format=AV_PIX_FMT_YUV420P;

	mt->format=vh;

	cm_mtype=mt;
	*/
	cm_out_ms = new _media_sample;
	cm_out_ms->can_break=0;

	cm_is_input=false;
}

// -----------------------------------------------------------------------------
_vmix_out_pin::~_vmix_out_pin()
{
    if (cm_out_frame)
    {
        av_freep(&cm_out_frame->data[0]);
		avcodec_free_frame(&cm_out_frame);
    }

    if (cm_out_ms)
    {
        cm_out_ms->buffer=NULL;
        delete cm_out_ms;
    }
}

// -----------------------------------------------------------------------------
void _vmix_out_pin::add_img(const _rect &rec,const _media_sample *ms,bool sink)
{
    if (!ms) return;
    if (!ms->buffer) return;

    AVFrame *in_frame = (AVFrame *)ms->buffer;

    //cout << "add img " << rec.w << " " << ms->stream_stamp << " " << sink << endl;

    if (!cm_out_frame)
    {
        cm_out_frame = av_frame_alloc();
        if (!cm_out_frame)
        {
            throw _pm_error(0,"_vmix_out_pin::add_img - Can not alloc frame.");
        }

        cm_out_ms->cm_btype=MEDIA_SAMPLE_BT_AV_FRAME;
        cm_out_ms->cm_mtype=MEDIA_SAMPLE_MT_VIDEO;
        cm_out_ms->buffer=(unsigned char *)cm_out_frame;

        cm_out_frame->format = in_frame->format;
        cm_out_frame->width = cm_img_rec.w;
        cm_out_frame->height = cm_img_rec.h;

        int ret = av_image_alloc(cm_out_frame->data, cm_out_frame->linesize,
                                 cm_out_frame->width, cm_out_frame->height,
                                 (AVPixelFormat)cm_out_frame->format, 32);
        if (ret < 0)
        {
            throw _pm_error(0,"_vmix_out_pin::add_img - Can not alloc raw pic buffer.");
        }
    }

    // copy
    if (rec.x==0 && rec.y==0 && rec.w==cm_img_rec.w && rec.h==cm_img_rec.h)
    {
        av_image_copy(cm_out_frame->data,cm_out_frame->linesize,
                      (const uint8_t **)in_frame->data,in_frame->linesize,
                      (AVPixelFormat)cm_out_frame->format,
                      cm_out_frame->width,cm_out_frame->height);
    }
    else
    {

        // YUV 420p only
        // Y
        int y;
        int x;
        for(y=0;y<in_frame->height;y++)
        {
            memcpy(cm_out_frame->data[0]+(rec.y+y)*cm_out_frame->linesize[0]+rec.x,
                   in_frame->data[0]+y * in_frame->linesize[0],
                   in_frame->width);
        }
        // Cb & Cr
        for(y=0;y<in_frame->height/2;y++)
        {
            memcpy(cm_out_frame->data[1]+(rec.y/2+y)*cm_out_frame->linesize[1]+rec.x/2,
                   in_frame->data[1]+y * in_frame->linesize[1],
                   in_frame->width/2);
            memcpy(cm_out_frame->data[2]+(rec.y/2+y)*cm_out_frame->linesize[2]+rec.x/2,
                   in_frame->data[2]+y * in_frame->linesize[2],
                   in_frame->width/2);
        }
    }

    if (sink)
    {
        cm_out_ms->stream_stamp=ms->stream_stamp;
    }
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
_vmix::_vmix()
{
}

// -----------------------------------------------------------------------------
_vmix::~_vmix()
{
    _pin_ptr_it it=cm_in_pins.begin();
    while(it!=cm_in_pins.end())
    {
        (*it)->release();
        it++;
    }
    cm_in_pins.clear();

    it=cm_out_pins.begin();
    while(it!=cm_out_pins.end())
    {
        (*it)->release();
        it++;
    }
    cm_out_pins.clear();
}

// -----------------------------------------------------------------------------
_pin *_vmix::create_input_pin(const _rect &dst_rec,bool sink)
{
    _vmix_in_pin *ip=new _vmix_in_pin(this,dst_rec,sink);
    cm_in_pins.push_back(ip);
    return ip;
}

// -----------------------------------------------------------------------------
_pin *_vmix::create_output_pin(const _rect &img_rec)
{
    _vmix_out_pin *op=new _vmix_out_pin(this,img_rec);
    cm_out_pins.push_back(op);
    return op;
}

// -----------------------------------------------------------------------------
_vmixer *create_vmixer()
{
    return new _vmix();
}
