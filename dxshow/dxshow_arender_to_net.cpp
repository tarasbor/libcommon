#include "dxshow.hpp"

#ifdef WINDOWS

// ------------------------------------------------------------------
// ------------------------------------------------------------------
class _aren_to_net:public _audio_render
{
    private:
    
        _net_render_filter *nr_filter;
        
    public:

        _aren_to_net();
        ~_aren_to_net();

        virtual IBaseFilter *filter() { if (nr_filter) return nr_filter->filter_iface(); return NULL; };
        
        virtual _media_header *media_header() { if (nr_filter) return nr_filter->media_header(); return NULL; };
        virtual _media_sample *media_sample() { if (nr_filter) return nr_filter->media_sample(); return NULL; };

        virtual void release() { delete this; };
};

// ------------------------------------------------------------------
_aren_to_net::_aren_to_net()
    :nr_filter(NULL)
{
    nr_filter = create_net_render_filter();
}

// ------------------------------------------------------------------
_aren_to_net::~_aren_to_net()
{
    if (nr_filter) nr_filter->release();
    nr_filter = NULL;
}

// ------------------------------------------------------------------
_audio_render *create_audio_render_to_net()
{
    return new _aren_to_net;
}

#endif