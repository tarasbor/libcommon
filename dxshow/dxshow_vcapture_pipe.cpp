#include "dxshow.hpp"

#ifdef WINDOWS

// ------------------------------------------------------------------
// ------------------------------------------------------------------
class _vcap_pipe:public _video_capture_pipe
{
    private:

        IGraphBuilder *pFilterGraphBuilder;
        ICaptureGraphBuilder2 *pCaptureGraphBuilder;

        IMediaControl *control;

        _video_capture *capture;
        _video_encoder *encoder;
        _video_render *render;
        _video_render *preview;

        //bool _need_preview;

        void init_graph_builders();
        
    public:

        _vcap_pipe();
        ~_vcap_pipe();

        virtual void set_capture_device(_video_capture *vc);
        virtual _video_capture *capture_device() { return capture; };

        virtual void set_video_encoder(_video_encoder *e);
        virtual _video_encoder *video_encoder() { return encoder; };

        virtual void set_video_render(_video_render *r);
        virtual _video_render *video_render() { return render; };

        virtual void set_video_preview(_video_render *pr);
        virtual _video_render *video_preview() { return preview; };

        virtual IGraphBuilder *graph() { return pFilterGraphBuilder; };

        virtual void start();
        virtual void stop();
        virtual void pause();

        virtual void release() { delete this; };
};

// ------------------------------------------------------------------
// ------------------------------------------------------------------
_vcap_pipe::_vcap_pipe()
    :pFilterGraphBuilder(NULL),pCaptureGraphBuilder(NULL),control(NULL),
    encoder(NULL),capture(NULL),render(NULL),preview(NULL)
{
    // Initialize the COM library.
    HRESULT hr = CoInitialize(NULL);
    if (FAILED(hr))
    {
        throw(_video_capture_error(hr,"Can not initialize COM library."));
    }

    init_graph_builders();
}

// ------------------------------------------------------------------
_vcap_pipe::~_vcap_pipe()
{
    if (control)
    {
        control->Stop();
        control->Release();
    }
    
    if (encoder) encoder->release();
    if (capture) capture->release();
    
    if (pFilterGraphBuilder) pFilterGraphBuilder->Release();
    if (pCaptureGraphBuilder) pCaptureGraphBuilder->Release();

    //if (render) render->release();
    render = NULL;

    //if (preview) preview->release();
    preview = NULL;

    CoUninitialize();
}

// ------------------------------------------------------------------
void _vcap_pipe::init_graph_builders()
{
    HRESULT hr = CoCreateInstance(CLSID_CaptureGraphBuilder2, NULL, CLSCTX_INPROC_SERVER, IID_ICaptureGraphBuilder2, (void**)&pCaptureGraphBuilder );
    if (FAILED(hr)) throw(_video_capture_error(hr,"Can not get Capture Graph Builder interface"));
    
    // Create the Filter Graph Manager.
    hr = CoCreateInstance(CLSID_FilterGraph, 0, CLSCTX_INPROC_SERVER, IID_IGraphBuilder, (void**)&pFilterGraphBuilder);
    if (FAILED(hr)) throw(_video_capture_error(hr,"Can not get Filter Graph Builder interface"));
    
    pCaptureGraphBuilder->SetFiltergraph(pFilterGraphBuilder);
}

// ------------------------------------------------------------------
void _vcap_pipe::set_capture_device(_video_capture *vc)
{
    HRESULT hr;
    
    if (capture)
    {
        hr = pFilterGraphBuilder->RemoveFilter(capture->filter());
        if (FAILED(hr)) throw(_video_capture_error(hr,"_video_pipeline::capture_device(set) - can not remove prev Capture Filter from Filter Graph"));
        
        capture->release();
    }

    capture = vc;

    if (capture)
    {
        hr = pFilterGraphBuilder->AddFilter(capture->filter(), L"Capture Filter");
        if (FAILED(hr)) throw(_video_capture_error(hr,"_video_pipeline::capture_device(set) - can not add Capture Filter to Filter Graph"));
    }
}

// ------------------------------------------------------------------
void _vcap_pipe::set_video_encoder(_video_encoder *e)
{
    HRESULT hr;
    
    if (encoder)
    {
        hr = pFilterGraphBuilder->RemoveFilter(encoder->filter());
        if (FAILED(hr)) throw(_video_capture_error(hr,"_video_pipeline::video_encoder(set) - can not remove prev Encoder Filter from Filter Graph"));
        
        encoder->release();
    }

    encoder = e;

    if (encoder)
    {
        hr = pFilterGraphBuilder->AddFilter(encoder->filter(), L"Encoder");
        if (FAILED(hr)) throw(_video_capture_error(hr,"_video_pipeline::video_encoder(set) - can not add Encoder Filter to Filter Graph"));
    }
}

// ------------------------------------------------------------------
void _vcap_pipe::set_video_render(_video_render *r)
{
    HRESULT hr;
    
    if (render)
    {
        hr = pFilterGraphBuilder->RemoveFilter(render->filter());
        if (FAILED(hr)) throw(_video_capture_error(hr,"_video_pipeline::video_render(set) - can not remove prev Render Filter from Filter Graph"));
        
        render->release();
    }

    render = r;

    if (render)
    {
        hr = pFilterGraphBuilder->AddFilter(render->filter(), L"Render");
        if (FAILED(hr)) throw(_video_capture_error(hr,"_video_pipeline::video_render(set) - can not add Render Filter to Filter Graph"));
    }
}

// ------------------------------------------------------------------
void _vcap_pipe::set_video_preview(_video_render *pr)
{
    HRESULT hr;
    
    if (preview)
    {
        hr = pFilterGraphBuilder->RemoveFilter(preview->filter());
        if (FAILED(hr)) throw(_video_capture_error(hr,"_video_pipeline::video_preview(set) - can not remove prev Render Filter from Filter Graph"));
        
        preview->release();
    }

    preview = pr;

    if (preview)
    {
        hr = pFilterGraphBuilder->AddFilter(preview->filter(), L"Preview Render");
        if (FAILED(hr)) throw(_video_capture_error(hr,"_video_pipeline::video_preview(set) - can not add Render Filter to Filter Graph"));
    }
}


// ------------------------------------------------------------------
void _vcap_pipe::start()
{
    if (control)
    {
        control->Run();
        return;
    }
        
    HRESULT hr;

    hr = pCaptureGraphBuilder->RenderStream(&PIN_CATEGORY_CAPTURE, &MEDIATYPE_Video, capture->filter(), encoder->filter(), render->filter());
    if (FAILED(hr)) throw(_video_capture_error(hr,"_video_pipeline::render - can not start preview render"));

    if (preview)
    {
        hr = pCaptureGraphBuilder->RenderStream(&PIN_CATEGORY_PREVIEW, &MEDIATYPE_Video, capture->filter(), NULL, preview->filter());
        if (FAILED(hr)) throw(_video_capture_error(hr,"_video_pipeline::render - can not start preview render"));
    }

    if (FAILED(hr)) throw(_video_capture_error(hr,"_video_pipeline::start - can render capture stream"));

    hr = pFilterGraphBuilder->QueryInterface( IID_IMediaControl, (void **)&control );
    if (FAILED(hr)) throw(_video_capture_error(hr,"_video_pipeline::start - can get Media Control Interface"));
    
    control->Run();
}

// ------------------------------------------------------------------
void _vcap_pipe::stop()
{
    if (control) control->Stop();
}

// ------------------------------------------------------------------
void _vcap_pipe::pause()
{
    if (control) control->Pause();
}


// ------------------------------------------------------------------
_video_capture_pipe *create_video_capture_pipe()
{
    return new _vcap_pipe;
}

#else     // *nix

class _vcap_pipe:public _video_capture_pipe
{
	protected:
		
		_video_capture *capture;
        _video_encoder *encoder;
        _video_render *render;
        _video_render *preview;
		
	public:

        _vcap_pipe();
        ~_vcap_pipe();

        virtual void set_capture_device(_video_capture *vc);
        virtual _video_capture *capture_device() { return capture; };

        virtual void set_video_encoder(_video_encoder *e);
        virtual _video_encoder *video_encoder() { return encoder; };

        virtual void set_video_render(_video_render *r);
        virtual _video_render *video_render() { return render; };

        virtual void set_video_preview(_video_render *pr);
        virtual _video_render *video_preview() { return preview; };

        //virtual IGraphBuilder *graph() { return NULL; }; // stub

        virtual void start();
        virtual void stop();
        virtual void pause();

        virtual void release() { delete this; };
};

// ------------------------------------------------------------------
// ------------------------------------------------------------------
_vcap_pipe::_vcap_pipe()
    :encoder(NULL),capture(NULL),render(NULL),preview(NULL)
{
}

// ------------------------------------------------------------------
_vcap_pipe::~_vcap_pipe()
{
    /*
	if (control)
    {
        control->Stop();
        control->Release();
    }
	*/ 
	
	// it is NOT good idea release objects in this place
    
	/*
    if (encoder) encoder->release();
    if (capture) capture->release();
        
    if (render) render->release();
    render = NULL;

    if (preview) preview->release();
    preview = NULL;
	*/     
}

// ------------------------------------------------------------------
void _vcap_pipe::set_capture_device(_video_capture *vc)
{
	
}

// ------------------------------------------------------------------
void _vcap_pipe::set_video_encoder(_video_encoder *e)
{
}

// ------------------------------------------------------------------
void _vcap_pipe::set_video_render(_video_render *r)
{
}

// ------------------------------------------------------------------
void _vcap_pipe::set_video_preview(_video_render *pr)
{
}

// ------------------------------------------------------------------
void _vcap_pipe::start()
{
}

// ------------------------------------------------------------------
void _vcap_pipe::stop()
{
}

// ------------------------------------------------------------------
void _vcap_pipe::pause()
{
}

// ------------------------------------------------------------------
_video_capture_pipe *create_video_capture_pipe()
{
	return new _vcap_pipe;
}

#endif