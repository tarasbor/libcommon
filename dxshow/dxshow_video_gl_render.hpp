#ifndef __DXSHOW_VIDEO_GL_RENDER__
#define __DXSHOW_VIDEO_GL_RENDER__

#include "dxshow.hpp"
#include <ghelp_gl.hpp>

_video_render *create_video_gl_render(_gh::_gl_wnd *mw,const _gh::_rect &r);

#endif // __DXSHOW_VIDEO_GL_RENDER__
