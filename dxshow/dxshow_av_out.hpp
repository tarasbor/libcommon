#ifndef __DXSHOW_AV_OUT__
#define __DXSHOW_AV_OUT__

#include "dxshow.hpp"

class _av_out:public _media_target
{
    protected:
        virtual ~_av_out() {};

    public:
        _av_out() {};

        virtual void release() = 0;

        virtual _pin *create_input_pin() = 0;

        virtual void uri(const char *uri) = 0;
		virtual const char *uri() const = 0;
};

_av_out *create_av_out(const std::string &guess_format);

#endif // __DXSHOW_AV_OUT__
