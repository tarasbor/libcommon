#ifndef __NET_RENDER_LIB_H__
#define __NET_RENDER_LIB_H__

#include "nix_dx.hpp"

// -------------------------------------------------------
class _net_render_filter
{
public:

	//virtual STDCALL(void) connect_to(IPin *from) = 0;
	//virtual STDCALL(void) set_graph(IGraphBuilder *g) = 0;
	//virtual STDCALL(IBaseFilter *) filter_iface() = 0;

	virtual STDCALL(_media_type *) media_type() = 0;
	virtual STDCALL(_media_sample *) media_sample() = 0;

	virtual STDCALL(void) release() = 0;
};

EXPORT(_net_render_filter *) create_net_render_filter();

// -------------------------------------------------------
class _gl_render_filter
{
public:

	//virtual STDCALL(IBaseFilter *) filter_iface() = 0;

	//virtual STDCALL(void) set_wnd(HWND wnd,const RECT *r,int rot_angle) = 0;
	virtual STDCALL(void) set_message(const char *msg,bool clear) = 0;

	virtual STDCALL(void) release() = 0;
};

EXPORT(_gl_render_filter *) create_gl_render_filter();

// -------------------------------------------------------
class _net_source_filter
{
public:

	//virtual STDCALL(IBaseFilter *) filter_iface() = 0;

	virtual STDCALL(void) set_media_type(const _media_type *mh) = 0;
	virtual STDCALL(void) set_media_sample(const _media_sample *ms) = 0;

	virtual STDCALL(void) release() = 0;

	//virtual STDCALL(IPin *) get_pin(int n) = 0;

	virtual STDCALL(void) set_sample_time(int st) = 0;

	virtual STDCALL(void) clear_media_samples() = 0;
	virtual STDCALL(int) media_samles_buffer_count() = 0;
};

EXPORT(_net_source_filter *) create_net_source_filter();


#endif
