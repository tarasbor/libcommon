#include "dx_headers.hpp"
#include "dx_error.hpp"
#include <string.h>

extern "C" {
#include <libavcodec/avcodec.h>
}

#ifdef DEBUG
#include <log.hpp>
#define MLOG(L) _log() << _log::_set_module_level("vcall",(L))
#endif // DEBUG

// -------------------------------------------------------
// MEDIA SAMLE
// -------------------------------------------------------
_media_sample::_media_sample()
:stream_stamp(0),buffer(NULL),buffer_size(0),data_size(0),can_break(false),cm_btype(MEDIA_SAMPLE_BT_RAW),cm_mtype(MEDIA_SAMPLE_MT_UNDEF)
{
}

// -------------------------------------------------------
_media_sample::_media_sample(const _media_sample &s)
:stream_stamp(s.stream_stamp),buffer(NULL),buffer_size(s.data_size),data_size(s.data_size),cm_btype(s.cm_btype),cm_mtype(s.cm_mtype)
{
    #ifdef DEBUG
    MLOG(10) << "_media_sample::_media_sample(const _media_sample &s)" << endl;
    #endif

    if (cm_btype==MEDIA_SAMPLE_BT_RAW)
    {
        if (data_size)
        {
            buffer=new unsigned char [data_size];
            memcpy(buffer,s.buffer,data_size);
        }
    }
    else if (cm_btype==MEDIA_SAMPLE_BT_AV_PACKET)
    {
        AVPacket *p=new AVPacket;
        av_init_packet(p);
        av_copy_packet(p,(AVPacket *)s.buffer);
        buffer=(unsigned char *)p;
        //throw _pm_error(0,"_media_sample::_media_sample(const _media_sample &s) - for cm_type==MEDIA_SAMPLE_TYPE_AV_PACKET not release yet");
    }
    else if (cm_btype==MEDIA_SAMPLE_BT_AV_FRAME)
    {
        throw _pm_error(0,"_media_sample::_media_sample(const _media_sample &s) - for cm_type==MEDIA_SAMPLE_TYPE_AV_FRAME not release yet");
    }


    #ifdef DEBUG
    MLOG(10) << "_media_sample::_media_sample(const _media_sample &s) OK" << endl;
    #endif
}


// -------------------------------------------------------
_media_sample::~_media_sample()
{
    #ifdef DEBUG
    MLOG(10) << "_media_sample::~_media_sample()" << endl;
    #endif

    if      (cm_btype==MEDIA_SAMPLE_BT_RAW)
    {
        if (buffer) delete buffer;
    }
    else if (cm_btype==MEDIA_SAMPLE_BT_AV_PACKET)
    {
        //AVPacket *p=(AVPacket *)buffer;
        //if (p) av_free_packet(p);
    }

    buffer=NULL;

    #ifdef DEBUG
    MLOG(10) << "_media_sample::~_media_sample() OK" << endl;
    #endif
}


// -------------------------------------------------------
int _media_sample::pack_size() const
{
    if (cm_btype==MEDIA_SAMPLE_BT_RAW)
    {
        return sizeof(can_break)+sizeof(data_size)+sizeof(stream_stamp)+sizeof(cm_btype)+sizeof(cm_mtype)+data_size;
    }
    else if (cm_btype==MEDIA_SAMPLE_BT_AV_PACKET)
    {
        return sizeof(can_break)+sizeof(data_size)+sizeof(stream_stamp)+sizeof(cm_btype)+sizeof(cm_mtype)+((AVPacket *)buffer)->size;
    }
    else throw _pm_error(0,"_media_sample::pack_size() - for cm_type==MEDIA_SAMPLE_BT_AV_* not release yet");
}

// -------------------------------------------------------
void _media_sample::pack(unsigned char *d) const
{
    #ifdef DEBUG
    MLOG(10) << "_media_sample::pack()" << endl;
    #endif

    if (cm_btype==MEDIA_SAMPLE_BT_RAW)
    {
        int pos=0;

        memcpy(d+pos,&cm_btype,sizeof(cm_btype));
        pos+=sizeof(cm_btype);

        memcpy(d+pos,&cm_mtype,sizeof(cm_mtype));
        pos+=sizeof(cm_mtype);

        memcpy(d+pos,&stream_stamp,sizeof(stream_stamp));
        pos+=sizeof(stream_stamp);

        memcpy(d+pos,&can_break,sizeof(can_break));
        pos+=sizeof(can_break);

        memcpy(d+pos,&data_size,sizeof(data_size));
        pos+=sizeof(data_size);

        if (buffer) memcpy(d+pos,buffer,data_size);
    }
    else if (cm_btype==MEDIA_SAMPLE_BT_AV_PACKET)
    {
        int pos=0;

        memcpy(d+pos,&cm_btype,sizeof(cm_btype));
        pos+=sizeof(cm_btype);

        memcpy(d+pos,&cm_mtype,sizeof(cm_mtype));
        pos+=sizeof(cm_mtype);

        memcpy(d+pos,&stream_stamp,sizeof(stream_stamp));
        pos+=sizeof(stream_stamp);

        memcpy(d+pos,&can_break,sizeof(can_break));
        pos+=sizeof(can_break);

        AVPacket *p=(AVPacket *)buffer;

        // TODO - fix sizeof(dsize)=sizeof(this->data_size)
        unsigned int dsize=p->size;
        memcpy(d+pos,&dsize,sizeof(dsize));
        pos+=sizeof(dsize);

        memcpy(d+pos,p->data,dsize);
    }
    else throw _pm_error(0,"_media_sample::pack() - for cm_type==MEDIA_SAMPLE_BT_AV_* not release yet");

    #ifdef DEBUG
    MLOG(10) << "_media_sample::pack() OK" << endl;
    #endif
}

// -------------------------------------------------------
int _media_sample::depack(const unsigned char *d, int d_size)
{
    #ifdef DEBUG
    MLOG(10) << "_media_sample::depack()" << endl;
    #endif

    if (buffer)
    {
        if (cm_btype==MEDIA_SAMPLE_BT_RAW) delete buffer;
        else if (cm_btype==MEDIA_SAMPLE_BT_AV_PACKET) av_free_packet((AVPacket *)buffer);
    }

    int pos = 0;

    memcpy(&cm_btype,d+pos,sizeof(cm_btype));
    pos+=sizeof(cm_btype);

    memcpy(&cm_mtype,d+pos,sizeof(cm_mtype));
    pos+=sizeof(cm_mtype);

    memcpy(&stream_stamp,d+pos,sizeof(stream_stamp));
    pos+=sizeof(stream_stamp);

    memcpy(&can_break, d+pos, sizeof(can_break));
    pos+=sizeof(can_break);

    memcpy(&data_size,d+pos,sizeof(data_size));
    pos+=sizeof(data_size);

    if (data_size)
    {
        if (cm_btype==MEDIA_SAMPLE_BT_RAW)
        {
            buffer_size=data_size;
            buffer = new unsigned char [buffer_size];
            memcpy(buffer, d+pos, data_size);
        }
        else if (cm_btype==MEDIA_SAMPLE_BT_AV_PACKET)
        {
            AVPacket *p=new AVPacket;
            av_init_packet(p);

            // TODO
            p->stream_index=0;
            p->pts=stream_stamp;
            p->size=d_size;
            if (can_break) p->flags |= AV_PKT_FLAG_KEY;
            p->data=(uint8_t *)d+pos;

            buffer=(unsigned char *)p;
        }
        else throw _pm_error(0,"_media_sample::depack() - for cm_type==MEDIA_SAMPLE_TYPE_AV_* not release yet");
    }

    #ifdef DEBUG
    MLOG(10) << "_media_sample::depack() OK" << endl;
    #endif

    return pack_size();
}

// -------------------------------------------------------
void _media_sample::copy_to_buffer(unsigned char *data,unsigned int len)
{

    #ifdef DEBUG
    MLOG(10) << "_media_sample::copy_to_buffer" << endl;
    #endif

	if (cm_btype==MEDIA_SAMPLE_BT_RAW)
    {
        if (buffer)
        {
            if (len<=buffer_size)
            {
                memcpy(buffer,data,len);
                data_size=len;
                return;
            }

            delete buffer;
            buffer=NULL;
        }

        buffer=new unsigned char [len];
        memcpy(buffer,data,len);
        buffer_size=len;
        data_size=len;
    }
    else throw _pm_error(0,"_media_sample::copy_to_buffer() - can not copy to buffer if media sample type != MEDIA_SAMPLE_BT_RAW");

	#ifdef DEBUG
    MLOG(10) << "_media_sample::copy_to_buffer OK" << endl;
    #endif
}

// -------------------------------------------------------
void _media_sample::create_buffer(unsigned int len)
{
    #ifdef DEBUG
    MLOG(10) << "_media_sample::create_buffer" << endl;
    #endif

    if (cm_btype==MEDIA_SAMPLE_BT_RAW)
    {
        if (buffer)
        {
            if (len<=buffer_size)
            {
                data_size=0;
                return;
            }

            delete buffer;
            buffer=NULL;
        }

        buffer=new unsigned char [len];
        buffer_size=len;
        data_size=0;
    }
    else throw _pm_error(0,"_media_sample::create_buffer() - can not create buffer if media sample type != MEDIA_SAMPLE_BT_RAW");

	#ifdef DEBUG
    MLOG(10) << "_media_sample::create_buffer OK" << endl;
    #endif
}

// -------------------------------------------------------
// VIDEO HEADER
// -------------------------------------------------------
_video_header::_video_header()
:_format_header(),source_rect(),target_rect(),bit_rate(0),error_bit_rate(0),image_dim(),image_bpp(0),image_pixel_format(0)
{
}

// -------------------------------------------------------
_video_header::~_video_header()
{
}

// -------------------------------------------------------
int _video_header::pack_size() const
{
    return sizeof(format_type)+sizeof(avg_nsec_per_frame)+
            sizeof(source_rect)+sizeof(target_rect)+
            sizeof(bit_rate)+sizeof(error_bit_rate)+
            sizeof(image_dim)+sizeof(image_bpp)+sizeof(image_pixel_format);
}

// -------------------------------------------------------
void _video_header::pack(unsigned char *d) const
{
    int pos=0;

    memcpy(d+pos, &format_type, sizeof(format_type));
    pos+=sizeof(format_type);

    memcpy(d+pos, &avg_nsec_per_frame, sizeof(avg_nsec_per_frame));
    pos+=sizeof(avg_nsec_per_frame);

    memcpy(d+pos, &source_rect, sizeof(source_rect));
    pos+=sizeof(source_rect);

    memcpy(d+pos, &target_rect, sizeof(target_rect));
    pos+=sizeof(target_rect);

    memcpy(d+pos, &bit_rate, sizeof(bit_rate));
    pos+=sizeof(bit_rate);

    memcpy(d+pos, &error_bit_rate, sizeof(error_bit_rate));
    pos+=sizeof(error_bit_rate);

    memcpy(d+pos, &image_dim, sizeof(image_dim));
    pos+=sizeof(image_dim);

    memcpy(d+pos, &image_bpp, sizeof(image_bpp));
    pos+=sizeof(image_bpp);

    memcpy(d+pos, &image_pixel_format, sizeof(image_pixel_format));
    pos+=sizeof(image_pixel_format);
}

// -------------------------------------------------------
int _video_header::depack(const unsigned char *d, int d_size)
{
    if (d_size!=pack_size()) return 0;

    int pos=0;

    memcpy(&format_type, d+pos, sizeof(format_type));
    pos+=sizeof(format_type);

    memcpy(&avg_nsec_per_frame, d+pos, sizeof(avg_nsec_per_frame));
    pos+=sizeof(avg_nsec_per_frame);

    memcpy(&source_rect, d+pos, sizeof(source_rect));
    pos+=sizeof(source_rect);

    memcpy(&target_rect, d+pos, sizeof(target_rect));
    pos+=sizeof(target_rect);

    memcpy(&bit_rate, d+pos, sizeof(bit_rate));
    pos+=sizeof(bit_rate);

    memcpy(&error_bit_rate, d+pos, sizeof(error_bit_rate));
    pos+=sizeof(error_bit_rate);

    memcpy(&image_dim, d+pos, sizeof(image_dim));
    pos+=sizeof(image_dim);

    memcpy(&image_bpp, d+pos, sizeof(image_bpp));
    pos+=sizeof(image_bpp);

    memcpy(&image_pixel_format, d+pos, sizeof(image_pixel_format));
    pos+=sizeof(image_pixel_format);

    return pack_size();
}

// -------------------------------------------------------
// AUDIO HEADER
// -------------------------------------------------------
_audio_header::_audio_header()
:_format_header(),sample_rate(0),channels_count(0),avg_bytes_per_sec(0),block_align(0),bits_per_sample(0),samples_order(0)
{
}

// -------------------------------------------------------
_audio_header::~_audio_header()
{
}

// -------------------------------------------------------
int _audio_header::pack_size() const
{
    return sizeof(format_type)+sizeof(avg_nsec_per_frame)+
            sizeof(sample_rate)+sizeof(channels_count)+sizeof(avg_bytes_per_sec)+
            sizeof(block_align)+sizeof(bits_per_sample)+sizeof(samples_order);
}

// -------------------------------------------------------
void _audio_header::pack(unsigned char *d) const
{
    int pos=0;

    memcpy(d+pos, &format_type, sizeof(format_type));
    pos+=sizeof(format_type);

    memcpy(d+pos, &avg_nsec_per_frame, sizeof(avg_nsec_per_frame));
    pos+=sizeof(avg_nsec_per_frame);

    memcpy(d+pos, &sample_rate, sizeof(sample_rate));
    pos+=sizeof(sample_rate);

	memcpy(d+pos, &channels_count, sizeof(channels_count));
	pos+=sizeof(channels_count);

	memcpy(d+pos, &avg_bytes_per_sec, sizeof(avg_bytes_per_sec));
	pos+=sizeof(avg_bytes_per_sec);

	memcpy(d+pos, &block_align, sizeof(block_align));
	pos+=sizeof(block_align);

	memcpy(d+pos, &bits_per_sample, sizeof(bits_per_sample));
	pos+=sizeof(bits_per_sample);

	memcpy(d+pos, &samples_order, sizeof(samples_order));
	pos+=sizeof(samples_order);
}

// -------------------------------------------------------
int _audio_header::depack(const unsigned char *d, int d_size)
{
    if (d_size!=pack_size()) return 0;

    int pos=0;

    memcpy(&format_type, d+pos, sizeof(format_type));
    pos+=sizeof(format_type);

    memcpy(&avg_nsec_per_frame, d+pos, sizeof(avg_nsec_per_frame));
    pos+=sizeof(avg_nsec_per_frame);

    memcpy(&sample_rate, d+pos, sizeof(sample_rate));
    pos+=sizeof(sample_rate);

	memcpy(&channels_count, d+pos, sizeof(channels_count));
	pos+=sizeof(channels_count);

	memcpy(&avg_bytes_per_sec, d+pos, sizeof(avg_bytes_per_sec));
	pos+=sizeof(avg_bytes_per_sec);

	memcpy(&block_align, d+pos, sizeof(block_align));
	pos+=sizeof(block_align);

	memcpy(&bits_per_sample, d+pos, sizeof(bits_per_sample));
	pos+=sizeof(bits_per_sample);

	memcpy(&samples_order, d+pos, sizeof(samples_order));
	pos+=sizeof(samples_order);

    return pack_size();
}

// -------------------------------------------------------
// PORTABLE HEADER
// -------------------------------------------------------
void _portable_header::pack(unsigned char *d) const
{
    int pos=0;

    memcpy(d+pos, &format_type, sizeof(format_type));
    pos+=sizeof(format_type);

    memcpy(d+pos, &avg_nsec_per_frame, sizeof(avg_nsec_per_frame));
    pos+=sizeof(avg_nsec_per_frame);

    unsigned int l=text.length();
    memcpy(d+pos, &l, sizeof(l));
    pos+=sizeof(avg_nsec_per_frame);

    memcpy(d+pos, text.data(), l);
}

// -------------------------------------------------------
int _portable_header::depack(const unsigned char *d, int d_size)
{
    //if (d_size!=pack_size()) return 0; - we can not check here & check expresssion is wrong - pack_size use text length

    int pos=0;

    memcpy(&format_type, d+pos, sizeof(format_type));
    pos+=sizeof(format_type);

    memcpy(&avg_nsec_per_frame, d+pos, sizeof(avg_nsec_per_frame));
    pos+=sizeof(avg_nsec_per_frame);

    unsigned int l;
    memcpy(&l, d+pos, sizeof(l));
    pos+=sizeof(l);

    if (d_size!=pack_size()-text.length()+l) return 0;

    char *tmp=new char [l];

	memcpy(tmp, d+pos, l);
	text.assign(tmp,l);

	delete tmp;

    return pack_size();
}


// -------------------------------------------------------
// STREAM HEADER
// -------------------------------------------------------
_stream_header::_stream_header()
:samples_are_fixed_size(false),use_interframes(false),major_type(0),minor_type(0),format_header(NULL)
{
}

// -------------------------------------------------------
_stream_header::~_stream_header()
{
    #ifdef DEBUG
    MLOG(10) << "_stream_header::~_stream_header()" << endl;
    #endif

    if (format_header) delete format_header;

    #ifdef DEBUG
    MLOG(10) << "_stream_header::~_stream_header() OK" << endl;
    #endif
}

// -------------------------------------------------------
void _stream_header::set_format_header(_format_header *s)
{
    #ifdef DEBUG
    MLOG(10) << "_stream_header::set_format_header" << endl;
    #endif

	if (format_header) delete format_header;

    format_header=s;

    #ifdef DEBUG
    MLOG(10) << "_stream_header::set_format_header OK" << endl;
    #endif
}

// -------------------------------------------------------
bool _stream_header::operator==(const _stream_header &mt) const
{
    return (major_type==mt.major_type && minor_type==mt.minor_type);
}

// -------------------------------------------------------
int _stream_header::pack_size() const
{
    unsigned int fs=0;
    if (format_header) fs = format_header->pack_size();

    return sizeof(samples_are_fixed_size)+sizeof(use_interframes)+
            sizeof(major_type)+sizeof(minor_type)+
            sizeof(fs)+fs;
}

// -------------------------------------------------------
void _stream_header::pack(unsigned char *d) const
{
    #ifdef DEBUG
    MLOG(10) << "_stream_header::pack" << endl;
    #endif

    int pos=0;

    memcpy(d+pos,&samples_are_fixed_size,sizeof(samples_are_fixed_size));
    pos+=sizeof(samples_are_fixed_size);

    memcpy(d+pos,&use_interframes,sizeof(use_interframes));
    pos+=sizeof(use_interframes);

    memcpy(d+pos,&major_type,sizeof(major_type));
    pos+=sizeof(major_type);

    memcpy(d+pos,&minor_type,sizeof(minor_type));
    pos+=sizeof(minor_type);

    unsigned int format_size=0;
    if (format_header) format_size=format_header->pack_size();

    memcpy(d+pos,&format_size,sizeof(format_size));
    pos+=sizeof(format_size);

    if (format_header) format_header->pack(d+pos);

    #ifdef DEBUG
    MLOG(10) << "_stream_header::pack OK" << endl;
    #endif
}

// -------------------------------------------------------
int _stream_header::depack(const unsigned char *d, int d_size)
{
    if (d_size<pack_size()) return 0;

    #ifdef DEBUG
    MLOG(10) << "_stream_header::depack" << endl;
    #endif

    int pos=0;

    memcpy(&samples_are_fixed_size, d+pos, sizeof(samples_are_fixed_size));
    pos+=sizeof(samples_are_fixed_size);

    memcpy(&use_interframes, d+pos, sizeof(use_interframes));
    pos+=sizeof(use_interframes);

    memcpy(&major_type, d+pos, sizeof(major_type));
    pos+=sizeof(major_type);

    memcpy(&minor_type, d+pos, sizeof(minor_type));
    pos+=sizeof(minor_type);

    unsigned int fs;
    memcpy(&fs, d+pos, sizeof(fs));
    pos+=sizeof(fs);

    if (format_header)
    {
        delete format_header;
        format_header=NULL;
    }

    if (fs)
    {
        MEDIA_FORMAT_TYPE_CLASS cl;
        memcpy(&cl,d+pos,sizeof(MEDIA_FORMAT_TYPE_CLASS));

        if (cl==MEDIA_FORMAT_VIDEO) format_header = new _video_header;
        else if (cl==MEDIA_FORMAT_AUDIO) format_header = new _audio_header;
        else if (cl==MEDIA_FORMAT_PORTABLE) format_header = new _portable_header;
        else throw _pm_error(cl,"_stream_header::depack - unknown MEDIA_FORMAT_TYPE value");

        format_header->depack(d+pos,fs);
    }

    #ifdef DEBUG
    MLOG(10) << "_stream_header::depack OK" << endl;
    #endif

    return pack_size();
}
