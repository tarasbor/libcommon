#ifndef __NIX_DX_CLASSES_H__
#define __NIX_DX_CLASSES_H__

using namespace std;

#include <list>
#include "dx_headers.hpp"

#ifdef WINDOWS

    #include <dshow.h>
    #include <dx_proxy.hpp>

    //#define EXPORT(A)	extern "C" __declspec(dllimport) A __stdcall
    //#define STDCALL(A)	A __stdcall

#else   // -------------------------------------------------------- *NIX ---------------------------

    #define STDCALL(A)	A
    #define EXPORT(A)	A

    #define HRESULT		int
    #define REFGUID		int
    #define HWND		int


#endif

#include <time.hpp>
#include <error.hpp>


// on *nix we do not have some DirectX Classes, Structs & Macros -))
// Write it myself -))


// ------------------------------------------------------------------------
//struct REFERENCE_TIME
//{
//	int stub;
//};

// ------------------------------------------------------------------------
//struct BITMAPINFOHEADER
//{
//	int stub;
//};

// ------------------------------------------------------------------------
//struct GUID
//{
//	int stub;
//};


// ------------------------------------------------------------------------
// ------------------------------------------------------------------------
class _pin;
typedef list<_pin *> _pins_ptr_list;
typedef list<_pin *>::iterator _pin_ptr_it;
typedef list<_pin *>::const_iterator _pin_ptr_const_it;

// ------------------------------------------------------------------------
class _pin
{
	public:
		enum _state
		{
			stoped,
			started,
			paused,
			error
		};

	protected:

		_stream_header *cm_sheader;
		_pins_ptr_list cm_connected_to;

		bool cm_is_input;
		_state cm_state;

		virtual ~_pin() { /* TODO disconnect_from_all();*/ if (cm_sheader) delete cm_sheader; };

	public:
		_pin():cm_sheader(NULL),cm_is_input(false),cm_state(stoped) {};

		virtual void release() { delete this; };

		//virtual void media_type(const _media_type *mt) { cm_mtype=mt; };
		virtual const _stream_header *stream_header() const { return cm_sheader; };
		virtual bool is_configured() const { return (cm_sheader!=NULL); };

		virtual bool try_accept_stream_header(const _stream_header *mt) const { return true; };

		virtual void connect(_pin *to);
		virtual void on_connect(_pin *from);

		virtual void disconnect(_pin *to);
		virtual void on_disconnect(_pin *from);

		virtual void disconnect_from_all();

		virtual _pins_ptr_list connected_to() { return cm_connected_to; };

		virtual bool is_input() { return cm_is_input; };

		virtual void begin_flush();
		virtual void end_flush();

		virtual void end_of_stream();
		virtual void new_segment();

		virtual void start();
		virtual void stop();
		virtual void pause();

		virtual _state state() const { return cm_state; };

		virtual void push_stream_header(const _stream_header *sh);
		virtual void push_media_sample(const _media_sample *ms);
};

// ------------------------------------------------------------------------
class _media_source
{
	protected:
		_media_source() {};
		virtual ~_media_source() {};

	public:

		virtual const _pins_ptr_list &out_pins() const = 0;

		virtual void release() = 0;
};

// ------------------------------------------------------------------------
class _media_target
{
	protected:
		_media_target() {};
		virtual ~_media_target() {};

	public:

		virtual const _pins_ptr_list &in_pins() const = 0;

		virtual void release() = 0;
};

// ------------------------------------------------------------------------
class _media_filter:public _media_source,public _media_target
{
	protected:
		_media_filter() {};
		virtual ~_media_filter() {};

	public:

	    virtual void release() = 0;
};

// ------------------------------------------------------------------
class _dxpipe
{
    protected:
        virtual ~_dxpipe() {};

    public:

        virtual void capture(_media_source *cap) = 0;
        virtual void coder(_media_filter *fil) = 0;
        virtual void render(_media_target *tar) = 0;
        virtual void preview(_media_target *tar) = 0;

        virtual _media_source *capture() = 0;
        virtual _media_filter *coder() = 0;
        virtual _media_target *render() = 0;
        virtual _media_target *preview() = 0;

        virtual void start() = 0;
        virtual void stop() = 0;
        virtual void pause() = 0;

        virtual void release() = 0;
};

// ------------------------------------------------------------------
_dxpipe *create_pipe();



/*
// ------------------------------------------------------------------------
class IBaseFilter
{
	protected:
		IBaseFilter() {};
		virtual ~IBaseFilter() {};

	public:
		virtual void Release() = 0;

		virtual void set_output(IBaseFilter *out) = 0;
};


// ------------------------------------------------------------------------
class IGraphBuilder
{
	protected:
		IGraphBuilder() {};
		virtual ~IGraphBuilder() {};

	public:
		virtual void Release() = 0;
};
*/

#endif
