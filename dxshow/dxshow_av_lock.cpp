#include "dxshow_av_lock.hpp"

#include <clthread.hpp>

#include <log.hpp>

#define MLOG(A) _log() << _log::_set_module_level("av_lock",(A))
#define DEBUG   10
#define DEBUG2  15

extern "C" {
#include <libavformat/avformat.h>
}

class _av_lm:public _av_lock_manager
{
    protected:
        static int ref;

        static int mgr_cb(void **mutex, AVLockOp op);
    public:
        _av_lm();
        ~_av_lm();
};

int _av_lm::ref = 0;

// -----------------------------------------------------------------------------
_av_lm::_av_lm()
{
    ref++;
    if (ref==1)
    {
        av_lockmgr_register(&mgr_cb);
    }
}

// -----------------------------------------------------------------------------
_av_lm::~_av_lm()
{
    ref--;
    if (ref==0)
    {
        av_lockmgr_register(NULL);
    }
}

// -----------------------------------------------------------------------------
int _av_lm::mgr_cb(void **mutex, AVLockOp op)
{
    if (op==AV_LOCK_CREATE)
    {
        *mutex = (void *) new _locker;
    }
    else if (op==AV_LOCK_OBTAIN)
    {
        _locker *l=(_locker *)*mutex;
        l->lock();
    }
    else if (op==AV_LOCK_RELEASE)
    {
        _locker *l=(_locker *)*mutex;
        l->unlock();
    }
    else if (op==AV_LOCK_DESTROY)
    {
        _locker *l=(_locker *)*mutex;
        delete l;
    }

    return 0;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
_av_lock_manager *create_av_lock_manager()
{
    return new _av_lm;
}

