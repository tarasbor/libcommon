#ifndef __LOG_CLASS__
#define __LOG_CLASS__

#include <string>
#include <set>
#include <map>
#include <list>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
//#include <time.h>

#include <clthread.hpp>
#include <time.hpp>

using namespace std;

// ----------------------------------------------
// log writers
// ----------------------------------------------

typedef set<string> _log_modules_accepted_list;

// base class for log output writers
class _log_writer_base
{
    _log_modules_accepted_list al;

public:

    /**
     * @brief returns true if the writer accepts messages for the given module name
     * @param name - module name
     * @see _log_writer_base::_log_writer_base
     */
    virtual bool is_module_accepted(const string& name) const;

    /**
     * @brief writes message to log
     * @param datetime - UTC time
     * @param tid - thread id
     * @param msg - the message
     * @param event_code - optional code for search
     */
    virtual void out_buff(const _time &datetime, unsigned long tid, const string& module, const string& msg) = 0;

    /**
     * @brief constructor
     * @param accepted_modules_list - see set_accepted_modules_list
     * @see is_module_accepted
     */
    _log_writer_base(const char* accepted_modules_list = NULL);
    virtual ~_log_writer_base() {}

    /**
     * @brief set accepted modules list
     * @param accepted_modules_list - list of module names which message output accepted for (use one of delimeters "," ";" or space) or empty (or NULL) for accept all modules
     * @see is_module_accepted
     */
    virtual void set_accepted_modules_list(const char* accepted_modules_list);
};

// class for multiple log output writers
class _log_multiwriter : public _log_writer_base
{
private:

    typedef list<_log_writer_base*> writers_list;
    writers_list m_writers;

public:

    /**
     * @brief add writer to pool
     * @param wr
     * @see _log_writer_base
     */
    void add_writer(_log_writer_base* wr) { m_writers.push_back(wr); }
    /**
     * @brief remove writer to pool
     * @param wr
     * @see _log_writer_base
     */
    void remove_writer(_log_writer_base* wr);
    /**
     * @brief writes message via appended log writers
     * @param datetime - UTC time
     * @param tid - thread id
     * @param msg - the log message
     * @param event_code - optional code for search
     */
    virtual void out_buff(const _time &datetime, unsigned long tid, const string &module, const string& msg);
    virtual bool is_module_accepted(const string& name) const;
    _log_multiwriter() : _log_writer_base(NULL) {}
    virtual ~_log_multiwriter() {}
};

// ----------------------------------------------
// log
// ----------------------------------------------

/// \class Multi-threaded log class
class _log
{
private:
protected:
public:

    /// \class single-threaded log class
    class _log_st : public ostringstream
    {
      public:

        map<string, int>::iterator it_curr_module; //< Current module (if selected)
        int curr_log_level; //< Current log level (0 by default)

        _log_st() : it_curr_module(_log::modules.end()),curr_log_level(0) { }
        _log_st(const _log_st& src) : it_curr_module(src.it_curr_module),curr_log_level(src.curr_log_level) { } //< Constructor for insert to STL container
        ~_log_st() { }
    };

protected:

    static map<unsigned long, _log::_log_st> logs; //< ST-Logs by thread id
    static _log_st& log() { return logs[get_current_thread_id()]; } // Returns ST-log for current thread

    static map<string, int> modules; //< Log level filters by logical modules

    static _log_writer_base* out; //< Pointer to buffer output object
    void out_buff(); //< buffer output method

    static _locker cs; //< Locker for static data

public:

    static void set_writer(_log_writer_base* o) { /*if(!o) throw TODO*/ out = o; } //< Sets the output object for the log object
    static const _log_writer_base *get_writer() { return out; }

    _log() { }
    ~_log() { }

    /**
     * Adds or replaces module name with it's log level setting
     *
     * @param module_name Module name
     * @param module_log_level Log level for given module name
     *
     * @see module()
     * @see level()
     * @see module_level
     * */
    static void max_level4module(const string& module_name, int module_log_level)
    {
        cs.lock();
        modules[module_name] = module_log_level;
        cs.unlock();
    }

    /**
     * Returns true if currently set by stream manipulator log level is lower or equal to log level of module.
     * @see max_level4module()
     * @see module()
     * @see level()
     * @see module_level()
     */
    bool curr_log_level_allowed();

    /**
     * Auto-overloaded operator << for all used types.
     */
    template<class T> _log& operator<<(T v)
    {
        cs.lock();
        if(curr_log_level_allowed())
          ((ostringstream&)log()) << v;
        cs.unlock();
        return *this;
    }

    // manipulators recognition

    /**
     * \struct Data type for stream manipulator module() detection
     */
    struct _set_module { string name; _set_module(const string& n) : name(n) {} };
    /**
     * Stream manipulator to set current module-level filter
     * @see level()
     * @see module_level()
     */
    static _set_module module(const string& m) { return _set_module(m); }
    /**
     * Dedicated operator << for manipulator module()
     * @see module()
     */
    _log& operator<<(_set_module sm);

    /**
     * \struct Data type for stream manipulator level() detection
     */
    struct _set_level { int value; _set_level(int v) : value(v) {} };
    /**
     * Stream manipulator to set current log level for next output
     * @see module()
     * @see module_level()
     */
    static _set_level level(int l) { return _set_level(l); }
    /**
     * Dedicated operator << for manipulator level()
     * @see module()
     */
    _log& operator<<(_set_level sl) { log().curr_log_level = sl.value; return *this; }

    /**
     * \struct Data type for stream manipulator module_level() detection
     */
    struct _set_module_level { string mname; int lvalue; _set_module_level(const string& n, int v) : mname(n), lvalue(v) {} };
    /**
     * Stream manipulator to set both current module-level filter and log level for next output
     * @see module()
     * @see level()
     */
    static _set_module_level module_level(const string& m, int l) { return _set_module_level(m, l); }
    /**
     * Dedicated operator << for manipulator level()
     * @see module_level()
     */
    _log& operator<<(_set_module_level sml) { *this << module(sml.mname); *this << level(sml.lvalue); return *this; }

    /**
     * /brief Recognizes std::manipulators.
     *
     * In addition to standard  behavior endl and flush invoke writing buffer to output.
     * Modifier flush is non-filtered by level, all others (incl. endl) are filtered
     */
    _log& operator<<(std::ostream&(*f)(std::ostream&));
};

#endif
