#include <log.hpp> // log2
#include <log_file_writer.hpp>

// -----------------------------------------------------------------------------

_log_file_writer::_log_file_writer(const std::string &fname,const char* accepted_modules_list)
: _log_writer_base(accepted_modules_list),fname(fname)
{
    file.open(fname.c_str(), ios::out | ios::app);
    if(!file)
    {
        //TODO
    }
}

// -----------------------------------------------------------------------------

_log_file_writer::~_log_file_writer()
{
    file.close();
    if((file.rdstate() & ofstream::failbit) != 0)
    {
        // TODO
    }
}

// -----------------------------------------------------------------------------

void _log_file_writer::out_buff(const _time &datetime, unsigned long tid, const string &modul, const string& msg)
{
    _time::_ys ys=datetime.split();
    tm t;
    t.tm_year = ys.y-1900;
    t.tm_mon = ys.m-1;
    t.tm_mday = ys.d;
    t.tm_hour = ys.h;
    t.tm_min = ys.n;
    t.tm_sec = ys.s;
	
    char cstr[128];
    strftime(cstr, 32, "%Y.%m.%d %H:%M:%S : ", &t);
    string str =cstr;
    str += "[";
	str +=modul;
	str +="] : ";

    sprintf(cstr, "[%lu] : ", get_current_thread_id());
    str += cstr;

    str += msg;
    file << str << std::flush;
}

