#include <log.hpp> // log2
#include <log_sqlite_writer.hpp>
#include <error.hpp>


// -----------------------------------------------------------------------------

_log_sqlite_writer::_log_sqlite_writer(const std::string &fname)
: fname(fname),db(NULL),cm_text_encoder(NULL)
{
    int kz=sqlite3_initialize();
    if (kz!=SQLITE_OK)
    {
        // TODO - set error status
        //throw _error(kz, sqlite3_errstr(kz));
    }
    string bd_fname=fname;
    
    kz=sqlite3_open_v2(
                bd_fname.c_str(),                                       /* Database filename (UTF-8) */
                &db,                                                    /* OUT: SQLite db handle */
                SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE,             /* Flags */
                NULL                                                    /* Name of VFS module to use */
                );
    if ( kz != SQLITE_OK)
    {
        sqlite3_close( db );        
        // TODO - set error status
        //throw _error(kz, sqlite3_errmsg(db));
        return ;
    }
    
    /* create a statement from an SQL string */
    sqlite3_stmt *stmt = NULL;
    string sql_str= "CREATE  TABLE  IF NOT EXISTS \"main\".\"log\" \
                     (\"num\" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,\
                      \"date\" INTEGER NOT NULL DEFAULT 0,\
                      \"thread\" INTEGER DEFAULT 0,\
                      \"module\" TEXT,\
                      \"message\" TEXT)";
    kz=sqlite3_prepare_v2( db, sql_str.c_str(), -1, &stmt, NULL );
    if ( kz != SQLITE_OK)
    {
        if (kz!=1)
        {
            sqlite3_close( db );
            // TODO - set error status
            //throw _error(kz, sqlite3_errmsg(db));
            return;
        }
        else return;
    }
    
    sqlite3_step( stmt );
    if ( kz != SQLITE_OK)
    {
        sqlite3_close( db );
        // TODO - set error status
        //throw _error(kz, sqlite3_errmsg(db));
        return;
    }
}

// -----------------------------------------------------------------------------

_log_sqlite_writer::~_log_sqlite_writer()
{
    sqlite3_close( db );
    sqlite3_shutdown();
}

// -----------------------------------------------------------------------------

void _log_sqlite_writer::out_buff(const _time &datetime, unsigned long tid, const string& module, const string& msg )
{
    sqlite3_stmt *stmt = NULL;
    int kz;
    string sql_str=
        "INSERT INTO \"main\".\"log\" (\"date\",\"thread\",\"module\",\"message\") VALUES (?1,?2,?3,?4)";
    
    kz=sqlite3_prepare_v2( db, sql_str.c_str(), -1, &stmt, NULL );
    if ( kz != SQLITE_OK)
    {
        sqlite3_close( db );
        // TODO - set error status
        //throw _error(kz, sqlite3_errmsg(db));
        return;
    }
    
    kz=sqlite3_bind_int64(stmt, 1, datetime.stamp());
    if ( kz != SQLITE_OK)
    {
        sqlite3_close( db );
        // TODO - set error status
        //throw _error(kz, sqlite3_errmsg(db));
        return;
    }
    
    kz=sqlite3_bind_int64(stmt, 2, tid);
    if ( kz != SQLITE_OK)
    {
        sqlite3_close( db );
        // TODO - set error status
        //throw _error(kz, sqlite3_errmsg(db));
        return;
    }
    
    string db_mod;
    if (cm_text_encoder) cm_text_encoder(module,db_mod); else db_mod=module;
     
    kz=sqlite3_bind_text(stmt, 3, db_mod.data(), db_mod.length(), NULL);
    if ( kz != SQLITE_OK)
    {
        sqlite3_close( db );
        // TODO - set error status
        //throw _error(kz, sqlite3_errmsg(db));
        return;
    }
    
    string db_msg;
    if (cm_text_encoder) cm_text_encoder(msg,db_msg); else db_msg=msg;
    
    kz=sqlite3_bind_text(stmt, 4, db_msg.data(), db_msg.length(), NULL);
    if ( kz != SQLITE_OK)
    {
        sqlite3_close( db );
        // TODO - set error status
        //throw _error(kz, sqlite3_errmsg(db));
        return;
    }
    
    int count_err=0;
    while((sqlite3_step( stmt) != SQLITE_DONE) && (++count_err < 1000))
    {
        Sleep(1);
    };
    
    sqlite3_reset(stmt);
  }

