#include <log.hpp> // use libcommon/log2
#include <string.h>
#include <time.hpp>
#include "text_help.hpp"

using namespace std;

//--------------------------------------------------------
// _log_writer_base
//--------------------------------------------------------

_log_writer_base::_log_writer_base(const char* name_list)
{
    set_accepted_modules_list(name_list);
}

bool _log_writer_base::is_module_accepted(const string& name) const
{
    if(al.empty()) return true;
    if(al.find(name)==al.end()) return false;
    return true;
}

void _log_writer_base::set_accepted_modules_list(const char* accepted_modules_list)
{
    al.clear();
    
    if(!accepted_modules_list) return;
    if (strlen(accepted_modules_list)==0) return;
    
    string str_name_list(accepted_modules_list);
    
    int i=0;
    while(1)
    {
        string mod=_text_help::get_field_from_st(str_name_list,",; ",i);
        if (mod.empty()) break;
        
        i++;
        al.insert(mod);
    }
}

//--------------------------------------------------------
// _log_multiwriter
//--------------------------------------------------------

void _log_multiwriter::out_buff(const _time &datetime, unsigned long tid, const string& module, const string& msg)
{
    if(m_writers.empty()) return;

    for(writers_list::iterator it = m_writers.begin(); it != m_writers.end(); ++it)
    {
        //        if(!(*it))
        //            throw _error(-1, "Log writer item of multilog writer is not initialized!");
        //TODO
        if ((*it)->is_module_accepted(module)) (*it)->out_buff(datetime, tid, module, msg);
    }
}

//--------------------------------------------------------
bool _log_multiwriter::is_module_accepted(const string& name) const
{
    return true;
}

//--------------------------------------------------------
void _log_multiwriter::remove_writer(_log_writer_base* wr)
{
    writers_list::iterator it=m_writers.begin();
    while(it!=m_writers.end())
    {
        if ((*it)==wr)
        {
            m_writers.erase(it);
            return;
        }
        it++;
    }
}

//_log_multiwriter::writers_list _log_multiwriter::m_writers;

//--------------------------------------------------------
// _log
//--------------------------------------------------------

map<unsigned long, _log::_log_st> _log::logs;
map<string, int> _log::modules;
_log_writer_base* _log::out=NULL;
_locker _log::cs;

//--------------------------------------------------------
bool _log::curr_log_level_allowed()
{
    _log_st& log = this->log();
	if (log.it_curr_module == modules.end()) return false;
	if(log.curr_log_level > log.it_curr_module->second) return false;
    return true;
}

//--------------------------------------------------------
_log& _log::operator<<(_log::_set_module sm)
{
	log().it_curr_module = modules.find(sm.name);
	if (log().it_curr_module==modules.end())
	{
		cs.lock();
		modules[sm.name]=0;
		cs.unlock();
		log().it_curr_module = modules.find(sm.name);
	}
	return *this;
}

//--------------------------------------------------------
void _log::out_buff()
{
    if (!out) return;
	
    _time t;
    t.set_now();

    cs.lock();
	
	_log_st& log = this->log();
	
	if(!out->is_module_accepted(log.it_curr_module->first))
	{
		cs.unlock();
		return;
	}

    out->out_buff(t, get_current_thread_id(), log.it_curr_module->first, log.str());
    log.str("");

    cs.unlock();
}

//--------------------------------------------------------
_log& _log::operator<<(std::ostream&(*f)(std::ostream&))
  {
    union
    {
        std::ostream&(*pin)(std::ostream&);
        void* pout;
    } p1, p2;
    
    p1.pin = f;
    
    if(curr_log_level_allowed())
    {
        (std::ostream&)log() << f;
        //        if(f == std::endl) // G++ compilation error - type cast
        p2.pin = std::endl;
        if(p1.pout == p2.pout)
          out_buff();
    }
    //    else if(f) == std::flush) // G++ compilation error - type cast
    else
    {
        p2.pin = std::flush;
        if(p1.pout == p2.pout)
          out_buff();
    }
    return *this;
}
