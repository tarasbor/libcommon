#ifndef __LOG_SQLITE_WRITER_H__
#define __LOG_SQLITE_WRITER_H__

#include <log.hpp> // use commonlib/log2
#include <sqlite3.h>

using namespace std;

typedef const string &_log_sql_writer_text_encoder(const string &from,string &to);

class _log_sqlite_writer : public _log_writer_base
{
protected:
    
    string fname;
    _log_sql_writer_text_encoder *cm_text_encoder;    
	
public:
    _log_sqlite_writer(const string &fname);
    virtual ~_log_sqlite_writer();
	
    virtual void out_buff(const _time &datetime, unsigned long tid, const string& module, const string &msg);
    virtual void set_text_encoder(_log_sql_writer_text_encoder &en) { cm_text_encoder=en; };
private:
    sqlite3 *db;
};

#endif
