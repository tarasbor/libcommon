#include <log_console_writer.hpp>

class _log_console_writer : public _log_writer_base
{
public:
    _log_console_writer(const char* accepted_modules_list):_log_writer_base(accepted_modules_list) {};
    virtual ~_log_console_writer() {};
	
    virtual void out_buff(const _time &datetime, unsigned long tid, const string &modul, const string &msg);
};

// -----------------------------------------------------------------------------

void _log_console_writer::out_buff(const _time &datetime, unsigned long tid, const string &modul, const string& msg)
{
    _time::_ys ys=datetime.split();
    tm t;
    t.tm_year = ys.y-1900;
    t.tm_mon = ys.m-1;
    t.tm_mday = ys.d;
    t.tm_hour = ys.h;
    t.tm_min = ys.n;
    t.tm_sec = ys.s;
	
    char cstr[128];
    strftime(cstr, 32, "%Y.%m.%d %H:%M:%S : ", &t);
    string str =cstr;
    str += "[";
	str +=modul;
	str +="] : ";

    sprintf(cstr, "[%lu] : ", get_current_thread_id());
    str += cstr;

    str += msg;
    cout << str << std::flush;
}

_log_writer_base *create_log_console_writer(const char* accepted_modules_list)
{
	return new _log_console_writer(accepted_modules_list);
}