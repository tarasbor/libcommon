#ifndef __LOG_FILE_WRITER_H__
#define __LOG_FILE_WRITER_H__

#include <log.hpp> // use commonlib/log2

using namespace std;

class _log_file_writer : public _log_writer_base
{
protected:
    string fname;
    ofstream file;

public:
    _log_file_writer(const string &fname,const char* accepted_modules_list = NULL);
    virtual ~_log_file_writer();

    virtual void out_buff(const _time &datetime, unsigned long tid, const string &modul, const string &msg);
  };

#endif
