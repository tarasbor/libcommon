#ifndef __LOG_CONSOLE_WRITER_H__
#define __LOG_CONSOLE_WRITER_H__

#include <log.hpp> // use commonlib/log2

using namespace std;

_log_writer_base *create_log_console_writer(const char* accepted_modules_list = NULL);

#endif
