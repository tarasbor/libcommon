#include "rs.hpp"
#include <clthread.hpp>
#include <time.hpp>

#include <stdio.h>
#include <map>

#if defined(WINDOWS)
#include <windows.h>
#else
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#endif

#include <iostream>

using namespace std;

// ------------------------------------------------------
// ------------------------------------------------------
class _rs_port:public _rs::_port
{
    private:

#if defined(WINDOWS)
        HANDLE     hComm;
        OVERLAPPED ov_read;
        OVERLAPPED ov_write;
        DCB        dcb;
#else
        int port_d;
#endif

        bool cm_is_open;

		std::string cm_open_as;

    public:

      _rs_port():cm_is_open(false) {};
      virtual ~_rs_port() { if (cm_is_open) close(); };

      virtual void open(int port_n);
      virtual void open(const std::string &port_name);
      virtual void close();
	  virtual const std::string &open_as() const { return cm_open_as; };
	  virtual bool is_open() const { return cm_is_open; };

      virtual void set_baud(int rate);
      virtual void set_params(int parity,int data_l,int stop_bits);

      virtual unsigned int  get_c();
      virtual void put_c(unsigned char c);

      virtual void get_s(std::string &s,unsigned int time_ms);
      virtual void put_s(const std::string &s);
      virtual void put_s(const unsigned char *s,int l);

};

// ------------------------------------------------------
void _rs_port::open(const std::string &port_name)
{
    if (cm_is_open) close();

#if defined(WINDOWS)

    hComm=INVALID_HANDLE_VALUE;
    memset(&ov_read,0,sizeof(OVERLAPPED));
    memset(&ov_write,0,sizeof(OVERLAPPED));
    ov_read.hEvent=INVALID_HANDLE_VALUE;
    ov_write.hEvent=INVALID_HANDLE_VALUE;
    dcb.DCBlength=sizeof(dcb);

    hComm=CreateFile(port_name.c_str(),GENERIC_READ | GENERIC_WRITE,
                      0,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED,
                      NULL);

    if (hComm==INVALID_HANDLE_VALUE) throw _rs::_error(_Z_ERROR_OPEN_COM,"_Z_ERROR_OPEN_COM");

    ov_write.hEvent=CreateEvent(NULL,FALSE,FALSE,NULL);
    ov_read.hEvent=CreateEvent(NULL,FALSE,FALSE,NULL);
    if (ov_write.hEvent==INVALID_HANDLE_VALUE ||
         ov_read.hEvent==INVALID_HANDLE_VALUE) throw _rs::_error(_Z_ERROR_CREATE_EVENT,"_Z_ERROR_CREATE_EVENT");

    if(!SetupComm(hComm,10240,10240)) throw _rs::_error(_Z_ERROR_SET_QUERY,"_Z_ERROR_SET_QUERY");

    if (!GetCommState(hComm,&dcb)) throw _rs::_error(_Z_ERROR_GET_STATE,"_Z_ERROR_GET_STATE");
    dcb.fOutxCtsFlow=FALSE;
    dcb.fOutxDsrFlow=FALSE;
    dcb.fDtrControl=DTR_CONTROL_DISABLE;
    dcb.fDsrSensitivity=FALSE;
    dcb.fOutX=FALSE;
    dcb.fInX=FALSE;
    dcb.fRtsControl=FALSE;
    dcb.fAbortOnError=TRUE;
    if (!SetCommState(hComm,&dcb)) throw _rs::_error(_Z_ERROR_SET_STATE,"_Z_ERROR_SET_STATE");

#else

    port_d = ::open(port_name.c_str(),O_RDWR | O_NOCTTY | O_NDELAY);

    if (port_d == -1) throw _rs::_error(_Z_ERROR_OPEN_COM,"_Z_ERROR_OPEN_COM");

    termios options;

    int r = tcgetattr(port_d,&options);
    if (r) throw _rs::_error(_Z_ERROR_GET_STATE,"_Z_ERROR_GET_STATE");

    // set local mode (disable control modem line) & enable read (reciver)
    options.c_cflag |= (CLOCAL | CREAD);

    //disable CTS/RTS
#if defined(_BSD_SOURCE)
    options.c_cflag &= ~CRTSCTS;
#elif defined(_SVID_SOURCE)
    options.c_cflag &= ~CRTSCTS;
#else
    options.c_cflag &= ~CNEW_RTSCTS;
#endif

    // set raw mode & disable echo & etc
    options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);

    // disable XON XOFF
    options.c_iflag &= ~(IXON | IXOFF | IXANY | INLCR | ICRNL);

    // set raw mode for output (disable output postprocess)
    options.c_oflag &= ~OPOST;

    r = tcsetattr(port_d, TCSANOW, &options);
    if (r) throw _rs::_error(_Z_ERROR_GET_STATE,"_Z_ERROR_SET_STATE");

#endif

	cm_open_as=port_name;
    cm_is_open=true;
}

// ------------------------------------------------------
void _rs_port::open(int port_n)
{
#if defined(WINDOWS)
    char tmp[32];
    sprintf(tmp,"\\\\.\\COM%i",port_n);
    open(std::string(tmp));
#else
    char tmp[32];
    sprintf(tmp,"/dev/ttyS%i",port_n);
    open(std::string(tmp));
#endif
}

// ------------------------------------------------------
void _rs_port::close()
{
    if (!cm_is_open) return;

#if defined(WINDOWS)

    PurgeComm(hComm, PURGE_RXABORT | PURGE_RXCLEAR);
    ClearCommError(hComm,NULL,NULL);
    CancelIo(hComm);

    Sleep(5000);

    if (ov_read.hEvent!=INVALID_HANDLE_VALUE) CloseHandle(ov_read.hEvent);
    if (ov_write.hEvent!=INVALID_HANDLE_VALUE) CloseHandle(ov_write.hEvent);
    if (hComm!=INVALID_HANDLE_VALUE) CloseHandle(hComm);

#else

    ::close(port_d);

#endif

    cm_is_open=false;
}

// ------------------------------------------------------
void _rs_port::set_baud(int rate)
{
#if defined(WINDOWS)
    if (!GetCommState(hComm,&dcb)) throw _rs::_error(_Z_ERROR_GET_STATE,"_Z_ERROR_GET_STATE");
    dcb.BaudRate=rate;
    if (!SetCommState(hComm,&dcb)) throw _rs::_error(_Z_ERROR_SET_STATE,"_Z_ERROR_SET_STATE");
#else
    termios options;

    int r = tcgetattr(port_d,&options);
    if (r) throw _rs::_error(_Z_ERROR_GET_STATE,"_Z_ERROR_GET_STATE");

	r = cfsetispeed(&options,rate);
    if (r) throw _rs::_error(_Z_ERROR_GET_STATE,"_Z_ERROR_SET_STATE");

	options.c_ospeed=rate;
    r = cfsetospeed(&options,rate);
    if (r) throw _rs::_error(_Z_ERROR_GET_STATE,"_Z_ERROR_SET_STATE");

    r = tcsetattr(port_d, TCSANOW, &options);
    if (r) throw _rs::_error(_Z_ERROR_GET_STATE,"_Z_ERROR_SET_STATE");
#endif
}

// ------------------------------------------------------
void _rs_port::set_params(int parity,int data_l,int stop_bits)
{
#if defined(WINDOWS)
    if (!GetCommState(hComm,&dcb)) throw _rs::_error(_Z_ERROR_GET_STATE,"_Z_ERROR_GET_STATE");
    dcb.fBinary=TRUE;
    dcb.fParity=TRUE;
    dcb.Parity=parity;
    dcb.ByteSize=data_l;
    dcb.StopBits=stop_bits;
    if (!SetCommState(hComm,&dcb)) throw _rs::_error(_Z_ERROR_SET_STATE,"_Z_ERROR_SET_STATE");
#else
    termios options;

    int r = tcgetattr(port_d,&options);
    if (r) throw _rs::_error(_Z_ERROR_GET_STATE,"_Z_ERROR_GET_STATE");

	cfmakeraw(&options);
	options.c_cflag&=IGNPAR;

    r = tcsetattr(port_d, TCSANOW, &options);
    if (r) throw _rs::_error(_Z_ERROR_GET_STATE,"_Z_ERROR_SET_STATE");
#endif
}

// ------------------------------------------------------
unsigned int _rs_port::get_c()
{

#if defined(WINDOWS)

    unsigned long r;
    unsigned char c;
    int           e;
    COMSTAT       cs;

    ClearCommError(hComm,&r,&cs);
    if (!cs.cbInQue) return _Z_NO_DATA;

    e=ReadFile(hComm,&c,1,&r,&ov_read);
    if (!e)
    {
        e=GetLastError();
        if (e==ERROR_IO_PENDING)
            GetOverlappedResult(hComm,&ov_read,&r,TRUE);
        else
            throw _rs::_error(_Z_ERROR_READ,"_Z_ERROR_READ");
     }

     if (!r) throw _rs::_error(_Z_ERROR_READ,"_Z_ERROR_READ");

     return c;

#else

     // check input buffer
     int bytes;
     int r = ioctl(port_d, FIONREAD, &bytes);
     if (r==-1) throw _rs::_error(_Z_ERROR_READ,"_Z_ERROR_READ");

     // empty
     if (!bytes) return _Z_NO_DATA;

     // read one char
     unsigned char c;
     r = read(port_d,&c,sizeof(c));

     if (r<=0) throw _rs::_error(_Z_ERROR_READ,"_Z_ERROR_READ");

     //cout << " " << hex << (int)c << " " << endl;

     return c;

     /*
     fd_set readset;

     FD_ZERO(&readset);
     FD_SET(port_d,&readset);

     timeval timeout;
     timeout.tv_sec=0;
     timeout.tv_usec=0;

     int r = select(port_d+1,&readset,NULL,NULL,&timeout);
     */

#endif
}

// ------------------------------------------------------
void _rs_port::put_c(unsigned char c)
{

#if defined(WINDOWS)

    unsigned long w;
    int           e;

    ClearCommError(hComm,NULL,NULL);

    if (!WriteFile(hComm,&c,1,&w,&ov_write))
    {
        e=GetLastError();
        if (e==ERROR_IO_PENDING)
        {
            if (!GetOverlappedResult(hComm,&ov_write,&w,TRUE)) throw _rs::_error(_Z_ERROR_WRITE,"_Z_ERROR_WRITE");
        }
        else throw _rs::_error(_Z_ERROR_WRITE,"_Z_ERROR_WRITE");
     }

     if (!w) throw _rs::_error(_Z_ERROR_WRITE,"_Z_ERROR_WRITE");

#else

    fd_set writeset;

    FD_ZERO(&writeset);
    FD_SET(port_d,&writeset);

    // we wait until write can done
    int r = select(port_d+1,NULL,&writeset,NULL,NULL);
    if (r==-1) throw _rs::_error(_Z_ERROR_WRITE,"_Z_ERROR_WRITE"); // interrupt was

    // and write
    if (FD_ISSET(port_d,&writeset))
    {
        r = write(port_d,&c,sizeof(c));
        if (r==-1) throw _rs::_error(_Z_ERROR_WRITE,"_Z_ERROR_WRITE");
    }

#endif
}

// ------------------------------------------------------
void _rs_port::put_s(const unsigned char *s,int l)
{

#if defined(WINDOWS)

    unsigned long w;

    ClearCommError(hComm,NULL,NULL);

    if (!WriteFile(hComm,s,l,&w,&ov_write))
    {
        int e=GetLastError();
        if (e==ERROR_IO_PENDING)
        {
            if (!GetOverlappedResult(hComm,&ov_write,&w,TRUE))
            {
                //int z=GetLastError();
                //w=e;
                throw _rs::_error(_Z_ERROR_WRITE,"_Z_ERROR_WRITE");
            }
        }
        else
        {
            //int i=e;
            //w=1;
            throw _rs::_error(_Z_ERROR_WRITE,"_Z_ERROR_WRITE");
        }
     }

     if (!w) throw _rs::_error(_Z_ERROR_WRITE,"_Z_ERROR_WRITE");

#else

    fd_set writeset;
    int pos=0;

    while(1)
    {
        FD_ZERO(&writeset);
        FD_SET(port_d,&writeset);

        // we wait until write can done
        int r = select(port_d+1,NULL,&writeset,NULL,NULL);
        if (r==-1) throw _rs::_error(_Z_ERROR_WRITE,"_Z_ERROR_WRITE"); // interrupt was

        // and write
        if (FD_ISSET(port_d,&writeset))
        {
            r = write(port_d,&s[pos],l);
            if (r==-1) throw _rs::_error(_Z_ERROR_WRITE,"_Z_ERROR_WRITE");
        }

        l-=r;
        if (!l) break;

        pos+=r;
    }
#endif

}

// ------------------------------------------------------
void _rs_port::put_s(const std::string &s)
{
    put_s((const unsigned char *)s.data(),s.length());
}

// ------------------------------------------------------
void _rs_port::get_s(std::string &s,unsigned int time_ms)
{

#if defined(WINDOWS)

    int   r;
    int   e;
    COMSTAT        cs;

    s="";

    int b_len=0;
    unsigned char *tmp=NULL;

    int b_time=GetTickCount();

    while(1)
    {
        ClearCommError(hComm,(unsigned long *)&r,&cs);
        if (!cs.cbInQue) break;

        if (cs.cbInQue>b_len)
        {
            b_len=cs.cbInQue;
            if (tmp) delete tmp;
            tmp=new unsigned char [b_len];
        }

        e=ReadFile(hComm,tmp,b_len,(unsigned long *)&r,&ov_read);
        if (!e)
        {
             e=GetLastError();
             if (e==ERROR_IO_PENDING)
                GetOverlappedResult(hComm,&ov_read,(unsigned long *)&r,TRUE);
             else throw _rs::_error(_Z_ERROR_READ,"_Z_ERROR_READ");
        }

        if (!r) throw _rs::_error(_Z_ERROR_READ,"_Z_ERROR_READ");

        s+=std::string((char *)tmp,r);

        if (time_ms==0) break;
        if (GetTickCount()-b_time>=time_ms) break;
    }

#else

    char *tmp = NULL;
    int tmp_len = 0;

    _time end_time;
    end_time.set_now();
    end_time.tick(time_ms*1000);

    while(1)
    {
        // check input buffer
        int bytes;
        int r = ioctl(port_d, FIONREAD, &bytes);
        if (r==-1) throw _rs::_error(_Z_ERROR_READ,"_Z_ERROR_READ");

        // empty
        if (!bytes) break;

        if (bytes>tmp_len)
        {
            tmp_len=bytes;
            if (tmp) delete tmp;
            tmp = new char [tmp_len];
        }

        r = read(port_d,tmp,tmp_len);
        if (r<=0) throw _rs::_error(_Z_ERROR_READ,"_Z_ERROR_READ");

        s+=std::string(tmp,r);

        if (!time_ms) break;

        _time now;
        now.set_now();
        if (now>end_time) break;
    }

#endif
}

// ------------------------------------------------------
// ------------------------------------------------------
_rs::_port *_rs::create_rs_port()
{
    return new _rs_port;
}
