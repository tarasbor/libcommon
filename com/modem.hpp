#include <com/rs.hpp>
#include <list>

using namespace std;

namespace _modem
{
    class _error:public _rs::_error
    {
        public:
        
          _error(int err_n,const std::string &str):_rs::_error(err_n,str) {};
    };

    class _device:public _rs::_port
    {
        private:
        public:

          _device() {};
          virtual ~_device() {};

          virtual void open(int port_n)=0;
          virtual void close()=0;

          virtual void set_baud(int rate)=0;
          virtual void set_params(int parity,int data_l,int stop_bits)=0;
          
          virtual int  get_c()=0;
          virtual void put_c(char c)=0;

          virtual void get_s(std::string &s,unsigned int time_ms)=0;
          virtual void put_s(const std::string &s)=0;

          virtual void cmd(const std::string &s,unsigned int time_ms,const list<std::string> *answers,std::string &answer)=0;
    };

    _device *create_device();
    
};

