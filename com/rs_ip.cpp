#include "rs_ip.hpp"
#include <net_line_tcp.h>

using namespace std;

// ------------------------------------------------------
// ------------------------------------------------------
class _rs_ip_port:public _rs::_port
{
    private:

        _net_line *con;

        std::string cm_in_buffer;

        bool cm_is_open;

		std::string cm_open_as;

    public:

      _rs_ip_port():con(NULL),cm_is_open(false) {};
      virtual ~_rs_ip_port() { if (cm_is_open) close(); };

      virtual void open(int port_n);
      virtual void open(const std::string &port_name);
      virtual void close();
	  virtual const std::string &open_as() const { return cm_open_as; };
	  virtual bool is_open() const { return cm_is_open; };

      virtual void set_baud(int rate) {};
      virtual void set_params(int parity,int data_l,int stop_bits) {};

      virtual unsigned int  get_c();
      virtual void put_c(unsigned char c);

      virtual void get_s(std::string &s,unsigned int time_ms);
      virtual void put_s(const std::string &s);
      virtual void put_s(const unsigned char *s,int l);
};

// ------------------------------------------------------
void _rs_ip_port::open(const std::string &port_name)
{
    if (cm_is_open) close();

    try
    {
        con = create_socket_line();

        _tcp_adress adr(port_name);
        con->connect(adr);
    }
    catch(_nerror &e)
    {
        throw _rs::_error(_Z_ERROR_OPEN_COM,"_Z_ERROR_OPEN_COM");
    }

	cm_open_as=port_name;
	cm_in_buffer.clear();
    cm_is_open=true;
}

// ------------------------------------------------------
void _rs_ip_port::open(int port_n)
{
    throw _rs::_error(_Z_ERROR_WRONG_OPER,"_Z_ERROR_WRONG_OPER");
}

// ------------------------------------------------------
void _rs_ip_port::close()
{
    if (!cm_is_open) return;

    if (con)
    {
        try
        {
            con->shutdown();
            con->release();
            con=NULL;
        }
        catch(_nerror &e)
        {
            throw _rs::_error(_Z_ERROR_CLOSE_COM,"_Z_ERROR_CLOSE_COM");
        }
    }

    cm_in_buffer.clear();

    cm_is_open=false;
}

// ------------------------------------------------------
unsigned int _rs_ip_port::get_c()
{
    if (!cm_is_open) throw _rs::_error(_Z_ERROR_READ,"_Z_ERROR_READ");

    if (!cm_in_buffer.empty())
    {
        unsigned int r=cm_in_buffer[0];
        cm_in_buffer.erase(0,1);

        return r;
    }

    // cm_in_buffer - empty
    try
    {
        bool r;
        timeval tv={0,0};

        con->is_ready(&r,NULL,NULL,tv);
        if (!r) return _Z_NO_DATA;

        con->recv(cm_in_buffer);
        if (cm_in_buffer.empty()) throw _rs::_error(_Z_ERROR_READ,"_Z_ERROR_READ");
    }
    catch(_nerror &e)
    {
        throw _rs::_error(_Z_ERROR_READ,"_Z_ERROR_READ");
    }

    // cm_in_buffer - not empty
    unsigned int r=cm_in_buffer[0];
    cm_in_buffer.erase(0,1);

    return r;
}

// ------------------------------------------------------
void _rs_ip_port::put_c(unsigned char c)
{
    put_s((const unsigned char *)&c,1);
}

// ------------------------------------------------------
void _rs_ip_port::put_s(const unsigned char *s,int l)
{
    put_s(string((const char *)s,l));
}

// ------------------------------------------------------
void _rs_ip_port::put_s(const std::string &s)
{
    try
    {
        con->send(s);
    }
    catch(_nerror &e)
    {
        throw _rs::_error(_Z_ERROR_WRITE,"_Z_ERROR_WRITE");
    }
}

// ------------------------------------------------------
void _rs_ip_port::get_s(std::string &s,unsigned int time_ms)
{
    throw _rs::_error(_Z_ERROR_WRONG_OPER,"_Z_ERROR_WRONG_OPER");
}

// ------------------------------------------------------
// ------------------------------------------------------
_rs::_port *_rs::create_rs_ip_port()
{
    return new _rs_ip_port;
}
