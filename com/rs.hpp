#ifndef   __RS_COMM_CLASS__
#define   __RS_COMM_CLASS__

#if defined(WINDOWS)
#include <windows.h>
#else
#include <termios.h>
#endif

#include <string>
#include <cstdio>

namespace _rs
{

    class _error
    {
        private:
          int n;
          std::string st;

        public:

          _error(int err_n,const std::string &str):n(err_n),st(str) {};

          int err_no() const { return n; };

          const std::string &err_str() const { return st; };

          std::string err() const
          {
              char tmp[32];
              std::sprintf(tmp,"%i",n);
              return std::string("Error #: ")+tmp+". "+st;
          }

    };

    #define  _Z_OK                   0
    #define  _Z_ERROR_OPEN_COM       0xFF01
    #define  _Z_ERROR_CREATE_EVENT   0xFF02
    #define  _Z_ERROR_GET_STATE      0xFF03
    #define  _Z_ERROR_SET_STATE      0xFF04
    #define  _Z_ERROR_SET_QUERY      0xFF05
    #define  _Z_ERROR_WRITE          0xFF06
    #define  _Z_ERROR_READ           0xFF07
    #define  _Z_ERROR_NO_FREE_MEM    0xFF08
    #define  _Z_ERROR_OPEN_FILE      0xFF09
    #define  _Z_ERROR_WRONG_OPER     0xFF0A
    #define  _Z_ERROR_CLOSE_COM      0xFF0B
    #define  _Z_NO_DATA              0xFF0C
    #define  _Z_NO_ANSWER            0xFF0D

    enum baud_rate
    {
        #if defined(WINDOWS)
        b110 = CBR_110,
        b300 = CBR_300,
        b600 = CBR_600,
        b1200 = CBR_1200,
        b2400 = CBR_2400,
        b4800 = CBR_4800,
        b9600 = CBR_9600,
        b14400 = CBR_14400,
        b19200 = CBR_19200,
        b38400 = CBR_38400,
        b57600 = CBR_57600,
        b115200 = CBR_115200,
        b128000 = CBR_128000,
        b256000 = CBR_256000
        #else
        b0 = B0,
        b50 = B50,
        b75 = B75,
        b110 = B110,
        b134 = B134,
        b150 = B150,
        b200 = B200,
        b300 = B300,
        b600 = B600,
        b1200 = B1200,
        b1800 = B1800,
        b2400 = B2400,
        b4800 = B4800,
        b9600 = B9600,
        b19200 = B19200,
        b38400 = B38400,
        b57600 = B57600,
        b115200 = B115200,
        b230400 = B230400,
        b460800 = B460800,
        b500000 = B500000,
        b576000 = B576000,
        b921600 = B921600,
        b1000000 = B1000000,
        b1152000 = B1152000,
        b1500000 = B1500000,
        b2000000 = B2000000,
        b2500000 = B2500000,
        b3000000 = B3000000,
        b3500000 = B3500000,
        b4000000 = B4000000
        #endif
    };

    enum parity
    {
        #if defined(WINDOWS)
        no_parity = NOPARITY,
        odd_parity = ODDPARITY,
        even_parity = EVENPARITY,
        mark_parity = MARKPARITY,
        space_parity = SPACEPARITY
        #else
        #endif
    };

    enum stop_bits
    {
        #if defined(WINDOWS)
        stop_bits_1 = ONESTOPBIT,
        stop_bits_15 = ONE5STOPBITS,
        stop_bits_2 = TWOSTOPBITS,
        #else
        #endif
    };


    class _port
    {
        private:
        public:

          _port() {};
          virtual ~_port() {};

          virtual void open(int port_n)=0;
          virtual void open(const std::string &port_name)=0;
          virtual void close()=0;
		  virtual const std::string &open_as() const = 0;
		  virtual bool is_open() const = 0;

          virtual void set_baud(int rate)=0;
          virtual void set_params(int parity,int data_l,int stop_bits)=0;

          virtual unsigned int  get_c()=0;
          virtual void put_c(unsigned char c)=0;

          virtual void get_s(std::string &s,unsigned int time_ms)=0;
          virtual void put_s(const std::string &s)=0;
          virtual void put_s(const unsigned char *s,int len)=0;
    };

    _port *create_rs_port();
};

#endif
