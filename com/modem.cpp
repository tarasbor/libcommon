#include "modem.hpp"

// ---------------------------------------------------------
class _device_imp:public _modem::_device
{
    private:

     _rs::_port *p;
    
    public:

      _device_imp():p(_rs::create_rs_port()) {};
      virtual ~_device_imp() { if (p) delete p; };

      virtual void open(int port_n) { p->open(port_n); };
      virtual void close() { p->close(); };

      virtual void set_baud(int rate) { p->set_baud(rate); };
      virtual void set_params(int parity,int data_l,int stop_bits) { p->set_params(parity,data_l,stop_bits); };
      
      virtual int  get_c() { return p->get_c(); };
      virtual void put_c(char c) { p->put_c(c); };

      virtual void get_s(std::string &s,unsigned int time_ms) { p->get_s(s,time_ms); };
      virtual void put_s(const std::string &s) { p->put_s(s); };

      virtual void cmd(const std::string &s,unsigned int time_ms,const list<std::string> *answers,std::string &answer);
};

// ---------------------------------------------------------
void _device_imp::cmd(const std::string &s,unsigned int time_ms,const list<std::string> *answers,std::string &answer)
{
    
}

// ---------------------------------------------------------
// ---------------------------------------------------------
_modem::_device *_modem::create_device()
{
    return new _device_imp;
}

