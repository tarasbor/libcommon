#ifndef   __RS_IFACE_COMM_CLASS__
#define   __RS_IFACE_COMM_CLASS__

#include "rs.hpp"

namespace _rs
{
    class _iface
    {
        public:

          _iface() {};
          virtual ~_iface() {};

		  virtual void set_params(int rate,int parity,int data_l,int stop_bits) = 0;
		  virtual bool is_params_set() const = 0;

          virtual unsigned int  get_c()=0;
          virtual void put_c(unsigned char c)=0;

          virtual void get_s(std::string &s,unsigned int time_ms)=0;
          virtual void put_s(const std::string &s)=0;

          virtual void lock()=0;
          virtual void unlock()=0;
          virtual bool try_locked()=0;

		  //virtual _port *port();
    };

    _iface *create_rs_iface(const std::string &port_name);
};

#endif
