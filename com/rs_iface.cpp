#include "rs_iface.hpp"

#include <clthread.hpp>
#include <map>

using namespace std;

// ------------------------------------------------------
// ------------------------------------------------------
class _iface_imp:public _rs::_iface
{
    private:

      struct _status
      {
          _locker lock;
          int ref_count;
		  bool params_is_set;
      };

      typedef map<_rs::_port *,_status *> _ports;
      static _ports ports;

      _rs::_port *port;

    public:

      _iface_imp(const string &port_name);
      virtual ~_iface_imp();

	  virtual void set_params(int rate,int parity,int data_l,int stop_bits);
	  virtual bool is_params_set() const;

      virtual unsigned int  get_c();
      virtual void put_c(unsigned char c);

      virtual void get_s(std::string &s,unsigned int time_ms);
      virtual void put_s(const std::string &s);

      virtual void lock();
      virtual void unlock();
      virtual bool try_locked();

	  static _rs::_port *find_port_by_name(const std::string port_name);
};

_iface_imp::_ports _iface_imp::ports;

// ------------------------------------------------------
_iface_imp::_iface_imp(const string &port_name)
{
	_ports::iterator it=ports.begin();
	while(it!=ports.end())
	{
		if (it->first->open_as()==port_name) break;
		it++;
	}

	if (it!=ports.end())
	{
		it->second->ref_count++;
		port=it->first;
		return;
	}

	_status *s=new _status;

	port=_rs::create_rs_port();
	port->open(port_name);

	s->ref_count=1;
	s->params_is_set=false;
	ports[port]=s;
}

// ------------------------------------------------------
_iface_imp::~_iface_imp()
{
    _ports::iterator it=ports.find(port);
    if (it!=ports.end())
    {
        it->second->ref_count--;
        if (it->second->ref_count==0)
        {
            delete it->second;
            ports.erase(it);
        }
    }
}

// ------------------------------------------------------
void _iface_imp::set_params(int rate,int parity,int data_l,int stop_bits)
{
	_ports::iterator it=ports.find(port);
    if (it!=ports.end())
    {
		it->second->lock.lock();
        try
        {
            port->set_baud(rate);
            //port->set_params(parity,data_l,stop_bits);
        }
        catch(_rs::_error &e)
        {
            it->second->lock.unlock();
            throw(e);
        }
        it->second->lock.unlock();
    }
}

// ------------------------------------------------------
bool _iface_imp::is_params_set() const
{
	_ports::iterator it=ports.find(port);
    if (it!=ports.end()) return it->second->params_is_set;

	return false;
}

// ------------------------------------------------------
unsigned int  _iface_imp::get_c()
{
    unsigned int res;

    _ports::iterator it=ports.find(port);
    if (it!=ports.end())
    {
        it->second->lock.lock();
        try
        {
            res=port->get_c();
        }
        catch(_rs::_error &e)
        {
            it->second->lock.unlock();
            throw(e);
        }
        it->second->lock.unlock();
    }

    return res;
}

// ------------------------------------------------------
void _iface_imp::put_c(unsigned char c)
{
    _ports::iterator it=ports.find(port);
    if (it!=ports.end())
    {
        it->second->lock.lock();
        try
        {
            port->put_c(c);
        }
        catch(_rs::_error &e)
        {
            it->second->lock.unlock();
            throw(e);
        }
        it->second->lock.unlock();
    }
}

// ------------------------------------------------------
void _iface_imp::get_s(std::string &s,unsigned int time_ms)
{
    _ports::iterator it=ports.find(port);
    if (it!=ports.end())
    {
        it->second->lock.lock();
        try
        {
            port->get_s(s,time_ms);
        }
        catch(_rs::_error &e)
        {
            it->second->lock.unlock();
            throw(e);
        }
        it->second->lock.unlock();
    }
}

// ------------------------------------------------------
void _iface_imp::put_s(const std::string &s)
{
    _ports::iterator it=ports.find(port);
    if (it!=ports.end())
    {
        it->second->lock.lock();
        try
        {
            port->put_s(s);
        }
        catch(_rs::_error &e)
        {
            it->second->lock.unlock();
            throw(e);
        }
        it->second->lock.unlock();
    }
}

// ------------------------------------------------------
void _iface_imp::lock()
{
    _ports::iterator it=ports.find(port);
    if (it!=ports.end())
    {
        it->second->lock.lock();
    }
}

// ------------------------------------------------------
void _iface_imp::unlock()
{
    _ports::iterator it=ports.find(port);
    if (it!=ports.end())
    {
        it->second->lock.unlock();
    }
}

// ------------------------------------------------------
bool _iface_imp::try_locked()
{
    _ports::iterator it=ports.find(port);
    if (it!=ports.end())
    {
        return it->second->lock.try_lock();
    }
    return false;
}

// ------------------------------------------------------
_rs::_port *_iface_imp::find_port_by_name(const std::string port_name)
{
	_ports::iterator it=ports.begin();
	while(it!=ports.end())
	{
		if (it->first->open_as()==port_name) return it->first;
		it++;
	}
	return NULL;
}

// ------------------------------------------------------
_rs::_iface *_rs::create_rs_iface(const std::string &port_name)
{
	return new _iface_imp(port_name);
}


//
