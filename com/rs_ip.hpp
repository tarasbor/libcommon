#ifndef   __RS_IP_COMM_CLASS__
#define   __RS_IP_COMM_CLASS__

#include "rs.hpp"

namespace _rs
{
    _port *create_rs_ip_port();
};

#endif
