/***************************************************************************
 *   What is it: This file is part of communication lib.
 *   Content: common (global) classes for ssl
 *   Version: 0.1b
 *   Last-Mod: 24.09.2006
 *                                                                         
 *   Copyright (C) 2006 by Proshin Alexey                                  
 *   paa713@mail.ru                                                        
 *                                                                         
 *   The contents of this file are subject to free software; 
     you can redistribute it and/or modify  
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or     
 *   (at your option) any later version.                                   
 *                                                                         
 *   The contents of this file is distributed in the hope that it will be useful,       
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
 *   GNU General Public License for more details.                          
 *                                                                         
 *   You should have received a copy of the GNU General Public License     
 *   along with this program; if not, write to the                         
 *   Free Software Foundation, Inc.,                                       
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
 ***************************************************************************/

#include <string>
#include <net_lib_ssl.h>

// ssl set error class (for ssl errors)
// -------------------------------------------------------
class _nssl_set_error:public _nerror
{
  private:
	std::string operation;
	int error_number;

  public:        
	_nssl_set_error(const std::string _oper):error_number(ERR_get_error()),operation(_oper) {};
	virtual ~_nssl_set_error() {};
	
	virtual std::string err_st() const { return std::string(ERR_error_string(error_number,NULL)); };
	virtual std::string err_st_full() const { return std::string(ERR_error_string(error_number,NULL))+
                                            std::string(" on command ")+operation; };
	virtual long err_no() const { return error_number; };

};

// ---------------------------------------------------
// ssl settings holder implementation
// ---------------------------------------------------
class _ssl_set_imp:public _ssl_set
{
  friend _ssl_set *create_ssl_set(_ssl_set::version v,_ssl_set::type t);
  
  private:
  
    static bool is_lib_init;
    static int ref_count;
    SSL_CTX *ssl_ctx;
  
    _ssl_set_imp(_ssl_set::version v,_ssl_set::type t);
    virtual ~_ssl_set_imp();
    
  public:
    virtual void pub_certificate_from_file(const char *fname,file_format format);
    virtual void RSA_private_key_from_file(const char *fname,file_format format);
    
    virtual void set_verify(verify_type);
    
    virtual SSL *new_ssl() { return SSL_new(ssl_ctx); };
      
    virtual void release() { delete this; };
};

int _ssl_set_imp::ref_count=0;
bool _ssl_set_imp::is_lib_init=false;

// ---------------------------------------------------
_ssl_set_imp::_ssl_set_imp(_ssl_set::version v,_ssl_set::type t):_ssl_set(v,t),ssl_ctx(NULL)
{
  
  if (is_lib_init==false)
  {
     SSL_load_error_strings();
     if (!SSL_library_init()) throw(_nssl_set_error("SSL_library_init()"));
     is_lib_init=true;
  }

  const SSL_METHOD *m;
  if (t==_ssl_set::server)
  {
    if (v==_ssl_set::ssl2) m=SSLv2_server_method();
    else if (v==_ssl_set::ssl3) m=SSLv3_server_method();
    else if (v==_ssl_set::tls1) m=TLSv1_server_method();
    else if (v==_ssl_set::all) m=SSLv23_server_method();
  }
  else if (t==_ssl_set::client)
  {
    if (v==_ssl_set::ssl2) m=SSLv2_client_method();
    else if (v==_ssl_set::ssl3) m=SSLv3_client_method();
    else if (v==_ssl_set::tls1) m=TLSv1_client_method();
    else if (v==_ssl_set::all) m=SSLv23_client_method();
  }
  else if (t==_ssl_set::both)
  {
    if (v==_ssl_set::ssl2) m=SSLv2_method();
    else if (v==_ssl_set::ssl3) m=SSLv3_method();
    else if (v==_ssl_set::tls1) m=TLSv1_method();
    else if (v==_ssl_set::all) m=SSLv23_client_method();
  }
  
  ssl_ctx=SSL_CTX_new(m);
}

// ---------------------------------------------------
_ssl_set_imp::~_ssl_set_imp()
{
  if (ssl_ctx) { SSL_CTX_free(ssl_ctx); ssl_ctx=NULL; }
}

// ---------------------------------------------------
void _ssl_set_imp::pub_certificate_from_file(const char *fname,file_format format)
{
  if (SSL_CTX_use_certificate_file(ssl_ctx,fname,format)!=1)
  {
    _nssl_set_error e("SSL_CTX_use_certificate_file() in _ssl_set::pub_certificate_from_file");    
    throw(e);
  }
}

// ---------------------------------------------------
void _ssl_set_imp::RSA_private_key_from_file(const char *fname,file_format format)
{
  if (SSL_CTX_use_RSAPrivateKey_file(ssl_ctx,fname,format)!=1)
    throw(_nssl_set_error("SSL_CTX_use_RSAPrivateKey_file() in _ssl_set::RSA_private_key_from_file()"));
}

void _ssl_set_imp::set_verify(verify_type v)
{
  SSL_CTX_set_verify(ssl_ctx,v,NULL);
}

// make function for ssl settings holder
_ssl_set *create_ssl_set(_ssl_set::version v,_ssl_set::type t) { return new _ssl_set_imp(v,t); };
