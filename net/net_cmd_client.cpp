#include "net_cmd_client.hpp"
#include "net_line_tcp.h"
//#include <map>
#include <clthread.hpp>
#include <stdio.h>
#include <time.h>

#include <iostream>

using namespace std;

// net cmd error
// -------------------------------------------------------
class _ncmd_error:public _nerror
{
  private:
        std::string operation;
        long error_number;

  public:
        _ncmd_error(const std::string &_oper,long error):operation(_oper),error_number(error) {};
        virtual std::string err_st() const { char tmp[256]; sprintf(tmp,"error #%i",error_number);
                                             return std::string(tmp); };
        virtual std::string err_st_full() const { char tmp[256]; sprintf(tmp,"error #%i",error_number);
                                                  return std::string(tmp)+
                                                  std::string(" on ")+operation; };
        virtual long err_no() const { return error_number; };
};

// network connection
// -------------------------------------------------------------------
class _nc:public _net_cmd_client
{
    private:

      _net_line *con;
      bool _is_connected;
      bool _is_broken;
      _locker lock;

	  string cm_cmd_end;
	  int cm_timeout;

    public:

      _nc():con(NULL),_is_connected(false),_is_broken(false) {};
      ~_nc();

      virtual void connect(const _net_adress &adress);
	  virtual void disconnect();

      virtual bool is_connected() const { return _is_connected; };
      virtual bool is_broken() const { return _is_broken; };

      virtual bool cmd(const string &command,string &rez);
      virtual void cmd_timeout_ms(int t) { cm_timeout=t; };
      virtual void cmd_end(const string &e) { cm_cmd_end=e; };

      virtual void release() { delete this;};

};

// -------------------------------------------------------------------
_nc::~_nc()
{
    if (con)
    {
        if (_is_connected)
        {
            disconnect();
        }
        else
        {
            con->shutdown();
            con->release();
            con=NULL;
        }
    }
}

// -------------------------------------------------------------------
void _nc::connect(const _net_adress &adress)
{
    if (_is_connected) return;

    lock.lock();

    try
    {
        if (con) con->release();
        con=NULL;

        con = create_socket_line();
        con->connect(adress);

		lock.unlock();
    }
    catch(_nerror &ne)
    {
        _is_broken=true;

        lock.unlock();
        throw(_ncmd_error(ne.err_st_full(),ne.err_no()));
    }

    _is_connected=true;
    _is_broken=false;
}


// -------------------------------------------------------------------
void _nc::disconnect()
{
    _is_broken=false;

    if (!_is_connected) return;
    if (!con) return;

    lock.lock();
    con->shutdown();
    if (con) con->release();
    con=NULL;
    lock.unlock();
}

// -------------------------------------------------------------------
bool _nc::cmd(const string &cmd,string &rez)
{
    if (!_is_connected || _is_broken)
    {
		throw(_ncmd_error("_net_cmd_client::cmd - connection is not connected or broken",0));
    }

    lock.lock();
    try
    {
        if (cmd.length())
        {
            //cout << "try send..." << endl;
            bool r_write;
            bool r_error;
            timeval tv={0,0};
            con->is_ready(NULL,&r_write,&r_error,tv);

            if (!r_write) throw _ncmd_error("_net_cmd_client::cmd - can not send...socket do not want accept data",0);
            if (r_error) throw _ncmd_error("_net_cmd_client::cmd - can not send...exceptfds = true",0);

            con->send(cmd+cm_cmd_end);
            //cout << "send...ok" << endl;
        }
    }
    catch(_nerror &ne)
    {
		_is_broken=true;
		lock.unlock();
		throw(_ncmd_error(string("_net_cmd_client::cmd - can not send - ")+ne.err_st_full(),0));
    }

    rez.clear();
    std::string tmp;

    time_t begin_time=time(NULL);

    while(1)
    {
        try
        {
            //cout << time(NULL) << "try recv..." << endl;
            bool r_read;
            bool r_error;
            timeval tv={10,0};  // 10 seconds timeout to wait answer command from server
            con->is_ready(&r_read,NULL,&r_error,tv);

            if (r_error) throw _ncmd_error("_net_cmd_client::cmd - can not recv...exceptfds = true",0);

            if (r_read)
            {
                //cout << time(NULL) << "try recv...have data" << endl;
                con->recv(tmp);

                if (tmp.empty())
                {
                    _is_broken=true;
                    lock.unlock();
                    throw(_ncmd_error("_net_cmd_client::cmd - can not recv - recv empty data - connection closed by remoute side",0));
                }

                rez+=tmp;
                if (rez.find(cm_cmd_end)!=std::string::npos)
                {
                    lock.unlock();
                    return true;
                }
                tmp.clear();
            }
            else
            {
                cout << "_net_cmd_client::cmd - can not recv - no any data above 10 sec - MICRO timeout" << endl;
                //cout << time(NULL) << "try recv...no data yet cm_timeout=" << cm_timeout << endl;
                throw(_ncmd_error("_net_cmd_client::cmd - can not recv - no any data above 10 sec - MICRO timeout",0));
            }
        }
        catch(_nerror &ne)
        {
            _is_broken=true;
			lock.unlock();
			throw(_ncmd_error(string("_net_cmd_client::cmd - can not recv - ")+ne.err_st_full(),0));
        }

        // STUB
		if (time(NULL)-begin_time>cm_timeout)
        {
            _is_broken=true;
			lock.unlock();
            throw(_ncmd_error(string("_net_cmd_client::cmd - timeout"),0));
        }
    }
}

// --------------------------------------------------------
_net_cmd_client *create_net_cmd_client()
{
    return new _nc;
}

// --------------------------------------------------------
_net_cmd_client *create_simple_net_cmd_client()
{
	_net_cmd_client *r=new _nc;
	r->cmd_timeout_ms(10000);
	r->cmd_end(";\r\n");
	return r;
}

