/***************************************************************************
 *   This file contain release of class for http req parse and to make http answer
 *
 *   Copyright (C) 2006 by Proshin Alexey
 *   paa713@mail.ru
 *
 *   The contents of this file are subject to free software;
     you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   The contents of this file is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***************************************************************************/

#include <net_http.h>
#include <fstream>
#include <stdlib.h>
#include <codepage.hpp>
#include <text_help.hpp>
#include <iostream>
#include <file_utils.hpp> // for dirExists()
#include <algorithm>        // for std::replace()

#include <system/file_utils.hpp>

struct _http_state_code
{
    int code;
    std::string str;
};

class http_help_imp:public _http_help
{
    protected:

        static _http_state_code http_state_codes[];
        static string cm_html_charset;

    public:

        virtual void html_charset(const std::string &cp) { cm_html_charset=cp; };
        virtual const std::string &html_charset() const { return cm_html_charset; };

        virtual int req_get_muv(const std::string &req,std::string &method,std::string &url,std::string &version);
        virtual int req_get_agent(const std::string &req,std::string &userAgent);

        virtual _http_help_req_params *req_get_cookies(const std::string &req);
        virtual std::string req_get_cookie(const std::string &req,const std::string &name);

        virtual int http_data(const std::string &data,const std::string &ctype,std::string &rez);
        virtual int http_file(const std::string &root_dir,const std::string &file_name,std::string &rez);
        virtual int http_any_file(const std::string &dir,const std::string &file_name,std::string &rez);
        virtual int word_file(const std::string &root_dir,const std::string &file_name,std::string &rez);

        virtual void set_cookie(const _http_help_req_params *c,std::string &rez);

        virtual int http_by_state_code(int code,const std::string &comments,std::string &rez);

        virtual bool its_file(const std::string &url);

        virtual void virtual_include(const std::string &root_dir,std::string &rez);
        virtual void virtual_for(std::string &rez);

        virtual void update_content_len(std::string &rez);

        virtual std::string q_str(const std::string &in);

        virtual void release() { delete this; };

        /// For compatibility with old vc code
        virtual int http_not_found(std::string &rez);
        virtual std::string req_get_param(const list<std::string> &params,const list<std::string> &values,const std::string &param);
        virtual std::string param_from_codes(const std::string &s);
        virtual void req_params_multipart(const std::string &req,list<std::string> &params,list<std::string> &values);
        virtual void req_params(const std::string &req,list<std::string> &params,list<std::string> &values);
};

_http_state_code http_help_imp::http_state_codes[] =
{
 {100,"Continue"},
 {101,"Switching Protocols"},
 {102,"Processing"},
 {105,"Name Not Resolved"},

 {200,"OK"},
 {201,"Created"},
 {202,"Accepted"},
 {203,"Non-Authoritative Information"},
 {204,"No Content"},
 {205,"Reset Content"},
 {206,"Partial Content"},
 {207,"Multi-Status"},
 {226,"IM Used"},

 {300,"Multiple Choices"},
 {301,"Moved Permanently"},
 {302,"Moved Temporarily"},
 {302,"Found"},
 {303,"See Other"},
 {304,"Not Modified"},
 {305,"Use Proxy"},
 {306,"Reserved"},
 {307,"Temporary Redirect"},

 {400,"Bad Request"},
 {401,"Unauthorized"},
 {402,"Payment Required"},
 {403,"Forbidden"},
 {404,"Not Found"},
 {405,"Method Not Allowed"},
 {406,"Not Acceptable"},
 {407,"Proxy Authentication Required"},
 {408,"Request Timeout"},
 {409,"Conflict"},
 {410,"Gone"},
 {411,"Length Required"},
 {412,"Precondition Failed"},
 {413,"Request Entity Too Large"},
 {414,"Request-URI Too Large"},
 {415,"Unsupported Media Type"},
 {416,"Requested Range Not Satisfiable"},
 {417,"Expectation Failed"},
 {418,"I'm a teapot"},
 {422,"Unprocessable Entity"},
 {423,"Locked"},
 {424,"Failed Dependency"},
 {425,"Unordered Collection"},
 {426,"Upgrade Required"},
 {428,"Precondition Required"},
 {429,"Too Many Requests"},
 {431,"Request Header Fields Too Large"},
 {434,"Requested host unavailable."},
 {449,"Retry With"},
 {451,"Unavailable For Legal Reasons"},
 {456,"Unrecoverable Error"},

 {500,"Internal Server Error"},
 {501,"Not Implemented"},
 {502,"Bad Gateway"},
 {503,"Service Unavailable"},
 {504,"Gateway Timeout"},
 {505,"HTTP Version Not Supported"},
 {506,"Variant Also Negotiates"},
 {507,"Insufficient Storage"},
 {508,"Loop Detected"},
 {509,"Bandwidth Limit Exceeded"},
 {510,"Not Extended"},
 {511,"Network Authentication Required"},

 {0,""}
};

string http_help_imp::cm_html_charset;

// ----------------------------------
int http_help_imp::req_get_muv(const std::string &req,std::string &method,std::string &url,std::string &version)
{
    std::string::size_type i;
    for(i=0;i<req.length();i++)
    {
        if (req[i]==' ') break;
    }

    method.assign(req,0,i);
    std::string::size_type j=i;
    i++;

    for(;i<req.length();i++)
    {
        if (req[i]==' ') break;
    }
    url.assign(req,j+1,i-j-1);

    j=i;
    i++;
    for(;i<req.length();i++)
    {
        if (req[i]==0x0D || req[i]==0x0A) break;
    }
    version.assign(req,j+1,i-j-1);

    return 0;
}

// ----------------------------------
int http_help_imp::req_get_agent(const std::string &req,std::string &userAgent)
{
    std::string::size_type i=req.find("User-Agent: ");
    if(i!=std::string::npos)   {
        std::string::size_type iend=req.find('\n',i+12);    // 12 = sizeof('User-Agent: ')
        if( iend!=std::string::npos ) {
            if(req[iend-1]=='\r')   // end of line -  \r\n
                iend--;
            userAgent.assign(req,i+12,iend-i-12);
        }
    }
    return 0;
}

// ----------------------------------
int http_help_imp::http_data(const std::string &data,const std::string &ctype,std::string &rez)
{
    rez="HTTP/1.1 200 ; Ok;\r\nContent-Type: ";
    rez+=ctype;
    rez+="\r\nAccess-Control-Allow-Origin: *\r\nContent-Length: ";
    rez+=to_string(data.length());

    if (ctype.find("application/font-woff")!=std::string::npos) // Fig Proshin Bug with failed binary files
    {
        rez+="\r\n\r\n";
        rez+=data;
    }
    else {
        rez+="\r\n\r\n";
        rez+=data;
        rez+="\r\n\r\n";
    }

    return 0;
}

// ----------------------------------
int http_help_imp::http_file(const std::string &root_dir,const std::string &file_name,std::string &rez)
{
    ifstream f;

    std::string f_name;
    if (file_name[0]=='\\' || file_name[0]=='/')
    {
        f_name=root_dir;                // SIGSEGV, SEGFAULT. file_name=/monitor/offices_in.html  root_dir=<optimized out>
        f_name+=&(file_name.c_str()[1]);
    }
    else f_name=file_name;

    if(dirExists(f_name.c_str()))   // fixed SEGFAULT when IE open  generated report .xls
        return -1;

    f.open((f_name).c_str(),ios::in | ios::binary);
    if (!f) { http_by_state_code(404,f_name,rez); return 0; }
    f.seekg(0,ios::end);
    int size=f.tellg();
    f.seekg(0,ios::beg);

    char *buf = new char [size];
    f.read(buf,size);

    f.close();

    std::string file_text=std::string(buf,size);
    delete buf;

    std::string ct="application/octet-stream";
    if      (file_name.find(".html")!=std::string::npos || file_name.find(".htm")!=std::string::npos ||
             file_name.find(".HTML")!=std::string::npos || file_name.find(".HTM")!=std::string::npos ||
             file_name.find(".SHTML")!=std::string::npos || file_name.find(".SHTM")!=std::string::npos ||
             file_name.find(".shtml")!=std::string::npos || file_name.find(".shtm")!=std::string::npos)
    {
        virtual_include(root_dir,file_text);
        virtual_for(file_text);
        ct="text/html;";
        if (!cm_html_charset.empty())
        {
            ct+=" charset=";
            ct+=cm_html_charset; // charset=windows-1251";
        }
    }
    else if (file_name.find(".jpg")!=std::string::npos || file_name.find(".JPG")!=std::string::npos) ct="image/jpeg";
    else if (file_name.find(".gif")!=std::string::npos || file_name.find(".GIF")!=std::string::npos) ct="image/gif";
    else if (file_name.find(".png")!=std::string::npos || file_name.find(".PNG")!=std::string::npos) ct="image/png";
	else if (file_name.find(".xls")!=std::string::npos || file_name.find(".XLS")!=std::string::npos) ct="application/vnd.ms-excel";
	else if (file_name.find(".css")!=std::string::npos || file_name.find(".CSS")!=std::string::npos) ct="text/css";
	else if (file_name.find(".json")!=std::string::npos || file_name.find(".JSON")!=std::string::npos)    {
        ct="application/json;";
        if (!cm_html_charset.empty())
        {
            ct+=" charset=";
            ct+=cm_html_charset; // charset=windows-1251";
        }
    }
    else if (file_name.find(".xml")!=std::string::npos || file_name.find(".XML")!=std::string::npos)    {
        ct="application/xml;";
        if (!cm_html_charset.empty())
        {
            ct+=" charset=";
            ct+=cm_html_charset; // charset=windows-1251";
        }
    }
    else if (file_name.find(".js")!=std::string::npos || file_name.find(".JS")!=std::string::npos)    {
        ct="application/javascript;";
        if (!cm_html_charset.empty())
        {
            ct+=" charset=";
            ct+=cm_html_charset; // charset=windows-1251";
        }
    }
    //  Fix Bug with failed binary files:
    //    Failed to decode downloaded font: http://192.168.3.150:47080/assets/bootstrap/dist/fonts/glyphicons-halflings-regular.woff OTS parsing error: incorrect file size in WOFF header
    else if (file_name.find(".woff")!=std::string::npos || file_name.find(".WOFF")!=std::string::npos) ct="application/font-woff";



    http_data(file_text,ct,rez);

    return 0;
}


int http_help_imp::http_any_file(const std::string &dir,const std::string &file_name,std::string &rez)
{
    ifstream f;
    std::string f_name;
    f_name = dir;
    if( f_name[f_name.size()-1]!='/' ) f_name += '/';    
    f_name += file_name;
    _text_help::replaceAll(f_name, "..", "");  // remove any relevant path. Only absolute allowed.

    if(dirExists(f_name.c_str()))   // fixed SEGFAULT when IE open generated report .xls
        return -1;

    f.open(f_name.c_str(),ios::in | ios::binary);
    if (!f) { http_by_state_code(404,"File '" + f_name + "' not founded.",rez); return -1; }
    f.seekg(0,ios::end);
    int size=f.tellg();
    f.seekg(0,ios::beg);

    char *buf = new char [size];
    f.read(buf,size);

    f.close();

    std::string file_text=std::string(buf,size);
    delete buf;

    std::string ct="application/octet-stream";
    /*if      (file_name.find(".html")!=std::string::npos || file_name.find(".htm")!=std::string::npos ||
             file_name.find(".HTML")!=std::string::npos || file_name.find(".HTM")!=std::string::npos ||
             file_name.find(".SHTML")!=std::string::npos || file_name.find(".SHTM")!=std::string::npos ||
             file_name.find(".shtml")!=std::string::npos || file_name.find(".shtm")!=std::string::npos)
    {
        virtual_include(root_dir,file_text);
        virtual_for(file_text);
        ct="text/html;";
        if (!cm_html_charset.empty())
        {
            ct+=" charset=";
            ct+=cm_html_charset; // charset=windows-1251";
        }
    }
    else*/ if (file_name.find(".jpg")!=std::string::npos || file_name.find(".JPG")!=std::string::npos) ct="image/jpeg";
    else if (file_name.find(".gif")!=std::string::npos || file_name.find(".GIF")!=std::string::npos) ct="image/gif";
    else if (file_name.find(".png")!=std::string::npos || file_name.find(".PNG")!=std::string::npos) ct="image/png";
    else if (file_name.find(".xls")!=std::string::npos || file_name.find(".XLS")!=std::string::npos) ct="application/vnd.ms-excel";
    else if (file_name.find(".css")!=std::string::npos || file_name.find(".CSS")!=std::string::npos) ct="text/css";
    else if (file_name.find(".json")!=std::string::npos || file_name.find(".JSON")!=std::string::npos)    {
        ct="application/json;";
        if (!cm_html_charset.empty())
        {
            ct+=" charset=";
            ct+=cm_html_charset; // charset=windows-1251";
        }
    }
    else if (file_name.find(".xml")!=std::string::npos || file_name.find(".XML")!=std::string::npos)    {
        ct="application/xml;";
        if (!cm_html_charset.empty())
        {
            ct+=" charset=";
            ct+=cm_html_charset; // charset=windows-1251";
        }
    }
    else if (file_name.find(".js")!=std::string::npos || file_name.find(".JS")!=std::string::npos)    {
        ct="application/javascript;";
        if (!cm_html_charset.empty())
        {
            ct+=" charset=";
            ct+=cm_html_charset; // charset=windows-1251";
        }
    }
    //  Fix Bug with failed binary files:
    //    Failed to decode downloaded font: http://192.168.3.150:47080/assets/bootstrap/dist/fonts/glyphicons-halflings-regular.woff OTS parsing error: incorrect file size in WOFF header
    else if ( file_name.find(".woff")!=std::string::npos || file_name.find(".WOFF")!=std::string::npos ) ct = "application/font-woff";
    else if ( file_name.find(".wav")!=std::string::npos ) ct = "audio/wav";

    http_data(file_text,ct,rez);

    return 0;
}


// ----------------------------------
int http_help_imp::word_file(const std::string &root_dir,const std::string &file_name,std::string &rez)
{
    ifstream f;

    std::string f_name;
    if (file_name[0]=='\\' || file_name[0]=='/')
    {
        f_name=root_dir;
        f_name+=&(file_name.c_str()[1]);
    }
    else f_name=file_name;

    f.open((f_name).c_str(),ios::in | ios::binary);
    if (!f) { http_by_state_code(404,f_name,rez); return 0; }
    f.seekg(0,ios::end);
    int size=f.tellg();
    f.seekg(0,ios::beg);

    char *buf = new char [size];
    f.read(buf,size);

    f.close();

    std::string file_text=std::string(buf,size);
    delete buf;

    std::string ct="Content-type: application/msword\r\n";
    if (file_name.find(".doc")!=std::string::npos || file_name.find(".DOC")!=std::string::npos)
    {
        virtual_include(root_dir,file_text);
        virtual_for(file_text);
    }

    http_data(file_text,ct,rez);

    return 0;
}

// ----------------------------------------------------
int http_help_imp::http_by_state_code(int code,const std::string &comments,std::string &rez)
{
    int n=0;
    while(http_state_codes[n].code!=0)
    {
        if (http_state_codes[n].code==code)
        {
            rez="HTTP/1.1 ";
            rez+=to_string(code)+" "+http_state_codes[n].str+"\r\n";
            rez+="Content-type: text/html;\r\n";

            std::string com="<html><body><h1>";
            if (comments.empty()) com+=http_state_codes[n].str; else com+=comments;
            com+="</h1></body></html>\r\n";

            rez+="Content-Length: ";
            rez+=to_string(com.length())+"\r\n";
            rez+="\r\n\r\n";
            rez+=com;

            return 0;
        }
        n++;
    }

    return http_by_state_code(500,"Unknow state code",rez);
}


// uncomment for compatibility with old code
// ----------------------------------------------------
int http_help_imp::http_not_found(std::string &rez)
{

    rez="HTTP/1.1 404 ; Object not found;\r\n";
    rez+="Content-type: text/html;\r\n";
    rez+="Content-Length: 47\r\n";
    rez+="\r\n";
    rez+="<html><body>Object not found...</body></html>\r\n";

    return 0;
}


// ----------------------------------------------------
bool http_help_imp::its_file(const std::string &url)
{
    if (url.find("?")!=std::string::npos) return false;
    if (url.find("&")!=std::string::npos) return false;
    return true;
}

// ----------------------------------------------------
std::string trim(const std::string &s)
{
    std::string::size_type pos;
    for(pos=0;pos<s.length();pos++)
      if (s[pos]!=' ') break;

    if (pos==s.length()) return "";

    std::string::size_type pos2;
    for(pos2=s.length()-1;pos2>pos;pos2--)
      if (s[pos2]!=' ') break;

    return std::string(s,pos,pos2-pos+1);
}

// ----------------------------------------------------
// Added for compatibility with old code vc projects
std::string http_help_imp::req_get_param(const list<std::string> &params,const list<std::string> &values,const std::string &param)
{
    list<std::string>::const_iterator it_p=params.begin();
    list<std::string>::const_iterator it_v=values.begin();

    while(it_p!=params.end() && it_v!=values.end())
    {
        if ((*it_p)==param) return trim(*it_v);
        it_v++;
        it_p++;
    }

    return "";
}

// ----------------------------------------------------
// Added for compatibility with old code vc projects
std::string http_help_imp::param_from_codes(const std::string &s)
{
    //cout << "params from codes:" << endl;
    //cout << "in = " << s << endl;
    std::string rez=s;

    _codepage::from_url_params_format(rez);

    //cout << "from url params = " << rez << endl;

    // ON *NIX is_uft8 function do not return right result - disable convert from utf8
    // TODO - fix it
    /*
    if (_codepage::is_utf8(rez))
    {
        //cout << "is in utf" << endl;
        _codepage::utf8_to_win1251(rez);
        //cout << "from utf = " << rez << endl;
    }
    */

    /*
    int pos=0;
    while(pos<s.length())
    {
        if (s[pos]=='%')
        {
            if (pos+1<s.length() && s[pos+1]=='%')
            {
                rez+='%';
                pos++;
            }
            else if (pos+2<s.length())
            {
                /* utf8 version
                unsigned char c=(unsigned char)( (cc_code(s[pos+1])<<4) + cc_code(s[pos+2]));
                std::string uni_str;
                int n;
                pos+=3;
                uni_str+=c;

                if      ((c & 0xF0) == 0xF0)
                {
                    n=3;
                }
                else if ((c & 0xE0) == 0xE0)
                {
                    n=2;
                }
                else if ((c & 0xC0) == 0xC0)
                {
                    n=1;
                }
                else
                {
                    n=0;
                }

                sprintf(ls,"c=%i, n=%i",(int)c,n);
                _log().out(ls,0,0);

                while(n)
                {
                    if (pos+2>=s.length()) break;
                    c=(unsigned char)( (cc_code(s[pos+1])<<4) + cc_code(s[pos+2]));
                    uni_str+=c;
                    pos+=3;
                    n--;
                }

                _codepage::utf8_to_win1251(uni_str);

                rez+=uni_str;
                pos--;

                _log().out(rez,0,0);
                ...

                unsigned char c=(unsigned char)( (_codepage::cc_code(s[pos+1])<<4) + _codepage::cc_code(s[pos+2]));
                pos+=2;
                rez+=c;
            }
        }
        else if (s[pos]=='+') rez+=' ';
        else rez+=s[pos];

        pos++;
    }

    //_log().out(rez,0,0);
    */

    return rez;
}


// ----------------------------------------------------
// Added for compatibility with old code vc projects
void http_help_imp::req_params_multipart(const std::string &req,list<std::string> &params,list<std::string> &values)
{
    std::string::size_type pos=req.find("Content-Type: multipart/form-data; boundary=");
    pos+=44;
    std::string::size_type pos2=req.find("\r\n",pos);

    std::string boundary_b="--"+std::string(req,pos,pos2-pos);

    while(1)
    {
        pos=req.find(boundary_b,pos);
        if (pos==std::string::npos) break;
        pos+=boundary_b.length();
        std::string::size_type pos2=req.find(boundary_b,pos)+boundary_b.length();

        std::string part=std::string(req,pos,pos2-pos);
        std::string::size_type name_pos=part.find("Content-Disposition: form-data; name=\"");
        if (name_pos==std::string::npos) continue;
        name_pos+=38;
        pos2=part.find('"',name_pos);
        std::string param_name=std::string(part,name_pos,pos2-name_pos);

        if (part[pos2+1]==';')
        {
            name_pos=part.find("filename=\"");
            if (name_pos!=std::string::npos)
            {
                name_pos+=10;
                pos2=part.find('"',name_pos);
                params.push_back(param_name+".filename");
                values.push_back(std::string(part,name_pos,pos2-name_pos));
            }

            name_pos=part.find("Content-Type: ");
            if (name_pos!=std::string::npos)
            {
                name_pos+=14;
                pos2=part.find("\r\n",name_pos);
                params.push_back(param_name+".content-type");
                values.push_back(std::string(part,name_pos,pos2-name_pos));
            }
        }

        name_pos=part.find("\r\n\r\n");
        std::string param_value;
        if (name_pos!=std::string::npos)
        {
            name_pos+=4;
            pos2=part.find("\r\n"+boundary_b,name_pos);
            param_value=std::string(part,name_pos,pos2-name_pos);
        }

        params.push_back(param_name);
        values.push_back(param_value);
        //cout << "param " << param_name << " size = " << param_value.length() << endl << flush;

        //cout << "pn!" << param_name << "!" << endl;
        //cout << "pv!" << param_value << "!" << endl;
    }
}


// ----------------------------------------------------
// Added for compatibility with old code vc projects
void http_help_imp::req_params(const std::string &req,list<std::string> &params,list<std::string> &values)
{
    params.clear();
    values.clear();

    std::string m;
    std::string u;
    std::string v;

    req_get_muv(req,m,u,v);


    std::string p_str;

    if (m=="GET")
    {
        std::string::size_type b_pos=u.find("?");
        if (b_pos==std::string::npos) return;

        b_pos++;
        p_str=u.substr(b_pos,u.length()-b_pos);

    }
    else if (m=="POST")
    {
        if (req.find("Content-Type: multipart/form-data")!=std::string::npos)
        {
            req_params_multipart(req,params,values);
            return;
        }

        std::string::size_type p_beg=req.find("\r\n\r\n");
        std::string::size_type p_end;
        if (p_beg==std::string::npos)
        {
            p_beg=req.find("\n\n");
            if (p_beg==std::string::npos) return;

            p_beg+=2;
            p_end=req.find("\n",p_beg);
            if (p_end==std::string::npos) p_end=req.length();

        } else
        {
            p_beg+=4;
            p_end=req.find("\r",p_beg);
            if (p_end==std::string::npos) p_end=req.length();
        }

        p_str=req.substr(p_beg,p_end-p_beg);
    }

    std::string::size_type b_pos=0;
    while(1)
    {
          std::string::size_type e_pos=p_str.find("&",b_pos);
          if (e_pos==std::string::npos) e_pos=p_str.length();

          std::string::size_type eq_pos=p_str.find("=",b_pos);
          if (eq_pos==std::string::npos) eq_pos=e_pos; else eq_pos++;

          params.push_back(param_from_codes(p_str.substr(b_pos,eq_pos-b_pos-1)));
          values.push_back(param_from_codes(p_str.substr(eq_pos,e_pos-eq_pos)));

          //cout << p_str.substr(b_pos,eq_pos-b_pos-1) << " " << p_str.substr(eq_pos,e_pos-eq_pos) << endl;

          b_pos=e_pos;
          if (b_pos==p_str.length()) break;
          b_pos++;
   }

   /*list<std::string>::iterator it=params.begin();
   list<std::string>::iterator it1=values.begin();
   while(it!=params.end())
   {
       cout << (*it) << " " << (*it1) << endl;
       it++;
       it1++;
   }*/

}


// ----------------------------------------------------
void http_help_imp::virtual_for(std::string &rez)
{
    while(1)
    {
        std::string::size_type pos=rez.find("<!--#for(");

        if (pos==std::string::npos) break;

        std::string::size_type begin_pos=pos;
        std::string::size_type param_pos=pos+9;

        pos=rez.find('=',param_pos);
        std::string param(rez,param_pos,pos-param_pos);
        param.insert(0,"%");

        int begin_param_value_pos=pos+1;
        pos=rez.find(';',pos+1);
        int begin_param_value=atoi(std::string(rez,begin_param_value_pos,pos-begin_param_value_pos).c_str());

        int end_param_value_pos = pos = rez.find('<',pos)+1;
        pos=rez.find(';',pos+1);
        int end_param_value=atoi(std::string(rez,end_param_value_pos,pos-end_param_value_pos).c_str());

        std::string::size_type tag_end_pos=rez.find("-->",pos);
        tag_end_pos+=3;

        int for_end_pos=rez.find("<!-- } -->",tag_end_pos);

        std::string txt;

        for(int i=begin_param_value;i<end_param_value;i++)
        {
            char st[16];
            sprintf(st,"%i",i);

            std::string tmp(rez,tag_end_pos,for_end_pos-tag_end_pos);

            while(1)
            {
                std::string::size_type param_pos=tmp.find(param);
                if (param_pos==std::string::npos) break;

                tmp.replace(param_pos,param.length(),st);
            }

            txt+=tmp;
        }

        rez.replace(begin_pos,for_end_pos+10-begin_pos,txt);

    }
}

// ----------------------------------------------------
void http_help_imp::virtual_include(const std::string &root_dir,std::string &rez)
{
    while(1)
    {
        std::string::size_type pos=rez.find("<!--#include virtual=\"");
        const int d_len=22;
        if (pos==std::string::npos) break;
        pos+=d_len;

        std::string::size_type e_pos=rez.find('"',pos);
        if (e_pos==std::string::npos) e_pos=rez.length()-1;
        std::string file_name(rez,pos,e_pos-pos);
//        cout << "f name = " << f_name << endl;

        std::string::size_type de_pos=rez.find("-->",pos);
        if (de_pos==std::string::npos) de_pos=rez.length()-4;  //( +3 next = -1);
        de_pos+=3;

        ifstream f;
        std::string f_name;
        if (file_name[0]=='\\' || file_name[0]=='/')
        {
            f_name=root_dir;
            f_name+=&(file_name.c_str()[1]);
        }
        else f_name=file_name;

        f.open(f_name.c_str(),ios::in | ios::binary);
        if (!f)
        {
//            cout << "de_pos = " << de_pos << "d len = " << de_pos-pos+d_len << endl;
            rez.replace(pos-d_len,de_pos-pos+d_len,"<!-- include not found ... -->");

//            cout << rez << endl;

            continue;
        }

        f.seekg(0,ios::end);
        int size=f.tellg();
        f.seekg(0,ios::beg);

        char *buf=new char [size];
        f.read(buf,size);

        rez.replace(pos-d_len,de_pos-pos+d_len,std::string(buf,size));

        delete buf;

        f.close();
    }
}

// ----------------------------------------------------
void http_help_imp::update_content_len(std::string &rez)
{
    std::string::size_type pos=rez.find("\r\n\r\n");
    if (pos==std::string::npos) return;
    pos+=4;

    std::string::size_type c_len=rez.length()-pos;
    pos=rez.find("Content-Length: ");
    if (pos==std::string::npos) return;
    pos+=16;
    int pos_s=rez.find('\r',pos);

    char tmp[16];
    sprintf(tmp,"%i",c_len);
    rez.replace(pos,pos_s-pos,tmp);
}

// ----------------------------------------------------
std::string http_help_imp::q_str(const std::string &in)
{
    std::string::size_type pos;
    std::string tmp=in;
    while(1)
    {
        pos=tmp.find('"');
        if (pos==std::string::npos) break;
        tmp.replace(pos,1,"&quot");
    }

    return tmp;

}

// ----------------------------------------------------
_http_help_req_params *http_help_imp::req_get_cookies(const std::string &req)
{
    std::string::size_type he_pos=req.find("\r\n\r\n");     // header end
    if (he_pos==std::string::npos) return NULL;             // wrong header

    std::string::size_type cbpos=req.find("Cookie: ");
    if (cbpos==std::string::npos) return NULL;              // no cookie at all

    if (cbpos>he_pos) return NULL;                          // "Cookie: " text after header end - may be it document content

    std::string::size_type cepos=req.find("\r\n",cbpos);
    if (cepos==std::string::npos) return NULL;              // wrong header - no \r\n at cookie string in header

    map<std::string,std::string> cm;
    _text_help::string_to_map(std::string(req,cbpos,cepos-cbpos),cm);

    if (cm.empty()) return NULL;

    _http_help_req_params *p = create_http_help_req_params();
    p->set(cm);

    return p;
}

// ----------------------------------------------------
std::string http_help_imp::req_get_cookie(const std::string &req,const std::string &cname)
{
    std::string::size_type he_pos=req.find("\r\n\r\n");     // header end
    if (he_pos==std::string::npos) return "";               // wrong header

    std::string::size_type cbpos=req.find("Cookie: ");
    if (cbpos==std::string::npos) return "";                // no cookie at all

    if (cbpos>he_pos) return "";                            // "Cookie: " text after header end - may be it document content

    std::string::size_type cepos=req.find("\r\n",cbpos);
    if (cepos==std::string::npos) return "";                // wrong header - no \r\n at cookie string in header

    return _text_help::get_param_value_from_st(std::string(req,cbpos,cepos-cbpos),cname);
}

// ----------------------------------------------------
void http_help_imp::set_cookie(const _http_help_req_params *c,std::string &rez)
{
    // make set cookie string
    std::string cstr="Set-Cookie: ";
    _http_help_req_params::_cont::const_iterator pit=c->begin();
    while(1)
    {
        cstr+=pit->first;
        cstr+="=";
        cstr+=pit->second;
        pit++;

        if (pit==c->end())
        {
            cstr+="\r\n";
            break;
        }
        cstr+="; ";
    }

    // insert into rez
    std::string::size_type pos=rez.find("\r\n");
    if (pos==std::string::npos) return;         // wrong header in rez - no new line

    pos+=2;
    if (pos>=rez.length()) return;              // wrong header in rez - new line only at end

    rez.insert(pos,cstr);
}

// ----------------------------------------------------
// make functions
// ----------------------------------------------------

_http_help *create_http_help()
{
    return new http_help_imp;
}

// ----------------------------------------------------
// ----------------------------------------------------
class _hh_req_params:public _http_help_req_params
{
    protected:
        _cont cm_params;
        static std::string cm_empty_val;

        virtual void parse_multipart(_http_help *hh,const std::string &req);
        virtual void parse_urlencoded(_http_help *hh,const std::string &s,const std::string &req_charset);
        virtual std::string param_from_codes(const std::string &s);

    public:
        _hh_req_params() {};
        virtual ~_hh_req_params() {};

        virtual void parse(_http_help *hh,const std::string &req);

        virtual void set(const std::string &p,const std::string &v) { cm_params[p]=v; };
        virtual void set(const _cont &c) { cm_params=c; };
        virtual const std::string &get(const std::string &p) const;

        virtual _cont::iterator begin() { return cm_params.begin(); };
        virtual _cont::iterator end() { return cm_params.end(); };

        virtual _cont::const_iterator begin() const { return cm_params.begin(); };
        virtual _cont::const_iterator end() const { return cm_params.end(); };

        virtual void release() { delete this; };
};

string _hh_req_params::cm_empty_val;

// ----------------------------------------------------
const std::string &_hh_req_params::get(const std::string &p) const
{
    _cont::const_iterator it=cm_params.find(p);
    if (it==cm_params.end()) return cm_empty_val;
    return it->second;
}



// ----------------------------------------------------
void _hh_req_params::parse(_http_help *hh,const std::string &req)
{
    cm_params.clear();

    std::string m;
    std::string u;
    std::string v;

    hh->req_get_muv(req,m,u,v);

    std::string p_str;

    if (m=="GET")
    {
        std::string::size_type b_pos=u.find("?");
        if (b_pos==std::string::npos) return;

        b_pos++;
        p_str=u.substr(b_pos,u.length()-b_pos);

        parse_urlencoded(hh,p_str,hh->html_charset());
    }
    else if (m=="POST")
    {        
        if (req.find("Content-Type: multipart/form-data")!=std::string::npos)
        {
            parse_multipart(hh,req);
        }


        //std::string::size_type ct_pos=req.find("Content-Type: application/x-www-form-urlencoded");
        std::string::size_type ct_pos = _text_help::ci_find_substr_char(req,"Content-Type: application/x-www-form-urlencoded");
        if (ct_pos!=std::string::npos)
        {
            std::string req_charset=hh->html_charset();
            std::string::size_type chset_pos=req.find(" charset=",ct_pos);
            if (chset_pos!=std::string::npos)
            {
                chset_pos+=9;
                std::string::size_type chset_pos2=req.find_first_of("\r ;",chset_pos);
                if (chset_pos2!=std::string::npos)
                {
                    req_charset=std::string(req,chset_pos,chset_pos2-chset_pos);
                }
            }

            std::string::size_type p_beg=req.find("\r\n\r\n");
            std::string::size_type p_end;
            if (p_beg==std::string::npos)
            {
                p_beg=req.find("\n\n");
                if (p_beg==std::string::npos) return;

                p_beg+=2;
                p_end=req.find("\n",p_beg);
                if (p_end==std::string::npos) p_end=req.length();

            } else
            {
                p_beg+=4;
                p_end=req.find("\r",p_beg);
                if (p_end==std::string::npos) p_end=req.length();
            }

            p_str=req.substr(p_beg,p_end-p_beg);

            parse_urlencoded(hh,p_str,req_charset);
        }
    }
}

// ----------------------------------------------------
void _hh_req_params::parse_urlencoded(_http_help *hh,const std::string &p_str,const std::string &req_charset)
{
    bool convert_charset=false;
    if (req_charset!=hh->html_charset()) convert_charset=true;

    std::string::size_type b_pos=0;
    while(1)
    {
        std::string::size_type e_pos=p_str.find("&",b_pos);
        if (e_pos==std::string::npos) e_pos=p_str.length();

        std::string::size_type eq_pos=p_str.find("=",b_pos);
        if (eq_pos==std::string::npos) eq_pos=e_pos; else eq_pos++;

        std::string p_name  = param_from_codes(p_str.substr(b_pos,eq_pos-b_pos-1));
        std::string p_value = param_from_codes(p_str.substr(eq_pos,e_pos-eq_pos));

        if (convert_charset)
        {
            if (req_charset=="UTF-8" && hh->html_charset()=="windows-1251")
            {
                _codepage::utf8_to_win1251(p_name);
                _codepage::utf8_to_win1251(p_value);
            }
        }

        cm_params [ p_name ] = p_value;

        b_pos=e_pos;
        if (b_pos==p_str.length()) break;
        b_pos++;
   }
}

// ----------------------------------------------------
void _hh_req_params::parse_multipart(_http_help *hh,const std::string &req)
{
    //cout << req << endl;
    std::string::size_type pos=req.find("Content-Type: multipart/form-data;");
    pos=req.find(" boundary=",pos);
    pos+=10;
    std::string::size_type pos2=req.find("\r\n",pos);

    //cout << "pos=" << pos << " pos2=" << pos2 << endl;

    std::string boundary_b="--"+std::string(req,pos,pos2-pos);
    //cout << "boundary_b=" << boundary_b << endl;

    bool convert_charset=false;

    std::string req_charset=hh->html_charset();
    std::string::size_type chset_pos=req.find(" charset=");
    if (chset_pos!=std::string::npos)
    {
        chset_pos+=9;
        std::string::size_type chset_pos2=req.find(';',chset_pos);
        if (chset_pos2!=std::string::npos)
        {
            req_charset=std::string(req,chset_pos,chset_pos2-chset_pos);
            if (req_charset!=hh->html_charset()) convert_charset=true;
        }
    }

    while(1)
    {
        bool convert_value_charset=true;

        pos=req.find(boundary_b,pos);
        if (pos==std::string::npos) break;
        pos+=boundary_b.length();
        std::string::size_type pos2=req.find(boundary_b,pos)+boundary_b.length();

        std::string part=std::string(req,pos,pos2-pos);

        std::string::size_type name_pos=part.find("Content-Disposition: form-data; name=\"");
        if (name_pos==std::string::npos) continue;
        name_pos+=38;
        pos2=part.find('"',name_pos);
        std::string param_name=std::string(part,name_pos,pos2-name_pos);

        if (part[pos2+1]==';')
        {
            name_pos=part.find("filename=\"");
            if (name_pos!=std::string::npos)
            {
                name_pos+=10;
                pos2=part.find('"',name_pos);
                cm_params[param_name+".filename"] = std::string(part,name_pos,pos2-name_pos);
            }

            name_pos=part.find("Content-Type: ");
            if (name_pos!=std::string::npos)
            {
                name_pos+=14;
                pos2=part.find("\r\n",name_pos);
                cm_params[param_name+".content-type"] = std::string(part,name_pos,pos2-name_pos);
            }

            // do not convert files & raw data
            convert_value_charset=false;
        }

        name_pos=part.find("\r\n\r\n");
        std::string param_value;
        if (name_pos!=std::string::npos)
        {
            name_pos+=4;
            pos2=part.find("\r\n"+boundary_b,name_pos);
            param_value=std::string(part,name_pos,pos2-name_pos);
        }

        if (convert_charset && convert_value_charset)
        {
            // TODO - make right conversion - now just from UTF-8 to windows-1251
            if (req_charset=="UTF-8" && hh->html_charset()=="windows-1251") _codepage::utf8_to_win1251(param_value);
        }

        cm_params [param_name] = param_value;
    }

}

// ----------------------------------------------------
std::string _hh_req_params::param_from_codes(const std::string &s)
{
	//cout << "params from codes:" << endl;
	//cout << "in = " << s << endl;
    std::string rez=s;

    _codepage::from_url_params_format(rez);

	//cout << "from url params = " << rez << endl;

	// ON *NIX is_uft8 function do not return right result - disable convert from utf8
	// TODO - fix it
	/*
    if (_codepage::is_utf8(rez))
    {
		//cout << "is in utf" << endl;
        _codepage::utf8_to_win1251(rez);
		//cout << "from utf = " << rez << endl;
    }
	*/

    /*
    int pos=0;
    while(pos<s.length())
    {
        if (s[pos]=='%')
        {
            if (pos+1<s.length() && s[pos+1]=='%')
            {
                rez+='%';
                pos++;
            }
            else if (pos+2<s.length())
            {
                /* utf8 version
                unsigned char c=(unsigned char)( (cc_code(s[pos+1])<<4) + cc_code(s[pos+2]));
                std::string uni_str;
                int n;


                pos+=3;
                uni_str+=c;

                if      ((c & 0xF0) == 0xF0)
                {
                    n=3;
                }
                else if ((c & 0xE0) == 0xE0)
                {
                    n=2;
                }
                else if ((c & 0xC0) == 0xC0)
                {
                    n=1;
                }
                else
                {
                    n=0;
                }

                sprintf(ls,"c=%i, n=%i",(int)c,n);
                _log().out(ls,0,0);

                while(n)
                {
                    if (pos+2>=s.length()) break;
                    c=(unsigned char)( (cc_code(s[pos+1])<<4) + cc_code(s[pos+2]));
                    uni_str+=c;
                    pos+=3;
                    n--;
                }

                _codepage::utf8_to_win1251(uni_str);

                rez+=uni_str;
                pos--;

                _log().out(rez,0,0);
                ...

                unsigned char c=(unsigned char)( (_codepage::cc_code(s[pos+1])<<4) + _codepage::cc_code(s[pos+2]));
                pos+=2;
                rez+=c;
            }
        }
        else if (s[pos]=='+') rez+=' ';
        else rez+=s[pos];

        pos++;
    }

    //_log().out(rez,0,0);
    */

	return rez;
}

// ----------------------------------------------------
// make functions
// ----------------------------------------------------
_http_help_req_params *create_http_help_req_params()
{
    return new _hh_req_params;
}
