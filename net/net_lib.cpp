/***************************************************************************
 *   What is it: This file is part of communication lib.
 *   Content: error & log class release
 *   Version: 0.1b
 *   Last-Mod: 24.09.2006
 *
 *   Copyright (C) 2006 by Proshin Alexey
 *   paa713@mail.ru
 *
 *   The contents of this file are subject to free software;
     you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   The contents of this file is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***************************************************************************/

#include "net_lib.h"

#if defined(WINDOWS)
#include <windows.h>
#include <winsock.h>
#else
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

#include <string.h>
#include <stdio.h>

using namespace std;

// net line error class (for socket errors)
// -------------------------------------------------------
class _nlib_error:public _nerror
{
  private:
        string operation;
        int error_number;

  public:
        _nlib_error(int err,const string &_oper):operation(_oper),error_number(err) {};
        virtual string err_st() const { return operation; };
        virtual string err_st_full() const
		{
			char tmp[64];
			sprintf(tmp,"%i",error_number);
			return string(tmp)+" on "+operation;
		};
        virtual long err_no() const { return error_number; };
};

// --------------------------------------------------------------------------
_tcp_adress::_tcp_adress(const std::string& host_and_port)
{
	string s;

    std::string::size_type pos = host_and_port.find(':');
    if (pos==string::npos) throw(_nlib_error(0,"Incorrect tcp address format (_tcp_adress::_tcp_adress(const std::string& host_and_port))"));
    s.assign(host_and_port,0,pos);
    port.assign(host_and_port,pos+1,host_and_port.length()-pos);

    hostent *host = gethostbyname(s.c_str());
    if (!host) throw(_nlib_error(0,string("Unknown host ")+s));
    in_addr addr;
    memcpy(&addr,host->h_addr_list[0],sizeof(in_addr));

	ip=inet_ntoa(addr);
}
