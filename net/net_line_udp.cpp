/***************************************************************************
 *   What is it: This file is part of communication lib.
 *   Content: releases classes for network line (point-point)
 *   Version: 0.1b
 *   Last-Mod: 24.09.2006
 *
 *   Copyright (C) 2006 by Proshin Alexey
 *   paa713@mail.ru
 *
 *   The contents of this file are subject to free software;
     you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   The contents of this file is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***************************************************************************/

#include "net_line.h"
#include <stdio.h>

using namespace std;

#ifdef WINDOWS
        #include <windows.h>
        #include <winsock.h>
#else
        #include <sys/types.h>
        #include <sys/socket.h>
        #include <string>
        #include <errno.h>
        #include <netinet/in.h>
        #include <arpa/inet.h>
        #include <openssl/ssl.h>
        #include <openssl/err.h>
        #include <unistd.h>

        typedef int SOCKET;
        #define INVALID_SOCKET -1
        #define SOCKET_ERROR -1
#endif

#ifndef NET_LINE_UDP_MAX_RECV
#define NET_LINE_UDP_MAX_RECV 512
#endif

// init class for windows socket lib.
// Its need to call WSAStartup one time befor any socket API function call.
// And WSACleanup in end of application
#ifdef WINDOWS
class _wsa_init_udp
{
  public:
    _wsa_init_udp();
    ~_wsa_init_udp() { WSACleanup();};
};

_wsa_init_udp::_wsa_init_udp()
{
    WORD wVersionRequested;
    WSADATA wsaData;
    int err;

    wVersionRequested = MAKEWORD( 2, 0 );

    err = WSAStartup( wVersionRequested, &wsaData );
    if ( err != 0 ) return;

    if ( LOBYTE( wsaData.wVersion ) != 2 || HIBYTE( wsaData.wVersion ) != 0 )
    {
       WSACleanup( );
       return;
    }
}
static _wsa_init_udp wsa_init_udp;
#endif

// net line error class (for socket errors)
// -------------------------------------------------------
class _nline_error:public _nerror
{
#ifdef WINDOWS
  private:
	    long error_number;
        std::string operation;


  public:
        _nline_error(const std::string &_oper):error_number(WSAGetLastError()),operation(_oper) {};
        virtual std::string err_st() const { char tmp[256]; sprintf(tmp,"error #%li",error_number);
                                             return std::string(tmp); };
        virtual std::string err_st_full() const { char tmp[256]; sprintf(tmp,"error #%li",error_number);
                                                  return std::string(tmp)+
                                                  std::string(" on command ")+operation; };
        virtual long err_no() const { return error_number; };
#else
  private:
        std::string operation;
        long error_number;

  public:
        _nline_error(const std::string &_oper):error_number(errno),operation(_oper) {};
        virtual std::string err_st() const { return std::string(strerror(error_number)); };
        virtual std::string err_st_full() const { return std::string(strerror(error_number))+
                                            std::string(" on command ")+operation; };
        virtual long err_no() const { return error_number; };
#endif
};

// ------------------------------------------------------------------
// simple udp socket class
// ------------------------------------------------------------------
class _simple_udp:public _net_line
{
  private:
        SOCKET s;

        //_tcp_adress tmp_adress;
		_tcp_adress ta_lo_adress;
		_tcp_adress ta_re_adress;

		sockaddr_in lo_adress;
		sockaddr_in re_adress;

        _simple_udp(const _simple_udp &_s);// { };
        _simple_udp &operator=(const _simple_udp &s);// { return *this; };

        const SOCKET handle() const { return s; };

        operator SOCKET() const { return s; };

        ~_simple_udp() { shutdown(); };

  public:
        _simple_udp();
        _simple_udp(SOCKET h):s(h),ta_lo_adress("",""),ta_re_adress("","") {};

        virtual void connect(const _net_adress &adress);
        virtual void stay_server_at(const _net_adress &adress);

        virtual void send(const std::string &data);
        virtual void recv(std::string &data,int max_size=0);

        virtual void shutdown();

        _net_line *accept();

        virtual void release() { delete this; };

        virtual void is_ready(bool *read,bool *write,bool *error,timeval &tv);

        virtual const _net_adress *remoute_adress();
        virtual const _net_adress *local_adress();

        virtual void set_block_mode(bool mode);
        virtual void allow_broadcast(bool allow);
};

// ---------------------------------------------------
// ---------------------------------------------------
_simple_udp::_simple_udp()
:s(INVALID_SOCKET),ta_lo_adress("",""),ta_re_adress("","")
{
    s=socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP);
    if (s==INVALID_SOCKET) throw _nline_error("socket() in _simple_udp::connect()");
}

// ---------------------------------------------------
void _simple_udp::connect(const _net_adress &adress)
{
    memset(&re_adress,0,sizeof(re_adress));
    re_adress.sin_family=AF_INET;
    re_adress.sin_port=htons(atoi(adress.get(1).c_str()));

    re_adress.sin_addr.s_addr=inet_addr(adress.get(0).c_str());

	ta_re_adress=_tcp_adress(adress.get(0),adress.get(1));
}

// ---------------------------------------------------
void _simple_udp::shutdown()
{
#ifdef WINDOWS
        if (s!=INVALID_SOCKET) { ::closesocket(s); s=INVALID_SOCKET; }
#else
        if (s!=INVALID_SOCKET) { ::close(s); s=INVALID_SOCKET; }
#endif

}

// ---------------------------------------------------
void _simple_udp::send(const string &data)
{
    int err;
    int pos=0;

    int psize=data.length();
	if (psize>NET_LINE_UDP_MAX_RECV) psize=NET_LINE_UDP_MAX_RECV;

	while(1)
	{
        err=::sendto(s,data.data()+pos,psize,0,(sockaddr *)&re_adress,sizeof(re_adress));
		if (err==SOCKET_ERROR) throw _nline_error("send() in _simple_udp::sendto()");

        pos+=err;

		psize=data.length()-pos;
		if (psize==0) break;

		if (psize>NET_LINE_UDP_MAX_RECV) psize=NET_LINE_UDP_MAX_RECV;
	}
}

// ---------------------------------------------------
void _simple_udp::recv(string &data,int max_size)
{
    int err;
    data.clear();

    char tmp[NET_LINE_UDP_MAX_RECV];

#ifdef WINDOWS
    int saddr_len=sizeof(re_adress);
#else
    socklen_t saddr_len=sizeof(re_adress);
#endif

    while(1)
    {
        err=::recvfrom(s,tmp,NET_LINE_UDP_MAX_RECV,0,(sockaddr *)&re_adress,&saddr_len);
        if (err==0 || err==SOCKET_ERROR) throw _nline_error("recvfrom() in _simple_udp::recv()");

        data+=string(tmp,err);
        if (max_size)
            if (data.length()+NET_LINE_UDP_MAX_RECV>max_size) break;

        fd_set set;
        FD_ZERO(&set);
        FD_SET(s,&set);
        timeval tv={0,0};

        err=select(s+1,&set,NULL,NULL,&tv);

        if (err==0) break;
        if (err==SOCKET_ERROR) throw _nline_error("select() in _simple_udp::recv()");
    }
}

// ---------------------------------------------------
void _simple_udp::stay_server_at(const _net_adress &adress)
{
	int err;

    int one = 1;
    if((setsockopt(s,SOL_SOCKET,SO_REUSEADDR,(char *)&one,sizeof(one))) == -1) throw
       _nline_error("setsockopt() in _simple_udp::stay_server_at()");

    lo_adress.sin_family=AF_INET;
    if (adress.get(0)=="0.0.0.0" || adress.get(0)=="any")
    {
        lo_adress.sin_addr.s_addr=0x00;
    }
    else
    {
        lo_adress.sin_addr.s_addr=inet_addr(adress.get(0).c_str());
    }
    lo_adress.sin_port=htons(atoi(adress.get(1).c_str()));

    err=::bind(s,(sockaddr *)&lo_adress,sizeof(lo_adress));
    if (err==SOCKET_ERROR) throw _nline_error("bind() in  _simple_udp::stay_server_at()");

    ta_lo_adress=_tcp_adress(adress.get(0),adress.get(1));
}

// ---------------------------------------------------
_net_line *_simple_udp::accept()
{
    throw _nline_error("udp line can not accept - its stub");
}

// --------------------------------------------------------
void _simple_udp::is_ready(bool *read,bool *write,bool *error,timeval &tv)
{
    fd_set rset;
    fd_set wset;
    fd_set eset;

    FD_ZERO(&rset);
    FD_ZERO(&wset);
    FD_ZERO(&eset);

    if (read) FD_SET(s,&rset);
    if (write) FD_SET(s,&wset);
    if (error) FD_SET(s,&eset);

    int err=::select(s+1,&rset,&wset,&eset,&tv);
    if (err==SOCKET_ERROR) throw(_nline_error("select() in _simple_udp::is_ready()"));

    if (read) { if (FD_ISSET(s,&rset)) *read=true; else *read=false; }
    if (write) { if (FD_ISSET(s,&wset)) *write=true; else *write=false; }
    if (error)  { if (FD_ISSET(s,&eset)) *error=true; else *error=false; }
}

// --------------------------------------------------------
const _net_adress *_simple_udp::remoute_adress()
{
    char tmps[16];
    sprintf(tmps,"%i",re_adress.sin_port);
    ta_re_adress = _tcp_adress(inet_ntoa(re_adress.sin_addr),tmps);

    return &ta_re_adress;
}

// --------------------------------------------------------
const _net_adress *_simple_udp::local_adress()
{
    char tmps[16];
    sprintf(tmps,"%i",lo_adress.sin_port);
    ta_lo_adress = _tcp_adress(inet_ntoa(lo_adress.sin_addr),tmps);

    return &ta_lo_adress;
}

// --------------------------------------------------------
void _simple_udp::set_block_mode(bool mode)
{
#ifdef WINDOWS
	unsigned long param;
	if (mode) param=0; else param=1;

    int err=ioctlsocket(s,FIONBIO,&param);
    if (err==SOCKET_ERROR) throw(_nline_error("Error in set_block_mode()"));
#else
    throw(_nline_error("Error in set_block_mode() - not release yet -( "));
#endif
}

// --------------------------------------------------------
void _simple_udp::allow_broadcast(bool allow)
{
#ifdef WINDOWS
	throw(_nline_error("Error in allow_broadcast() - not release yet -( "));
#else
    int bp = 1;
    if (!allow) bp=0;
    if (setsockopt(s,SOL_SOCKET,SO_BROADCAST,(void *)&bp,sizeof(bp))<0) throw _nline_error("Error in allow_broadcast() - setsockopt() failed");
#endif
}

// make socket function
_net_line *create_simple_udp_line() { return new _simple_udp(); }
