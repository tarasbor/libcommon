// ServerSMTP.cpp: implementation of the ServerSMTP class.
//
//////////////////////////////////////////////////////////////////////

#include "ServerSMTP.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

// ---------------------------------------------------
// ---------------------------------------------------
ServerSMTP::ServerSMTP()
{

}

// ---------------------------------------------------
// ---------------------------------------------------
ServerSMTP::~ServerSMTP()
{

}

// ---------------------------------------------------
// ---------------------------------------------------
int ServerSMTP::Connect(char const *ip,int port)
{
        int i;
        
        i=s.Connect(ip,port);
        if (i)
        {
                error_st=s.Error();
                return i;
        }

        i=command("","220");
        if (i) return i;
        
        i=command("HELO UT_MAIL_AGENT\r\n","250");
        if (i) return i;

        return 0;
}

// ---------------------------------------------------
// ---------------------------------------------------
int ServerSMTP::AuthPlain(char const *mail,char const *user_name,char const *pass)
{
        int i;

        char tmp[256];

        // make data as user\0mail\0pass exmp test\0test@wis.ex.com\0test_pass
        strcpy(tmp,user_name);
        i=strlen(tmp)+1;
        strcpy(&tmp[i],mail);
        i+=strlen(mail)+1;
        strcpy(&tmp[i],pass);
        i+=strlen(mail);

        char tmp2[256];
        to_base64(tmp,tmp2,i);

        sprintf(tmp,"AUTH PLAIN %s\r\n",tmp2);

        i=command(tmp,"235");
        if (i) return i;

        return 0;
}

// ---------------------------------------------------
// ---------------------------------------------------
int ServerSMTP::AuthLogin(char const *user_name,char const *pass)
{
        int i;

        i=command("AUTH LOGIN\r\n","334");
        if (i) return i;

        char tmp[256];

        // send user
        to_base64(user_name,tmp,strlen(user_name));
        i=strlen(tmp);
        tmp[i]='\r';
        tmp[i+1]='\n';
        tmp[i+2]='\0';
        i=command(tmp,"334");
        if (i) return i;

        to_base64(pass,tmp,strlen(pass));
        i=strlen(tmp);
        tmp[i]='\r';
        tmp[i+1]='\n';
        tmp[i+2]='\0';
        i=command(tmp,"235");
        if (i) return i;

        return 0;
}

// ---------------------------------------------------
// ---------------------------------------------------
int ServerSMTP::Send(char const *from,char const *to,char const *text)
{

    char buf[1024];
        int i;

        // MAIL FROM
        sprintf(buf,"MAIL FROM: <%s>\r\n",from);
        i=command(buf,"250");
        if (i) return i;
        
        // RCPT TO
        sprintf(buf,"RCPT TO: <%s>\r\n",to);
        i=command(buf,"250");
        if (i) return i;

        // DATA
        i=command("DATA\r\n","354");
        if (i) return i;

        // mail
        int size=strlen(text);
        int pos=0;
        while(pos<size)
        {

                int cnt=1024;
                if (size-pos<1024) cnt=size-pos;
                i=s.Send(&text[pos],cnt);
                if (i)
                {
                        sprintf(buf,"��� %i ������ �� ���...",s.Error().c_str());
                        error_st=buf;
                        return i;
                }

                if (cnt==1024) p_proc();
                pos+=1024;
        }

        i=command("\r\n.\r\n","250");
        if (i) return i;

        // DATA
        i=command("RSET\r\n","250");
        // sometimes RSER return 220
        if (i) 
                if (strcmp(last_answer.substr(0,3).c_str(),"220"))
                        return i;

        return 0;
}

// ---------------------------------------------------
// ---------------------------------------------------
int ServerSMTP::Disconnect()
{

        int i;

        // QUIT
        i=command("QUIT\r\n","221");
        if (i) return i;

        s.Disconnect();
        return 0;
}

// ---------------------------------------------------
// ---------------------------------------------------
int ServerSMTP::command(char const *cmd,char const *answer)
{
        int i;
        char tmp[1024];

        if (strlen(cmd)>0)
        {
                i=s.Send(cmd,strlen(cmd));
                if (i)
                {
                        sprintf(tmp,"����SMTP %s:%s",cmd,s.Error().c_str());
                        error_st=tmp;
                        return i;
                }
        }

        std::string in;
        int last_pos=0;
        while(1)
        {
                int size;
                i=s.Recv(tmp,1024,&size);
                if (i)
                {
                        sprintf(tmp,"���SMTP � %s:%s",cmd,s.Error().c_str());
                        error_st=tmp;
                        return i;
                }

                tmp[size]='\0';
                in+=tmp;

                int pos=in.find("\r\n",last_pos);
                if (pos!=std::string::npos && pos==in.length()-2) break;
                last_pos=pos+2;

        }

        last_answer=in;

        if (strncmp(answer,in.c_str(),strlen(answer)))
        {
                sprintf(tmp,"��� ��� SMTP ����%s : %s",cmd,in.c_str());
                error_st=tmp;
                return 1;
        }

        return 0;
}
