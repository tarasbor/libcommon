/***************************************************************************
 *   What is it: This file is part of communication lib.
 *   Content: releases classes for SSL server
 *   Version: 0.1b
 *   Last-Mod: 24.09.2006
 *                                                                         
 *   Copyright (C) 2006 by Proshin Alexey                                  
 *   paa713@mail.ru                                                        
 *                                                                         
 *   The contents of this file are subject to free software; 
     you can redistribute it and/or modify  
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or     
 *   (at your option) any later version.                                   
 *                                                                         
 *   The contents of this file is distributed in the hope that it will be useful,       
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
 *   GNU General Public License for more details.                          
 *                                                                         
 *   You should have received a copy of the GNU General Public License     
 *   along with this program; if not, write to the                         
 *   Free Software Foundation, Inc.,                                       
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
 ***************************************************************************/

#include <net_line_ssl.h>
#include <net_server_ssl.h>
#include <list>

#define KEEP_ALIVE_TIME 15                 

// ----------------------------------------------------
// ssl server<->client connection
// ----------------------------------------------------
class _ssl_server_connection:public _net_server_connection
{
  friend _net_server_connection *create_ssl_server_connection(_net_line *line,_net_server *server);
  
  private:
  
    _net_line *s;
    _net_server *parent;
    time_t last_time;
    int _keep_alive_time;
    
    std::string in;
    std::string out;
  
    _ssl_server_connection(_net_line *line,_net_server *server);
    virtual ~_ssl_server_connection();
    
  public:
    virtual void release() { delete this;};
    virtual int Poll();
    virtual void set_keep_alive_time(int sec) { _keep_alive_time=sec; };
};

// ----------------------------------------------------
_ssl_server_connection::_ssl_server_connection(_net_line *line,_net_server *server):
                        s(line),parent(server),last_time(time(NULL)),in(""),out(""),
			_keep_alive_time(KEEP_ALIVE_TIME)
{
  _nlog().out("ssl server connection : i am start...",SERVER_LOG_LEVEL2);
}

// ----------------------------------------------------
_ssl_server_connection::~_ssl_server_connection()
{
  _nlog().out("ssl server connection : i am shutdown...",SERVER_LOG_LEVEL2);
  if (s)
  {
    s->shutdown();
    s->release();
  }
  s=NULL;
}

// ----------------------------------------------------
int _ssl_server_connection::Poll()
{
  try
  {
    bool flag;
    timeval tv={0,0};
    
    s->is_ready(&flag,NULL,NULL,tv);
    
    if (flag)
    {        
        _nlog().out("ssl server connection : have income data. Recive it...",SERVER_LOG_LEVEL2);
        std::string tmp="";
        s->recv(tmp);
        last_time=time(NULL);
        in+=tmp;        
        if (in.length())
	{
	   _nlog().out("ssl server connection : data recive done. Process it...",SERVER_LOG_LEVEL2);
	   flag=parent->in_process(in,out);
	}        
        if (!flag)
	{
	   _nlog().out("ssl server connection : Can`t process it. Ignore...",SERVER_LOG_LEVEL2);
	   return 0;
	}
	
	_nlog().out("ssl server connection : process done. Send answer...",SERVER_LOG_LEVEL2);
	
	// we can resive eof (close socket by clinet)
	// in this case flag_close = true
	// (P.S. we can`t recive any data (in HTTP protocol) on this step - only eof notify
	bool flag_close;
        s->is_ready(&flag_close,&flag,NULL,tv);	
	if (flag_close)
	{
	  _nlog().out("ssl server connection : but client close connection...",SERVER_LOG_LEVEL2);
	  return -1;
	}
	_nlog().out("ssl server connection : process done. Send answer way is clear...",SERVER_LOG_LEVEL2);
	
        if (flag) s->send(out);
	_nlog().out("ssl server connection : Send done...",SERVER_LOG_LEVEL2);
    }
    else
    {
      if (difftime(time(NULL),last_time)>KEEP_ALIVE_TIME) return -1;       
    }
    
  }
  catch(_nerror &e)
  {
  
    if (e.err_no()==0)
      _nlog().out("ssl server connection : was closed by remote side",SERVER_LOG_LEVEL2);
    else
      _nlog().out("ssl server connection : error was - "+e.err_st_full(),SERVER_LOG_LEVEL1);
      
    return -1;
  }
  
  return 0;
}

// ----------------------------------------------------
_net_server_connection *create_ssl_server_connection(_net_line *line,_net_server *server)
{
     return new _ssl_server_connection(line,server);
}


// ----------------------------------------------------
// ssl (tcp) server listen & accept connection
// ----------------------------------------------------
class _ssl_server_process:public _net_server_process
{
  friend _net_server_process *create_ssl_server_process(const _net_adress &adress,_net_server *server,
                                                              _ssl_set *ss);
  
  private:

        bool _is_online;
        _net_line *s;
        _net_server *parent;
        _tcp_adress adress;
	_ssl_set *ssl_set;
        
        //_locker lock;
        
        std::list<_net_server_connection *> con_list;
        
        _ssl_server_process(const _net_adress &adress,_net_server *server,_ssl_set *ss);
        virtual ~_ssl_server_process();
        
        virtual int Poll();
        
  public:        
  
        virtual void release() { delete this; };
        virtual bool is_online() const { return _is_online; };        
};

// -------------------------------------------------------------------
_ssl_server_process::_ssl_server_process(const _net_adress &_adress,_net_server *server,_ssl_set *ss):
                     _is_online(false),parent(server),s(create_ssl_line(ss)),
                     adress(_adress.get(0),_adress.get(1)),ssl_set(ss)
{
  _nlog().out("ssl server start...",SERVER_LOG_LEVEL1);
  Start();
}

// -------------------------------------------------------------------
_ssl_server_process::~_ssl_server_process()
{

  _nlog().out("ssl server stop...",SERVER_LOG_LEVEL1);
  if (!is_online()) return;
  
  _nlog().out("  : stop all connections...",SERVER_LOG_LEVEL2);
  std::list<_net_server_connection *>::iterator it=con_list.begin();
  while(it!=con_list.end())
  {
      (*it)->Stop();
      Sleep(1);
      (*it)->release();
      it=con_list.erase(it);    
  }
  
  _nlog().out("  : shutdown socket...",SERVER_LOG_LEVEL2);
  s->shutdown();
  s->release();
  
  _is_online=false;
  
  _nlog().out("ssl server stoped.",SERVER_LOG_LEVEL1);
 
}

// -------------------------------------------------------------------
int _ssl_server_process::Poll()
{
  if (!is_online())
  {
    _nlog().out("ssl server try to go on-line...",SERVER_LOG_LEVEL1);
    try { s->stay_server_at(adress); }
    catch(_nerror &e)
    {
      _nlog().out("ssl server try to go on-line...error - "+e.err_st_full(),SERVER_LOG_LEVEL1);
      Sleep(1000);
      return 0;
    }
    
    _nlog().out("ssl server try to go on-line...done",SERVER_LOG_LEVEL1);
    _is_online=true;
    return 0;
  }
  
  timeval time={0,0};
  bool read;
  try { s->is_ready(&read,NULL,NULL,time); }
  catch(_nerror &e)
  {
    _nlog().out("ssl server : select NOT ACCEPTED - "+e.err_st_full(),SERVER_LOG_LEVEL1);
    return 0;
  }
  
  if (read)
  {
    _nlog().out("ssl server : income connection. Try accept...",SERVER_LOG_LEVEL1);
    _net_line *tmp;
    try { tmp=s->accept(); }
    catch(_nerror &e)
    {
       _nlog().out("ssl server : income connection. NOT ACCEPTED - "+e.err_st_full(),SERVER_LOG_LEVEL1);
       return 0;
    }
    
    _net_server_connection *con=create_ssl_server_connection(tmp,parent);
    con_list.push_back(con);
    con->Start();    
  }
  
  //lock.lock();
  std::list<_net_server_connection *>::iterator it=con_list.begin();
  while(it!=con_list.end())
  {
    int status=(*it)->GetStatus();
    if (status==TH_STATUS_CRUSH || status==TH_STATUS_KILLED)
    {
       (*it)->release();
       it=con_list.erase(it);
       continue;
    }
    it++;
  }
  //lock.unlock();
  
  return 0;   
}

// -------------------------------------------------------------------
_net_server_process *create_ssl_server_process(const _net_adress &adress,_net_server *server,
                                                     _ssl_set *ssl_set)
{
   return new _ssl_server_process(adress,server,ssl_set);
}

