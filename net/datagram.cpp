#include <datagram.hpp>
#include <cstring>

using namespace std;

// -------------------------------------------------------------------------
_datagram::_datagram(const unsigned char *rdata,int rsize,DATAGRAM_TYPE_TYPE dt):dtype(dt),d(NULL),d_size(0)
{
    dstamp.set_now();
    
    d_size=rsize+header_len();

    d=new unsigned char [d_size];

    _time::time_val_i t=dstamp.stamp();
    
    memcpy(d,&t,sizeof(_time::time_val_i));
    memcpy(d+sizeof(_time::time_val_i),&dtype,sizeof(DATAGRAM_TYPE_TYPE));
    memcpy(d+sizeof(_time::time_val_i)+sizeof(DATAGRAM_TYPE_TYPE),&rsize,sizeof(unsigned int));
    memcpy(d+header_len(),rdata,rsize);
}

// -------------------------------------------------------------------------
_datagram::_datagram(const char *data)
{
    // stamp
    _time::time_val_i ts;
    memcpy(&ts,data,sizeof(ts));
    dstamp.stamp(ts);
    
    // type
    memcpy(&dtype,data+sizeof(ts),sizeof(DATAGRAM_TYPE_TYPE));

    // raw size
    int rs;
    memcpy(&rs,data+sizeof(ts)+sizeof(DATAGRAM_TYPE_TYPE),sizeof(int));
    
    d_size=rs+header_len();

    d=new unsigned char [d_size];
    memcpy(d,data,rs+header_len());
}

// -------------------------------------------------------------------------
_datagram::_datagram(const _datagram &sd)
{
    dstamp=sd.dstamp;
    dtype=sd.dtype;
    d_size=sd.d_size;

    d=new unsigned char [d_size];

    memcpy(d,sd.d,d_size);
}

// -------------------------------------------------------------------------
_datagram::~_datagram()
{
    if (d) delete d;
}

