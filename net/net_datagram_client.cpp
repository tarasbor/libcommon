#include "net_datagram_client.hpp"
#include <net_line_tcp.h>
#include <stdio.h>
#include <string.h>
#include <clthread.hpp>

using namespace std;

// net datagram client error
// -------------------------------------------------------
class _ndgclient_error:public _nerror
{
  private:
        std::string operation;
        long error_number;

  public:
        _ndgclient_error(const std::string &_oper,long error):error_number(error),operation(_oper) {};
        virtual std::string err_st() const { char tmp[256]; sprintf(tmp,"error #%i",error_number);
                                             return std::string(tmp); };
        virtual std::string err_st_full() const { char tmp[256]; sprintf(tmp,"error #%i",error_number);
                                                  return std::string(tmp)+
                                                  std::string(" on ")+operation; };
        virtual long err_no() const { return error_number; };
};

// network datagram connection
// -------------------------------------------------------------------
class _ndc: public _net_datagram_client
{
    private:

      _net_line *con;
      bool cm_is_connected;
      bool cm_is_broken;

      int cm_connection_id;

      std::string in;
      std::string out;

    public:

      _ndc();
      virtual ~_ndc();

	  virtual void connect(const _net_adress &adress);
	  virtual void disconnect();

      virtual bool is_connected() const { return cm_is_connected; };
      virtual bool is_broken() const { return cm_is_broken; };
      virtual int connection_id() const { return cm_connection_id; };

      //virtual void kill();

      virtual void recv();
      virtual _datagram *datagram();

      virtual void send();
      virtual void datagram(_datagram *);
      virtual void datagram(const unsigned char* str, unsigned int len);
      virtual bool is_sended() const { return  out.empty(); };
	  virtual int out_buffer_size() const { return out.length(); };

      virtual void release() { delete this; };
};

// -------------------------------------------------------------------
_ndc::_ndc():
cm_is_connected(false),cm_is_broken(false),con(NULL),cm_connection_id(0)
{
}

// -------------------------------------------------------------------
_ndc::~_ndc()
{
    if (con)
    {
        if (cm_is_connected)
        {
            disconnect();
        }
        else
        {
            con->shutdown();
            con->release();
            con=NULL;
        }
    }
}

// --------------------------------------------------------
void _ndc::connect(const _net_adress &adress)
{
    if (cm_is_connected) return;

    try
    {
        if (con) con->release();
        con=NULL;

        con = create_socket_line();
        con->connect(adress);

		while(1)
        {
            recv();

            _datagram *sd=datagram();
            if (!sd)
            {
                Sleep(1);
                continue;
            }

			cm_connection_id=0;

			// TODO: check datagram type - BUT on this release we do not user value of connection_id type
			if (sd->raw_size()>=sizeof(int))
			{
				memcpy(&cm_connection_id,sd->raw_data(),sizeof(int));
				delete sd;
				break;
			}

			delete sd;
        }
    }
    catch(_nerror &ne)
    {
        cm_is_broken=true;
        throw(_ndgclient_error(ne.err_st_full(),ne.err_no()));
    }

    cm_is_connected=true;
    cm_is_broken=false;
}

// --------------------------------------------------------
void _ndc::disconnect()
{
    try
    {
        if (con)
        {
           con->shutdown();
           con->release();
           con=NULL;
        }
    }
    catch(_nerror &ne)
    {
        throw(_ndgclient_error(ne.err_st_full(),ne.err_no()));
    }
    cm_is_connected=false;
    cm_is_broken=false;
}

/*
// --------------------------------------------------------
void _ndc::kill()
{
    cm_is_broken=false;
    cm_is_connected=false;

    if (!con) return;

    con->shutdown();
    if (con) con->release();
    con=NULL;
}
*/

// --------------------------------------------------------
void _ndc::recv()
{
    if (!con) return;
    if (cm_is_broken) return;

    std::string tmp;
    try
    {
        bool read;
        timeval tv={0,0};

        con->is_ready(&read,NULL,NULL,tv);
        if (!read) return;

        con->recv(tmp,1024*100);

        if (tmp.empty())
        {
            cm_is_broken=true;
            throw(_ndgclient_error("_ndc::recv - recv empty data - connection was closed by remoute side.",0));
        }
    }
    catch(_nerror &ne)
    {
        cm_is_broken=true;
        throw(_ndgclient_error(ne.err_st_full(),ne.err_no()));
    }
    in+=tmp;
}

// --------------------------------------------------------
void _ndc::send()
{
    if (!con) return;
    if (cm_is_broken) return;

    if (!out.length()) return ;

    std::string tmp;
    try
    {
        bool write;
        timeval tv={0,0};

        con->is_ready(NULL,&write,NULL,tv);
        if (!write) return;

        con->send(out);

        out.clear();
    }
    catch(_nerror &ne)
    {
        cm_is_broken=true;
        throw(_ndgclient_error(ne.err_st_full(),ne.err_no()));
    }
}


// --------------------------------------------------------
_datagram *_ndc::datagram()
{
    _datagram *sd=NULL;

    if (in.size()>=_datagram::header_len())
    {
        int s; // raw data size
        memcpy(&s,in.data()+(_datagram::header_len()-sizeof(int)),sizeof(int));

        if (in.size()>=s+_datagram::header_len())
        {
            sd=new _datagram(in.data());
            in.erase(0,s+_datagram::header_len());
        }
    }

    return sd;
}

// --------------------------------------------------------
void _ndc::datagram(_datagram *sd)
{
    if (!sd) return;

    out+=string((char *)sd->data(),sd->size());
}

void _ndc::datagram(const unsigned char *str, unsigned int len)
{
    if (!str) return;

    out +=string((char *)str,len);
}

// --------------------------------------------------------
_net_datagram_client *create_net_datagram_client()
{
    return new _ndc();
}


