/***************************************************************************
 *   What is it: This file is part of communication lib.
 *   Content: releases classes for network line (point-point)
 *   Version: 0.1b
 *   Last-Mod: 24.09.2006
 *
 *   Copyright (C) 2006 by Proshin Alexey
 *   paa713@mail.ru
 *
 *   The contents of this file are subject to free software;
     you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   The contents of this file is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***************************************************************************/

#include "net_line.h"
#include <stdio.h>

#ifdef WINDOWS
        using namespace std;
        #include <WS2tcpip.h>   // for socklen_t
        #include <winsock.h>
        #include <windows.h>
#else
        #include <sys/types.h>
        #include <sys/socket.h>
        #include <string>
        #include <errno.h>
        #include <netinet/in.h>
        #include <arpa/inet.h>
        #include <openssl/ssl.h>
        #include <openssl/err.h>
        #include <unistd.h>
        #include <sys/time.h> // for timeval
        #include <fcntl.h>  // for fcntl()

        typedef int SOCKET;
        #define INVALID_SOCKET -1
        #define SOCKET_ERROR -1
#endif

#ifndef NET_LINE_TCP_RECV_PORTION_SIZE
#define NET_LINE_TCP_RECV_PORTION_SIZE 10240
#endif

// init class for windows socket lib.
// Its need to call WSAStartup one time befor any socket API function call.
// And WSACleanup in end of application
#ifdef WINDOWS
class _wsa_init
{
  public:
    _wsa_init();
    ~_wsa_init() { WSACleanup();};
};

_wsa_init::_wsa_init()
{
    WORD wVersionRequested;
    WSADATA wsaData;
    int err;

    wVersionRequested = MAKEWORD( 2, 0 );

    err = WSAStartup( wVersionRequested, &wsaData );
    if ( err != 0 ) return;

    if ( LOBYTE( wsaData.wVersion ) != 2 || HIBYTE( wsaData.wVersion ) != 0 )
    {
       WSACleanup( );
       return;
    }
}
static _wsa_init wsa_init;
#endif

// net line error class (for socket errors)
// -------------------------------------------------------
class _nline_error:public _nerror
{
#ifdef WINDOWS
  private:
	    long error_number;
        std::string operation;


  public:
        _nline_error(const std::string &_oper):error_number(WSAGetLastError()),operation(_oper) {};
        virtual std::string err_st() const { char tmp[256]; sprintf(tmp,"error #%li",error_number);
                                             return std::string(tmp); };
        virtual std::string err_st_full() const { char tmp[256]; sprintf(tmp,"error #%li",error_number);
                                                  return std::string(tmp)+
                                                  std::string(" on command: ")+operation; };
        virtual long err_no() const { return error_number; };
#else
  private:
        std::string operation;
        long error_number;

  public:
        _nline_error(const std::string &_oper):operation(_oper),error_number(errno) {};
        virtual std::string err_st() const { return std::string(strerror(error_number)); };
        virtual std::string err_st_full() const { return std::string(strerror(error_number))+
                                            std::string(" on command: ")+operation; };
        virtual long err_no() const { return error_number; };
#endif
};

// ------------------------------------------------------------------
// socket class
// ------------------------------------------------------------------
class _socket:public _net_line
{
  private:
        SOCKET s;
        bool is_connected;

        _tcp_adress tmp_adress;

        _socket(const _socket &_s);// { };
        _socket &operator=(const _socket &s);// { return *this; };

        const SOCKET handle() const { return s; };

        operator SOCKET() const { return s; };

        ~_socket() { shutdown(); } // why destructor in private?!!

  public:
        _socket():s(INVALID_SOCKET),is_connected(false),tmp_adress("","") {}
        _socket(SOCKET h):s(h),is_connected(true),tmp_adress("","") {}

        virtual void connect(const _net_adress &adress);
        virtual void stay_server_at(const _net_adress &adress);

        virtual void send(const std::string &data);
        virtual void recv(std::string &data,int max_size=0);

        virtual void shutdown();

        _net_line *accept();

        virtual void release() { delete this; };

        virtual void is_ready(bool *read,bool *write,bool *error,timeval &tv);

        virtual const _net_adress *remoute_adress();
        virtual const _net_adress *local_adress();

        virtual void set_block_mode(bool mode);     // Not work on linux! No info how work on windows!
        virtual void allow_broadcast(bool allow);
};

// ---------------------------------------------------
// ---------------------------------------------------
void _socket::connect(const _net_adress &adress)
{
    sockaddr_in sockAddr;
    int i;    
    char buff[1024]; memset(buff,0,1024);
    socklen_t lon;
    fd_set myset;

    s=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
    if (s==INVALID_SOCKET)
    {
          throw _nline_error("socket() in _socket::connect()");
    }

    //int z=1;
    //setsockopt(s,SOL_SOCKET,SO_KEEPALIVE,(void *)&z,sizeof(z));

    memset(&sockAddr,0,sizeof(sockAddr));
    sockAddr.sin_family=AF_INET;
    sockAddr.sin_port=htons(atoi(adress.get(1).c_str()));
    sockAddr.sin_addr.s_addr=inet_addr(adress.get(0).c_str());


#ifdef WINDOWS
    // 1. set the socket in non-blocking
    unsigned long iMode = 1;
    int iResult = ioctlsocket(s, FIONBIO, &iMode);
    if (iResult != NO_ERROR)
    {
        sprintf(buff, "ioctlsocket failed with error: %i\n", iResult);
        shutdown();
        throw _nline_error(buff);
    }
#else
    // 1. Non blocking socket
    if ( (i=fcntl(s, F_GETFL, NULL))<0 )    {
        sprintf(buff, "Error fcntl(..., F_GETFL) (%s)\n", strerror(errno));
        shutdown();
        throw _nline_error(buff);
    }
    i |= O_NONBLOCK;
    if( fcntl(s, F_SETFL, i) < 0)   {
        sprintf(buff, "Error fcntl(..., F_SETFL) (%s)\n", strerror(errno));
        shutdown();
        throw _nline_error(buff);
    }
#endif

    // 2. Trying to connect
#ifdef WINDOWS
    iResult=::connect(s, (struct sockaddr *)&sockAddr, sizeof(sockAddr));
    if(iResult==false)
    {
        sprintf(buff,"connect() failed with error: %i\n", iResult);
        shutdown();
        throw _nline_error(buff);
    }
#else
    // 2. Trying to connect with timeout
    int valopt;
    i = ::connect(s, (struct sockaddr *)&sockAddr, sizeof(sockAddr));
    if (i < 0) {
        if (errno == EINPROGRESS) {
            //sprintf(buff, "EINPROGRESS in connect() - selecting\n");
            do {
                struct timeval tv;
                tv.tv_sec = 10;     // 10 seconds timeout
                tv.tv_usec = 0;
                FD_ZERO(&myset);
                FD_SET(s, &myset);

                i = select(s+1, NULL, &myset, NULL, &tv);
                if (i < 0 && errno != EINTR) {
                    sprintf(buff, "Error connecting %d - %s\n", errno, strerror(errno));
                    shutdown();
                    throw _nline_error(buff);
                }
                else if (i > 0) {
                    // Socket selected for write
                    lon = sizeof(int);

                    if (getsockopt(s, SOL_SOCKET, SO_ERROR, (void*)(&valopt), &lon) < 0) {
                        sprintf(buff, "Error in getsockopt() %d - %s\n", errno, strerror(errno));
                        shutdown();
                        throw _nline_error(buff);
                    }

                    // Check the value returned...
                    if (valopt) {
                        sprintf(buff, "Error in delayed connection() %d - %s\n", valopt, strerror(valopt) );
                        shutdown();
                        throw _nline_error(buff);
                    }
                    break;
                }
                else {
                    sprintf(buff, "Timeout in select() - Cancelling!\n");
                    shutdown();
                    throw _nline_error(buff);
                }
            } while (1);
        }
        else {
            sprintf(buff, "Error connecting %d - %s\n", errno, strerror(errno));
            shutdown();
            throw _nline_error(buff);
        }
    }
#endif


#ifdef WINDOWS
    // restart the socket in blocking mode
    iMode = 0;
    iResult = ioctlsocket(s, FIONBIO, &iMode);
    if (iResult != NO_ERROR)
    {
        sprintf(buff,"ioctlsocket failed with error: %i\n", iResult);
        shutdown();
        throw _nline_error(buff);
    }
    fd_set Write, Err;
    FD_ZERO(&Write);
    FD_ZERO(&Err);
    FD_SET(s, &Write);
    FD_SET(s, &Err);

    // check if the socket is ready
    TIMEVAL Timeout;
    Timeout.tv_sec = 10;
    Timeout.tv_usec = 0;
    select(0,NULL,&Write,&Err,&Timeout);
    if(!FD_ISSET(s, &Write))
    {
        sprintf(buff,"FD_ISSET failed");
        shutdown();
        throw _nline_error(buff);
    }
#else
    // Set to blocking mode again...
    if( (i = fcntl(s, F_GETFL, NULL)) < 0) {
        sprintf(buff, "Error fcntl(..., F_GETFL) (%s)\n", strerror(errno));
        shutdown();
        throw _nline_error(buff);
    }
    i &= (~O_NONBLOCK);
    if( fcntl(s, F_SETFL, i) < 0) {
        sprintf(buff, "Error fcntl(..., F_SETFL) (%s)\n", strerror(errno));
        shutdown();
        throw _nline_error(buff);
    }
#endif

    /////// older code.
    /*
    i=::connect(s,(sockaddr *)&sockAddr,sizeof(sockAddr));
    if (i)
    {
         shutdown();
         throw _nline_error("connect() in _socket::connect()");
    }*/

    // success
    is_connected=1;
}

// ---------------------------------------------------
void _socket::shutdown()
{
#ifdef WINDOWS
        //if (is_connected && s!=INVALID_SOCKET) { ::shutdown(s,SD_BOTH); is_connected=false; }
        if (is_connected && s!=INVALID_SOCKET) { ::shutdown(s,0x02); is_connected=false; }
        if (s!=INVALID_SOCKET) { ::closesocket(s); s=INVALID_SOCKET; }
#else
        if (is_connected && s!=INVALID_SOCKET) { ::shutdown(s,SHUT_RDWR); is_connected=false; }
        if (s!=INVALID_SOCKET) { ::close(s); s=INVALID_SOCKET; }
#endif

}

// ---------------------------------------------------
void _socket::send(const std::string &data)
{    
    int err;
    int pos=0;
    int size=data.length();

    while(1)
    {
#ifdef WINDOWS
        err=::send(s,data.data()+pos,size,0);
#else
        err=::send(s,data.data()+pos,size,MSG_NOSIGNAL);  // MSG_NOSIGNAL - Fix HTTP server signal 'Broken Pipe' (when monitoring)!!
#endif

        if (err==SOCKET_ERROR)
        {
            throw _nline_error("send() in _socket::send()");
        }

        if (err==size) break;
        pos+=err;
        size-=err;
    }
}



// ---------------------------------------------------
void _socket::recv(std::string &data,int max_size)
{
    int err;
    data.clear();

    char *tmp=new char [NET_LINE_TCP_RECV_PORTION_SIZE];

    while(1)
    {
        err=::recv(s,tmp,NET_LINE_TCP_RECV_PORTION_SIZE,0);

        if (err==0) break;
        if (err==SOCKET_ERROR)
        {
                delete tmp;
                throw _nline_error("recv() in _socket::recv()");
        }

        data+=std::string(tmp,err);
        if (max_size)
            if (data.length()+NET_LINE_TCP_RECV_PORTION_SIZE>max_size) break;

        fd_set set;
        FD_ZERO(&set);
        FD_SET(s,&set);
        timeval tv={0,0};

        err=select(s+1,&set,NULL,NULL,&tv);

        if (err==0) break;
        if (err==SOCKET_ERROR) { delete tmp; throw _nline_error("select() in _socket::recv()"); }
    }

    delete tmp;
}

// ---------------------------------------------------
void _socket::stay_server_at(const _net_adress &adress)
{
sockaddr_in ss;
int err;

    s=socket(PF_INET,SOCK_STREAM,IPPROTO_TCP);
    if (s==INVALID_SOCKET) throw _nline_error("socket() in _socket::stay_server_at()");

    int one = 1;
    if((setsockopt(s,SOL_SOCKET,SO_REUSEADDR,(char *)&one,sizeof(one))) == -1) throw
       _nline_error("setsockopt() in _socket::stay_server_at()");

    ss.sin_family=AF_INET;
    if (adress.get(0)=="0.0.0.0" || adress.get(0)=="any")
    {
        //ss.sin_addr.s_addr=ADDR_ANY;
        ss.sin_addr.s_addr=0x00;
    }
    else
    {
        ss.sin_addr.s_addr=inet_addr(adress.get(0).c_str());
    }
    ss.sin_port=htons(atoi(adress.get(1).c_str()));

    err=::bind(s,(sockaddr *)&ss,sizeof(ss));
    if (err==SOCKET_ERROR) throw _nline_error("bind() in _socket::stay_server_at()");

    err=::listen(s,5);
    if (err==SOCKET_ERROR) throw _nline_error("listen() in _socket::stay_server_at()");
}

// ---------------------------------------------------
_net_line *_socket::accept()
{
    sockaddr_in addr;
#ifdef WINDOWS
    int addr_len=sizeof(addr);
#else
    socklen_t addr_len=sizeof(addr);
#endif
    SOCKET tmps;

    tmps=::accept(s,(sockaddr *)&addr,&addr_len);
    if (tmps==INVALID_SOCKET) throw _nline_error("accept() in _socket::accept()");

    return new _socket(tmps);
}

// --------------------------------------------------------
void _socket::is_ready(bool *read,bool *write,bool *error,timeval &tv)
{
    fd_set rset;
    fd_set wset;
    fd_set eset;

    FD_ZERO(&rset);
    FD_ZERO(&wset);
    FD_ZERO(&eset);

    if (read) FD_SET(s,&rset);
    if (write) FD_SET(s,&wset);
    if (error) FD_SET(s,&eset);

    int err=::select(s+1,&rset,&wset,&eset,&tv);
    if (err==SOCKET_ERROR) throw(_nline_error("select() in _socket::is_ready()"));

    if (read) { if (FD_ISSET(s,&rset)) *read=true; else *read=false; }
    if (write) { if (FD_ISSET(s,&wset)) *write=true; else *write=false; }
    if (error)  { if (FD_ISSET(s,&eset)) *error=true; else *error=false; }
}

// --------------------------------------------------------
const _net_adress *_socket::remoute_adress()
{
    tmp_adress = _tcp_adress("","");
    if (!is_connected) return &tmp_adress;

    sockaddr_in addr;
#ifdef WINDOWS
    int addr_len=sizeof(addr);
#else
    socklen_t addr_len=sizeof(addr);
#endif

    if( getpeername(s,(sockaddr *)&addr,&addr_len)<0 ) // if error (e.g. broken pipe) return empty addr
    {
        return &tmp_adress;
    }

    char tmps[16];
    sprintf(tmps,"%i",addr.sin_port);
    tmp_adress = _tcp_adress(inet_ntoa(addr.sin_addr),tmps);

    return &tmp_adress;
}

// --------------------------------------------------------
const _net_adress *_socket::local_adress()
{
    tmp_adress = _tcp_adress("","");
    if (is_connected) return &tmp_adress;

    sockaddr_in addr;
#ifdef WINDOWS
    int addr_len=sizeof(addr);
#else
    socklen_t addr_len=sizeof(addr);
#endif

    if( getsockname(s,(sockaddr *)&addr,&addr_len)<0 ) // if error return empty addr
    {
        return &tmp_adress;
    }

    char tmps[16];
    sprintf(tmps,"%i",addr.sin_port);
    tmp_adress = _tcp_adress(inet_ntoa(addr.sin_addr),tmps);

    return &tmp_adress;
}

// --------------------------------------------------------
void _socket::set_block_mode(bool mode)
{
#ifdef WINDOWS
	unsigned long param;
	if (mode) param=0; else param=1;

    int err=ioctlsocket(s,FIONBIO,&param);
    if (err==SOCKET_ERROR) throw(_nline_error("Error in set_block_mode()"));
#else
    throw(_nline_error("Error in set_block_mode() - not release yet -( "));
#endif
}

// --------------------------------------------------------
void _socket::allow_broadcast(bool allow)
{
	throw(_nline_error("Error in allow_broadcast() - TCP socket can not be broadcast..."));
}


// make socket function
_net_line *create_socket_line() { return new _socket(); }
