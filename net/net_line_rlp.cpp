/***************************************************************************
 *   What is it: This file is part of communication lib.
 *   Content: releases classes for network line (point-point)
 *   Version: 0.1b
 *   Last-Mod: 12.11.2010
 *                                                                         
 *   Copyright (C) 2006 by Proshin Alexey                                  
 *   paa713@mail.ru                                                        
 *                                                                         
 *   The contents of this file are subject to free software; 
     you can redistribute it and/or modify  
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or     
 *   (at your option) any later version.                                   
 *                                                                         
 *   The contents of this file is distributed in the hope that it will be useful,       
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
 *   GNU General Public License for more details.                          
 *                                                                         
 *   You should have received a copy of the GNU General Public License     
 *   along with this program; if not, write to the                         
 *   Free Software Foundation, Inc.,                                       
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
 ***************************************************************************/

#include "net_line_rlp.h"

#ifdef WINDOWS
        using namespace std;
        #include <windows.h>
#endif


// net rlp  error class (for rlp socket errors)
// -------------------------------------------------------
class _nrlp_error:public _nerror
{
  private:
        std::string operation;
        long error_number;

  public:
        _nrlp_error(const std::string &_oper,long err_n):error_number(err_n),operation(_oper) {};
        virtual std::string err_st() const { char tmp[256]; sprintf(tmp,"error #%i",error_number);
                                             return std::string(tmp); };
        virtual std::string err_st_full() const { char tmp[256]; sprintf(tmp,"error #%i",error_number);
                                                  return std::string(tmp)+
                                                  std::string(" on command ")+operation; };
        virtual long err_no() const { return error_number; };
};

// ------------------------------------------------------------------
// rlp socket class
// ------------------------------------------------------------------
class _rlp_socket:public _net_line
{
  private:

        _rlp_rs *rs;
        
        bool is_connected;

        _rlp_adress loc_adress;
        _rlp_adress rem_adress;

        _rlp_socket(const _rlp_socket &_s); 
        _rlp_socket &operator=(const _rlp_socket &s);         
        
        ~_rlp_socket() { shutdown(); };
        
  public:
        _rlp_socket(_rlp_rs *rlp_rs);
        
        virtual void connect(const _net_adress &adress) { throw _nrlp_error("_rlp_socket::connect - not implement yet",0); };
        virtual void stay_server_at(const _net_adress &adress) { throw _nrlp_error("_rlp_socket::stay_server_at - not implement yet",0); };
        
        virtual _net_line *accept() { throw _nrlp_error("_rlp_socket::accept - not implement yet",0); };
        
        virtual void send(const std::string &data) { throw _nrlp_error("_rlp_socket::send - not implement yet",0); };
        virtual void recv(std::string &data,int max_size=0) { throw _nrlp_error("_rlp_socket::recv - not implement yet",0); };
        
        virtual void shutdown();
        virtual void release() { delete this; };

        virtual const _net_adress *remoute_adress() { return &rem_adress; };
        virtual const _net_adress *local_adress() { return &loc_adress; };
        
        virtual void is_ready(bool *read,bool *write,bool *error,timeval &tv) { throw _nrlp_error("_rlp_socket::is_ready - not implement yet",0); };

        virtual void set_block_mode(bool mode) { if (!mode) throw _nrlp_error("_rlp_socket::set_block_mode - mode can not be NON blocking",0); };
};

// -------------------------------------------------------
// ------------------------------------------------------
_rlp_socket::_rlp_socket(_rlp_rs *rlp_rs):loc_adress("",""),rem_adress("",""),is_connected(false),rs(rlp_rs)
{
    unsigned char self_addr = *(unsigned char *)rs->config(_rlp_rs::NODE_ADDR);
    char tmp[32];
    sprintf(tmp,"%c",self_addr);
    
    loc_adress=_rlp_adress(tmp,"");
}

// ------------------------------------------------------
void _rlp_socket::shutdown()
{
    is_connected=false;
}

/*
// ---------------------------------------------------
// ---------------------------------------------------
void _socket::connect(const _net_adress &adress)
{        
        sockaddr_in sockAddr;        
        int i;
 
        s=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);      
        if (s==INVALID_SOCKET)
        {
              throw _nline_error("socket() in _socket::connect()");
        }

        memset(&sockAddr,0,sizeof(sockAddr));
        sockAddr.sin_family=AF_INET;
        sockAddr.sin_port=htons(atoi(adress.get(1).c_str()));
        sockAddr.sin_addr.s_addr=inet_addr(adress.get(0).c_str());
        i=::connect(s,(sockaddr *)&sockAddr,sizeof(sockAddr));
        if (i)
        {
             shutdown();
             throw _nline_error("connect() in _socket::connect()");
        }

        is_connected=1;
}

// ---------------------------------------------------
void _socket::shutdown()
{
#ifdef WINDOWS
        //if (is_connected && s!=INVALID_SOCKET) { ::shutdown(s,SD_BOTH); is_connected=false; }
        if (is_connected && s!=INVALID_SOCKET) { ::shutdown(s,0x02); is_connected=false; }
        if (s!=INVALID_SOCKET) { ::closesocket(s); s=INVALID_SOCKET; } 
#else    
        if (is_connected && s!=INVALID_SOCKET) { ::shutdown(s,SHUT_RDWR); is_connected=false; }
        if (s!=INVALID_SOCKET) { ::close(s); s=INVALID_SOCKET; }
#endif
        
}

// ---------------------------------------------------
void _socket::send(const std::string &data)
{
    int err;
    int pos=0;
    int size=data.length();

        while(1)
        {               
                err=::send(s,data.data()+pos,size,0);
                if (err==SOCKET_ERROR) 
                {
                        throw _nline_error("send() in _socket::send()");
                }
                if (err==size) break;
                pos+=err;
                size-=err;
        }
}

// ---------------------------------------------------
void _socket::recv(std::string &data,int max_size)
{
    int err;
    data="";
    
    char *tmp=new char [1024];    

    while(1)
    {
        err=::recv(s,tmp,1024,0);
        if (err==0 || err==SOCKET_ERROR)
        {
                delete tmp;
                throw _nline_error("recv() in _socket::recv()");
        }        
        
        data+=std::string(tmp,err);
        if (max_size)
            if (data.length()+1024>max_size) break;
        
        fd_set set;
        FD_ZERO(&set);
        FD_SET(s,&set);
        timeval tv={0,0};
        
        err=select(s+1,&set,NULL,NULL,&tv);
        
        if (err==0) break;
        if (err==SOCKET_ERROR) { delete tmp; throw _nline_error("select() in _socket::recv()"); }
    }
    
    delete tmp;
}

// ---------------------------------------------------
void _socket::stay_server_at(const _net_adress &adress)
{
sockaddr_in ss;
int err;
    
    s=socket(PF_INET,SOCK_STREAM,IPPROTO_TCP);
    if (s==INVALID_SOCKET) throw _nline_error("socket() in _socket::stay_server_at()");
    
    int one = 1;    
    if((setsockopt(s,SOL_SOCKET,SO_REUSEADDR,(char *)&one,sizeof(one))) == -1) throw
       _nline_error("setsockopt() in _socket::stay_server_at()");
        
    ss.sin_family=AF_INET;
    if (adress.get(0)=="0.0.0.0" || adress.get(0)=="any")
    {
        //ss.sin_addr.s_addr=ADDR_ANY;
        ss.sin_addr.s_addr=0x00;
    }
    else
    {
        ss.sin_addr.s_addr=inet_addr(adress.get(0).c_str());
    }
    ss.sin_port=htons(atoi(adress.get(1).c_str()));

    err=::bind(s,(sockaddr *)&ss,sizeof(ss));
    if (err==SOCKET_ERROR) throw _nline_error("bind() in _socket::stay_server_at()");
    
    err=::listen(s,5);
    if (err==SOCKET_ERROR) throw _nline_error("listen() in _socket::stay_server_at()");
}

// ---------------------------------------------------
_net_line *_socket::accept()
{
sockaddr_in addr;
#ifdef WINDOWS
    int addr_len=sizeof(addr);
#else
    socklen_t addr_len=sizeof(addr);
#endif
SOCKET tmps;
    
    tmps=::accept(s,(sockaddr *)&addr,&addr_len);
    if (tmps==INVALID_SOCKET) throw _nline_error("accept() in _socket::accept()");    
    
    return new _socket(tmps);
}

// --------------------------------------------------------
void _socket::is_ready(bool *read,bool *write,bool *error,timeval &tv)
{
    fd_set rset;
    fd_set wset;
    fd_set eset;
    
    FD_ZERO(&rset);
    FD_ZERO(&wset);
    FD_ZERO(&eset);
    
    if (read) FD_SET(s,&rset);
    if (write) FD_SET(s,&wset);
    if (error) FD_SET(s,&eset);
    
    int err=::select(s+1,&rset,&wset,&eset,&tv);
    if (err==SOCKET_ERROR) throw(_nline_error("select() in _socket::is_ready()"));
    
    if (read) if (FD_ISSET(s,&rset)) *read=true; else *read=false;
    if (write) if (FD_ISSET(s,&wset)) *write=true; else *write=false;
    if (error) if (FD_ISSET(s,&eset)) *error=true; else *error=false;
}

// --------------------------------------------------------
const _net_adress *_socket::remoute_adress()
{
    tmp_adress = _tcp_adress("","");
    if (!is_connected) return &tmp_adress;
    
    sockaddr_in addr;
#ifdef WINDOWS
    int addr_len=sizeof(addr);
#else
    socklen_t addr_len=sizeof(addr);
#endif
    
    getpeername(s,(sockaddr *)&addr,&addr_len);

    char tmps[16];
    sprintf(tmps,"%i",addr.sin_port);
    tmp_adress = _tcp_adress(inet_ntoa(addr.sin_addr),tmps);
    
    return &tmp_adress;
}

// --------------------------------------------------------
const _net_adress *_socket::local_adress()
{
    tmp_adress = _tcp_adress("","");
    if (is_connected) return &tmp_adress;
    
    sockaddr_in addr;
#ifdef WINDOWS
    int addr_len=sizeof(addr);
#else
    socklen_t addr_len=sizeof(addr);
#endif
    
    getsockname(s,(sockaddr *)&addr,&addr_len);

    char tmps[16];
    sprintf(tmps,"%i",addr.sin_port);
    tmp_adress = _tcp_adress(inet_ntoa(addr.sin_addr),tmps);
    
    return &tmp_adress;
}

// --------------------------------------------------------
void _socket::set_block_mode(bool mode)
{
    unsigned long param;
    if (mode) param=0; else param=1;

    int err=ioctlsocket(s,FIONBIO,&param);
    if (err==SOCKET_ERROR) throw(_nline_error("Error in set_block_mode()"));
}
*/

// make socket function
_net_line *create_rlp_line(_rlp_rs *rs) { return new _rlp_socket(rs); }
