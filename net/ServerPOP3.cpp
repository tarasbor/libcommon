// ServerPOP3.cpp: implementation of the ServerPOP3 class.
//
//////////////////////////////////////////////////////////////////////

#include "ServerPOP3.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

ServerPOP3::ServerPOP3():p_proc(NULL)
{

}

ServerPOP3::~ServerPOP3()
{

}

// ---------------------------------------------------
// ---------------------------------------------------
int ServerPOP3::Connect(char const *ip,int port)
{
        int i;
        
        i=s.Connect(ip,port);
        if (i)
        {
                error_st=s.Error();
                return i;
        }

        i=command("","+OK");
        if (i) return i;

        return 0;
}

// ---------------------------------------------------
// ---------------------------------------------------
int ServerPOP3::Auth(char const *user,char const *pass)
{

        int i;
        char tmp[128];

        // USER
        sprintf(tmp,"USER %s\r\n",user);
        i=command(tmp,"+OK");
        if (i) return i;

        // PASS
        sprintf(tmp,"PASS %s\r\n",pass);
        i=command(tmp,"+OK");
        if (i) return i;

        return 0;
}

int ServerPOP3::MsgCountSize(int *_count,int *_size)
{
        int i;
        char tmp_c[1025];

        *_count=*_size=0;

        i=s.Send("LIST\r\n",6);
        if (i)
        {
                sprintf(tmp_c,"����POP3 %s:%s",tmp_c,s.Error().c_str());
                error_st=tmp_c;
                return i;
        }

        std::string in;
        while(1)
        {
                int size;
                i=s.Recv(tmp_c,1024,&size);
                if (i)
                {
                        sprintf(tmp_c,"���POP3 � LIST\r\n:%s",s.Error().c_str());
                        error_st=tmp_c;
                        return i;
                }

                tmp_c[size]='\0';
                in+=tmp_c;

                int pos=in.find("\r\n.\r\n");
                if (pos!=std::string::npos && pos==in.length()-5) break;

                pos=in.find("-ERR");
                if (pos!=std::string::npos && pos==in.length()-2)
                {
                        sprintf(tmp_c,"��� ��� POP3 ����LIST\r\n: %s",in.c_str());
                        error_st=tmp_c;
                        return 1;
                }

        }

	char *tmp=new char [in.length()+128];
	
        // answer like "+OK 2 Messages\r\n
        //             "1 1234\r\n"
        //             "2 30647\r\n"
        //             ".\r\n"

        // detect count
        int pos=4;
        pos=in.find(" ",pos);
        if (pos==std::string::npos)
        {
                sprintf(tmp,"���������� ����LIST:\r\n%s",in.c_str());
                error_st=tmp;
        }
	
        strcpy(tmp,in.c_str());
        tmp[pos]='\0';
        *_count=atoi(&tmp[4]);

        // detect size

        *_size=0;
        for(i=0;i<*_count;i++)
        {
                sprintf(tmp,"\r\n%i ",i+1);
                int len=strlen(tmp);

                pos=in.find(tmp);
                if (pos==std::string::npos)
                {
                        sprintf(tmp,"���������� ����LIST:\r\n%s",in.c_str());
                        error_st=tmp;
			delete tmp;
                        return 1;
                }

                int pos2=in.find("\r\n",pos+len);
                if (pos2==std::string::npos)
                {
                        sprintf(tmp,"���������� ����LIST:\r\n%s",in.c_str());
                        error_st=tmp;
			delete tmp;
                        return 1;
                }

                strcpy(tmp,in.c_str());
                tmp[pos2]='\0';
                *_size+=atoi(&tmp[pos+len]);
        }
    
    delete tmp;
    
    return 0;
}

// ---------------------------------------------------
int ServerPOP3::Del(int number)
{

        int i;
        char tmp[128];

        sprintf(tmp,"DELE %i\r\n",number);
        i=command(tmp,"+OK");
        if (i) return i;

        return 0;
}

// ---------------------------------------------------
// ---------------------------------------------------
int ServerPOP3::Disconnect()
{

        int i;

        // QUIT
        i=command("QUIT\r\n","+OK");
        if (i) return i;

        s.Disconnect();
        return 0;
}

// ---------------------------------------------------
// ---------------------------------------------------
int ServerPOP3::Recv(std::string &msg_text,int number)
{
        int i;
        char tmp[128];
        char *text;

        //detect message size
        sprintf(tmp,"LIST %i\r\n",number);
        i=command(tmp,"+OK");
        if (i) return i;

        sprintf(tmp,"+OK %i ",number);
        i=strlen(tmp);
        strcpy(tmp,last_answer.c_str());
        *strstr(tmp,"\r\n")='\0';
        int msg_size=atoi(&tmp[i]);

        // start recv message

        sprintf(tmp,"RETR %i\r\n",number);
        i=s.Send(tmp,strlen(tmp));
        if (i)
        {
                sprintf(tmp,"����POP3 %s:%s",tmp,s.Error().c_str());
                error_st=tmp;
                return i;
        }

        msg_size+=2048;                      // for msg header from pop3 server
        text = new char [msg_size+1];        // 1 - '\0' 
        msg_text="";
	
        int pos=0;
        while(1)
        {
	        
                // realloc text buffer , may be not need, but for safe
                if (msg_size-pos<1024)
                {		        
                        char *t = new char [msg_size+2048];
                        msg_size+=2048;
                        strcpy(t,text);
                        delete text;
                        text = t;
                }

                int size;
                i=s.Recv(&text[pos],1024,&size);
                if (i)
                {
                        sprintf(tmp,"��� ��������� ���:%s",s.Error().c_str());
                        error_st=tmp;
                        return i;
                }

                //if (size==1024 && p_proc) p_proc();
                 
                text[pos+size]='\0';
                char *end;
		int fpos=pos;
		if (pos>2) fpos=pos-2;
                if ((end=strstr(&text[fpos],"\r\n.\r\n"))!=NULL)
                {		        
                        *end='\0';
                        break;
                }

                pos+=size;
        }

        msg_text=text;

        delete text;
	
        return 0;

}

// ---------------------------------------------------
// ---------------------------------------------------
int ServerPOP3::command(char const *cmd,char const *answer)
{
        int i;
        char tmp[1024];

        if (strlen(cmd)>0)
        {
                i=s.Send(cmd,strlen(cmd));
                if (i)
                {
                        sprintf(tmp,"����POP3 %s:%s",cmd,s.Error().c_str());
                        error_st=tmp;
                        return i;
                }
        }

        std::string in;
        int last_pos=0;
        while(1)
        {
                int size;
                i=s.Recv(tmp,1024,&size);
                if (i)
                {
                        sprintf(tmp,"���POP3 � %s:%s",cmd,s.Error().c_str());
                        error_st=tmp;
                        return i;
                }

                tmp[size]='\0';
                in+=tmp;

                int pos=in.find("\r\n",last_pos);
                if (pos!=std::string::npos && pos==in.length()-2) break;
                last_pos=pos+2;

        }

        if (strncmp(answer,in.c_str(),strlen(answer)))
        {
                sprintf(tmp,"��� ��� POP3 ����%s : %s",cmd,in.c_str());
                error_st=tmp;
                return 1;
        }

        last_answer=in;

        return 0;
}
