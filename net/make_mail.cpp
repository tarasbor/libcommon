
// ------------------------------------------------
void GenerateDateSt(char *date)
{

    SYSTEMTIME time;
    SYSTEMTIME ltime;

    GetLocalTime(&time);
    GetSystemTime(&ltime);

    char t_diff[10];
    if ((time.wHour-ltime.wHour)*100+time.wMinute-ltime.wMinute < 0)
            sprintf(t_diff,"%03i%02i",time.wHour-ltime.wHour,time.wMinute-ltime.wMinute); 
    else
    sprintf(t_diff,"+%02i%02i",time.wHour-ltime.wHour,time.wMinute-ltime.wMinute);

    char *DoW[] = {"Sun","Mon","Tue","Wed","Thu","Fri","Sat"};
    char *Month[] = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};

    sprintf(date,"%s, %i %s %i %02i:%02i:%02i %s",DoW[time.wDayOfWeek],time.wDay,Month[time.wMonth-1],time.wYear,time.wHour,time.wMinute,time.wSecond,t_diff);
}

// ------------------------------------------------
void GenerateBoundary(char *b,char const *f_name)
{
    SYSTEMTIME time;
    GetLocalTime(&time);

    sprintf(b,"----=_NextPart_RDG_MAIL_MAKE_%i%i%i_%i%i%i_%s",time.wYear,time.wMonth,time.wDay,time.wHour,time.wMinute,time.wSecond,f_name);
}
                                                                                                                                                                    

// ----------------------------------------------------
int to_base64(char const *in,char *out,int size)
{

	char pad='=';
	int index = 0;
	int index_out = 0;
	unsigned char b1,b2,b3;
	unsigned char o1,o2,o3,o4;

	while(index<size)
	{
        b1=in[index];
		if (index+1<size) b2=in[index+1]; else b2=0;
		if (index+2<size) b3=in[index+2]; else b3=0;

        o1=(b1 >> 2) & 0x3F;
		o2=((b1 & 0x03) << 4) | ((b2 >> 4) & 0x0F);
		o3=((b2 & 0x0F) << 2) | ((b3 >> 6) & 0x03);
		o4=(b3) & 0x3F;

        out[index_out]=c_str[o1];
        out[index_out+1]=c_str[o2];
        out[index_out+2]=c_str[o3];
        out[index_out+3]=c_str[o4];

		index_out+=4;

		index+=3;

		if (index % 19 == 0)
		{
			out[index_out]='\r';
            out[index_out+1]='\n';
			index_out+=2;
		}
	}

	for(int i=0;i<index-size;i++) out[index_out-i-1]=pad;

	out[index_out]='\0';

	return index_out;
}

// mail_from - ����� ����������� 
// mail_to - ����� ����������
// data_in - ������ �� ������
// size - ������ ������ ������
// f_name - ��� ������
// text - ������� ����� � ����������
// ------------------------------------------------
int MakeMail(const char *main_from,
             const char *main_to,
			 char *data_in,int size,char *f_name,
			 string &text)
{
    char tmp[256];
    char boundary[256];
    char date[256];

    GenerateDateSt(date);
    GenerateBoundary(boundary,f_name);

    // head
    sprintf(tmp,"From: <%s>\r\n",mail_from);
    text+=tmp;
    sprintf(tmp,"To: <%s>\r\n",mail_to);
    text+=tmp;
    sprintf(tmp,"Subject: %s\r\n",f_name);
    text+=tmp;
    sprintf(tmp,"Date: %s\r\n",date);
    text+=tmp;
    sprintf(tmp,"MIME-Version: 1.0\r\n");
    text+=tmp;
    sprintf(tmp,"Content-Type: multipart/mixed;\r\n");
    text+=tmp;
    sprintf(tmp,"\tboundary=\"%s\"\r\n",boundary);
    text+=tmp;
    sprintf(tmp,"X-Mail-make-by: RDG Main DLL\r\n");
    text+=tmp;
    sprintf(tmp,"X-Mailer: RDG Main DLL\r\n");
    text+=tmp;
    sprintf(tmp,"\r\nThis is a multi-part message in MIME format.\r\n\r\n",boundary);
    text+=tmp;

    // body
    sprintf(tmp,"--%s\r\n",boundary);
    text+=tmp;
    sprintf(tmp,"Content-Type: text/plain;\r\n");
    text+=tmp;
    sprintf(tmp,"\tchar-set=\"windows-1251\";\r\n");
    text+=tmp;
    sprintf(tmp,"Content-Transfer-Encoding: 7bit\r\n");
    text+=tmp;
    sprintf(tmp,"\r\nThis message contain %s file from %s.\r\n\r\n",f_name,mail_from);
    text+=tmp;
    
    //attach
    sprintf(tmp,"--%s\r\n",boundary);
    text+=tmp;
    sprintf(tmp,"Content-Type: application/octet-stream;\r\n");
    text+=tmp;
    sprintf(tmp,"\tname=\"%s\"\r\n",f_name);
    text+=tmp;
    sprintf(tmp,"Content-Transfer-Encoding: base64\r\n");
    text+=tmp;
    sprintf(tmp,"Content-Disposition: attachment;\r\n");
    text+=tmp;
    sprintf(tmp,"\tfilename=\"%s\"\r\n\r\n",f_name);
    text+=tmp;

    char *data_out = new char [size * 2];
    to_base64(data_in,data_out,size);

    text+=data_out;
    text+="\r\n\r\n";

    delete data_out;

    // end
    sprintf(tmp,"--%s--\r\n",boundary);
    text+=tmp;

    return res;
}

