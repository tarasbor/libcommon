/***************************************************************************
 *   What is it: This file is part of communication lib.
 *   Content: error & log class release
 *   Version: 0.1b
 *   Last-Mod: 24.09.2006
 *
 *   Copyright (C) 2006 by Proshin Alexey
 *   paa713@mail.ru
 *
 *   The contents of this file are subject to free software;
     you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   The contents of this file is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***************************************************************************/

#include "net_lib_error.h"
#include <stdio.h>
#include <string.h>
#include <time.h>

using namespace std;

#ifdef OLD_NET_LIB_LOG
bool _nlog::_need_log(false);
std::list<_nlog::_log_line> *_nlog::lines;
int _nlog::ref(0);
_locker _nlog::lock;
bool _nlog::_have_date;

// ---------------------------------------------------------
void _nlog::clear()
{
    lock.lock();
    lines->clear();
    _have_date=false;
    lock.unlock();
}

// ---------------------------------------------------------
void _nlog::start()
{
    lock.lock();
    _need_log=true;
    _have_date=false;
    lock.unlock();
}

// ---------------------------------------------------------
_nlog::_nlog()
{
  lock.lock();
  ref++;
  if (ref==1)
  {
      lines = new list<_log_line>;
  }
  lock.unlock();
}

// ---------------------------------------------------------
_nlog::~_nlog()
{
  lock.lock();
  ref--;
  if (!ref)
  {
//    lines->clear();
    delete lines;
  }
  lock.unlock();
}

// ---------------------------------------------------------
void _nlog::out(const std::string &st,int log_level)
{
  lock.lock();
  if (_need_log)
  {
    time_t c_time=time(NULL);
    char tmp[1024];
    strftime(tmp,64,"%Y.%m.%d %T",localtime(&c_time));
    strcat(tmp," : ");
    sprintf(&tmp[strlen(tmp)],"[%lu] : %s",get_current_thread_id(),st.c_str());

    _log_line line;
    line.level=log_level;
    line.txt=tmp;

    lines->push_back(line);
    _have_date=true;
  }
  lock.unlock();
}

// -----------------------------------------------------------------
const std::string _nlog::get(int log_level) const
{
  lock.lock();

  std::string st;
  std::list<_log_line>::const_iterator it=lines->begin();
  for(;it!=lines->end();it++)
  {
    if ((*it).level>log_level) continue;
    st+=(*it).txt;
    st+="\n";
  }

  lines->clear();

  lock.unlock();


  return st;
}
#endif
