
#ifndef __NET_SMTP_CLIENT_H__
#define __NET_SMTP_CLIENT_H__

#include <string>
#include "net_lib.h"

using namespace std;

class _smtp_client
{
	public:
		virtual ~_smtp_client() {};
		virtual void release() = 0;
		
		virtual void connect(const _net_adress &adress) = 0;
		virtual void disconnect() = 0;
		
		virtual void helo(const string &hello) = 0;
		virtual void ehlo(const string &hello) = 0;
		
		virtual void auth_plain(const string &mail,const string &uname,const string &upass) = 0;
		virtual void auth_login(const string &uname,const string &upass) = 0;
		
		virtual void send(const string &from,const string &to,const string &data) = 0;
};

_smtp_client *create_smtp_client();

#endif

/*
#include "_socket.h"
#include "base64.h"

typedef void (*PROGRESSPROC)();

class ServerSMTP  
{
private:

	_socket s;
	std::string error_st;
	std::string last_answer;

	int command(char const *cmd,char const *anser);

	PROGRESSPROC p_proc;

public:

	ServerSMTP();
	virtual ~ServerSMTP();

	int Connect(char const *ip,int port);
	int Send(char const *from,char const *to,char const *text);
	int Disconnect();
	std::string Error() { return error_st; };
	void SetCallback(PROGRESSPROC p) { p_proc=p; };
	int AuthPlain(char const *mail,char const *user_name,char const *pass);
	int AuthLogin(char const *user_name,char const *pass);

};

*/