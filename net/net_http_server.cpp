/***************************************************************************
 *   This file contain release http server
 *
 *   Copyright (C) 2006 by Proshin Alexey
 *   paa713@mail.ru
 *
 *   The contents of this file are subject to free software;
     you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   The contents of this file is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***************************************************************************/

#include <net_http_server.h>
#include <net_server_tcp.h>
#include <stdlib.h>

// -------------------------------------------------------------------
// http server
// -------------------------------------------------------------------
class _http_server:public _net_server
{
  friend _net_server *create_http_server();

  private:

        _net_server_processor *processor;
        _net_server_process *listen;

        _http_server():processor(NULL),listen(NULL) {};
        virtual ~_http_server() { shutdown(); };

  public:

        virtual void online(const _net_adress &adress);
        virtual void shutdown();
        virtual void release() { delete this; };

        virtual bool in_process(std::string &in,std::string &out,_net_server_connection *ns);
        virtual void set_processor(_net_server_processor *pr) { processor=pr; };
		virtual _net_server_processor *get_processor() { return processor; };

        virtual void reg_connection(_net_server_connection *c) {};
        virtual void unreg_connection(_net_server_connection *c) {};

        virtual void keep_alive_s(int sec) { if (listen) listen->keep_alive_s(sec); };
        virtual int keep_alive_s() const { if (listen) return listen->keep_alive_s(); return 0; };

        virtual void reinit_time_s(int sec) { };
        virtual int reinit_time_s() const { return 0; };
};

// ---------------------------------------------------
void _http_server::shutdown()
{
  if (!listen) return;
  listen->Kill();
  while(listen->GetStatus()!=TH_STATUS_KILLED) Sleep(100);
  listen->release();
  listen=NULL;
}

// ---------------------------------------------------
void _http_server::online(const _net_adress &adress)
{
  if (listen) return;
  listen=create_tcp_server_process(adress,this);
  listen->Start();
  while(!listen->is_online()) { Sleep(100); };
}

// ---------------------------------------------------
bool _http_server::in_process(std::string &in,std::string &out,_net_server_connection *ns)
{
    if (!processor) return false;

    std::string::size_type i;
    std::string::size_type head_len=0;
    std::string::size_type body_len=0;

    std::string::size_type pos=in.find("\r\n\r");
    if (pos!=std::string::npos) head_len=pos+4; else return false;

    i=in.find("Content-length");
    if (i==std::string::npos) i=in.find("Content-Length");
    if (i!=std::string::npos)
    {
      std::string::size_type j=in.find('\r',i);
      i=in.find(':',i+1);
      body_len=atoi(in.substr(i+1,j-i-1).c_str());
      if (in.length()<head_len+body_len) return false;
    }

	bool res=processor->process(in,out,ns);
	if (res) in="";
    return res;
}

// ---------------------------------------------------
_net_server *create_http_server() { return new _http_server(); }
