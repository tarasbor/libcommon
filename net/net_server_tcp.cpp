/***************************************************************************
 *   What is it: This file is part of communication lib.
 *   Content: releases classes for TCP server
 *   Version: 0.1b
 *   Last-Mod: 24.09.2006
 *
 *   Copyright (C) 2006 by Proshin Alexey
 *   paa713@mail.ru
 *
 *   The contents of this file are subject to free software;
     you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   The contents of this file is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***************************************************************************/

#include "net_server_tcp.h"
#include "net_line_tcp.h"
#include "net_line.h"
#include <list>
#include <time.h>

#include <iostream>

using namespace std;

#define NET_LIB_LOG

#ifdef NET_LIB_LOG
   #include <log.hpp>
   #define MLOG(A) _log() << _log::_set_module_level("net_server_tcp",(A))
#endif


// ----------------------------------------------------
// tcp server<->client connection
// ----------------------------------------------------
class _tcp_server_connection:public _net_server_connection
{
  friend _net_server_connection *create_tcp_server_connection(_net_line *line,_net_server *server,int ka);

  private:

    _net_line *s;
    _net_server *parent;
    time_t last_time;
    int _keep_alive_time;
    _locker lock;

    std::string in;
    std::string out;

    _tcp_server_connection(_net_line *line,_net_server *server,int ka);
    virtual ~_tcp_server_connection();

  public:
    virtual void release() { delete this; };

    virtual void set_keep_alive_time(int sec) { _keep_alive_time=sec; };
    virtual _locker *locker() { return &lock; };
    virtual _net_line *net_line() { return s; };
    virtual void to_out(const std::string &data) { out+=data; };

    virtual int poll();
};

// ----------------------------------------------------
_tcp_server_connection::_tcp_server_connection(_net_line *line,_net_server *server,int ka):
                        s(line),parent(server),last_time(time(NULL)),_keep_alive_time(ka),
                        in(""),out("")
{
#ifdef NET_LIB_LOG
    MLOG(SERVER_LOG_LEVEL2) << "_tcp_server_connection::() : i am start..." << endl;
#endif
}

// ----------------------------------------------------
_tcp_server_connection::~_tcp_server_connection()
{
#ifdef NET_LIB_LOG
    MLOG(SERVER_LOG_LEVEL2) << "_tcp_server_connection::~() : i am shutdown..." << endl;
#endif

    if (s)
    {
        s->shutdown();
        s->release();
    }
    s=NULL;
}

// ----------------------------------------------------
int _tcp_server_connection::poll()
{
    lock.lock();

    try
    {
        timeval tv={0,0};

        // at first send out data (if we have out data) - for stream servers
        if (!out.empty())
        {
            bool can_write;

            s->is_ready(NULL,&can_write,NULL,tv);
            if (can_write)
            {
                #ifdef NET_LIB_LOG
                        MLOG(SERVER_LOG_LEVEL2) << "_tcp_server_connection::Poll : have data to send. Send answer way is clear..." << endl;
                #endif

                s->send(out);
                out.clear();

                #ifdef NET_LIB_LOG
                        MLOG(SERVER_LOG_LEVEL2) << "_tcp_server_connection::Poll : Send done..." << endl;
                #endif
            }
            /*
            else
            {
                #ifdef NET_LIB_LOG
                        MLOG(SERVER_LOG_LEVEL2) << "_tcp_server_connection::Poll : have data to send. client don`t `want` recive it. Wait for next time..." << endl;
                #endif

                last_time=time(NULL);
                lock.unlock();
                return 0;
            }
            */

            last_time=time(NULL);
        }

        bool ready_read;

        s->is_ready(&ready_read,NULL,NULL,tv);
        if (ready_read)
        {
            #ifdef NET_LIB_LOG
                MLOG(SERVER_LOG_LEVEL2) << "_tcp_server_connection::Poll : have income data. Recive it..." << endl;
            #endif

            std::string tmp;
            s->recv(tmp);

            if (tmp.empty())
            {
                lock.unlock();

                #ifdef NET_LIB_LOG
                    MLOG(SERVER_LOG_LEVEL2) << "_tcp_server_connection::Poll : recive empty data. Connection was closed by remoute side..." << endl;
                #endif

                return -1;
            }

            in+=tmp;

            #ifdef NET_LIB_LOG
                MLOG(SERVER_LOG_LEVEL2) << "_tcp_server_connection::Poll : have income data. data recive done. Process it..." << endl;
            #endif

            // WE MUST UNLOCK current connection while proccess data
            lock.unlock();
            bool was_processed=parent->in_process(in,out,this);
            lock.lock();

            last_time=time(NULL);
        }

        if (_keep_alive_time && difftime(time(NULL),last_time)>_keep_alive_time)
        {
            lock.unlock();
            return -1;
        }
    }
    catch(_nerror &e)
    {
        #ifdef NET_LIB_LOG
                if (e.err_no()==0)
                        MLOG(SERVER_LOG_LEVEL2) << "_tcp_server_connection::Poll : connection was closed by remote side" << endl;
                else
                        MLOG(SERVER_LOG_LEVEL1) << "_tcp_server_connection::Poll : error was - " << e.err_st_full() << endl;
        #endif

        lock.unlock();
        return -1;
    }

    lock.unlock();

    return 0;
}


// ----------------------------------------------------
_net_server_connection *create_tcp_server_connection(_net_line *line,_net_server *server,int ka)
{
     return new _tcp_server_connection(line,server,ka);
}

// connections manager interface
// ----------------------------------------------------
// ----------------------------------------------------
class _conns_manager
{
    protected:
        _net_server *cm_server;

    public:
        _conns_manager(_net_server *server):cm_server(server) {};
        virtual ~_conns_manager() {};

        virtual void add_connection(_net_line *line,int ka) = 0;
        virtual void check_connections() = 0;
};

// connections manager (strategy - multi - one connection = one thread)
// ----------------------------------------------------
// ----------------------------------------------------
class _conns_manager_mth:public _conns_manager
{
    protected:

        // thread wrapper for connection
        class _con_thread:public _thread
        {
            protected:
                _net_server_connection *cm_con;

            public:
                _con_thread(_net_server_connection *c):cm_con(c) { SetSleepTime(1); };
                virtual ~_con_thread() { if (cm_con) cm_con->release(); cm_con=NULL; };
                virtual _net_server_connection *connection() { return cm_con; };
                virtual int Poll() { if (cm_con) return cm_con->poll(); return -1; };
        };

        typedef std::list<_con_thread *> _con_threads_list;
        _con_threads_list cm_con_threads_list;

    public:
        _conns_manager_mth(_net_server *server):_conns_manager(server) {
            #ifdef NET_LIB_LOG
                MLOG(SERVER_LOG_LEVEL2) << "_conns_manager_mth::() - ..." << endl;
            #endif
        }
        virtual ~_conns_manager_mth();

        virtual void add_connection(_net_line *line,int ka);
        virtual void check_connections();
};

// ----------------------------------------------------
_conns_manager_mth::~_conns_manager_mth()
{
#ifdef NET_LIB_LOG
    MLOG(SERVER_LOG_LEVEL2) << "_conns_manager_mth::~() - ..." << endl;
#endif
    _con_threads_list::iterator it=cm_con_threads_list.begin();
    while(it!=cm_con_threads_list.end())
    {
        (*it)->Kill();
        while((*it)->GetStatus()!=TH_STATUS_KILLED) Sleep(100);
        cm_server->unreg_connection((*it)->connection());
        delete (*it);
        it=cm_con_threads_list.erase(it);
    }
}

// ----------------------------------------------------
void _conns_manager_mth::add_connection(_net_line *line,int ka)
{
#ifdef NET_LIB_LOG
    string remoteAddr;
    /*string localAddr;
    localAddr=line->local_adress()->get(0);
    localAddr+=":";
    localAddr+=line->local_adress()->get(1);*/
    remoteAddr=line->remoute_adress()->get(0);
    remoteAddr+=":";
    remoteAddr+=line->remoute_adress()->get(1);
    MLOG(SERVER_LOG_LEVEL4) << "_conns_manager_mth::add_connection(...) : ka - " << ka << " : " /*<< localAddr*/ << "-" << remoteAddr << endl;
#endif
    _net_server_connection *con=create_tcp_server_connection(line,cm_server,ka);
    _con_thread *con_th=new _con_thread(con);
    cm_con_threads_list.push_back(con_th);

	cm_server->reg_connection(con);

    int err=con_th->Start();
	if (err)
    {
        #ifdef NET_LIB_LOG
            MLOG(SERVER_LOG_LEVEL1) << "_conns_manager_mth::add_connection : start conection error  - " << err << endl;
        #endif
    }
}

// ----------------------------------------------------
void _conns_manager_mth::check_connections()
{
    _con_threads_list::iterator it=cm_con_threads_list.begin();
    while(it!=cm_con_threads_list.end())
    {
        int status=(*it)->GetStatus();
        if (status==TH_STATUS_CRUSH || status==TH_STATUS_KILLED)
        {
            #ifdef NET_LIB_LOG
                string remoteAddr; /*string localAddr;
                localAddr=(*it)->connection()->net_line()->local_adress()->get(0);
                localAddr+=":";
                localAddr+=(*it)->connection()->net_line()->local_adress()->get(1);*/
                remoteAddr=(*it)->connection()->net_line()->remoute_adress()->get(0);
                remoteAddr+=":";
                remoteAddr+=(*it)->connection()->net_line()->remoute_adress()->get(1);

                MLOG(SERVER_LOG_LEVEL4) << "_conns_manager_mth::check_connections() : unreg_connection : " /*<< localAddr*/ << "-" << remoteAddr << endl;
            #endif
            cm_server->unreg_connection((*it)->connection());
            delete (*it);

            // it=con_list.erase(it); may be unsafe (why ???? i do not remember)
			cm_con_threads_list.erase(it);
			it=cm_con_threads_list.begin();

            continue;
        }
        it++;
    }
}

// connections manager (strategy - single - one thread for all connections)
// ----------------------------------------------------
// ----------------------------------------------------
class _conns_manager_sth:public _conns_manager
{
    protected:

        // work thread
        class _work_thread:public _thread
        {
            protected:
                _net_server *cm_server;
                _locker cm_lock;

                typedef std::list<_net_server_connection *> _cons_list;
                _cons_list cm_cons_list;

            public:
                _work_thread(_net_server *server);
                virtual ~_work_thread();

                virtual int Poll();
                virtual void add_connection(_net_line *line,int ka);
        };

        _work_thread *cm_work_thread;

    public:
        _conns_manager_sth(_net_server *server);
        virtual ~_conns_manager_sth();

        virtual void add_connection(_net_line *line,int ka) {
            #ifdef NET_LIB_LOG
                MLOG(SERVER_LOG_LEVEL2) << "_conns_manager_sth::add_connection(...) - ..." << endl;
            #endif
            if (cm_work_thread) cm_work_thread->add_connection(line,ka);
        }
        virtual void check_connections() { /* just stub in this strategy */ };
};

// ----------------------------------------------------
_conns_manager_sth::_conns_manager_sth(_net_server *server):_conns_manager(server),cm_work_thread(NULL)
{
#ifdef NET_LIB_LOG
    MLOG(SERVER_LOG_LEVEL2) << "_conns_manager_sth::_conns_manager_sth() - constructor..." << endl;
#endif

    cm_work_thread = new _work_thread(cm_server);
    cm_work_thread->SetSleepTime(1);
    cm_work_thread->Start();
}

// ----------------------------------------------------
_conns_manager_sth::~_conns_manager_sth()
{
#ifdef NET_LIB_LOG
    MLOG(SERVER_LOG_LEVEL2) << "_conns_manager_sth::~_conns_manager_sth() - destructor..." << endl;
#endif

    if (cm_work_thread)
    {
        cm_work_thread->Kill();
        while((cm_work_thread->GetStatus()!=TH_STATUS_KILLED)) Sleep(100);
        delete cm_work_thread;
        cm_work_thread = NULL;
    }
}

// ----------------------------------------------------
_conns_manager_sth::_work_thread::_work_thread(_net_server *server):cm_server(server)
{
}

// ----------------------------------------------------
_conns_manager_sth::_work_thread::~_work_thread()
{
    _cons_list::iterator it=cm_cons_list.begin();
    while(it!=cm_cons_list.end())
    {
        cm_server->unreg_connection((*it));
        (*it)->release();
        it=cm_cons_list.erase(it);
    }
}

// ----------------------------------------------------
int _conns_manager_sth::_work_thread::Poll()
{
    cm_lock.lock();

    _cons_list::iterator it=cm_cons_list.begin();
    while(it!=cm_cons_list.end())
    {
        int res=(*it)->poll();
        if (res<0)
        {
            cm_server->unreg_connection((*it));
            (*it)->release();

            it=cm_cons_list.erase(it);
            continue;
        }
        it++;
    }

    cm_lock.unlock();
}

// ----------------------------------------------------
void _conns_manager_sth::_work_thread::add_connection(_net_line *line,int ka)
{
    cm_lock.lock();

    _net_server_connection *con=create_tcp_server_connection(line,cm_server,ka);
    cm_cons_list.push_back(con);

	cm_server->reg_connection(con);

    cm_lock.unlock();
}

// connections manager (strategy - pool - threads pool for all connections)
// ----------------------------------------------------
// ----------------------------------------------------
class _conns_manager_pth:public _conns_manager
{
    public:
        _conns_manager_pth(_net_server *server):_conns_manager(server) {};
        virtual ~_conns_manager_pth() {};

        virtual void add_connection(_net_line *line,int ka);
};


// ----------------------------------------------------
// tcp server listen & accept connection
// ----------------------------------------------------
class _tcp_server_process:public _net_server_process
{
  friend _net_server_process *create_tcp_server_process(const _net_adress &adress,_net_server *server);

  private:

        bool _is_online;

        _net_server *parent;
        _net_line *s;

        _tcp_adress adress;

        time_t last_reinit_time;
        int cm_reinit_time;

        int cm_keep_alive_time;

        _conns_manager *cm_cmanager;

        _tcp_server_process(const _net_adress &adress,_net_server *server);
        virtual ~_tcp_server_process();

        virtual int Poll();

  public:

        virtual void release() { delete this; };
        virtual bool is_online() const { return _is_online; };

        virtual void keep_alive_s(int sec) { cm_keep_alive_time=sec; };
        virtual int keep_alive_s() const { return cm_keep_alive_time; };

        virtual void reinit_time_s(int sec) { cm_reinit_time=sec; };
        virtual int reinit_time_s() const { return cm_reinit_time; };
};

// -------------------------------------------------------------------
_tcp_server_process::_tcp_server_process(const _net_adress &_adress,_net_server *server):
                    cm_cmanager(NULL),
                     _is_online(false),parent(server),s(create_socket_line()),
                     adress(_adress.get(0),_adress.get(1)),last_reinit_time(0),cm_keep_alive_time(0),cm_reinit_time(0)
{
#ifdef NET_LIB_LOG
    MLOG(SERVER_LOG_LEVEL1) << "_tcp_server_process::_tcp_server_process - create..." << endl;
#endif

    cm_cmanager = new _conns_manager_mth(server);

    SetSleepTime(1);
    Start();
}

// -------------------------------------------------------------------
_tcp_server_process::~_tcp_server_process()
{
#ifdef NET_LIB_LOG
    MLOG(SERVER_LOG_LEVEL1) << "_tcp_server_process::~_tcp_server_process - stop..." << endl;
#endif

    if (!is_online()) return;

#ifdef NET_LIB_LOG
    MLOG(SERVER_LOG_LEVEL2) << "_tcp_server_process::~_tcp_server_process - shutdown socket..." << endl;
#endif
    s->shutdown();
    s->release();

#ifdef NET_LIB_LOG
    MLOG(SERVER_LOG_LEVEL2) << "_tcp_server_process::~_tcp_server_process - release connections manager..." << endl;
#endif

    delete cm_cmanager;
    cm_cmanager = NULL;

    _is_online=false;

#ifdef NET_LIB_LOG
    MLOG(SERVER_LOG_LEVEL1) << "_tcp_server_process::~_tcp_server_process - stoped..." << endl;
#endif

}

// -------------------------------------------------------------------
int _tcp_server_process::Poll()
{
    if (!is_online())
    {
        #ifdef NET_LIB_LOG
                MLOG(SERVER_LOG_LEVEL1) << "_tcp_server_process::Poll : try to go on-line..." << endl;
        #endif

        try { s->stay_server_at(adress); }
        catch(_nerror &e)
        {
            #ifdef NET_LIB_LOG
                MLOG(SERVER_LOG_LEVEL1) << "_tcp_server_process::Poll : try to go on-line...error - " << e.err_st_full() << endl;
            #endif
            Sleep(1000);
            return 0;
        }

        #ifdef NET_LIB_LOG
                MLOG(SERVER_LOG_LEVEL1) << "_tcp_server_process::Poll : ONLINE" << endl;
        #endif

		_is_online=true;

        if (cm_reinit_time) last_reinit_time=time(NULL);

		return 0;
    }

    if (cm_reinit_time && difftime(time(NULL),last_reinit_time)>cm_reinit_time)
    {
        #ifdef NET_LIB_LOG
                MLOG(SERVER_LOG_LEVEL1) << "_tcp_server_process::Poll : server stop...by reinit timeout - TCP_SERVER_TO_REINIT" << endl;
        #endif
        s->shutdown();
        s->release();

        _is_online=false;
        s=NULL;
        s=create_socket_line();

        return 0;
    }

    timeval time={0,0};
    bool read;
    try { s->is_ready(&read,NULL,NULL,time); }
    catch(_nerror &e)
    {
        #ifdef NET_LIB_LOG
                MLOG(SERVER_LOG_LEVEL1) << "_tcp_server_process::Poll : select NOT ACCEPTED - " << e.err_st_full() << endl;
        #endif
        return 0;
    }

    if (read)
    {
        #ifdef NET_LIB_LOG
                MLOG(SERVER_LOG_LEVEL1) << "_tcp_server_process::Poll : income connection. Try accept..." << endl;
        #endif

        _net_line *tmp;
        try { tmp=s->accept(); }
        catch(_nerror &e)
        {
            #ifdef NET_LIB_LOG
                MLOG(SERVER_LOG_LEVEL1) << "_tcp_server_process::Poll : NOT ACCEPTED - " << e.err_st_full() << endl;
            #endif
            return -1;
        }

        cm_cmanager->add_connection(tmp,cm_keep_alive_time);
    }

    // check killed or crushed connections
    cm_cmanager->check_connections();

    return 0;
}

// -------------------------------------------------------------------
_net_server_process *create_tcp_server_process(const _net_adress &adress,_net_server *server)
{
    return new _tcp_server_process(adress,server);
}
