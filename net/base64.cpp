#include "base64.h"

char c_str[]="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

// ----------------------------------------------------
// ----------------------------------------------------
int from_base64(unsigned char *st,int *n)
{

	char *tmp=new char [*n];

	// first - decode text string to bytes
	int i;
	int j;
	int pad_cnt=0;

	for(i=0,j=0;i<*n;i++)
	{
		for(int o=0;o<sizeof(c_str);o++)
			if (st[i]==c_str[o])
			{
				tmp[j]=o;
				j++;
				break;
			}
		if (st[i]=='=')
		{
			tmp[j]=0;
			j++;
			pad_cnt++;
		}
	}

	*n=j;

	// step 2 - from 4 6-bit make 3 8-bit
	for(i=0,j=0;i<*n;i+=4,j+=3)
	{
		char o1=tmp[i];
		char o2=tmp[i+1];
		char o3=tmp[i+2];
		char o4=tmp[i+3];

		st[j]=((o1 << 2) & 0xFC) | ((o2 >> 4) & 0x03);
		st[j+1]=((o2 << 4) & 0xF0) | ((o3 >> 2) & 0x0F); 
		st[j+2]=((o3 << 6)  & 0xC0) | o4;
	}

	*n=j-pad_cnt;

	delete tmp;

	return 0;
}


// ----------------------------------------------------
// ----------------------------------------------------
int to_base64(char const *in,char *out,int size)
{

	char pad='=';
	int index = 0;
	int index_out = 0;
	unsigned char b1,b2,b3;
	unsigned char o1,o2,o3,o4;

	while(index<size)
	{
        b1=in[index];
		if (index+1<size) b2=in[index+1]; else b2=0;
		if (index+2<size) b3=in[index+2]; else b3=0;

        o1=(b1 >> 2) & 0x3F;
		o2=((b1 & 0x03) << 4) | ((b2 >> 4) & 0x0F);
		o3=((b2 & 0x0F) << 2) | ((b3 >> 6) & 0x03);
		o4=(b3) & 0x3F;

        out[index_out]=c_str[o1];
        out[index_out+1]=c_str[o2];
        out[index_out+2]=c_str[o3];
        out[index_out+3]=c_str[o4];

		index_out+=4;

		index+=3;

		if (index % 19 == 0)
		{
			out[index_out]='\r';
            out[index_out+1]='\n';
			index_out+=2;
		}
	}

	for(int i=0;i<index-size;i++) out[index_out-i-1]=pad;

	out[index_out]='\0';

	return index_out;
}
