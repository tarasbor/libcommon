/***************************************************************************
 *   What is it: This file is part of communication lib.
 *   Content: releases classes for network ssl line (point-point)
 *   Version: 0.1b
 *   Last-Mod: 24.09.2006
 *                                                                         
 *   Copyright (C) 2006 by Proshin Alexey                                  
 *   paa713@mail.ru                                                        
 *                                                                         
 *   The contents of this file are subject to free software; 
     you can redistribute it and/or modify  
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or     
 *   (at your option) any later version.                                   
 *                                                                         
 *   The contents of this file is distributed in the hope that it will be useful,       
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
 *   GNU General Public License for more details.                          
 *                                                                         
 *   You should have received a copy of the GNU General Public License     
 *   along with this program; if not, write to the                         
 *   Free Software Foundation, Inc.,                                       
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
 ***************************************************************************/

#include <net_line_ssl.h>

#ifdef WINDOWS
        #include <windows.h>
        #include <winsock.h>
#else
        #include <sys/types.h>
        #include <sys/socket.h>
        #include <string>
        #include <errno.h>
        #include <netinet/in.h>
        #include <arpa/inet.h>
        #include <openssl/ssl.h>
        #include <openssl/err.h>

        typedef int SOCKET;
        #define INVALID_SOCKET -1
        #define SOCKET_ERROR -1
#endif

// ssl line error class (for ssl errors)
// -------------------------------------------------------
class _nssl_error:public _nerror
{
  private:
        std::string operation;
        int error_number;
        std::string err_string;

  public:        
        _nssl_error(int err_no,const char *err_st, const std::string _oper):
                    error_number(err_no),operation(_oper),err_string(err_st) {};
        virtual std::string err_st() const { return std::string(ERR_error_string(error_number,NULL)); };
        virtual std::string err_st_full() const { return std::string(ERR_error_string(error_number,NULL))+
                                            std::string(" on command ")+operation; };
        virtual long err_no() const { return error_number; };

};

// ------------------------------------------------------------------
// SSL socket class
// ------------------------------------------------------------------
class _ssl_socket:public _net_line
{
  private:
        SOCKET s;
        SSL *ssl;
        _ssl_set *ssl_set;
        bool is_connected;

        _ssl_socket(const _ssl_socket &_s) { }; 
        _ssl_socket &operator=(const _ssl_socket &s) { return *this; };         
        
        const SOCKET handle() const { return s; };
        
        operator SOCKET() const { return s; };
        
        ~_ssl_socket() { shutdown(); };
        
  public:
        _ssl_socket(_ssl_set *ss):s(INVALID_SOCKET),is_connected(false),ssl(NULL),ssl_set(ss) { };
        _ssl_socket(SOCKET h,SSL *_ssl):s(h),is_connected(true),ssl(_ssl) {};
        
        virtual void connect(const _net_adress &adress);
        virtual void stay_server_at(const _net_adress &adress); 

        virtual void send(const std::string &data);
        virtual void recv(std::string &data);
        
        virtual void shutdown();
        
        _net_line *accept();
        
        virtual void release() { delete this; };
        
        virtual void is_ready(bool *read,bool *write,bool *error,timeval &tv);
        
};

// ---------------------------------------------------
// ---------------------------------------------------
void _ssl_socket::connect(const _net_adress &adress)
{
        sockaddr_in sockAddr;
        char st[1000];
        int i;
 
        s=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);      
        if (s==INVALID_SOCKET)
        {
              throw _nssl_error(errno,strerror(errno),"socket() in _ssl_socket::connect()");
              return;
        }

        memset(&sockAddr,0,sizeof(sockAddr));
        sockAddr.sin_family=AF_INET;
        sockAddr.sin_port=htons(atoi(adress.get(1).c_str()));
        sockAddr.sin_addr.s_addr=inet_addr(adress.get(0).c_str());
        i=::connect(s,(sockaddr *)&sockAddr,sizeof(sockAddr));
        if (i)
        {
             throw _nssl_error(errno,strerror(errno),"connect() in _ssl_socket::connect()");
             shutdown();
             return;         
        }

        is_connected=1;        
}

// ---------------------------------------------------
// ---------------------------------------------------
void _ssl_socket::shutdown()
{
#ifdef WINDOWS
        if (is_connected && s!=INVALID_SOCKET) { ::shutdown(s,SD_BOTH); is_connected=false; } 
#else            
        if (is_connected && s!=INVALID_SOCKET && ssl)
        {
                SSL_shutdown(ssl);
                ::shutdown(s,SHUT_RDWR);
                is_connected=false;
        }
#endif
        if (ssl) { SSL_free(ssl); ssl=NULL; }
        if (s!=INVALID_SOCKET) { ::close(s); s=INVALID_SOCKET; }
}

// ---------------------------------------------------
// ---------------------------------------------------
void _ssl_socket::send(const std::string &data)
{
    int err;
    int pos=0;
    int size=data.length();

        while(1)
        {               
                //cout << "befor SSL_write " << data.length() << " " << pos << endl << flush;
                err=SSL_write(ssl,data.c_str()+pos,size);
                //cout << "after SSL_write " << endl << flush;
                if (err<0) 
                {
                        //cout << "err < 0 " << endl << flush;
                        int e=ERR_get_error();
                        throw _nssl_error(e,ERR_error_string(e,NULL),
                                "SSL_write() in _ssl_socket::send()");
                }
                if (err==0)
                {
                        //cout << "err = 0 " << endl << flush;
                        int e=ERR_get_error();
                        throw _nssl_error(e,ERR_error_string(e,NULL),
                                         "remote side of connection was closed in _ssl_socket::send()");
                }
                if (err==size) break;  
                pos+=err;
                size-=err;
        }
        
        //cout << "send done " << endl << flush;
}

// ---------------------------------------------------
// ---------------------------------------------------
void _ssl_socket::recv(std::string &data)
{
    int err;
    data="";
    
    char *tmp=new char [1024];

    while(1)
    {
        err=::SSL_read(ssl,tmp,1024);
        if (err<0)
        {
                delete tmp;
                int e=ERR_get_error();
                throw _nssl_error(e,ERR_error_string(e,NULL),"SSL_read() in _ssl_socket::recv()");
        }
        if (err==0)
        {
                delete tmp;
                int e=ERR_get_error();
                throw _nssl_error(e,ERR_error_string(e,NULL),
                                      "remote side of connection was closed in _ssl_socket::recv()");
        }
        data+=std::string(tmp,err);
        
        fd_set set;
        FD_ZERO(&set);
        FD_SET(s,&set);
        timeval tv={0,0};
        
        err=select(s+1,&set,NULL,NULL,&tv);
        
        if (err==0) break;
        if (err==SOCKET_ERROR)
        {
            delete tmp;
            throw _nssl_error(errno,strerror(errno),"select() in _ssl_socket::recv()");
        }
    }
    
    delete tmp;
}

// ---------------------------------------------------
void _ssl_socket::stay_server_at(const _net_adress &adress)
{
sockaddr_in ss;
int err;
    
    s=socket(PF_INET,SOCK_STREAM,IPPROTO_TCP);
    if (s==INVALID_SOCKET)
      throw _nssl_error(errno,strerror(errno),"socket() in _ssl_socket::stay_server_at()");
    
    int one = 1;
    if((setsockopt(s,SOL_SOCKET,SO_REUSEADDR,(char *)&one,sizeof(one))) == -1)
      throw _nssl_error(errno,strerror(errno),"setsockopt() in _ssl_socket::stay_server_at()");
    
    ss.sin_family=AF_INET;
    ss.sin_port=htons(atoi(adress.get(1).c_str()));
    ss.sin_addr.s_addr=inet_addr(adress.get(0).c_str());

    err=::bind(s,(sockaddr *)&ss,sizeof(ss));
    if (err==SOCKET_ERROR)
      throw _nssl_error(errno,strerror(errno),"bind() in _ssl_socket::stay_server_at()");
    
    err=::listen(s,5);
    if (err==SOCKET_ERROR)
      throw _nssl_error(errno,strerror(errno),"listen() in _ssl_socket::stay_server_at()");
}

// ---------------------------------------------------
_net_line *_ssl_socket::accept()
{
sockaddr_in addr;
#ifdef WINDOWS
    int addr_len=sizeof(addr);
#else    
    socklen_t addr_len=sizeof(addr);
#endif
SOCKET tmps;
    
    tmps=::accept(s,(sockaddr *)&addr,&addr_len);
    if (tmps==INVALID_SOCKET)
      throw _nssl_error(errno,strerror(errno),"accept() in _ssl_socket::accept()");
    
    SSL *tmpssl=ssl_set->new_ssl();
    SSL_set_fd(tmpssl,tmps);
    
    int err=SSL_accept(tmpssl);
    if (err<=0)
    {   
        //cout << err << " - " << ERR_error_string(ERR_get_error(),NULL) << endl;
        int e=ERR_get_error();
        throw(_nssl_error(e,ERR_error_string(e,NULL),"SSL_accept() in _ssl_socket::accept()"));
    }
    
    return new _ssl_socket(tmps,tmpssl);
}

// --------------------------------------------------------
void _ssl_socket::is_ready(bool *read,bool *write,bool *error,timeval &tv)
{
    fd_set rset;
    fd_set wset;
    fd_set eset;
    
    FD_ZERO(&rset);
    FD_ZERO(&wset);
    FD_ZERO(&eset);
    
    if (read) FD_SET(s,&rset);
    if (write) FD_SET(s,&wset);
    if (error) FD_SET(s,&eset);
    
    int err=::select(s+1,&rset,&wset,&eset,&tv);
    if (err==SOCKET_ERROR)
      throw(_nssl_error(errno,strerror(errno),"select() in _ssl_socket::is_ready()"));
    
    if (read) if (FD_ISSET(s,&rset)) *read=true; else *read=false;
    if (write) if (FD_ISSET(s,&wset)) *write=true; else *write=false;
    if (error) if (FD_ISSET(s,&eset)) *error=true; else *error=false;
}

// make socket function
_net_line *create_ssl_line(_ssl_set *ss) { return new _ssl_socket(ss); }

