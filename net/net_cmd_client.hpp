/***************************************************************************
 *   What is it: This file is part of communication lib.
 *   Content: help class for cmd (as telnet or pop3) protocol
 *   Version: 0.1b
 *   Last-Mod: 24.09.2006
 *                                                                         
 *   Copyright (C) 2006 by Proshin Alexey                                  
 *   paa713@mail.ru                                                        
 *                                                                         
 *   The contents of this file are subject to free software; 
     you can redistribute it and/or modify  
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or     
 *   (at your option) any later version.                                   
 *                                                                         
 *   The contents of this file is distributed in the hope that it will be useful,       
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
 *   GNU General Public License for more details.                          
 *                                                                         
 *   You should have received a copy of the GNU General Public License     
 *   along with this program; if not, write to the                         
 *   Free Software Foundation, Inc.,                                       
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
 ***************************************************************************/
#ifndef _NET_CMD_CLIENT_H__
#define _NET_CMD_CLIENT_H__

#include <string>
#include <net_lib.h>

// --------------------------------------------------------
// --------------------------------------------------------
class _net_cmd_client
{
	private:
		_net_cmd_client(const _net_cmd_client &c) {};
		_net_cmd_client &operator=(const _net_cmd_client &c) { return *this; };
		
	protected:
		virtual ~_net_cmd_client() {};
		
    public:
      _net_cmd_client() {};
      
      virtual void connect(const _net_adress &adress) = 0;
	  virtual void disconnect() = 0;
	 
      virtual bool is_connected() const = 0;
      virtual bool is_broken() const = 0;
      
      virtual bool cmd(const std::string &command,std::string &rez) = 0;
	  virtual void cmd_timeout_ms(int t) = 0;
	  virtual void cmd_end(const std::string &e) = 0;
	  
	  virtual void release() = 0;
};

// --------------------------------------------------------
_net_cmd_client *create_net_cmd_client();
_net_cmd_client *create_simple_net_cmd_client();

#endif
