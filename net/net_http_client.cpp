/***************************************************************************
 *   What is it: This file is part of communication lib.
 *   Content: releases classes for network http client
 *   Version: 0.1b
 *   Last-Mod: 22.10.2007
 *
 *   Copyright (C) 2006 by Proshin Alexey
 *   paa713@mail.ru
 *
 *   The contents of this file are subject to free software;
     you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   The contents of this file is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***************************************************************************/

#include "net_http_client.h"
#include "net_line_tcp.h"
#include <map>
#include <text_help.hpp>

#ifdef WINDOWS
#else
        #include <sys/socket.h>
        #include <netinet/in.h>
        #include <arpa/inet.h>
        #include <netdb.h>
        #include <string.h>
#endif

#include <iostream>

class _http_cc_error:public _nerror
{
#ifdef WINDOWS
  private:
        std::string operation;
        long error_number;

  public:
        _http_cc_error(const std::string &_oper):error_number(WSAGetLastError()),operation(_oper) {};
        virtual std::string err_st() const { char tmp[256]; sprintf(tmp,"error #%i",error_number);
                                             return std::string(tmp); };
        virtual std::string err_st_full() const { char tmp[256]; sprintf(tmp,"error #%i",error_number);
                                                  return std::string(tmp)+
                                                  std::string(" on command ")+operation; };
        virtual long err_no() const { return error_number; };
#else
  private:
        std::string operation;
        long error_number;

  public:
        _http_cc_error(const std::string &_oper):error_number(errno),operation(_oper) {};
        virtual std::string err_st() const { return std::string(strerror(error_number)); };
        virtual std::string err_st_full() const { return std::string(strerror(error_number))+
                                            std::string(" on command ")+operation; };
        virtual long err_no() const { return error_number; };
#endif
};

// ------------------------------------------------------
class _http_cc                       // client connection
{
    private:

      _net_line *line;

      bool is_connected;

      std::string cm_proxy_name;
      int cm_proxy_port;

      std::string last_server;
      int last_port;

      void connect(const std::string &server_name,int server_port);
      void connect_direct(const std::string &server_name,int server_port);
      void connect_proxy();

      void disconnect();

      void read_answer(_http_answer &answer);
      void recv_by_length(_http_answer &answer,int len);
      void recv_chunked(_http_answer &answer);

      bool disconnect_if_need(const _http_answer &answer);

    public:

      _http_cc();
      virtual ~_http_cc();

      void use_proxy(const std::string &proxy_name,int proxy_port) { cm_proxy_name=proxy_name; cm_proxy_port=proxy_port; };

      void get(const std::string &server_name,int server_port,const std::string &path,const _http_options &opts,_http_answer &answer);
      void post(const std::string &server_name,int server_port,const std::string &path,const _http_options &opts,const std::string &data,_http_answer &answer);
      void put(const std::string &server_name,int server_port,const std::string &path,const _http_options &opts,const std::string &data,_http_answer &answer);
};

// --------------------------------------------------------
_http_cc::_http_cc():last_server(""),last_port(0),is_connected(false),line(NULL)
{
}

// --------------------------------------------------------
_http_cc::~_http_cc()
{
    disconnect();
}

// --------------------------------------------------------
void _http_cc::connect(const std::string &server_name,int server_port)
{
    if (cm_proxy_name.empty()) connect_direct(server_name,server_port); else connect_proxy();
}

// --------------------------------------------------------
void _http_cc::connect_direct(const std::string &server_name,int server_port)
{
    if (is_connected)
      if (server_name==last_server && server_port==last_port) return;
    else disconnect();

    line = create_socket_line();


    std::string server_ip;
    int i;
    for(i=0;i<server_name.length();i++) if (server_name[i]>'9' || server_name[i]<'0' || server_name[i]!='.') break;

    if (i==server_name.length()) server_ip=server_name;
    else
    {
        hostent *host = gethostbyname(server_name.c_str());
        if (!host)
        {
            throw(_http_cc_error(string("Can not resolve ")+server_name));
        }
        in_addr addr;
        memcpy(&addr,host->h_addr_list[0],sizeof(in_addr));
        server_ip=inet_ntoa(addr);
    }

    char s_port[16];
    sprintf(s_port,"%i",server_port);
    _tcp_adress a(server_ip,s_port);

    line->connect(a);

    last_server=server_name;
    last_port=server_port;
    is_connected=true;
}

// --------------------------------------------------------
void _http_cc::connect_proxy()
{
    if (is_connected)
    {
        if (last_server!=cm_proxy_name)
        {
            disconnect();
        }
        else
        {
            return;
        }
    }

    line = create_socket_line();

    std::string proxy_ip;
    int i;
    for(i=0;i<cm_proxy_name.length();i++) if (cm_proxy_name[i]>'9' || cm_proxy_name[i]<'0' || cm_proxy_name[i]!='.') break;

    if (i==cm_proxy_name.length()) proxy_ip=cm_proxy_name;
    else
    {
        hostent *host = gethostbyname(cm_proxy_name.c_str());
        if (!host)
        {
            throw(_http_cc_error(string("Can not resolve ")+cm_proxy_name));
        }
        in_addr addr;
        memcpy(&addr,host->h_addr_list[0],sizeof(in_addr));
        proxy_ip=inet_ntoa(addr);
    }

    char s_port[16];
    sprintf(s_port,"%i",cm_proxy_port);
    _tcp_adress a(proxy_ip,s_port);

    line->connect(a);

    last_server=cm_proxy_name;
    last_port=cm_proxy_port;
    is_connected=true;
}

// --------------------------------------------------------
void _http_cc::disconnect()
{
    if (!is_connected) return;

    if (line)
    {
        line->shutdown();
        line->release();
    }
    line=NULL;

    is_connected=false;
}

// --------------------------------------------------------
bool _http_cc::disconnect_if_need(const _http_answer &answer)
{
    if (answer.find_option("Connection")=="close")
    {
        disconnect();
        return true;
    }

    if (!cm_proxy_name.empty() && answer.find_option("Proxy-Connection")=="close")
    {
        disconnect();
        return true;
    }

    return false;
}

// --------------------------------------------------------
void _http_cc::get(const std::string &server_name,int server_port,const std::string &path,const _http_options &opts,_http_answer &answer)
{
    connect(server_name,server_port);

    std::string req="GET ";

    // In RFC 2616 say
    // The absoluteURI form is REQUIRED when the request is being made to a proxy. (Section 5.1.2)
    if (!cm_proxy_name.empty())
    {
        req+="http://";
        req+=server_name;
        req+=":";
        req+=to_string(server_port);
    }

    req+=path;
    req+=" HTTP/1.1\r\n";
    req+="Host: ";
    req+=server_name;
    req+="\r\n";

    _http_options::const_iterator oit=opts.begin();
    while(oit!=opts.end())
    {
        req+=oit->first;
        req+=": ";
        req+=oit->second;
        req+="\r\n";
        oit++;
    }

    req+="\r\n";

    line->send(req);

    read_answer(answer);

    disconnect_if_need(answer);
}

// --------------------------------------------------------
void _http_cc::post(const std::string &server_name,int server_port,const std::string &path,const _http_options &opts,const std::string &data,_http_answer &answer)
{
    connect(server_name,server_port);

    std::string req="POST ";

    // In RFC 2616 say
    // The absoluteURI form is REQUIRED when the request is being made to a proxy. (Section 5.1.2)
    if (!cm_proxy_name.empty())
    {
        req+="http://";
        req+=server_name;
        req+=":";
        req+=to_string(server_port);
    }

    req+=path;
    req+=" HTTP/1.1\r\n";
    req+="Host: ";
    req+=server_name;
    req+="\r\n";

    _http_options::const_iterator oit=opts.begin();
    while(oit!=opts.end())
    {
        req+=oit->first;
        req+=": ";
        req+=oit->second;
        req+="\r\n";
        oit++;
    }
    req+="Content-Length: ";
    req+=to_string(data.length());
    req+="\r\n\r\n";
    req+=data;

    line->send(req);

    read_answer(answer);

    disconnect_if_need(answer);
}

// --------------------------------------------------------
void _http_cc::put(const std::string &server_name,int server_port,const std::string &path,const _http_options &opts,const std::string &data,_http_answer &answer)
{
    connect(server_name,server_port);

    std::string req="PUT ";

    // In RFC 2616 say
    // The absoluteURI form is REQUIRED when the request is being made to a proxy. (Section 5.1.2)
    if (!cm_proxy_name.empty())
    {
        req+="http://";
        req+=server_name;
        req+=":";
        req+=to_string(server_port);
    }

    req+=path;
    req+=" HTTP/1.1\r\n";
    req+="Host: ";
    req+=server_name;
    req+="\r\n";

    _http_options::const_iterator oit=opts.begin();
    while(oit!=opts.end())
    {
        req+=oit->first;
        req+=": ";
        req+=oit->second;
        req+="\r\n";
        oit++;
    }
    req+="Content-Length: ";
    req+=to_string(data.length());
    req+="\r\n\r\n";
    req+=data;

    line->send(req);

    read_answer(answer);

    disconnect_if_need(answer);
}

// --------------------------------------------------------
void _http_cc::read_answer(_http_answer &answer)
{
    answer.data="";
    answer.header="";
    answer.opts_map.clear();
    answer.res_code=0;
    answer.res_phrase="";

    std::string tmp;
    int pos;

    while(1)
    {
        line->recv(tmp);
        cout << tmp << endl;
        answer.header+=tmp;
        pos=answer.header.find("\r\n\r\n");
        if (pos!=std::string::npos) break;
    }

    //cout << "-------------------------------------" << endl;
    //cout << answer.header;
    //cout << "-------------------------------------" << endl;

    answer.data.assign(answer.header,pos+4,answer.header.length()-pos-4);
    answer.header.replace(pos,answer.header.length()-pos,"");

    answer.parse_header();

    // In RFC 2068 define order how detect answer length:
    // 1. Have null string after head - no answer body
    // 2. Have Transfer-Encoding tag - need chunked read
    // 3. Have Content-Length tag - it`s set answer body length
    // 4. multipart/byteranges - no support yet
    // 5. By connection closing

    if (answer.header.find("\r\n\r\n\r\n")==pos) return;
    else if (answer.find_option("Transfer-Encoding")=="chunked") recv_chunked(answer);
    else if (!answer.find_option("Content-Length").empty()) recv_by_length(answer,atoi(answer.find_option("Content-Length").c_str()));
    else throw(_http_answer::_nhttp_answer_error("_http_cc::read_answer - Not define length of answer & Transfer-Encoding is not chunked",0));

    //cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
    //cout << answer.data;

}

// --------------------------------------------------------
void _http_cc::recv_by_length(_http_answer &answer,int len)
{
    int rlen=answer.data.length();
    std::string tmp;
    while(rlen<len)
    {
        line->recv(tmp);
        rlen+=tmp.length();
        answer.data+=tmp;
    }
    if (answer.data.length()>len) answer.data.erase(len-1,answer.data.length()-len);
}

// --------------------------------------------------------
void _http_cc::recv_chunked(_http_answer &answer)
{
    std::string clear_data;

    while(1)
    {
        int shift_size;
        while(1)
        {
            std::string csize = _text_help::get_field_from_st(answer.data,'\r',0);
            //cout << "~" << csize << "~" << endl;
            int size;
            sscanf(csize.c_str(),"%x",&size);
            if (size==0)
            {
                shift_size=0;
                break;
            }

            int pos=answer.data.find("\r\n")+2;
            int len=size;
            if (pos+len > answer.data.length()) len=answer.data.length()-pos;
            clear_data+=std::string(answer.data,pos,len);
            if (len<size)
            {
                answer.data="";
                shift_size=size-len;
                break;
            }

            answer.data.replace(0,len+pos+2,"");
        }

        if (shift_size==0) break;

        std::string tmp;

        //???????
        while(answer.data.length()<shift_size+5)
        {
            line->recv(tmp);
            //cout << "------------------------------------" << endl << tmp;
            answer.data+=tmp;
        }

        /*while(answer.data.rfind("\r\n"))
        {
            line->recv(tmp);
            cout << "++++++++++++++++++++++++++++++++++++" << endl << tmp;
            answer.data+=tmp;
        }*/

        clear_data+=std::string(answer.data,0,shift_size);
        answer.data.replace(0,shift_size+2,"");

    }

    answer.data=clear_data;

}


// --------------------------------------------------------

// --------------------------------------------------------
// --------------------------------------------------------
class _http_client_imp:public _http_client
{
    private:

      _http_cc cc;

      void split_url(const std::string &url,std::string &proto,std::string &server,std::string &path,int *port);

    public:

      _http_client_imp();
      virtual ~_http_client_imp();

      virtual void release() { delete this; };

      virtual void use_proxy(const std::string &proxy_name,int proxy_port) { cc.use_proxy(proxy_name,proxy_port); };

      virtual void get(const std::string &url,const _http_options &opts,_http_answer &answer);
      virtual void post(const std::string &url,const _http_options &opts,const std::string &data,_http_answer &rez);
      virtual void put(const std::string &url,const _http_options &opts,const std::string &data,_http_answer &rez);
};

// ------------------------------------------------------
_http_client_imp::_http_client_imp()
{
}

// ------------------------------------------------------
_http_client_imp::~_http_client_imp()
{
}

// ------------------------------------------------------
void _http_client_imp::split_url(const std::string &url,std::string &proto,std::string &server,std::string &path,int *port)
{
    proto="http";
    path.clear();
    server.clear();
    *port=80;

    string u=url;

    // split proto://...
    string::size_type pos=u.find("://");
    if (pos!=string::npos)
    {
        proto.assign(u,0,pos);
        u.erase(0,pos+3);
    }

    // split server/resourse
    pos=u.find("/");
    if (pos!=string::npos)
    {
        server.assign(u,0,pos);
        path.assign(u,pos,u.length()-pos);
    }
    else
    {
        server=u;
        path="/";
    }

    u=_text_help::get_field_from_st(server,':',1);
    if (!u.empty())
    {
        *port=atoi(u.c_str());
        server=_text_help::get_field_from_st(server,':',0);
    }
}

// ------------------------------------------------------
void _http_client_imp::get(const std::string &url,const _http_options &opts,_http_answer &answer)
{
    std::string proto;
    std::string server_name;
    std::string path;
    int port;

    split_url(url,proto,server_name,path,&port);

    cc.get(server_name,port,path,opts,answer);
}

// ------------------------------------------------------
void _http_client_imp::post(const std::string &url,const _http_options &opts,const std::string &data,_http_answer &answer)
{
    std::string proto;
    std::string server_name;
    std::string path;
    int port;

    split_url(url,proto,server_name,path,&port);

    cc.post(server_name,port,path,opts,data,answer);
}

// ------------------------------------------------------
void _http_client_imp::put(const std::string &url,const _http_options &opts,const std::string &data,_http_answer &answer)
{
    std::string proto;
    std::string server_name;
    std::string path;
    int port;

    split_url(url,proto,server_name,path,&port);

    cc.put(server_name,port,path,opts,data,answer);
}

// ------------------------------------------------------
// ------------------------------------------------------
_http_client *create_http_client()
{
    return new _http_client_imp;
}
