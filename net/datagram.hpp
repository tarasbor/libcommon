#ifndef __NET_DATAGRAM__
#define __NET_DATAGRAM__

#include <time.hpp>
#include <string>

// bin format of datagram
// time stamp
// type (DATAGRAM_TYPE_TYPE)
// raw data size (int)

#if !defined(DATAGRAM_TYPE_TYPE)
#define DATAGRAM_TYPE_TYPE      unsigned char
#endif

class _datagram
{
    private:

      _time dstamp;                         // datagram stamp
      DATAGRAM_TYPE_TYPE dtype;             // datagram type
    
      unsigned char *d;                     // datagram stamp, type & raw data buffer
      unsigned int d_size;                  // all size;
      
    public:

      _datagram(const unsigned char *rdata,int rsize,DATAGRAM_TYPE_TYPE dt);
      _datagram(const _datagram &sd);
      _datagram(const char *data);
      ~_datagram();

      DATAGRAM_TYPE_TYPE type() const { return dtype; };
      const _time &stamp() const { return dstamp; };
      
      const unsigned char *raw_data() const { return d+header_len(); };
      unsigned int raw_size() const { return d_size-header_len(); };

      unsigned int size() const { return d_size; };
      const unsigned char *data() const { return d; };

      static unsigned int header_len() { return sizeof(_time::time_val_i)+sizeof(DATAGRAM_TYPE_TYPE)+sizeof(int); };  
};

#endif

