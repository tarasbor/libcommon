#include "net_smtp_client.h"
#include "net_cmd_client.hpp"
#include "base64.h"

// net cl error
// -------------------------------------------------------
class _ncl_error:public _nerror
{
  private:
        std::string operation;
        long error_number;

  public:
        _ncl_error(const std::string &_oper,long error):error_number(error),operation(_oper) {};
        virtual std::string err_st() const { char tmp[256]; sprintf(tmp,"error #%i",error_number);
                                             return std::string(tmp); };
        virtual std::string err_st_full() const { char tmp[256]; sprintf(tmp,"error #%i",error_number);
                                                  return std::string(tmp)+
                                                  std::string(" on ")+operation; };
        virtual long err_no() const { return error_number; };
};

// ---------------------------------------------------------------------------------------------
class _smtp_cl:public _smtp_client
{
	private:

		_net_cmd_client *cl;

		virtual void smtp_cmd(const string &cmd,const string &rcode,string &rez);

	public:
		_smtp_cl():cl(create_simple_net_cmd_client()) { cl->cmd_end("\r\n"); };
		virtual ~_smtp_cl();
		virtual void release() { delete this; };

		virtual void connect(const _net_adress &adress);
		virtual void disconnect() { if (cl) { cl->disconnect(); } };

		virtual void helo(const string &hello) { string rez; smtp_cmd(string("HELO ")+hello,"250",rez); };
		virtual void ehlo(const string &hello) { string rez; smtp_cmd(string("EHLO ")+hello,"250",rez); };

		virtual void auth_plain(const string &mail,const string &uname,const string &upass);
		virtual void auth_login(const string &uname,const string &upass);

		virtual void send(const string &from,const string &to,const string &data);
};

// ---------------------------------------------------
// ---------------------------------------------------
_smtp_cl::~_smtp_cl()
{
	if (cl)
	{
		cl->release();
		cl=NULL;
	}
}

// ---------------------------------------------------
// ---------------------------------------------------
void _smtp_cl::smtp_cmd(const string &cmd,const string &rcode,string &rez)
{
	cl->cmd(cmd,rez);

	int pos=rez.find(rcode);
	if (pos!=0) throw(_ncl_error(string("smtp_client::smtp_cmd - ")+cmd+" - answer was - "+rez,0));
}

// ---------------------------------------------------
// ---------------------------------------------------
void _smtp_cl::connect(const _net_adress &adress)
{
    if (cl->is_connected()) throw(_ncl_error("smtp_client::connect - already connected...",0));

	cl->connect(adress);

	string rez;
	smtp_cmd("","220",rez);
}

// ---------------------------------------------------
void _smtp_cl::auth_plain(const string &mail,const string &uname,const string &upass)
{
        // make data as user\0mail\0pass exmp test\0test@wis.ex.com\0test_pass
		string ls=uname;
		ls+='\0';
		ls+=mail;
		ls+='\0';
		ls+=upass;

        char *tmp = new char [ls.length()*2+1];
        to_base64(ls.data(),tmp,ls.length());

		try
		{
			smtp_cmd(string("AUTH PLAIN ")+tmp,"235",ls);
		}
		catch(...)
		{
			delete tmp;
			throw;
		}

		delete tmp;
}

// ---------------------------------------------------
void _smtp_cl::auth_login(const string &uname,const string &upass)
{
	string rez;
	smtp_cmd("AUTH LOGIN","334",rez);

    char tmp[256];

    // send user
    to_base64(uname.c_str(),tmp,uname.length());
    smtp_cmd(tmp,"334",rez);

	// send pass
    to_base64(upass.c_str(),tmp,upass.length());
    smtp_cmd(tmp,"235",rez);
}

// ---------------------------------------------------
void _smtp_cl::send(const string &from,const string &to,const string &data)
{
	string rez;

	string cmd;

	cmd=string("MAIL FROM: <")+from+">";
	smtp_cmd(cmd,"250",rez);

    cmd=string("RCPT TO: <")+to+">";
	smtp_cmd(cmd,"250",rez);

	smtp_cmd("DATA","354",rez);

	smtp_cmd(data,"250",rez);

	try
	{
	    // sometimes it back 220
		smtp_cmd("RSET","250",rez);
    }
	catch(...)
	{
		if (rez.find("220")!=0) throw;
	}
}

// ----------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------
_smtp_client *create_smtp_client()
{
	return new _smtp_cl;
}
