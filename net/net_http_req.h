/***************************************************************************
 *   What is it: This file is part of communication lib.
 *   Content: types for http request & answers
 *   Version: 0.1b
 *   Last-Mod: 22.10.2007
 *
 *   Copyright (C) 2006 by Proshin Alexey
 *   paa713@mail.ru
 *
 *   The contents of this file are subject to free software;
     you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   The contents of this file is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***************************************************************************/

#ifndef ____NET_HTTP_REQ_REZ_CLASSES__
#define ____NET_HTTP_REQ_REZ_CLASSES__

#include <net_lib_error.h>
#include <string>
#include <map>
#include <list>
#include <cstdio>

using namespace std;

struct _http_answer
{

    typedef std::pair<std::string,std::string> _option;
    typedef std::multimap<std::string,std::string> _options_map;

    struct _cookie
    {
        std::string name;
        std::string value;
        std::string domain;
        std::string path;
        std::string exp_time;
        std::string ex_opt;
    };
    typedef std::list<_cookie> _cookies_list;

    class _nhttp_answer_error:public _nerror
    {
        std::string error;
        long error_number;

        public:

        _nhttp_answer_error(const std::string &e_st,int number):error(e_st),error_number(number) {};
        virtual std::string err_st() const { return error; };
        virtual std::string err_st_full() const { char *tmp = new char [error.length()+48]; sprintf(tmp,"%s. Code = %i",error.c_str(),error_number); std::string s = tmp; delete tmp; return s; }
        virtual long err_no() const { return error_number; };
    };

    int res_code;
    std::string res_phrase;

    std::string header;
    _options_map opts_map;

    std::string data;

    void parse_header();

    void parse_header_first_line(const std::string &line);
    void parse_header_line(const std::string &line);

    static std::string cm_null_option;
    const std::string &find_option(const std::string &option) const;

    _cookies_list cookies() const;
};

#endif
