/***************************************************************************
 *   What is it: This file is part of communication lib.
 *   Content: interface class for help class for http requests and answers
 *   Version: 0.1b
 *   Last-Mod: 24.09.2006
 *
 *   Copyright (C) 2006 by Proshin Alexey
 *   paa713@mail.ru
 *
 *   The contents of this file are subject to free software;
     you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   The contents of this file is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***************************************************************************/
#ifndef __HTTP_REQ_HELP_CLASS__
#define __HTTP_REQ_HELP_CLASS__

#include <string>
#include <map>
#include <list> // for get_req_param()

using namespace std;

class _http_help;
class _http_help_req_params;
class _http_help_cookie;

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
class _http_help_req_params
{
    protected:
        _http_help_req_params() {};
        virtual ~_http_help_req_params() {};

    public:
        typedef map<std::string,std::string> _cont;

        virtual void parse(_http_help *hh,const std::string &req) = 0;

        virtual void set(const std::string &p,const std::string &v) = 0;
        virtual void set(const _cont &c) = 0;
        virtual const std::string &get(const std::string &p) const = 0;

        virtual _cont::iterator begin() = 0;
        virtual _cont::iterator end() = 0;

        virtual _cont::const_iterator begin() const = 0;
        virtual _cont::const_iterator end() const = 0;

        virtual void release() = 0;
};

// -------------------------------------------------------------------------
// help class for process http requests and make http answers
// -------------------------------------------------------------------------
class _http_help
{
    protected:

      _http_help() {};
      virtual ~_http_help() {};

    public:

        virtual void html_charset(const std::string &cp) = 0;
        virtual const std::string &html_charset() const = 0;

        virtual int req_get_muv(const std::string &req,std::string &method,std::string &url,std::string &version)=0;      
        virtual int req_get_agent(const std::string &req,std::string &userAgent)=0;

        virtual _http_help_req_params *req_get_cookies(const std::string &req) = 0;
        virtual std::string req_get_cookie(const std::string &req,const std::string &cname) = 0;

        virtual int http_data(const std::string &data,const std::string &ctype,std::string &rez)=0;
        virtual int http_file(const std::string &root_dir,const std::string &file_name,std::string &rez)=0;
        virtual int http_any_file(const std::string &dir,const std::string &file_name,std::string &rez)=0;      // Any file, except .html,.htm, etc...
        virtual int word_file(const std::string &root_dir,const std::string &file_name,std::string &rez)=0;

        virtual void set_cookie(const _http_help_req_params *c,std::string &rez) = 0;

        virtual int http_by_state_code(int code,const std::string &comments,std::string &rez)=0;

        virtual bool its_file(const std::string &url)=0;

        virtual void virtual_include(const std::string &root_dir,std::string &rez)=0;
        virtual void virtual_for(std::string &rez)=0;

        virtual void update_content_len(std::string &rez)=0;

        virtual std::string q_str(const std::string &in)=0;

        virtual void release()=0;

      // compatibility with old vc projects
      virtual int http_not_found(std::string &rez) = 0;
      virtual std::string req_get_param(const list<std::string> &params,const list<std::string> &values,const std::string &param) = 0;
      virtual std::string param_from_codes(const std::string &s) = 0;
      virtual void req_params_multipart(const std::string &req,list<std::string> &params,list<std::string> &values) = 0;
      virtual void req_params(const std::string &req,list<std::string> &params,list<std::string> &values) = 0;


};

// make function
_http_help *create_http_help();
_http_help_req_params *create_http_help_req_params();

#endif
