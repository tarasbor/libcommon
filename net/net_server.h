/***************************************************************************
 *   What is it: This file is part of communication lib.
 *   Content: interface class for network servers
 *   Version: 0.1b
 *   Last-Mod: 24.09.2006
 *
 *   Copyright (C) 2006 by Proshin Alexey
 *   paa713@mail.ru
 *
 *   The contents of this file are subject to free software;
     you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   The contents of this file is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***************************************************************************/
#ifndef __NET_SERVER_CLASSES__
#define __NET_SERVER_CLASSES__

#include <string>
#include <clthread.hpp>
#include "net_lib.h"
#include "net_line.h"

#define SERVER_LOG_LEVEL1 5
#define SERVER_LOG_LEVEL2 10
#define SERVER_LOG_LEVEL3 20
#define SERVER_LOG_LEVEL4 30

// ----------------------------------------------------------------------
// any network server connection thread (from client to server)
// ----------------------------------------------------------------------
class _net_server_connection//: public _thread
{
  protected:

        _net_server_connection() {};
        virtual ~_net_server_connection() {};

  public:

        virtual void release()=0;
        virtual void set_keep_alive_time(int sec) = 0;
        virtual _locker *locker() = 0;
        virtual _net_line *net_line() = 0;

        virtual int poll() = 0;

        virtual void to_out(const std::string &data) = 0;
};

// ---------------------------------------------------------------------
// any network server processor interface
// ---------------------------------------------------------------------
class _net_server_processor
{
  public:

        virtual ~_net_server_processor() {};
        virtual bool process(std::string &in,std::string &out,_net_server_connection *nc) = 0;

        virtual void reg_connection(_net_server_connection *c) = 0;
        virtual void unreg_connection(_net_server_connection *c) = 0;
};

// ---------------------------------------------------------------------
// any network server
// ---------------------------------------------------------------------
class _net_server
{
  protected:

        _net_server() {};
        virtual ~_net_server() {};

  public:

        virtual void online(const _net_adress &adress) = 0;
        virtual void shutdown() = 0;
        virtual void release() = 0;

        virtual bool in_process(std::string &in,std::string &out,_net_server_connection *nc) = 0;
        virtual void set_processor(_net_server_processor *pr) = 0;
		virtual _net_server_processor *get_processor() = 0;

        virtual void reg_connection(_net_server_connection *c) = 0;
        virtual void unreg_connection(_net_server_connection *c) = 0;

        virtual void keep_alive_s(int sec) = 0;
        virtual int keep_alive_s() const = 0;

        virtual void reinit_time_s(int sec) = 0;
        virtual int reinit_time_s() const = 0;
};

// ---------------------------------------------------------------------
// any network server thread (listen & accept income connections)
// ---------------------------------------------------------------------
class _net_server_process: public _thread
{
  protected:

        _net_server_process() {};
        virtual ~_net_server_process() {};

  public:

        virtual bool is_online() const =0;
        virtual void release()=0;

        virtual void keep_alive_s(int sec) = 0;
        virtual int keep_alive_s() const = 0;

        virtual void reinit_time_s(int sec) = 0;
        virtual int reinit_time_s() const = 0;
};

#endif
