/***************************************************************************
 *   What is it: This file is part of communication lib.
 *   Content: releases classes for http requests & answers
 *   Version: 0.1b
 *   Last-Mod: 22.10.2007
 *
 *   Copyright (C) 2006 by Proshin Alexey
 *   paa713@mail.ru
 *
 *   The contents of this file are subject to free software;
     you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   The contents of this file is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***************************************************************************/

#include <net_http_req.h>
#include <text_help.hpp>
#include <cstdlib>

// --------------------------------------------------------
std::string _http_answer::cm_null_option="";

// --------------------------------------------------------
void _http_answer::parse_header_first_line(const std::string &line)
{
    std::string http_version = _text_help::get_field_from_st(line,' ',0);
    if (http_version!="HTTP/1.1" && http_version!="HTTP/1.0") throw(_nhttp_answer_error("_http_answer::parse_header_first_line - No HTTP version or wrong version in header",0));

    res_code = atoi(_text_help::get_field_from_st(line,' ',1).c_str());
    res_phrase = _text_help::get_field_from_st(line,' ',2).c_str();
}

// --------------------------------------------------------
void _http_answer::parse_header_line(const std::string &line)
{
    std::string::size_type pos=line.find(':');
    if (pos==std::string::npos) return;

    int len=line.length();

    std::string option=string(line,0,pos);
    std::string value="";
    if (pos+2<len)
    {
        value.assign(line,pos+2,len-2);
    }

    opts_map.insert(_option(option,value));

    /*
    if      (option=="Connection") opt_map[option]=std::string(value,1,value.length()-1);
    else if (option=="Transfer-Encoding") opt_map[option]=std::string(value,1,value.length()-1);
    else if (option=="Content-Length") opt_map[option]=std::string(value,1,value.length()-1);
    else if (option=="Content-Type") opt_map[option]=std::string(value,1,value.length()-1);
    else if (option=="Content-Encoding") opt_map[option]=std::string(value,1,value.length()-1);
    */
}

// --------------------------------------------------------
void _http_answer::parse_header()
{
    opts_map.clear();

    std::string line;
    int i=0;
    while(1)
    {
        line=_text_help::get_field_from_st(header,'\r',i);
        if (line=="") break;

        if (i==0) parse_header_first_line(line); else parse_header_line(std::string(line,1,line.length()-1));
        i++;
    }

    if (i==0) throw(_nhttp_answer_error("_http_answer::parse_header - No CRLF in header",0));
}

// --------------------------------------------------------
const std::string &_http_answer::find_option(const std::string &option) const
{
    _http_answer::_options_map::const_iterator oit=opts_map.find(option);
    if (oit==opts_map.end()) return cm_null_option;
    return oit->second;
}

// --------------------------------------------------------
_http_answer::_cookies_list _http_answer::cookies() const
{
    _http_answer::_cookies_list cl;

    _http_answer::_options_map::const_iterator oit=opts_map.begin();
    while(oit!=opts_map.end())
    {
        if (oit->first=="Set-Cookie")
        {
            _http_answer::_cookie c;

            int i=0;
            while(true)
            {
                std::string pv=_text_help::get_field_from_st(oit->second,';',i);
                if (pv.empty()) break;

                if (i==0)
                {
                    c.name=_text_help::get_field_from_st(pv,'=',0);
                    c.value=_text_help::get_field_from_st(pv,'=',1);
                }
                else
                {
                    std::string p=_text_help::get_field_from_st(pv,'=',0);
                    std::string v=_text_help::get_field_from_st(pv,'=',1);

                    if (!v.empty())
                    {
                        if      (p==" domain") c.domain=v;
                        else if (p==" path") c.path=v;
                        else if (p==" expires") c.exp_time=v;
                        else c.ex_opt=v;
                    }
                    else c.ex_opt=p;
                }

                i++;
            }

            cl.push_back(c);

        }
        oit++;
    }

    return cl;
}

