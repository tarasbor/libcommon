/***************************************************************************
 *   What is it: This file is part of communication lib.
 *   Content: interface class for http http client
 *   Version: 0.1b
 *   Last-Mod: 22.10.2007
 *
 *   Copyright (C) 2006 by Proshin Alexey
 *   paa713@mail.ru
 *
 *   The contents of this file are subject to free software;
     you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   The contents of this file is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***************************************************************************/
#ifndef __NET_HTTP_CLIENT_CLASSES__
#define __NET_HTTP_CLIENT_CLASSES__

#include <net_http_req.h>
#include <string>
#include <map>

typedef std::map<std::string,std::string> _http_options;

// ------------------------------------------------------
class _http_client
{
    public:

      _http_client() {};
      virtual ~_http_client() {};

      virtual void release() = 0;

      virtual void use_proxy(const std::string &proxy_name,int proxy_port) = 0;
      virtual void get(const std::string &url,const _http_options &opts,_http_answer &rez) = 0;
      virtual void post(const std::string &url,const _http_options &opts,const std::string &data,_http_answer &rez) = 0;
      virtual void put(const std::string &url,const _http_options &opts,const std::string &data,_http_answer &rez) = 0;

};

// ------------------------------------------------------
_http_client *create_http_client();

#endif
