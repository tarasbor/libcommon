// ServerPOP3.h: interface for the ServerPOP3 class.
//
//////////////////////////////////////////////////////////////////////

#ifndef __POP3_CLIENT__
#define __POP3_CLIENT__

#include "_socket.h"

typedef void (*PROGRESSPROC)();

class ServerPOP3  
{
private:

	_socket s;
	std::string error_st;
	std::string last_answer;

	int command(char const *cmd,char const *anser);

	PROGRESSPROC p_proc;

public:
	ServerPOP3();
	virtual ~ServerPOP3();

	int Connect(char const *ip,int port);
	int Auth(char const *user,char const *pass);
	int MsgCountSize(int *count,int *size);
	int Recv(std::string &msg_text,int number);
	int Del(int number);
	int Disconnect();
	std::string Error() { return error_st; };
	void SetCallback(PROGRESSPROC p) { p_proc=p; };

};

#endif
