/***************************************************************************
 *   This file contain release of https class 
 *                                                                         
 *   Copyright (C) 2006 by Proshin Alexey                                  
 *   paa713@mail.ru                                                        
 *                                                                         
 *   The contents of this file are subject to free software; 
     you can redistribute it and/or modify  
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or     
 *   (at your option) any later version.                                   
 *                                                                         
 *   The contents of this file is distributed in the hope that it will be useful,       
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
 *   GNU General Public License for more details.                          
 *                                                                         
 *   You should have received a copy of the GNU General Public License     
 *   along with this program; if not, write to the                         
 *   Free Software Foundation, Inc.,                                       
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
 ***************************************************************************/

#include <net_https_server.h>
#include <net_server_ssl.h>

// ---------------------------------------------------
// https server
// ---------------------------------------------------
class _https_server:public _net_server
{
  friend _net_server *create_https_server(_ssl_set *ss);
  
  private:
  
        _net_server_processor *processor;
        _net_server_process *listen;
        _ssl_set *ssl_set;
        
        _https_server(_ssl_set *ss):listen(NULL),processor(NULL),ssl_set(ss) {};
        virtual ~_https_server() { shutdown(); };
        
  public:
  
        virtual void online(const _net_adress &adress);
        virtual void shutdown();
        virtual void release() { delete this; };
        
        virtual bool in_process(std::string &in,std::string &out);
        virtual void set_processor(_net_server_processor *pr) { processor=pr; };
};

// ---------------------------------------------------
void _https_server::shutdown()
{
  if (!listen) return;
  listen->Kill();
  listen->release();
  listen=NULL;
}

// ---------------------------------------------------
void _https_server::online(const _net_adress &adress)
{
  if (listen) return;
  listen=create_ssl_server_process(adress,this,ssl_set);
  listen->Start();
  while(!listen->is_online()) { Sleep(100); }; 
}

// ---------------------------------------------------
bool _https_server::in_process(std::string &in,std::string &out)
{       
    if (!processor) return false;
    
    int i;
    int head_len=0;
    int body_len=0;       
        
    int pos=in.find("\r\n\r");
    if (pos!=std::string::npos) head_len=pos+4; else return false;    
    
    i=in.find("Content-length");
    if (i==std::string::npos) i=in.find("Content-Length");
    if (i!=std::string::npos)
    {
      int j=in.find('\r',i);
      i=in.find(':',i+1);
      body_len=atoi(in.substr(i+1,j-i-1).c_str());    
      if (in.length()<head_len+body_len) return false;
    }
        
    bool res=processor->process(in,out);
    if (res) in="";
    return res;
}

// ---------------------------------------------------
_net_server *create_https_server(_ssl_set *ss) { return new _https_server(ss); }
