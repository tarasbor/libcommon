// ServerSMTP.h: interface for the ServerSMTP class.
//

#include "_socket.h"
#include "base64.h"

typedef void (*PROGRESSPROC)();

class ServerSMTP  
{
private:

	_socket s;
	std::string error_st;
	std::string last_answer;

	int command(char const *cmd,char const *anser);

	PROGRESSPROC p_proc;

public:

	ServerSMTP();
	virtual ~ServerSMTP();

	int Connect(char const *ip,int port);
	int Send(char const *from,char const *to,char const *text);
	int Disconnect();
	std::string Error() { return error_st; };
	void SetCallback(PROGRESSPROC p) { p_proc=p; };
	int AuthPlain(char const *mail,char const *user_name,char const *pass);
	int AuthLogin(char const *user_name,char const *pass);

};

