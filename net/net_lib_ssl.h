/***************************************************************************
 *   What is it: This file is part of communication lib.
 *   Content: common (global) classes for ssl
 *   Version: 0.1b
 *   Last-Mod: 24.09.2006
 *                                                                         
 *   Copyright (C) 2006 by Proshin Alexey                                  
 *   paa713@mail.ru                                                        
 *                                                                         
 *   The contents of this file are subject to free software; 
     you can redistribute it and/or modify  
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or     
 *   (at your option) any later version.                                   
 *                                                                         
 *   The contents of this file is distributed in the hope that it will be useful,       
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
 *   GNU General Public License for more details.                          
 *                                                                         
 *   You should have received a copy of the GNU General Public License     
 *   along with this program; if not, write to the                         
 *   Free Software Foundation, Inc.,                                       
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
 ***************************************************************************/
#ifndef __COMMON_COMMUNICATION_CLASSES_SSL__
#define __COMMON_COMMUNICATION_CLASSES_SSL__

#include <string>
#include <net_lib.h>

#include <openssl/ssl.h>
#include <openssl/err.h>



// ---------------------------------------------------------------------
// ssl setting class (its wrap around CTX, SSL and etc struct in ssl lib)
// ---------------------------------------------------------------------
class _ssl_set
{
  public:
    enum version {ssl2,ssl3,tls1,all};
    enum type {client,server,both};
    enum file_format { pem=SSL_FILETYPE_PEM, ansi1=SSL_FILETYPE_ASN1};
    enum verify_type { none=SSL_VERIFY_NONE, peer=SSL_VERIFY_PEER,
                       fail_if_no_peer=SSL_VERIFY_FAIL_IF_NO_PEER_CERT,
                       client_once=SSL_VERIFY_CLIENT_ONCE};
    
  protected:
    _ssl_set(version v,type t) {};
    virtual ~_ssl_set() {};
    
  public:
  
    virtual void pub_certificate_from_file(const char *fname,file_format format)=0;
    virtual void RSA_private_key_from_file(const char *fname,file_format format)=0;
    
    virtual void set_verify(verify_type)=0;
    
    virtual SSL *new_ssl()=0;
    
    virtual void release()=0;
};

_ssl_set *create_ssl_set(_ssl_set::version v,_ssl_set::type t);

#endif
