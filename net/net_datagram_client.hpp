/***************************************************************************
 *   What is it: This file is part of communication lib.
 *   Content: help class for datagram recive client
 *   Version: 0.1b
 *   Last-Mod: 24.09.2006
 *
 *   Copyright (C) 2006 by Proshin Alexey
 *   paa713@mail.ru
 *
 *   The contents of this file are subject to free software;
     you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   The contents of this file is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***************************************************************************/
#ifndef _NET_DATAGRAM_CLIENT_H__
#define _NET_DATAGRAM_CLIENT_H__

#include <string>
#include <datagram.hpp>
#include <net_lib.h>

// --------------------------------------------------------
// --------------------------------------------------------
class _net_datagram_client
{
	protected:
	  _net_datagram_client() {};
	  virtual ~_net_datagram_client() {};

    public:

	  virtual void connect(const _net_adress &adress) = 0;
	  virtual void disconnect() = 0;

      virtual bool is_connected() const = 0;
      virtual bool is_broken() const = 0;
      virtual int connection_id() const = 0;

      //virtual void kill() = 0;

      virtual void recv() = 0;
      virtual _datagram *datagram() = 0;

      virtual void send() = 0;
      virtual void datagram(_datagram *) = 0;
      virtual void datagram(const unsigned char *str, unsigned int len) = 0;  // send plain text
      virtual bool is_sended() const = 0;
	  virtual int out_buffer_size() const = 0;

	  virtual void release() = 0;

};

// --------------------------------------------------------
_net_datagram_client *create_net_datagram_client();

#endif
