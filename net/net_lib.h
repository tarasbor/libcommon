/***************************************************************************
 *   What is it: This file is part of communication lib.
 *   Content: common (global) classes
 *   Version: 0.1b
 *   Last-Mod: 24.09.2006
 *                                                                         
 *   Copyright (C) 2006 by Proshin Alexey                                  
 *   paa713@mail.ru                                                        
 *                                                                         
 *   The contents of this file are subject to free software; 
     you can redistribute it and/or modify  
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or     
 *   (at your option) any later version.                                   
 *                                                                         
 *   The contents of this file is distributed in the hope that it will be useful,       
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
 *   GNU General Public License for more details.                          
 *                                                                         
 *   You should have received a copy of the GNU General Public License     
 *   along with this program; if not, write to the                         
 *   Free Software Foundation, Inc.,                                       
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
 ***************************************************************************/
#ifndef __COMMON_COMMUNICATION_CLASSES__
#define __COMMON_COMMUNICATION_CLASSES__

#include <string>
#include "net_lib_error.h"

// ---------------------------------------------------------------------
// adress class interface
// ---------------------------------------------------------------------
class _net_adress
{
  public:
        _net_adress() {};
        virtual ~_net_adress() {};
        
        virtual std::string get(int index) const=0;
};

// ---------------------------------------------------------------------
// tcp adress (ip & port) class
// ---------------------------------------------------------------------
class _tcp_adress:public _net_adress
{
  private:
        std::string ip;
        std::string port;
  public:
        _tcp_adress(const std::string &_ip,const std::string &_port):ip(_ip),port(_port) {};
		_tcp_adress(const std::string &host_and_port);
        virtual ~_tcp_adress() {};
        
        virtual std::string get(int index) const { if (index==0) return ip; else return port; };
};

// ---------------------------------------------------------------------
// RTP address
// ---------------------------------------------------------------------
class _rlp_adress:public _net_adress
{
  private:
        std::string ip;
        std::string port;
  public:
        _rlp_adress(const std::string &_ip,const std::string &_port):ip(_ip),port(_port) {};
        virtual ~_rlp_adress() {};
        
        virtual std::string get(int index) const { if (index==0) return ip; else return port; };
};

#endif
