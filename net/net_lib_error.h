/***************************************************************************
 *   What is it: This file is part of communication lib.
 *   Content: error (exception) classes
 *   Version: 0.1b
 *   Last-Mod: 24.09.2006
 *                                                                         
 *   Copyright (C) 2006 by Proshin Alexey                                  
 *   paa713@mail.ru                                                        
 *                                                                         
 *   The contents of this file are subject to free software; 
     you can redistribute it and/or modify  
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or     
 *   (at your option) any later version.                                   
 *                                                                         
 *   The contents of this file is distributed in the hope that it will be useful,       
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
 *   GNU General Public License for more details.                          
 *                                                                         
 *   You should have received a copy of the GNU General Public License     
 *   along with this program; if not, write to the                         
 *   Free Software Foundation, Inc.,                                       
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
 ***************************************************************************/
#ifndef __ERROR_COMMUNICATION_CLASSES__
#define __ERROR_COMMUNICATION_CLASSES__

#include <string>
#include <list>

// common error class
class _nerror
{
  public:
    
        _nerror() {};
        virtual ~_nerror() {};
        
        virtual std::string err_st() const =0;
        virtual std::string err_st_full() const =0;
        virtual long err_no() const =0;
};

#ifdef OLD_NET_LIB_LOG
// "log" class (its hold log in memory, you can get it`s content by call get with need log_level detail)
// For more info on log detail level see .h files for needed leyer.
// WARNING!!! If you need log you must call need_log() befor use it at first time, if not don`t call it

#include <clthread.hpp>

class _nlog
{
  private:

        static bool _need_log;
        static _locker lock;
        static int ref;
        static bool _have_date;
        
        struct _log_line
        {
          int level;
          std::string txt;

          bool operator<(const _log_line &l) const { return txt<l.txt; };
          bool operator==(const _log_line &l) const { if (txt==l.txt && level==l.level) return true; return false; };
        };
        
        static std::list<_log_line> *lines;
  public:
        _nlog();
        ~_nlog();
        
        void out(const std::string &st,int log_level);
        const std::string get(int log_level) const;
        void clear();
        void start();
        bool have_data() const { return _have_date; }
};

#endif

#endif

